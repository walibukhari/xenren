	## Xenrereb Hybrid Application   

## Migrate to Androidx
1. On your ** Android Studio ** > Refractor > Migrate to AndroidX
2. update `android/gradle.properties` by adding/uncommenting
```
android.useAndroidX=true
android.enableJetifier=true
```
3. clean and build/run
### Troubleshooting 
If you get an error such as `android.support.v4.content.FileProvider class not found` and you have migrated to AndroidX you have to change the name in `AndroidManifest.xml` to `androidx.core.content.FileProvider`

## Run App in Ios
clone or download branch iosComplete-
- `npm install`
- `Open Xcode`
- `File > Open > Go to ios folder > select "xenren.xcworkspace"`
- `Add developer account to Xcode.for more detail please visit this link.` (https://jameshfisher.com/2017/03/03/add-developer-account-to-xcode/)
- `Xcode > Product > Run `
```

## Run App in Android

## Deployment Steps

**Steps to run application in development mode**
 
- `npm install`
- `cd android`
- `./gradlew clean`
- `cd..`
-`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/`
- `react-native run-android`

Also you will need to run local server using

- `react-native start` or - `npm start`

for generate apk
 /node_modules/react-native/react.gradle file
and add the doLast right after the doFirst block, manually.

    doFirst { ... }
    //after this
    doLast {
        def moveFunc = { resSuffix ->
            File originalDir = file("$buildDir/generated/res/react/release/drawable-${resSuffix}");
            if (originalDir.exists()) {
                File destDir = file("$buildDir/../src/main/res/drawable-${resSuffix}");
                ant.move(file: originalDir, tofile: destDir);
            }
        }
        moveFunc.curry("ldpi").call()
        moveFunc.curry("mdpi").call()
        moveFunc.curry("hdpi").call()
        moveFunc.curry("xhdpi").call()
        moveFunc.curry("xxhdpi").call()
        moveFunc.curry("xxxhdpi").call()
    }
    
    Open 'android' folder in Android Studio
    place your keystore to 'android/app' folder
    
    change properties in 'android/gradle.properties'

    MYAPP_RELEASE_STORE_FILE=my-release-key.keystore
    MYAPP_RELEASE_KEY_ALIAS=my-key-alias
    MYAPP_RELEASE_STORE_PASSWORD=12345678
    MYAPP_RELEASE_KEY_PASSWORD=12345678
    
    ------------------

    In the menu bar, click Build > Build > Generate Signed Bundle/APK.
    In the Generate Signed Bundle or APK dialog, select Android App Bundle or APK and click Next
```



## Customize Calendar

goto ```node_modules/react-native-calendars/src/calendar/header/style.js``` and replace header with following

```    
header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      // paddingLeft: -10,
      // paddingRight: -10,
      marginTop: 6,
      alignItems: 'center',
      backgroundColor: '#57b029',
      color: '#fff',
      width: '103%',
      marginLeft: -6
},
```
