import React from 'react';
import { AppRegistry } from 'react-native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import { Sentry } from 'react-native-sentry';
import component from './src/reducers/component';
import auth from './src/reducers/authReducer';
import nav from './src/navigation/reducers';
import sharedOffice from './src/shared_office/reducer';
import jobDetail from './src/screens/jobs/reducer';
import rootSaga from './src/sagas';
import AppWithNavigationState from './src/navigation/navigator';
import accountReducer from './src/screens/account/reducers/accountReducer'
console.disableYellowBox = true;

// Declaring Reducers
const appReducer = combineReducers({
  nav,
  component,
  auth,
  sharedOffice,
  jobDetail,
  profile: accountReducer,
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(appReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

// Declare that AppWithNavigationState is Controlled by redux
const AppWithNavigationStateAndRedux = props => (
  <Provider store={store}>
    <AppWithNavigationState />
  </Provider>
);
Sentry.config('https://8e2f7372902d4b028ab5b5b7b332f2f5@sentry.io/1383731').install();
AppRegistry.registerComponent(
  'xenren',
  () => AppWithNavigationStateAndRedux,
);
