import initialState from './initialState';
import actionTypes from '../constants';

export default function authReducer(state = initialState.auth, action) {
  switch (action.type) {
    case actionTypes.FCM_TOKEN:
      return {
        ...state,
        fcmToken: action.fcm_token,
      };

    case actionTypes.CURRENT_QRCODE:
      return {
        ...state,
        qrCode: action.qrCode,
      };

    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        userData: action.data,
      };

    case actionTypes.SHARED_OFFICE:
      return {
        ...state,
        sharedOffice: action.data,
      };
    case actionTypes.SELECTED_SEAT_DATA:
      return {
        ...state,
        selectedSeatData: action.data,
      };

    case actionTypes.LOGIN_FAILED:
      return {
        ...state,
        loginError: action.error,
      };

    case actionTypes.UPLOAD_START:
      return {
        ...state,
        uploading: true,
      };

    case actionTypes.UPLOAD_DONE:
      return {
        ...state,
        uploading: false,
      };

    case actionTypes.USER_VERIFIED_SUCCESS:
      return {
        ...state,
        userData: { ...state.userData, isVerified: true },
      };


    default:
      return state;
  }
}
