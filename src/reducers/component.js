/* eslint-disable no-param-reassign */
import initialState from "./initialState";
import actionTypes from "../constants";

export default function componentReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.set_root:
      state.root = action.root;
      return state;
    default:
      return state;
  }
}
