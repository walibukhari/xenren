/* eslint-disable no-undef */

export default initialState = {
  root: null,
  auth: {
    fcm_token: {},
    userData: {
      isVerified: false,
    },
    sharedOffice: null,
    loginError: {},
    uploading: false,
    uploadError: false,
    qrCode: '',
    selectedSeatData: null,
  },
};
