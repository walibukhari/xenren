import { all } from 'redux-saga/effects';
import sharedOfficeSaga from './shared_office/saga';

export default function* rootSaga() {
  yield all([sharedOfficeSaga()]);
}
