import HttpRequest from '../components/HttpRequest';

const countryData = {
  country: async function countryData() {
    let resp = '';
    await HttpRequest.detectCountry().then((response) => {
      resp = response;
    }).catch(() => {});
    return resp;
  },
};

export default countryData;
