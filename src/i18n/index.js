

import i18n from 'react-native-i18n';
import english from './en';
import chinese from './cn';
import countryData from './detactCountry';
import LocalData from '../components/LocalData';

i18n.fallback = true;

export const DEFAULT_COUNTRY = countryData.country();
export const USER_PERFERRED_LOCALE = i18n.currentLocale();

export const Translations = {
  en: english,
  'zh-Hans-CN': chinese,
};

// logout current user to change App locale.
// after logout it will fetch country language and set app lang according to that country.
// but there is only 2 language available in app translation.
// 1 - Chinese  2 - English

export function translate(message) {
  let CC = '';
  LocalData.getLocale().then((response) => {
    let resp = JSON.parse(response);
    if (resp === null || resp === undefined) {
      CC = DEFAULT_COUNTRY._55.data.cc.toLowerCase();
      LocalData.setLocale({ locale: CC });
    } else {
      CC = resp.locale;
    }
  });
  if (CC === 'cn' || CC === 'tw' || CC === 'zh') {
    return Translations['zh-Hans-CN'][message];
  } else {
    return Translations['en'][message];
  }
}
