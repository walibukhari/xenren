import React from "react";
import { Text, View, Modal, Image, StyleSheet } from "react-native";
import FontStyle from "../../constants/FontStyle";
import { translate } from '../../i18n';

const SuccessModal = ({ visible }) => (
  <Modal
    animationType="fade"
    transparent={true}
    visible={visible}
    onRequestClose={() => console.log("Modal Closed")}
  >
    <View style={styles.dialogStyle}>
      <View style={styles.dialogBoxStyle}>
        <Image
          source={require("../../../images/login/login_success.png")}
          style={{ height: 90 }}
          resizeMode="contain"
        />
        <View style={{ height: 20 }} />
        <Text
          style={{
            fontFamily: FontStyle.Bold,
            color: "#36a563",
            fontSize: 20,
            textAlign: "center"
          }}
        >
          {translate('success')}
        </Text>
      </View>
    </View>
  </Modal>
);

export default SuccessModal;

const styles = StyleSheet.create({
  dialogBoxStyle: {
    width: 250,
    height: 200,
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  }
});
