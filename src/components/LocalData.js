import AsyncStorage from "@react-native-community/async-storage";

export default {
  getUserData() {
    return AsyncStorage.getItem("@UserData");
  },
  getSaveData() {
    return AsyncStorage.getItem("@AppData");
  },
  getFileData() {
    return AsyncStorage.getItem("@FileData");
  },
  getFile1Data() {
    return AsyncStorage.getItem("@FileData1");
  },
  getVerifyData() {
    return AsyncStorage.getItem("@VerifyData");
  },
  getLocale() {
    return AsyncStorage.getItem("@setLocale");
  },
  setUserData(data) {
    return AsyncStorage.setItem("@UserData", JSON.stringify(data));
  },

  setAppData(data) {
    return AsyncStorage.setItem("@AppData", JSON.stringify(data));
  },

  setfileData(data) {
    return AsyncStorage.setItem("@FileData", JSON.stringify(data));
  },

  setfile1Data(data) {
    return AsyncStorage.setItem("@FileData1", JSON.stringify(data));
  },

  setVerifyData(data) {
    return AsyncStorage.setItem("@VerifyData", JSON.stringify(data));
  },

  setLocale(data) {
    return AsyncStorage.setItem("@setLocale", JSON.stringify(data));
  },
};
