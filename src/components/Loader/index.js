import React from "react";
import { View, Modal, ActivityIndicator } from "react-native";

const Loader = ({ isLoading }) => (
  <Modal
    visible={isLoading}
    animationType="fade"
    transparent={true}
    onRequestClose={() => console.log("Modal Closed")}
  >
    <View
      style={{
        backgroundColor: "rgba(52, 52, 52, 0.8)",
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <ActivityIndicator color="#fff" />
    </View>
  </Modal>
);

export default Loader;
