import React, { useState } from 'react';
import {
    View,
    Text,
    ActivityIndicator,
    Image,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import Config from '../../Config';


export const ImageDeferrable = ({ source, style, children }) => { 
    let [loading, isStillLoading] = useState(true);
    return (
        <View
            style={[imageDeferrableStyle.img_container, style || {}]}>
            {loading == true && (
                <ActivityIndicator size="small" style={imageDeferrableStyle.loader} color={Config.primaryColor} />
            )}
            <Image
                style={imageDeferrableStyle.image}
                resizeMode="cover"
                onLoad={() => isStillLoading(false)}
                source={source} />
                { children }
        </View>
    );
}

const imageDeferrableStyle = StyleSheet.create({
    img_container: {
        overflow: 'hidden',
        flex: 1,
        justifyContent: "center",
        alignSelf: "center"
    },
    loader: {
        flex: 1,
        alignSelf: 'center',
        alignContent: 'center',
    },
    image: {
        flex: 1,
        height: "100%",
        width: "100%"
    }
});