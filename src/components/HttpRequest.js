/* eslint-disable no-console */
/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-plusplus */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable consistent-return */
/* eslint-disable max-len */
/* eslint-disable indent */
import axios from 'axios';
import qs from 'qs';
import Config from '../Config';
import DeviceInfo from "react-native-device-info";

let baseUrlConfig = Config.baseUrl;
let baseUrlV1Config = Config.baseUrlV1;
export const baseUrlV1 = baseUrlV1Config;
export const baseUrl = baseUrlConfig;
export const paypalUrl = Config.basePathPayPal; // 'https://api.sandbox.paypal.com/v1';
/* TODO:
 * @Shiven if this path is not being used, then please remove it
 * */

export default {
  /** AUTH */
  deviceToken(accessToken, deviceToken, deviceOs) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('device_token', deviceToken);
    formData.append('device_os', deviceOs);

    return axios.post(`${baseUrlV1}/save-token`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  me(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  getLocation(accessToken,ip){
    const url = `${baseUrl}/get/location?ip=${ip}`;
    console.log(url);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(url, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  login(email, password) {
    const formData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    return axios.post(`${baseUrlV1}/auth/login`, formData);
  },

  register(name, last_name, email, password, token) {
    console.log(name,last_name,email,password);
    const formData = new FormData();
    formData.append('real_name', name);
    formData.append('last_name', last_name);
    formData.append('email', email);
    formData.append('password', password);
    formData.append('device_token', token);
    return axios.post(`${baseUrlV1}/auth/register`, formData);
  },

  forgotPassword(email) {
    const formData = new FormData();
    formData.append('email', email);
    return axios.post(`${baseUrlV1}/auth/forgot-password`, formData);
  },

  uploadPicture(type, image, user_id) {
    const formData = new FormData();
    formData.append('img_type', type);
    formData.append('image', image);
    formData.append('user_id', user_id);
    return axios
        .post(`${baseUrl}/uploadIdCard`, formData, {
          headers: {
            Accept: 'application/json',
            'content-type': 'multipart/form-data',
          },
        })
        .then((response) => response)
        .then((data) => data)
        .catch((error) => {
          throw error;
        });
  },
  getUserProfile(accessToken) {
    return axios
      .get(`${baseUrlV1}/me`, {
        headers: {
          Accept: 'application/json',
          Authorization: accessToken,
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },
  async getUsersRatings(accessToken) {
    try {
      const response = await fetch(`${baseUrlV1}/me/ratings`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
      });
      const res = await response.json();
      return res;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  updateContactInfo(qq_id, wechat_id, skype_id, handphone_no, accessToken) {
    const formData = new FormData();
    formData.append('qq_id', qq_id);
    formData.append('wechat_id', wechat_id);
    formData.append('skype_id', skype_id);
    formData.append('handphone_no', handphone_no);
    return axios
      .post(`${baseUrlV1}/me/updateContactInfo`, formData, {
        headers: {
          Accept: 'application/json',
          Authorization: accessToken,
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },
  async getUsersReviews(accessToken) {
    try {
      const response = await fetch(`${baseUrlV1}/me/reviews`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
      });
      const res = await response.json();
      return res;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  async getUserInfo(accessToken) {
    return axios.get(`${baseUrlV1}/me/userInfo`, {
      headers: {
        Authorization: accessToken,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });

    // try {
    //   const response = await fetch(`${baseUrlV1}/me/userInfo`, {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'application/json',
    //       'Content-Type': 'application/json',
    //       Authorization: accessToken,
    //     },
    //   });
    //   const res = await response.json();
    //   return res;
    // } catch (errors) {
    //   console.log('catch', errors);
    // }
  },
  async getUsersPortfolio(accessToken) {
    try {
      const response = await fetch(`${baseUrlV1}/me/portfolio`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
      });
      const res = await response.json();
      console.log('portfolio response');
      console.log(res);
      return res;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  async getUsersComments(accessToken) {
    try {
      const response = await fetch(`${baseUrlV1}/me/comments`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
      });
      const res = await response.json();
      return res;
    } catch (errors) {
      console.log('catch', errors);
    }
  },

  getSkills() {
    return axios
      .get(`${baseUrl}/skills`, {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },

  getLanguages() {
    return axios
      .get(`${baseUrlV1}/languages`, {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },

  getCountries(city) {
    return axios
      .get(`${baseUrl}/cities-country?city_id=${city}`, {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },
  getCity() {
    return axios.get(`${baseUrl}/cities-only`, {
        headers: {
          Accept: 'application/json',
        },
      })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },
  async updatePersonalInfo(
    nick_name,
    about_me,
    email,
    password,
    qq,
    weChat,
    skype,
    headphone,
    accessToken,
  ) {
    try {
      const response = await fetch(`${baseUrlV1}/me/update/personal-address`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
        body: JSON.stringify({
          nick_name,
          about_me,
          email,
          password,
          qq,
          weChat,
          skype,
          headphone,
        }),
      });
      const res = await response.json();
      if (res.status === 'success') {
        // console.log('successsssss')
        return res;
      }
      const error = res;
      throw error;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  async updateUsersAddress(accessToken, country, city, address, language) {
    try {
      const response = await fetch(`${baseUrlV1}/me/update/address`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
        body: JSON.stringify({
          country,
          city,
          address,
          language,
        }),
      });
      const res = await response.json();
      console.log('Response from Address API------->', res);
      if (res.status === 'success') {
        return res;
      }
      const error = res;
      throw error;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  async portfolioCreate(
    accessToken,
    job_position_id,
    title,
    url,
    hidUploadedFile,
  ) {
    const formData = new FormData();
    formData.append('job_position_id', job_position_id);
    formData.append('title', title);
    formData.append('url', url);
    let i;
    for (i = 0; i < hidUploadedFile.length; i++) {
      formData.append('hidUploadedFile[]', hidUploadedFile[i]);
    }
    return axios({
      method: 'post',
      url: `${baseUrlV1}/me/portfolio/create`,
      data: formData,
      headers: {
        Authorization: accessToken,
      },
    })
      .then((response) => response)
      .then((data) => data)
      .catch((error) => {
        throw error;
      });
  },
  async updateJobDetail(
    accessToken,
    experience_year,
    job_position,
    job_title,
    skills,
    pay_hourly,
    currency,
  ) {
    try {
      const response = await fetch(`${baseUrlV1}/me/update/job-detail`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: accessToken,
        },
        body: JSON.stringify({
          experience_year,
          job_position,
          job_title,
          skills,
          pay_hourly,
          currency,
        }),
      });

      const res = await response.json();

      console.log(res);
      if (res.status === 'success') {
        return res;
      }
      const error = res;
      throw error;
    } catch (errors) {
      console.log('catch', errors);
    }
  },
  updateProfilePicture(accessToken, profileImage) {
    const imageData = new FormData();
    imageData.append('avatar', profileImage);
    return axios
      .post(`${baseUrlV1}/me/updateProfilePicture`, imageData, {
        headers: {
          Authorization: accessToken,
        },
      })
      .then((response) => {
        console.log(response.data);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },
  uploadId(accessToken, profileImage) {
    const imageData = new FormData();
    imageData.append('avatar', profileImage);
    return axios
      .post(`${baseUrl}/uploadIdCard`, imageData, {
        headers: {
          Authorization: accessToken,
        },
      })
      .then((response) => {
        console.log(response.data);
        return response.data;
      })
      .catch((error) => {
        throw error;
      });
  },

  // getSingleChat(accessToken, chatRoomID) {
  //   const AuthStr = "Bearer ".concat(accessToken);
  //   return axios.get(`${baseUrlV1}/sharedOffice/room/chat/${chatRoomID}`, {
  //     headers: {
  //       Authorization: AuthStr
  //     }
  //   });
  // },

  // Select Chat Room
  getSharedOffices(accessToken, lat, lang, ip = '', map = false) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(AuthStr);
    console.log(`${baseUrl}/sharedoffice?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}&map=${map}`);
    return axios.get(`${baseUrl}/sharedoffice?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}&map=${map}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Select Chat Room
  getSharedOfficesMapData(accessToken, lat, lang, ip = '', map = false) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(AuthStr);
    console.log('device info');
    console.log(DeviceInfo);
    console.log(DeviceInfo.getTotalMemory());
    let totalMemory = DeviceInfo.getTotalMemory();
    let limit ;
    if(totalMemory >= 3972603904) {
      limit = 300;
    } else if(totalMemory >= 2972603904) {
      limit = 350;
    } else if(totalMemory >= 1500000000) {
      limit = 150;
    } else if(totalMemory >= 1400000000) {
      limit = 100;
    } else {
      limit = 50;
    }
    console.log(`${baseUrl}/get-map-data?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}&map=${map}&limit=${limit}`);
    return axios.get(`${baseUrl}/get-map-data?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}&map=${map}&limit=${limit}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Select Chat Room
  getAllSharedOffices(accessToken, lat, lang, ip = '') {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(`${baseUrl}/sharedofficeAll?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}`);
    return axios.get(`${baseUrl}/sharedofficeAll?lat=${lat}&lang=${lang}&ip=${ip}&mobileApp=${true}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },


  getSharedOfficeFilterRecord(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(`${baseUrl}/sharedofficeFilterData`);
    return axios.get(`${baseUrl}/sharedofficeFilterData`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getSharedOfficeDetail(accessToken, userID, officeId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('user_id', userID);
    formData.append('office_id', officeId);
    return axios.post(`${Config.baseUrl}/officeDetailsBtn`, formData, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  getSharedOfficeDetailById(accessToken, userID, officeId) {
    const AuthStr = `Bearer ${accessToken}`;
    const formData = new FormData();
    console.log(`${Config.baseUrl}/officeSpaceDetailsBtn/${officeId}`);
    console.log(AuthStr);
    console.log(userID);
    console.log(officeId);
    formData.append('user_id', userID);
    formData.append('office_id', officeId);
    return axios.post(`${Config.baseUrl}/officeSpaceDetailsBtn/${officeId}`, formData, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  // Indoor Chat
  // Check Contact of Shared Office Users List
  getSharedOfficeUsersList(accessToken, officeId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/sharedOffice/users/${officeId}`, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  getJoinedGroupsList(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/joined-groups`, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  sendMsgSingleChat(accessToken, msg, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('message', msg);
    return axios.post(`${baseUrlV1}/chat/${userId}`, formData, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  getSingleChat(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/chat/${userId}`, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  // Contact Management
  reportUser(accessToken, userId, reason) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('user_id', userId);
    formData.append('reasson', reason);
    return axios.post(`${baseUrlV1}/report-user`, formData, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  blockSingleUser(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('user_id', userId);
    return axios.post(`${baseUrlV1}/contact-management/blockSingleUser`, formData, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  },

  unblockSingleUser(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const body = new FormData();
    body.append('user_id', '2');
    console.log('unblock single user api -----> ', body);
    return axios.post(`${baseUrlV1}/contact-management/unblockUser`, body, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  blockMultipleUsers(accessToken, userIds) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const body = { user_id: '2,3,4,5' };
    return axios.post(`${baseUrlV1}/contact-management/BlockMultiUser`, body, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  },

  unBlockMultipleUsers(accessToken, userIds) {
    const AuthStr = 'Bearer '.concat(accessToken);
    let body = new FormData();
    body.append('user_id', userIds);
    body.append('user_id', '2,3,4,5');
    return axios.post(`${baseUrlV1}/contact-management/unblockUser`, body, {
      headers: {
        Authorization: AuthStr,
        'Content-Type': 'application/json',
      },
    });
  },

  // //////////////////////////////////////////////////////

  blockUsersList(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/contact-management/usersList`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Hide Contact Info
  hideInfo(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/contact-management/hideContactPlatformMultipleUsers`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  blockedUsers(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log('get blocked users api -------> ', AuthStr);
    return axios.get(`${baseUrlV1}/contact-management/getBlockedUsers`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Block multiple Users
  // blockMultipleUsers(accessToken, body) {
  //   const AuthStr = 'Bearer '.concat(accessToken);
  //   return axios({
  //     method: 'put',
  //     url: `${baseUrlV1}/contact-management/blockUsers`,
  //     data: body,
  //     headers: {
  //       Authorization: AuthStr,
  //       'Content-Type': 'application/x-www-form-urlencoded',
  //     },
  //   });
  // },

  // blockSingleUser(accessToken, formData) {
  //   const AuthStr = 'Bearer '.concat(accessToken);
  //   return axios({
  //     method: 'put',
  //     url: `${baseUrlV1}/contact-management/blockUserFromChat`,
  //     data: formData,
  //     headers: {
  //       Authorization: AuthStr,
  //     },
  //   });
  // },

  // unblockSingleUser(accessToken, body) {
  //   const AuthStr = 'Bearer '.concat(accessToken);
  //   return axios.put(`${baseUrlV1}/contact-management/unblockUsers`, body, {
  //     headers: {
  //       Authorization: AuthStr,
  //       'Content-Type': 'application/json',
  //     },
  //   });
  // },

  // hidePlatformContacts(accessToken, formData) {
  //   const AuthStr = 'Bearer '.concat(accessToken);
  //   return axios({
  //     method: 'put',
  //     url: `${baseUrlV1}/contact-management/blockUser`,
  //     data: formData,
  //     headers: {
  //       Authorization: AuthStr,
  //     },
  //   });
  // },

  // Other User's Profile
  getUserProfileByID(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/public-profile/${userId}`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  // Other User's Profile
  getUserOpenProjects(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/get-my-open-projects/`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  getUserPlatformReview(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/user/platformReviews/${userId}`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },
  getUserCustomerReview(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/user/customerReviews/${userId}`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  getUserPortfoilo(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/portfolio/${userId}`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  // Find Project Module
  searchJobs(projectType, filters) {
    return axios.get(`${baseUrlV1}/jobs/search?project_type=${projectType}&${filters}`);
  },

  dislikeProject(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/addThumbdownJob`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  addFavouriteProject(accessToken, projectType, projectId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    let formData = new FormData();
    formData.append('projectId', projectId);
    formData.append('projectType', projectType);
    return axios.post(`${baseUrlV1}/addFavouriteJob`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },
  makeOffer(accessToken, formData) {
    console.log('Form Data----------->', formData);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/jobs/make_offer`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getDiscussRoomChat(accessToken, projectId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(AuthStr);
    console.log(`${baseUrlV1}/joinDiscussion/getProject/${projectId}`);
    return axios.get(`${baseUrlV1}/joinDiscussion/getProject/${projectId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getMySpaces(accessToken, projectId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/my-spaces`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  sendMsgDiscussRoom(accessToken, chatRoomID, msg) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('projectChatRoomId', chatRoomID);
    formData.append('message', msg);
    return axios.post(`${baseUrlV1}/sendMessage`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  uploadFileDiscussRoom(accessToken, profileImage, projectChatRoomId, msg) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const imageData = new FormData();
    imageData.append('projectChatRoomId', projectChatRoomId);
    imageData.append('message', msg);
    imageData.append('file', profileImage);
    console.log('Body Data ---- ', imageData);
    return axios.post(
      `${baseUrlV1}/projectChat/postProjectChatFile`,
      imageData,
      {
        headers: {
          Authorization: AuthStr,
          // Accept: 'application/json',
          // 'content-type': 'multipart/form-data;',
        },
      },
    );
  },

  freelancerDetail(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/freelancer_detail/${userId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  freelancerThumbUP(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/joinDiscussion/thumbup/${userId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  freelancerThumbDown(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/joinDiscussion/thumbdown/${userId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Find Freelancers

  findFreelancers(filters, accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/freelancer/search?${filters}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  sendOffer(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/freelancer/sendOffer`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  inviteFreelancer(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log('HTTP PAge-------------->', accessToken, formData);
    return axios.post(`${baseUrlV1}/freelancer/inviteToWork`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  addFreelancerToFAV(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/freelancer/addToFavorite/${userId}`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  removeFreelancerToFAV(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/freelancer/removeFromFavorite/${userId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Facebook Binding
  registerWithFacebook(userName, email, fb_id) {
    const formData = new FormData();
    formData.append('real_name', userName);
    formData.append('email', email);
    formData.append('facebook_id', fb_id);
    console.log('facebook signup', formData);
    return axios.post(`${baseUrlV1}/auth/facebook`, formData);
  },

  loginWithFacebook(accessToken, fb_id) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('facebook_id', fb_id);
    console.log('Form Data --- ', formData);
    return axios.post(`${baseUrlV1}/auth/BindFacebook`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  setFacebookPassword(accessToken, password) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('password', password);
    return axios.post(`${baseUrlV1}/auth/set_password`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getAllNotifications(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/get_all_notification`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },
  getSystemNotification(accessToken, id) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/system_notification`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },
  newNotificationDetail(accessToken, id) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios({
      method: 'put',
      url: `${baseUrlV1}/me/notification_detail/${id}`,
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getInboxNotification(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/inbox_notification`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  // QRScanner Flow APIs
  pickSeatByQRCode(accessToken, qr_seat, office_id, type = 1, end_time = null, start_date, end_date, is_custom_date) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('qr_seat', qr_seat);
    formData.append('office_id', office_id);
    formData.append('scan_type', type);
    formData.append('end_time', end_time);
    formData.append('start_date', start_date);
    formData.append('end_date', end_date);
    formData.append('is_custom_date', is_custom_date);

    console.log('HttpRequest pickSeatByQRCode', formData);
    return axios.post(`${baseUrl}/sharedofficeQrSeat`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  changeSeatByQR(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.put(`${baseUrl}/sharedofficeChangeSeatByQR`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  scanOtherTypeBarCodes(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/scan-other-barcode`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  getOtherTypeBarCodes(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/scan-other-barcode`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  },

  stopTimer(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrl}/sharedofficeStopTimer`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  getCost(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrl}/sharedofficeGetCost`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  getUserImage(accessToken) {
    const AuthStr = `Bearer ${accessToken}`;
    return axios.get(`${baseUrl}/userImage`, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  reset(accessToken) {
    const AuthStr = `Bearer ${accessToken}`;
    return axios.get(`${baseUrl}/reset`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  submitUserIdentity(accessToken, formData) {
    const AuthStr = `Bearer ${accessToken}`;
    return axios.post(`${baseUrl}/submitUserIdentity`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  // Paypal APIs ------ Top-Up Module
  generateToken() {
    return axios({
      method: 'post',
      url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
      headers: {
        Authorization:
          'Basic QVRMT2liNEdmMnZIU2ttSThVUVh4bUlNcXhJTEEtZmhfbTMzN1JrNFl3dEhhdEQ1VUQ0NVM4N3c0alpfaUxFS3Bnb0prMHU0ZUViM2FVdHM6RU9MX1l2WnY4M0NScWxzb0Z3Mk9mTWhTRWZhS29FZFRZbWhYUDV1ZXFFOWVsSFpaYUlGQWd6UFR6YkFVRWFLTGZXR2lxVGw0NXNlRjRIMDk=',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: qs.stringify({
        grant_type: 'client_credentials',
      }),
    });
  },

  initiateRevenueMonsterPayment(accessToken, amount) {
    let formData = new FormData();
    formData.append('amount', amount);
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log('initiateRevenueMonsterPayment', `${baseUrlV1}/topup-revenue-monster`, formData);
    return axios.post(`${baseUrlV1}/topup-revenue-monster`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  initiatePayment(paypalToken, data) {
    return axios({
      method: 'post',
      url: 'https://api.sandbox.paypal.com/v1/payments/payment',
      headers: {
        Authorization: `Bearer ${paypalToken}`,
        'Content-Type': 'application/json',
      },
      data,
    });
  },

  showPayment(paypalToken, payment_id) {
    return axios({
      method: 'get',
      url: `https://api.sandbox.paypal.com/v1/payments/payment/${payment_id}`,
      headers: {
        Authorization: `Bearer ${paypalToken}`,
        'Content-Type': 'application/json',
      },
    });
  },

  executePayment(paypalToken, payment_id, payer_id) {
    const data = {
      payer_id,
    };
    return axios({
      method: 'post',
      url: `https://api.sandbox.paypal.com/v1/payments/payment/${payment_id}/execute`,
      data,
      headers: {
        Authorization: `Bearer ${paypalToken}`,
        'Content-Type': 'application/json',
      },
    });
  },

  topUpBalance(accessToken, data, amount) {
    let formData = new FormData();
    formData.append('payment_method_response', JSON.stringify({ data }));
    formData.append('amount', amount);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/topup-amount`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  registered(data, key) {
    const formData = new FormData();
    formData.append('sixDigitCode', data);
    formData.append('key', key);
    return axios.post(`${baseUrlV1}/auth/registered`, formData);
  },

  resendCode(email, key) {
    return axios.get(`${baseUrl}/resend-code?key=${key}&email=${email}`);
  },

  withdrawApi(accessToken, amount) {
    let formData = new FormData();
    formData.append('amount', amount);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/withdraw-api`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  logout(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/auth/logout`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  sendInboxMessage(accessToken, inboxId, toUser, message, category) {
    const AuthStr = 'Bearer '.concat(accessToken);
    let formData = new FormData();
    formData.append('inbox_id', inboxId);
    formData.append('to_user', toUser);
    formData.append('message', message);
    formData.append('category', category);
    return axios.post(`${baseUrlV1}/send-inbox-message`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  acceptOffer(accessToken, receiverId, inboxId) {
    let formData = new FormData();
    formData.append('inboxId', inboxId);
    formData.append('receiverId', receiverId);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/acceptOffer`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  rejectOffer(accessToken, receiverId, inboxId) {
    let formData = new FormData();
    formData.append('inboxId', inboxId);
    formData.append('receiverId', receiverId);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/acceptOffer`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  deleteInboxChat(accessToken, inboxId) {
    let formData = new FormData();
    formData.append('inboxId', inboxId);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/inbox/trash`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  sharedOfficeRating(accessToken, officeId, rate, comment) {
    let formData = new FormData();
    formData.append('office_id', officeId);
    formData.append('rate', rate);
    formData.append('comment', comment);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrl}/sharedOffice/rating`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  uploadUserInfo(accessToken, imageObj, name, profession) {
    let formData = new FormData();
    formData.append('image', imageObj);
    formData.append('name', name);
    formData.append('job_position_id', profession);
    const AuthStr = 'Bearer '.concat(accessToken);
    setTimeout(function () {
      console.log(formData);
      console.log(AuthStr);
    },200);
    return axios.post(`${Config.baseUrlV1}/updateUserProfile`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  userIdentity(accessToken, status) {
    const formData = new FormData();
    formData.append('status', status);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrl}/saveStatus`, formData, {
      headers: {
        Authorization: AuthStr,
        Accept: 'application/json',
      },
    });
  },

  hideFromAll(accessToken, formData) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${baseUrlV1}/contact-management/hideContactPlatformAllUsers`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getMoreSharedOfficeData(accessToken, url) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(url, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getOnlyCountries() {
    return axios.get(`${baseUrl}/countries`, {
      headers: {
        Accept: 'application/json',
      },
    });
  },

  getNotificationSettings(accessToken) {
    const AuthStr = 'Bearer '.concat(accessToken);
    console.log(AuthStr);
    console.log(`${baseUrlV1}/push-notification-settings`);
    return axios.get(`${baseUrlV1}/push-notification-settings`, {
      headers: {
        Accept: 'application/json',
        Authorization: AuthStr,
      },
    });
  },

  saveNotificationSettings(accessToken, formObj) {
    let formData = new FormData();

    formData.append('low_fixed_price', formObj.low_fixed_price);
    formData.append('high_fixed_price', formObj.high_fixed_price);
    formData.append('low_hourly_price', formObj.low_hourly_price);
    formData.append('high_hourly_price', formObj.high_hourly_price);
    formData.append('low_atleast_spend', formObj.low_atleast_spend);
    formData.append('high_atleast_spend', formObj.high_atleast_spend);
    formData.append('package_id', formObj.package_id);
    formData.append('keyword', formObj.keyword);
    formData.append('language', formObj.language);
    formData.append('country', formObj.country);
    formData.append('country_type', formObj.country_type);
    formData.append('active', formObj.active);

    console.log(formData);
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.post(`${Config.baseUrlV1}/create/push-notification-settings`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  getSharedOfficeBookingUserDetail(accessToken, userId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    return axios.get(`${baseUrlV1}/me/sharedOfficeBookingUserDetail?user_id=${userId}`, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  deleteSharedOfficeBookingRequest(accessToken, userId, clientEmail) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('user_id', userId);
    formData.append('client_email', clientEmail);
    return axios.post(`${baseUrlV1}/me/deleteSharedOfficeBooking`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  sendSharedOfficeReplyMessage(accessToken, message, sendTo) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('message', message);
    formData.append('send_to', sendTo);
    return axios.post(`${baseUrlV1}/me/sendBookingMessage`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  detectCountry() {
    return axios.get(Config.getIpAddress, {
      headers: {
        Accept: 'application/json',
      },
    });
  },

  awardProject(accessToken, inboxId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('inboxId', inboxId);
    return axios.post(`${baseUrlV1}/me/awardProject`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  rejectProjectOffer(accessToken, inboxId, receiverId) {
    const AuthStr = 'Bearer '.concat(accessToken);
    const formData = new FormData();
    formData.append('inboxId', inboxId);
    formData.append('receiverId', receiverId);
    return axios.post(`${baseUrlV1}/me/rejectProjectOffer`, formData, {
      headers: {
        Authorization: AuthStr,
      },
    });
  },

  // Link Paypal
  // linkPaypalAccount() {
  //   return axios.post(`${baseUrlV1}/`)
  // },
};
