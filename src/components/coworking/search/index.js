import React, { useState } from 'react';
import {
    View,
    TouchableOpacity,
    Image,
    Text,
    ActivityIndicator,
    StyleSheet
} from 'react-native';
import Config from '../../../Config';


export const ItemDescriptor = ({ item, locale: stateLocale }) => {
    const locale = useState(stateLocale);
    const office_name = locale === 'cn' || locale === 'tw' || locale === 'zh' ? item.version_chinese === 'true' ? item.office_name_cn : item.office_name : item.version_english === 'false' || item.version_english === null ? item.office_name_cn : item.office_name;
    const description = locale === 'cn' || locale === 'tw' || locale === 'zh' ? item.version_chinese === 'true' ? item.office_description_cn : item.description : item.version_english === 'false' || item.version_english === null ? item.office_description_cn : item.description;
    return (
        <View
            style={itemDescriptorStyle.container}>
            <Text style={itemDescriptorStyle.officeName}>
                {office_name}
            </Text>
            <Text
                textBreakStrategy="highQuality"
                style={itemDescriptorStyle.location}
            >
                { item.location }
            </Text>
            <Text
                numberOfLines={2}
                style={itemDescriptorStyle.description}>
                {description}
            </Text>
            <View style={itemDescriptorStyle.informationContainer}>
                <View style={itemDescriptorStyle.iconsContainer}>
                    <Image
                        style={itemDescriptorStyle.icon}
                        resizeMode="cover"
                        source={require('../../../../images/search/couch.png')} />
                    <Image
                        style={itemDescriptorStyle.icon}
                        resizeMode="cover"
                        source={require('../../../../images/search/printer.png')} />
                    <Image
                        style={itemDescriptorStyle.icon}
                        resizeMode="cover"                        
                        source={require('../../../../images/search/wifi.png')} />
                </View>
                <View style={itemDescriptorStyle.pricingContainer}>
                    <Text style={itemDescriptorStyle.price}>${item.monthly_rate || 0}</Text>
                    <Text style={itemDescriptorStyle.priceUnit}>/mo</Text>
                </View>
            </View>
        </View>
    )
}

const itemDescriptorStyle = StyleSheet.create({
    container: {
        alignContent: 'stretch',
        backgroundColor: '#fff',
        flex: 1,
        flexGrow: 2,
        flexDirection: 'column',
        padding: 10
    },
    officeName: {
        fontSize: 18,
        fontWeight: '500',
        color: '#4b4b4b',
        marginBottom: 2.5
    },
    location: {
        color: Config.primaryColor,
        fontSize: 13,
        marginTop: 2.5,
        marginBottom: 2.5
    },
    description: {
        color: '#808080',
        fontSize: 12,
        marginRight: 20,
        overflow: "hidden",
        marginTop: 2.5,
        marginBottom: 5
    },
    informationContainer: {
        flexDirection: "row",
        marginTop: 2.5,
        justifyContent: 'space-between'
    },
    pricingContainer: {
        flexDirection: "row",
        alignItems: "flex-end"
    },
    price: {
        fontSize: 15,
        fontWeight: 'bold',
        color: Config.primaryColor
    },
    priceUnit: {
        fontSize: 12,
        color: '#808080'
    },
    iconsContainer: {
        flexDirection: "row",
        marginTop: 2.5,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    icon: {
        height: 20,
        width: 20,
        flexBasis: 20,
        borderRadius: 5,
        marginRight: 5
    }
});

export const ResultItem = ({ item, locale, onPress }) => {
    const [imgLoading, setImgLoading] = useState(true);
    return (
        <TouchableOpacity
            onPress={onPress}
            style={{
                alignItems: 'center',
                justifyContent: "center",
            }}
        >
            <View style={resultItemStyle.resultItem} >
                <View
                    style={resultItemStyle.img_container}>
                    {imgLoading === true && (
                        <ActivityIndicator size="small" style={resultItemStyle.loader} color={Config.primaryColor} />
                    )}
                    <Image
                        style={resultItemStyle.image}
                        resizeMode="cover"
                        onLoad={() => setImgLoading(false)}
                        source={{ uri: `${Config.webUrl}/${item.image}`, cache: 'force-cache' }}
                    />

                </View>
                <ItemDescriptor item={item} locale={locale} />
            </View>
        </TouchableOpacity>
    )
};

const resultItemStyle = StyleSheet.create({
    resultItem: {
        width: "100%",
        overflow: "hidden",
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'white',
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 2
    },
    img_container: {
        width: "35%",
        justifyContent: "center",
        alignSelf: "center"
    },
    loader: { top: 45 },
    image: { flex: 1 }
})
