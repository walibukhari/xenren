/* eslint-disable radix */
/* eslint-disable no-useless-return */
/* eslint-disable no-alert */
/* eslint-disable prefer-const */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-empty */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable no-dupe-keys */
/* eslint-disable consistent-return */
/* eslint-disable react/no-string-refs */
/* eslint-disable no-shadow */
/* eslint-disable max-len */
/* eslint-disable quotes */
/* eslint-disable no-mixed-operators */
/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-key */
/* eslint-disable global-require */
/* eslint-disable react/no-deprecated */

import React, { Component } from "react";
import { connect } from "react-redux";
// android only
import KeyEvent from "react-native-keyevent";
import {
  Text,
  View,
  TextInput,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Dimensions,
  ActivityIndicator,
  Keyboard,
  Modal,
  StyleSheet
} from "react-native";
import _ from "lodash";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Feather from "react-native-vector-icons/Feather";
import { translate } from "../i18n";
import Config from "../Config";
import FooterTabs from "./FooterTabs";
import actions from "../shared_office/actions";
import LocalData from "../components/LocalData";
import HttpRequest from "../components/HttpRequest";
import TimerComponent from "./TimerComponent";
import FontStyle from "../constants/FontStyle";
import AddInfoPopUp from "../screens/profile/AddInfoPopUp";
import Input from '../screens/profile/Input';
import ImagePicker from "react-native-image-picker";
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import moment from "moment";


let { width, height } = Dimensions.get("window");
class DashBoard extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    const isPortrait = () => {
      const dim = Dimensions.get('screen');
      if (dim.height >= dim.width) {
        return 'portrait'
      }
      else {
        return 'landscape'
      }
    }

    Dimensions.addEventListener('change', (handler) => {
      height = handler.screen.height;
      width = handler.screen.width;
      this.setState({
        orientation: isPortrait()
      });
      this.forceUpdate();
    });

    this.root = this.props.component.root;
    this.state = {
      totalAmountSpent: 0,
      totalTimeSpent: "loading",
      currency: "",
      showAutocomplete: true,
      autocompleteText: "",
      autocompletePosition: {
        x: 0,
        y: -200,
        width: 100,
      },
      buttons: [
        {
          image: require("../../images/share_office/icon_mid_1.png"),
          label: translate("find_project"),
        },
        {
          image: require("../../images/share_office/icon_mid_2.png"),
          label: translate("find_freelancer"),
        },
        {
          image: require("../../images/share_office/icon_mid_3.png"),
          label: translate("find_cofounder"),
        },
      ],
      jobPosition: [
        {
          id: 1,
          name_en: 'Designer',
        },
        {
          id: 3,
          name_en: 'Software Engineering',
        },
        {
          id: 4,
          name_en: 'Investor',
        },
        {
          id: 5,
          name_en: 'Product Manager',
        },
        {
          id: 6,
          name_en: 'Marketing Executive',
        },
      ],
      jobPositionId: '',
      selectedButton: 0,
      skills: [],
      filteredSkills: [],
      selectedSkills: [],
      resultShow: false,
      // showBind: false,
      showModal: false,
      showCost: true,
      disable: true,
      seatNumber: '',
      duplicate: '',
      interval: false,
      canceled: false,
      infoModal: false,
      infoLoading: false,
      name: '',
      activeTabIndex: 0,
      buttonColor: false,
      tmpImg: `${Config.webUrl}images/image.png`,
      locale: '',
      orientation: isPortrait(),
      currentDate: '2019-10-04'
    };
  }
  Interval = 0;
  componentDidMount() {
    console.log('in dashboard');
    LocalData.getLocale().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ locale: resp.locale });
    });
    this.setState({ name: this.props.userData.real_name });
    LocalData.getSaveData().then((response) => {
      response = JSON.parse(response);
      if (response !== null) {
        if (response.userInfo === true) { this.setState({ infoModal: true }); }
        this.setState({ canceled: true });
      }
    });
    if (this.state.showCost) {
      this.getCost();
    }
    this.getSkillsList();
    // remove last selectedSkills
    KeyEvent.onKeyDownListener((keyCode) => {
      if (keyCode === 67 && this.state.autocompleteText === "") {
        let selectedSkills = this.state.selectedSkills;
        if (selectedSkills.length !== 0) {
          selectedSkills.splice(selectedSkills.length - 1, 1);
          this.setState({ selectedSkills });
        }
      }
    });
  }

  getCost() {
    if (this.props.userData.token) {
      HttpRequest.getCost(this.props.userData.token)
          .then((response) => {
            let amount = 0;
            let time = "";
            let userFacebookId = response.data.user.facebook_id;
            if (response.data.status === "success") {
              this.setState({ seatNumber: response.data.seatNumber });
              amount = response.data.totalCost;
              time = response.data.minutes;
              if (userFacebookId !== null) {
                //
              } else if (this.state.canceled === false) {
                this.props.navigation.navigate("BindFacebook", {
                  count: 1,
                  email: "",
                });
              }
              this.setState({
                interval: true,
                resultShow: true,
                totalTimeSpent: time,
                totalAmountSpent: amount,
                timeType: false,
                currency: this.props.userData.currency,
              });
              this.setTimer();
            } else {
              this.setState({ totalTimeSpent: "", disable: false });
              if (userFacebookId !== null) {
                //
              } else if (this.state.canceled === false) {
                this.props.navigation.navigate("BindFacebook", {
                  count: 1,
                  email: "",
                });
              }
            }
          })
          .catch((error) => {
            alert('error');
            alert(error);
            this.setState({ totalTimeSpent: "", disable: false });
          });
    } else {
      Alert.alert("Error", translate('network_error'), [
        {
          text: "OK",
        }
      ]);

    }
  }
  setTimer() {
    if (this.state.interval === true) {
      this.Interval = setInterval(() => {
        this.getCost();
      }, 58000);
    }
  }
  stopTimerAPI() {
    HttpRequest.stopTimer(this.props.userData.token)
        .then((response) => {
          console.log(this.props.userData);
          if (response.data.status === "success") {
            // alert(response.data.message);
            this.setState({
              showModal: false,
              resultShow: false,
            });
            this.props.userData.seat_number = null;
            // this.props.sharedOffice(null);
            this.props.navigation.navigate("SharedOfficeReview", { timeCost: response.data.timeCost });
          } else if (response.data.status === "error") {
            Alert.alert("Error", response.data.message, [
              {
                text: "OK",
              }
            ]);
          } else {
            Alert.alert("Error", response.data.error, [
              {
                text: "OK",
              }
            ]);
          }
        })
        .catch((error) => {
          alert(error);
        });
  }

  getSkillsList() {
    HttpRequest.getSkills()
        .then((response) => {
          this.setState({
            skills: response.data,
          });
        })
        .catch(() => {});
  }

  async componentWillUnmount() {
    clearInterval(this.Interval);
    KeyEvent.removeKeyDownListener();
    this.setState({ showCost: false });
  }

  highlight = string => string;

  filter(teks) {
    if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
      const pattern = new RegExp(teks, "i");
      let filteredSkills = _.filter(this.state.skills, (item) => {
        if (item.cn.search(pattern) !== -1) {
          return item;
        }
      });
      filteredSkills = _.slice(filteredSkills, 0, 10);
      this.setState({ filteredSkills });
    } else {
      const pattern = new RegExp(teks, "i");
      let filteredSkills = _.filter(this.state.skills, (item) => {
        if (item.text.search(pattern) !== -1) {
          return item;
        }
      });
      filteredSkills = _.slice(filteredSkills, 0, 10);
      this.setState({ filteredSkills });
    }
  }

  selectSkill(item) {
    console.log(item);
    console.log(item);
    if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {

      let duplicate = false;
      this.state.selectedSkills.forEach((item2) => {
        if(item.value == item2.value) {
          duplicate = true;
        }
      });
      if(duplicate) {
        Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
          {
            text: "OK",
          }
        ]);
        return false;
      }

      if (item.cn !== this.state.duplicate) {
        this.setState({ duplicate: item.cn });
        const { selectedSkills } = this.state;
        selectedSkills.push(item);
        this.setState({
          selectedSkills,
          autocompleteText: "",
        });
      } else {
        Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
          {
            text: "OK",
          }
        ]);
      }
    } else {
      let duplicate = false;
      this.state.selectedSkills.forEach((item2) => {
        if(item.value == item2.value) {
          duplicate = true;
        }
      });
      if(duplicate) {
        Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
          {
            text: "OK",
          }
        ]);
        return false;
      }

      if (item.text !== this.state.duplicate) {
        this.setState({ duplicate: item.text });
        const { selectedSkills } = this.state;
        selectedSkills.push(item);
        this.setState({
          selectedSkills,
          autocompleteText: "",
        });
      } else {
        Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
          {
            text: "OK",
          }
        ]);
      }
    }
    console.log(this.state.selectedSkills);
  }

  removeSelectedSkills(item) {
    if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
      const { selectedSkills } = this.state;
      const filteredSelectedSkills = [];
      selectedSkills.forEach((item2) => {
        if (item2.cn !== item.cn) {
          filteredSelectedSkills.push(item2);
        }
      });
      this.setState({ selectedSkills: filteredSelectedSkills, duplicate: '' });
    } else {
      const { selectedSkills } = this.state;
      const filteredSelectedSkills = [];
      selectedSkills.forEach((item2) => {
        if (item2.text !== item.text) {
          filteredSelectedSkills.push(item2);
        }
      });
      this.setState({ selectedSkills: filteredSelectedSkills, duplicate: '' });
    }
  }

  showSearchResult() {
    Keyboard.dismiss();
    if (this.state.selectedSkills.length > 0) {
      if (this.state.selectedButton === 0) {
        this.props.navigation.navigate("JobsList", {
          selectedSkills: this.state.selectedSkills,
          locale: this.state.locale,
        });
      } else {
        this.props.navigation.navigate("SearchResult", {
          selectedSkills: this.state.selectedSkills,
          locale: this.state.locale,
          title:
              this.state.selectedButton === 1
                  ? "Find Experts"
                  : translate("find_cofounder"),
        });
      }
    } else {
      Alert.alert("Error", translate('select_skills'), [
        {
          text: "OK",
        }
      ]);
    }
  }

  closeTimer = (data) => {
    if (data === "yes") {
      this.stopTimerAPI();
    } else {
      this.setState({ showModal: false });
    }
  };

  onStopTimer = () => {
    this.setState({ showModal: true });
  };

  onQrDetected() {
    this.props.navigation.navigate("QRScanner", { changeSeat: false });
  }

  // bindAccount = () => {
  //   this.setState({ showBind: false }, () => {
  //     this.props.navigation.navigate("BindFacebook", {
  //       count: 1,
  //       email: "",
  //     });
  //   });
  // };
  //
  // closeAccountModal = () => {
  //   this.setState({ showBind: false }, () => {
  //     const data = this.props.userData;
  //     data.isBind = true;
  //     this.props.userLogin(data, this.props.userData.token);
  //   });
  // };

  searchProducts = (e) => {
    this.showSearchResult();
  };

  // renderModal = () => (
  //   <Modal
  //     animationType="fade"
  //     transparent={true}
  //     visible={this.state.showBind}
  //     onRequestClose={() => {
  //       this.setState({
  //         showBind: false,
  //       });
  //     }}
  //   >
  //     <View style={styles.verifyDialogStyle}>
  //       <View style={styles.verifyInfoBoxStyle}>
  //         <View
  //           style={{
  //             margin: 10,
  //             justifyContent: "space-between",
  //             alignItems: "center",
  //             height: 160,
  //           }}
  //         >
  //           <Image
  //             style={{
  //               width: 100,
  //               height: 100,
  //               marginTop: 10,
  //               resizeMode: "contain",
  //             }}
  //             source={require("../../images/share_office/icon_xenren.png")}
  //           />
  //           <Text
  //             style={{
  //               color: "#fff",
  //               fontSize: 19,
  //               fontFamily: FontStyle.Light,
  //               marginLeft: 5,
  //               marginRight: 5,
  //               color: "gray",
  //               textAlign: "center",
  //             }}
  //           >
  //             Bind Your Facebook Account
  //           </Text>
  //         </View>
  //         <View
  //           style={{
  //             width: "95%",
  //             height: 50,
  //             flexDirection: "row",
  //             justifyContent: "space-between",
  //             alignItems: "center",
  //             marginTop: 10,
  //           }}
  //         >
  //           <TouchableOpacity
  //             style={{
  //               width: "48%",
  //               height: 45,
  //               justifyContent: "center",
  //               alignItems: "center",
  //               borderRadius: 3,
  //               marginBottom: 10,
  //               backgroundColor: Config.primaryColor,
  //             }}
  //             onPress={() => this.bindAccount()}
  //           >
  //             <Text
  //               style={{
  //                 color: "white",
  //                 fontSize: 13,
  //                 fontFamily: FontStyle.Regular,
  //                 textAlign: "center",
  //               }}
  //             >
  //               Bind
  //             </Text>
  //           </TouchableOpacity>
  //           <TouchableOpacity
  //             style={{
  //               width: "48%",
  //               height: 45,
  //               justifyContent: "center",
  //               alignItems: "center",
  //               borderColor: Config.primaryColor,
  //               borderWidth: 1,
  //               borderRadius: 3,
  //               marginBottom: 10,
  //             }}
  //             onPress={() => this.closeAccountModal()}
  //           >
  //             <Text
  //               style={{
  //                 color: Config.primaryColor,
  //                 fontSize: 13,
  //                 fontFamily: FontStyle.Regular,
  //                 textAlign: "center",
  //               }}
  //             >
  //               Not Now
  //             </Text>
  //           </TouchableOpacity>
  //         </View>
  //       </View>
  //     </View>
  //   </Modal>
  // );

  renderSearchResult = () => {
    const searchResult = null;
    if (this.state.autocompleteText !== "") {
      return (
          <View
              style={[
                styles.searchBoxStyle,
                {
                  top: height * 0.6 - 130,
                  left: this.state.autocompletePosition.x,
                  width: this.state.autocompletePosition.width,
                  height: this.state.autocompletePosition.height,
                },
              ]}
          >
            {this.state.filteredSkills.map((item, index) => {
              const style = {
                flex: 1,
                width: "100%",
                flexDirection: "row",
                paddingHorizontal: 2,
                paddingVertical: 2,
                height: 35,
                alignItems: "center",
              };
              if (index !== 0) {
                style.borderTopColor = "#eee";
                style.borderTopWidth = 1;
              }
              return (
                  <TouchableOpacity
                      key={index}
                      style={style}
                      onPress={() => {
                        this.selectSkill(item);
                      }}
                  >
                    <Text ellipsizeMode="tail" numberOfLines={1}>
                      <Text style={{ fontFamily: FontStyle.Medium }}>
                        {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? this.highlight(item.cn) : this.highlight(item.text)}
                      </Text>
                      <Text>{"    |    "}</Text>
                      {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? this.highlight(item.desc_cn) : this.highlight(item.description)}
                    </Text>
                  </TouchableOpacity>
              );
            })}
          </View>
      );
    }
  };

  renderButtons = () =>
      this.state.buttons.map((item, index) => (
          <TouchableOpacity
              key={`btn_${index}`}
              style={styles.middleItemStyle}
              onPress={() => {
                this.setState({ selectedButton: index });
              }}
          >
            {this.state.selectedButton === index ? (
                <Image
                    source={require("../../images/share_office/selected_tab.png")}
                    style={{
                      height: 15,
                      width: 30,
                      tintColor: Config.primaryColor,
                    }}
                    resizeMode="stretch"
                />
            ) : null}
            <View
                style={{
                  width: "100%",
                  height: 90,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor:
                      this.state.selectedButton === index ? "#58af2a" : "#2e404e",
                  borderStyle: "solid",
                  borderWidth: 0,
                  borderColor: "#f2f2f2",
                  borderLeftWidth: index ? 1 : 0,
                  borderRightWidth: 0,
                }}
            >
              <Image
                  source={item.image}
                  style={{
                    height: 30,
                    width: 30,
                  }}
                  resizeMode="contain"
              />
              <Text
                  style={{
                    color: "#fff",
                    fontSize: 12,
                    marginTop: 10,
                    fontFamily: FontStyle.Regular,
                  }}
              >
                {item.label}
              </Text>
            </View>
          </TouchableOpacity>
      ));

  renderJobPosition() {
    return this.state.jobPosition.map((item, i) => (
        <TouchableOpacity
            key={item.id}
            style={
              this.state.activeTabIndex === i
                  ? {
                    backgroundColor: this.state.buttonColor === true ? Config.primaryColor : Config.backGroundColor,
                    flexDirection: 'row',
                    paddingHorizontal: 12,
                    paddingVertical: 5,
                    borderRadius: 15,
                    marginRight: 5,
                    marginTop: 2,
                  }
                  : styles.tabControlStyle
            }
            onPress={() => {
              this.setState({
                buttonColor: true,
                activeTabIndex: i,
                jobPositionId: item.id,
              });
            }}
        >
          <Text style={styles.tabControlText}>{item.name_en}</Text>
        </TouchableOpacity>
    ));
  }

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: "images",
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.fileSize > 2000000) {
        Alert.alert("Error", translate('select_skills'), [
          {
            text: "OK",
          }
        ]);
      } else {
        const source = {uri: response.uri};
        if (response.didCancel) {
        } else if (response.error) {
        } else {
          this.setState({
            tmpImg: response.uri,
            ImageShow: true,
          });
        }
      }
    });
  }
  saveInfo() {
    if (this.state.name !== '') {
      this.setState({ infoLoading: true });
      let imageObjectArr = {
        uri: this.state.tmpImg,
        name: 'photo_id.png' || 'photo_id.jpg' || 'photo_id.jpeg',
        type: 'image/png' || 'image/jpg' || 'image/jpeg',
      };
      let token = this.props.userData.token;
      let name = this.state.name;
      let profession = this.state.jobPositionId;
      let that = this;
      HttpRequest.uploadUserInfo(token, imageObjectArr, name, profession)
          .then((response) => {
            console.log('response in add user info popup');
            console.log(response);
            if (response.data.status === 'success') {
              that.props.userData.real_name = response.data.user.real_name;
              that.props.userData.img_avatar = response.data.user.img_avatar;
              that.setState({ infoModal: false, infoLoading: false });
              LocalData.setAppData({ userInfo: false });
            } else {
              alert(response.data.status);
              Alert.alert("Error", response.data.status, [
                {
                  text: "OK",
                }
              ]);
            }
          })
          .catch((error) => {
            console.log(error);
            this.setState({ infoLoading: false });
          });
    } else {
      Alert.alert("Error", "All field required", [
        {
          text: "OK",
        }
      ]);
    }
  }
  isCanceled() {
    this.setState({ infoModal: false });
    LocalData.setAppData({ userInfo: false });
  }

  render() {
    const selectedSkillsElement = this.state.selectedSkills.map((item, index) => (
        <View
            style={styles.tagInput}
            key={index}
        >
          <Text style={{ color: "#fff" }}>{this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.cn : item.text} </Text>
          <TouchableOpacity style={{ marginTop: 1 }} onPress={() => {
            this.removeSelectedSkills(item);
          }}>
            <Entypo name="cross" size={20} color={"#fff"}/></TouchableOpacity>
        </View>
    ));
    return (
        <View
            style={{
              flex: 1,
              backgroundColor: "rgba(87,176,41,0.9)",
              justifyContent: "space-between",
              alignItems: "center",
            }}
        >

          <Modal
              onRequestClose={() => this.setState({ infoModal: false })}
              animationType="fade"
              transparent={true}
              visible={this.state.infoModal}
          >
            <View style={styles.modalRootStyle}>
              <View style={styles.modalBoxStyle}>
                <TouchableOpacity
                    onPress={() => this.isCanceled() }
                    style={{ alignSelf: 'flex-end', margin: 10, height: 28, width: 28 }}
                >
                  <Feather name="x" size={28} color={Config.primaryColor}/>
                </TouchableOpacity>
                <View style={{ alignItems: 'center', bottom: width / 14 }}>
                  <Text style={styles.addInfoTextStyle}>{translate('add_info')}</Text>
                  <Text style={styles.textDescriptionStyle}>{translate('add_correct_info')}</Text>
                </View>
                <View style={styles.imageModal}>
                  <Image source={{ uri: this.state.tmpImg }}
                         style={styles.userImage}
                         resizeMode="cover"
                  />
                </View>
                <TouchableOpacity style={styles.uploadImageStyle}
                                  onPress={() => this.launchCamera() }
                >
                  <Text
                      style={{
                        color: Config.primaryColor,
                        fontFamily: FontStyle.Regular,
                        fontSize: 12,
                      }}>{translate('upload')}</Text>
                </TouchableOpacity>
                <View style={{ }}>
                  <Input
                      title={translate('your_name')}
                      placeholder={translate('write_name')}
                      value={this.state.name}
                      onChangeText={(name) => {
                        this.setState({ name });
                      }}
                  />
                </View>
                <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: 10,
                    }}>
                  <Text style={{ fontFamily: FontStyle.Regular }}>{translate('profession')}</Text>
                  <View style={styles.topTabWrapperStyle}>
                    {this.renderJobPosition()}
                  </View>
                </View>
                <TouchableOpacity style={styles.submitInfo}
                                  onPress={() => this.saveInfo() }
                >
                  {this.state.infoLoading === true && (
                      <ActivityIndicator color="#fff" />
                  )}
                  {this.state.infoLoading === false && (
                      <Text
                          style={{
                            color: Config.white,
                            fontFamily: FontStyle.Regular,
                          }}
                      >{translate('submit')}</Text>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <KeyboardAvoidingView
              style={{
                // height: Platform.OS === 'android' ? height - 85 : 1,
                flex: 1,
                width: "100%",
              }}
          >
            {this.state.showModal === true && (
                <TimerComponent
                    timerCallback={this.closeTimer}
                    value={true}
                    totalTime={this.state.totalTimeSpent}
                    timeType={this.state.timeType}
                    totalAmount={this.state.totalAmountSpent}
                    currency={this.state.currency === 1 ? "$" : "¥"}
                    seat={this.props.userData.seat_number}
                />
            )}
            <View
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  backgroundColor: "transparent",
                }}
            >
              <Image
                  source={require("../../images/share_office/new-background-image.png")}
                  style={{
                    height: height * 0.6,
                    width: "100%",
                    flexDirection: "column",
                    justifyContent: "flex-end",
                    alignItems: "flex-start",
                  }}
                  resizeMode="cover"
              />

              {/*<Image*/}
              {/*  source={require("../../images/share_office/icon_xenren_top.png")}*/}
              {/*  style={{*/}
              {/*    height: 80,*/}
              {/*    width: 80,*/}
              {/*    position: "absolute",*/}
              {/*    top: 70,*/}
              {/*  }}*/}
              {/*  resizeMode="cover"*/}
              {/*/>*/}
              <Text
                  style={orientation[this.state.orientation].logo}
              >
                XenRen
              </Text>
              <Text
                  style={orientation[this.state.orientation].subtitle}
                  // onPress={() => {
                  //   this.props.navigation.navigate("SharedOfficeFiltersMaps",{
                  //     data: {
                  //       name: "ahsan"
                  //     }
                  //   });
                  // }}
              >
                Coworker & Coworking space
              </Text>

              <View
                  style={orientation[this.state.orientation].searchBoxWrapperStyle}
                  ref="SearchInput"
                  onLayout={({ nativeEvent }) => {
                    this.refs.SearchInput.measure((x, y, width, height, pageX, pageY) => {
                      this.setState({
                        autocompletePosition: {
                          x: pageX,
                          y: pageY - 35,
                          width,
                        },
                      });
                    });
                  }}
              >
                <ScrollView
                    showsHorizontalScrollIndicator={true}
                    horizontal={true}
                    style={{
                      flexDirection: "row",
                      width: "80%",
                      height: "100%",
                      marginLeft: 5,
                      marginRight: 5,
                    }}
                >
                  {selectedSkillsElement}
                  <TextInput
                      style={{
                        flex: 1,
                        width: 300,
                        padding: 8,
                      }}
                      placeholder={translate("search")}
                      autoCapitalize="none"
                      autoCorrect={false}
                      underlineColorAndroid="transparent"
                      onSubmitEditing={this.searchProducts.bind(this)}
                      onFocus={() => {
                        this.setState({ showAutocomplete: true });
                      }}
                      onBlur={() => {
                        this.setState({ showAutocomplete: false });
                      }}
                      onChangeText={(autocompleteText) => {
                        this.setState({ autocompleteText });
                        this.filter(autocompleteText);
                      }}
                      value={this.state.autocompleteText}
                  />
                </ScrollView>

                <TouchableOpacity
                    style={styles.searchButtonStyle}
                    onPress={() => {
                      this.showSearchResult();
                    }}
                >
                  <Ionicons
                      name="ios-search"
                      color={Config.primaryColor}
                      size={20}
                  />
                </TouchableOpacity>
              </View>

              <View
                  style={{
                    position: "absolute",
                    top: height * 0.6 - 105,
                    width: "100%",
                    height: 105,
                    backgroundColor: "transparent",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
              >
                {this.renderButtons()}
              </View>

              {this.state.resultShow === false ? (
                  <TouchableOpacity
                      style={{
                        height: height * 0.4 - 65,
                        width: "100%",
                        top: 0,
                        opacity: 0.9,
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: "column",
                      }}
                      disabled={this.state.disable}
                      onPress={() => {
                        this.onQrDetected();
                      }}
                  >
                    <Image
                        source={require("../../images/other/home_search_background.jpg")}
                        style={{
                          height: "100%",
                          width: "100%",
                          position: "absolute",
                          top: 0,
                        }}
                        resizeMode="cover"
                    />
                    <Image
                        source={require("../../images/other/home_search_icon.png")}
                        style={{
                          height: 50,
                          width: 50,
                        }}
                        resizeMode="contain"
                    />
                    <Text
                        style={{
                          color: "#fff",
                          backgroundColor: "transparent",
                        }}
                    >
                      {translate("scan_qr_code")}
                    </Text>
                  </TouchableOpacity>
              ) : (
                  <View
                      style={{
                        height: height / 3.5,
                        width,
                        flexDirection: "column",
                        backgroundColor: "#5bbd28",
                        paddingVertical: 15,
                      }}
                  >
                    <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          paddingHorizontal: 15,
                          paddingVertical: 25,
                          justifyContent: "center",
                        }}
                    >
                      <View
                          style={{
                            flex: 0.3,
                            flexDirection: "column",
                            justifyContent: "center",
                          }}
                      >
                        <Text
                            style={{
                              color: "#fff",
                              textAlign: "center",
                              fontFamily: FontStyle.Bold,
                            }}
                        >
                          {translate("MONEY_SPENT")}
                        </Text>
                        <Text
                            style={{
                              color: "#fff",
                              textAlign: "center",
                              fontFamily: FontStyle.Bold,
                            }}
                        >
                          {this.state.totalAmountSpent}{" "}
                          {this.state.currency === 1 ? "USD" : "RMB"}
                        </Text>
                      </View>

                      <View
                          style={{
                            flex: 0.3,
                            flexDirection: "column",
                            justifyContent: "center",
                          }}
                      >
                        <View
                            style={{
                              width: 100,
                              height: 100,
                              borderRadius: 60,
                              backgroundColor: "white",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                        >
                          <Image
                              style={{
                                width: 60,
                                height: 70,
                              }}
                              resizeMode="contain"
                              source={require("../../images/select_seat/icon_greenSeatLarge.png")}
                          />
                          <View
                              style={{
                                position: "absolute",
                                top: 20,
                                justifyContent: "center",
                                alignItems: "center",
                                paddingLeft: 2,
                              }}
                          >
                            <Text
                                style={{
                                  textAlign: "center",
                                  color: Config.primaryColor,
                                  fontSize: 12,
                                  fontFamily: FontStyle.Medium,
                                  width: 35,
                                  paddingVertical: 8,
                                }}
                                ellipsizeMode="tail"
                                numberOfLines={1}
                            >
                              {this.state.seatNumber}
                            </Text>
                          </View>
                        </View>
                      </View>

                      <View
                          style={{
                            flex: 0.3,
                            flexDirection: "column",
                            justifyContent: "center",
                          }}
                      >
                        <Text
                            style={{
                              color: "#fff",
                              textAlign: "center",
                              fontFamily: FontStyle.Bold,
                            }}
                        >
                          {translate("TIME_SPENT")}
                        </Text>
                        {this.state.totalTimeSpent === "loading" ? (
                            <ActivityIndicator size="small" color="white" />
                        ) : (
                            <Text
                                style={{
                                  color: "#fff",
                                  textAlign: "center",
                                  fontFamily: FontStyle.Bold,
                                }}
                            >
                              {this.state.totalTimeSpent}{" "}
                              {/* {this.state.timeType === true ? 'HRS' : 'MIN'} */}
                            </Text>
                        )}
                      </View>
                    </View>

                    <View
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          paddingHorizontal: 15,
                          paddingVertical: 15,
                        }}
                    >
                      <TouchableOpacity
                          style={{
                            backgroundColor: "#fff",
                            flex: 0.45,
                            height: 45,
                            borderColor: "#fff",
                            borderRadius: 5,
                            justifyContent: "center",
                          }}
                          onPress={() => {
                            this.props.navigation.navigate("SeatStatus", {
                              changeSeat: true,
                              time: this.state.totalTimeSpent,
                              amount: this.state.totalAmountSpent,
                              duration: this.state.timeType === true ? "HRS" : "MIN",
                              currency: this.state.currency === 1 ? "USD" : "RMB",
                            });
                            // this.props.navigation.navigate('SelectSeat', { qr: this.props.qrCode, changeSeat: true });
                          }}
                      >
                        <Text
                            style={{
                              textAlign: "center",
                              color: Config.primaryColor,
                              fontFamily: FontStyle.Bold,
                            }}
                        >
                          {translate("CHANGE_SEAT")}
                        </Text>
                      </TouchableOpacity>
                      <View style={{ flex: 0.1 }} />
                      <TouchableOpacity
                          style={{
                            backgroundColor: "transparent",
                            flex: 0.45,
                            height: 45,
                            borderColor: "#fff",
                            borderRadius: 5,
                            borderWidth: 2,
                            justifyContent: "center",
                            marginLeft: 3,
                          }}
                          onPress={() => {
                            // this.setState({ showModal: true });
                            this.onStopTimer();
                          }}
                      >
                        <Text
                            style={{
                              textAlign: "center",
                              color: "#fff",
                              fontFamily: FontStyle.Bold,
                            }}
                        >
                          {translate("STOP")}{" "}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
              )}
            </View>

            {this.renderSearchResult()}
            {/* {this.renderModal()} */}
            <AddInfoPopUp />
          </KeyboardAvoidingView>
          {/* <View style={{position:'absolute', bottom:0,height:70, backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}> */}
          <FooterTabs navigation={this.props.navigation} currentIndex={2} />
          {/* </View> */}
        </View>
    );
  }
}

const orientation = {
  portrait: {
    logo: {
      color: "#fff",
      fontSize: 42,
      fontFamily: FontStyle.Bold,
      position: "absolute",
      top: height * 0.6 - 330,
    },
    subtitle: {
      color: "#fff",
      fontSize: 11,
      fontFamily: FontStyle.Medium,
      position: "absolute",
      top: height * 0.6 - 285,
    },
    searchBoxWrapperStyle: {
      borderRadius: 2,
      backgroundColor: "#fff",
      height: 45,
      width: "90%",
      flexDirection: "row",
      alignItems: "center",
      position: "absolute",
      top: height * 0.6 - 170,
    },
  },
  landscape: {
    logo: {
      color: "#fff",
      fontSize: 42,
      fontFamily: FontStyle.Bold,
      position: "absolute",
      top: 10
    },
    subtitle: {
      color: "#fff",
      fontSize: 11,
      fontFamily: FontStyle.Medium,
      position: "absolute",
      top: 57,
    },
    searchBoxWrapperStyle: {
      borderRadius: 2,
      backgroundColor: "#fff",
      height: 45,
      width: "90%",
      flexDirection: "row",
      alignItems: "center",
      position: "absolute",
      top: 72,
    },
  }
}
const styles = {
  rootStyle: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
  },

  topStyle: {
    flex: 3,
    flexDirection: "column",
    marginBottom: -15,
  },
  modalBoxStyle: {
    width: width - 60,
    height: width + 90,
    backgroundColor: Config.white,
    borderRadius: 5,
  },

  verifyDialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center",
  },

  verifyInfoBoxStyle: {
    width: "90%",
    height: 250,
    borderRadius: 10,
    backgroundColor: "#fff",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },

  middleStyle: {
    height: 95,
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "flex-end",
    borderBottomColor: "#fff",
    borderBottomWidth: 1,
  },

  middleItemStyle: {
    flex: 1,
    height: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
    flexDirection: "column",
  },

  bottomStyle: {
    flex: 2,
  },

  inputStyle: {
    flex: 1,
    height: 40,
    marginLeft: 10,
    fontSize: 12,
    fontFamily: FontStyle.Regular,
  },

  topContainerStyle: {
    height: "100%",
    width: "100%",
    position: "absolute",
    top: 0,
    backgroundColor: "rgba(52, 52, 52, 0.6)",
    justifyContent: "center",
    alignItems: "center",
  },

  // searchBoxWrapperStyle: {
  //   borderRadius: 2,
  //   backgroundColor: "#fff",
  //   height: 45,
  //   width: "90%",
  //   flexDirection: "row",
  //   alignItems: "center",
  //   position: "absolute",
  //   top: height * 0.6 - 170,
  // },

  searchBoxStyle: {
    position: "absolute",
    backgroundColor: "white",
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    flexDirection: "column",
    paddingVertical: 0,
    // ios
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0,
    },
    // android
    elevation: 1,
    paddingHorizontal: 10,
  },

  bottomContainerStyle: {
    height: "100%",
    width: "100%",
    position: "absolute",
    top: 0,
    justifyContent: "center",
    alignItems: "center",
  },

  tagInput: {
    height: 30,
    borderRadius: 5,
    flexDirection: "row",
    backgroundColor: Config.primaryColor,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 10,
    marginLeft: 5,
    marginTop: 5,
  },

  searchButtonStyle: {
    paddingRight: 5,
    backgroundColor: "transparent",
    marginRight: 6,
  },
  topTabWrapperStyle: {
    flexDirection: 'row',
    marginLeft: 30,
    flexWrap: 'wrap',
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  tabControlStyle: {
    backgroundColor: Config.backGroundColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
    marginTop: 2,
  },
  tabControlText: {
    fontSize: 8,
    fontFamily: FontStyle.Light,
  },
  addInfoTextStyle: {
    fontFamily: FontStyle.Medium,
    fontSize: 20,
    color: '#000',
  },
  modalRootStyle: {
    flex: 1,
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    alignItems: 'center',
    justifyContent: "center",
  },
  textDescriptionStyle: {
    fontFamily: FontStyle.Light,
    fontSize: 12,
    color: Config.textColor,
  },
  imageModal: {
    flexDirection: "column",
    height: 100,
    width: 100,
    paddingVertical: 8,
    alignSelf: "center",
    marginBottom: 12,
    marginLeft: 8,
    borderRadius: 100,
    bottom: 8,
  },
  userImage: {
    height: 97,
    width: 97,
    bottom: 8.5,
    borderRadius: width / 8.5,
  },
  uploadImageStyle: {
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    width: width / 4,
    height: 23,
    left: width / 3.4,
    borderRadius: 10,
    borderColor: Config.primaryColor,
    bottom: 8,
  },
  submitInfo: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    width: width / 1.3,
    left: 12,
    marginTop: 13,
    borderRadius: 3,
    backgroundColor: Config.primaryColor,
  },
  dialogStyle2: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogBoxStyle2: {
    width: 300,
    height: 280,
    backgroundColor: '#fff',
    borderRadius: 10,
    // alignItems: 'center',
    // justifyContent: 'center',
  },


};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
    sharedOfficeData: state.auth.sharedOffice,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
        dispatch({
          type: "set_root",
          root,
        }),
    stopTimer: id => dispatch(actions.sharedOfficeStopTimer(id)),
    userLogin: (userData, token) =>
        dispatch({
          type: "LOGIN_SUCCESS",
          data: {
            ...userData,
            token,
          },
        }),
    sharedOffice: data => dispatch({
      type: 'SHARED_OFFICE',
      data,
    }),
  };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(DashBoard);
