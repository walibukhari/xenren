import React from 'react';
import { 
    TouchableOpacity,
    Platform,
    View,
    Image,
    Text,
    StyleSheet,
    Dimensions
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import GlowEffectComponent from './GlowEffectComponent';

import { translate } from '../../i18n';
import Config from '../../Config';
const { width, height } = Dimensions.get('window');
const containerWidth = width * 0.55; // will get 70% of container width

export default class ProfileGlowModal extends React.Component { 
    constructor(props) { 
        super(props);
    }

    renderGradientBox() { 
        if (Platform.OS == 'ios') { 
            
        }
    }
    render() { 
        let { userInfo } = this.props;
        console.log(userInfo)
        let gradientBox = null;
        let contentComponents = (
            <React.Fragment>
                <View style={{
                    flex: 1,
                    marginRight: 3,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                    <Image style={{
                        flexBasis: 40,
                        height: 40,
                        width: 40,
                        alignSelf: 'center',
                        borderRadius: 20,
                    }} source={{ uri: userInfo.img_avatar }} />
                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexGrow: 3
                    }}>
                        <Text style={{
                            color: Config.primaryColor,
                            fontSize: 11,
                            fontWeight: '300',
                            fontFamily: 'Montserrat',
                            textAlign: 'left',
                        }}>{userInfo.title}</Text>
                        <Text style={{
                            color: 'black',
                            fontSize: 12,
                            fontWeight: '500',
                            fontFamily: 'Montserrat-Light',
                            textAlign: 'left',
                        }}>{userInfo.name}</Text>
                        <Text style={{
                            color: 'gray',
                            fontSize: 10,
                            fontWeight: '100',
                            fontFamily: 'Montserrat-Light',
                            textAlign: 'left',
                        }}>{translate('has_checked_in')}</Text>
                    </View>
                </View>

                <View style={{
                    flexBasis: 30,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    width: '30%',
                    paddingRight: 10,
                    backgroundColor: 'transparent',
                }}
                >
                    <SimpleLineIcons size={20} name="arrow-right" color='#57af2a' />
                </View>
            </React.Fragment>);
        if (Platform.OS == 'ios') {
            gradientBox = (
                <TouchableOpacity style={styles.checkProfileDialogStyle} onPress={this.props.onCheckUserProfileTapped}>
                    <View style={styles.checkProfileBoxStyle}>
                       { contentComponents }
                    </View>
                </TouchableOpacity>);
        }
        else { 
            gradientBox = (
                <GlowEffectComponent
                    wrapperStyle={[{
                        backgroundColor: '#fff',
                        width: containerWidth,
                        borderRadius: 50,
                    }, styles.checkProfileBoxStyle]}
                    width={containerWidth}
                    height={50}>
                    {contentComponents}
                </GlowEffectComponent>
            )
        }
        return (
            <React.Fragment>
                <View style={styles.checkProfileDialogStyle}>
                    {gradientBox}
                </View>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    checkProfileDialogStyle: {
        top: Platform.OS === 'ios' ? height / 1.21 : height / 1.32,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkProfileBoxStyle: {
        width: containerWidth,
        padding: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        ...Platform.select({
            ios: {
                height: 50,
                backgroundColor: '#fff',
                borderRadius: 30,
                borderWidth: 2,
                borderColor: Config.primaryColor,
                elevation: 5,
                shadowColor: Config.primaryColor,
                shadowOpacity: 0.8,
                shadowRadius: 2,
                shadowOffset: {
                    height: 1,
                    width: 1,
                },
                // android: {}
            }
        })
    },
})