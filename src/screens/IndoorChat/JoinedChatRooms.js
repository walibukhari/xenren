import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Text, View, Image, TouchableOpacity, FlatList, ActivityIndicator, Alert, Platform, Dimensions} from 'react-native';
import Config from '../../Config';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import HttpRequest from '../../components/HttpRequest';
import { NavigationActions } from 'react-navigation';
import FontStyle from '../../constants/FontStyle';
import { translate } from '../../i18n';

class JoinedChatRooms extends Component {
  static navigationOptions = ({ navigation }) => {
    const data = navigation.state.params.sharedOfficeData;
    return {
      headerTitle: (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
            borderRadius: 50,
            borderWidth: 2,
            borderColor: Config.primaryColor,
            shadowColor: '#000',
             shadowOffset: {
               width: 0,
               height: 1,
              },
            shadowOpacity: 0.1,
            shadowRadius: 2,
            bottom: Platform.OS === 'ios' ? 4 : 0,
            marginLeft: Platform.OS === 'ios' ? 0 : Dimensions.get('window').width/ 5
          }} >
            <Text style={{ fontSize: 14, color: '#000', fontFamily: FontStyle.Medium }} numberOfLines={1} ellipsizeMode='tail'>
              {data.locale === 'cn' || data.locale === 'tw' || data.locale === 'zh' ? data.office.chinese_version === 'true' ? data.office.shared_office_languages.office_name_cn : data.office.office_name : data.office.english_version ? data.office.english_version === 'false' ? data.office.shared_office_languages.office_name_cn : data.office.office_name.substring(0, 10) : data.office.english_version === 'false' || data.office.english_version === null ? data.office.shared_office_languages.office_name_cn : data.office.version_english === 'true' ? data.office.office_name.substring(0, 10) : data.office && data.office.shared_office_language_data && data.office.shared_office_language_data.office_name_cn ? data.office.shared_office_language_data.office_name_cn : 'n/a'}
            </Text>
            <Image source={require('../../../images/other/icon_location.png')} style={{
 marginLeft: 3, tintColor: Config.primaryColor, width: 22, height: 22, paddingRight: 5,
}} />
          </View>
      ),
      headerLeft: <MaterialIcons style={{ marginLeft: 20 }}
      size={20}
      onPress={() => navigation.goBack()}
      name="arrow-back"
    />,
      headerTitleStyle: { textAlign: 'center', alignSelf: 'center' },
      headerRight: (<TouchableOpacity style={{ width: 30 }}>
      </TouchableOpacity>),

    };
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      groupsData: [],
    };
  }

  componentDidMount() {
    const access_token = this.props.userData.token;
    this.getJoinedGroups(access_token);
  }

  getJoinedGroups = (access_token) => {
    HttpRequest.getJoinedGroupsList(access_token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          this.setState({
            groupsData: result.data,
            isLoading: false,
          });
        } else {
            Alert.alert("Error", response.data.message, [
                {
                    text: "OK",
                }
            ]);
          this.setState({ isLoading: false });
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({
          isLoading: false,
        });
      });
  };

  onChangeChatRoom = (item) => {
    const resetAction = NavigationActions.reset({
      index: 1,
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Dashboard' }),
        NavigationActions.navigate({ routeName: 'IndoorChat', params: { sharedOfficeData: item } }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  };

  onRefresh() {
    this.setState({ isLoading: true });
    this.getJoinedGroups(this.props.userData.token);
  }

  renderSeparator = () => (
      <View
        style={{
          height: 10,
          width: '100%',
          backgroundColor: '#ecf0f1',
          marginLeft: '0%',
        }}
      />
  );

  renderRow = item => (
      <TouchableOpacity key={item.id} style={{
 borderRadius: 3, backgroundColor: 'white', width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
}}
        onPress={() => { this.onChangeChatRoom(item); }}>
        <Image style={{ height: '100%', width: '35%', resizeMode: 'cover' }} source={{ uri: `${Config.webUrl}${item.office.image}` }} />
        <View style={{
 width: '63%', justifyContent: 'center', alignItems: 'flex-start', marginTop: 5, marginBottom: 5, paddingRight: 7, paddingLeft: 5,
}}>
          <Text style={{ fontSize: 15, fontFamily: FontStyle.Bold, color: '#3D3D3D' }}>
            {item.office.office_name}
          </Text>

          <View style={{
 width: '100%', height: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
}}>
            <Text style={{
 fontSize: 11, fontFamily: FontStyle.Medium, marginRight: 12, color: '#8D8D8D',
}}>
                {translate('user_using_the_chat')}
                  </Text>
            <View style={{
 width: 35, height: '95%', justifyContent: 'center', alignItems: 'center', borderRadius: 2, backgroundColor: Config.primaryColor,
}}>
              <Text style={{ fontSize: 11, fontFamily: FontStyle.Medium, color: 'white' }}>
                {item.office.total_members ? item.office.total_members : ''}</Text>
            </View>
          </View>

          <View style={{
 width: '100%', height: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 1,
}}>
            <Text style={{
 fontSize: 11, fontFamily: FontStyle.Regular, marginRight: 12, color: '#8D8D8D',
}}>{translate('user_check_in')}</Text>
            <View style={{
 width: 35, height: '95%', justifyContent: 'center', alignItems: 'center', borderRadius: 2, backgroundColor: Config.primaryColor,
}}>
              <Text style={{ fontSize: 11, fontFamily: FontStyle.Regular, color: 'white' }}>
                  {item.office.checked_in_users ? item.office.checked_in_users : ''}
              </Text>
            </View>
          </View>

          <View style={{
 width: '100%', height: 1, backgroundColor: '#F2F2F2', marginTop: 5, marginBottom: 0,
}} />

          <Text style={{ fontSize: 14, fontFamily: FontStyle.Bold, color: '#5C5C5C' }}>{translate('prices')}</Text>

          <View style={{
 width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
}}>
            <Text style={{ fontSize: 12, fontFamily: FontStyle.Medium, color: '#8D8D8D' }}>
                {translate('seat')}{' '}<Text style={{ fontSize: 12, fontFamily: FontStyle.Medium, color: Config.primaryColor }}>
                {item.office.seat_price ? item.office.seat_price.month_price : ''}$</Text></Text>
            <Text style={{ fontSize: 12, fontFamily: FontStyle.Medium, color: '#8D8D8D' }}>
                {translate('meeting_room')}{' '}<Text style={{ fontSize: 12, fontFamily: FontStyle.Medium, color: Config.primaryColor }}>
                $
                  </Text> </Text>
          </View>
        </View>
      </TouchableOpacity>
  );

  render() {
    if (this.state.isLoading === true) {
      return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator />
      </View>);
    }
    return (
        <View style={styles.rootStyle}>
          <FlatList style={{ marginTop: 10, marginBottom: 10 }}
            showsVerticalScrollIndicator={false}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            data={this.state.groupsData}
            renderItem={({ item }) => this.renderRow(item)}
            keyExtractor={item => item.subtitle}
            ItemSeparatorComponent={this.renderSeparator}
          />
        </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
  },
  boxInsideStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  greyedTextDescriptionStyle: {
    color: '#c3c3c3',
    fontSize: 18,
    fontFamily: FontStyle.Light,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,

)(JoinedChatRooms);
