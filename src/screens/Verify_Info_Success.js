/* eslint-disable spaced-comment */
/* eslint-disable padded-blocks */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable global-require */
/* eslint-disable space-before-blocks */
/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Text,
    View,
    Image,
    TouchableOpacity, Modal,
} from 'react-native';
import {NavigationActions} from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import {translate} from '../i18n';
import FontStyle from '../constants/FontStyle';
import FontAwesomeIcons from "react-native-vector-icons/FontAwesome5";

class Verify_Info_Success extends Component {

    constructor(props) {
        super(props);
        this.root = this.props.component.root;

        this.goToProfile = this.goToProfile.bind(this);
    }

    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            title: null,
            headerStyle: {
                backgroundColor: '#FFF',
                borderBottomColor: '#FFF'
            },
            headerTintColor: Config.topNavigation.headerIconColor,
            headerTitleStyle:
                {
                    color: Config.topNavigation.headerTextColor,
                    alignSelf: 'center',
                    fontFamily: FontStyle.Regular,
                    width: '100%',
                },
            headerLeft:
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack();
                    }}
                    style={{
                        marginLeft: 10,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        alignItems: 'center',
                        padding: 5,
                    }}>
                    <FontAwesomeIcons
                        size={16}
                        name="chevron-left"
                        color={Config.topNavigation.headerIconColor}
                    />
                    <Text style={{
                        fontWeight: 'bold',
                        color: Config.topNavigation.headerIconColor,
                        paddingHorizontal: 5
                    }}>{translate('back')}</Text>
                </TouchableOpacity>,
        };
    };

    goToProfile() {
        this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: 'Dashboard',
                }),
            ],
        }));
    }

    render() {
        return (
            <View style={styles.rootStyle}>
                {/*<View style={styles.rootStyle}>*/}
                {/*    <Image*/}
                {/*        style={{width: 80, height: 80}}*/}
                {/*        source={require('../../images/other/home_search_background.jpg')}*/}
                {/*    />*/}
                {/*    <Text style={styles.VerifyInfoStyle}>*/}
                {/*        {' '}*/}
                {/*        {translate('verify_your_info')}{' '}*/}
                {/*    </Text>*/}
                {/*    <Text style={styles.verifyTextStyle}>*/}
                {/*        {translate('info_verified_successfully')}*/}
                {/*    </Text>*/}
                {/*</View>*/}
                {/*<TouchableOpacity*/}
                {/*    style={styles.okButtonStyle}*/}
                {/*    onPress={() => this.goToProfile()}*/}
                {/*>*/}
                {/*    <View>*/}
                {/*        <Text style={{color: 'white', fontSize: 16}}>*/}
                {/*            {' '}*/}
                {/*            {translate('ok')}{' '}*/}
                {/*        </Text>*/}
                {/*    </View>*/}
                {/*</TouchableOpacity>*/}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={true}
                    onRequestClose={() => console.log('Modal Closed')}>
                    <View style={styles.verifyDialogStyle}>
                        <View style={styles.verifyInfoBoxStyle}>
                            <Image style={{
                                width: 80, height: 110, marginTop: 15, resizeMode: 'contain',
                            }} source={require('../../images/notification/new-icons/successful.png')}/>
                            <Text style={{
                                fontSize: 24,
                                fontFamily: 'Montserrat-Bold',
                                marginLeft: 25,
                                marginRight: 25,
                                marginBottom: 15,
                                color: Config.primaryColor,
                                textAlign: 'center',
                            }}>
                                Verification Successful!
                            </Text>

                            <Text style={{
                                fontSize: 14,
                                fontWeight: '100',
                                fontFamily: 'Montserrat-Light',
                                marginLeft: 25,
                                marginRight: 25,
                                marginBottom: 25,
                                color: 'black',
                                textAlign: 'center',
                            }}>
                                Congratulations on your
                                appointment successfully!
                            </Text>

                            <View style={{
                                flexDirection: 'row',
                                height: 40,
                                marginVertical: 15,
                                marginHorizontal: 30
                            }}>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: 25,
                                    backgroundColor: '#36404f',
                                }} onPress={() => {
                                    // Do Anything
                                }}>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: 'bold',
                                        color: '#FFF'
                                    }}>{translate('OK')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        ); //return
    } //render
} //VerifyInfo

const styles = {
    rootStyle: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        paddingHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
    },

    okButtonStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '10%',
        backgroundColor: Config.primaryColor,
    },

    verifyTextStyle: {
        textAlign: 'center',
        paddingTop: 10,
        fontSize: 20,
        justifyContent: 'center',
    },

    VerifyInfoStyle: {
        fontSize: 32,
        fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: 'center',
        paddingTop: 10,
    },

    verifyDialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialogBoxStyle: {
        width: '70%',
        height: 'auto',
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    verifyInfoBoxStyle: {
        width: '70%',
        height: 'auto',
        borderRadius: 10,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
};

const mapStateToProps = state => ({
    component: state.component,
});

const mapDispatchToProps = dispatch => ({
    setRoot: root =>
        dispatch({
            type: 'set_root',
            root,
        }),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Verify_Info_Success);
