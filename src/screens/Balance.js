/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */
/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Config from '../Config';
import { translate } from '../i18n';

class Balance extends Component {
  constructor(props) {
    super(props);
    ini = this;
  }

  static navigationOptions = () => {
    return {
      headerTintColor: 'green',
      headerRight: (<TouchableOpacity onPress={() => {
        ini.handlePaymentRecord();
      }}>

        <Image
          source={require('../../images/other/icon_balance.png')}
          style={{
            width: 30,
            height: 30,
            resizeMode: 'contain',
            marginRight: 10,
            tintColor: Config.primaryColor,
          }}/>

      </TouchableOpacity>),
      headerTitleStyle: {
        color: 'green',
        fontWeight: 'bold'
      },
    };
  };


  handlePaymentRecord() {
    this.props.navigation.navigate('PaymentRecord');
  }


  render() {
    return (

      <View style={styles.rootStyle}>
        <View style={styles.mainViewportStyle}>
          <View style={styles.itemDividerStyle}>
            <View style={styles.itemStyle}>
              <Image source={require('../../images/other/icon_topup.png')} style={{
                tintColor: Config.primaryColor,
                width: 90,
                height: 90,
              }}/>
              <Text style={{
                color: Config.primaryColor,
                fontSize: 25,
                marginTop: 15,
                fontWeight: 'bold',
              }}>{translate('TOP_UP')}</Text>
            </View>
            <View style={styles.itemStyle}>
              <Image source={require('../../images/other/icon_withdraw.png')} style={{
                tintColor: Config.primaryColor,
                width: 90,
                height: 90,
              }}/>
              <Text style={{
                color: Config.primaryColor,
                fontSize: 25,
                marginTop: 15,
                fontWeight: 'bold',
              }}>{translate('WITHDRAW')}</Text>
            </View>
          </View>
          <View style={styles.itemDividerStyle}>
            <View style={styles.itemStyle}>
              <Text style={{
                color: '#9e9e9e',
                fontSize: 28,
                fontWeight: 'bold',
              }}>{translate('balance')}</Text>
              <View style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
                <Text style={{
                  color: Config.primaryColor,
                  fontSize: 38,
                  marginRight: 10,
                  fontWeight: 'bold',
                }}>3105</Text>
                <Image source={require('../../images/other/icon_yen.png')} style={{
                  width: 40,
                  height: 40,
                  tintColor: Config.primaryColor,
                  marginTop: 7,
                }}/>
              </View>
            </View>
            <View style={styles.itemStyle}>
              <Text style={{
                color: '#9e9e9e',
                fontSize: 28,
                fontWeight: 'bold',
              }}>{translate('XEN_POINTS')}</Text>
              <View style={{
                flexDirection: 'row',
                marginTop: 10,
              }}>
                <Text style={{
                  color: Config.primaryColor,
                  fontSize: 38,
                  marginRight: 10,
                  fontWeight: 'bold',
                }}>30 XEN</Text>
              </View>
            </View>
          </View>
          <View style={{ alignItems: 'center' }}>
            <View style={{
              flexDirection: 'row',
              marginTop: 10,
            }}>
              <Text style={{
                color: Config.primaryColor,
                fontSize: 38,
                marginRight: 10,
                fontWeight: 'bold',
              }}>{translate('WITHDRAW')} 90</Text>
              <Image source={require('../../images/other/icon_yen.png')} style={{
                width: 40,
                height: 40,
                tintColor: Config.primaryColor,
                marginTop: 7,
              }}/>
            </View>
          </View>

        </View>
      </View>

    );// return
  }// render
}// Withdraw


function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  mainViewportStyle: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '75%',
    paddingVertical: 30,
  },

  itemDividerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 40,
  },

  itemStyle: {
    flexDirection: 'column',
  },
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Balance);
