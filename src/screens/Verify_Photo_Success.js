/* eslint-disable no-undef */
/* eslint-disable global-require */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable class-methods-use-this */
/* eslint-disable spaced-comment */
/* eslint-disable indent */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ToastAndroid,
  ListView,
  TextInput,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';

class Verify_Photo_Success extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'Verify',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.root = this.props.component.root;
  }


  render() {
    return (

      <View style={styles.rootStyle}>
        <View style={styles.rootStyle}>
          <Image style={{
            width: 80,
            height: 80
          }} source={require('../../images/other/home_search_background.jpg')}/>
          <Text style={styles.VerifyInfoStyle}> {translate('verify_successful')} </Text>
          <Text style={styles.verifyTextStyle}> {translate('photo_verified')}</Text>
        </View>
        <TouchableOpacity style={styles.okButtonStyle}>
          <View>
            <Text style={{
              color: 'white',
              fontSize: 16
            }}> {translate('OK')} </Text>
          </View>
        </TouchableOpacity>
      </View>

    );//return
  }//render
}//VerifyInfo

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },

  okButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '10%',
    backgroundColor: Config.primaryColor,
  },

  verifyTextStyle: {
    textAlign: 'center',
    paddingTop: 10,
    fontSize: 20,
    justifyContent: 'center',
  },

  VerifyInfoStyle: {
    fontSize: 32,
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center',
    paddingTop: 10,
  },

};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Verify_Photo_Success);
