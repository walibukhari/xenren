/* eslint-disable no-console */
/* eslint-disable global-require */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable comma-dangle */
/* eslint-disable object-shorthand */
/* eslint-disable function-paren-newline */
/* eslint-disable prefer-destructuring */
/* eslint-disable prefer-template */
/* eslint-disable semi */
/* eslint-disable prefer-const */
/* eslint-disable import/first */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable quotes */
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  Image,
  Dimensions,
  Alert,
  Platform
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";
import Pushy from 'pushy-react-native';
import { NavigationActions } from "react-navigation";
import Config from "../Config";
import { translate } from "../i18n";
import HttpRequest from "../components/HttpRequest";
import LocalData from "../components/LocalData";
import VerificationTopup from "./verifyId/VerificationTopup";

const { height } = Dimensions.get('window');
class SignUp extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    console.log('SignUp.js');
    super(props);

    this.state = {
      name: '',
      last_name:'',
      email: '',
      password: '',
      nameFocus: false,
      emailFocus: false,
      passwordFocus: false,
      isLoading: false,
      isFbLoading: false,
      isShowSuccess: false,
      isShowError: false,
      successMessage: 'Success',
      errorMessage: 'Error',
      token: '',
    };

    this.root = this.props.component.root;
    this.registerDevice();
  }

  gotoForgotPassword() {
    this.props.navigation.navigate("ForgotPassword");
  }

  gotoSignIn() {
    this.props.navigation.navigate("Login");
  }

  gotoPrivacyPolicy() {
    this.props.navigation.navigate("PrivacyPolicy");
  }

  gotoTermOfService() {
    this.props.navigation.navigate("TermOfService");
  }

  gotoMenu() {
    this.props.navigation.gotoMenu();
  }
  ValidateFields(message) {
    // Show Dialog
    this.setState({
      isLoading: false,
      isShowError: true,
      errorMessage: message
    });
    // Hide Dialog
    setTimeout(() => {
      this.setState({
        isShowError: false
      });
    }, 3000);
  }
  signUp() {
    let message;
    if (this.state.isLoading === false) {
      this.setState({ isLoading: true });
      console.log('token---<<', this.state.token);
      if(this.state.name === '') {
        message = 'first name required';
        this.ValidateFields(message)
      } else if (this.state.last_name === '') {
        message = 'last name required';
        this.ValidateFields(message)
      } else if(this.state.email === '') {
        message = 'email required';
        this.ValidateFields(message)
      } else if(this.state.password === '') {
        message = 'password required';
        this.ValidateFields(message)
      } else {
        HttpRequest.register(
            this.state.name,
            this.state.last_name,
            this.state.email,
            this.state.password,
            this.state.token
        )
            .then((response) => {
              this.setState({isLoading: false});
              const result = response.data;
              console.log('result--->', result);
              if (result.status === "success") {
                // Show Dialog
                this.setState({
                  isLoading: false,
                  isShowSuccess: true,
                  successMessage: result.message
                });
                // this.props.userLogin(result.user, result.access_token);
                // LocalData.setUserData({ token: result.access_token, ...result.user });

                // Change Page
                setTimeout(() => {
                  this.setState({
                    isShowSuccess: false
                  });
                  // this.props.navigation.navigate('BindFacebook', { count: 1, email: '' });
                  this.props.navigation.navigate("GuestRegister", {
                    key: result.key,
                    email: this.state.email,
                  });
                }, 2000);
              } else {
                // Show Dialog
                this.setState({
                  isLoading: false,
                  isShowError: true
                  // errorMessage: result.message,
                });

                // Hide Dialog
                setTimeout(() => {
                  this.setState({
                    isShowError: false
                  });
                }, 2000);
              }

              this.setState({isLoading: false});
            })
            .catch((error) => {
              console.log('error');
              console.log(JSON.stringify(error.response.data.status));
              let message = "Cannot Signup, Please Try Again.";
              if (error.response != null && error.response.data != null) {
                message = error.response.data.message;
              }
              // Show Dialog
              this.setState({
                isLoading: false,
                isShowError: true,
                errorMessage: message
              });

              // Hide Dialog
              setTimeout(() => {
                this.setState({
                  isShowError: false
                });
              }, 3000);
            });
      }
    }
  }

  registerDevice = () => {
    Pushy.register().then(async (deviceToken) => {
      this.setState({ token: deviceToken });
    }).catch((err) => {
      // Handle registration errors
      console.log(err);
    });
  };

  fbLogin = () => {
    LoginManager.logInWithReadPermissions(["public_profile", "email"])
      .then((result) => {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let accessToken = data.accessToken;

            const infoRequest = new GraphRequest(
              '/me?fields=id,name,email',
              null,
              (err, res) => {
                if (err) {
                  Alert.alert("Error", "Error fetching data: " + err.toString(), [
                    {
                      text: "OK",
                    }
                  ]);
                } else {
                  this.setState({ isFbLoading: true });
                  this.registerUser(res);
                  // this.registerUser(res);
                  // this.props.navigation.navigate("BindFacebook", {
                  //  data: res,
                  //  count: 3
                  // });
                }
              }
            );

            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      })
      .catch((error) => {});
    LoginManager.logOut();
  };

  registerUser = (data) => {
    HttpRequest.registerWithFacebook(data.name, data.email, data.id)
      .then((response) => {
        const result = response.data;
        if (result.status === "success") {
          LocalData.setUserData({ token: result.access_token, ...result.user });
          this.props.userLogin(result.user, result.access_token);
          this.setState({ isFbLoading: false });
          if (result.user.hint !== null) {
            this.props.navigation.dispatch(NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'Dashboard',
                }),
              ],
            }));
          } else {
            this.props.navigation.navigate('BindFacebook', { data: data, count: 3 });
          }
        }
      })
      .catch((error) => {
        alert(error);
      })
  };

  render() {
    return (
      <View
        style={styles.rootStyle}
        //  contentContainerStyle={{justifyContent:'center', alignItems:'center'}}
        keyboardShouldPersistTaps="always"
        horizontal={false}
        showsVerticalScrollIndicator={false}
      >
        <VerificationTopup
          showModal={this.state.isFbLoading}
          showOKButton={false}
          type={"loading"}
          message={"Please Wait"}
          closeModal={() => {
            this.setState({ isFbLoading: false })
          }}
        />
        <View style={styles.headerStyle}>
          <View style={styles.headerLineStyle} />
          <Text style={styles.headerTextStyle}>{translate("sign_up")}</Text>
          <View style={styles.headerLineStyle} />
        </View>
        <View style={{ height: 50 }} />
        <View style={styles.formInputStyle}>
          <View
            style={[
              styles.inputWrapperStyle,
              this.state.nameFocus
                ? { borderBottomColor: Config.primaryColor }
                : {}
            ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-person" size={25} color="#717171"/>
              : < Ionicons name = "ios-person" size={25} color="#717171" style={{ marginBottom: -10 }} />
            }
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('first_name')}
              autoCapitalize='none'
              autoCorrect={false}
              underlineColorAndroid='transparent'
              onFocus={() => { this.setState({ nameFocus: true }); }}
              onBlur={() => { this.setState({ nameFocus: false }); }}
              onChangeText={(name) => this.setState({ name })}
              value={this.state.name}
            />
          </View>
          <View
              style={[
                styles.inputWrapperStyle,
                this.state.nameFocus
                    ? { borderBottomColor: Config.primaryColor }
                    : {}
              ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-person" size={25} color="#717171"/>
                : < Ionicons name = "ios-person" size={25} color="#717171" style={{ marginBottom: -10 }} />
            }
            <TextInput
                style={styles.inputStyle}
                placeholder={translate('last_name')}
                autoCapitalize='none'
                autoCorrect={false}
                underlineColorAndroid='transparent'
                onFocus={() => { this.setState({ nameFocus: true }); }}
                onBlur={() => { this.setState({ nameFocus: false }); }}
                onChangeText={(last_name) => this.setState({ last_name })}
                value={this.state.last_name}
            />
          </View>
          <View
            style={[
              styles.inputWrapperStyle,
              this.state.emailFocus
                ? { borderBottomColor: Config.primaryColor }
                : {}
            ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-mail" size={25} color="#717171"/>
              : < Ionicons name = "ios-mail" size={25} color="#717171" style={{ marginBottom: -10 }} />
            }
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('email')}
              autoCapitalize='none'
              autoCorrect={false}
              underlineColorAndroid='transparent'
              keyboardType="email-address"
              onFocus={() => { this.setState({ emailFocus: true }); }}
              onBlur={() => { this.setState({ emailFocus: false }); }}
              onChangeText={(email) => this.setState({ email })}
              value={this.state.email}
            />
          </View>
          <View
            style={[
              styles.inputWrapperStyle,
              this.state.passwordFocus
                ? { borderBottomColor: Config.primaryColor }
                : {}
            ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-lock" size={25} color="#717171"/>
              : < Ionicons name = "ios-lock" size={25} color="#717171" style={{ marginBottom: -10 }} />
            }
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('password')}
              autoCapitalize='none'
              autoCorrect={false}
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onFocus={() => { this.setState({ passwordFocus: true }); }}
              onBlur={() => { this.setState({ passwordFocus: false }); }}
              onChangeText={(password) => this.setState({ password })}
              value={this.state.password}
            />
          </View>
          <View style={{ height: 20 }} />
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={styles.signInButtonStyle}
              onPress={() => {
                this.signUp();
              }}
            >
              {this.state.isLoading === true && (
                <ActivityIndicator color="#fff" />
              )}
              {this.state.isLoading === false && (
                <Text style={styles.signInTextStyle}>
                  {translate("sign_up")}
                </Text>
              )}
            </TouchableOpacity>
          </View>

          <View style={{ height: 10 }} />

          <View style={{ justifyContent: "center" }}>
            <Text>{translate("or")}</Text>
          </View>

          <View style={{ height: 10 }} />

          <View style={styles.socialButtonContainerStyle}>
            <TouchableOpacity
              style={styles.socialButtonStyle}
              onPress={this.fbLogin} >
              <Ionicons name="logo-facebook" size={30} color="#fff" />
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.socialButtonStyle}>
              <Ionicons name="logo-twitter" size={30} color="#fff" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButtonStyle}>
              <Ionicons name="logo-googleplus" size={30} color="#fff" />
            </TouchableOpacity> */}
          </View>

          <View style={{ height: 60 }} />

          <View style={styles.signUpWrapperStyle}>
            <Text>{translate("already_have_an_account")} ? </Text>
            <TouchableOpacity
              onPress={() => {
                this.gotoSignIn();
              }}
            >
              <Text style={styles.signUpButtonStyle}>
                {translate("sign_in")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.footerStyle}>
          <Text style={{ textAlign: "center", fontSize: 12 }}>
            <Text>{translate("by_signing_up_you_agree_to_the")}</Text>
            <Text
              style={styles.footerButtonStyle}
              onPress={() => {
                this.gotoPrivacyPolicy();
              }}
            >
              {translate("privacy_policy")}
            </Text>
            <Text>
              {" "}
              {translate("and")} {"\n"}
            </Text>
            <Text
              style={styles.footerButtonStyle}
              onPress={() => {
                this.gotoTermOfService();
              }}
            >
              {translate("term_service")}
            </Text>
          </Text>
        </View>

        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isShowSuccess}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                source={require("../../images/login/login_success.png")}
                style={{ height: 90 }}
                resizeMode="contain"
              />
              <View style={{ height: 20 }} />
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#36a563",
                  fontSize: 20,
                  textAlign: "center"
                }}
              >
                {this.state.successMessage}
              </Text>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isShowError}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                source={require("../../images/login/login_error.png")}
                style={{ height: 90 }}
                resizeMode="contain"
              />
              <View style={{ height: 20 }} />
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#ff2057",
                  fontSize: 20,
                  textAlign: "center"
                }}
              >
                {this.state.errorMessage}
              </Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flexDirection: "column",
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center"
  },
  headerStyle: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20
  },
  headerLineStyle: {
    width: 20,
    height: 2,
    marginHorizontal: 5,
    backgroundColor: Config.primaryColor
  },
  headerTextStyle: {
    color: Config.textColor,
    fontSize: 35,
    fontWeight: "bold"
  },
  formInputStyle: {
    flexDirection: "column",
    alignItems: "center",
    width: "75%"
  },
  inputWrapperStyle: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: Config.textColor,
    flexDirection: "row",
    alignItems: "center",
    padding: 10,
    marginVertical: 10
  },
  inputStyle: {
    flex: 1,
    height: 40,
    marginLeft: 10
  },
  signInButtonStyle: {
    backgroundColor: Config.primaryColor,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  signInTextStyle: {
    color: "#ffffff",
    fontSize: 15
  },
  socialButtonContainerStyle: {
    flexDirection: "row"
  },
  socialButtonStyle: {
    height: 60,
    width: 60,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Config.textColor,
    margin: 5
  },
  forgotPasswordWrapperStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  forgotPasswordButtonStyle: {},
  forgotPasswordTextStyle: {
    fontSize: 15,
    color: Config.textColor
  },
  signUpWrapperStyle: {
    borderBottomWidth: 1,
    borderBottomColor: Config.textColor,
    padding: 10,
    flexDirection: "row"
  },
  signUpButtonStyle: {
    fontWeight: "bold",
    color: Config.primaryColor
  },
  footerStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 10
  },
  footerButtonStyle: {
    fontWeight: "bold",
    color: Config.textColor,
    textAlign: "center",
    marginBottom: 10
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },
  dialogBoxStyle: {
    width: 250,
    height: 200,
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  }
};

function mapStateToProps(state) {
  return {
    component: state.component
  };
}

const mapDispatchToProps = (dispatch) => ({
setRoot: (root) => dispatch({
    type: 'set_root',
    root,
  }),
userLogin: (userData, token) => dispatch({
    type: 'LOGIN_SUCCESS',
    data: {
      ...userData,
      token,
    },
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);
