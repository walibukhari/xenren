/* eslint-disable global-require */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-plusplus */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import Swiper from 'react-native-swiper';
import Config from '../Config';
import { translate } from '../i18n';

const { height, width } = Dimensions.get('window');

class Intro extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.root = props.component.root;
    console.log('intro.js');
  }

  gotoSignUp() {
    this.props.navigation.navigate('SignUp');
  }

  gotoLogin() {
    this.props.navigation.navigate('Login');
  }

  gotoPrivacyPolicy = () => {
    this.props.navigation.navigate('PrivacyPolicy');
  }

  gotoTermOfService() {
    this.props.navigation.navigate('TermOfService');
  }

  render() {
    return (
      <View style={styles.rootStyle} >
        <View style={styles.container}>
          <View style={{ height: height * 0.78 }}>
            <Swiper
              showsButtons={false}
              paginationStyle={styles.paginationStyle}
              renderPagination={(index, total) => {
                const arr = [];
                for (let i = 0; i < total; i++) {
                  if (index === i) {
                    arr.push(<View key={i} style={{
                      height: 8, width: 8, backgroundColor: '#57af2a', borderRadius: 4, marginHorizontal: 5,
                    }} />);
                  } else {
                    arr.push(<View key={i} style={{
                      height: 8, width: 8, backgroundColor: '#a9aba8', borderRadius: 4, marginHorizontal: 5,
                    }} />);
                  }
                }
                return (
                  <View style={{
                    alignItems: 'center', justifyContent: 'center', flexDirection: 'row', position: 'absolute', bottom: 0, alignSelf: 'center',
                  }} >
                    <View style={{
                      height: 1, width: 40, backgroundColor: '#57af2a', marginHorizontal: 5,
                    }} />
                    {arr}
                    <View style={{
                      height: 1, width: 40, backgroundColor: '#57af2a', marginHorizontal: 5,
                    }} />
                  </View>
                );
              }}
            >
              <View style={{
                alignItems: 'center', height: '100%', justifyContent: 'space-around',
              }} >
                <View style={{
                  width: '100%', marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignItems: 'center',
                }} >

                  <View style={{
                    flexDirection: 'row', justifyContent: 'center', marginTop: Platform.OS === 'ios' ? 45 : 15,
                  }} >
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'normal', padding: 5,
                    }} >
                      {translate('creative')}
                    </Text>
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'bold', padding: 5, fontFamily: 'Montserrat-Light',
                    }} >
                      {translate('service').toUpperCase()}
                    </Text>
                  </View>

                  <Text style={{
                    textAlign: 'center', fontSize: 18, fontWeight: 'normal', fontFamily: 'Montserrat-Light',
                  }} >
                    {translate('get_completed_work')}
                  </Text>

                </View>

                <View style={styles.bigImageWrapperStyle}>
                  <Image source={require('./../../images/intro/Detail_1.png')}
                    style={{
                      width: '90%',
                      height: '90%',
                    }} resizeMode="contain" />
                </View>
              </View>

              <View style={{
                alignItems: 'center', height: '100%', justifyContent: 'space-around',
              }} >
                <View style={{
                  width: '100%', marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignItems: 'center',
                }} >

                  <View style={{
                    flexDirection: 'row', justifyContent: 'center', marginTop: Platform.OS === 'ios' ? 45 : 15,
                  }} >
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'normal', padding: 5,
                    }} >
                    {translate('best')}
                    </Text>
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'bold', padding: 5, fontFamily: 'Montserrat-Light',
                    }} >
                    {translate('freelancers').toUpperCase()}
                    </Text>
                  </View>

                  <Text style={{
                    textAlign: 'center', fontSize: 18, fontWeight: 'normal', fontFamily: 'Montserrat-Light',
                  }} >
                    {translate('get_hired_profesional')}
                  </Text>

                </View>

                <View style={styles.bigImageWrapperStyle}>
                  <Image source={require('./../../images/intro/Detail_2.png')}
                    style={{
                      width: '90%',
                      height: '90%',
                    }} resizeMode="contain" />
                </View>
              </View>

              <View style={{
                alignItems: 'center', height: '100%', justifyContent: 'space-around',
              }} >
                <View style={{
                  width: '100%', marginTop: 10, marginBottom: 10, justifyContent: 'space-between', alignItems: 'center',
                }} >

                  <View style={{
                    flexDirection: 'row', justifyContent: 'center', marginTop: Platform.OS === 'ios' ? 45 : 15,
                  }} >
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'normal', padding: 5,
                    }} >
                    {translate('unlimited')}
                    </Text>
                    <Text style={{
                      textAlign: 'center', fontSize: 35, fontWeight: 'bold', padding: 5, fontFamily: 'Montserrat-Light',
                    }} >
                    {translate('jobs').toUpperCase()}
                    </Text>
                  </View>

                  <Text style={{
                    textAlign: 'center', fontSize: 18, fontWeight: 'normal', fontFamily: 'Montserrat-Light',
                  }} >
                  {translate('thousands_of_jobs_by_clients')}
                  </Text>

                </View>

                <View style={styles.bigImageWrapperStyle}>
                  <Image source={require('./../../images/intro/Detail_3.png')}
                    style={{
                      width: '92%',
                      height: '92%',
                    }} resizeMode="contain" />
                </View>
              </View>

            </Swiper>

          </View>

          <View style={{
            width: '100%', height: height * 0.22, justifyContent: 'flex-end', alignItems: 'center', bottom: Platform.OS === 'ios' ? 15 : 0,
          }} >
            <View
              style={{
                alignItems: 'center', justifyContent: 'center', flexDirection: 'row', width: '100%',
              }} >
              <TouchableOpacity onPress={() => { this.gotoSignUp(); }}
                style={{
                  backgroundColor: '#57af2a',
                  height: '70%',
                  width: '45%',
                  marginLeft: 10,
                  marginRight: 7,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 8,
                }} >
                <Text style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'center',
                }} >
                  {' '}
                  {translate('sign_up')}{' '}
                </Text>
              </TouchableOpacity>


              <TouchableOpacity onPress={() => { this.gotoLogin(); }}
                style={{
                  backgroundColor: '#f7f7f7',
                  height: '70%',
                  width: '45%',
                  marginRight: 10,
                  marginLeft: 7,
                  borderColor: '#57af2a',
                  borderWidth: 2,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 8,
                }} >
                <Text style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  color: '#57af2a',
                  textAlign: 'center',
                }} >
                  {' '}
                  {translate('sign_in')}{' '}
                </Text>
              </TouchableOpacity>

            </View>

            <View style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'center',
              marginBottom: 10,
            }}
            >
              <Text style={{ textAlign: 'center', fontSize: 12 }}>
                <Text>{translate('by_signing_up_you_agree_to_the')} </Text>
                <Text style={styles.footerButtonStyle}
                  onPress={() => { this.gotoPrivacyPolicy(); }} >
                  {translate('privacy_policy')}
                </Text>
                <Text>
                  {' '}{translate('and')}{' \n'}
                </Text>
                <Text style={styles.footerButtonStyle}
                  onPress={() => { this.gotoTermOfService(); }}>
                  {translate('term_service')} V.1.6
                </Text>
              </Text>
            </View>
          </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    flexDirection: 'column',
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',

    backgroundColor: '#f7f7f7',
    width: '100%',
    // ///////////////////////
    height: '100%',
  },
  wrapper: {},
  bigImageWrapperStyle: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    width: width * 0.86,
    height: width * 0.86,
    borderRadius: width * 0.43,
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallImageWrapperStyle: {
    backgroundColor: '#fff',
    width: 70,
    height: 70,
    borderRadius: 35,
    position: 'relative',
    top: -275,
    left: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },

  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  footerStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    // marginBottom: 10,
  },
  footerButtonStyle: {
    fontWeight: 'bold',
    color: Config.textColor,
    textAlign: 'center',
  },
});

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Intro);
