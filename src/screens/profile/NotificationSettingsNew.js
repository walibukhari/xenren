import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
    Text,
    View,
    Image,
    Switch,
    TouchableOpacity,
    ScrollView, TextInput, Alert, Keyboard, Dimensions
} from 'react-native';
import Config from '../../Config';
import {translate} from '../../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import CustomMarker from './CustomMarker';
import _ from "lodash";
import HttpRequest from "../../components/HttpRequest";
import Entypo from "react-native-vector-icons/Entypo";
import MultiSelectModal from "../../components/MultiSelectModal";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const {width, height} = Dimensions.get("window");

class NotificationSettingsNew extends Component {

    constructor(props) {
        super(props);
        this.state = {
            autocompleteText: "",
            autocompleteLanguageText: "",
            autocompletePosition: {
                x: 0,
                y: -200,
                width: 100,
            },
            atleastSpentLow: 10,
            atleastSpentHigh: 850,
            filteredSkills: [],
            fixedPriceLow: 3000,
            fixedPriceHigh: 6000,
            hourlyPriceLow: 10,
            hourlyPriceHigh: 850,
            locale: 'env',
            languages: [],
            skills: [],
            switchValue: false,
            selectedSkills: [],
            selectedSkillsNew: [],
            selectedLanguages: [],
            modalVisible: false,
            languageModalVisible: false,
            countries: [],
            selectedCountryItems: [],
            countryType: 1,
            countryTypeOriginal: 1,
            selectedCountries: [],
            active: true,
            countryModalVisible: false,
            searchKeyword: [],
            searchLanguageKeyword: [],
            notification_pricing: [
                { price: 0.99, currency: 'USD', limit: 100 },
                { price: 1.99, currency: 'USD', limit: 200 },
                { price: 2.99, currency: 'USD', limit: 300 },
            ],
            total_free_notifications: 0,
            remaining_notifications: 0,
        };
    }

    static navigationOptions = ({navigation: {navigate}}) => ({
        title: 'ABC'
    });

    static propTypes = {
        navigation: PropTypes.object.isRequired,
    };

    componentDidMount() {
        this.getSkillsList();
        this.getCountries();
        this.getLanguages();
        this.getNotificationSettings();
    };

    componentDidUpdate() {
    };

    getCountries() {
        HttpRequest.getOnlyCountries()
            .then((response) => {
                this.setState({
                    countries: response.data,
                });
            })
            .catch(error => {
            });
    };

    getNotificationSettings() {
        HttpRequest.getNotificationSettings(this.props.userData.token)
            .then((response) => {
                console.log('check response');
                console.log(response);
                const data = response.data.data;
                console.log("previous record");
                console.log(data,'notification settings response');
                console.log(data.language);
                var t = this;
                this.setState({
                    atleastSpentLow: data.low_atleast_spend,
                    atleastSpentHigh: data.high_atleast_spend,
                    fixedPriceLow: data.low_fixed_price,
                    fixedPriceHigh: data.high_fixed_price,
                    hourlyPriceLow: data.low_hourly_price,
                    hourlyPriceHigh: data.high_hourly_price,
                    countryType: data.country_type,
                    active: data.active == 1 ? true : false,
                    total_free_notifications: data.total_free_notifications,
                    remaining_notifications: data.remaining_notifications
                });
                setTimeout(function(){
                    data.language.map(function (item) {
                        let {languages, selectedLanguages} = t.state;
                        let emptyArrLang = selectedLanguages;
                        languages.map(subItem => {
                            if (item.language_name.id === subItem.id) {
                                !subItem.isSelect
                                    ? emptyArrLang.push(subItem.id)
                                    : emptyArrLang.splice(emptyArrLang.indexOf(subItem.id), 1);
                                subItem.isSelect = !subItem.isSelect;
                            }
                        });
                        t.setState({languages: languages, selectedLanguages: emptyArrLang});
                        console.log('emptyArrLang');
                        console.log(emptyArrLang);
                    });
                    data.country.map(function (item) {
                        t.selectedCountries(item.country_name);
                    });
                    data.keywords.map(function(item){
                        let {skills, selectedSkillsNew} = t.state;
                        let emptyArrKeyWords = selectedSkillsNew;
                        skills.map(subItem => {
                            if (item.skills.id === subItem.value) {
                                !subItem.isSelect
                                    ? emptyArrKeyWords.push(subItem.value)
                                    : emptyArrKeyWords.splice(emptyArrKeyWords.indexOf(subItem.value), 1);
                                subItem.isSelect = !subItem.isSelect;
                            }
                        });
                        t.setState({skills: skills, selectedSkillsNew: emptyArrKeyWords});
                    });
                }, 900);
            })
            .catch(error => {
            });
    }

    getLanguages() {
        HttpRequest.getLanguages()
            .then((response) => {
                this.setState({
                    languages: response.data.data,
                });
            })
            .catch(error => {
            });
    };

    closeSkillModal = () => {
        this.setState({
            modalVisible: false,
        });

    };

    closeLanguageModal = () => {
        this.setState({
            languageModalVisible: false,
        });

    };

    closeCountryModal = () => {
        this.setState({
            countryModalVisible: false,
        });
    }

    closeCountryModalWithSave = () => {
        this.setState({
            countryModalVisible: false
        });
        this.saveNotificationSettings();

    };

    getSkillsList() {
        HttpRequest.getSkills()
            .then((response) => {
                this.setState({
                    skills: response.data,
                });
            })
            .catch(() => {
            });
    };

    searchLanguage = (e) => {
        this.showLanguageResult();
    };

    highlight = string => string;

    filter(teks) {
        if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
            const pattern = new RegExp(teks, "i");
            let filteredSkills = _.filter(this.state.skills, (item) => {
                if (item.cn.search(pattern) !== -1) {
                    return item;
                }
            });
            filteredSkills = _.slice(filteredSkills, 0, 10);
            this.setState({filteredSkills});
        } else {
            const pattern = new RegExp(teks, "i");
            let filteredSkills = _.filter(this.state.skills, (item) => {
                if (item.text.search(pattern) !== -1) {
                    return item;
                }
            });
            filteredSkills = _.slice(filteredSkills, 0, 10);
            this.setState({filteredSkills});
        }
    };

    setModalVisible(visible, type) {
        this.setState({
            countryModalVisible: visible,
            countryType: type
        });
        let t = this;
        setTimeout(function(){
            t.saveNotificationSettings();
        }, 900)
    };

    selectSkill(item) {
        if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {

            let duplicate = false;
            this.state.selectedSkills.forEach((item2) => {
                if (item.value == item2.value) {
                    duplicate = true;
                }
            });
            if (duplicate) {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
                return false;
            }

            if (item.cn !== this.state.duplicate) {
                this.setState({duplicate: item.cn});
                const {selectedSkills} = this.state;
                selectedSkills.push(item);
                this.setState({
                    selectedSkills,
                    autocompleteText: "",
                });
            } else {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
            }
        } else {
            let duplicate = false;
            this.state.selectedSkills.forEach((item2) => {
                if (item.value == item2.value) {
                    duplicate = true;
                }
            });
            if (duplicate) {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
                return false;
            }

            if (item.text !== this.state.duplicate) {
                this.setState({duplicate: item.text});
                const {selectedSkills} = this.state;
                selectedSkills.push(item);
                this.setState({
                    selectedSkills,
                    autocompleteText: "",
                });
            } else {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
            }
        }
    };

    showSearchResult() {
        Keyboard.dismiss();
        if (this.state.selectedSkills.length > 0) {
            if (this.state.selectedButton === 0) {
                this.props.navigation.navigate("JobsList", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                });
            } else {
                this.props.navigation.navigate("SearchResult", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                    title:
                        this.state.selectedButton === 1
                            ? "Find Experts"
                            : translate("find_cofounder"),
                });
            }
        } else {
            Alert.alert("Error", translate('select_skills'), [
                {
                    text: "OK",
                }
            ]);
        }
    };

    showLanguageResult() {
        Keyboard.dismiss();
        if (this.state.selectedSkills.length > 0) {
            if (this.state.selectedButton === 0) {
                this.props.navigation.navigate("JobsList", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                });
            } else {
                this.props.navigation.navigate("SearchResult", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                    title:
                        this.state.selectedButton === 1
                            ? "Find Experts"
                            : translate("find_cofounder"),
                });
            }
        } else {
            Alert.alert("Error", translate('select_skills'), [
                {
                    text: "OK",
                }
            ]);
        }
    };

    toggleSwitch = value => {
        //onValueChange of the switch this function will be called
        this.setState({active: value});
        this.saveNotificationSettings();
        //state changes according to switch
        //which will result in re-render the text
    };

    goBack = () => {
        this.props.navigation.navigate('Root');
    };

    fixedPriceChange = values => {
        this.setState({
            fixedPriceLow: values[0],
            fixedPriceHigh: values[1]
        })

    };

    atleastSpentChange = values => {
        this.setState({
            atleastSpentLow: values[0],
            atleastSpentHigh: values[1]
        })

    };

    multiSliderHourlyValuesChange = values => {
        this.setState({
            hourlyPriceLow: values[0],
            hourlyPriceHigh: values[1]
        })

    };

    updateCountryType = type => {
        this.setState({
            countryType: type
        });
        if (type != 1) {
            this.setModalVisible(true, type);
        } else {
            this.setModalVisible(false, type);
        }
    };

    //Countries
    selectedCountries = item => {
        let {countries, selectedCountryItems} = this.state;
        let emptyArr = selectedCountryItems;
        countries.map(subItem => {
            if (item.id === subItem.id) {
                !subItem.isSelect
                    ? emptyArr.push(subItem.id)
                    : emptyArr.splice(emptyArr.indexOf(subItem.id), 1);
                subItem.isSelect = !subItem.isSelect;
            }
        });
        this.setState({countries: countries, selectedCountries: emptyArr});
    };

    renderCountries = item => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                    onPress={() => this.selectedCountries(item)}
                    style={styles.renderContainer}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            color: item.isSelect ? Config.primaryColor : null
                        }}
                    >
                        {item.name}
                    </Text>

                    {item.isSelect && (
                        <MaterialIcons
                            name={"check"}
                            size={20}
                            style={{marginRight: 15}}
                            color={Config.primaryColor}
                        />
                    )}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    toCSS = (obj, separator) => {
        var arr = [];

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(obj[key]);
            }
        }

        return arr.join(separator || ",");
    };

    saveNotificationSettings = () => {
        let selectedCountries = this.toCSS(this.state.selectedCountries);
        let data = this.state.selectedSkillsNew;
        let sc = this.state.skills.filter(function (item) {
            if (data.indexOf(item.value) > -1) {
                return item;
            }
        });
        var selectedSkills = sc.map(function (item) {
            return item.value;
        });
        selectedSkills = selectedSkills.join(',');

        let dataLang = this.state.selectedLanguages;
        let scLang = this.state.languages.filter(function (item) {
            if (dataLang.indexOf(item.id) > -1) {
                return item;
            }
        });
        let selectedLanguages = scLang.map(function (item) {
            return item.id;
        });
        selectedLanguages = selectedLanguages.join(',')

        let formObj = {
            'low_fixed_price': this.state.fixedPriceLow,
            'high_fixed_price': this.state.fixedPriceHigh,
            'low_hourly_price': this.state.hourlyPriceLow,
            'high_hourly_price': this.state.hourlyPriceHigh,
            'low_atleast_spend': this.state.atleastSpentLow,
            'high_atleast_spend': this.state.atleastSpentHigh,
            'package_id': 1,
            'keyword': selectedSkills,
            'language': selectedLanguages,
            'country': selectedCountries,
            'country_type': this.state.countryType,
            'active': !this.state.active == true ? 1 : 0,
        };
        console.log('saving now');
        console.log(formObj);
        HttpRequest.saveNotificationSettings(this.props.userData.token, formObj)
            .then((response) => {
                const data = response.data.data;
            })
            .catch(error => {
            });


    };

    renderSelectedSkills = (data) => {
        var t = this;
        var sc = this.state.skills.filter(function (item) {
            if (data.indexOf(item.value) > -1) {
                return item;
            }
        });
        return _.map(sc, (item, key) => {
            return (
                <View
                    style={[styles.tagInput, {
                        color: "black",
                        shadowColor: multiTextColor,
                        shadowOpacity: 0.2,
                        shadowRadius: 5,
                        shadowOffset: {
                            height: 1,
                            width: 1,
                        },
                        borderColor: '#fff'
                    }]}
                    key={key}
                    elevation={3}
                >
                    <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 12
                    }}>
                        {item.text}
                    </Text>
                </View>
            );
        });
    };

    renderSelectedLanguages = (data) => {
        var t = this;
        var sc = this.state.languages.filter(function (item) {
            if (data.indexOf(item.id) > -1) {
                return item;
            }
        });
        console.log("sc");
        console.log(sc);

        return _.map(sc, (item, key) => {
            return (
                <View
                    style={[styles.tagInput, {
                        color: "black",
                        shadowColor: multiTextColor,
                        shadowOpacity: 0.2,
                        shadowRadius: 5,
                        shadowOffset: {
                            height: 1,
                            width: 1,
                        },
                        borderColor: '#fff'
                    }]}
                    key={key}
                    elevation={3}
                >
                    <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 12
                    }}>
                        {item.name}
                    </Text>
                </View>
            );
        });
    };

    selectSkills = (item) => {
        let {skills, selectedSkillsNew} = this.state;
        let emptyArr = selectedSkillsNew;
        skills.map(subItem => {
            if (item.value === subItem.value) {
                !subItem.isSelect
                    ? emptyArr.push(subItem.value)
                    : emptyArr.splice(emptyArr.indexOf(subItem.value), 1);
                subItem.isSelect = !subItem.isSelect;
            }
        });
        this.setState({skills: skills, selectedSkillsNew: emptyArr});
        this.saveNotificationSettings();
    };

    selectLanguage = (item) => {
        let {languages, selectedLanguages} = this.state;
        let emptyArr = selectedLanguages;
        languages.map(subItem => {
            if (item.id === subItem.id) {
                !subItem.isSelect
                    ? emptyArr.push(subItem.id)
                    : emptyArr.splice(emptyArr.indexOf(subItem.id), 1);
                subItem.isSelect = !subItem.isSelect;
            }
        });
        this.setState({languages: languages, selectedLanguages: emptyArr});
        this.saveNotificationSettings();
    };

    renderSkills = item => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                    onPress={() => this.selectSkills(item)}
                    style={styles.renderContainer}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            color: item.isSelect ? Config.primaryColor : null
                        }}
                    >
                        {item.text}
                    </Text>

                    {item.isSelect && (
                        <MaterialIcons
                            name={"check"}
                            size={20}
                            style={{marginRight: 15}}
                            color={Config.primaryColor}
                        />
                    )}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    renderLanguages = item => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                    onPress={() => this.selectLanguage(item)}
                    style={styles.renderContainer}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            color: item.isSelect ? Config.primaryColor : null
                        }}
                    >
                        {item.name}
                    </Text>

                    {item.isSelect && (
                        <MaterialIcons
                            name={"check"}
                            size={20}
                            style={{marginRight: 15}}
                            color={Config.primaryColor}
                        />
                    )}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    renderNotificationPricingItem(item, key) {
        return (<View key={key} style={{
            width: 110,
            height: 90,
            margin: 5,
            borderWidth: 1,
            borderColor: Config.primaryColor
        }}>
            <Image
                style={{
                    width: 40,
                    height: 40,
                    left: 30,
                    // top: 25,
                    // bottom: 10
                }}
                source={require('../../../images/hand.png')} />
            <Text style={{
                fontFamily: FontStyle.Bold,
                fontSize: 12,
                color: '#b7b7b7',
                textAlign: "center",
                flex: 1
            }}>
                Top up {item.limit} push
            </Text>
            <Text style={{
                fontFamily: FontStyle.Bold,
                fontSize: 12,
                color: '#b7b7b7',
                textAlign: "center",
                flex: 1
            }}>
                {item.price} {item.currency}
            </Text>
        </View>);
    }
    render() {
        if (this.state.loading === true) {
            return (
                <View style={{alignItems: 'center', justifyContent: 'center', marginTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        } else {
            return (
                <ScrollView>
                    <View style={styles.rootStyle}>
                        {/* Back Button View */}
                        <View style={styles.navigationStyle}>
                            <View style={styles.navigationWrapper}>
                                <TouchableOpacity
                                    onPress={this.goBack}
                                    style={{
                                        marginTop: 8,
                                        marginLeft: 2,
                                        flexDirection: "row"
                                    }}
                                >
                                    <Image
                                        style={{width:20,height:26,position:'relative',top:-3}}
                                        source={require('../../../images/arrowLA.png')}
                                    />

                                    <Text
                                        style={{
                                            marginLeft: 10,
                                            marginTop:-1,
                                            fontSize: 19.5,
                                            fontWeight:'normal',
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Regular,
                                        }}
                                    >
                                        {translate("back")}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={this.goBack}
                                    style={{
                                        marginTop: 0,
                                        marginLeft: 2,
                                        flexDirection: "row"
                                    }}
                                >
                                    <Switch
                                        onValueChange={this.toggleSwitch}
                                        style={{backgroundColor: 'transparent', borderRadius: 17,marginTop:-15}}
                                        value={this.state.active}
                                        trackColor={{true: Config.primaryColor, false: '#B7B7B7'}}
                                        activeText={'On'}
                                        inActiveText={'Off'}
                                    />
                                </TouchableOpacity>

                            </View>
                        </View>

                        {/* Main Page Starts here */}
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            alignItems: 'stretch',
                        }}>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                alignSelf: 'center'
                            }}>
                                {/*First Section*/}
                                <View style={{
                                    width: width,
                                    height: 100,
                                }}>
                                    <Text style={{
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                        left: 10,
                                        marginTop: 15
                                    }}>Fixed Price</Text>

                                    <TouchableOpacity style={{
                                        marginLeft: 14,
                                        width: width,
                                        marginTop: 0,
                                        left: 0,
                                    }}>
                                        <MultiSlider
                                            selectedStyle={{
                                                backgroundColor: Config.primaryColor,
                                            }}
                                            unselectedStyle={{
                                                backgroundColor: '#d7d7d7',
                                            }}
                                            trackStyle={{
                                                backgroundColor: 'red',
                                            }}
                                            values={[
                                                3000,
                                                6000,
                                            ]}
                                            touchDimensions={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 15,
                                                slipDisplacement: 200
                                            }}
                                            sliderLength={330}
                                            onValuesChange={this.fixedPriceChange}
                                            onValuesChangeFinish={this.saveNotificationSettings}
                                            min={100}
                                            max={10000}
                                            customMarker={CustomMarker}
                                            step={1}
                                            allowOverlap
                                            snapped
                                        />
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        color: infoTextColor,
                                        fontSize: 12,
                                        marginLeft: '30%',
                                    }}>
                                        {this.state.fixedPriceLow}USD - {this.state.fixedPriceHigh}USD
                                    </Text>
                                </View>
                                {/*First Section*/}
                            </View>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                marginTop: 13,
                                alignSelf: 'center'
                            }}>
                                <View style={{width: width, height: 100}}>
                                    {/*Second section*/}
                                    <View style={{
                                        width: width,
                                        height: 100,
                                        borderTopWidth: 3,
                                        borderTopColor: borderColor
                                    }}>
                                        <Text style={{
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Bold,
                                            left: 10,
                                            marginTop: 15
                                        }}>
                                            Hourly Price
                                        </Text>

                                        <TouchableOpacity style={{
                                            marginLeft: 14,
                                            width: width,
                                            left: 0,
                                        }}>
                                            <MultiSlider
                                                selectedStyle={{
                                                    backgroundColor: Config.primaryColor,
                                                }}
                                                unselectedStyle={{
                                                    backgroundColor: '#d7d7d7',
                                                }}
                                                trackStyle={{
                                                    backgroundColor: 'red',
                                                }}
                                                values={[
                                                    this.state.hourlyPriceLow,
                                                    this.state.hourlyPriceHigh,
                                                ]}
                                                touchDimensions={{
                                                    height: 50,
                                                    width: 50,
                                                    borderRadius: 15,
                                                    slipDisplacement: 200
                                                }}
                                                sliderLength={330}
                                                onValuesChange={this.multiSliderHourlyValuesChange}
                                                onValuesChangeFinish={this.saveNotificationSettings}
                                                min={0}
                                                max={1000}
                                                customMarker={CustomMarker}
                                                step={1}
                                                allowOverlap
                                                snapped
                                            />
                                        </TouchableOpacity>
                                        <Text style={{
                                            fontFamily: FontStyle.Regular,
                                            color: infoTextColor,
                                            fontSize: 12,
                                            marginLeft: '35%',
                                        }}>
                                            {this.state.hourlyPriceLow}USD - {this.state.hourlyPriceHigh}USD
                                        </Text>
                                    </View>
                                    {/*Second section*/}
                                </View>
                            </View>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                marginTop: 13,
                                alignSelf: 'center'
                            }}>
                                <View style={{width: width, height: 100}}>
                                    {/*Third Section*/}
                                    <View style={{
                                        width: width,
                                        height: 90,
                                        borderTopWidth: 3,
                                        borderColor: borderColor,
                                        marginBottom: 20
                                    }}>
                                        <Text style={{
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Bold,
                                            left: 10,
                                            marginTop: 15
                                        }}>Keyword</Text>

                                        <ScrollView
                                            showsHorizontalScrollIndicator={true}
                                            horizontal={true}
                                            style={{
                                                flexDirection: "row",
                                                marginRight: 5,
                                                border: 1,
                                                borderWidth: 1,
                                                borderRadius: 3,
                                                width: width * .9,
                                                marginLeft: 10,
                                                marginTop: 10,
                                                borderColor: textAreaBorderColor,
                                            }}
                                        >
                                            {this.renderSelectedSkills(this.state.selectedSkillsNew)}
                                            <View>
                                                <TextInput
                                                    style={{
                                                        flex: 1,
                                                        width: 300,
                                                        padding: 8,
                                                        fontSize: 12,
                                                        fontFamily: FontStyle.Regular
                                                    }}
                                                    placeholder={'Select Keyword'}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    underlineColorAndroid="transparent"
                                                    value={''}
                                                    ref={(c: any) => {
                                                        this.textInputRef = c;
                                                    }}
                                                    onFocus={() => {
                                                        let visible = this.state.modalVisible;
                                                        if (visible) {
                                                            this.setState({
                                                                modalVisible: false
                                                            });
                                                        } else {
                                                            this.setState({
                                                                modalVisible: true
                                                            });
                                                            this.textInputRef.blur()
                                                        }
                                                    }}
                                                />
                                            </View>

                                        </ScrollView>
                                    </View>
                                    {/*Third Section*/}
                                </View>
                            </View>


                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                borderTopWidth: 3,
                                borderTopColor: borderColor,
                                height: 230,
                            }}>
                                <View style={{flex: 1, flexDirection: 'row'}}>
                                    <Text style={{
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                        marginTop: 15,
                                        marginLeft: 10
                                    }}>Country</Text>
                                </View>

                                <TouchableOpacity style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    marginTop: -20,
                                }} onPress={() => {
                                    this.updateCountryType(1);
                                }}>
                                    <View style={{
                                        width: width * .10,
                                        height: 20,
                                        "display": "flex",
                                        "align-items": "center",
                                        "justify-content": "center",
                                        left: 10,
                                        top: 10
                                    }}>
                                        {this.state.countryType == 1 &&
                                        <Entypo
                                            name="check"
                                            size={10}
                                            color={Config.primaryColor}
                                        />
                                        }
                                    </View>
                                    <View style={{
                                        width: width * .8,
                                        height: 50,
                                        borderBottomWidth: 1,
                                        borderBottomColor: 'lightgray'
                                    }}>
                                        <Text style={{
                                            fontFamily: this.state.countryType == 1 ? FontStyle.Bold : FontStyle.Light,
                                            color: '#353635',
                                        }}>
                                            All
                                        </Text>
                                        <Text style={{
                                            fontFamily: FontStyle.Regular,
                                            color: '#b7b7b7',
                                            fontSize: 12
                                        }}>
                                            Receive from all countries
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                }} onPress={() => {
                                    this.updateCountryType(2);
                                }}>
                                    <View style={{
                                        width: width * .10,
                                        height: 20,
                                        "display": "flex",
                                        "align-items": "center",
                                        "justify-content": "center",
                                        left: 10,
                                        top: 10
                                    }}>
                                        {this.state.countryType == 2 &&
                                        <Entypo
                                            name="check"
                                            size={10}
                                            color={Config.primaryColor}
                                        />}
                                    </View>
                                    <View style={{
                                        width: width * .8,
                                        height: 50,
                                        borderBottomWidth: 1,
                                        borderBottomColor: 'lightgray'
                                    }}>
                                        <Text style={{
                                            fontFamily: this.state.countryType == 2 ? FontStyle.Bold : FontStyle.Light,
                                            color: '#353635',
                                        }}>
                                            All Expect...
                                        </Text>
                                        <Text style={{
                                            fontFamily: FontStyle.Regular,
                                            color: '#b7b7b7',
                                            fontSize: 12
                                        }}>
                                            Receive from all countries except countries you select
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                }} onPress={() => {
                                    this.updateCountryType(3);
                                }}>
                                    <View style={{
                                        width: width * .10,
                                        height: 20,
                                        "display": "flex",
                                        "align-items": "center",
                                        "justify-content": "center",
                                        left: 10,
                                        top: 10
                                    }}>
                                        {this.state.countryType == 3 &&
                                        <Entypo
                                            name="check"
                                            size={10}
                                            color={Config.primaryColor}
                                        />}
                                    </View>
                                    <View style={{
                                        width: width * .8,
                                        height: 50,
                                        borderBottomWidth: 1,
                                        borderBottomColor: 'lightgray'
                                    }}>
                                        <Text style={{
                                            fontFamily: this.state.countryType == 3 ? FontStyle.Bold : FontStyle.Light,
                                            color: '#353635',
                                        }}>
                                            Only From
                                        </Text>
                                        <Text style={{
                                            fontFamily: FontStyle.Regular,
                                            color: '#b7b7b7',
                                            fontSize: 12
                                        }}>
                                            Receive only from selected countries
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                            </View>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                alignSelf: 'center'
                            }}>
                                <View style={{
                                    width: width,
                                    height: 100,
                                    borderTopWidth: 3,
                                    borderTopColor: borderColor
                                }}>
                                    <Text style={{
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                        left: 10,
                                        marginTop: 10
                                    }}>Atleast Spent</Text>

                                    <TouchableOpacity style={{
                                        marginLeft: 14,
                                        width: width,
                                        left: 0,
                                    }}>
                                        <MultiSlider
                                            selectedStyle={{
                                                backgroundColor: Config.primaryColor,
                                            }}
                                            unselectedStyle={{
                                                backgroundColor: '#d7d7d7',
                                            }}
                                            trackStyle={{
                                                backgroundColor: 'red',
                                            }}
                                            values={[
                                                this.state.atleastSpentLow,
                                                this.state.atleastSpentHigh,
                                            ]}
                                            touchDimensions={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 15,
                                                slipDisplacement: 200
                                            }}
                                            onValuesChangeFinish={this.saveNotificationSettings}
                                            sliderLength={330}
                                            onValuesChange={this.atleastSpentChange}
                                            min={1}
                                            max={10000}
                                            customMarker={CustomMarker}
                                            step={1}
                                            allowOverlap
                                            snapped
                                        />
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        color: infoTextColor,
                                        fontSize: 12,
                                        marginLeft: '35%',
                                    }}>
                                        {this.state.atleastSpentLow}USD - {this.state.atleastSpentHigh}USD
                                    </Text>
                                </View>
                            </View>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                marginTop: 13,
                                alignSelf: 'center'
                            }}>
                                <View style={{width: width, height: 100}}>
                                    {/*Third Section*/}
                                    <View style={{
                                        width: width,
                                        height: 90,
                                        borderTopWidth: 3,
                                        borderColor: borderColor,
                                        marginBottom: 20
                                    }}>
                                        <Text style={{
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Bold,
                                            left: 10,
                                            marginTop: 15
                                        }}>Language</Text>

                                        <ScrollView
                                            showsHorizontalScrollIndicator={true}
                                            horizontal={true}
                                            style={{
                                                flexDirection: "row",
                                                marginRight: 5,
                                                border: 1,
                                                borderWidth: 1,
                                                borderRadius: 3,
                                                width: width * .9,
                                                marginLeft: 10,
                                                marginTop: 10,
                                                borderColor: textAreaBorderColor,
                                            }}
                                        >
                                            {this.renderSelectedLanguages(this.state.selectedLanguages)}
                                            <View>
                                                <TextInput
                                                    style={{
                                                        flex: 1,
                                                        width: 300,
                                                        padding: 8,
                                                        fontSize: 12,
                                                        fontFamily: FontStyle.Regular
                                                    }}
                                                    placeholder={'Select Language'}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    underlineColorAndroid="transparent"
                                                    value={''}
                                                    ref={(d: any) => {
                                                        this.textInputRef = d;
                                                    }}
                                                    onFocus={() => {
                                                        let visible = this.state.languageModalVisible;
                                                        if (visible) {
                                                            this.setState({
                                                                languageModalVisible: false
                                                            });
                                                        } else {
                                                            this.setState({
                                                                languageModalVisible: true
                                                            });
                                                            this.textInputRef.blur()
                                                        }
                                                    }}
                                                />
                                            </View>

                                        </ScrollView>
                                    </View>
                                    {/*Third Section*/}
                                </View>
                            </View>


                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                marginTop: 13,
                                alignSelf: 'center'
                            }}>
                                <View style={{ height: 130 }}>
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                        borderTopWidth: 3,
                                        borderTopColor: borderColor }}>

                                        { this.state.notification_pricing.map(this.renderNotificationPricingItem) }
                                    </View>
                                    <View style={{
                                        // borderTopWidth: 3,
                                        // borderTopColor: borderColor
                                    }}>
                                        <View style={{alignSelf: 'flex-start'}}>
                                            <Text> Remaining </Text>
                                        </View>
                                        <View style={{alignSelf: 'flex-end', marginTop: -14}}>
                                            <Text> {this.state.remaining_notifications}/{this.state.total_free_notifications} </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                                marginTop: 13,
                                alignSelf: 'center'
                            }}>
                                <View style={{
                                    borderTopWidth: 3,
                                    borderTopColor: borderColor,
                                    height: 40,
                                    borderBottomWidth: 6,
                                    borderBottomColor: '#e6e6e6'
                                }}>
                                    <View style={{alignSelf: 'center'}}>
                                        <Text style={{
                                            fontFamily: FontStyle.Regular,
                                            fontSize: 10,
                                            color: "gray"
                                        }}> The complete info you fill, will allow you to receive better proposal! </Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </View>


                    {
                        this.state.modalVisible && (
                            <MultiSelectModal
                                visible={this.state.modalVisible}
                                onRequestClose={() => this.closeSkillModal()}
                                title={'Select Keyword'}
                                onChangeText={searchKeyword =>
                                    this.setState({searchKeyword})
                                }
                                onCrossPressed={() => this.closeSkillModal()}
                                value={this.state.searchKeyword}
                                data={
                                    this.state.skills.filter(item =>
                                        item.text.includes(this.state.searchKeyword)
                                    )
                                }
                                renderItem={({item, index}) =>
                                    this.renderSkills(item, index)
                                }
                                extraData={this.state}
                                submitPressed={() => this.closeSkillModal()}
                            />
                        )
                    }

                    {
                        this.state.languageModalVisible && (
                            <MultiSelectModal
                                visible={this.state.languageModalVisible}
                                onRequestClose={() => this.closeLanguageModal()}
                                title={'Select Language'}
                                onChangeText={searchLanguageKeyword =>
                                    this.setState({searchLanguageKeyword})
                                }
                                onCrossPressed={() => this.closeLanguageModal()}
                                value={this.state.searchLanguageKeyword}
                                data={
                                    this.state.languages.filter(item =>
                                        item.name.includes(this.state.searchLanguageKeyword)
                                    )
                                }
                                renderItem={({item, index}) =>
                                    this.renderLanguages(item, index)
                                }
                                extraData={this.state}
                                submitPressed={() => this.closeLanguageModal()}
                            />
                        )
                    }

                    {
                        this.state.countryModalVisible && (
                            <MultiSelectModal
                                visible={this.state.countryModalVisible}
                                onRequestClose={() => this.closeCountryModal()}
                                title={'Select Countries'}
                                onChangeText={searchCountry =>
                                    this.setState({searchCountry})
                                }
                                onCrossPressed={() => this.closeCountryModal()}
                                value={this.state.searchCountry}
                                data={
                                    this.state.countries.filter(item =>
                                        item.name
                                    )
                                }
                                renderItem={({item, index}) =>
                                    this.renderCountries(item, index)
                                }
                                extraData={this.state}
                                submitPressed={() => this.closeCountryModalWithSave()}
                            />
                        )
                    }
                </ScrollView>
            );
        }
    }
}

const borderColor = '#e6e6e6';
const textAreaBorderColor = '#d4d4d4';
const multiTextColor = '#363636';
const infoTextColor = '#353635';
const checkboxBackgroundColor = '#b7b7b7';
const starsColor = '#d7d7d7';
const applyFilterBackground = '#f7f7f7';
const styles = {
    renderContainer: {
        height: 45,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    },

    rootStyle: {
        backgroundColor: "#fff",
        flex: 1,
        flexDirection: "column"
    },

    tagInput: {
        height: 30,
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: '#fff',
        borderColor: '#B7B7B7',
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10,
        marginLeft: 7,
        marginTop: 5,
        width: 70
    },

    contactTextStyle: {
        color: '#fff',
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 30,
        paddingVertical: 10,
        fontSize: 15,
        marginTop: 10,
        fontFamily: FontStyle.Bold,
    },

    backArrowStyle: {
        marginRight: 10
    },

    backArrowIconStyle: {
        fontFamily: "Montserrat-Light",
        marginLeft: 10
    },

    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 20,
        paddingHorizontal: 5
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%"
    },

    searchBoxWrapperStyle: {
        borderRadius: 2,
        backgroundColor: "#fff",
        height: 45,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        top: 50,
        // borderTopWidth: 3,
        // borderTopColor: '#B7B7B7',
    },

    searchBoxWrapperStyleOne: {
        borderRadius: 2,
        backgroundColor: "#fff",
        height: 60,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        top: 40,
        // borderTopWidth: 3,
        // borderTopColor: '#B7B7B7',
    },

};

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        }),
        mySelectedSeatData: data => dispatch({
            type: 'SELECTED_SEAT_DATA',
            data: {...data},
        }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationSettingsNew);

// export default NotificationSettings
