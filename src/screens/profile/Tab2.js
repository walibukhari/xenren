import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Modal, ActivityIndicator, Dimensions, ProgressBarAndroid, Platform, ProgressViewIOS, ImageBackground, ScrollView, Alert } from 'react-native';
import LocalData from '../../components/LocalData';
import HttpRequest from '../../components/HttpRequest';
import ImagePicker from 'react-native-image-picker';
import Config from '../../Config';
import { translate } from '../../i18n';
import { Sentry } from 'react-native-sentry';
import FontStyle from '../../constants/FontStyle';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';

const { height, width } = Dimensions.get('window');
class Tab2 extends Component {
  constructor(props) {
    super(props);

    const prop = this.props.navigation;
    this.state = {
      idCard: require('../../../images/idCard.png'),
      banIdCard: require('../../../images/idCard.png'),
      idHoldingCard: require('../../../images/holdId.png'),
      banIdHoldingCard: require('../../../images/holdId.png'),
      plus: require('../../../images/plus.png'),
      showModal: false,
      uploadType: 'id_card', // id_holding_card
      userId: null,
      props: prop,
      token: null,
      id: false,
      holdId: false,
      status: translate('uploading'),
      status1: translate('uploading'),
      uploaded: false,
      disable: true,
      loading: true,
      uploading: false,
      idUploading: false,
      showIcon: true,
      showIcon1: true,
      idName: '',
      idHoldName: '',
      progressBar: 0.1,
      progressBar1: 0.1,
      percentage: 10,
      percentage1: 10,
      source: '',
      percentageSign: '%',
      percentageSign1: '%',
      adminApproved: false,
      approved: false,
      notApproved: false,
      idImageName: '',
      holdImageName: '',
      info: false,
      bit: false,
    };
    LocalData.getUserData().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ token: resp.token, userId: resp.id });
      this.getUserImage(this.state.token);
    });
  }
  interval = 0;
  interval1 = 0;
  componentDidMount() {
    LocalData.getVerifyData().then((res) => {
      const resp = JSON.parse(res);
      if (resp !== null) { this.setState({ info: true }); }
    });
  }
  componentDidUpdate() {
    if (this.state.uploadType === 'id_card') {
      if (this.state.percentage === 110) {
        this.setState({
          idCard: this.state.source,
          percentage: '',
          status: translate('upload_success'),
          percentageSign: '',
          uploaded: true,
          // showIcon: false,
          // disable: false,
        });
        clearInterval(this.interval);
      }
    } else
    if (this.state.percentage1 === 110) {
      this.setState({
        idHoldingCard: this.state.source,
        percentage1: '',
        status1: translate('upload_success'),
        percentageSign1: '',
        // showIcon1: false,
        disable: false,
      });
      clearInterval(this.interval1);
    }
  }
  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.interval1);
  }

  uploadUserImage(imageType, userImage, userId, image) {
    this.setState({ source: image });
    HttpRequest.uploadPicture(imageType, userImage, userId)
      .then((resp) => {
        const result = resp.data;
        if (result.status === 'OK') {
          if (imageType === 'ID') {
            this.setInterval();
          }
          if (imageType === 'HOLD_ID') {
            this.setInterval();
          }
        } else if (resp.data.status === 'ERROR') {
          this.setState({
            idCard: this.state.banIdCard,
            idHoldingCard: this.state.banIdHoldingCard,
            percentage: '',
            status: '',
            uploading: false,
          });
          Alert.alert("Error", result.message, [
            {
              text: "OK",
            }
          ]);
        } else {
          // this.setState({ loading: false });
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  setInterval() {
    if (this.state.uploadType === 'id_card') {
      if (this.state.notApproved === true) {
        this.setState({ notApproved: false, approved: false, adminApproved: false });
      }
      this.interval = setInterval(
        () => this.setState(prevState => ({
          progressBar: prevState.progressBar + 0.1,
          percentage: prevState.percentage + 10,
        })),
        200,
      );
    } else {
      this.interval1 = setInterval(
        () => this.setState(prevState => ({
          progressBar1: prevState.progressBar1 + 0.1,
          percentage1: prevState.percentage1 + 10,
        })),
        200,
      );
    }
  }

  reset() {
    HttpRequest.reset(this.state.token)
      .then((response) => {
        if (response.data.status === 'success') {
          this.state.props.navigate('Tab');
          this.setState({
            disable: false,
          });
        } else {
          Alert.alert("Error", response.data.message, [
            {
              text: "OK",
            }
          ]);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  getUserImage(token) {
    HttpRequest.getUserImage(token)
      .then((response) => {
        const result = response.data.data;
        const idImg = result && result.id_image ? result.id_image : '';
        const idImgHold = result && result.hold_id_image ? result.hold_id_image : '';
        const imageId = { uri: idImg };
        const holdImageId = { uri: idImgHold };
        if (response.data.status === 'success' || response.data.bit === 0) {
          if (idImg === Config.webUrl && idImgHold === Config.webUrl) {
            this.setState({ loading: false });
          } else if (idImg && idImgHold === '') {
            this.setState({
              idCard: imageId,
              loading: false,
              uploaded: true,
            });
          } else if (idImg === '' && idImgHold) {
            this.setState({
              idHoldingCard: holdImageId,
              loading: false,
            });
          } else if (imageId === '' || idImgHold === '') {
            this.setState({ loading: false });
          } else {
            const status = response.data.data.status;
            LocalData.getFileData().then((res) => {
              const resp = JSON.parse(res);
              if (resp !== null) { this.setState({ idImageName: resp.idCardImage }); }
            });
            LocalData.getFile1Data().then((res) => {
              const resp = JSON.parse(res);
              if (resp !== null) { this.setState({ holdImageName: resp.idHoldImage }); }
            });
            if (status === 0 || status === null) {
              this.setState({
                approved: false,
                idCard: imageId,
                idHoldingCard: holdImageId,
                loading: false,
                adminApproved: true,
                showIcon: false,
                showIcon1: false,
              });
            } else if (status === 1) {
              this.setState({
                approved: true,
                idCard: imageId,
                idHoldingCard: holdImageId,
                loading: false,
                adminApproved: true,
                showIcon: false,
                showIcon1: false,
              });
            } else if (status === 3) {
              this.setState({
                idCard: imageId,
                idHoldingCard: holdImageId,
                loading: false,
                disable: false,
                showIcon: true,
                showIcon1: true,
              });
            } else {
              this.setState({
                notApproved: true,
                approved: true,
                bit: true,
                idCard: imageId,
                idHoldingCard: holdImageId,
                loading: false,
                adminApproved: true,
                showIcon: true,
                showIcon1: true,
              });
            }
          }
        } else {
          this.setState({
            idCard: imageId,
            idHoldingCard: holdImageId,
            disable: true,
            loading: false,
          });
        }
      })
      .catch((error) => {
        Sentry.captureException(error);
        alert(error);
      });
  }

  onSelectImage(response) {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };
      const ImageSource = {
        uri: response.uri,
        type: 'image/jpeg' || 'image/png' || 'image/jpg',
        name: 'photo_id.jpg',
      };
      if (this.state.uploadType === 'id_card') {
        this.setState({
          // idCard: source,
          showModal: false,
          status: translate('uploading'),
          percentage: 10,
          percentageSign: '%',
          uploading: true,
          idName: response.fileName,
        });
        LocalData.setfileData({ idCardImage: response.fileName });
        this.uploadUserImage('ID', ImageSource, this.state.userId, source);
      } else {
        this.setState({
          // idHoldingCard: source,
          showModal: false,
          status1: translate('uploading'),
          percentage1: 10,
          percentageSign1: '%',
          idUploading: true,
          idHoldName: response.fileName,
        });
        LocalData.setfile1Data({ idHoldImage: response.fileName });
        this.uploadUserImage('HOLD_ID', ImageSource, this.state.userId, source);
      }
    }
  }

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      this.onSelectImage(response);
    });
  }

  submitImageVerification() {
    if (this.state.info === true) {
      LocalData.setVerifyData({ photo: true });
      this.state.props.navigate('MyProfile');
    } else {
      this.state.props.navigate('VerifyPhoto');
    }
  }

  launchImageLibrary() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.fileSize > 2000000) {
        Alert.alert("Error", translate('file_size_large'), [
          {
            text: "OK",
          }
        ]);
      } else if (response.fileSize < 44000) {
        Alert.alert("Error", translate('file_size_small'), [
          {
            text: "OK",
          }
        ]);
      } else {
        this.onSelectImage(response);
      }
    });
  }
  uploadHoldIdCard() {
    if (this.state.uploaded === false) {
      Alert.alert("Error", translate('upload_card_first'), [
        {
          text: "OK",
        }
      ]);
    } else {
      this.setState({ showModal: true, uploadType: 'id_holding_card' });
    }
  }

  render() {
    let title = translate('upload_holding_idCard');
    if (this.state.uploadType === 'id_card') {
      title = translate('upload_idCard');
    }
    if (this.state.loading === true) {
      return (
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: height / 3 }}>
              <ActivityIndicator />
            </View>
      );
    }
    return (
        <ScrollView style={styles.rootStyle}>
          <Modal
              animationType={'fade'}
              transparent={true}
              visible={this.state.showModal}
              onRequestClose={() => {
                this.setState({
                  showModal: false,
                });
              }}>
            <View style={styles.modalStyle}>
              <TouchableOpacity style={{ flex: 1 }}
                                onPress={() => {
                                  this.setState({
                                    showModal: false,
                                  });
                                }}
              />
              <View style={styles.modalBoxStyle}>
                <Text style={styles.modalTextStyle}>{title}</Text>
                <View style={styles.modalBoxWrapperStyle}>
                  <TouchableOpacity style={styles.modalButtonWrapperStyle} onPress={() => {
                    this.launchImageLibrary();
                  }}>
                    <View style={styles.circleButtonStyle}>
                      <Image source={require('../../../images/profile/icon_gallery.png')}
                             style={{
                               width: 30, height: 30, resizeMode: 'contain', tintColor: '#fff',
                             }}/>
                    </View>
                    <Text style={styles.textButtonStyle}>{translate('gallery')}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.modalButtonWrapperStyle} onPress={() => {
                    this.launchCamera();
                  }}>
                    <View style={styles.circleButtonStyle}>
                      <Image source={require('../../../images/profile/icon_camera.png')}
                             style={{
                               width: 30, height: 30, resizeMode: 'contain', tintColor: '#fff',
                             }}/>
                    </View>
                    <Text style={styles.textButtonStyle}>{translate('camera')}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.modalButtonWrapperStyle} onPress={() => {
                    this.launchCamera();
                  }}>
                    <View style={styles.circleButtonStyle}>
                      <Image source={require('../../../images/profile/icon_qrscan.png')}
                             style={{
                               width: 30, height: 30, resizeMode: 'contain', tintColor: '#fff',
                             }}/>
                    </View>
                    <Text style={styles.textButtonStyle}>{translate('qr_scan')}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <View style={{
            marginTop: width / 14,
            flexDirection: 'row',
            alignSelf: 'center',
          }}>
            <View style={{
              right: 5,
              width: width / 2.3,
              alignItems: 'center',
            }}>
              { this.state.showIcon === true ?
                  <TouchableOpacity
                      style={{ zIndex: 1 }}
                      onPress={() => this.setState({ showModal: true, uploadType: 'id_card' }) }
                  >
                    <Image source={this.state.plus}
                           style={{
                             width: 30,
                             height: 30,
                             resizeMode: 'contain',
                             marginLeft: 80,
                           }}/>
                  </TouchableOpacity>
                  : <View style={{ height: 30 }}/> }
              <ImageBackground resizeMode={'contain'}
                               style={{ bottom: 15 }}
                               source={require('../../../images/Doted.png')}>
                {this.state.adminApproved === false ?
                <View style={styles.idCardContainer}>
                  <Image source={this.state.idCard}
                         style={{
                           width: 50, height: 50, resizeMode: 'contain',
                         }}/>
                  <Text
                      style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 9,
                        width: 80,
                      }}
                      ellipsizeMode='tail'
                      numberOfLines={1}
                  >{this.state.uploading === false ? translate('upload_idCard') : this.state.idName }</Text>
                  { this.state.uploading === true && (
                      <Text
                          style={{
                            fontSize: 10,
                            alignSelf: 'flex-start',
                            marginLeft: 16,
                            fontFamily: FontStyle.Light,
                          }}>
                        {this.state.status}{this.state.percentage}{this.state.percentageSign}
                      </Text>
                  )}
                  { this.state.uploading === true &&
                  <ProgressBarAndroid
                      styleAttr="Horizontal"
                      indeterminate={false}
                      progress={this.state.progressBar}
                      style={{ width: 120 }}
                      color={Config.primaryColor}
                  />
                  }
                </View>
                    :
                    <View style={styles.idCardContainer}>
                      <View style={{ height: 50, width: 50 }}>
                      <Image source={this.state.idCard}
                             style={{
                               width: 50, height: 50, top: 0,
                             }}
                             resizeMode="stretch"
                      />
                      </View>
                             <View
                                 style={{
                                   left: 4,
                                   bottom: 3,
                                 }}>
                      <Text
                          style={{
                            fontFamily: FontStyle.Regular,
                            fontSize: 9,
                            width: 80,
                            top: 2,
                          }}
                          ellipsizeMode='tail'
                          numberOfLines={1}
                      >
                        {this.state.idImageName}
                      </Text>
                             </View>
                      <View
                          style={{
                            backgroundColor: this.state.notApproved ? Config.textColor : Config.primaryColor,
                            alignSelf: 'flex-start',
                            marginLeft: 20,
                            width: width / 3,
                            height: 17,
                            borderRadius: 5,
                          }}
                      >
                        {this.state.approved === false ?
                            <Text
                                style={{
                                  fontSize: 11,
                                  color: Config.white,
                                  textAlign: 'center',
                                  top: 2,
                                  fontFamily: FontStyle.Regular,
                                }}>
                              {translate('wait_for_approval')}
                            </Text>
                            :
                            <View>
                              {this.state.notApproved === false ?
                                  <Text
                                      style={{
                                        fontSize: 9,
                                        color: Config.white,
                                        textAlign: 'center',
                                        top: 2,
                                        fontFamily: FontStyle.Regular,
                                      }}>{translate('approved_by_admin')}{' '}
                                    <AntDesign name="checkcircleo" size={8} color={Config.white}/>
                                  </Text>
                                  : <Text
                                      style={{
                                        fontSize: 9,
                                        color: Config.white,
                                        textAlign: 'center',
                                        top: 2.5,
                                        fontFamily: FontStyle.Regular,
                                      }}>{translate('reject_by_admin')}</Text> }
                            </View>
                        }
                      </View>
                    </View>
                }
              </ImageBackground>
            </View>
            <View style={{
              width: width / 2.3,
              alignItems: 'center',
            }}>
              { this.state.showIcon1 === true ?
                  <TouchableOpacity
                      style={{ zIndex: 1 }}
                      onPress={() => this.uploadHoldIdCard() }
                  >
                    <Image source={this.state.plus}
                           style={{
                             width: 30, height: 30, resizeMode: 'contain', marginLeft: 80,
                           }}/>
                  </TouchableOpacity>
                  : <View style={{ height: 30 }}/>}
              <ImageBackground resizeMode={'contain'}
                               style={{ bottom: 15 }}
                               source={require('../../../images/Doted.png')}>
                {this.state.adminApproved === false ?
                    <View style={styles.idCardContainer}>
                      <Image source={this.state.idHoldingCard}
                             style={{
                               width: 50, height: 50, resizeMode: 'contain',
                             }}/>
                      <Text
                          style={{
                            fontFamily: FontStyle.Light,
                            fontSize: 9,
                            width: 80,
                          }}
                          ellipsizeMode='tail'
                          numberOfLines={1}
                      >{this.state.idUploading === false ? translate('upload_holding_idCard') : this.state.idHoldName}</Text>
                      {this.state.idUploading === true && (
                          <Text
                              style={{
                                fontSize: 12,
                                alignSelf: 'flex-start',
                                marginLeft: 16,
                                fontFamily: FontStyle.Light,
                              }}>
                            {this.state.status1}{this.state.percentage1}{this.state.percentageSign1}
                          </Text>
                      )}
                      {this.state.idUploading === true && (
                          <ProgressBarAndroid
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={this.state.progressBar1}
                              style={{ width: 120 }}
                              color={Config.primaryColor}
                          />
                      )}
                    </View>
                    :
                    <View style={styles.idCardContainer}>
                      <View style={{ height: 50, width: 50 }}>
                      <Image source={this.state.idHoldingCard}
                             style={{
                               width: 50, height: 50, top: 0,
                             }}
                             resizeMode="stretch"
                      />
                      </View>
                      <View
                          style={{
                            left: 4,
                            bottom: 3,
                          }}>
                        <Text
                            style={{
                              fontFamily: FontStyle.Regular,
                              fontSize: 9,
                              top: 2,
                              width: 80,
                            }}
                            ellipsizeMode='tail'
                            numberOfLines={1}
                        >
                          {this.state.holdImageName}
                        </Text>
                      </View>
                      <View
                          style={{
                            backgroundColor: this.state.notApproved ? Config.textColor : Config.primaryColor,
                            alignSelf: 'flex-start',
                            marginLeft: 20,
                            width: width / 3,
                            height: 17,
                            borderRadius: 5,
                          }}
                      >
                        {this.state.approved === false ?
                            <Text
                                style={{
                                  fontSize: 11,
                                  color: Config.white,
                                  textAlign: 'center',
                                  top: 2,
                                  fontFamily: FontStyle.Regular,
                                }}>
                              {translate('wait_for_approval')}
                            </Text>
                            :
                            <View>
                              {this.state.notApproved === false ?
                                  <Text
                                      style={{
                                        fontSize: 9,
                                        color: Config.white,
                                        textAlign: 'center',
                                        top: 2,
                                        fontFamily: FontStyle.Regular,
                                      }}>{translate('approved_by_admin')}{' '}
                              <AntDesign name="checkcircleo" size={8} color={Config.white}/>
                                  </Text>
                                : <Text
                                      style={{
                                        fontSize: 9,
                                        color: Config.white,
                                        textAlign: 'center',
                                        top: 2.5,
                                        fontFamily: FontStyle.Regular,
                                      }}>{translate('reject_by_admin')}</Text> }
                            </View>
                        }
                      </View>
                    </View>
                }
              </ImageBackground>
            </View>
          </View>
          <View style={styles.viewColumn}>
            <Text style={{ fontFamily: FontStyle.Bold }}>
              {translate('documents_guidelines')}
            </Text>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_1')}{'\n'}
                {translate('requirements_line_2')}{'\n'}
                {translate('requirements_line_3')}
              </Text>
            </View>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_4')}{'\n'}
                {translate('requirements_line_5')}{'\n'}
                {translate('requirements_line_6')}
              </Text>
            </View>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_7')}
              </Text>
            </View>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_8')}{'\n'}
                {translate('requirements_line_9')}
              </Text>
            </View>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_10')}{'\n'}
                (*.pdf,*jpg,*png).
              </Text>
            </View>
            <View style={styles.viewRow}>
              <Entypo name="check" size={15} color={Config.primaryColor}/>
              <Text style={styles.textClass}>
                {translate('requirements_line_11')}
              </Text>
            </View>
          </View>
          <TouchableOpacity
              disabled={this.state.disable}
              onPress={() => this.submitImageVerification()}
              style={{
                alignSelf: 'center',
                backgroundColor: this.state.disable ? Config.textColor : Config.primaryColor,
                width: width / 2,
                height: 30,
                borderRadius: 5,
                marginTop: 30,
              }}>
            <Text style={styles.buttonStyle}>
              {translate('continue')}
            </Text>
          </TouchableOpacity>
        </ScrollView>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: Config.white,
    flex: 1,
  },
  textClass: {
    fontFamily: FontStyle.Light,
    fontSize: 12,
  },
  viewRow: {
    flexDirection: 'row',
    marginTop: 8,
  },
  viewColumn: {
    marginTop: 20,
    flexDirection: 'column',
    left: 20,
    width: width - 25,
  },
  idCardContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width / 2.3,
    height: width / 3.5,
  },
  modalStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  modalBoxStyle: {
    backgroundColor: '#fff',
    height: 220,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  modalTextStyle: {
    fontSize: 20,
    color: '#000',
  },
  buttonStyle: {
    alignSelf: 'center',
    top: 6,
    fontFamily: FontStyle.Regular,
    color: Config.white,
  },
  modalBoxWrapperStyle: {
    flexDirection: 'row',
    marginTop: 30,
  },
  modalButtonWrapperStyle: {
    flexDirection: 'column',
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleButtonStyle: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  textButtonStyle: {
    fontSize: 12,
  },
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: height / 30,
    width: '95%',
    height: '10%',
    borderRadius: 5,
    backgroundColor: Config.primaryColor,
  },
  header: {
    width: '100%',
    height: '10%',
    borderRadius: 2,
    backgroundColor: Config.primaryColor,
  },
  status: {
    color: '#fff',
    marginTop: 10,
    marginLeft: 15,
    fontSize: 20,
  },
};

// function mapStateToProps(state) {
//     return {
//         component: state.component,
//     }
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         setRoot: (root) => dispatch({
//             type: 'set_root',
//             root: root
//         })
//     }
// }

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(Tab2);

export default Tab2;
