import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    DatePickerAndroid,
    DatePickerIOS,
    ActivityIndicator,
    Dimensions,
    Alert, Platform, Modal,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../../Config';
import Input from './Input';
import Select2 from './Select2';
import CustomInput from './CustomInput';
import RadioButton from './RadioButton';
import countryJsonData from './country.json';
import LocalData from '../../components/LocalData';
import { translate } from '../../i18n';
import HttpRequest from '../../components/HttpRequest';
import Feather from "react-native-vector-icons/Feather";
import FontStyle from "../../constants/FontStyle";

const { height, width } = Dimensions.get('window');
class Tab1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      real_name: '',
      gender: '',
      genderData: [
        {
          id: '0',
          text: translate('male'),
        },
        {
          id: '1',
          text: translate('female'),
        },
      ],
      country: '',
      countryList: countryJsonData,
      handphone: '',
      idCard: '',
      address: '',
      birthDate: null,
      token: null,
      loading: true,
      disable: false,
      approved: false,
      disapproved: false,
      textLoading: false,
      verifying: false,
      iosBirthDay: new Date(),
      datePickerModelIos: false,
    };
    this.root = this.props.component.root;
  }
  componentDidMount() {
    LocalData.getUserData().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ token: resp.token });
      this.getUserInfo(this.state.token);
    });
  }

  getUserInfo(access_token) {
    HttpRequest.getUserImage(access_token)
      .then((response) => {
        if (response.data.status === 'success') {
          if (response.data.data === null) {
            this.setState({ loading: false });
            LocalData.setVerifyData({ Information: false });
          } else {
            this.setState({
              real_name: response.data.data.real_name,
              idCard: response.data.data.id_card_no,
              birthDate: response.data.data.date_of_birth,
              address: response.data.data.address,
              gender: response.data.data.gender,
              handphone: response.data.data.handphone_no,
              country: response.data.data.country_id === null ? '' : response.data.data.country_id,
            });
            const status = response.data.data.status;
            if (status === 0 || status === null) {
              this.setState({ disable: true, verifying: true });
            } else if (status === 1) {
              this.setState({ disable: true, approved: true });
            } else if (status === 2) {
              LocalData.setVerifyData({ Information: false });
              this.setState({ disapproved: true });
            }
            this.setState({ loading: false });
          }
        } else {
          this.setState({ loading: false });
        }
      })
      .catch((error) => {
        this.setState({ loading: false });
        alert(error);
      });
  }
  submitForm() {
    this.setState({ textLoading: true });
    if (
      this.state.realName === '' ||
      this.state.idCard === '' ||
      this.state.birthDate === '' ||
      this.state.address === '' ||
      this.state.gender === '' ||
      this.state.handphone === ''
    ) {
        Alert.alert("Error", "Please fill Full form", [
            {
                text: "OK",
            }
        ]);
    } else {
      const data = {
        real_name: this.state.real_name,
        id_card: this.state.idCard,
        dob: this.state.birthDate,
        address: this.state.address,
        gender: this.state.gender,
        hand_phone: this.state.handphone,
        country_id: this.state.country,
      };
      HttpRequest.submitUserIdentity(this.state.token, data)
        .then((response) => {
          if (response.data.status === 'success') {
            LocalData.getVerifyData().then((res) => {
              const resp = JSON.parse(res);
              if (resp !== null) {
                if (resp.photo === undefined) {
                  LocalData.setVerifyData(null);
                  this.props.navigation.navigate('MyProfile', { activeTab: 1 });
                } else {
                  this.props.navigation.navigate('VerifyPhoto');
                }
              } else {
                this.props.navigation.navigate('MyProfile', { activeTab: 1 });
              }
            });
          } else {
              Alert.alert("Error", response.data.message, [
                  {
                      text: "OK",
                  }
              ]);
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  }

  showDatepicker() {
    if (Platform.OS === 'android') {
        if (this.state.birthDate === null || this.state.birthDate === 'null') {
            const today = new Date();
            const d = today.getDate();
            const m = today.getMonth();
            const y = today.getFullYear();
            DatePickerAndroid.open({
                date: new Date(y, m, d),
            })
                .then((result) => {
                    const {
                        action, year, month, day,
                    } = result;
                    if (action !== DatePickerAndroid.dismissedAction) {
                        // Selected year, month (0-11), day
                        const selectedDate = `${year}-${month + 1}-${day}`;
                        this.setState({birthDate: selectedDate});
                    }
                })
                .catch(() => {
                });
        } else {
            const array = this.state.birthDate.split('-');
            DatePickerAndroid.open({
                date: new Date(
                    parseInt(array[0]),
                    parseInt(array[1]) - 1,
                    parseInt(array[2]),
                ),
            })
                .then((result) => {
                    const {
                        action, year, month, day,
                    } = result;
                    if (action !== DatePickerAndroid.dismissedAction) {
                        // Selected year, month (0-11), day
                        const selectedDate = `${year}-${month + 1}-${day}`;
                        this.setState({birthDate: selectedDate});
                    }
                })
                .catch(() => {
                });
        }
    } else {
        this.setState({ datePickerModelIos: true });
    }
  }

  setDate = (date) => {
     this.setState({ iosBirthDay: date });
      const year = date.getFullYear();
      const month = date.getMonth();
      const day = date.getDate();
      const selectedDate = `${year}-${month + 1}-${day}`;
      this.setState({birthDate: selectedDate});
  };

  render() {
      const birthDate = (
          <TouchableOpacity
              onPress={() => {
                  this.showDatepicker();
              }}
              style={{
                  paddingVertical: 10,
                  flexDirection: 'row',
                  alignItems: 'center',
                  width: '100%',
              }}
          >
              <Text style={{flex: 1}}>{this.state.birthDate}</Text>
              <Ionicons name="md-calendar" color="#aaaaaa" size={20}/>
          </TouchableOpacity>
      );
    const select2 = (
      <Select2
        parentStyle={{ width: '100%' }}
        dataSource={this.state.countryList}
        renderId={item => item.code}
        renderRow={item => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 10,
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1 }}>{item.name}</Text>
          </View>
        )}
        renderDisplay={item => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1 }}>{item.name}</Text>
            <Ionicons name="md-arrow-dropdown" color="#000" />
          </View>
        )}
        renderEmpty={() => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1 }}>-</Text>
            <Ionicons name="md-arrow-dropdown" color="#000" />
          </View>
        )}
        renderLabel={item => item.name}
        onSelect={(item) => {
          this.setState({ country: item.code });
        }}
        cancelText={translate('cancel')}
        selectedValue={this.state.country}
      />
    );
    if (this.state.loading === true) {
      return (
              <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: height / 2.5 }}>
                  <ActivityIndicator />
              </View>
      );
    } else {
      return (
            <ScrollView style={styles.rootStyle}>
                <Modal
                    onRequestClose={() => this.setState({ datePickerModelIos: false })}
                    animationType="fade"
                    transparent={true}
                    visible={this.state.datePickerModelIos}
                >
                    <TouchableOpacity
                        style={styles.modalRootStyle}
                        onPress={() => {
                            this.setState({ datePickerModelIos: false });
                        }}
                    >
                        <View style={styles.modalBoxStyle}>
                            <TouchableOpacity
                                onPress={() => this.setState({ datePickerModelIos: false }) }
                                style={{ alignSelf: 'flex-end', margin: 10, height: 28, width: 28 }}
                            >
                                <Feather name="x" size={28} color={Config.primaryColor}/>
                            </TouchableOpacity>
                            <DatePickerIOS
                                date={this.state.iosBirthDay}
                                onDateChange={this.setDate}
                                mode={'date'}
                            />
                            <TouchableOpacity style={styles.submitInfo}
                                              onPress={() => this.setState({ datePickerModelIos:false }) }
                            >
                                    <Text
                                        style={{
                                            color: Config.white,
                                            fontFamily: FontStyle.Regular,
                                        }}
                                    >{translate('submit')}</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableOpacity>
                </Modal>
                <Input
                    title={translate('REAL_NAME')}
                    placeholder={translate('enter_your_name')}
                    value={this.state.real_name}
                    onChangeText={(real_name) => {
                        this.setState({ real_name });
                    }}
                />

                <Input
                    title={translate('ID_CARD_NO')}
                    placeholder={translate('enter_your_id_number')}
                    value={this.state.idCard}
                    onChangeText={(idCard) => {
                        this.setState({ idCard });
                    }}
                />

                <CustomInput title={translate('BIRTH_DATE')} content={birthDate}/>

                <Input
                    title={translate('ADDRESS')}
                    placeholder={translate('enter_your_current_address')}
                    value={this.state.address}
                    onChangeText={(address) => {
                        this.setState({ address });
                    }}
                />

                <RadioButton
                    title={translate('GENDER')}
                    value={this.state.gender}
                    radioData={this.state.genderData}
                    onSelect={(gender) => {
                        this.setState({ gender });
                    }}
                />

                <Input
                    title={translate('HANDPHONE')}
                    placeholder={translate('enter_your_handphopne_number')}
                    value={this.state.handphone}
                    onChangeText={(handphone) => {
                        this.setState({ handphone });
                    }}
                />

                <CustomInput title={translate('COUNTRY')} content={select2}/>

                <TouchableOpacity
                    disabled={this.state.disable}
                    style={{
                        height: 35,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: Config.primaryColor,
                        borderRadius: 5,
                        marginHorizontal: 10,
                        marginVertical: 10,
                    }}
                    onPress={() => {
                        this.submitForm();
                    }}
                >
                    {this.state.textLoading === true && (
                        <ActivityIndicator color={'#ffffff'} />
                    )}
                    {this.state.textLoading === false && (
                        <Text style={{ color: '#fff' }}>
                            {this.state.approved === false ? this.state.disapproved === true ? translate('reSubmit') : this.state.verifying === false ? translate('submit') : translate('verifying') : translate('approved') }
                        </Text>
                    )}
                </TouchableOpacity>
            </ScrollView>
      );
    }
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  button: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Config.primaryColor,
    borderRadius: 5,
    marginHorizontal: 10,
    marginVertical: 10,
  },

  modalRootStyle: {
    flex: 1,
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    alignItems: 'center',
    justifyContent: "center",
  },

  modalBoxStyle: {
    width: width - 60,
    height: width - 20,
    backgroundColor: Config.white,
    borderRadius: 5,
  },
  submitInfo: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    width: width / 1.3,
    left: 12,
    marginTop: 13,
    borderRadius: 3,
    backgroundColor: Config.primaryColor,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userInfo: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(Tab1));
