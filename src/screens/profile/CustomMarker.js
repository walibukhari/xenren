import React from 'react';
import { StyleSheet, Image } from 'react-native';

class CustomMarker extends React.Component {
    render() {
        return (
            <Image
                style={styles.image}
                source={
                    require('../../../images/roundedcircle.png')
                }
                resizeMode="contain"
            />
        );
    }
}

const styles = StyleSheet.create({
    image: {
        height: 40,
        width: 12,
    },
});

export default CustomMarker;