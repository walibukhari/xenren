import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
  Text,
  TextInput,
  Image,
  StyleSheet
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import HttpRequest from "../../../components/HttpRequest";
import FontStyle from "../../../constants/FontStyle";
import style from "./style";
import Color from "../../../Config";
import CameraPopUp from "../../verifyId/CameraPopUp";

export default class AddInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      array: [
        { name: "Designer", isSelect: false },
        { name: "Software Engineer", isSelect: false },
        { name: "Investor", isSelect: false },
        { name: "Product Manager", isSelect: false },
        { name: "Product Executive", isSelect: false }
      ],
      isShow: false,
      cameraModal: false,
      userImage: ""
    };
  }

  getImage = (data, value) => {
    this.setState({ cameraModal: value });
  };

  openCameraModal = () => {
    console.log("show camera pop up");
    this.setState({ cameraModal: true });
  };

  selectProfession = (array, value) => {
    array.map((item, index) => {
      index === value ? (item.isSelect = true) : (item.isSelect = false);
    });
    this.setState({ array });
  };

  renderProfession = () => {
    const { array } = this.state;

    return array.map((item, index) => {
      return (
        <View key={index} style={{ marginHorizontal: 3 }} key={index}>
          {item.isSelect ? (
            <TouchableOpacity
              onPress={() => this.selectProfession(array, index)}
              style={[
                style.professionButton,
                { backgroundColor: Color.primaryColor }
              ]}
            >
              <Text style={{ paddingHorizontal: 15, color: "white" }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => this.selectProfession(array, index)}
              style={[style.professionButton, { backgroundColor: "#ececec" }]}
            >
              <Text style={{ paddingHorizontal: 15, color: "grey" }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      );
    });
  };

  render() {
    const { userImage } = this.state;
    return (
      <Modal
        visible={this.state.isShow}
        animationType="fade"
        transparent={true}
        onRequestClose={() => {
          this.setState({
            isShow: false
          });
        }}
      >
        <ScrollView style={style.scrollStyle}>
          <View style={style.modalContainer}>
            <View style={style.closeContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ isShow: false });
                }}
              >
                <Image
                  style={{ width: 25, height: 25 }}
                  resizeMode="contain"
                  source={
                    userImage
                      ? userImage
                      : require("../../../../images/facebook/cross.png")
                  }
                />
              </TouchableOpacity>
            </View>
            <View style={style.headerContainer}>
              <Text
                style={[style.headerText, { fontSize: 23, color: "black" }]}
              >
                Add Your Info
              </Text>
              <Text
                style={[
                  style.headerText,
                  { fontSize: 17, color: Color.textColor }
                ]}
              >
                Please add your correct information
              </Text>
            </View>

            <Image
              style={style.userImage}
              resizeMode="contain"
              source={require("../../../../images/profile/defaultUser.png")}
            />
            <TouchableOpacity
              onPress={() => this.openCameraModal()}
              style={style.uploadButton}
            >
              <Text
                style={[
                  style.headerText,
                  { fontSize: 15, color: Color.primaryColor }
                ]}
              >
                Upload
              </Text>
            </TouchableOpacity>
            <View style={style.nameBoxStyle}>
              <View style={style.addNameContainer}>
                <Text style={style.nameLabelStyle}>YOUR NAME</Text>
              </View>
              <View style={style.textInputContainer}>
                <TextInput
                  style={style.textInputStyle}
                  underlineColorAndroid="transparent"
                />
              </View>

              <Image
                style={style.userImage}
                resizeMode="contain"
                source={require("../../../../images/profile/defaultUser.png")}
              />
              <TouchableOpacity
                onPress={() => this.openCameraModal()}
                style={style.uploadButton}
              >
                <Text
                  style={[
                    style.headerText,
                    { fontSize: 15, color: Color.primaryColor }
                  ]}
                >
                  Upload
                </Text>
              </TouchableOpacity>
              <View style={style.nameBoxStyle}>
                <View style={style.addNameContainer}>
                  <Text style={style.nameLabelStyle}>YOUR NAME</Text>
                </View>
                <View style={style.textInputContainer}>
                  <TextInput
                    style={style.textInputStyle}
                    underlineColorAndroid="transparent"
                  />
                </View>

                <TouchableOpacity style={style.submitButton}>
                  <Text style={style.submitTextStyle}>SUBMIT</Text>
                </TouchableOpacity>
              </View>

              <TouchableOpacity style={style.submitButton}>
                <Text style={style.submitTextStyle}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <CameraPopUp
          showModal={this.state.cameraModal}
          callback={this.getImage}
        />
      </Modal>
    );
  }
}
