import { StyleSheet, Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('window').width
import FontStyle from "../../../constants/FontStyle";
import Color from '../../../Config';

export default StyleSheet.create({

    scrollStyle: {
        backgroundColor: "rgba(0,0,0,0.5)",
        flex: 1,
        flexDirection: "column",
        paddingHorizontal: 10,
        paddingTop: "5%"
      },
    modalContainer: {
        borderRadius: 4,
        borderColor: "#fff",
        borderWidth: 1,
        backgroundColor: "#fff",
        paddingHorizontal: 10,
        paddingVertical: 10,
        alignItems:'center'
      },
    closeContainer:{
        width:'100%',
        justifyContent:'center',
        alignItems:'flex-end',
        padding:3,
    },
    headerContainer:{
       width:'100%',
       justifyContent:'center',
       alignItems:'center',
       marginBottom: 15 
    },
    headerText: {
        paddingVertical:5,
        fontFamily: FontStyle.Regular,
    },
    userImage: { 
        marginVertical:5, 
        height:120, 
        width:120
    },
    uploadButton: { 
        marginTop:10, 
        justifyContent:'center', 
        alignItems:'center', 
        height:35, 
        width:130, 
        borderColor: Color.primaryColor, 
        borderWidth:2, 
        borderRadius:17 
    },
    nameBoxStyle: {
        width:'100%', 
        marginVertical:15, 
        borderWidth:1, 
        borderColor:'#efefef', 
        borderRadius:3
    },
    addNameContainer: {
        width:'100%',
        alignItems:'flex-start',
        justifyContent:'center',
        height:25,
        backgroundColor: '#ececec'
    },
    nameLabelStyle: {
        paddingHorizontal:10, 
        fontSize:12, 
        fontFamily: FontStyle.Regular,  
        color:'grey'
    },
    textInputContainer: {
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
    },
    textInputStyle: {
        width:'100%',
        alignItems:'flex-start',
        justifyContent:'center',
        height:40,
        paddingHorizontal:10
    },
    professionView: { 
        width:'100%', 
        flexDirection:'row', 
        flexWrap:'wrap', 
        justifyContent:'center', 
        marginVertical:10 
    },
    professionButton: {
        height:35, 
        borderRadius:16, 
        marginTop:5, 
        justifyContent:'center'
    },
    submitButton: {
        width:'100%',
        height:50,
        backgroundColor: Color.primaryColor,
        borderRadius:3,
        justifyContent:'center',
        alignItems:'center',
        marginVertical: 10
    },
    submitTextStyle: {
        color:'white', 
        fontSize:15,
        fontFamily: FontStyle.Regular
    }

})
