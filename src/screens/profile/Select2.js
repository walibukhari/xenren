import React, { Component } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import _ from 'lodash';
import axios from 'axios';
import {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  ListView,
  Modal,
  TextInput,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
// eslint-disable-next-line import/no-unresolved
import { translate } from '../../i18n';

class Select2 extends Component {
  static propTypes = {
    parentStyle: PropTypes.object,
    style: PropTypes.object,
    dataSource: PropTypes.object,
    renderId: PropTypes.func,
    renderRow: PropTypes.func,
    renderDisplay: PropTypes.func,
    renderEmpty: PropTypes.func,
    renderLabel: PropTypes.func,
    onSelect: PropTypes.func,
    cancelText: PropTypes.string,
    selectedValue: PropTypes.string,
    allowRemote: PropTypes.bool,
    renderUrl: PropTypes.func,
    renderHttpResponse: PropTypes.func,
    navigation: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
    balance: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    const { allowRemote = allowRemote != null ? allowRemote : false, dataSource } = this.props;

    this.state = {
      show: false,
      textFilter: '',
      allowRemote,
      realData: allowRemote ? [] : dataSource,
      displayData: allowRemote ? [] : dataSource,
      selectedItem: allowRemote ? null : this.filterByValue(dataSource),
    };
  }

  filter(teks) {
    if (this.state.allowRemote) {
      const { renderUrl, renderHttpResponse } = this.props;

      axios.get(renderUrl(teks)).then((result) => {
        this.setState({
          displayData: renderHttpResponse(result),
        });
      });
    } else {
      const pattern = new RegExp(teks, 'i');
      const displayData = _.filter(this.state.realData, (datum) => {
        let isFound = false;
        Object.keys(datum).forEach((key) => {
          const obj = datum[key];
          if (obj != null && typeof obj === 'string' && obj.search(pattern) !== -1) {
            isFound = true;
          }
        });

        if (isFound) {
          return datum;
        }

        return {};
      });

      this.setState({ displayData });
    }
  }

  filterByValue(data) {
    const { selectedValue, renderId } = this.props;

    if (selectedValue === null || selectedValue === undefined) {
      return data[0];
    }

    let realData = null;
    data.forEach((item) => {
      if (renderId(item) === selectedValue) {
        realData = item;
      }
    });
    return realData;
  }

  selectRow(data) {
    this.props.onSelect(data);
    this.setState({
      show: false,
      selectedItem: data,
    });
  }

  render() {
    const {
      cancelText = cancelText || 'Cancel',
      parentStyle,
      style,
      renderDisplay,
      renderEmpty,
      renderRow,
    } = this.props;
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    const dataSource = ds.cloneWithRows(this.state.displayData);

    return (
      <View style={parentStyle}>
        <TouchableOpacity style={[styles.containerStyle, style]}
          onPress={() => { this.setState({ show: true }); }}>
          {this.state.selectedItem != null && renderDisplay(this.state.selectedItem)}
          {this.state.selectedItem == null && renderEmpty()}
        </TouchableOpacity>

        <Modal
          animationType='fade'
          onRequestClose={() => {
            this.setState({ show: false });
          }}
          transparent={true}
          visible={this.state.show}
        >
          <View style={styles.modalContainerStyle}>
            <View style={styles.modalBoxStyle}>
              <View style={styles.filterTopStyle}>
                <FontAwesome5Icon name={'search'}/>
                <TextInput
                  style={styles.filterTopTextStyle}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  onChangeText={(textFilter) => {
                    this.setState({ textFilter });
                    this.filter(textFilter);
                  }}
                  autoFocus={true}
                  placeholder={translate('search')}
                  value={this.state.textFilter}
                  autoCapitalize='none'
                  autoCorrect={false}
                />
              </View>
              <View style={{ flex: 1 }}>
                <ListView
                  dataSource={dataSource}
                  enableEmptySections={true}
                  renderRow={(rowData) => {
                    return (
                      <TouchableOpacity
                        style={{ flex: 1 }}
                        onPress={() => { this.selectRow(rowData); }}>

                        {renderRow(rowData)}

                      </TouchableOpacity>
                    );
                  }}
                />
              </View>
              <View style={styles.bottomButtonStyle}>
                <TouchableHighlight
                  style={styles.btnTextStyle}
                  onPress={() => { this.setState({ show: false }); }}>
                  <Text style={styles.textStyle}>{cancelText}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 3,
    flex: 1,
  },
  modalContainerStyle: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBoxStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    paddingTop: getStatusBarHeight(true),
    paddingBottom: 30,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
    borderRadius: 5,
  },
  filterTopStyle: {
    height: 40,
    paddingVertical: 0,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
    borderRadius: 7,
  },
  filterTopTextStyle: {
    flex: 1,
    color: '#000',
    backgroundColor: 'transparent',
    paddingHorizontal: 5,
    marginLeft: 5,
  },
  bottomButtonStyle: {
    height: 50,
    flexDirection: 'row',
    padding: 5,
  },
  btnTextStyle: {
    height: 40,
    flex: 1,
    backgroundColor: '#95a5a6',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  textStyle: {
    fontSize: 15,
    color: '#ffffff',
  },
};

export default Select2;
