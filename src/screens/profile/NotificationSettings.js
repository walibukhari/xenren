import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
    Text,
    View,
    Image,
    Switch,
    TouchableOpacity,
    ScrollView, TextInput, Alert, Keyboard, Dimensions, KeyboardAvoidingView,
} from 'react-native';
import Config from '../../Config';
import {translate} from '../../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import CustomMarker from './CustomMarker';
import _ from "lodash";
import MultiSelect from 'react-native-multiple-select';

import HttpRequest from "../../components/HttpRequest";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import KeyEvent from "react-native-keyevent";
import MultiSelectModal from "../../components/MultiSelectModal";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const {width, height} = Dimensions.get("window");

class NotificationSettings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            autocompleteText: "",
            autocompleteLanguageText: "",
            autocompletePosition: {
                x: 0,
                y: -200,
                width: 100,
            },
            atleastSpentLow: 10,
            atleastSpentHigh: 850,
            filteredSkills: [],
            fixedPriceLow: 3000,
            fixedPriceHigh: 6000,
            hourlyPriceLow: 10,
            hourlyPriceHigh: 850,
            locale: 'env',
            languages: [],
            skills: [],
            switchValue: false,
            selectedSkills: [],
            selectedLanguages: [],
            modalVisible: false,
            countries: [],
            selectedCountryItems: [],
            countryType: 1,
            countryTypeOriginal: 1,
            selectedCountries: [],
            active: true
        };
    }

    static navigationOptions = ({navigation: {navigate}}) => ({
        title: 'ABC'
    });

    static propTypes = {
        navigation: PropTypes.object.isRequired,
    };

    componentDidMount() {
        this.getNotificationSettings();
        this.getSkillsList();
        this.getCountries();
        this.getLanguages();
        KeyEvent.onKeyDownListener((keyCode) => {
            if (keyCode === 67 && this.state.autocompleteText === "" || this.state.searchLanguage === "") {
                let selectedSkills = this.state.selectedSkills;
                if (selectedSkills.length !== 0) {
                    selectedSkills.splice(selectedSkills.length - 1, 1);
                    this.setState({selectedSkills});
                }
            }
        });
    };

    componentDidUpdate() {
    };

    getCountries() {
        HttpRequest.getOnlyCountries()
            .then((response) => {
                this.setState({
                    countries: response.data,
                });
            })
            .catch(error => {
            });
    };

    getNotificationSettings() {
        HttpRequest.getNotificationSettings(this.props.userData.token)
            .then((response) => {
                const data = response.data.data;
                let selectedSkills = []
                data.keywords.map(function (item) {
                    selectedSkills.push({
                        "cn": item.skills.name_cn,
                        "desc_cn": item.skills.description_cn,
                        "description": item.skills.description_en,
                        "text": item.skills.name_en,
                        "value": item.skills.id,
                    });
                });
                let selectedLanguages = [];
                data.language.map(function (item) {
                    selectedLanguages.push(item.language_name);
                });
                let selectedCountries = [].
                data.country.map(function(item){
                    selectedCountries.push(item.country_name.id);
                });
                console.log("selectedCountries");
                console.log(selectedCountries);
                this.setState({
                    atleastSpentLow: data.low_atleast_spend,
                    atleastSpentHigh: data.high_atleast_spend,
                    fixedPriceLow: data.low_fixed_price,
                    fixedPriceHigh: data.high_fixed_price,
                    hourlyPriceLow: data.low_hourly_price,
                    hourlyPriceHigh: data.high_hourly_price,
                    selectedSkills: selectedSkills,
                    countryType: data.country_type,
                    selectedLanguages: selectedLanguages,
                    selectedCountries: selectedCountries,
                    active: data.active == 1 ? true : false
                })
            })
            .catch(error => {
            });
    }

    getLanguages() {
        HttpRequest.getLanguages()
            .then((response) => {
                this.setState({
                    languages: response.data.data,
                });
            })
            .catch(error => {
            });
    };

    closeCountryModal = () => {
        this.setState({
            modalVisible: false,
            countryType: this.state.countryTypeOriginal
        });

    };

    closeCountryModalWithSave = () => {
        this.setState({
            modalVisible: false
        });
        this.saveNotificationSettings();

    };

    getSkillsList() {
        HttpRequest.getSkills()
            .then((response) => {
                this.setState({
                    skills: response.data,
                });
            })
            .catch(() => {
            });
    };

    searchProducts = (e) => {
        this.showSearchResult();
    };

    searchLanguage = (e) => {
        this.showLanguageResult();
    };

    highlight = string => string;

    filter(teks) {
        if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
            const pattern = new RegExp(teks, "i");
            let filteredSkills = _.filter(this.state.skills, (item) => {
                if (item.cn.search(pattern) !== -1) {
                    return item;
                }
            });
            filteredSkills = _.slice(filteredSkills, 0, 10);
            this.setState({filteredSkills});
        } else {
            const pattern = new RegExp(teks, "i");
            let filteredSkills = _.filter(this.state.skills, (item) => {
                if (item.text.search(pattern) !== -1) {
                    return item;
                }
            });
            filteredSkills = _.slice(filteredSkills, 0, 10);
            this.setState({filteredSkills});
        }
    };

    filterLanguage(text) {
        // if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
        //     const pattern = new RegExp(text, "i");
        //     let filteredSkills = _.filter(this.state.skills, (item) => {
        //         if (item.cn.search(pattern) !== -1) {
        //             return item;
        //         }
        //     });
        //     filteredSkills = _.slice(filteredSkills, 0, 10);
        //     this.setState({filteredSkills});
        // } else {
        const pattern = new RegExp(text, "i");
        let filteredSkills = _.filter(this.state.languages, (item) => {
            if (item.name.search(pattern) !== -1) {
                return item;
            }
        });
        filteredSkills = _.slice(filteredSkills, 0, 10);
        this.setState({filteredSkills});
        // }
    };

    setModalVisible(visible, type) {
        this.setState({
            modalVisible: visible,
            countryType: type
        });
    };

    selectSkill(item) {
        if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {

            let duplicate = false;
            this.state.selectedSkills.forEach((item2) => {
                if (item.value == item2.value) {
                    duplicate = true;
                }
            });
            if (duplicate) {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
                return false;
            }

            if (item.cn !== this.state.duplicate) {
                this.setState({duplicate: item.cn});
                const {selectedSkills} = this.state;
                selectedSkills.push(item);
                this.setState({
                    selectedSkills,
                    autocompleteText: "",
                });
            } else {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
            }
        } else {
            let duplicate = false;
            this.state.selectedSkills.forEach((item2) => {
                if (item.value == item2.value) {
                    duplicate = true;
                }
            });
            if (duplicate) {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
                return false;
            }

            if (item.text !== this.state.duplicate) {
                this.setState({duplicate: item.text});
                const {selectedSkills} = this.state;
                selectedSkills.push(item);
                this.setState({
                    selectedSkills,
                    autocompleteText: "",
                });
            } else {
                Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                    {
                        text: "OK",
                    }
                ]);
            }
        }
    };

    selectLanguage(item) {
        // if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
        //
        //     let duplicate = false;
        //     this.state.selectedSkills.forEach((item2) => {
        //         if (item.value == item2.value) {
        //             duplicate = true;
        //         }
        //     });
        //     if (duplicate) {
        //         Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
        //             {
        //                 text: "OK",
        //             }
        //         ]);
        //         return false;
        //     }
        //
        //     if (item.cn !== this.state.duplicate) {
        //         this.setState({duplicate: item.cn});
        //         const {selectedSkills} = this.state;
        //         selectedSkills.push(item);
        //         this.setState({
        //             selectedSkills,
        //             autocompleteLanguageText: "",
        //         });
        //     } else {
        //         Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
        //             {
        //                 text: "OK",
        //             }
        //         ]);
        //     }
        // } else {
        let duplicate = false;
        this.state.selectedSkills.forEach((item2) => {
            if (item.id == item2.id) {
                duplicate = true;
            }
        });
        if (duplicate) {
            Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                {
                    text: "OK",
                }
            ]);
            return false;
        }

        if (item.name !== this.state.duplicate) {
            this.setState({duplicate: item.name});
            const {selectedLanguages} = this.state;
            selectedLanguages.push(item);
            this.setState({
                selectedLanguages,
                autocompleteLanguageText: "",
            });
        } else {
            Alert.alert("Error", translate('duplicate_skills_not_allowed'), [
                {
                    text: "OK",
                }
            ]);
        }
        // }
    };

    removeSelectedSkills(item) {
        if (this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh') {
            const {selectedSkills} = this.state;
            const filteredSelectedSkills = [];
            selectedSkills.forEach((item2) => {
                if (item2.cn !== item.cn) {
                    filteredSelectedSkills.push(item2);
                }
            });
            this.setState({selectedSkills: filteredSelectedSkills, duplicate: ''});
        } else {
            const {selectedSkills} = this.state;
            const filteredSelectedSkills = [];
            selectedSkills.forEach((item2) => {
                if (item2.text !== item.text) {
                    filteredSelectedSkills.push(item2);
                }
            });
            this.setState({selectedSkills: filteredSelectedSkills, duplicate: ''});
        }
    };

    removeSelectedLanguage(item) {
        const {selectedSkills} = this.state;
        const filteredSelectedSkills = [];
        selectedSkills.forEach((item2) => {
            if (item2.text !== item.text) {
                filteredSelectedSkills.push(item2);
            }
        });
        this.setState({selectedSkills: filteredSelectedSkills, duplicate: ''});
    };

    showSearchResult() {
        Keyboard.dismiss();
        if (this.state.selectedSkills.length > 0) {
            if (this.state.selectedButton === 0) {
                this.props.navigation.navigate("JobsList", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                });
            } else {
                this.props.navigation.navigate("SearchResult", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                    title:
                        this.state.selectedButton === 1
                            ? "Find Experts"
                            : translate("find_cofounder"),
                });
            }
        } else {
            Alert.alert("Error", translate('select_skills'), [
                {
                    text: "OK",
                }
            ]);
        }
    };

    showLanguageResult() {
        Keyboard.dismiss();
        if (this.state.selectedSkills.length > 0) {
            if (this.state.selectedButton === 0) {
                this.props.navigation.navigate("JobsList", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                });
            } else {
                this.props.navigation.navigate("SearchResult", {
                    selectedSkills: this.state.selectedSkills,
                    locale: this.state.locale,
                    title:
                        this.state.selectedButton === 1
                            ? "Find Experts"
                            : translate("find_cofounder"),
                });
            }
        } else {
            Alert.alert("Error", translate('select_skills'), [
                {
                    text: "OK",
                }
            ]);
        }
    };

    renderSearchResult = () => {
        const searchResult = null;
        if (this.state.autocompleteText !== "") {
            return (
                <View
                    // style={[
                    //     styles.searchBoxStyle,
                    //     {
                    //         top: height * 0.6 - 130,
                    //         left: this.state.autocompletePosition.x,
                    //         width: this.state.autocompletePosition.width,
                    //         height: this.state.autocompletePosition.height,
                    //     },
                    // ]}
                >
                    {this.state.filteredSkills.map((item, index) => {
                        const style = {
                            // flex: 1,
                            // width: "100%",
                            // flexDirection: "row",
                            paddingHorizontal: 2,
                            paddingVertical: 2,
                            height: 55,
                            alignItems: "center",
                            backgroundColor: "gray"
                            // marginTop: 200
                        };
                        if (index !== 0) {
                            style.borderTopColor = "#eee";
                            style.borderTopWidth = 1;
                        }
                        return (
                            <TouchableOpacity
                                key={index}
                                // style={style}
                                style={{
                                    top: -180,
                                    height: 30,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    backgroundColor: '#fff'
                                }}
                                onPress={() => {
                                    this.selectSkill(item);
                                }}
                            >
                                <Text ellipsizeMode="tail" numberOfLines={1}>
                                    <Text style={{fontFamily: FontStyle.Medium}}>
                                        {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? this.highlight(item.cn) : this.highlight(item.text)}
                                    </Text>
                                    <Text>{"    |    "}</Text>
                                    {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? this.highlight(item.desc_cn) : this.highlight(item.description)}
                                </Text>
                            </TouchableOpacity>
                        );
                    })}
                </View>
            );
        }
    };

    renderLanguageSearchResult = () => {
        const searchResult = null;
        if (this.state.autocompleteLanguageText !== "") {
            return (
                <View
                    // style={[
                    //     styles.searchBoxStyle,
                    //     {
                    //         top: height * 0.6 - 130,
                    //         left: this.state.autocompletePosition.x,
                    //         width: this.state.autocompletePosition.width,
                    //         height: this.state.autocompletePosition.height,
                    //     },
                    // ]}
                >
                    {this.state.filteredSkills.map((item, index) => {
                        const style = {
                            // flex: 1,
                            // width: "100%",
                            // flexDirection: "row",
                            paddingHorizontal: 2,
                            paddingVertical: 2,
                            height: 55,
                            alignItems: "center",
                            backgroundColor: "gray"
                            // marginTop: 200
                        };
                        if (index !== 0) {
                            style.borderTopColor = "#eee";
                            style.borderTopWidth = 1;
                        }
                        return (
                            <TouchableOpacity
                                key={index}
                                // style={style}
                                style={{
                                    top: -250,
                                    height: 30,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    backgroundColor: '#fff'
                                }}
                                onPress={() => {
                                    this.selectLanguage(item);
                                }}
                            >
                                <Text ellipsizeMode="tail" numberOfLines={1}>
                                    <Text style={{fontFamily: FontStyle.Medium}}>
                                        {this.highlight(item.name)}
                                    </Text>
                                </Text>
                            </TouchableOpacity>
                        );
                    })}
                </View>
            );
        }
    };

    toggleSwitch = value => {
        console.log(value);
        //onValueChange of the switch this function will be called
        this.setState({active: value});
        //state changes according to switch
        //which will result in re-render the text
    };

    goBack = () => {
        this.props.navigation.navigate('Root');
    };

    fixedPriceChange = values => {
        this.setState({
            fixedPriceLow: values[0],
            fixedPriceHigh: values[1]
        })

    };

    atleastSpentChange = values => {
        this.setState({
            atleastSpentLow: values[0],
            atleastSpentHigh: values[1]
        })

    };

    multiSliderHourlyValuesChange = values => {
        this.setState({
            hourlyPriceLow: values[0],
            hourlyPriceHigh: values[1]
        })

    };

    updateCountryType = type => {
        this.setState({
            countryType: type
        })
    };

    //Countries
    selectedCountries = item => {
        let {countries, selectedCountryItems} = this.state;
        let emptyArr = selectedCountryItems;
        countries.map(subItem => {
            if (item.id === subItem.id) {
                !subItem.isSelect
                    ? emptyArr.push(subItem.id)
                    : emptyArr.splice(emptyArr.indexOf(subItem.id), 1);
                subItem.isSelect = !subItem.isSelect;
            }
        });
        this.setState({countries: countries, selectedCountries: emptyArr});
    };

    renderCountries = item => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                    onPress={() => this.selectedCountries(item)}
                    style={styles.renderContainer}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            color: item.isSelect ? Config.primaryColor : null
                        }}
                    >
                        {item.name}
                    </Text>

                    {item.isSelect && (
                        <MaterialIcons
                            name={"check"}
                            size={20}
                            style={{marginRight: 15}}
                            color={Config.primaryColor}
                        />
                    )}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    toCSS = (obj, separator) => {
        var arr = [];

        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                arr.push(obj[key]);
            }
        }

        return arr.join(separator || ",");
    };

    saveNotificationSettings = () => {

        console.log(this.state.selectedCountries);
        let selectedCountries = this.toCSS(this.state.selectedCountries);
        var selectedSkills = this.state.selectedSkills.map(function (item) {
            return item.value;
        });
        selectedSkills = selectedSkills.join(',');
        let selectedLanguages = this.state.selectedLanguages.map(function (item) {
            return item.id;
        });
        selectedLanguages = selectedLanguages.join(',')

        let formObj = {
            'low_fixed_price': this.state.fixedPriceLow,
            'high_fixed_price': this.state.fixedPriceHigh,
            'low_hourly_price': this.state.hourlyPriceLow,
            'high_hourly_price': this.state.hourlyPriceHigh,
            'low_atleast_spend': this.state.atleastSpentLow,
            'high_atleast_spend': this.state.atleastSpentHigh,
            'package_id': 1,
            'keyword': selectedSkills,
            'language': selectedLanguages,
            'country': selectedCountries,
            'country_type': this.state.countryType,
            'active': this.state.active,
        };
        HttpRequest.saveNotificationSettings(this.props.userData.token, formObj)
            .then((response) => {
                const data = response.data.data;
                console.log(data);
            })
            .catch(error => {
            });


    };

    render() {
        const selectedSkillsElement = this.state.selectedSkills.map((item, index) => (
            <View
                style={[styles.tagInput, {
                    color: "black",
                    shadowColor: "#f7f7f7",
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    }
                }]}
                key={index}
                elevation={5}
            >
                <Text>
                    {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.cn : item.text}
                </Text>
                <TouchableOpacity style={{marginTop: 1}} onPress={() => {
                    this.removeSelectedSkills(item);
                }}>
                    <Entypo name="cross" size={20} color={"#B7B7B7"}/></TouchableOpacity>
            </View>
        ));

        const selectedLanguageElement = this.state.selectedLanguages.map((item, index) => (
            <View
                style={[styles.tagInput, {
                    color: "black",
                    shadowColor: "#f7f7f7",
                    shadowOpacity: 0.8,
                    shadowRadius: 1,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    }
                }]}
                key={index}
                elevation={5}
            >
                <Text>
                    {item.name}
                </Text>
                <TouchableOpacity style={{marginTop: 1}} onPress={() => {
                    this.removeSelectedLanguage(item);
                }}>
                    <Entypo name="cross" size={20} color={"#B7B7B7"}/></TouchableOpacity>
            </View>
        ));

        if (this.state.loading === true) {
            return (
                <View style={{alignItems: 'center', justifyContent: 'center', marginTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        } else {
            return (
                <ScrollView>
                    <View style={styles.rootStyle}>
                        {/* Back Button View */}
                        <View style={styles.navigationStyle}>
                            <View style={styles.navigationWrapper}>
                                <TouchableOpacity
                                    onPress={this.goBack}
                                    style={{
                                        marginTop: 8,
                                        marginLeft: 2,
                                        flexDirection: "row"
                                    }}
                                >
                                    <SimpleLineIcons size={20} name="arrow-left" color={Config.primaryColor}/>
                                    <Text
                                        style={{
                                            marginLeft: 5,
                                            fontSize: 18,
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Regular,
                                        }}
                                    >
                                        {translate("back")}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={this.goBack}
                                    style={{
                                        marginTop: 8,
                                        marginLeft: 2,
                                        flexDirection: "row"
                                    }}
                                >
                                    <Switch
                                        onValueChange={this.toggleSwitch}
                                        style={{backgroundColor: '#B7B7B7', borderRadius: 17}}
                                        value={this.state.active}
                                        trackColor={{true: Config.primaryColor, false: '#B7B7B7'}}
                                        activeText={'On'}
                                        inActiveText={'Off'}
                                    />
                                </TouchableOpacity>

                            </View>
                        </View>

                        {/* Main Page Starts here */}
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'flex-start',
                            alignItems: 'stretch',
                        }}>

                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                                alignItems: 'stretch',
                            }}>
                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 15
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Fixed Price
                                    </Text>

                                    <TouchableOpacity style={{
                                        marginLeft: 25,
                                        marginTop: 25
                                    }}>
                                        <MultiSlider
                                            selectedStyle={{
                                                backgroundColor: Config.primaryColor,
                                            }}
                                            unselectedStyle={{
                                                backgroundColor: '#B7B7B7',
                                            }}
                                            trackStyle={{
                                                backgroundColor: 'red',
                                            }}
                                            values={[
                                                3000,
                                                6000,
                                            ]}
                                            touchDimensions={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 15,
                                                slipDisplacement: 200
                                            }}
                                            sliderLength={330}
                                            onValuesChange={this.fixedPriceChange}
                                            onValuesChangeFinish={this.saveNotificationSettings}
                                            min={100}
                                            max={10000}
                                            customMarker={CustomMarker}
                                            step={1}
                                            allowOverlap
                                            snapped
                                        />
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        left: 100,
                                        color: '#353635'
                                    }}>
                                        {this.state.fixedPriceLow}USD - {this.state.fixedPriceHigh}USD
                                    </Text>
                                </View>

                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 80
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Hourly Price
                                    </Text>

                                    <TouchableOpacity style={{
                                        marginLeft: 25,
                                        marginTop: 25
                                    }}>
                                        <MultiSlider
                                            selectedStyle={{
                                                backgroundColor: Config.primaryColor,
                                            }}
                                            unselectedStyle={{
                                                backgroundColor: '#B7B7B7',
                                            }}
                                            trackStyle={{
                                                backgroundColor: 'red',
                                            }}
                                            values={[
                                                this.state.hourlyPriceLow,
                                                this.state.hourlyPriceHigh,
                                            ]}
                                            touchDimensions={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 15,
                                                slipDisplacement: 200
                                            }}
                                            sliderLength={330}
                                            onValuesChange={this.multiSliderHourlyValuesChange}
                                            onValuesChangeFinish={this.saveNotificationSettings}
                                            min={0}
                                            max={1000}
                                            customMarker={CustomMarker}
                                            step={1}
                                            allowOverlap
                                            snapped
                                        />
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        left: 100,
                                        color: '#353635'
                                    }}>
                                        {this.state.hourlyPriceLow}USD - {this.state.hourlyPriceHigh}USD
                                    </Text>
                                </View>

                                {/* Keyword Section */}
                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 80
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Keyword
                                    </Text>

                                    <View
                                        style={styles.searchBoxWrapperStyle}
                                        ref="SearchInput"
                                        onLayout={({nativeEvent}) => {
                                            this.refs.SearchInput.measure((x, y, width, height, pageX, pageY) => {
                                                this.setState({
                                                    autocompletePosition: {
                                                        x: pageX,
                                                        y: pageY - 35,
                                                        width,
                                                    },
                                                });
                                            });
                                        }}
                                    >
                                        <ScrollView
                                            showsHorizontalScrollIndicator={true}
                                            horizontal={true}
                                            style={{
                                                flexDirection: "row",
                                                width: "80%",
                                                height: "100%",
                                                marginLeft: 5,
                                                marginRight: 5,
                                                borderColor: '#B7B7B7',
                                                border: 1
                                            }}
                                        >
                                            {selectedSkillsElement}
                                            <TextInput
                                                style={{
                                                    flex: 1,
                                                    width: 300,
                                                    padding: 8,
                                                }}
                                                placeholder={'Select Keywords'}
                                                autoCapitalize="none"
                                                autoCorrect={false}
                                                underlineColorAndroid="transparent"
                                                onSubmitEditing={this.searchProducts.bind(this)}
                                                onFocus={() => {
                                                    this.setState({showAutocomplete: true});
                                                }}
                                                onBlur={() => {
                                                    this.setState({showAutocomplete: false});
                                                }}
                                                onChangeText={(autocompleteText) => {
                                                    this.setState({autocompleteText});
                                                    this.filter(autocompleteText);
                                                }}
                                                value={this.state.autocompleteText}
                                            />
                                        </ScrollView>

                                        <TouchableOpacity
                                            style={styles.searchButtonStyle}
                                            onPress={() => {
                                                this.showSearchResult();
                                            }}
                                        >
                                        </TouchableOpacity>
                                    </View>
                                    {this.renderSearchResult()}

                                </View>

                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 50
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Country
                                    </Text>

                                    <TouchableOpacity onPress={() => {
                                        this.updateCountryType(1);
                                    }}>
                                        <View style={{
                                            top: 26,
                                            width: "80%",
                                            marginLeft: 20,
                                            borderBottom: 1,
                                            borderBottomColor: 'red',
                                        }}>
                                            {this.state.countryType == 1 &&
                                            <Ionicons size={20} color={Config.primaryColor} name="md-checkmark"
                                                      style={{position: "absolute"}}/>}
                                            <Text style={{
                                                fontFamily: FontStyle.Bold,
                                                left: 30,
                                                color: '#353635',
                                                top: -5
                                            }}>
                                                All
                                            </Text>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                left: 30,
                                                color: '#b7b7b7',
                                                top: -5,
                                                fontSize: 12
                                            }}>
                                                Receive from all countries
                                            </Text>
                                        </View>
                                    </TouchableOpacity>

                                    <View
                                        style={{
                                            borderBottomColor: '#e6e6e6',
                                            borderBottomWidth: 1,
                                            top: 25,
                                            width: "85%",
                                            left: 50
                                        }}
                                    />

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(true, 2);
                                    }}>
                                        {this.state.countryType == 2 &&
                                        <Ionicons size={20} color={Config.primaryColor} name="md-checkmark"
                                                  style={{position: "absolute", top: 45, left: 20}}/>}
                                        <View style={{
                                            top: 65,
                                            width: "80%",
                                            marginLeft: 20,
                                            borderBottom: 1,
                                            borderBottomColor: 'red',
                                        }}>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                left: 30,
                                                color: '#353635',
                                                top: -20
                                            }}>
                                                All Expect...
                                            </Text>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                left: 30,
                                                color: '#b7b7b7',
                                                top: -15,
                                                fontSize: 12
                                            }}>
                                                Receive from all countries except countries you select
                                            </Text>
                                            <SimpleLineIcons color={'#b7b7b7'} name="arrow-right" style={{
                                                position: "absolute",
                                                right: -40
                                            }}/>

                                        </View>
                                    </TouchableOpacity>

                                    <View
                                        style={{
                                            borderBottomColor: '#e6e6e6',
                                            borderBottomWidth: 1,
                                            top: 55,
                                            width: "85%",
                                            left: 50
                                        }}
                                    />

                                    <TouchableOpacity onPress={() => {
                                        this.setModalVisible(true, 3);
                                    }}>
                                        {this.state.countryType == 3 &&
                                        <Ionicons size={20} color={Config.primaryColor} name="md-checkmark"
                                                  style={{position: "absolute", top: 70, left: 20}}/>}
                                        <View style={{
                                            top: 90,
                                            width: "80%",
                                            marginLeft: 20,
                                            borderBottom: 1,
                                            borderBottomColor: 'red',
                                        }}>
                                            {/*<Ionicons size={20} color={Config.primaryColor} name="md-checkmark"/>*/}
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                left: 30,
                                                color: '#353635',
                                                top: -20
                                            }}>
                                                Only From
                                            </Text>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                left: 30,
                                                color: '#b7b7b7',
                                                top: -20,
                                                fontSize: 12
                                            }}>
                                                Receive only from selected countries
                                            </Text>
                                            <SimpleLineIcons color={'#b7b7b7'} name="arrow-right" style={{
                                                position: "absolute",
                                                right: -40
                                            }}/>
                                        </View>
                                    </TouchableOpacity>
                                    <View
                                        style={{
                                            borderBottomColor: '#e6e6e6',
                                            borderBottomWidth: 1,
                                            top: 80,
                                            width: "85%",
                                            left: 50
                                        }}
                                    />
                                </View>

                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 180
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Atleast Spent
                                    </Text>

                                    <TouchableOpacity style={{
                                        marginLeft: 25,
                                        marginTop: 25
                                    }}>
                                        <MultiSlider
                                            selectedStyle={{
                                                backgroundColor: Config.primaryColor,
                                            }}
                                            unselectedStyle={{
                                                backgroundColor: '#B7B7B7',
                                            }}
                                            trackStyle={{
                                                backgroundColor: 'red',
                                            }}
                                            values={[
                                                this.state.atleastSpentLow,
                                                this.state.atleastSpentHigh,
                                            ]}
                                            touchDimensions={{
                                                height: 50,
                                                width: 50,
                                                borderRadius: 15,
                                                slipDisplacement: 200
                                            }}
                                            onValuesChangeFinish={this.saveNotificationSettings}
                                            sliderLength={330}
                                            onValuesChange={this.atleastSpentChange}
                                            min={1}
                                            max={10000}
                                            customMarker={CustomMarker}
                                            step={1}
                                            allowOverlap
                                            snapped
                                        />
                                    </TouchableOpacity>
                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        left: 100,
                                        color: '#353635'
                                    }}>
                                        {this.state.atleastSpentLow}USD - {this.state.atleastSpentHigh}USD
                                    </Text>
                                </View>

                                {/* Country Section */}
                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 80
                                }}>
                                    <Text style={{
                                        marginLeft: 20,
                                        fontSize: 15,
                                        top: 18,
                                        color: Config.primaryColor,
                                        fontFamily: FontStyle.Bold,
                                    }}>
                                        Language
                                    </Text>

                                    <View
                                        style={styles.searchBoxWrapperStyle}
                                        ref="SearchInput"
                                        onLayout={({nativeEvent}) => {
                                            this.refs.SearchInput.measure((x, y, width, height, pageX, pageY) => {
                                                this.setState({
                                                    autocompletePosition: {
                                                        x: pageX,
                                                        y: pageY - 35,
                                                        width,
                                                    },
                                                });
                                            });
                                        }}
                                    >
                                        <ScrollView
                                            showsHorizontalScrollIndicator={true}
                                            horizontal={true}
                                            style={{
                                                flexDirection: "row",
                                                width: "80%",
                                                height: "100%",
                                                marginLeft: 5,
                                                marginRight: 5,
                                                borderColor: '#B7B7B7',
                                                border: 1
                                            }}
                                        >
                                            {selectedLanguageElement}
                                            <TextInput
                                                style={{
                                                    flex: 1,
                                                    width: 300,
                                                    padding: 8,
                                                }}
                                                placeholder={'Select Language'}
                                                autoCapitalize="none"
                                                autoCorrect={false}
                                                underlineColorAndroid="transparent"
                                                onSubmitEditing={this.searchLanguage.bind(this)}
                                                onFocus={() => {
                                                    this.setState({showLanguageAutoComplete: true});
                                                }}
                                                onBlur={() => {
                                                    this.setState({showLanguageAutoComplete: false});
                                                }}
                                                onChangeText={(autocompleteLanguageText) => {
                                                    this.setState({autocompleteLanguageText});
                                                    this.filterLanguage(autocompleteLanguageText);
                                                }}
                                                value={this.state.autocompleteLanguageText}
                                            />
                                        </ScrollView>

                                        <TouchableOpacity
                                            style={styles.searchButtonStyle}
                                            onPress={() => {
                                                this.showLanguageResult();
                                            }}
                                        >
                                        </TouchableOpacity>
                                    </View>
                                    {this.renderLanguageSearchResult()}

                                </View>


                                <View style={{
                                    width: '100%',
                                    height: 50,
                                    backgroundColor: 'white',
                                    borderTopWidth: 3,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 50
                                }}>

                                    <View
                                        style={styles.searchBoxWrapperStyleOne}
                                    >

                                        <View style={{flex: 1, flexDirection: 'row'}}>
                                            <TouchableOpacity>
                                                <View style={{
                                                    width: 105,
                                                    marginLeft: 20,
                                                    marginRight: 10,
                                                    height: 80,
                                                    borderWidth: 1,
                                                    borderColor: Config.primaryColor,
                                                    borderRadius: 2
                                                }}>
                                                    <Image
                                                        style={{
                                                            width: 40,
                                                            height: 40,
                                                            left: 30,
                                                            // top: 25,
                                                            // bottom: 10
                                                        }}
                                                        source={require('../../../images/hand.png')}/>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        Top up 30 push
                                                    </Text>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        1 USD
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>

                                            <TouchableOpacity>
                                                <View style={{
                                                    width: 105,
                                                    marginRight: 10,
                                                    border: 2,
                                                    height: 80,
                                                    borderWidth: 1,
                                                    borderColor: Config.primaryColor,
                                                    borderRadius: 2
                                                }}>
                                                    <Image
                                                        style={{
                                                            width: 40,
                                                            height: 40,
                                                            left: 30,
                                                            // top: 25,
                                                            // bottom: 10
                                                        }}
                                                        source={require('../../../images/hand.png')}/>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        Top up 30 push
                                                    </Text>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        1 USD
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>

                                            <TouchableOpacity>
                                                <View style={{
                                                    width: 105,
                                                    marginRight: 10,
                                                    height: 80,
                                                    borderWidth: 1,
                                                    borderColor: Config.primaryColor,
                                                    borderRadius: 2
                                                }}>
                                                    <Image
                                                        style={{
                                                            width: 40,
                                                            height: 40,
                                                            left: 30,
                                                            // top: 25,
                                                            // bottom: 10
                                                        }}
                                                        source={require('../../../images/hand.png')}/>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        Top up 30 push
                                                    </Text>
                                                    <Text style={{
                                                        fontFamily: FontStyle.Bold,
                                                        fontSize: 12,
                                                        color: '#b7b7b7',
                                                        textAlign: "center",
                                                        flex: 1
                                                    }}>
                                                        1 USD
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>

                                        <View>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                right: -30,
                                                top: 70,
                                            }}>
                                                45
                                            </Text>
                                            <Text style={{
                                                fontFamily: FontStyle.Regular,
                                                right: 280,
                                                top: 55,
                                            }}>
                                                Remaining
                                            </Text>
                                        </View>
                                    </View>

                                </View>


                                <View style={{
                                    width: '100%',
                                    height: 70,
                                    backgroundColor: 'white',
                                    borderTopWidth: 1,
                                    borderBoidth: 4,
                                    borderTopColor: '#e6e6e6',
                                    marginTop: 100
                                }}>

                                    <View
                                        style={[styles.searchBoxWrapperStyle, {
                                            top: 10
                                        }]}
                                    >

                                        <Text style={{
                                            fontFamily: FontStyle.Bold,
                                            fontSize: 12,
                                            color: '#b7b7b7',
                                            textAlign: "center",
                                            flex: 1
                                        }}>
                                            The complete info you fill, will allow you to receive better proposal!
                                        </Text>

                                    </View>
                                    <View
                                        style={{
                                            borderBottomColor: '#e6e6e6',
                                            borderBottomWidth: 8,
                                            top: 60,
                                            width: "100%",
                                        }}
                                    />
                                </View>


                            </View>

                        </View>
                    </View>

                    {this.state.modalVisible && (
                        <MultiSelectModal
                            visible={this.state.modalVisible}
                            onRequestClose={() => this.closeCountryModal()}
                            title={'Select Countries'}
                            onChangeText={searchCountry =>
                                this.setState({searchCountry})
                            }
                            onCrossPressed={() => this.closeCountryModal()}
                            value={this.state.searchCountry}
                            data={
                                this.state.countries.filter(item =>
                                    item.name
                                )
                            }
                            renderItem={({item, index}) =>
                                this.renderCountries(item, index)
                            }
                            extraData={this.state}
                            submitPressed={() => this.closeCountryModalWithSave()}
                        />
                    )}

                </ScrollView>
            );
        }
    }
}

const styles = {
    renderContainer: {
        height: 45,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    },

    rootStyle: {
        backgroundColor: "#fff",
        flex: 1,
        flexDirection: "column"
    },

    tagInput: {
        height: 30,
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: '#fff',
        borderColor: '#B7B7B7',
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10,
        marginLeft: 7,
        marginTop: 5,
        width: 70
    },

    contactTextStyle: {
        color: '#fff',
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 30,
        paddingVertical: 10,
        fontSize: 15,
        marginTop: 10,
        fontFamily: FontStyle.Bold,
    },

    backArrowStyle: {
        marginRight: 10
    },

    backArrowIconStyle: {
        fontFamily: "Montserrat-Light",
        marginLeft: 10
    },

    navigationStyle: {
        height: 40,
        flexDirection: "row",
        marginTop: 20,
        paddingHorizontal: 5
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%"
    },

    searchBoxWrapperStyle: {
        borderRadius: 2,
        backgroundColor: "#fff",
        height: 45,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        top: 50,
        // borderTopWidth: 3,
        // borderTopColor: '#B7B7B7',
    },

    searchBoxWrapperStyleOne: {
        borderRadius: 2,
        backgroundColor: "#fff",
        height: 60,
        width: "100%",
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        top: 40,
        // borderTopWidth: 3,
        // borderTopColor: '#B7B7B7',
    },

};

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        }),
        mySelectedSeatData: data => dispatch({
            type: 'SELECTED_SEAT_DATA',
            data: {...data},
        }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationSettings);

// export default NotificationSettings