/* eslint-disable react/prop-types */
/* eslint-disable indent */
import React, { Component } from 'react';
// eslint-disable-next-line no-unused-vars
import {
  Text,
  View,
  TextInput,
} from 'react-native';
import FontStyle from '../../constants/FontStyle';

export default class Input extends Component {
  constructor(props) {
    super(props);
    this.state = {
      secureTextEntry: false,
    };
  }

  componentDidMount() {
    if (this.props.secureTextEntry) {
      this.setState({ secureTextEntry: this.props.secureTextEntry });
    }
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.headerWrapperStyle}>
          <Text style={styles.headerStyle}>{this.props.title}</Text>
        </View>
        <View style={styles.inputWrapperStyle}>
          <TextInput style={styles.inputStyle}
                     maxLength={this.props.maxLength || null}
                     placeholder={this.props.placeholder}
                     autoCapitalize='none'
                     keyboardType={this.props.keyboardType}
                     autoCorrect={false}
                     underlineColorAndroid='transparent'
                     onChangeText={this.props.onChangeText ? text => this.props.onChangeText(text) : null}
                     value={this.props.value}
                     secureTextEntry={this.state.secureTextEntry}
                     editable={this.props.editable}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#e5e5e5',
    marginVertical: 5,
    marginHorizontal: 10,
  },

  headerWrapperStyle: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: '#f6f6f6',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },

  headerStyle: {
    fontSize: 12,
    color: '#8b8b8b',
    fontFamily: FontStyle.Regular,
  },

  inputWrapperStyle: {
    paddingVertical: 0,
    paddingHorizontal: 10,
    flexDirection: 'row',
  },

  inputStyle: {
    flex: 1,
    height: 40,
    fontSize: 14,
    color: '#aaaaaa',
    // fontFamily: FontStyle.Light,
  },
};
