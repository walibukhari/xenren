/* eslint-disable no-undef */
/* eslint-disable max-len */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-const */

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  FlatList, TextInput, Image, Modal,
} from 'react-native';
import { connect } from 'react-redux';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AvatarWithStatus from '../components/AvatarWithStatus';
import Config from '../Config';
import HttpRequest from '../components/HttpRequest';
import FontStyle from '../constants/FontStyle';
import { translate } from '../i18n';
import AntDesign from 'react-native-vector-icons/AntDesign';

let deviceHeight = Dimensions.get('screen').height;
let deviceWidth = Dimensions.get('screen').width;

class NotificationDetail extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'Legends Lair',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
          <View style={style.rootStyle}>
          {/* Back Button View */}
          <View style={style.navigationStyle}>
              <View style={style.navigationWrapper}>
                  <TouchableOpacity
                      onPress={() => navigation.goBack()}
                      style={{
                          marginTop: 8,
                          marginLeft: 10,
                          flexDirection: "row",
                      }}
                  >
                      <Image
                          style={{width:20,height:26,position:'relative',top:-1}}
                          source={require('../../images/arrowLA.png')}
                      />

                      <Text
                          onPress={() => navigation.goBack()}
                          style={{
                              marginLeft: 10,
                              marginTop:0,
                              fontSize: 19.5,
                              fontWeight:'normal',
                              color: Config.primaryColor,
                              fontFamily: FontStyle.Regular,
                          }}
                      >
                          Legends Lair
                      </Text>
                  </TouchableOpacity>
              </View>
          </View>
      </View>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      data: '',
      userId: this.props.userData.id,
      userImageAvatar: this.props.userData.img_avatar,
      notification_id: this.props.navigation.state.params.id,
      row: this.props.navigation.state.params.data,
      sender_name: this.props.userData.real_name,
      sender_profile_pic: '',
      message: '',
      userData: '',
      loading: true,
      category: this.props.navigation.state.params.data.category,
      sharedOffice: this.props.navigation.state.params.sharedOffice,
      msg: '',
      sender_id: '',
      projects: '',
      description: '',
      length: 0,
      activeMsg: true,
      receiverId: '',
      inboxId: '',
      isLoading: false,
      rejectLoading: false,
      confirmDelete: false,
      isReply: false,
      replyLoading: false,
      replyText: '',
      projectStatus: '',
      chatType: '',
    };
  }

  componentDidMount() {
    console.log('NotificationDetail.js category--->', this.state.category);
    if (this.state.sharedOffice === true) {
      this.setState({
        category: 'Shared Office Booking',
        loading: false,
        replyText: `${translate('admin_of')}${this.state.row.office.office_name}\n\n${translate('come_on_the_booking')}\n\n${translate('warm_regrets')}`,
      });
      this.getUserDetail(this.state.row.user_id);
    } else {
      let inboxId = this.state.notification_id;
      this.getNotificationDetails(this.props.userData.token, inboxId);
    }
  }

  getUserDetail = (userId) => {
    const accessToken = this.props.userData.token;
    HttpRequest.getSharedOfficeBookingUserDetail(accessToken, userId)
      .then((response) => {
        let result = response.data;
        if (result.status === 'success') {
          this.setState({ userData: result.data });
        } else {
          alert(result.message);
        }
      })
      .catch((error) => {
        alert(error);
        // this.setState({ loading: false });
      });
  };

  getNotificationDetails(access_token, id) {
    HttpRequest.newNotificationDetail(access_token, id)
      .then((response) => {
        const result = response.data;
        console.log('data---<<', result);
        if (result.status === 'success') {
          if (result.projects === null || result.projects === undefined) {
            this.setState({ activeMsg: false });
          } else {
            this.setState({
              projects: result.projects,
              projectStatus: result.projects.status,
              receiverId: result.projects.creator.id,
              description: result.projects.description.replace(/(<([^>]+)>)/ig, ''),
              length: result.data.length,
            });
          }
          let data = result.data;
          if (data.length > 0) {
            data = data.reverse();
            this.setState({
              chatData: response.data.data,
              chatType: response.data.type,
              sender_id: data[0].sender_id,
              loading: false,
            });
          } else {
            this.setState({ loading: false });
          }
        } else if (result.status === 'failure') {
          alert(result.status);
        } else {
          alert(translate('network_error'));
        }
      })
      .catch(() => {
        alert(translate('network_error'));
      });
  }
  sendMessage = () => {
    if (this.state.msg === '') {
      alert(translate('empty_message'));
    } else {
      let newData = {
        sender_id: this.state.userId,
        message: this.state.msg,
      };
      this.setState(prevState => ({
        chatData: [...prevState.chatData, newData],
        msg: '',
      }));
      this.sendMsgAPI(this.props.userData.token);
    }
  };

  sendMsgAPI = (access_token) => {
    const myMsg = this.state.msg;
    const inboxId = this.state.notification_id;
    const senderId = this.state.sender_id;
    let type = 2;
    HttpRequest.sendInboxMessage(access_token, inboxId, senderId, myMsg, type)
      .then((response) => {
        const result = response.data;
      })
      .catch((error) => {
        alert(error);
      });
  };

  renderRow(item) {
    if (this.state.chatData) {
      if (item.item.sender_id === this.state.userId) {
        return (
            <View style={styles.rowLeftStyle}>
              <Image source={{ uri: this.state.userImageAvatar }} style={styles.avatarLeftStyle} />
              <Image source={require('./../../images/private_chat/corner_1.png')}
                     style={{
                       height: 10, width: 10, tintColor: '#fff', marginRight: -2, marginTop: 5,
                     }} />
              <View style={styles.infoStyle}>
                <View style={styles.bubbleLeftStyle}>
                  <Text>{item.item.message}</Text>
                </View>
              </View>
            </View>
        );
      }
      return (
            <View style={styles.rowRightStyle}>
              <View style={[styles.infoStyle, { alignItems: 'flex-end' }]}>
                <View style={styles.bubbleRightStyle}>
                  <Text style={{ color: '#fff' }}>{item.item.message}</Text>
                </View>
                {this.state.activeMsg === true && (
                <View style={styles.bubbleRightProjects}>
                  <Text style={{ color: Config.primaryColor }}>{translate('send_invite_project')}</Text>
                  <Text style={styles.textColor}>{item.item.sender_name}</Text>
                  <Text style={styles.textColor}>Require Daily Updates:{this.state.projects.daily_update === 0 ? translate('no') : translate('yes') }</Text>
                  <Text style={styles.textColor}>Skills:
                  {this.state.projects.project_skills.length > 0 ? this.state.projects.project_skills.map((items, index) => {
                          return '  ' + items.skill.name_en;
                  }) : '' }</Text>
                </View>
                )}
              </View>
              <TouchableOpacity>
                <Image source={{ uri: item.item.sender_profile_pic }} style={styles.avatarRightStyle} />
              </TouchableOpacity>
            </View>
      );
    }
  }
  renderOfferReceivedRow(item) {
    if (this.state.chatData) {
      if (item.item.sender_id === this.state.userId) {
        return (
            <View style={styles.rowLeftStyle}>
              <Image source={{ uri: this.state.userImageAvatar }} style={styles.avatarLeftStyle} />
              <Image source={require('./../../images/private_chat/corner_1.png')}
                     style={{
                       height: 10, width: 10, tintColor: '#fff', marginRight: -2, marginTop: 5,
                     }} />
              <View style={styles.infoStyle}>
                <View style={styles.bubbleLeftStyle}>
                    <Text style={{ color: this.state.chatType === 16 ? Config.primaryColor : '#fff' }}>{item.item.message}</Text>
                </View>
                  {item.index === 0 && (
                      <View style={styles.bubbleRightProjects}>
                          <Text style={{ color: Config.primaryColor }}>{translate('send_offer')}</Text>
                          <Text style={styles.textColor}>{this.state.projects.name}</Text>
                          <Text style={styles.textColor}>{this.state.description}</Text>
                          <Text style={styles.textColor}>{translate('daily_updates')}:{this.state.projects.daily_update === 0 ? translate('no') : translate('yes') }</Text>
                          <Text style={styles.textColor}>
                              Offer: {'$ '}{item.item.quote_price} {this.state.projects.pay_type === 2 ? translate('hourly_pay') : translate('Fixed_price')}
                          </Text>
                          {this.state.activeMsg === false ? <View /> : this.state.projects.project_questions.length > 0 ? this.state.projects.project_questions.map((items, index) => {
                              if (items.applicant_answer.length === 0) {
                                  return (
                                      <Text key={index} style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                          Q#{index + 1} {items.question}
                                      </Text>
                                  );
                              } else {
                                  return (
                                      <View key={index}>
                                          <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                              Q#{index + 1} {items.question}
                                          </Text>
                                          <Text style={styles.textColor}>{items.applicant_answer[index].answer}</Text>
                                      </View>
                                  );
                              }
                          }) : <View />
                          }
                      </View>
                  )}
              </View>
            </View>
        );
      }
      return (
            <View style={styles.rowRightStyle}>
              <View style={[styles.infoStyle, { alignItems: 'flex-end' }]}>
                <View style={styles.bubbleRightStyle}>
                    <Text style={{ color: '#fff' }}>{item.item.message}</Text>
                </View>
                  {item.index === 0 && (
                      <View style={styles.bubbleRightProjects}>
                          <Text style={{ color: Config.primaryColor }}>{translate('send_offer')}</Text>
                          <Text style={styles.textColor}>{this.state.projects.name}</Text>
                          <Text style={styles.textColor}>{this.state.description}</Text>
                          <Text style={styles.textColor}>{translate('daily_updates')}:{this.state.projects.daily_update === 0 ? translate('no') : translate('yes') }</Text>
                          <Text style={styles.textColor}>
                              Offer: {'$ '}{item.item.quote_price} {this.state.projects.pay_type === 2 ? translate('hourly_pay') : translate('Fixed_price')}
                          </Text>
                          {this.state.activeMsg === false ? <View /> : this.state.projects.project_questions.length > 0 ? this.state.projects.project_questions.map((items, index) => {
                              if (items.applicant_answer.length === 0) {
                                  return (
                                      <Text key={index} style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                          Q#{index + 1} {items.question}
                                      </Text>
                                  );
                              } else {
                                  return (
                                      <View key={index}>
                                          <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                              Q#{index + 1} {items.question}
                                          </Text>
                                          <Text style={styles.textColor}>{items.applicant_answer[index].answer}</Text>
                                      </View>
                                  );
                              }
                          }) : <View />
                          }
                      </View>
                  )}
              </View>
              <TouchableOpacity>
                <Image source={{ uri: item.item.sender_profile_pic }} style={styles.avatarRightStyle} />
              </TouchableOpacity>
            </View>
      );
    }
  }

  renderOfferRow(item) {
    if (this.state.chatData) {
      if (item.item.sender_id === this.state.userId) {
        return (
            <View style={styles.rowLeftStyle}>
              <Image source={{ uri: this.state.userImageAvatar }} style={styles.avatarLeftStyle} />
              <Image source={require('./../../images/private_chat/corner_1.png')}
                     style={{
                       height: 10, width: 10, tintColor: '#fff', marginRight: -2, marginTop: 5,
                     }} />
              <View style={styles.infoStyle}>
                <View style={styles.bubbleLeftStyle}>
                  <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Regular, fontSize: 16 }}>{translate('accept_offer')}</Text>
                    {item.index === 0 && (
                        <View style={styles.bubbleRightProjects}>
                            <Text style={styles.textColor}>Project Name: {this.state.projects.name}</Text>
                            <Text style={styles.textColor}>description: {this.state.description}</Text>
                            <Text style={styles.textColor}>Offer: {'$ '}{this.state.projects.reference_price}</Text>
                            {this.state.activeMsg === false ? <View /> : this.state.projects.project_questions.length > 0 ? this.state.projects.project_questions.map((items, index) => {
                                if (items.applicant_answer.length === 0) {
                                    return (
                                        <Text key={index} style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                            Q#{index + 1} {items.question}
                                        </Text>
                                    );
                                } else {
                                    return (
                                        <View key={index}>
                                            <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                                Q#{index + 1} {items.question}
                                            </Text>
                                            <Text style={styles.textColor}>{items.applicant_answer[index].answer}</Text>
                                        </View>
                                    );
                                }
                            }) : <View />
                            }
                        </View>
                    )}
                </View>
              </View>
            </View>
        );
      }
      return (
            <View style={styles.rowRightStyle}>
              <View style={[styles.infoStyle, { alignItems: 'flex-end' }]}>
                {/*<View style={styles.bubbleRightStyle}>
                  <Text style={{ color: '#fff' }}>{this.state.category === 'Applicant Accepted' ? 'Congratulation, you have been selected as freelancer' : item.item.message}</Text>
                </View>*/}
                  <View style={styles.bubbleLeftStyle}>
                      {item.item.message === '' ? <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Regular, fontSize: 16 }}>{translate('accept_offer')}</Text> : <Text>{item.item.message}</Text> }
                      {item.index === 0 && (
                          <View style={styles.bubbleRightProjects}>
                              <Text style={styles.textColor}>Project Name: {this.state.projects.name}</Text>
                              <Text style={styles.textColor}>description: {this.state.description}</Text>
                              <Text style={styles.textColor}>Offer: {'$ '}{this.state.projects.reference_price}</Text>
                              {this.state.activeMsg === false ? <View /> : this.state.projects.project_questions.length > 0 ? this.state.projects.project_questions.map((items, index) => {
                                  if (items.applicant_answer.length === 0) {
                                      return (
                                          <Text key={index} style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                              Q#{index + 1} {items.question}
                                          </Text>
                                      );
                                  } else {
                                      return (
                                          <View key={index}>
                                              <Text style={{ color: Config.primaryColor, fontFamily: FontStyle.Light }}>
                                                  Q#{index + 1} {items.question}
                                              </Text>
                                              <Text style={styles.textColor}>{items.applicant_answer[index].answer}</Text>
                                          </View>
                                      );
                                  }
                              }) : <View />
                              }
                          </View>
                      )}
                  </View>
              </View>
              <TouchableOpacity>
                <Image source={{ uri: item.item.sender_profile_pic }} style={styles.avatarRightStyle} />
              </TouchableOpacity>
            </View>
      );
    }
  }

  projectDetail() {
    this.props.navigation.navigate('ProjectDetail', { project: this.state.projects });
  }
  acceptOffer() {
    const access_token = this.props.userData.token;
    const receiverId = this.state.receiverId;
    const inboxId = this.state.notification_id;
    this.setState({ isLoading: true });
    HttpRequest.acceptOffer(access_token, receiverId, inboxId)
      .then((response) => {
        let result = response.data;
        if (result.status === 'success') {
          this.setState({ isLoading: true });
          alert(result.message);
          setTimeout(() => {
            this.props.navigation.navigate('Notification');
          }, 2000);
        } else {
          this.setState({ isLoading: false });
          alert(result.message);
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        alert(error);
      });
  }
  rejectOffer() {
    const access_token = this.props.userData.token;
    const receiverId = this.state.receiverId;
    const inboxId = this.state.notification_id;
    this.setState({ rejectLoading: true });
    HttpRequest.rejectOffer(access_token, receiverId, inboxId)
      .then((response) => {
        let result = response.data;
        if (result.status === 'success') {
          this.setState({ rejectLoading: false });
          alert(result.message);
          setTimeout(() => {
            this.props.navigation.navigate('Notification');
          }, 2000);
        } else {
          alert(result.message);
        }
      })
      .catch((error) => {
        this.setState({ rejectLoading: false });
        alert(error);
      });
  }
  deleteMsg() {
    const access_token = this.props.userData.token;
    const inboxId = this.state.notification_id;
    HttpRequest.deleteInboxChat(access_token, inboxId)
      .then((response) => {
        let result = response.data;
        if (result.status === 'success') {
          this.props.navigation.navigate('Notification');
        } else {
          alert(result.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }
  delRequest() {
    const accessToken = this.props.userData.token;
    const id = this.state.userData.id;
    const clientEmail = this.state.row.client_email;
    HttpRequest.deleteSharedOfficeBookingRequest(accessToken, id, clientEmail)
      .then((response) => {
        if (response.data.status === 'success') {
          this.setState({ confirmDelete: false });
          alert(translate('deleted_success'));
          this.props.navigation.navigate('Notification');
        } else {
          this.setState({ confirmDelete: false });
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }
  replyMessage() {
    const accessToken = this.props.userData.token;
    const message = this.state.replyText;
    const sendTo = this.state.row.user_id;
    this.setState({ replyLoading: true });
    HttpRequest.sendSharedOfficeReplyMessage(accessToken, message, sendTo)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          alert(translate('reply_success'));
          this.setState({ isReply: false, replyLoading: false });
        } else {
          alert(result.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  changeText = (text) => {
    this.setState({ replyText: text });
  };
  awardProject() {
    // TODO create award Project API
    const access_token = this.props.userData.token;
    const inboxId = this.state.notification_id;
    this.setState({ isLoading: true });
    HttpRequest.awardProject(access_token, inboxId)
      .then((response) => {
        let result = response.data;
        if (result.status === 'success') {
          this.setState({ isLoading: false });
          alert(result.message);
          setTimeout(() => {
            this.props.navigation.navigate('Notification');
          }, 2000);
        } else {
          this.setState({ isLoading: false });
          alert(result.message);
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        alert(error);
      });
  }
  rejectProject() {
    const access_token = this.props.userData.token;
    const inboxId = this.state.notification_id;
    const receiverId = this.state.sender_id;
    this.setState({ rejectLoading: true });
    HttpRequest.rejectProjectOffer(access_token, inboxId, receiverId)
      .then((response) => {
        let result = response.data;
        console.log('response---<<', response);
        if (result.status === 'success') {
          this.setState({ rejectLoading: false });
          alert(result.message);
          setTimeout(() => {
            this.props.navigation.navigate('Notification');
          }, 1500);
        } else {
          this.setState({ rejectLoading: false });
          alert(result.message);
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        alert(error);
      });
  }

  render() {
    if (this.state.loading === true) {
      return (
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator color={Config.primaryColor}/>
          </View>
      );
    }
    return (
          <View style={styles.rootStyle}>
            {this.state.category === 'Receive New Message' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.messageWrapperRead}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={styles.inputTextWrapperStyle}>
                        <View style={styles.inputBoxStyle}>
                            <TextInput style={styles.inputTextStyle}
                                       placeholder={translate('type_your_message')}
                                       autoCapitalize='none'
                                       autoCorrect={false}
                                       multiline={true}
                                       underlineColorAndroid='transparent'
                                       onChangeText={msg => this.setState({ msg })}
                                       value={this.state.msg}
                            />

                        </View>

                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => { this.sendMessage(); }}
                        >
                            <Text>{translate('send')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            {this.state.category === 'Invite you to project' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.footerContent}>
                            <View style={styles.buttonWrapper}>
                                <TouchableOpacity
                                    style={[styles.actionButton, styles.activeButton]}
                                    onPress={() => { this.projectDetail(); }}
                                >
                                    <Text style={[styles.buttonText, styles.activeButtonText]}>{translate('job_details')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.actionButton}
                                    onPress={() => { this.deleteMsg(); }}
                                >
                                    <Text style={styles.buttonText}>{translate('delete')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.messageWrapper1}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={styles.inputTextWrapperStyle}>
                        <View style={styles.inputBoxStyle}>
                            <TextInput style={styles.inputTextStyle}
                                       placeholder={translate('type_your_message')}
                                       autoCapitalize='none'
                                       autoCorrect={false}
                                       multiline={true}
                                       underlineColorAndroid='transparent'
                                       onChangeText={msg => this.setState({ msg })}
                                       value={this.state.msg}
                            />

                        </View>

                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => { this.sendMessage(); }}
                        >
                            <Text>{translate('send')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            {this.state.category === 'Offer Received' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        {this.state.projectStatus === 1 && (
                        <View style={[
                            styles.footerContent,
                            this.state.sender_id !== this.state.userId ? {} : styles.footerHeight,
                        ]}>
                            {this.state.sender_id !== this.state.userId && (
                            <View style={styles.buttonWrapper}>
                                <TouchableOpacity
                                    style={[styles.actionButton, styles.activeButton]}
                                    onPress={() => { this.awardProject(); }}
                                >
                                    {this.state.isLoading === true && (
                                        <ActivityIndicator color="#fff" />
                                    )}
                                    {this.state.isLoading === false && (
                                        <Text style={[styles.buttonText, styles.activeButtonText]}>{translate('accept')}</Text>
                                    )}
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.actionButton}
                                    onPress={() => { this.rejectProject(); }}
                                >

                                    {this.state.rejectLoading === true && (
                                        <ActivityIndicator color={Config.primaryColor} />
                                    )}
                                    {this.state.rejectLoading === false && (
                                        <Text style={styles.buttonText}>{translate('reject')}</Text>
                                    )}
                                </TouchableOpacity>
                            </View>)}
                        </View>)}
                        <View style={styles.messageWrapper1}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderOfferReceivedRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={[styles.inputTextWrapperStyle, this.state.projectStatus === 2 || this.state.projectStatus === 3 || this.state.projectStatus === 4 || this.state.projectStatus === 5 ? styles.projectStatus : {}]}>
                        <View style={styles.inputBoxStyle}>
                            <TextInput style={styles.inputTextStyle}
                                       placeholder={translate('type_your_message')}
                                       autoCapitalize='none'
                                       autoCorrect={false}
                                       multiline={true}
                                       underlineColorAndroid='transparent'
                                       onChangeText={msg => this.setState({ msg })}
                                       value={this.state.msg}
                            />

                        </View>

                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => { this.sendMessage(); }}
                        >
                            <Text>{translate('send')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            {this.state.category === 'Send Offer' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.footerContent}>
                            <View style={styles.buttonWrapper}>
                                <TouchableOpacity
                                    style={[styles.actionButton, styles.activeButton]}
                                    onPress={() => { this.acceptOffer(); }}
                                >
                                    {this.state.isLoading === true && (
                                        <ActivityIndicator color="#fff" />
                                    )}
                                    {this.state.isLoading === false && (
                                        <Text style={[styles.buttonText, styles.activeButtonText]}>{translate('accept')}</Text>
                                    )}
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.actionButton}
                                    onPress={() => { this.rejectOffer(); }}
                                >

                                    {this.state.rejectLoading === true && (
                                        <ActivityIndicator color="#fff" />
                                    )}
                                    {this.state.rejectLoading === false && (
                                        <Text style={styles.buttonText}>{translate('reject')}</Text>
                                    )}
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.messageWrapper1}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderOfferRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={styles.inputTextWrapperStyle}>
                        <View style={styles.inputBoxStyle}>
                            <TextInput style={styles.inputTextStyle}
                                       placeholder={translate('type_your_message')}
                                       autoCapitalize='none'
                                       autoCorrect={false}
                                       multiline={true}
                                       underlineColorAndroid='transparent'
                                       onChangeText={msg => this.setState({ msg })}
                                       value={this.state.msg}
                            />

                        </View>

                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => { this.sendMessage(); }}
                        >
                            <Text>{translate('send')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            {this.state.category === 'Applicant Accepted' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.messageWrapperRead}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderOfferRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                    <View style={styles.inputTextWrapperStyle}>
                        <View style={styles.inputBoxStyle}>
                            <TextInput style={styles.inputTextStyle}
                                       placeholder={translate('type_your_message')}
                                       autoCapitalize='none'
                                       autoCorrect={false}
                                       multiline={true}
                                       underlineColorAndroid='transparent'
                                       onChangeText={msg => this.setState({ msg })}
                                       value={this.state.msg}
                            />

                        </View>
                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => { this.sendMessage(); }}
                        >
                            <Text>{translate('send')}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
            {this.state.category === 'User ask for contact' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.footerContent}>
                            <TouchableOpacity
                                style={styles.actionButton}
                                onPress={() => { this.deleteMsg(); }}
                            >
                                <Text style={styles.buttonText}>{translate('delete')}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.messageWrapper1}>
                            <FlatList
                                ref={ref => this.flatList = ref}
                                onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
                                onLayout={() => this.flatList.scrollToEnd({ animated: true })}
                                showsVerticalScrollIndicator={false}
                                data={this.state.chatData}
                                renderItem={item => this.renderRow(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        <View style={styles.inputTextWrapperStyle}>
                            <View style={styles.inputBoxStyle}>
                                <TextInput style={styles.inputTextStyle}
                                           placeholder={translate('type_your_message')}
                                           autoCapitalize='none'
                                           autoCorrect={false}
                                           multiline={true}
                                           underlineColorAndroid='transparent'
                                           onChangeText={msg => this.setState({ msg })}
                                           value={this.state.msg}
                                />

                            </View>
                            <TouchableOpacity
                                style={{ paddingLeft: 20 }}
                                onPress={() => { this.sendMessage(); }}
                            >
                                <Text>{translate('send')}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )}
            {this.state.category === 'Shared Office Booking' && (
                <View style={styles.rootStyle}>
                    <View style={styles.topContent}>
                        <View style={styles.profileWrapper}>
                            <AvatarWithStatus diameter={80} indicatorColor={Config.primaryColor}
                                              source={{ uri: this.state.userImageAvatar }}/>
                            <Text style={styles.profileTitle}>{this.state.sender_name}</Text>
                        </View>
                        <View style={styles.footerContent}>
                            <View style={styles.buttonWrapper}>
                                <TouchableOpacity
                                    style={styles.actionButton}
                                    onPress={() => { this.setState({ confirmDelete: true }); }}
                                >
                                    <Text style={styles.buttonText}>{translate('delete')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.actionButton}
                                    onPress={() => { this.setState({ isReply: true }); }}
                                >
                                    <Text style={styles.buttonText}>{translate('reply')}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.messageWrapper1}>
                            {/* ***** */}
                            <View style={styles.rowLeftStyle}>
                                <Image source={{ uri: this.state.userData.img_avatar }} style={styles.avatarLeftStyle} />
                                <Image source={require('./../../images/private_chat/corner_1.png')}
                                       style={{
                                           height: 10, width: 10, tintColor: '#fff', marginRight: -2, marginTop: 5,
                                       }} />
                                <View style={styles.infoStyle}>
                                    <View style={styles.bubbleLeftStyle}>
                                        <View style={styles.bubbleRightProjects}>
                                            <Text style={{ color: Config.primaryColor }}>{translate('send_booking_request')}</Text>
                                            <Text style={styles.textColor}>{translate('name')} : {this.state.row.full_name}</Text>
                                            <Text style={styles.textColor}>{translate('no_of_person')} : {this.state.row.no_of_persons}</Text>
                                            <Text style={styles.textColor}>{translate('no_of_rooms')} : {this.state.row.no_of_rooms}</Text>
                                            <Text style={styles.textColor}>{translate('phone_no')} : {this.state.row.phone_no}</Text>
                                            <Text style={styles.textColor}>{translate('check_in_date')} : {this.state.row.check_in}</Text>
                                            <Text style={styles.textColor}>{translate('check_out_date')} :{this.state.row.check_out}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            {/* ***** */}
                        </View>
                    </View>
                </View>
            )}
            <Modal
                onRequestClose={() => this.setState({ confirmDelete: false })}
                animationType="fade"
                transparent={true}
                visible={this.state.confirmDelete}
            >
              <View style={styles.modalContainerStyle}>
                <View style={styles.modalBoxStyle}>
                  <View style={styles.powerButton}>
                    <View style={styles.powerIcon}>
                      <AntDesign name="delete"
                                   color={ Config.primaryColor }
                                   style={{ left: 5, top: 5 }}
                                   size={45}
                      />
                    </View>
                  </View>
                  <View
                      style={{
                        height: deviceWidth / 3.16,
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                    <Text
                        style={{ fontFamily: FontStyle.Bold, fontSize: 16 }}
                    >{translate('are_you_sure_logout')}</Text>
                    <Text
                        style={{ fontFamily: FontStyle.Bold, fontSize: 16 }}
                    >{translate('delete')}?</Text>
                  </View>
                  <View style={styles.confirmButtonWrapper}>
                    <TouchableOpacity
                        onPress={() => {
                          this.delRequest();
                        }}
                        style={styles.yes}
                    >
                      <Text style={{ color: Config.white, fontFamily: FontStyle.Regular }}>{translate('yes')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                          this.setState({ confirmDelete: false });
                        }}
                        style={styles.no}
                    >
                      <Text style={{ color: Config.white, fontFamily: FontStyle.Regular }}>{translate('no')}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>

            <Modal
                animationType="fade"
                transparent={true}
                onRequestClose={() => {
                  this.setState({
                    isReply: false,
                  });
                }}
                visible={this.state.isReply}>
              <View style={styles.dialogStyle}>
                <View style={styles.dialogBoxStyle}>

                  <View style={styles.applyStyle}>
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                    }}>
                      <Text style={styles.text6Style}>{translate('reply_box')}</Text>
                    </View>
                    <TouchableOpacity style={{
                      flex: 1,
                      flexDirection: 'column',
                      marginTop: 5,
                    }} onPress={() => this.setState({ isReply: false })}>
                      <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        marginTop: 0,
                      }}>
                        <Image source={require('../../images/jobs/cross.png')}
                               style={styles.avatarStyle1}/>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{
                    marginTop: 5,
                    paddingHorizontal: 0,
                  }}>

                    <View style={styles.amountInputWrapper}>
                      <TextInput
                          underlineColorAndroid="transparent"
                          placeholder={translate('please_write')}
                          onChangeText={(text) => this.changeText(text)}
                          value={this.state.replyText}
                          multiline={true}
                          numberOfLines={5}
                          style={{ paddingLeft: 5, alignItems: 'flex-start', color: '#aaaaaa', }}/>
                    </View>
                  </View>
                    <TouchableOpacity
                        style={styles.buttonWrapper2}
                        onPress={() => this.replyMessage()}
                    >
                        {this.state.replyLoading ? (
                            <ActivityIndicator size="small" color={Config.white} />
                        ) : (
                      <Text style={styles.fontC}>{translate('reply')}</Text>
                            )}
                    </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
    );
  }
}

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 48,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}
const styles = StyleSheet.create({
  rootStyle: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },

  modalContainerStyle: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  footerHeight: {
    height: 52,
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  projectStatus: {
    top: deviceWidth / 7,
  },
  dialogBoxStyle: {
    width: 350,
    backgroundColor: '#fff',
    borderRadius: 3,
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    // top: -15,
    resizeMode: 'contain',
    position: 'absolute',
    right: 0,
  },
  applyStyle: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    marginTop: 5,
    paddingBottom: 5,
    borderColor: '#ecf0f1',
    borderBottomWidth: 1,
  },
  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: '#e6e6e6',
  },
  buttonWrapper2: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: '95%',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 7,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    borderRadius: 5,
  },

  fontC: {
    fontSize: 12,
    color: '#fff',
  },

  modalBoxStyle: {
    width: '80%',
    height: deviceWidth / 1.59,
    backgroundColor: Config.white,
    borderRadius: 10,
  },
  powerButton: {
    height: deviceWidth / 5,
    borderTopEndRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },

  powerIcon: {
    backgroundColor: Config.white,
    borderRadius: 50,
    height: 55,
    width: 55,
    alignSelf: 'center',
  },

  confirmButtonWrapper: {
    flexDirection: 'row',
  },

  yes: {
    backgroundColor: Config.primaryColor,
    flex: 0.5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomLeftRadius: 5,
  },
  no: {
    backgroundColor: Config.textSecondaryColor,
    flex: 0.5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomEndRadius: 5,
  },

  innerWrapper: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
  },

  inputTextWrapperStyle: {
    backgroundColor: '#fff',
    paddingVertical: 5,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },

  rowLeftStyle: {
    flexDirection: 'row',
    marginVertical: 5,
    marginRight: 5,
  },
  rowRightStyle: {
    flexDirection: 'row',
    marginVertical: 5,
    marginLeft: 5,
  },
  bubbleRightStyle: {
    backgroundColor: Config.primaryColor,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderTopRightRadius: 0,
  },
  bubbleRightProjects: {
    backgroundColor: '#f8f8f8',
    borderRadius: 10,
    width: deviceWidth / 1.6,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderTopRightRadius: 0,
    marginTop: 5,
  },
  avatarRightStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginHorizontal: 5,
  },

  avatarLeftStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginHorizontal: 0,
  },

  infoStyle: {
    flexDirection: 'column',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 5,
  },
  bubbleLeftStyle: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    borderTopLeftRadius: 0,
  },

  inputBoxStyle: {
    flex: 1,
    backgroundColor: '#f3f3f3',
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  inputTextStyle: {
    fontSize: 14,
  },

  footerContent: {
    width: '100%',
    alignItems: 'center',
  },

  buttonWrapper: {
    flexDirection: 'row',
    width: '90%',
    justifyContent: 'space-between',
    marginBottom: 15,
  },

  profileWrapper: {
    backgroundColor: '#f9f9f9',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    paddingVertical: 15,
  },
  textColor: {
    color: Config.textSecondaryColor,
    fontFamily: FontStyle.Light,
  },

  profileTitle: {
    fontFamily: 'Raleway-Medium',
    fontSize: 18,
    marginTop: 5,
  },
  profileSub: {
    fontFamily: 'Raleway-Medium',
    fontSize: 15,
    marginTop: 5,
  },

  messageWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    height: deviceHeight - 330,
  },

  messageWrapperRead: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    height: deviceHeight - 295,
  },

  messageWrapper1: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    height: deviceHeight - 350,
  },

  messageText: {
    fontFamily: 'Raleway-Medium',
  },

  paragraph: {
    marginTop: 7,
    marginBottom: 7,
  },

  actionButton: {
    backgroundColor: '#fff',
    borderColor: Config.primaryColor,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    width: '45%',
    borderRadius: 4,
    height: 45,
  },

  activeButton: {
    backgroundColor: Config.primaryColor,
  },

  buttonText: {
    fontFamily: 'Raleway-Medium',
    fontSize: 18,
    color: Config.primaryColor,
  },

  activeButtonText: {
    color: '#fff',
  },
});
const mapStateToProps = state => ({
  component: state.component,
  userData: state.auth.userData,
});

export default connect(mapStateToProps)(NotificationDetail);
