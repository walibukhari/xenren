/* eslint-disable consistent-return */
/* eslint-disable max-len */
/* eslint-disable array-callback-return */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */
/* eslint-disable import/first */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Modal,
  Linking,
  ScrollView,
  Image,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Dimensions,
  Alert,
  SafeAreaView
} from 'react-native';
import Config from '../Config';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { translate } from '../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import FontStyle from '../constants/FontStyle';
import HttpRequest from '../components/HttpRequest';
import Swiper from 'react-native-swiper';
import { WebView } from 'react-native-webview';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import Feather from "react-native-vector-icons/Feather";

const lunchIcon = require('./chatDetails/lunch-icon1.png');
const machIcon = require('./chatDetails/mach-icon2.png');
const printIcon = require('./chatDetails/print-icon1.png');
const varfcIcon = require('./chatDetails/var-fc-icon2.png');
const wifiIcon = require('./chatDetails/wifi-icon1.png');

const { width } = Dimensions.get('window');
class ChatDetail extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      phoneNum: null,
      imageData: '',
      imageData1: '',
      showPic: false,
      showOnePic: false,
      onePicArr: [],
      dataSource: [
        {
          uri: require('./chatDetails/banner-thumb1.jpg'),
        },
        {
          uri: require('./chatDetails/banner-thumb2.jpg'),
        },
        {
          uri: require('./chatDetails/banner-thumb3.jpg'),
        },
      ],
      prices: [
        {
          title: 'Seat',
          perHour: '3.5$/hr',
          perMonth: '300$/month',
        },
      ],
      meetingRoom: [
        {
          title: 'Meeting room',
          perHour: '3$/hr',
          perMonth: '300$/month',
        },
      ],
      userInfo: this.props.userData,
      location: this.props.navigation.state.params.chatData.location,
      url: 'https://maps.google.com/maps?q=',
      mapView: '',
      imageLoading: true,
      setPage: '',
    };
  }

  componentDidMount() {
      console.log('now in chat detail');
      console.log(this.props.navigation.state.params);
      console.log(this.props.navigation.state.params.chatData);
      console.log(this.props.navigation.state.params.chatData.id);
      const officeID = this.props.navigation.state.params.chatData.id;
    const userID = this.state.userInfo.id;
    HttpRequest.getSharedOfficeDetail(this.state.userInfo.token, userID, officeID)
      .then((response) => {
        const result = response.data[0];
        const imagesArr = [];
        if (result.success === true) {
          const url = this.state.url + this.state.location;
            this.setState({
            phoneNum: result.contact_Phone,
            // isLoading: false,
            data: result,
            mapView: url,
          });
          result.moreimages.map((item) => {
            imagesArr.push({
              uri: `${item.name_image}`,
            });
          });
          const arr = [];

          arr.push({
            uri: `${result.image}`,
          });
          const val = [...arr, ...imagesArr];
            this.setState({ imageData: val, imageData1: val, isLoading: false })
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        alert(error);
      });
  }

  openPic(image) {
    let img = image.name_image;
      let array = [];
      let pushData = [];
      this.state.imageData1.map((item) => {
          if (item.uri != img) {
              array.push(item);
          }
      });
      pushData.push({
          uri: img,
      });
      let newData = [...pushData, ...array];

      this.setState({
      imageData1: newData,
      showPic: true,
    });
  }
    goBackPage() {
        this.props.navigation.navigate('SharedOfficeFiltersMaps', {
            access_token: this.props.userData.token,
            sharedOffices: this.state.list,
            ip4: this.state.ip4,
            lat: this.state.latitude,
            lng: this.state.longitude
        });
    }
  showOnePic() {
    this.setState({ showOnePic: !this.state.showOnePic });
  }
    openLink = () => {
      const url = this.state.mapView;
        Linking.canOpenURL(url)
        .then((supported) => {
          if (supported) {
            Linking.openURL(url);
          } else {
              Alert.alert("Error", translate('cannot_open_link'), [
                  {
                      text: "OK",
                  }
              ]);
          }
        })
        .catch(err => console.error(err));
    };

    onLoad = () => {
      this.setState({ imageLoading: false });
    };

  renderMoreImages = (image, index) => (
    <TouchableOpacity key={index} onPress={() => this.openPic(image)}>
      {this.state.imageLoading === true && (
          <ActivityIndicator style={{ top: 50 }} />
      )}
      <Image
        style={styles.imageOverlay}
        source={{ uri: `${image.name_image}` }}
        onLoad={this.onLoad}
      />
    </TouchableOpacity>
  );

  renderFacilities = (faclity) => {
    let facilityWidth = 50,
      facilityHeight = 50,
      textStyle = {
        width: facilityWidth + 12,
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
      },
      facilityStyle = {
        width: facilityWidth,
        height: facilityHeight,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
      },
      imageStyle = {
        width: '100%',
        height: '100%',
      };
    if (faclity.name === 'WiFi') {
      return (
        <View>
          <View style={facilityStyle}>
            <Image style={imageStyle} source={wifiIcon} />
          </View>
          <View style={textStyle}>
            <Text>{translate('wifi')}</Text>
          </View>
        </View>
      );
    } else if (faclity.name === 'Lunch') {
      return (
        <View>
          <View style={facilityStyle}>
            <Image style={imageStyle} source={lunchIcon} />
          </View>
          <View style={textStyle}>
            <Text>{translate('lunch')}</Text>
          </View>
        </View>
      );
    } else if (faclity.name === 'Vending Machine') {
      return (
        <View>
          <View style={facilityStyle}>
            <Image style={imageStyle} source={machIcon} />
          </View>
          <View style={textStyle}>
            <Text>{translate('machine')}</Text>
          </View>
        </View>
      );
    } else if (faclity.name === 'Printer') {
      return (
        <View>
          <View style={facilityStyle}>
            <Image style={imageStyle} source={printIcon} />
          </View>
          <View style={textStyle}>
            <Text>{translate('print')}</Text>
          </View>
        </View>
      );
    }
    return (
      <View>
        <View style={facilityStyle}>
          <Image style={imageStyle} source={varfcIcon} />
        </View>
        <View style={textStyle}>
          <Text>{translate('faculity')}</Text>
        </View>
      </View>
    );
  };

  openDailer = () => {
    if (this.state.phoneNum !== null) {
      const url = `tel://${this.state.phoneNum}`;
      Linking.openURL(url);
    } else {
        Alert.alert("Error", translate('phone_not_available'), [
            {
                text: "OK",
            }
        ]);
    }
  };

  renderKm = () => {
      if(this.props.navigation.state.params.chatData.total_distance){
          return this.props.navigation.state.params.chatData.total_distance.toFixed(2)
      } else {
          return 0;
      }
  }

  render() {
      let header =  <View style={style.rootStyle}>
          {/* Back Button View */}
          <View style={style.navigationStyle}>
              <View style={style.navigationWrapper}>
                  <TouchableOpacity
                      onPress={() => this.goBackPage()}
                      style={{
                          marginTop: 8,
                          marginLeft: 4,
                          flexDirection: "row",
                      }}
                  >
                      <Image
                          style={{width:20,height:26,position:'relative',top:-3}}
                          source={require('../../images/arrowLA.png')}
                      />

                      <Text onPress={() => {
                          this.goBackPage();
                      }} style={{
                          marginLeft: 10,
                          marginTop:0,
                          fontSize: 19,
                          fontWeight:'normal',
                          color: Config.primaryColor,
                          fontFamily: FontStyle.Regular,
                      }}>{
                          this.props.navigation.state.params.locale === 'cn' || this.props.navigation.state.params.locale === 'tw' || this.props.navigation.state.params.locale === 'zh' ? this.props.navigation.state.params.chatData.version_chinese === 'true' ? this.props.navigation.state.params.chatData.office_name_cn : this.props.navigation.state.params.chatData.office_name : this.props.navigation.state.params.chatData.version_english === 'false' || this.props.navigation.state.params.chatData.version_english === null ? this.props.navigation.state.params.chatData.office_name_cn : this.props.navigation.state.params.chatData.office_name
                      }
                      </Text>
                  </TouchableOpacity>
              </View>
          </View>
      </View>

      if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <ActivityIndicator />
        </View>
      );
    }
    if (this.state.data.length !== 0) {
      const officeData = this.state.data;
      const arr = [];
      arr.push({ source: { uri: `${officeData.image}` } });
      return (
            <View
                style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}
            >
                {header}
                {this.state.isLoading}
      <ScrollView
        style={styles.rootStyle}
        contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        keyboardShouldPersistTaps="always"
        horizontal={false}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() =>
              this.setState({
                showOnePic: true,
                onePicArr: arr,
              })
            }
            style={{ width: '95%' }}
          >
            <Image
              style={{
                width: '100%',
                height: 200,
              }}
              resizeMode="stretch"
              source={{ uri: `${officeData.image}` }}
            />
          </TouchableOpacity>

          {/* Render More Images */}
          <View
            style={{
              width: '95%',
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={officeData.moreimages}
              contentStyle={{}}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    flex: 0.3,
                    paddingLeft: index === 0 ? 0 : 6,
                    paddingTop: 5,
                  }}
                >
                  {this.renderMoreImages(item, index)}
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          <View style={styles.activeUserscontainer}>
            <View>
              <Text
                style={{
                  paddingLeft: 10,
                  fontSize: 14,
                  color: '#404040',
                }}
              >
                  {translate('active_users')}
              </Text>
            </View>
            <View style={{ flexDirection: 'column' }}>
              <Text
                style={{
                  paddingRight: 10,
                  fontSize: 14,
                  color: '#404040',
                }}
              >
                {' '}
                <FontAwesome
                  style={{
                    color: '#000000',
                  }}
                >
                  {Icons.users}
                </FontAwesome>{' '}
                {officeData.active_user_last_30_days}
              </Text>
            </View>
          </View>

          <View style={styles.currentCheckIncontainer}>
            <Text
              style={{
                paddingRight: 10,
                paddingLeft: 10,
                fontSize: 14,
                color: 'white',
              }}
            >
              {' '}
              <FontAwesome>{Icons.users}</FontAwesome>{' '}{translate('current_checkIn_users')}{' '}{officeData.current_check_in_users || 0}
            </Text>
          </View>

          {/* For Prices */}
          <FlatList
            data={officeData.price}
            style={{
              width: '95%',
            }}
            renderItem={({ item }) => (
              <View
                style={{
                  paddingBottom: 3,
                  borderBottomColor: '#e9e9e9',
                  borderBottomWidth: 2,
                }}
              >
                <Text
                  style={{
                    color: '#404040',
                    fontSize: 24,
                  }}
                >
                    {translate('price_per')}{item.name.replace(/[0-9]/g, '').replace('.', '')}
                </Text>
                <View
                  style={{
                    width: '100%',
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <Text
                    style={{
                      color: Config.primaryColor,
                    }}
                  >
                    {item.time_price}$ / hr
                  </Text>
                  <Text
                    style={{
                      color: Config.primaryColor,
                    }}
                  >
                    {item.month_price}$ /{' '}{translate('month')}
                  </Text>
                </View>
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />

          <TouchableOpacity
            onPress={() => this.openDailer()}
            style={styles.activeUserscontainer}
          >
            <Text
              style={{
                paddingRight: 10,
                paddingLeft: 10,
                fontSize: 14,
                color: '#404040',
              }}
            >
              {' '}
              <FontAwesome>{Icons.phone}</FontAwesome> {translate('contact_no')}.:{' '}
              {this.state.phoneNum}
            </Text>
          </TouchableOpacity>

          {/* For Facilities */}
          <View style={styles.facilityContainer}>
            <Text
              style={{
                fontSize: 16,
                marginBottom: 5,
              }}
            >
                {translate('what_facility')}
            </Text>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={officeData.facility}
              style={{
                width: '100%',
              }}
              renderItem={({ item }) => (
                <View
                  style={{
                    paddingHorizontal: 5,
                  }}
                >
                  {this.renderFacilities(item)}
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          <View style={styles.facilityContainer}>
            <Text
              style={{
                fontSize: 16,
              }}
            >
              {translate('distance_from_location')}
            </Text>
            <Text
              style={{
                fontSize: 13,
                marginBottom: 5,
                marginTop: 5,
                color: '#939393',
              }}
            >
                {this.renderKm()}{' '}{translate('km_far_away')}
            </Text>
                  <TouchableOpacity
                      disable={true}
                      onPress={() => this.openLink()}
                      style={{
                          width: width * 1.78,
                          right: 6,
                          height: width / 1.7,
                          overflow:'hidden'
                      }}>
                      <WebView
                          source={{html: "<div class='mapouter'><div class='gmap_canvas'><iframe width='500' height='500' id='gmap_canvas' src='" + this.state.mapView + "&output=embed' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' style='border:0; pointer-events: none;'></iframe>Werbung: <a href='https://www.xenren.co'>xenren.co</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:500px;}</style></div>"}}
                      />
                  </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            marginTop: 15,
            marginBottom: 15,
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            onPress={() => this.openDailer()}
            style={{
              width: '49%',
              height: 40,
              borderRadius: 3,
              backgroundColor: Config.primaryColor,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontSize: 15,
                color: 'white',
                textAlign: 'center',
              }}
            >
                {translate('contact')}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('IndoorChat', {
                sharedOfficeData: { office: officeData.office_details, locale: this.props.navigation.state.params.locale },
              })
            }
            style={{
              width: '49%',
              height: 40,
              borderRadius: 3,
              borderWidth: 1,
              borderColor: Config.primaryColor,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text
              style={{
                fontSize: 15,
                color: Config.primaryColor,
                textAlign: 'center',
              }}
            >
                {translate('join_chat_room')}
            </Text>
          </TouchableOpacity>
        </View>

        <Modal
          transparent={true}
          supportedOrientations={['portrait', 'landscape']}
          visible={this.state.showPic}
          onRequestClose={() => this.setState({ showPic: false }) }
        >
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#2C2C2C',
                }}
            >
                <SafeAreaView style={{ flex: 1 }}>
                    <TouchableOpacity
                        onPress={() => this.setState({ showPic: false })}
                        style={{ alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}
                    >
                        <Entypo name="cross" size={30} color={'#fff'}/>
                    </TouchableOpacity>
                    <Swiper
                        showsButtons={true}
                        nextButton={<Ionicons
                            name="ios-arrow-forward"
                            color={Config.white}
                            size={80}
                        />}
                        prevButton={<Ionicons
                            name="ios-arrow-back"
                            color={Config.white}
                            size={80}
                        />}
                    >
                        {this.state.imageData1.map((item, index) => {
                            return (
                                <View key={index} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image
                                        source={item}
                                        resizeMode="stretch"
                                        style={{ height: 250, width: width }}
                                    />
                                </View>
                            );
                        })}
                    </Swiper>
                </SafeAreaView>
            </View>
        </Modal>

        {/* Showing one pic */}

        <Modal
          transparent={true}
          supportedOrientations={['portrait', 'landscape']}
          visible={this.state.showOnePic}
          onRequestClose={() => this.setState({ showOnePic: false }) }
        >
          <View
            style={{
              flex: 1,
              backgroundColor: '#2C2C2C',
            }}
          >
              <SafeAreaView style={{ flex: 1 }}>
                  <TouchableOpacity
                      style={{ alignItems: 'flex-end', marginTop: 30, marginRight: 10 }}
                      onPress={() => this.showOnePic()}
                  >
                      <Entypo name="cross" size={30} color={'#fff'}/>
                  </TouchableOpacity>
                  <Swiper
                      showsButtons={true}
                      nextButton={<Ionicons
                          name="ios-arrow-forward"
                          color={Config.white}
                          size={80}
                      />}
                      prevButton={<Ionicons
                          name="ios-arrow-back"
                          color={Config.white}
                          size={80}
                      />}
                  >
                      {this.state.imageData.map((item, index) => {
                          return (
                              <View key={index} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                  <Image
                                      source={item}
                                      resizeMode="stretch"
                                      style={{ height: 250, width: width }}
                                  />
                              </View>
                          );
                      })}
                  </Swiper>
              </SafeAreaView>
          </View>
        </Modal>
      </ScrollView>
    </View>
      );
    }
    if (this.state.data.length === 0) {
      return (
          <View>
              {header}
            <View style={[styles.container, { justifyContent: 'center' }]}>
                <Text>{translate('went_wrong')}</Text>
            </View>
          </View>
      );
    }
  }
}

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

const styles = {
  rootStyle1: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#2C2C2C',
  },
  container3: {
    flex: 1,
    justifyContent: 'flex-end',

    backgroundColor: '#f7f7f7',
    width: '100%',
    // ///////////////////////
    height: '100%',
  },
  bigImageWrapperStyle: {
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 5,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    width: width * 0.86,
    height: width * 0.86,
    borderRadius: width * 0.43,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerButtonStyle: {
    fontWeight: 'bold',
    color: Config.textColor,
    textAlign: 'center',
  },
  rootStyle: {
    flexDirection: 'column',
    width: '95%',
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    width: '100%',
    borderRadius: 5,
  },
  largeFont: {
    fontSize: 16,
    color: '#404040',
    marginTop: 5,
  },
  mediumFont: {
    fontSize: 13,
    color: '#585858',
    width: 100,
  },
  activeUserscontainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    width: '95%',
    backgroundColor: '#E9E9E9',
    justifyContent: 'space-between',
  },
  currentCheckIncontainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    width: '95%',
    backgroundColor: Config.primaryColor,
  },
  container1: {
    width: '95%',
    marginTop: 5,
    paddingBottom: 5,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  container2: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    paddingBottom: 5,
  },
  facilityContainer: {
    width: '95%',
    marginTop: 10,
    paddingBottom: 5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  greyedTextDescriptionStyle: {
    color: '#c3c3c3',
    fontSize: 18,
    fontWeight: '100',
  },
  imageOverlay: {
    height: 100,
    width: 100,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatDetail);
