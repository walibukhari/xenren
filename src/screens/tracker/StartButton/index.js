/* eslint-disable import/first */
/* eslint-disable react/prop-types */
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import startButtonStyles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { translate } from '../../../i18n';

const StartButton = ({
  onPress,
}) => (
    <TouchableOpacity
    onPress={onPress}
    style={startButtonStyles.button}>
    <Icon name='ios-play' color='white' size={14} style={{ marginRight: 15 }} />
        <Text style={startButtonStyles.btnTxt}>
            {translate('START')}
        </Text>
    </TouchableOpacity>
);

export default StartButton;
