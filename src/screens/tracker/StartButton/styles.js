import { StyleSheet } from 'react-native';
import Config from '../../../Config';

const startButtonStyles = StyleSheet.create({
  button: {
    width: '100%',
    height: '90%',
    flexDirection: 'row',
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:2,
  },
  btnTxt: {
    color: 'white',
    fontSize: 18,
  },
});

export default startButtonStyles;
