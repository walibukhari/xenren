/* eslint-disable react/jsx-key */
/* eslint-disable react/prop-types */
/* eslint-disable max-len */
/* eslint-disable react/jsx-no-comment-textnodes */
import React from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  Picker,
  ScrollView,
  ActivityIndicator,
  PickerIOS,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import trackerStyles from './styles';
import Config from '../../../Config';
import { translate } from '../../../i18n';
var PickerItemIOS = PickerIOS.Item;

const mapIcon = require('../../chatDetails/maps.png');
const uploadPhotoIcon = require('../../../../images/other/uploadPhoto.png');
const uploadVideoIcon = require('../../../../images/other/uploadVideo.png');

const TimeTracker = ({
                       projectVal,
                       onProjectChange,
                       memoVal,
                       onMemoChange,
                       pickerItems,
                       isShowingTime,
                       loading,
                       onUploadImage,
                       onUploadVideo,
                     }) => (
  <ScrollView>
    <Image
      style={trackerStyles.mapImage}
      source={mapIcon}
    />

    <View style={[trackerStyles.inputView, trackerStyles.memo]}>
      <Text style={trackerStyles.inputText}>
          {translate('memo')}
      </Text>
      <TextInput
        placeholder={translate('write_memo')}
        underlineColorAndroid='transparent'
        multiline={true}
        value={memoVal}
        onChangeText={onMemoChange}
      />
    </View>
    {
      isShowingTime
        ? <View style={{
          alignItems: 'center',
          marginTop: 50,
        }}>
          <Text style={{ fontSize: 26 }}>{translate('today')}</Text>
          <Text style={{ fontSize: 72 }}>00:00:00</Text>
          <Text>{translate('weekly_limit')}</Text>
          <View style={trackerStyles.uploadView}>
            <TouchableOpacity style={trackerStyles.uploadButtons} onPress={onUploadImage}>
              <Image style={trackerStyles.uploadIcons}
                     source={uploadPhotoIcon}
              />
              <Text>{translate('upload_photo')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={trackerStyles.uploadButtons} onPress={onUploadVideo}>
              <Image style={trackerStyles.uploadIcons}
                     source={uploadVideoIcon}
              />
              <Text>{translate('upload_video')}</Text>
            </TouchableOpacity>
          </View>
        </View> : <Text/>
    }

      <View style={trackerStyles.inputView}>
          <Text style={trackerStyles.inputText}>
              {translate('Project')}
          </Text>
          {loading ? <ActivityIndicator color={Config.primaryColor} size='small'/>
            :
            <Picker
              selectedValue={projectVal}
              onValueChange={onProjectChange}
              style={{
                height: 50,
                width: '100%',
              }}>
              <Picker.Item label={translate('select_project')} value={'0'}/>
              {pickerItems.map(item => (
                <Picker.Item label={item.title} value={item.title} key={item.id}/>
              ))
              }
            </Picker>}
      </View>
  </ScrollView>
);

TimeTracker.propTypes = {
  projectVal: PropTypes.string,
  onProjectChange: PropTypes.func,
  memoVal: PropTypes.string,
  onMemoChange: PropTypes.func,
};

export default TimeTracker;
