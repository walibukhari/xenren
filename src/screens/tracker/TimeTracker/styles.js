import { StyleSheet } from 'react-native';

const trackerStyles = StyleSheet.create({
  mapImage: {
    width: '100%',
    height: 100,
    marginBottom: 10,
  },
  inputView: {
    width: '90%',
    alignSelf: 'center',
    borderColor: '#ddd',
    borderWidth: 1,
    height: 65,
    marginTop: 10,
    paddingHorizontal: 5,
    paddingVertical: 2,
    borderRadius: 3,
  },
  memo: {
    height: 90,
  },
  inputText: {
    fontWeight: '600',
  },
  uploadView: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  uploadButtons: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  uploadIcons: {
    height: 50, width: 50, resizeMode: 'contain', marginBottom: 10,
  },
});

export default trackerStyles;
