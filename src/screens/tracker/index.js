/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-console */
/* eslint-disable class-methods-use-this */
import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    Image,
    TextInput,
    ActivityIndicator,
    Picker,
    ScrollView,
    Platform,
    ActionSheetIOS
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import TimeTracker from './TimeTracker';
import trackingStyles from './styles';
import StartButton from './StartButton';
import StopAndPauseButton from './StopAndPauseButton';
import HttpRequest from '../../components/HttpRequest';
import LocalData from '../../components/LocalData';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {translate} from '../../i18n';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import trackerStyles from "./TimeTracker/styles";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import {NavigationActions} from "react-navigation";

export default class Tracker extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            selectedProject: '0',
            items: [],
            memo: '',
            showTimer: false,
            isLoading: true,
        };
    }

    componentDidMount = () => {
        console.log('tracker/index.js')
        LocalData.getUserData()
            .then((res) => {
                res = JSON.parse(res);
                HttpRequest.getUsersPortfolio(`bearer ${res.token}`)
                    .then((resp) => {
                        if (resp.status === 'success') {
                            this.setState({
                                items: resp.data.portfolios,
                                isLoading: false,
                            });
                        } else {
                            alert(translate('went_wrong'));
                            this.setState({isLoading: false});
                        }
                    })
                    .catch((e) => {
                        alert(e);
                        this.setState({isLoading: false});
                    });
            });
    };

    _onStartingProject = () => {
        if (this.state.selectedProject === '0') {
            alert(translate('select_a_project'));
        } else {
            this.setState({showTimer: true});
        }
    };

    _uploadMedia = (type) => {
        ImagePicker.showImagePicker({
            quality: 1,
            mediaType: type,
            title: `Select a ${type}`,
        }, (res) => {
            if (res.didCancel) {
            } else if (res.error) {
            } else {
                alert(res.uri);
            }
        });
    };

    renderPicker = () => {
        if(Platform.OS == 'android') {
            return <Picker
                selectedValue={this.state.selectedProject}
                onValueChange={item => this.setState({selectedProject: item})}
                style={{
                    height: 50,
                    width: '100%',
                }}>
                <Picker.Item label={translate('select_project')} value={'0'}/>
                {this.state.items.map(item => (
                    <Picker.Item label={item.title} value={item.title} key={item.id}/>
                ))
                }
            </Picker>
        } else {
            return <TouchableOpacity style={{
                borderColor: "lightgrey",
                height: 40,
                width: 134,
                color: "9A9C9E"
            }} onPress={this.selectProjects.bind(this)}>
                <Text style={{
                    height: 40,
                    backgroundColor: '#ffffff',
                    paddingLeft: 15,
                    paddingRight: 15,
                    paddingTop: 15,
                    paddingBottom: 15,
                    color: "9A9C9E"
                }}>
                    Select Project
                </Text>
            </TouchableOpacity>
        }
    }

    selectProjects() {
        let obj = this.state.items.map((item) => {
            return item.title;
        });
        console.log(obj);
        if(obj.length < 1) {
            alert('No Project Found...!');
            return null;
        }
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: obj,
            },
            (buttonIndex) => {
                // if (buttonIndex === 1) {
                console.log(buttonIndex);
                if(buttonIndex == 0) {
                    this.setState({
                        selectCurrencyLabel: "USD"
                    });
                } else {
                    this.setState({
                        selectCurrencyLabel: "yuan"
                    });
                }
                /* destructive action */
                // }
            });
    }
    goBack = () => {
        this.props.navigation.goBack();
    };
    render() {
        ('project value', this.state.selectedProject);
        return (
            <View style={trackingStyles.container}>
                <View style={styles.rootStyle}>
                        {/* Back Button View */}
                        <View style={styles.navigationStyle}>
                            <View style={styles.navigationWrapper}>
                                <TouchableOpacity
                                    onPress={this.goBack}
                                    style={{
                                        marginTop: 8,
                                        marginLeft: 10,
                                        flexDirection: "row",
                                    }}
                                >
                                    <Image
                                        style={{width:20,height:26,position:'relative',top:-3}}
                                        source={require('../../../images/arrowLA.png')}
                                    />

                                    <Text
                                        style={{
                                            marginLeft: 10,
                                            marginTop:-1,
                                            fontSize: 19.5,
                                            fontWeight:'normal',
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Regular,
                                        }}
                                    >
                                        {translate("tracker")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                <ScrollView>
                    <Image
                        style={trackerStyles.mapImage}
                        source={require('../chatDetails/maps.png')}
                    />

                    <View style={[trackerStyles.inputView, trackerStyles.memo]}>
                        <Text style={trackerStyles.inputText}>
                            {translate('memo')}
                        </Text>
                        <TextInput
                            placeholder={translate('write_memo')}
                            underlineColorAndroid='transparent'
                            multiline={true}
                            value={this.state.memo}
                            onChangeText={memo => this.setState({memo})}
                        />
                    </View>
                    {
                        this.state.showTimer
                            ? <View style={{
                                alignItems: 'center',
                                marginTop: 50,
                            }}>
                                <Text style={{fontSize: 26}}>{translate('today')}</Text>
                                <Text style={{fontSize: 72}}>00:00:00</Text>
                                <Text>{translate('weekly_limit')}</Text>
                                <View style={trackerStyles.uploadView}>
                                    <TouchableOpacity style={trackerStyles.uploadButtons} onPress={onUploadImage}>
                                        <Image style={trackerStyles.uploadIcons}
                                               source={uploadPhotoIcon}
                                        />
                                        <Text>{translate('upload_photo')}</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={trackerStyles.uploadButtons} onPress={onUploadVideo}>
                                        <Image style={trackerStyles.uploadIcons}
                                               source={uploadVideoIcon}
                                        />
                                        <Text>{translate('upload_video')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View> : <Text/>
                    }

                    <View style={trackerStyles.inputView}>
                        <Text style={trackerStyles.inputText}>
                            {translate('Project')}
                        </Text>
                        {
this.state.isLoading ? <ActivityIndicator color={Config.primaryColor} size='small'/> : this.renderPicker()
                        }
                    </View>
                </ScrollView>
                <View style={trackingStyles.startButton}>
                    {
                        this.state.showTimer === false
                            ? <StartButton
                                onPress={() => this._onStartingProject()}
                            /> :
                            <StopAndPauseButton
                                onPausePress={() => alert(translate('PAUSE'))}
                                onStopPress={() => this.setState({showTimer: false})}
                            />
                    }
                </View>
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}
