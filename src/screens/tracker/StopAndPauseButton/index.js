/* eslint-disable react/prop-types */
import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Config from '../../../Config';
import startButtonStyles from '../StartButton/styles';
import stopButtonStyles from './styles';
import { translate } from '../../../i18n';

const StopAndPauseButton = ({
  onStopPress,
  onPausePress,
}) => (
    <View style={stopButtonStyles.container}>
    <TouchableOpacity
    style={stopButtonStyles.pauseButton}
    onPress={onPausePress}>
    <Icon name='ios-pause' color='white' size={14} style={{ marginRight: 15 }} />
        <Text style={startButtonStyles.btnTxt}>{translate('PAUSE')}</Text>
    </TouchableOpacity>
    <TouchableOpacity
    style={stopButtonStyles.stopButton}
    onPress={onStopPress}>
    <Icon name='ios-square' color={Config.primaryColor} size={14} style={{ marginRight: 15 }} />
        <Text style={[startButtonStyles.btnTxt, { color: Config.primaryColor }]}>{translate('STOP')}</Text>
    </TouchableOpacity>
    </View>
);

export default StopAndPauseButton;
