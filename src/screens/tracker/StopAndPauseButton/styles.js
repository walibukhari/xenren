import { StyleSheet } from 'react-native';
import Config from '../../../Config';

const stopButtonStyles = StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  pauseButton: {
    width: '65%',
    height: 40,
    backgroundColor: Config.primaryColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  stopButton: {
    width: '30%',
    height: 40,
    borderColor: Config.primaryColor,
    borderWidth: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default stopButtonStyles;
