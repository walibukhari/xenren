import { StyleSheet } from 'react-native';

const trackingStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  startButton: {
    height: 50, bottom: 0, width: '90%', alignSelf: 'center', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',
  },
});

export default trackingStyles;
