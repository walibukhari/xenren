/* eslint-disable no-mixed-operators */
/* eslint-disable arrow-parens */
/* eslint-disable no-return-assign */
/* eslint-disable no-else-return */
/* eslint-disable object-curly-spacing */
/* eslint-disable key-spacing */
/* eslint-disable object-curly-newline */
/* eslint-disable prefer-const */
/* eslint-disable comma-dangle */
/* eslint-disable quotes */
/* eslint-disable max-len */
/* eslint-disable semi */
/* eslint-disable indent */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-const-assign */
/* eslint-disable global-require */
/* eslint-disable react/no-deprecated */
import React, {Component} from "react";
import {connect} from "react-redux";
import {
    Dimensions,
    Text,
    View,
    ActivityIndicator,
    TouchableOpacity,
    Alert,
    FlatList,
    Image,
    TextInput,
    Modal,
    Keyboard,
    Linking,
    Animated,
    UIManager,
    findNodeHandle,
    Clipboard,
    KeyboardAvoidingView
} from "react-native";

import Config from "../Config";
import {translate} from "../i18n";
import HttpRequest from "../components/HttpRequest";
import FontStyle from "../constants/FontStyle";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

const {width, height} = Dimensions.get("window");

class ChatSingle extends Component {
    static navigationOptions = ({navigation}) => {
        const {params = {}} = navigation.state;
        return {
            title: navigation.state.params.userInfo.real_name,
            headerTintColor: Config.topNavigation.headerIconColor,
            headerTitleStyle: {
                color: Config.topNavigation.headerTextColor,
                alignSelf: "center",
                fontFamily: FontStyle.Regular,
                width: "100%"
            },
            headerLeft: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack();
                    }}
                    style={{
                        marginLeft: 10,
                        flexDirection: "row",
                        alignSelf: "center",
                        padding: 5
                    }}
                >
                    <SimpleLineIcons
                        size={16}
                        name="arrow-left"
                        color={Config.topNavigation.headerIconColor}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('ContactManagement', {userInfo: params.userInfo})
                    }}
                >
                    <Image
                        source={require("./../../images/private_chat/icon_manage.png")}
                        style={{
                            width: 30,
                            height: 30,
                            resizeMode: "contain",
                            marginRight: 10
                        }}
                    />
                </TouchableOpacity>
            )
        };
    };

    TextInputEl;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            chatData: [],
            showModal: false,
            userInfo: null,
            msg: "",
            contactModel: false,
            shift: new Animated.Value(0),
        };
    }

    componentWillMount() {
        this.setState({
            userInfo: this.props.navigation.state.params.userInfo
        });
        this.props.navigation.setParams({
            handleManageContact: () => this.handleManageContact
        });

        Keyboard.addListener('keyboardDidChangeFrame', (event) => {
            const { height: ScreenHeight } = Dimensions.get('screen');
            const KeyboardHeight = (ScreenHeight - event.endCoordinates.screenY);
            const gap = KeyboardHeight > 0 ? KeyboardHeight * -1 : KeyboardHeight;
            Animated.timing(
                this.state.shift,
                {
                    toValue: gap,
                    duration: 100,
                    useNativeDriver: true,
                }
            ).start();
        });

    }

    componentDidMount() {
        console.log('ChatSingle.js')
        this.getMessagesAPI();
    }

    handleManageContact() {
        this.props.navigation.navigate("ContactManagement", {
            userInfo: this.state.userInfo
        });
    }

    handlePress = () => {
        if (
            this.state.userInfo.handphone_no !== "" &&
            this.state.userInfo.handphone_no !== null
        ) {
            const url = `http://api.whatsapp.com/send?phone=${this.state.userInfo.handphone_no}`;
            Linking.canOpenURL(url)
                .then(supported => {
                    if (!supported) {
                        console.log(`Can't handle url: ${url}`);
                    } else {
                        return Linking.openURL(url);
                    }
                })
                .catch(err => console.log(err));
        } else {
            Alert.alert("Warning", translate('handphone_not_available'), [
                {
                    text: "OK",
                }
            ]);
        }
    };
    handlePhone = (phone, raw) => {
        this.setState({contactModel: false}, () => {
            if (phone !== null) {
                Clipboard.setString(phone);
                alert(`${raw}\n${phone}\n${translate('copy_to_clipboard')}!`);
                setTimeout(() => {
                    Alert.alert(translate('copy_to_clipboard'), `${raw}\n${phone}\n${translate('copy_to_clipboard')}!`, [
                        {
                            text: "OK",
                        }
                    ]);
                }, 200)
            } else {
                setTimeout(() => {
                    Alert.alert("Warning", translate('handphone_not_available'), [
                        {
                            text: "OK",
                        }
                    ]);
                }, 200)
            }
        });
        // if (
        //   this.state.userInfo.handphone_no !== "" &&
        //   this.state.userInfo.handphone_no !== null
        // ) {
        //   // `whatsapp://send?phone`
        //   const url = `tel://${this.state.userInfo.handphone_no}`;
        //   Linking.canOpenURL(url)
        //     .then(supported => {
        //       if (!supported) {
        //         console.log(`Can't handle url: ${url}`);
        //       } else {
        //         return Linking.openURL(url);
        //       }
        //     })
        //     .catch(err => console.log("An error occurred", err));
        // } else {
        //   alert("Hand Phone number not available,");
        // }
    };

    getMessagesAPI = () => {
        // console.log('token is --', this.props.userData.token);
        HttpRequest.getSingleChat(this.props.userData.token, this.state.userInfo.id)
            .then(response => {
                // console.log(response.data);
                if (response.data.status === "success") {
                    this.setState({
                        chatData: response.data.data,
                        isLoading: false
                    });
                } else {
                    Alert.alert("Error", translate('network_error'), [
                        {
                            text: "OK",
                        }
                    ]);
                }
            })
            .catch((error) => {
                this.setState({isLoading: false});
                alert(error);
            });
    };

    sendMsgAPI = () => {
        if (this.state.msg !== "") {
            HttpRequest.sendMsgSingleChat(
                this.props.userData.token,
                this.state.msg,
                this.state.userInfo.id
            )
                .then(response => {
                    if (response.data.status === "success") {
                        // console.log('console send msg response -- ', response.data);
                    } else {
                        Alert.alert("Error", translate('network_error'), [
                            {
                                text: "OK",
                            }
                        ]);
                    }
                })
                .catch(error => {
                    alert(error);
                });
        }
    };

    sendMessage = () => {
        if (this.state.msg) {
            let newData = {
                from_user_id: this.props.userData.id,
                custom_message: this.state.msg
            };
            this.setState(prevState => ({
                chatData: [...prevState.chatData, newData],
                msg: ""
            }));
            this.sendMsgAPI();
        }
    };

    renderRow(item) {
        // console.log(item);
        if (this.state.chatData) {
            if (item.item.from_user_id === this.props.userData.id) {
                return (
                    <View style={styles.rowLeftStyle}>
                        <Image
                            source={{uri: this.props.userData.img_avatar}}
                            style={styles.avatarLeftStyle}
                        />
                        <Image
                            source={require("./../../images/private_chat/corner_1.png")}
                            style={{
                                height: 10,
                                width: 10,
                                tintColor: "#fff",
                                marginRight: -2,
                                marginTop: 5
                            }}
                        />
                        <View style={styles.infoStyle}>
                            <View style={styles.bubbleLeftStyle}>
                                <Text>{item.item.custom_message}</Text>
                            </View>
                        </View>
                    </View>
                );
            } else {
                return (
                    <View style={styles.rowRightStyle}>
                        <View style={[styles.infoStyle, {alignItems: "flex-end"}]}>
                            <View style={styles.bubbleRightStyle}>
                                <Text style={{color: "#fff"}}>
                                    {item.item.custom_message}
                                </Text>
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                this.handleUserProfile();
                            }}
                        >
                            <Image
                                source={{
                                    uri: `${item.item.from_user.img_avatar}`
                                }}
                                style={styles.avatarRightStyle}
                            />
                        </TouchableOpacity>
                    </View>
                );
            }
        }
    }

    writeToClipboard = async (contact, raw) => {
        if (contact !== null || contact !== undefined) {
            await Clipboard.setString(contact);
            Alert.alert(translate('copy_to_clipboard'), `${raw}\n${contact}\n${translate('copy_to_clipboard')}`, [
                {
                    text: "OK",
                }
            ]);
        } else {
            Alert.alert(translate('copy_to_clipboard'), `${raw}\n${translate('not_available_for_copied')}`, [
                {
                    text: "OK",
                }
            ]);
        }
    };

    render() {
        const { shift } = this.state;
        console.log(shift)
        return (
            <View style={styles.rootStyle}>
                <View style={styles.headerStyle}>
                    <TouchableOpacity
                        style={styles.socialButtonStyle}
                        onPress={() => {
                            this.writeToClipboard(this.state.userInfo.qq_id, 'qq');
                        }}
                    >
                        <Image
                            source={require("./../../images/private_chat/icon_qq.png")}
                            style={styles.socialIconStyle}
                        />
                        <Text
                            style={{
                                fontSize: 12,
                                color: "#000"
                            }}
                        >
                            {translate("qq")}
                        </Text>
                        <Text
                            style={{
                                fontSize: 10,
                                color: "#555"
                            }}
                        >
                            {this.state.userInfo.qq_id}
                        </Text>
                    </TouchableOpacity>
                    <View
                        style={{
                            width: 1,
                            backgroundColor: "#f7f7f7"
                        }}
                    />
                    <TouchableOpacity
                        style={styles.socialButtonStyle}
                        onPress={() => {
                            this.writeToClipboard(this.state.userInfo.skype_id, 'Skype');
                        }}
                    >
                        <Image
                            source={require("./../../images/private_chat/icon_skype.png")}
                            style={styles.socialIconStyle}
                        />
                        <Text
                            style={{
                                fontSize: 12,
                                color: "#000"
                            }}
                        >
                            {translate("skype")}
                        </Text>
                        <Text
                            style={{
                                fontSize: 10,
                                color: "#555"
                            }}
                        >
                            {this.state.userInfo.skype_id}
                        </Text>
                    </TouchableOpacity>
                    <View
                        style={{
                            width: 1,
                            backgroundColor: "#f7f7f7"
                        }}
                    />
                    <TouchableOpacity
                        style={styles.socialButtonStyle}
                        onPress={() => {
                            this.writeToClipboard(this.state.userInfo.wechat_id, 'WeChat');
                        }}
                    >
                        <Image
                            source={require("./../../images/private_chat/icon_wechat.png")}
                            style={styles.socialIconStyle}
                        />
                        <Text
                            style={{
                                fontSize: 12,
                                color: "#000"
                            }}
                        >
                            {translate("wechat")}
                        </Text>
                        <Text
                            style={{
                                fontSize: 10,
                                color: "#555"
                            }}
                        >
                            {this.state.userInfo.wechat_id}
                        </Text>
                    </TouchableOpacity>
                    <View
                        style={{
                            width: 1,
                            backgroundColor: "#f7f7f7"
                        }}
                    />
                    <TouchableOpacity
                        style={styles.socialButtonStyle}
                        onPress={() => {
                            this.setState({contactModel: true});
                        }}
                    >
                        <Image
                            source={require("./../../images/private_chat/icon_phone.png")}
                            style={styles.socialIconStyle}
                        />
                        <Text
                            style={{
                                fontSize: 12,
                                color: "#000"
                            }}
                        >
                            {translate("phone")}
                        </Text>
                        <Text
                            style={{
                                fontSize: 10,
                                color: "#555"
                            }}
                        >
                            {this.state.userInfo.handphone_no}
                        </Text>
                    </TouchableOpacity>
                </View>

                {this.state.isLoading && (
                    <View
                        style={{
                            position: "absolute",
                            top: height * 0.5 - 50,
                            left: width * 0.5 - 10
                        }}
                    >
                        <ActivityIndicator/>
                    </View>
                )}
                <FlatList
                    ref={ref => (this.flatList = ref)}
                    onContentSizeChange={() =>
                        this.flatList.scrollToEnd({animated: true})
                    }
                    onLayout={() => this.flatList.scrollToEnd({animated: true})}
                    showsVerticalScrollIndicator={false}
                    data={this.state.chatData}
                    renderItem={item => this.renderRow(item)}
                    keyExtractor={(item, index) => index.toString()}
                />

                <View style={styles.inputTextWrapperStyle}>
                    <View style={styles.inputBoxStyle}>
                        <Animated.View style={[styles.container, {transform: [{translateY: shift}]}]}>
                            <KeyboardAvoidingView>
                                <TextInput
                                    style={styles.inputTextStyle}
                                    ref={(ref) => this.TextInputEl = ref }
                                    placeholder={translate("type_your_message")}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    multiline={true}
                                    underlineColorAndroid="transparent"
                                    onChangeText={msg => this.setState({ msg })}
                                    value={this.state.msg}
                                />
                            </KeyboardAvoidingView>
                            <TouchableOpacity
                                style={{ width: 35, position: 'absolute', right: -30, top: 18}}
                                onPress={() => {
                                    this.sendMessage();
                                }}
                            >
                                <Text>{translate('send')}</Text>
                            </TouchableOpacity>
                        </Animated.View>
                    </View>
                </View>

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showModal}
                    onRequestClose={() => console.log("Modal Closed")}
                >
                    <View style={styles.dialogStyle}>
                        <View style={styles.dialogBoxStyle}>
                            <View style={{height: 20}}/>
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignItems: "center",
                                    height: 50,
                                    paddingHorizontal: 10
                                }}
                            >
                                <Image
                                    source={require("../../images/other/icon_phone_laptop.png")}
                                    style={{
                                        height: 40,
                                        width: 40,
                                        tintColor: Config.primaryColor,
                                        marginRight: 10
                                    }}
                                    resizeMode="contain"
                                />
                                <Text
                                    style={{
                                        flex: 1,
                                        fontSize: 10
                                    }}
                                >
                                    {translate("platform_discussion_issue")}
                                </Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignItems: "center",
                                    height: 50,
                                    paddingHorizontal: 10
                                }}
                            >
                                <Image
                                    source={require("../../images/other/icon_announce.png")}
                                    style={{
                                        height: 40,
                                        width: 40,
                                        tintColor: Config.primaryColor,
                                        marginRight: 10
                                    }}
                                    resizeMode="contain"
                                />
                                <Text
                                    style={{
                                        flex: 1,
                                        fontSize: 10
                                    }}
                                >
                                    {translate("advertise_job")}
                                </Text>
                            </View>
                            <View style={{height: 20}}/>
                            <View
                                style={{
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: "#f36c4f",
                                    paddingVertical: 10,
                                    paddingHorizontal: 10,
                                    height: 50
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 10,
                                        fontWeight: "bold",
                                        color: "#fff",
                                        textAlign: "center"
                                    }}
                                >
                                    {translate("rules_voilation")}
                                </Text>
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    height: 40
                                }}
                            >
                                <TouchableOpacity
                                    style={{
                                        flex: 1,
                                        justifyContent: "center",
                                        alignItems: "center",
                                        borderBottomLeftRadius: 10
                                    }}
                                    onPress={() => {
                                        this.setState({showModal: false});
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: "#f36c4f",
                                            fontSize: 11,
                                            fontWeight: "bold"
                                        }}
                                    >
                                        {translate("LEAVE")}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{
                                        flex: 1,
                                        justifyContent: "center",
                                        alignItems: "center",
                                        borderBottomRightRadius: 10,
                                        backgroundColor: Config.primaryColor
                                    }}
                                    // onPress={() => { this.copyToClipboard(); }}
                                >
                                    <Text
                                        style={{
                                            color: "#fff",
                                            fontSize: 11,
                                            fontWeight: "bold"
                                        }}
                                    >
                                        {translate("AGREE")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Modal
                    animationType={'fade'}
                    transparent={true}
                    visible={this.state.contactModel}
                    onRequestClose={() => {
                        this.setState({
                            contactModel: false,
                        });
                    }}>
                    <View style={styles.modalStyle}>
                        <TouchableOpacity style={{flex: 1}}
                                          onPress={() => {
                                              this.setState({
                                                  contactModel: false,
                                              });
                                          }}
                        />
                        <View style={styles.modalBoxStyle}>
                            <View style={styles.modalBoxWrapperStyle}>
                                <TouchableOpacity style={styles.modalButtonWrapperStyle}
                                                  onPress={() => this.handlePhone(this.state.userInfo.handphone_no, 'handphone')}
                                >
                                    <View style={styles.circleButtonStyle}>
                                        <Image source={require('./../../images/private_chat/copy.png')}
                                               style={{
                                                   width: 30, height: 30, resizeMode: 'contain', tintColor: '#fff',
                                               }}/>
                                    </View>
                                    <Text style={styles.textButtonStyle}>{translate('copy')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.modalButtonWrapperStyle}
                                                  onPress={() => {
                                                      this.handlePress();
                                                  }}
                                >
                                    <View style={styles.circleButtonStyle}>
                                        <Image source={require('./../../images/private_chat/whatsapp.png')}
                                               style={{
                                                   width: 30, height: 30, resizeMode: 'contain', tintColor: '#fff',
                                               }}/>
                                    </View>
                                    <Text style={styles.textButtonStyle}>WhatsApp</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        height: '100%',
        justifyContent: 'space-around',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%'
    },
    textInput: {
        backgroundColor: 'white',
        height: 100,
    },
    rootStyle: {
        backgroundColor: "#f3f3f3",
        flex: 1
    },
    headerStyle: {
        flexDirection: "row",
        height: 70,
        backgroundColor: "#fff"
    },
    socialButtonStyle: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    socialIconStyle: {
        height: 25,
        width: 25,
        resizeMode: "contain",
        marginBottom: 5
    },
    // rowStyle: {
    //     flexDirection: 'row',
    //     marginVertical: 5,
    // },
    // avatarStyle: {
    //     height: 50,
    //     width: 50,
    //     borderRadius: 25,
    //     marginHorizontal: 10
    // },
    infoStyle: {
        flexDirection: "column",
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "flex-start",
        marginTop: 5
    },
    rowLeftStyle: {
        flexDirection: "row",
        marginVertical: 5,
        marginRight: 5
    },
    rowRightStyle: {
        flexDirection: "row",
        marginVertical: 5,
        marginLeft: 5
    },
    avatarLeftStyle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        marginHorizontal: 0
    },
    avatarRightStyle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        marginHorizontal: 5
    },
    bubbleLeftStyle: {
        backgroundColor: "#fff",
        borderRadius: 10,
        padding: 10,
        borderTopLeftRadius: 0
    },
    bubbleRightStyle: {
        backgroundColor: Config.primaryColor,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderTopRightRadius: 0
    },
    inputTextWrapperStyle: {
        backgroundColor: "#fff",
        paddingVertical: 10,
        paddingHorizontal: 15,
        flexDirection: "row",
        alignItems: "center"
    },
    inputBoxStyle: {
        flex: 1,
        backgroundColor: "#f3f3f3",
        paddingVertical: 8,
        paddingHorizontal: 20,
        height: 40
    },
    inputTextStyle: {
        fontSize: 14,
        height: 50,
        top: 5,
        width: '120%',
        paddingLeft: 15,
        marginLeft: -12,
        paddingTop: 22,
        backgroundColor: "#fff",
    },
    dialogStyle: {
        flex: 1,
        backgroundColor: "rgba(44, 62, 80, 0.6)",
        alignItems: "center",
        justifyContent: "center"
    },
    dialogBoxStyle: {
        width: 280,
        height: 230,
        backgroundColor: "#fff",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    modalStyle: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        justifyContent: 'flex-end',
        flexDirection: 'column',
    },
    modalBoxStyle: {
        backgroundColor: '#fff',
        height: 220,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    modalTextStyle: {
        fontSize: 20,
        color: '#000',
    },
    modalBoxWrapperStyle: {
        flexDirection: 'row',
        marginTop: 30,
    },
    modalButtonWrapperStyle: {
        flexDirection: 'column',
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleButtonStyle: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: Config.primaryColor,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    textButtonStyle: {
        fontSize: 12,
    },
};

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setRoot: root =>
            dispatch({
                type: "set_root",
                root
            })
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatSingle);
