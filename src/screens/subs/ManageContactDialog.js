/* eslint-disable no-console */
/* eslint-disable react/no-string-refs */
/* eslint-disable react/jsx-key */
/* eslint-disable max-len */
/* eslint-disable no-lone-blocks */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-expressions */
/* eslint-disable array-callback-return */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/no-deprecated */
/* eslint-disable comma-dangle */
/* eslint-disable indent */
import React, { Component } from "react";
import {
  View,
  Modal,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ListView,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import Config from "../../Config";
import { translate } from "../../i18n";
import HttpRequest from "../../components/HttpRequest";

class ManageContactDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contacts: [],
      showDialog: this.props.showDialog,
      blockedUsersList: [],
      blockedUsersImages: [],
      data: [],
    };

    this._onPressRow = this._onPressRow.bind(this);
  }

  componentDidMount() {
    this.getUsersList();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.showDialog) {
      this.setState({ showDialog: nextProps.showDialog });
    }
  }

  blockMultipleUsers = () => {
    const data = {
      users: 2,
      platform: "",
      all: 0,
      status: 1
    };
    HttpRequest.blockMultipleUsers(this.props.userData.token, data).then(
      res => {}
    );
  };

  convertArrayToDataSource(arr) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return ds.cloneWithRows(arr);
  }

  getUsersList() {
    HttpRequest.blockUsersList(this.props.userData.token)
      .then((response) => {
        const result = response.data;
        if (result.data.length > 0) {
          const alreadyBlockedUsers = this.props.user_ids;

          const images = [];
          const ids_List = [];
          const userArr = [];
          result.data.map(item => {
            alreadyBlockedUsers.includes(item.id)
              ? (item.isSelect = true)
              : (item.isSelect = false);
            if (this.props.userData.id !== item.id) { userArr.push(item); }
            if (item.isSelect) {
              images.push(item.img_avatar);
              ids_List.push(item.id);
            }
          });
          this.setState({
            blockedUsersList: ids_List,
            blockedUsersImages: images,
            contacts: userArr,
            dataSource: this.convertArrayToDataSource(userArr)
          });
          this.props.disableButton(true);
        } else {
          alert(translate('empty_list'));
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  renderRow = (item, sectionId, rowId) => (
    <TouchableOpacity onPress={() => this._onPressRow(rowId, item)}>
      <View style={styles.rowStyle}>
        <Image
          source={{ uri: `${item.img_avatar}` }}
          style={styles.avatarStyle}
        />
        <View style={styles.infoStyle}>
          <Text style={styles.memberNameStyle}>{item.real_name}</Text>
        </View>
        {item.isSelect && (
          <View style={styles.buttonMemberWrapperStyle}>
            <Entypo
              name="check"
              color={Config.primaryColor}
              size={40}
            />
          </View>
        )}
      </View>
    </TouchableOpacity>
  );

  _onPressRow(rowID, rowData) {
    clonedData = this.state.contacts.slice();
    clonedData[rowID] = {
      ...this.state.contacts[rowID],
      isSelect: !rowData.isSelect
    };
    const newDataSource = this.state.dataSource.cloneWithRows(clonedData);
    this.setState({ contacts: clonedData, dataSource: newDataSource });
    {
      this.blockUsers(rowID);
    }
  }

  donePressed() {
    this.props.callbackFromParent(
      this.state.blockedUsersImages,
      this.state.blockedUsersList
    );

    this.setState({ showDialog: false });
  }

  blockUsers = index => {
    if (!this.state.contacts[index].isSelect) {
      this.setState(
        {
          blockedUsersImages: [
            ...this.state.blockedUsersImages,
            this.state.contacts[index].img_avatar
          ],
          blockedUsersList: [
            ...this.state.blockedUsersList,
            this.state.contacts[index].id
          ]
        },
        () => {
          this.props.callbackFromParent(
            this.state.blockedUsersImages,
            this.state.blockedUsersList
          );
        }
      );
    } else {
      const itemIndex = this.state.blockedUsersList.indexOf(
        this.state.contacts[index].id
      );
      this.setState(
        prevState => ({
          blockedUsersImages: [
            ...prevState.blockedUsersImages.slice(0, itemIndex),
            ...prevState.blockedUsersImages.slice(itemIndex + 1)
          ],
          blockedUsersList: [
            ...prevState.blockedUsersList.slice(0, itemIndex),
            ...prevState.blockedUsersList.slice(itemIndex + 1)
          ]
        }),
        () => {
          this.props.callbackFromParent(
            this.state.blockedUsersImages,
            this.state.blockedUsersList
          );
        }
      );
    }
  };

  SearchFilter(item) {
   const newData = this.state.contacts.filter((dept) => {
     if (dept.name !== null) {
       const itemData = dept.name.toUpperCase();
       const textData = item.toUpperCase();
       return itemData.indexOf(textData) > -1;
     }
   });
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(newData),
    });
  }

  renderBlockedUsers = item =>
    item.map((image, index) => {
      return (
        <View key={index} style={{ marginVertical: 10 }}>
          <Image
            source={{ uri: `${image}` }}
            style={styles.selectedUser}
          />
        </View>
      );
    });

  render() {
    return (
      <Modal
        animationType="fade"
        onRequestClose={() => {
          this.setState({
            showDialog: !this.state.showDialog
          });
        }}
        transparent={true}
        visible={this.state.showDialog}
      >
        <View style={styles.modalContainerStyle}>
          <View style={styles.modalBoxStyle}>
            <View style={styles.modalHeaderWrapper}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <TouchableOpacity onPress={() => this.donePressed()}>
                  <Text
                    style={{
                      color: Config.primaryColor,
                      textAlign: "center",
                      fontSize: 15,
                      fontWeight: "bold"
                    }}
                  >
                    {translate('confirm')}
                  </Text>
                </TouchableOpacity>
                <Text style={styles.headerText}>{translate('select_user')}</Text>
                <TouchableOpacity onPress={() => this.donePressed()}>
                  <Text
                    style={{
                      color: Config.primaryColor,
                      textAlign: "center",
                      fontSize: 15,
                      fontWeight: "bold"
                    }}
                  >
                    {translate('cancel')}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.searchBoxWrapperStyle} ref="SearchInput">
                <TextInput
                  style={styles.inputStyle}
                  placeholder={translate("search")}
                  autoCapitalize="none"
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  onChangeText={(textFilter) => {
                    this.SearchFilter(textFilter);
                  }}
                />
                <TouchableOpacity
                  style={styles.searchButtonStyle}
                  onPress={() => {}}
                >
                  <Ionicons
                    name="ios-search"
                    color={Config.primaryColor}
                    size={20}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingHorizontal: 5, backgroundColor: "#f9f9f9" }}>
              <View
                style={{ flexDirection: "row", marginTop: 16, marginLeft: 10 }}
              >
                <ScrollView
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    {this.renderBlockedUsers(this.state.blockedUsersImages)}
                  </View>
                </ScrollView>
              </View>
            </View>
            <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderRow.bind(this)}
            />
            {/* <FlatList data={this.state.contacts}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => this.renderRow(item, index)}
            /> */}
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = {
  containerStyle: {
    backgroundColor: "#ffffff",
    justifyContent: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 3,
    flex: 1
  },

  modalBoxStyle: {
    width: "80%",
    height: "80%",
    backgroundColor: "#ffffff",
    borderRadius: 5
  },

  modalHeaderWrapper: {
    paddingHorizontal: 20,
    paddingTop: 20
  },

  modalContainerStyle: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  headerText: {
    fontWeight: "bold",
    color: "#000",
    fontSize: 18
  },

  searchBoxWrapperStyle: {
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "#fff",
    height: 40,
    width: "100%",
    flexDirection: "row",
    borderColor: "#000",
    borderWidth: 1
  },

  inputStyle: {
    flex: 1,
    height: 40,
    marginLeft: 10,
    fontSize: 12
  },

  searchButtonStyle: {
    padding: 10,
    backgroundColor: "transparent"
  },

  rootStyle: {
    backgroundColor: "#fff",
    flex: 1
  },

  rowStyle: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    flexDirection: "row",
    alignItems: "center"
  },

  avatarStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    margin: 10
  },

  infoStyle: {
    flexDirection: "column",
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },

  buttonMemberWrapperStyle: {
    paddingHorizontal: 10
  },

  memberNameStyle: {
    color: "#000",
    fontSize: 18
  },

  selectedUser: {
    height: 50,
    width: 50,
    marginRight: 10,
    borderRadius: 50,
  }
};

const mapStateToProps = state => {
  return {
    userData: state.auth.userData
  };
};

export default connect(
  mapStateToProps,
  null
)(ManageContactDialog);
