import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ListView,
  TextInput,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Config from '../../Config';
import Tab1 from './notification/Tab1';
import Tab2 from './notification/Tab2';
import Tab3 from './notification/Tab3';
import FooterTabs from '../FooterTabs';
import { translate } from '../../i18n';
import FontStyle from '../../constants/FontStyle';
import LocalData from "../../components/LocalData";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

class Notification extends Component {
    static navigationOptions = {
        header: null,
    };

  constructor(props) {
    super(props);
    console.log('notification.js')
    this.state = {
      screen: 1,
    };
  }

  switchScreen(screen) {
    this.setState({ screen });
  }

  renderButton(screen, flex, icon, label, iconWidth, iconHeight) {
    let color = '#95a5a6';
    if (this.state.screen == screen) {
      color = Config.primaryColor;
    }
    return (
      <TouchableOpacity style={[styles.itemButtonStyle, { flex }]}
                        onPress={() => {
                          this.switchScreen(screen);
                        }}>
        <Image source={icon}
               style={{
                 height: iconHeight,
                 width: iconWidth,
                 resizeMode: 'contain',
                 tintColor: color,
               }}/>
        <Text style={{
          color,
          fontSize: 12,
        }}> {label}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.rootStyle}>
          <View style={style.rootStyle}>
              {/* Back Button View */}
              <View style={style.navigationStyle}>
                  <View style={style.navigationWrapper}>
                      <TouchableOpacity
                          onPress={() => this.props.navigation.goBack()}
                          style={{
                              marginTop: 8,
                              marginLeft: 10,
                              flexDirection: "row",
                          }}
                      >
                          <Image
                              style={{width:20,height:26,position:'relative',top:-3}}
                              source={require('../../../images/arrowLA.png')}
                          />

                          <Text
                              onPress={() => this.props.navigation.goBack()}
                              style={{
                                  marginLeft: 10,
                                  marginTop:-1,
                                  fontSize: 19.5,
                                  fontWeight:'normal',
                                  color: Config.primaryColor,
                                  fontFamily: FontStyle.Regular,
                              }}
                          >
                              {translate("inbox")}
                          </Text>
                      </TouchableOpacity>
                  </View>
              </View>
          </View>
        <View style={{
          flex: 1,
          marginTop: 3,
        }}>
          {this.state.screen == 1 && <Tab1/>}
          {this.state.screen == 2 && <Tab2/>}
          {this.state.screen == 3 && <Tab3/>}
        </View>
        <View style={styles.footerStyle}>
          {this.renderButton(1, 2, require('../../../images/notification/icon_tab_1.png'), translate('all'), 20, 20)}

          <View style={{
            width: 1,
            height: 40,
            backgroundColor: '#dadada',
          }}/>

          {this.renderButton(2, 3, require('../../../images/notification/icon_tab_2.png'), translate('system_message'), 25, 25)}

          <View style={{
            width: 1,
            height: 40,
            backgroundColor: '#dadada',
          }}/>

          {this.renderButton(3, 2, require('../../../images/notification/icon_tab_3.png'), translate('inbox'), 20, 20)}
        </View>
        <FooterTabs navigation={this.props.navigation} currentIndex={3}/>
      </View>
    );
  }
}

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    // marginBottom: 10
  },
  footerStyle: {
    height: 40,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    // paddingVertical: 5,
    zIndex: 0,
  },
  itemButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
