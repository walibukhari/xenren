/* eslint-disable one-var */
/* eslint-disable no-return-assign */
/* eslint-disable no-lone-blocks */
/* eslint-disable no-else-return */
/* eslint-disable no-empty */
/* eslint-disable no-const-assign */
/* eslint-disable global-require */
/* eslint-disable prefer-const */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
/* eslint-disable quotes */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
} from 'react-native';
import Config from '../../../Config';
import HttpRequest from '../../../components/HttpRequest';
import LocalData from '../../../components/LocalData';
import { translate } from '../../../i18n';

class Tab1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadings: false,
      dataSource: [],
      count: 0,
      user: '',
    };
    this.root = this.props.component.root;
    this.navigation = this.root.props;
  }

  componentDidMount() {
    LocalData.getUserData()
      .then((response) => {
        response = JSON.parse(response);
        this.setState({ user: response });
        this.getNotification(response.token);
      });
  }

  getNotification(access_token) {
    this.setState({ isLoadings: true });
    HttpRequest.getAllNotifications(access_token)
      .then((response) => {
        const result = response;
        let resp = [];
        if (result.data.sharedOfficeBooking.length > 0) {
          result.data.sharedOfficeBooking.map((item) => {
            item.category = 'Shared Office Booking';
            item.check = true;
            item.message = 'Shared Office Booking Notification';
            item.is_read = false;
            item.time = item.created_at;
            resp.push(item);
          });
          resp = [...resp, ...result.data.data];
        } else {
          resp = result.data.data;
        }
        let arr = [];
        resp.map((item) => {
          if (item.category === 'User :nickname just come into project discussion room') {} else if (item.category === 'Congratulations, your job post is live.') {} else {
            if (item.category === 'Reject Send Offer') {
              item.category = 'Offer Received';
            }
            return arr.push(item);
          }
        });
        if (response.data.status === 'Success') {
          this.setState({
            count: result.data.data.length,
            dataSource: arr,
            isLoadings: false,
          });
        } else {
          alert(result.data.message);
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({ isLoadings: false });
      });
  }

  renderImage = (image, item) => {
    let path = require('../../../../images/notification/icon_project_ended.png');
    if (image === 'Invite you to project') {
      path = require('../../../../images/notification/icons/icon_i_accept_invitation_project.png');
    } else if (image === 'Applicant Accepted') {
      path = require('../../../../images/notification/icons/icon_i_accept_offer_of_project.png');
    } else if (image === 'Receive New Message') {
      path = require('../../../../images/notification/icon_list_message.png');
    } else if (image === 'Send Offer') {
      path = require('../../../../images/notification/icons/icon_i_send_my_offer.png');
    } else if (image === 'Project Completed') {
      path = require('../../../../images/notification/icons/icon_project_ended.png');
    } else if (image === 'Shared Office Booking') {
      path = require('../../../../images/notification/icon_list_message.png');
    } else {
      path = require('../../../../images/notification/icons/icon_i_post_offer2.png');
    }
    return path;
  };

  renderRow(row) {
    if (row.is_read) {
      return (
        <TouchableOpacity
          onPress={() =>
            this.navigation.navigation.navigate('NotificationDetail', {
              id: row.notification_id,
              data: row,
              sharedOffice: row.check ? true : false,
            })
          }
          style={styles.rowStyle}
        >
          <View style={styles.imageContainerStyle}>
            <Image
              source={this.renderImage(row.category, row)}
              style={[
                {
                  height: 40,
                  width: 40,
                  resizeMode: 'contain',
                },
                { tintColor: row.tintColor },
              ]}
            />
          </View>
          <View style={styles.informationContainerStyle}>
            <Text
              style={{
                fontSize: 14,
                color: '#95a5a6',
                marginBottom: 5,
              }}
              ellipsizeMode="tail"
              numberOfLines={2}
            >
              {row.message === 'Offer Received' && row.type === 16 ? 'Offer Rejected' : row.message}
            </Text>
            <Text style={{
              fontSize: 10,
              color: '#95a5a6',
            }}>{row.time} </Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
            <TouchableOpacity
                style={styles.rowStyle}
                onPress={() =>
                    this.navigation.navigation.navigate('NotificationDetail', {
                        id: row.notification_id,
                        data: row,
                        sharedOffice: row.check ? true : false,
                    })
                }
            >
                <View style={styles.imageContainerStyle}>
                    <Image
                        source={this.renderImage(row.category, row)}
                        style={[
                            {
                                height: 40,
                                width: 40,
                                resizeMode: 'contain',
                            },
                            { tintColor: Config.primaryColor },
                        ]}
                    />
                </View>
                <View style={styles.informationContainerStyle}>
                    <Text
                        style={{
                            fontSize: 14,
                            color: '#95a5a6',
                            marginBottom: 5,
                        }}
                        ellipsizeMode="tail"
                        numberOfLines={2}
                    >
                        {row.message}
                    </Text>
                    <Text style={{
                        fontSize: 10,
                        color: '#95a5a6',
                    }}>
                        {row.time}{' '}
                    </Text>
                </View>
            </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <Modal
          visible={this.state.isLoadings}
          animationType="fade"
          transparent={true}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View
            style={{
              backgroundColor: 'rgba(52, 52, 52, 0.8)',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <ActivityIndicator color="#fff"/>
          </View>
        </Modal>
        {this.state.count === 0 && (
          <View
            style={[
              styles.rootStyle,
              {
                justifyContent: 'center',
                alignItems: 'center',
              },
            ]}
          >
            <Image
              source={require('../../../../images/notification/icon_bell.png')}
              style={{
                width: 200,
                height: 200,
                resizeMode: 'contain',
                tintColor: '#bdc3c7',
                marginBottom: 50,
              }}
            />
            <Text
              style={{
                fontSize: 35,
                color: '#bdc3c7',
                marginBottom: 20,
                fontWeight: 'bold',
              }}
            >
                {translate('no_notifications')}
            </Text>
            <Text
              style={{
                fontSize: 20,
                color: '#bdc3c7',
                textAlign: 'center',
              }}
            >
              {translate('dont_have_notification')}
            </Text>
          </View>
        )}
        {this.state.count > 0 && (
          <FlatList
            data={this.state.dataSource.sort((a, b) => {
              let id_A = a.notification_id,
                id_B = b.notification_id;
              return id_B - id_A;
            })}
            renderItem={({ item }) => this.renderRow(item)}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },

  rowStyle: {
    height: 70,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ecf0f1',
  },

  imageContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRightWidth: 2,
    borderRightColor: Config.primaryColor,
  },

  informationContainerStyle: {
    flex: 1,
    flexDirection: 'column',
    height: 60,
    paddingHorizontal: 10,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Tab1);
