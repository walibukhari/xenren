/* eslint-disable quotes */
/* eslint-disable global-require */
/* eslint-disable max-len */
/* eslint-disable no-else-return */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, ListView, Image, TouchableOpacity, Modal, ActivityIndicator } from 'react-native';
import HttpRequest from '../../../components/HttpRequest';
import LocalData from '../../../components/LocalData';
import Config from '../../../Config';
import { translate } from '../../../i18n';

class Tab2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }),
      count: 0,
      isLoadings: false,
    };
  }
  componentDidMount() {
    LocalData.getUserData().then((response) => {
      response = JSON.parse(response);
      this.getSystemNotification(response.token);
    });
  }

  getSystemNotification(access_token) {
    HttpRequest.getSystemNotification(access_token).then((response) => {
      const result = response;
      // this.setState({getLanguagesArr : result.data})
      this.setState({ isLoadings: true });
      if (response.data.status === 'Success') {
        this.setState({ dataSource: this.state.dataSource.cloneWithRows(result.data.data) });
        this.setState({ isLoadings: false, count: result.data.data.length });
      } else {
        let error = result;
        throw error;
      }
    }).catch((error) => {
      this.setState({ isLoadings: false });
    });
  }

  borderColorProp(color) {
    if (color === Config.primaryColor) {
      return Config.primaryColor;
    } else {
      return Config.primaryColor;
    }
  }
  renderRow(row) {
    return (
          <TouchableOpacity style={styles.rowStyle} >
            {/* <View style={[styles.imageContainerStyle, { borderRightColor: this.borderColorProp(row.indicatorColor) }]}>
              <Image source={{uri : 'https://www.xenren.co/'+row.sender_profile_pic }} style={{
                height: 60, width: 60,
                resizeMode: 'contain',
                borderRadius: 30,
              }} />
              <View style={[styles.onlineIndicatorStyle, { backgroundColor: Config.primaryColor }]} />
            </View> */}
            <View style={styles.informationContainerStyle}>
              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <Text style={{ fontSize: 14, color: '#34495e', flex: 1 }}>{row.category}</Text>
                <Text style={{ fontSize: 10, color: '#95a5a6' }}>{row.time}</Text>
              </View>
              <Text style={{ fontSize: 10, color: '#95a5a6' }}
                ellipsizeMode='tail' numberOfLines={1}>{row.category}</Text>
              <View style={{ flexDirection: 'row', marginTop: 3 }}>
                <Text style={{ fontSize: 10, color: '#95a5a6' }}>{row.message} : </Text>
                <Text style={{ fontSize: 10, color: '#95a5a6', flex: 1 }}
                  ellipsizeMode='tail' numberOfLines={2}>{row.message}</Text>
              </View>
            </View>
          </TouchableOpacity>
    );
  }
  render() {
    if (this.state.count === 0) {
      return (
            <View style={[styles.rootStyle, {
                justifyContent: 'center',
            alignItems: 'center',
            }]}>
                <Image source={require('../../../../images/notification/icon_bell.png')}
                    style={{
                        width: 200,
                        height: 200,
                        resizeMode: 'contain',
                        tintColor: '#bdc3c7',
                        marginBottom: 50,
                    }} />
                <Text style={{
                    fontSize: 35, color: '#bdc3c7', marginBottom: 20, fontWeight: 'bold',
                }}>
                    {translate('no_notifications')}
                </Text>
                <Text style={{ fontSize: 20, color: '#bdc3c7', textAlign: 'center' }}>
                    {translate('dont_have_notification')}
                </Text>
            </View>
      );
    } else {
      return (
    <View style={styles.rootStyle}>
            <Modal
                  visible={this.state.isLoadings}
                  animationType="fade"
                  transparent={true}
                  onRequestClose={() => console.log("Modal Closed")}>
                 <View style={{
                     backgroundColor: 'rgba(52, 52, 52, 0.8)',
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                }}>
                     <ActivityIndicator color='#fff' />
                 </View>
            </Modal>
        <ListView
            dataSource={this.state.dataSource}
            renderRow={(rowData) => {
            return this.renderRow(rowData);
            }}
        />
    </View>);
    }
  }
}
const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  rowStyle: {
    height: 80,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ecf0f1',
  },
  imageContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 70,
    height: 70,
    borderRightWidth: 1,
  },
  informationContainerStyle: {
    flex: 1,
    flexDirection: 'column',
    height: 60,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  onlineIndicatorStyle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    position: 'absolute',
    right: 7,
    bottom: 7,
    borderWidth: 1,
    borderColor: '#fff',
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Tab2);
