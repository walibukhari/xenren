/* eslint-disable no-undef */
/* eslint-disable no-useless-concat */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
/* eslint-disable indent */
/* eslint-disable semi */
/* eslint-disable quotes */
/* eslint-disable object-curly-newline */
/* eslint-disable max-len */
/* eslint-disable global-require */
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  Alert,
  Share,
  ScrollView,
  Image,
  TouchableOpacity,
  Modal,
  Platform,
  ActivityIndicator, Dimensions,
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { NavigationActions } from "react-navigation";
import ImagePicker from "react-native-image-picker";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import FeatherIcon from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { translate } from "../../i18n";
import LocalData from "../../components/LocalData";
import Config from "../../Config";
import FooterTabs from "../FooterTabs";
import FontStyle from "../../constants/FontStyle";
import HttpRequest from "../../components/HttpRequest";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import { Icon } from 'react-native-elements';

const { width, height } = Dimensions.get("window");
class Profile extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      showLogoutDialog: false,
      userInfos: {},
      imageModal: false,
      isLoadings: false,
      loading: false,
      saveLoading: false,
      ImageShow: false,
      tmpImg: "",
      imageLoading: this.props.userData.is_favorite === null,
      access_token: "",
      saveState: false,
    };
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
      console.log('profile.js');
    const { userData } = this.props;
    this.setState({ userInfos: userData, tmpImg: userData.img_avatar });
    LocalData.getUserData().then((response) => {
      response = JSON.parse(response);
      console.log(response);
      this.setState({ access_token: response.token });
      // this.getUsersInfo(response.token);
    });
  }

  // getUsersInfo(access_token) {
  //   let user_img_avatar;
  //   access_token = `bearer ${access_token}`;
  //   HttpRequest.getUserProfile(access_token)
  //     .then(response => {
  //       const result = response.data;
  //       if (result.status === "success") {
  //         if (result.data.img_avatar) {
  //           user_img_avatar = result.data.img_avatar;
  //         } else {
  //           user_img_avatar = `${Config.webUrl}images/avatar_xenren.png`;
  //         }
  //         this.setState({
  //           imageLoading: false,
  //           tmpImg: user_img_avatar
  //         });
  //       } else {
  //         global.setTimeout(() => {
  //           Alert.alert("Warning", "Token expire!", [
  //             {
  //               text: "OK",
  //               onPress: () => {
  //                 this.logout();
  //               }
  //             }
  //           ]);
  //         }, 200);
  //       }
  //     })
  //     .catch(error => {
  //       const message = "Cannot Fetch, Please Try Again.";
  //     });
  // }

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: "images",
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.fileSize > 2000000) {
          Alert.alert("Error", translate('file_size_large'), [
              {
                  text: "OK",
              },
          ]);
          return;
      }

      // if (response.didCancel) {
      // } else if (response.error) {
      // } else {
//    # above previous codes #
      if (!response.didCancel || !response.didCancel) {
          this.setState({
              tmpImg: response.uri,
              ImageShow: true,
              saveState: true,
          });
      }
    });
  }

  saveProfileImage = () => {
    let self = this;
    this.setState({ saveLoading: true });
    let access_token = `${"Bearer" + " "}${this.state.access_token}`;
    let imageObjectArr = {
      uri: this.state.tmpImg,
      name: 'photo_id.png' || 'photo_id.jpg' || 'photo_id.jpeg',
      type: 'image/png' || 'image/jpg' || 'image/jpeg',
    };

    HttpRequest.updateProfilePicture(access_token, imageObjectArr)
      .then((response) => {
        const result = response;
        if (result.status === "success") {
            console.log('success response');
            this.props.userData.img_avatar = result.data.avatar;
            Alert.alert("Success", "Successfully Updated", [
                {
                    text: "OK",
                },
            ]);
          self.setState({
            imageModal: false,
            isShowSuccess: true,
            ImageShow: false,
            saveState: false,
            saveLoading: false,
          });
          setTimeout(() => {
            self.setState({
              isShowSuccess: false,
              saveLoading: false,
            });
          }, 3000);
        } else if (result.status === "error") {
          console.log('error response');
          alert(result.data.errors.avatar);
            self.setState({
            isLoading: false,
            isShowError: true,
            errorMessage: result.message,
            saveLoading: false,
          });
          setTimeout(() => {
            self.setState({
              isShowError: false,
            });
          }, 3000);
        } else {
          console.log('idle response');
          global.setTimeout(() => {
            Alert.alert("Warning", "Token expire!", [
              {
                text: "OK",
                onPress: () => {
                  this.logout();
                },
              },
            ]);
          }, 200);
        }
      })
      .catch((error) => {
        this.setState({ saveLoading: false });
        alert(error);
      });
  };

  logout() {
    this.setState({ showLogoutDialog: false, loading: true });
    HttpRequest.logout(this.state.access_token)
        .then((response) => {
          if (response.data.status === 'success') {
            LocalData.setUserData(null);
            LocalData.setAppData(null);
            LocalData.setfileData(null);
            LocalData.setfile1Data(null);
            LocalData.setVerifyData(null);
            LocalData.setLocale(null);
            this.props.navigation.dispatch(
                NavigationActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "Login",
                    }),
                  ],
                }),
            );
          }
        })
        .catch((error) => {
          alert(error);
        });
  }

  userProfile = () => {
    this.props.navigation.navigate("UserProfile");
  };

  VerifyPhoto = () => {
    this.props.navigation.navigate("MyProfile");
  };

  withdraw = () => {
    // this.props.navigation.navigate("Withdraw");
      this.props.navigation.navigate('Deposit', { securityDeposit: false });
  };

  topUpScreen = () => {
      console.log('got to top up');
    this.props.navigation.navigate("TopUp");
  };

  balance = () => {
      console.log('go to balance.js');
    this.props.navigation.navigate("Balance");
  };

  tracker = () => {
    this.props.navigation.navigate("Tracker");
  };

  notifications = () => {
    this.props.navigation.navigate("NotificationSettings");
  };

  myspace = () => {
    this.props.navigation.navigate("MySpaces");
  };

  onInvitePressed = () => {
    Share.share({
      message: `${translate('app_name')}.\n${Config.webUrl}register?inviteCode=${this.props.userData.id}`,
      title: "Click To Install",
    });
  };

  goBack = () => {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: "Dashboard",
        }),
      ],
    }));
  };

  closeModal = () => {
    this.setState({ imageModal: false });
  };

  closeImgModal() {
    this.setState({
      imageModal: false,
      ImageShow: false,
    });
  }

  onLoad = () => {
    if (this.props.userData.is_favorite === null) {
      this.setState({ imageLoading: false });
      this.props.userData.is_favorite = undefined;
    }
  };

  render() {
    if (this.state.loading === true) {
      return (
          <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: height / 2 }}>
            <ActivityIndicator />
          </View>
      )
    }
    return (
        <View style={styles.rootStyle}>
          {/* Back Button View */}
          <View style={styles.navigationStyle}>
            <View style={styles.navigationWrapper}>
              <TouchableOpacity
                  onPress={this.goBack}
                  style={{
                    marginTop: 8,
                      marginLeft: 10,
                      flexDirection: "row",
                  }}
              >
                  <Image
                      style={{width:20,height:26,position:'relative',top:-3}}
                      source={require('../../../images/arrowLA.png')}
                  />

                  <Text
                    style={{
                      marginLeft: 10,
                      marginTop:-1,
                      fontSize: 19.5,
                      fontWeight:'normal',
                      color: Config.primaryColor,
                      fontFamily: FontStyle.Regular,
                    }}
                >
                  {translate("back")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ marginLeft: 2 }}
                onPress={() => {
                  this.notifications();
                }}
              >
                <View style={styles.logoutButtonStyle}>
                    <Image
                        style={{width:25,height:26,position:'relative',top:0}}
                        source={require('../../../images/bellIcon.png')}
                    />
                  {/*<FeatherIcon name="bell" color={Config.primaryColor} size={25}/>*/}
                </View>
              </TouchableOpacity>
            </View>
          </View>

          {/* Profile Image View */}
          <View style={styles.photoWrapperStyle}>
            {/* Image */}
            <TouchableOpacity
              style={{ width: 100 }}
              onPress={() => {
                this.setState({ imageModal: true });
              }}
            >
              {this.state.imageLoading && (
                  <ActivityIndicator size="small" style={{ marginTop: 30 }} color={Config.primaryColor} />
              )}
              <Image
                  source={{ uri: this.props.userData.img_avatar }}
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: 50,
                    borderWidth: 2,
                    borderColor: Config.primaryColor,
                  }}
                  onLoad={this.onLoad}
              />
            </TouchableOpacity>

            {/* Profile */}
            <View style={styles.columnStyle}>
              <Text style={styles.userNameStyle}>
                {this.state.userInfos.real_name}
              </Text>
              <Text style={styles.userLevelStyle}>
                {this.state.userInfos.title}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.userProfile();
                }}
              >
                <Text style={[styles.rowTextStyle, { color: Config.primaryColor }]}>{translate("my_profile")}</Text>
              </TouchableOpacity>
            </View>
          </View>

          {/* Finance Options View */}
          <View style={styles.selectFinanceWrapper}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={styles.btnFinance}
                onPress={() => {
                  this.withdraw();
                }}
              >
                <View style={styles.photoWrapperStyle}>
                  <Image
                    source={require("../../../images/other/icon_withdraw.png")}
                    style={styles.imgFinanceTextStyle}
                  />
                  <Text style={styles.btnFinanceTextStyle}>{translate("withdraw")}</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnFinance}
                onPress={() => {
                  this.topUpScreen();
                }}
              >
                <View style={styles.photoWrapperStyle}>
                  <Image
                    source={require("../../../images/other/icon_topup.png")}
                    style={styles.imgFinanceTextStyle}
                  />
                  <Text style={styles.btnFinanceTextStyle}>{translate("top_up")}</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.btnFinance}
                onPress={() => {
                  this.balance();
                }}
              >
                <View style={styles.photoWrapperStyle}>
                  <Image
                    source={require("../../../images/other/icon_balance.png")}
                    style={styles.imgFinanceTextStyle}
                  />
                  <Text style={styles.btnFinanceTextStyle}>{translate("balance")}</Text>
                </View>
              </TouchableOpacity>

            </ScrollView>
          </View>

          {/* Other Options View */}
          <ScrollView>
            <View
              style={{
                flex: 1,
                alignItems: "flex-start",
                marginTop: 25,
                marginLeft: "-20%",
              }}
            >
              <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => {
                  this.VerifyPhoto();
                }}
              >
                <Image
                  source={require("../../../images/other/icon_verify.png")}
                  style={styles.iconStyle}
                />
                <Text style={styles.rowTextStyle}>{translate("verify_info")}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => {
                  this.onInvitePressed();
                }}
              >
                <Image
                  source={require("../../../images/people.png")}
                  style={styles.iconStyle}
                />
                <Text style={styles.rowTextStyle}>{translate("invite_people")}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => {
                  this.tracker();
                }}
              >
                <Image
                  source={require("../../../images/other/tracker.png")}
                  style={styles.iconStyle}
                />
                <Text style={styles.rowTextStyle}>{translate("tracker")}</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => {
                  this.notifications();
                }}
              >
                <Image
                  source={require("../../../images/other/notificationSettings.png")}
                  style={styles.iconStyleNew}
                />
                <Text style={styles.rowTextStyle}>{'Notifications'}</Text>
              </TouchableOpacity> */}
              <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => {
                  this.myspace();
                }}
              >
                <Image
                  source={require("../../../images/sharedOffices/icon.png")}
                  style={styles.iconStyleNew}
                />
                <Text style={styles.rowTextStyle}>{'My Spaces'}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rowStyle}
                onPress={() => this.setState({ showLogoutDialog: true })}
              >
                <FeatherIcon name="log-out" style={[styles.iconStyleNew, { marginTop: 15 }]} color={Config.primaryColor} size={28}/>
                <Text style={styles.rowTextStyle}>{'Logout'}</Text>
              </TouchableOpacity>

            </View>
          </ScrollView>

          <Modal
              animationType="fade"
              onRequestClose={() => {
                this.closeModal();
              }}
              transparent={true}
              visible={this.state.imageModal}
          >
            <View style={styles.modalContainerStyleImg}>
              <View style={styles.modalBoxStyleImg}>
                <View style={styles.modalHeaderWrapperImg}>
                  <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                      }}
                  >
                    <View/>
                    <View style={{ width: "100%" }}>
                      <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                      >
                        <Text/>
                        <Text
                            style={{
                              fontSize: 20,
                              color: "#000",
                              textAlign: "center",
                            }}
                        >
                            {translate("edit_your_profile")}{" "}
                        </Text>
                        <TouchableOpacity
                            style={{
                              marginRight: 3,
                              flexDirection: "row",
                            }}
                            onPress={() => this.closeImgModal()}
                        >
                          <Ionicons
                              name="md-close-circle"
                              size={24}
                              color={Config.primaryColor}
                          />
                        </TouchableOpacity>
                      </View>

                      <Text
                          style={{
                            fontSize: 16,
                            color: "gray",
                            textAlign: "center",
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                          }}
                      >
                          {translate("gain_more_traffic")}{" "}
                      </Text>
                    </View>
                    <View
                        style={{
                          flex: 0.1,
                          alignSelf: "center",
                        }}
                    >
                      <TouchableOpacity
                          style={{
                            marginRight: 3,
                            flexDirection: "row",
                          }}
                          onPress={() => this.closeImgModal()}
                      >
                        <Ionicons
                            name="md-close-circle"
                            size={24}
                            color={Config.primaryColor}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                {this.state.saveState === false && (
                    <View
                        style={{
                          flexDirection: "column",
                          height: 225,
                          width: 220,
                          paddingVertical: 8,
                          alignSelf: "center",
                          marginBottom: 12,
                          marginLeft: 8,
                          borderRadius: 110,
                        }}
                    >
                      <Image
                          source={{ uri: this.state.tmpImg }}
                          style={{
                            height: 212,
                            width: 215,
                            borderRadius: 105,
                          }}
                          resizeMode="cover"
                      />
                    </View>
                )}
                {this.state.saveState === true && (
                    <TouchableOpacity
                        onPress={() => this.launchCamera()}
                        style={{
                          flexDirection: "column",
                          height: 225,
                          width: 220,
                          paddingVertical: 8,
                          alignSelf: "center",
                          marginBottom: 12,
                          marginLeft: 8,
                          borderRadius: 110,
                        }}
                    >
                      <Image
                          source={{ uri: this.state.tmpImg }}
                          style={{
                            height: 212,
                            width: 215,
                            borderRadius: 105,
                          }}
                          resizeMode="cover"
                      />
                    </TouchableOpacity>
                )}

                {this.state.ImageShow === false && (
                    <View
                        style={{
                          justifyContent: "center",
                          paddingHorizontal: 15,
                          marginBottom: 10,
                        }}
                    >
                      <TouchableOpacity
                          style={{
                            flexDirection: "row",
                            backgroundColor: Config.primaryColor,
                            justifyContent: "center",
                            paddingVertical: 12,
                            borderRadius: 5,
                          }}
                          onPress={() => this.launchCamera()}
                      >
                        <Text
                            style={{
                              color: "#fff",
                              textAlign: "center",
                              fontWeight: "bold",
                              fontSize: 17,
                              fontFamily: FontStyle.Medium,
                            }}
                        >
                            {translate("CHANGE")}
                        </Text>
                      </TouchableOpacity>
                    </View>
                )}
                {this.state.ImageShow === true && (
                    <View
                        style={{
                          justifyContent: 'center',
                          paddingHorizontal: 15,
                          marginBottom: 10,
                        }}
                    >
                      <TouchableOpacity
                          style={{
                            flexDirection: "row",
                            backgroundColor: Config.primaryColor,
                            justifyContent: "center",
                            paddingVertical: 12,
                            borderRadius: 5,
                            marginBottom: 10,
                          }}
                          onPress={() => this.saveProfileImage()}
                      >
                        {this.state.saveLoading ? (
                            <ActivityIndicator color="#fff" size="small"/>
                        ) : (
                            <Text
                                style={{
                                  color: "#fff",
                                  textAlign: "center",
                                  fontWeight: "bold",
                                  fontSize: 17,
                                  fontFamily: FontStyle.Medium,
                                }}
                            >
                                {translate("save")}
                            </Text>
                        )}
                      </TouchableOpacity>
                    </View>
                )}
              </View>
            </View>
          </Modal>

          <Modal
              onRequestClose={() => this.setState({ showLogoutDialog: false })}
              animationType="fade"
              transparent={true}
              visible={this.state.showLogoutDialog}
          >
            <View style={styles.modalContainerStyle}>
              <View style={styles.modalBoxStyle}>
                <View style={styles.powerButton}>
                  <View style={styles.powerIcon}>
                    <FontAwesome name="power-off"
                                  color={Config.primaryColor}
                                  style={{ left: 8, top: 5 }}
                                  size={45}
                    />
                  </View>
                </View>
                <View
                    style={{
                      // height: width / 3.16,
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                  <Text
                      style={{ fontFamily: FontStyle.Bold, fontSize: 16 }}
                  >{translate("are_you_sure_logout")}</Text>
                  <Text
                      style={{ fontFamily: FontStyle.Bold, fontSize: 16 }}
                  >{translate("logout")}?</Text>
                </View>
                <View style={styles.confirmButtonWrapper}>
                  <TouchableOpacity
                      onPress={() => {
                        this.logout();
                      }}
                      style={styles.yes}
                  >
                    <Text style={{ color: Config.white, fontFamily: FontStyle.Regular }}>{translate("yes")}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={() => {
                        this.setState({ showLogoutDialog: false });
                      }}
                      style={styles.no}
                  >
                    <Text style={{ color: Config.white, fontFamily: FontStyle.Regular }}>{translate("no")}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          <FooterTabs navigation={this.props.navigation} currentIndex={4}/>
        </View>
    ); // return
  } // render
} // Profile

const styles = {
  rootStyle: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    // paddingHorizontal: 10
  },

  navigationWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },

  backArrowStyle: {
    marginRight: 10,
  },

  backArrowIconStyle: {
    fontFamily: "Montserrat-Light",
    marginLeft: 10,
  },

  photoWrapperStyle: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: 15,
    paddingHorizontal: 15,
  },

  navigationStyle: {
    height: 55,
    flexDirection: "row",
    marginTop: 10,
    paddingHorizontal: 5,
    // alignItems:'center'
  },

  userNameStyle: {
    fontSize: 25,
    fontFamily: FontStyle.Regular,
  },

  userLevelStyle: {
    fontSize: 22,
    fontFamily: FontStyle.Light,
  },

  rowStyle: {
    flexDirection: "row",
    alignItems: "center",
    height: 40,
    paddingHorizontal: 30,
    paddingVertical: 5,
  },

  columnStyle: {
    width: '70%',
    marginLeft: 15,
    flexDirection: "column",
  },

  logoutButtonStyle: {
    width: 40,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },

  rowTextStyle: {
    fontFamily: FontStyle.Light,
    fontSize: 18,
    width: 190,
    color: Config.topNavigation.headerTextColor,
  },

  iconStyle: {
    width: 35,
    height: 40,
    resizeMode: "contain",
    tintColor: Config.primaryColor,
    marginRight: 33,
    marginLeft: 75,
  },


  iconStyleNew: {
    width: 35,
    height: 40,
    resizeMode: "contain",
    marginRight: 33,
    marginLeft: 75,
  },

  infoDepositStyle: {
    height: 72,
    backgroundColor: "#f1fff2",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    overflow: "hidden",
  },

  depositButtonStyle: {
    backgroundColor: Config.primaryColor,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
  },

  iconTransparentStyle: {
    position: "absolute",
    left: 10,
    top: 10,
    width: 100,
    height: 100,
  },

  modalBoxStyleImg: {
    width: "90%",
    backgroundColor: "#ffffff",
    borderRadius: 5,
    justifyContent: "space-between",
  },

  modalHeaderWrapperImg: {
    paddingHorizontal: 20,
    paddingTop: 20,
    flexDirection: "row",
  },

  modalContainerStyleImg: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  modalContainerStyle: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  modalBoxStyle: {
    width: "80%",
    height: width / 1.59,
    backgroundColor: Config.white,
    borderRadius: 10,
  },

  modalContentStyle: {
    flexDirection: "column",
    flex: 1,
    justifyContent: "space-around",
    paddingTop: 30,
    width: "80%",
    alignItems: "center",
  },
  powerButton: {
    height: width / 5,
    borderTopEndRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
  },

  powerIcon: {
    backgroundColor: Config.white,
    borderRadius: 50,
    height: 55,
    width: 55,
    alignSelf: 'center',
  },

  confirmTextStyle: {
    fontSize: 20,
    marginVertical: 20,
    textAlign: "center",
    color: "#3e3e3e",
  },

  confirmButtonWrapper: {
    alignSelf: 'flex-end',
    flexDirection: "row",
  },
  yes: {
    backgroundColor: Config.primaryColor,
    flex: 0.5,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderBottomLeftRadius: 5,
  },
  no: {
    backgroundColor: Config.textSecondaryColor,
    flex: 0.5,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderBottomEndRadius: 5,
  },

  selectFinanceWrapper: {
    flexDirection: 'column',
    paddingHorizontal: 5,
    paddingVertical: 15,
  },
  btnFinance: {
    width: 'auto',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: Config.primaryColor,
    marginRight: 10,
  },
  btnFinanceTextStyle: {
    color: Config.primaryColor,
    fontSize: 15,
    fontWeight: 'bold',
  },
  imgFinanceTextStyle: {
    width: 25,
    height: 25,
    resizeMode: "contain",
    tintColor: Config.primaryColor,
    marginRight: 6,
  },

};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
        type: "set_root",
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
