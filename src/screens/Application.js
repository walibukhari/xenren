/* eslint-disable no-else-return */
/* eslint-disable max-len */
/* eslint-disable prefer-const */
/* eslint-disable no-tabs */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */
import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import Config from "../Config";

import ShareOffice from "./Dashboard";
import SelectSeat from "../shared_office/PickSeatScreen";
import Notification from "./subs/Notification";
import Profile from "./subs/Profile";

class Application extends Component {
  static navigationOptions = {
    // title: '共享办公',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      buttons: [
        {
          active: true,
          middle: false,
          icon: require("../../images/select_seat/bottom_icon_home.png"),
          label: "共享办公",
          topLabel: "共享办公"
        },
        {
          active: false,
          middle: false,
          icon: require("../../images/select_seat/bottom_icon_chat.png"),
          label: "室内聊天",
          topLabel: "室内聊天"
        },
        {
          active: false,
          middle: true,
          icon: require("../../images/share_office/icon_xenren.png"),
          label: "资策室",
          topLabel: "资策室"
        },
        {
          active: false,
          middle: false,
          icon: require("../../images/select_seat/bottom_icon_message.png"),
          label: "消息",
          topLabel: "消息"
        },
        {
          active: false,
          middle: false,
          icon: require("../../images/select_seat/bottom_icon_profile.png"),
          label: "我",
          topLabel: "我的"
        }
      ],
      activeIndex: 0,
      visibility: 0
    };
    this.root = this.props.component.root;
  }

  componentDidMount() {
    this.setState({ activateScreen: "slowP" });
  }

  verifyUser() {
    this.props.navigation.navigate("VerifyId");
  }

  gotoLogin() {
    this.props.navigation.navigate("Login");
  }

  activateMenu(index) {
    let buttons = this.state.buttons;
    buttons = buttons.map(item => {
      item.active = false;
      return item;
    });
    buttons[index].active = true;
    if (index === 1) {
      this.root.navigate("IndoorChat");
    } else if (index === 0) {
      this.root.navigate("SelectChat");
    } else {
      this.setState({ buttons, activeIndex: index });
    }
  }

  render() {
    if (this.state.activeIndex !== 4) {
      return (
        <View style={styles.containerStyle}>
          {this.state.activeIndex !== 0 && (
            <View style={styles.navigationStyle}>
              <View style={styles.navigationTextStyle}>
                <Text style={{ fontSize: 18 }}>
                  {this.state.buttons[this.state.activeIndex].topLabel}
                </Text>
              </View>
            </View>
          )}

          <View style={styles.bodyStyle}>
            {this.state.activeIndex === 0 && <ShareOffice />}
            {this.state.activeIndex === 2 && <SelectSeat />}
            {this.state.activeIndex === 3 && <Notification />}
            {this.state.activeIndex === 4 && <Profile />}
          </View>
          <View style={styles.footerMenuStyle}>
            <View
              style={{
                backgroundColor: "#fff",
                height: 60,
                position: "absolute",
                bottom: 0,
                left: 0,
                width: "100%",
                borderTopColor: Config.lineColor,
                borderTopWidth: 1,
                zIndex: 0
              }}
            />

            {this.state.buttons.map((item, index) => {
              if (item.middle) {
                let color = item.active
                  ? Config.primaryColor
                  : Config.textColor;
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.footerMiddleButtonStyle}
                    onPress={() => {
                      this.activateMenu(index);
                    }}
                  >
                    <View style={styles.logoCircleStyle}>
                      <Image
                        source={item.icon}
                        style={{
                          height: 30,
                          justifyContent: "center",
                          alignItems: "center",
                          alignSelf: "center",
                          position: "absolute"
                        }}
                        resizeMode="contain"
                      />
                    </View>
                    <Text style={[styles.footerTextStyles, { color }]}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                );
              } else {
                let color = item.active
                  ? Config.primaryColor
                  : Config.textColor;
                return (
                  <TouchableOpacity
                    key={index}
                    style={styles.footerButtonStyle}
                    onPress={() => {
                      this.activateMenu(index);
                    }}
                  >
                    <Image
                      source={item.icon}
                      style={{ height: 20, tintColor: color }}
                      resizeMode="contain"
                    />
                    <Text style={[styles.footerTextStyle, { color }]}>
                      {item.label}
                    </Text>
                  </TouchableOpacity>
                );
              }
            })}
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.bodyStyle}>
          {this.state.activeIndex === 4 && (
            <Profile setParentState={newState => this.setState(newState)} />
          )}
        </View>
      );
    } // else
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column"
  },
  navigationStyle: {
    height: 50,
    flexDirection: "row",
    marginTop: 20,
    borderBottomColor: Config.lineColor,
    borderBottomWidth: 1
  },
  navigationButtonStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  navigationTextStyle: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  bodyStyle: {
    flex: 1
  },
  footerMenuStyle: {
    backgroundColor: "transparent",
    flexDirection: "row",
    height: 60,
    alignItems: "flex-end"
  },
  footerButtonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
    flexDirection: "column"
  },
  footerMiddleButtonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 75,
    flexDirection: "column"
  },
  logoCircleStyle: {
    justifyContent: "center",
    alignItems: "center",
    width: 38,
    height: 38,
    borderRadius: 19,
    backgroundColor: "#f3f3f3",
    zIndex: 1,
    top: 10
  },
  footerTextStyle: {
    fontSize: 11,
    marginTop: 5
  },
  footerTextStyles: {
    fontSize: 11,
    marginTop: 13
  }
};

// function mapStateToProps(state) {
//     return {
//         component: state.component,
//     }
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         setRoot: (root) => dispatch({
//             type: 'set_root',
//             root: root
//         })
//     }
// }

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(Application);
export default Application;
