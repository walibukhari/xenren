import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import FontAwesomeIcons, {FA5Style} from 'react-native-vector-icons/FontAwesome5';
import Config from '../Config';
import { translate } from '../i18n';
import Tab1 from './profile/Tab1';
import Tab2 from './profile/Tab2';
import FontStyle from "../constants/FontStyle";

class MyProfile extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
    };
  }

  componentDidMount() {
    if (this.props.navigation.state.params !== undefined) {
      this.setState({ activeTabIndex: 1 });
    }
  }

  render() {
    return (
        <View style={styles.rootStyle}>
          {/* Back Button View */}
          <View style={styles.navigationStyle}>
            <View style={styles.navigationWrapper}>
              <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                  style={{
                    marginTop: 8,
                    marginLeft: 4,
                    flexDirection: 'row',
                  }}
              >
                {/*<FontAwesomeIcons*/}
                {/*    size={26}*/}
                {/*    name="chevron-left"*/}
                {/*    color={Config.primaryColor}*/}
                {/*    fa5Style={FA5Style.regular}*/}
                {/*/>*/}
                <Image
                    style={{width:20,height:26,position:'relative',top:-3}}
                    source={require('../../images/arrowLA.png')}
                />
              </TouchableOpacity>

              <Text
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                style={{
                  marginLeft: 10,
                  marginTop:2,
                  fontSize: 19.5,
                  fontWeight:'normal',
                  color: Config.primaryColor,
                  fontFamily: FontStyle.Regular,
                }}
              >
                {translate('myProfile')}
              </Text>

            </View>
          </View>

          <View style={styles.topButtonWrapperStyle}>
            <TouchableOpacity
              style={[
                styles.topButtonStyle,
                this.state.activeTabIndex === 0
                  ? styles.topButtonActiveStyle : {},
              ]}
              onPress={() => {
                // this.goToVerifyInfo();
                this.setState({ activeTabIndex: 0 });
              }} >
              <Text
                style={this.state.activeTabIndex === 0 ? styles.topTextActiveStyle : {}}>{translate('information')}</Text>
            </TouchableOpacity>

            <View style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}/>

            <TouchableOpacity
              style={[
                styles.topButtonStyle,
                this.state.activeTabIndex === 1
                  ? styles.topButtonStyle : {},
              ]}
              onPress={() => {
                this.setState({ activeTabIndex: 1 });
              }}>
              <Text
                style={this.state.activeTabIndex === 1 ? styles.topTextActiveStyle : {}}>{translate('photo')}</Text>
            </TouchableOpacity>
          </View>
          {this.state.activeTabIndex === 0 && <Tab1 navigation={ this.props.navigation }/>}
          {this.state.activeTabIndex === 1 && <Tab2 navigation={ this.props.navigation }/>}
        </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  navigationStyle: {
    height: 55,
    flexDirection: 'row',
    paddingHorizontal: 10,
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 1,
    // alignItems:'center'
  },
  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  topButtonWrapperStyle: {
    flexDirection: 'row',
    elevation: 1,
  },
  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
  },
  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },
  topTextActiveStyle: {
    color: Config.primaryColor,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyProfile);
