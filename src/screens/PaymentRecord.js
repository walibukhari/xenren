/* eslint-disable indent */
/* eslint-disable global-require */
/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, ListView, Image, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../Config';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

class PaymentRecord extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('payment_record'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
      headerRight: (<TouchableOpacity>

        <Image source={require('../../images/other/icon_balance.png')} style={{
          width: 30,
          height: 30,
          resizeMode: 'contain',
          marginRight: 10,
          tintColor: Config.primaryColor,
        }}/>

      </TouchableOpacity>),
    };
  };

  constructor(props) {
    super(props);

    const data = [
      {
        day: '22',
        month: 'Sep',
        name: 'Silicon Valley',
        timeSpent: '10 Hours Spent',
        amount: '3105',
      },

      {

        day: '21',
        month: 'Sep',
        name: 'Sisaro',
        timeSpent: '13 Hours Spent',
        amount: '2335',
      },
      {

        day: '30',
        month: 'Aug',
        name: 'Piccaso Bio',
        timeSpent: '14 Hours Spent',
        amount: '4335',
      },

    ];

    this.state = {

      dataSource: this.convertArrayToDataSource(data),

    };

    // eslint-disable-next-line react/prop-types
    this.root = this.props.component.root;

    // eslint-disable-next-line no-undef
    ini = this;
  }

  convertArrayToDataSource(arr) {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    return ds.cloneWithRows(arr);
  }


  renderRow(rowData) {
    return (
      <View style={{
        paddingHorizontal: 20,
        paddingVertical: 20,
        backgroundColor: '#fff',
        elevation: 5,
        marginBottom: 15,
      }}>
        <View style={styles.boxInsideStyle}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{
              color: Config.primaryColor,
              fontSize: 18,
              fontWeight: 'bold',
            }}>
              {rowData.day}.
            </Text>
            <Text style={styles.greyedTextDescriptionStyle}>{rowData.month}</Text>
          </View>
          <View style={{ flexDirection: 'column' }}>
            <Text style={{
              fontSize: 16,
              fontWeight: '500'
            }}>{rowData.name}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Ionicons name="ios-timer" size={19} color="#c3c3c3"/>
              <Text style={{
                ...styles.greyedTextDescriptionStyle,
                marginLeft: 10
              }}>{rowData.timeSpent}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{
              color: Config.primaryColor,
              textAlignVertical: 'center',
              fontSize: 18,
              marginRight: 10
            }}>{rowData.amount}</Text>
            <Image source={require('../../images/other/icon_yen.png')} style={{
              width: 25,
              height: 25,
              tintColor: Config.primaryColor,
              marginTop: 7,
            }}/>
          </View>
        </View>
      </View>
    );
  }


  render() {
    return (
      <View style={styles.rootStyle}>
        <ListView
          style={{ flex: 1 }}
          dataSource={this.state.dataSource}
          renderRow={rowData => this.renderRow(rowData)}
        />
      </View>

    );// return
  }// render
}// PaymentRecord


const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
  },

  boxInsideStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  greyedTextDescriptionStyle: {
    color: '#c3c3c3',
    fontSize: 15,
    fontWeight: '100',
  },
};


function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PaymentRecord);
