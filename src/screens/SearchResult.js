import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
	Image,
	Text,
	View,
	TouchableOpacity,
	FlatList,
	Picker,
	Dimensions,
	Modal,
	Alert,
	Platform
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../Config';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';
import { Rating } from 'react-native-elements';
import FontStyle from '../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Feather from 'react-native-vector-icons/Feather';
import { Dialog } from "../components/Dialog/index";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

const { width, height } = Dimensions.get('window');

class SearchResult extends Component {
	static navigationOptions = {
		header: null,
	};

	constructor(props) {
		super(props);
		this.state = {
			officeImage: '',
			isLoading: true,
			officeInfoModel: false,
			isAdvanceFilter: false,
			isShowFilter: false,
			freelancersList: [],
			filteredList: [],
			search: '',
			allData: [],
			skillList: '',
			country_id: '',
			language_id: '',
			reviewType_id: '',
			jobPosition_id: '',
			jobTitle_id: '',
			hourlyRate_id: '',
			country: '',
			language: '',
			reviewType: '',
			jobPosition: '',
			jobTitle: '',
			hourlyRate: '',
			locale: this.props.navigation.state.params.locale,
			colors: ['#f1c40f', Config.primaryColor, '#ecf0f1'],
			skillIcons: [
				require('../../images/share_office/icon_crown.png'),
				require('../../images/share_office/icon_check.png'),
			],
			datePickerModelIos: false,
			jobPositionModel: false,
			hourlyRateModel: false,
			jobTitleModel: false,
			reviewTypeModel: false,
			languageModel: false,
			countryModel: false,
		};
	}
	goBackPage() {
		this.props.navigation.goBack();
	}
	sendChat = (item) => {
		this.props.navigation.navigate('ChatSingle', { userInfo: item });
	};

	componentDidMount() {
		console.log('SearchResult.js'+this.props.navigation.state.params.title);
		// Set route params
		const arr = [];
		this.props.navigation.state.params.selectedSkills.map((item) => {
			arr.push(item.value);
		});
		this.setState(
			{
				skillList: arr.toString(),
				filter: `skill_id_list=${arr.toString()}`,
			},
			() => {
				this.findFreelancerAPI(`skill_id_list=${this.state.skillList}`);
			},
		);
		this.props.navigation.setParams({
			headerRight: (
				<TouchableOpacity
					style={{
						width: 30,
						height: 30,
						marginRight: 15,
						alignSelf: 'center',
					}}
					onPress={() => this.onFilterTapped()}
				>
					<Feather name="filter" size={28} color={Config.primaryColor}/>
				</TouchableOpacity>
			),
		});
	}

	findFreelancerAPI = (filter) => {
		HttpRequest.findFreelancers(filter, this.props.userData.token)
			.then((response) => {
				if (response.data.status === 'success') {
					const arr = [];
					if (response.data.data.users.length === 0) {
						Alert.alert("Error", "No Freelancer Found", [
							{
								text: "OK",
							}
						]);
					}
					response.data.data.users.map((item) => {
						if (item.is_favorite !== null) {
							item.isFavourite = true;
							arr.push(item);
						} else {
							item.isFavourite = false;
							arr.push(item);
						}
					});
					this.setState({
						freelancersList: arr,
						allData: response.data.data,
						isLoading: false,
						listLength: response.data.data.users.length,
						filter: '',
					});
				} else {
					this.setState({
						isLoading: false,
					});
				}
			})
			.catch((error) => {
				this.setState({
					isLoading: false,
				});
			});
	};

	addFavAPI(userId) {
		HttpRequest.addFreelancerToFAV(this.props.userData.token, userId)
			.then((response) => {
				console.log(response.data);
			})
			.catch((error) => {
				alert(error);
			});
	}

	removeFavAPI(userId) {
		HttpRequest.removeFreelancerToFAV(this.props.userData.token, userId)
			.then((response) => {
				console.log(
					'Response of Remove Freelancer to Fav list ----- ',
					response.data,
				);
			})
			.catch((error) => {
				alert(error);
			});
	}

	openOfficeInfoModel(officeData) {
		this.setState({
			officeInfoModel: true,
			officeImage: `https://www.xenren.co/${officeData.last_scanned_office.office.image}`,
			lastScannerOfficeRating: officeData.last_scanned_office.office.rating,
			lastScannerOfficeName: officeData.last_scanned_office.office.office_name,
			lastScannerOfficeLocation: officeData.last_scanned_office.office.location,
		});
	}

	closeOfficeInfoModel() {
		this.setState({
			officeInfoModel: false,
		});
	}

	onFilterTapped = () => {
		if (this.state.freelancersList.length > 0) {
			this.setState({ isShowFilter: true });
		}
	};

	handleSearchInput = (e) => {
		const text = e.toLowerCase();
		const fullList = this.state.freelancersList;
		const filteredList = fullList.filter((item) => {
			if (
				item.real_name && item.real_name.toLowerCase().match(text) ||
				item.country && item.country.full_name && item.country.full_name.toLowerCase().match(text) ||
				item.about_me && item.about_me.toLowerCase().match(text)
			) {
				return item;
			}
		});

		if (!text || text === '') {
			this.setState({
				filteredList: fullList,
				listLength: fullList.length,
				search: '',
			});
		} else if (!filteredList.length) {
			this.setState({
				filteredList,
				listLength: filteredList.length,
				search: text,
			});
		} else if (Array.isArray(filteredList)) {
			this.setState({
				filteredList,
				listLength: filteredList.length,
				search: text,
			});
		}
	};

	checkPortfolio = (user) => {
		this.props.navigation.navigate('OtherUserProfile', {
			user_id: user.id,
			type: 'FindFreelancer',
			jobPositions: this.state.allData.jobPositions,
		});
	};

	renderSkill(skills) {
		const adminVerified = [];
		const clientVerified = [];
		const unVerified = [];
		skills.map((item) => {
			if (item.experience > 0) {
				adminVerified.push(item);
			}
		});
		skills.map((item) => {
			if (item.is_client_verified !== null && item.is_client_verified !== 0) {
				clientVerified.push(item);
			} else {
				if (item.experience === 0) {
					unVerified.push(item);
				}
			}
		});
		const newSkills = [...adminVerified, ...clientVerified, ...unVerified];
		return newSkills.map((item, index) => (
			<View
				key={index}
				style={styles.skillsStyle}
				backgroundColor={
					item.experience > 0
						? this.state.colors[0]
						: item.is_client_verified > 0 && item.is_client_verified !== null
						? this.state.colors[1]
						: this.state.colors[2]
				}
			>
				{item.experience > 0 ?
					<View
						style={{
							width: 24,
							height: 24,
							borderRadius: 12,
							borderColor: '#fff',
							borderWidth: 1,
							justifyContent: 'center',
							alignItems: 'center',
						}}
					>
						<Image
							style={{
								height: 13,
								width: 13,
								resizeMode: 'contain',
								tintColor: this.state.colors[2],
							}}
							source={this.state.skillIcons[0]}
						/>
					</View>
					: item.is_client_verified !== 0 && item.is_client_verified !== null ?
						<View
							style={{
								width: 24,
								height: 24,
								borderRadius: 12,
								borderColor: '#fff',
								borderWidth: 1,
								justifyContent: 'center',
								alignItems: 'center',
							}}
						>
							<Image
								style={{
									height: 13,
									width: 13,
									resizeMode: 'contain',
									tintColor: this.state.colors[2],
								}}
								source={this.state.skillIcons[1]}
							/>
						</View>
						: <View/>}
				<Text
					style={{
						paddingHorizontal: 10,
						color:
							item.experience > 0
								? Config.white
								: item.is_client_verified > 0 && item.is_client_verified !== null
								? Config.white
								: '#000000',
						backgroundColor: 'transparent',
						fontFamily: FontStyle.Light,
					}}
				>
					{item.skill !== null ? item.skill.name_en : ''}
				</Text>
			</View>
		));
	}

	onAddFav = (item, index) => {
		const tempList =
			this.state.filteredList.length > 0
				? this.state.filteredList
				: this.state.freelancersList;
		tempList[index].isFavourite = true;
		this.state.filteredList.length > 0
			? this.setState({ filteredList: tempList })
			: this.setState({ freelancersList: tempList });
		this.addFavAPI(item.id);
	};

	onRemoveFav = (item, index) => {
		const tempList =
			this.state.filteredList.length > 0
				? this.state.filteredList
				: this.state.freelancersList;
		tempList[index].isFavourite = false;
		this.state.filteredList.length > 0
			? this.setState({ freelancersList: tempList })
			: this.setState({ filteredList: tempList });
		this.removeFavAPI(item.id);
	};

	renderFavourite = (item, index) => {
		if (item.isFavourite === true) {
			return (
				<TouchableOpacity
					onPress={() => this.onRemoveFav(item, index)}
					style={styles.iconStyle1}
				>
					<Ionicons
						name="ios-heart"
						size={25}
						color="tomato"
						style={{ margin: 5 }}
					/>
				</TouchableOpacity>
			);
		}
		return (
			<TouchableOpacity
				onPress={() => this.onAddFav(item, index)}
				style={styles.iconStyle1}
			>
				<Ionicons
					name="ios-heart"
					size={20}
					color="lightgrey"
					style={{ margin: 5 }}
				/>
			</TouchableOpacity>
		);
	};

	onRefresh() {
		this.setState({ isRefreshing: true });
		this.findFreelancerAPI(`skill_id_list=${this.state.skillList}&${this.state.filter}`);
	}

	renderRow = (item, index) => (
		<View
			style={{
				marginHorizontal: 10,
				marginVertical: 5,
			}}
		>
			<View style={styles.boxInsideStyle}>
				<View style={styles.topSideStyle}>
					<Image
						source={{ uri: item.user_image }}
						style={styles.avatarStyle}
					/>
					<View
						style={{
							flex: 1,
							flexDirection: 'column',
							marginLeft: 10,
						}}
					>
						<View
							style={{
								flexDirection: 'row',
								alignItems: 'center',
							}}
						>
							<Text
								ellipsizeMode="tail"
								numberOfLines={1}
								style={styles.text1Style}
							>
								{item.real_name}
							</Text>
							{this.renderFavourite(item, index)}
						</View>

						<View
							style={{
								margin: 5,
								alignItems: 'flex-start',
							}}
						>
							<Rating
								type="star"
								startingValue={Number(item.comment_rating)}
								readonly
								imageSize={15}
								style={{ alignContent: 'flex-start' }}
							/>
							{
								item.last_scanned_office !== null && (
									<TouchableOpacity
										onPress={() => this.openOfficeInfoModel(item) }
										style={{
											// height: 20,
											// width: 20,
											marginLeft: 75,
											marginTop: -5,
											position: 'absolute',
										}}
									>
										<Image
											source={require('./../../images/office-4.png')}
										/>
									</TouchableOpacity>
								)
							}


						</View>
						<View
							style={{
								justifyContent: 'center',
								alignItems: 'flex-start',
							}}
						>
							<Text style={styles.text2Style}>
								{item.title !== null ? item.title : ''}{' '}
							</Text>
							<View
								style={{
									flexDirection: 'row',
									justifyContent: 'flex-start',
									alignItems: 'center',
								}}
							>
								<Text style={styles.text2Style}>{translate('last_login_time')}: </Text>
								{item.last_login_at !== null && (
									<Text
										style={{
											fontSize: 14,
											color: 'gray',
											fontFamily: FontStyle.Regular,
										}}
									>
										{item.last_login_at}
									</Text>
								)}
							</View>
						</View>

						<Text style={styles.text3Style}>
							{item.status !== null ? item.status : ''}
						</Text>
					</View>
				</View>

				{item.skills !== null && (
					<View style={styles.middleSideStyle}>
						{this.renderSkill(item.skills)}
					</View>
				)}

				<View
					style={{
						flexDirection: 'row',
						width: '100%',
						marginTop: 10,
					}}
				>
					<View
						style={{
							width: '50%',
							height: 50,
							borderColor: '#f7f7f7',
							borderWidth: 1,
							justifyContent: 'center',
							alignItems: 'center',
						}}
					>
						<Text style={{ padding: 5 }}>
							{translate('country')}:{' '}
							<Text
								style={{
									fontSize: 14,
									color: Config.primaryColor,
									fontFamily: FontStyle.Regular,
								}}
							>
								{item.country !== null ? item.country.name : ''}
							</Text>
						</Text>
					</View>
					<View
						style={{
							width: '50%',
							height: 50,
							borderColor: '#f7f7f7',
							borderWidth: 1,
							justifyContent: 'center',
							alignItems: 'center',
						}}
					>
						<Text style={{ padding: 5 }}>
							{translate('hourly_rate')}: $
							<Text
								style={{
									fontSize: 14,
									color: Config.primaryColor,
									fontFamily: FontStyle.Regular,
								}}
							>
								{item.hourly_pay}
							</Text>
						</Text>
					</View>
				</View>

				<View
					style={{
						flexDirection: 'row',
						paddingHorizontal: 10,
						paddingVertical: 10,
					}}
				>
					<TouchableOpacity
						style={styles.cardButtonStyle}
						onPress={() => {
							this.checkPortfolio(item);
						}}
					>
						<Ionicons
							name="ios-folder-open"
							color={Config.textSecondaryColor}
							size={15}
						/>
						<Text style={styles.cardButtonTextStyle}>
							{' '}
							{translate('portfolio').toUpperCase()}{' '}
						</Text>
					</TouchableOpacity>
					<View
						style={{
							width: 1,
							backgroundColor: Config.textSecondaryColor,
						}}
					/>
					<TouchableOpacity
						style={styles.cardButtonStyle}
						onPress={() => this.sendChat(item)}
					>
						<Ionicons
							name="ios-mail"
							color={Config.textSecondaryColor}
							size={15}
						/>
						<Text style={styles.cardButtonTextStyle}>
							{' '}
							{translate('check')}{' '}
						</Text>
					</TouchableOpacity>
				</View>
			</View>
		</View>
	);

	render() {
		return (
			<View style={styles.rootStyle}>
				<View style={style.rootStyle}>
					{/* Back Button View */}
					<View style={style.navigationStyle}>
						<View style={style.navigationWrapper}>
							<TouchableOpacity
								onPress={() => this.goBackPage()}
								style={{
									marginTop: 8,
									marginLeft: 10,
									flexDirection: "row",
								}}
							>
								<Image
									style={{width:20,height:26,position:'relative',top:-3}}
									source={require('../../images/arrowLA.png')}
								/>

								<Text
									onPress={() => this.goBackPage()}
									style={{
										marginLeft: 10,
										marginTop:-1,
										fontSize: 19.5,
										fontWeight:'normal',
										color: Config.primaryColor,
										fontFamily: FontStyle.Regular,
									}}
								>
									{this.props.navigation.state.params.title}
								</Text>
							</TouchableOpacity>
							<TouchableOpacity
									style={{ marginLeft: 0,marginRight:10,marginTop:8 }}
									onPress={() => this.onFilterTapped()}
							>
									<View style={styles.logoutButtonStyle}>
										<Feather name="filter" size={28} color={Config.primaryColor}/>
									</View>
								</TouchableOpacity>
						</View>
					</View>
				</View>
				<View>
				<SearchBar
					darkTheme
					barStyle="default"
					autoCorrect={false}
					searchBarStyle="minimal"
					placeholder={translate('search')}
					searchIcon={{ size: 24 }}
					icon={{
						type: 'font-awesome',
						name: 'search',
						marginTop: 15,
						marginBottom: 15,
					}}
					cancelIcon={false}
					containerStyle={{
						backgroundColor: '#f7f7f7',
						width: '100%',
						flexDirection: 'row-reverse',
						alignContent: 'space-between',
						justifyContent: 'space-between',
						borderTopWidth: 0,
						borderBottomWidth: 0,
					}}
					inputContainerStyle={{
						backgroundColor: '#ffffff',
						flexDirection: 'row-reverse',
						alignContent: 'flex-start',
						justifyContent: 'flex-start',
						margin: 0,
					}}
					inputStyle={{
						backgroundColor: '#ffffff',
						width: '100%',
					}}
					onChangeText={text => this.handleSearchInput(text)}
					value={this.state.search}
					onCancel={this.clearSearchText}
				/>
				<Text
					style={{
						paddingTop: 1,
						borderTopColor: '#f7f7f7',
						paddingBottom: 10,
						paddingLeft: 5,
					}}
				>
					{' '}
					{this.state.listLength}{' '}{translate('freelancer_found')}
				</Text>

				<FlatList
					data={
						this.state.search.length == 0
							? this.state.freelancersList
							: this.state.filteredList
					}
					renderItem={({ item, index }) => this.renderRow(item, index)}
					onRefresh={() => this.onRefresh()}
					refreshing={this.state.isLoading}
					keyExtractor={(item, index) => index.toString()}
				/>

				<Modal
					animationType="fade"
					transparent={true}
					visible={this.state.isShowFilter}
					onRequestClose={() => console.log('Modal Closed')}
				>
					<View style={styles.modalContainer}>
						{this.state.freelancersList.length > 0 && this.renderSearchModal()}
					</View>
				</Modal>

				<Modal
					onRequestClose={() => {
						this.setState({
							showModal: false,
						});
					}}
					animationType="fade"
					transparent={true}
					visible={this.state.officeInfoModel}
				>
					<View style={styles.modalRootStyle}>
						<View style={styles.infoModalBoxStyle}>
							<TouchableOpacity
								onPress={() => this.closeOfficeInfoModel()}
								style={{
									bottom: 9,
								}}
							>
								<Image source={require('./../../images/select_seat/cross.png')}
								       style={{
									       height: 30,
									       width: 30,
									       marginLeft: 215,
								       }} resizeMode='cover'/>
							</TouchableOpacity>

							<View style={styles.topSideStyle}>
								<Image
									source={{ uri: this.state.officeImage }}
									style={styles.avatarStyleModel}
								/>
								<View
									style={{
										flex: 1,
										flexDirection: 'column',
										marginLeft: 10,
									}}
								>
									<View
										style={{
											flexDirection: 'row',
											alignItems: 'center',
										}}
									>
									</View>

									<View
										style={{
											margin: 0,
											alignItems: 'flex-start',
											marginTop: -10
										}}
									>
										<Rating
											type="star"
											startingValue={this.state.lastScannerOfficeRating}
											readonly
											imageSize={15}
											style={{ alignContent: 'flex-start' }}
										/>
									</View>
									<View
										style={{
											justifyContent: 'center',
											alignItems: 'flex-start',
										}}
									>
										<Text style={styles.text2StyleModel}>
											25 Reviews
										</Text>
										<View
											style={{
												flexDirection: 'row',
												justifyContent: 'flex-start',
												alignItems: 'center',
											}}
										>
										</View>
									</View>
								</View>
							</View>

							<View>
								<Text style={styles.text3StyleModel}>
									{this.state.lastScannerOfficeName}
								</Text>
							</View>

							<View>
								<Text style={styles.text3StyleModel1}>
									<Feather name="map-pin" size={15}/>
									{this.state.lastScannerOfficeLocation}
								</Text>
							</View>

						</View>
					</View>
				</Modal>
				</View>
			</View>
		);
	}

	renderSearchModal() {
		return (
			<View style={styles.modalView}>
				<View
					style={{
						flexDirection: 'row',
						width: '100%',
						height: 36,
						justifyContent: 'space-between',
						alignItems: 'center',
						borderBottomColor: 'lightgrey',
						borderBottomWidth: 0,
						paddingLeft: 10,
						paddingRight: 10,
						borderRadius:3,
					}}
				>
					<Text
						style={{
							fontSize: 16,
							fontFamily: FontStyle.Regular,
							color: 'black',
							paddingLeft: 10,
							textAlign: 'center',
						}}
					>
						{translate('filter')}
					</Text>
					<TouchableOpacity
						onPress={() => this.setState({
							isShowFilter: false,
							jobPosition_id: '',
							hourlyRate_id: '',
							jobTitle_id: '',
							reviewType_id: '',
							country_id: '',
							language_id: ''
						})}
					>
						<Image
							style={{
								width: 20,
								height: 20,
							}}
							resizeMode="contain"
							source={require('../../images/facebook/cross.png')}
						/>
					</TouchableOpacity>
				</View>

				{this.renderJobPosition(this.state.allData.jobPositions)}
				{this.renderHourlyRate(this.state.allData.hourlyRanges)}
				<View
					style={{
						width: '90%',
						margin: 5,
					}}
				>
					<Text
						style={{
							color: Config.primaryColor,
							fontFamily: FontStyle.Bold,
							fontSize: 15,
						}}
					>
						{translate('advance_filter')}
					</Text>
				</View>
				{this.renderReviewType([
					{
						type: translate('official_review'),
						id: 1,
					},
					{
						type: translate('common_review'),
						id: 2,
					},
				])}
				{this.renderJobTitle(this.state.allData.statusList)}
				{this.renderCountry(this.state.allData.countries)}
				{this.renderLanguage(this.state.allData.languages)}

				<TouchableOpacity
					style={{
						width: '90%',
						height: 40,
						justifyContent: 'center',
						alignItems: 'center',
						marginTop: 15,
						marginBottom: 15,
						borderRadius: 3,
						backgroundColor: Config.primaryColor,
					}}
					onPress={() => this.applySearch()}
				>
					<Text
						style={{
							color: 'white',
							fontFamily: FontStyle.Bold,
							fontSize: 15,
						}}
					>
						{translate('submit')}
					</Text>
				</TouchableOpacity>
			</View>
		);
	}

	applySearch() {
		let searchStr = `skill_id_list=${this.state.skillList}`;
		console.log('Job Postion -- ', this.state);
		if (this.state.jobPosition_id !== '') {
			searchStr = searchStr.concat(`&job_position_id=${this.state.jobPosition_id}`);
		}
		if (this.state.hourlyRate_id !== '') {
			searchStr = searchStr.concat(`&hourly_range_id=${this.state.hourlyRate_id}`);
		}
		if (this.state.reviewType_id !== '') {
			searchStr = searchStr.concat(`&review_type_id=${this.state.reviewType_id}`);
		}
		if (this.state.jobTitle_id !== '') {
			searchStr = searchStr.concat(`&job_title=${this.state.jobTitle_id}`);
		}
		if (this.state.country_id !== '') {
			searchStr = searchStr.concat(`&hourly_range_id=${this.state.country_id}`);
		}
		if (this.state.language_id !== '') {
			searchStr = searchStr.concat(`&hourly_range_id=${this.state.language_id}`);
		}
		this.setState(
			{
				filter: searchStr,
				isLoading: true,
				isShowFilter: false,
			},
			() => {
				this.findFreelancerAPI(this.state.filter);
			},
		);
	}

	changeHourlyRate = (value) => {
		this.setState({ hourlyRate_id: value });
	};

	changeJobPosition = (value) => {
		this.setState({ jobPosition_id: value });
	};

	changeReviewType = (value) => {
		this.setState({ reviewType_id: value });
	};

	changeJobTitle = (value) => {
		this.setState({ jobTitle_id: value });
	};

	changeCountry = (value) => {
		this.setState({ country_id: value });
	};

	changeHourlyRateLanguage = (value) => {
		this.setState({ language_id: value });
	};

	renderJobPosition(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
					<Picker style={{justifyContent:'center'}}
						selectedValue={this.state.jobPosition_id}
						itemStyle={styles.dataItem}
						style={styles.dataPicker}
						onValueChange={itemValue => this.changeJobPosition(itemValue)}
					>
						<Picker.Item label={translate('Job_Position')} value="" key=""/>
						{data.map((item, key) => (
							<Picker.Item
								label={this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? `${item.name_cn} | ${item.remark_cn}` : `${item.name_en} | ${item.remark_en}`}
								value={item.id}
								key={item.id}
							/>
						))}
					</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ jobPositionModel: true })}
						>
							<Text style={{ padding: 5, }}>
								{data.map((item) => {
									if (item.id === this.state.jobPosition_id) {
										show = false;
										return item.name_en;
									}
								})}
								{show === true ? translate('Job_Position') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.jobPositionModel}
							title={translate('Job_Position')}
							onTouchOutside={() =>
								this.setState({ jobPositionModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
							<Picker
								selectedValue={this.state.jobPosition_id}
								itemStyle={styles.dataItem}
								style={styles.dataPicker}
								onValueChange={itemValue => this.changeJobPosition(itemValue)}
							>
								<Picker.Item label={translate('Job_Position')} value="" key=""/>
								{data.map((item) => (
									<Picker.Item
										label={this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? `${item.name_cn} | ${item.remark_cn}` : `${item.name_en} | ${item.remark_en}`}
										value={item.id}
										key={item.id}
									/>
								))}
							</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ jobPositionModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}

	renderHourlyRate(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
					<Picker
						selectedValue={this.state.hourlyRate_id}
						itemStyle={styles.dataItem}
						style={styles.dataPicker}
						onValueChange={itemValue => this.changeHourlyRate(itemValue)}
					>
						<Picker.Item label={translate('hourly_rate')} value="" key=""/>
						{data.map((item, key) => (
							<Picker.Item label={item.name} value={item.id} key={item.id}/>
						))}
					</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ hourlyRateModel: true })}
						>
							<Text style={{ padding: 5,  }}>
								{data.map((item) => {
									if (item.id === this.state.hourlyRate_id) {
										show = false;
										return item.name;
									}
								})}
								{show === true ? translate('hourly_rate') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.hourlyRateModel}
							title={translate('hourly_rate')}
							onTouchOutside={() =>
								this.setState({ hourlyRateModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
								<Picker
									selectedValue={this.state.hourlyRate_id}
									itemStyle={styles.dataItem}
									style={styles.dataPicker}
									onValueChange={itemValue => this.changeHourlyRate(itemValue)}
								>
									<Picker.Item label={translate('hourly_rate')} value="" key=""/>
									{data.map((item) => (
										<Picker.Item label={item.name} value={item.id} key={item.id}/>
									))}
								</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ hourlyRateModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}

	renderReviewType(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
				<Picker
					selectedValue={this.state.reviewType_id}
					itemStyle={styles.dataItem}
					style={styles.dataPicker}
					onValueChange={itemValue => this.changeReviewType(itemValue)}
				>
					<Picker.Item label={translate('select_review')} value="" key="" />
					{data.map((item, key) => (
						<Picker.Item label={item.type} value={item.id} key={item.id} />
					))}
				</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ reviewTypeModel: true })}
						>
							<Text style={{ padding: 5,  }}>
								{data.map((item) => {
									if (item.id === this.state.reviewType_id) {
										show = false;
										return item.type;
									}
								})}
								{show === true ? translate('select_review') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.reviewTypeModel}
							title={translate('select_review')}
							onTouchOutside={() =>
								this.setState({ reviewTypeModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
								<Picker
									selectedValue={this.state.reviewType_id}
									itemStyle={styles.dataItem}
									style={styles.dataPicker}
									onValueChange={itemValue => this.changeReviewType(itemValue)}
								>
									<Picker.Item label={translate('select_review')} value="" key="" />
									{data.map((item) => (
										<Picker.Item label={item.type} value={item.id} key={item.id} />
									))}
								</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ reviewTypeModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}

	renderCountry(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
				<Picker
					selectedValue={this.state.country_id}
					itemStyle={styles.dataItem}
					style={styles.dataPicker}
					onValueChange={itemValue => this.changeCountry(itemValue)}
				>
					<Picker.Item label={translate('select_country')} value="" key="" />
					{data.map((item, key) => (
						<Picker.Item label={item.name} value={item.id} key={item.id} />
					))}
				</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ countryModel: true })}
						>
							<Text style={{ padding: 5,  }}>
								{data.map((item) => {
									if (item.id === this.state.country_id) {
										show = false;
										return item.name;
									}
								})}
								{show === true ? translate('select_country') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.countryModel}
							title={translate('select_country')}
							onTouchOutside={() =>
								this.setState({ countryModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
								<Picker
									selectedValue={this.state.country_id}
									itemStyle={styles.dataItem}
									style={styles.dataPicker}
									onValueChange={itemValue => this.changeCountry(itemValue)}
								>
									<Picker.Item label={translate('select_country')} value="" key="" />
									{data.map((item, key) => (
										<Picker.Item label={item.name} value={item.id} key={item.id} />
									))}
								</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ countryModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}

	renderLanguage(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
				<Picker
					selectedValue={this.state.language_id}
					itemStyle={styles.dataItem}
					style={styles.dataPicker}
					onValueChange={itemValue => this.changeHourlyRateLanguage(itemValue)}
				>
					<Picker.Item label={translate('select_language')} value="" key="" />
					{data.map((item, key) => (
						<Picker.Item label={item.name} value={item.id} key={item.id} />
					))}
				</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ languageModel: true })}
						>
							<Text style={{ padding: 5,  }}>
								{data.map((item) => {
									if (item.id === this.state.language_id) {
										show = false;
										return item.name;
									}
								})}
								{show === true ? translate('select_language') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.languageModel}
							title={translate('select_language')}
							onTouchOutside={() =>
								this.setState({ languageModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
								<Picker
									selectedValue={this.state.language_id}
									itemStyle={styles.dataItem}
									style={styles.dataPicker}
									onValueChange={itemValue => this.changeHourlyRateLanguage(itemValue)}
								>
									<Picker.Item label={translate('select_language')} value="" key="" />
									{data.map((item) => (
										<Picker.Item label={item.name} value={item.id} key={item.id} />
									))}
								</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ languageModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}

	renderJobTitle(data) {
		let show = true;
		return (
			<View style={styles.dataContainer}>
				{Platform.OS === 'android' ?
				<Picker
					selectedValue={this.state.jobTitle_id}
					itemStyle={styles.dataItem}
					style={styles.dataPicker}
					onValueChange={itemValue => this.changeJobTitle(itemValue)}
				>
					<Picker.Item label={translate('Job_Title')} value="" key="" />
					{data.map((item, key) => (
						<Picker.Item
							label={`${item.name}`}
							value={item.id}
							key={item.id}
						/>
					))}
				</Picker> :
					<View>
						<TouchableOpacity
							onPress={() => this.setState({ jobTitleModel: true })}
						>
							<Text style={{ padding: 5,  }}>
								{data.map((item) => {
									if (item.id === this.state.jobTitle_id) {
										show = false;
										return item.name;
									}
								})}
								{show === true ? translate('Job_Title') : ''}
							</Text>
						</TouchableOpacity>
						<Dialog
							visible={this.state.jobTitleModel}
							title={translate('hourly_rate')}
							onTouchOutside={() =>
								this.setState({ jobTitleModel: false })
							}
						>
							<View style={{ height: width / 1.5 }}>
								<Picker
									selectedValue={this.state.jobTitle_id}
									itemStyle={styles.dataItem}
									style={styles.dataPicker}
									onValueChange={itemValue => this.changeJobTitle(itemValue)}
								>
									<Picker.Item label={translate('Job_Title')} value="" key="" />
									{data.map((item) => (
										<Picker.Item
											label={`${item.name}`}
											value={item.id}
											key={item.id}
										/>
									))}
								</Picker>
							</View>
							<TouchableOpacity style={styles.submitInfo}
											  onPress={() => this.setState({ jobTitleModel: false }) }
							>
								<Text
									style={{
										color: Config.white,
										fontFamily: FontStyle.Regular,
									}}
								>{translate('submit')}</Text>
							</TouchableOpacity>
						</Dialog>
					</View>
				}
			</View>
		);
	}
}

const style = {
	rootStyle: {
		backgroundColor: "white",
		// paddingHorizontal: 10
	},

	navigationWrapper: {
		flexDirection: "row",
		justifyContent: "space-between",
		width: "100%",
	},
	navigationStyle: {
		height: 55,
		flexDirection: "row",
		marginTop: 10,
		paddingHorizontal: 5,
		// alignItems:'center'
	},
}

const styles = {
	rootStyle: {
		flexDirection: 'column',
		flex: 1,
		backgroundColor: '#f7f7f7',
	},

	submitInfo: {
		alignItems: 'center',
		justifyContent: 'center',
		height: 45,
		width: width / 1.3,
		borderRadius: 3,
		backgroundColor: Config.primaryColor,
	},

	boxInsideStyle: {
		flexDirection: 'column',
		backgroundColor: '#fff',
	},

	modalContainer: {
		width,
		height,
		flexDirection: 'column',
		backgroundColor: 'rgba(44, 62, 80, 0.6)',
		alignItems: 'center',
		justifyContent: 'center',
	},

	modalView: {
		alignSelf: 'center',
		width: '90%',
		height: 480,
		backgroundColor: '#fff',
		elevation: 20,
		justifyContent: 'space-around',
		alignItems: 'center',
		borderRadius:3,
	},

	topSideStyle: {
		flexDirection: 'row',
		paddingHorizontal: 10,
		paddingTop: 0,
	},

	middleSideStyle: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		paddingHorizontal: 10,
	},

	avatarStyle: {
		width: 50,
		height: 50,
		top: 7,
		borderRadius: 25,
	},

	avatarStyleModel: {
		width: 100,
		height: 50,
		marginTop: -15
	},

	cardButtonStyle: {
		flex: 1,
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
	},

	cardButtonTextStyle: {
		color: Config.textSecondaryColor,
		fontSize: 12,
		fontFamily: FontStyle.Regular,
	},

	text1Style: {
		flex: 1,
		fontSize: 16,
		marginRight: 20,
		fontFamily: FontStyle.Bold,
	},

	text2Style: {
		fontSize: 13,
		color: '#bdc3c7',
		fontFamily: FontStyle.Regular,
	},

	text2StyleModel: {
		fontSize: 13,
		marginLeft: 12,
		color: '#bdc3c7',
		fontFamily: FontStyle.Regular,
	},

	text3Style: {
		marginVertical: 5,
		fontSize: 14,
		fontFamily: FontStyle.Bold,
		color: Config.primaryColor,
		paddingBottom: 5,
	},

	text3StyleModel: {
		marginLeft: 10,
		marginTop: 3,
		fontSize: 20,
		fontFamily: FontStyle.Bold,
	},

	text3StyleModel1: {
		marginLeft: 12,
		fontSize: 14,
		fontFamily: FontStyle.Light,
		paddingBottom: 5,
	},

	starStyle: {
		flexDirection: 'row',
	},

	iconStyle1: {
		flexDirection: 'row',
	},

	skillsStyle: {
		borderRadius: 15,
		borderColor: '#fff',
		borderWidth: 1,
		flexDirection: 'row',
		alignItems: 'center',
		marginRight: 5,
		padding: 5,
	},

	amountInputWrapper: {
		marginBottom: 5,
		borderWidth: 1,
		borderColor: '#e6e6e6',
	},

	amountInputTitle: {
		color: '#979797',
		backgroundColor: '#f6f6f6',
		padding: 1,
		fontSize: 10,
		fontFamily: FontStyle.Bold,
	},

	dataContainer: {
		width: '90%',
		height: 40,
		justifyContent: 'center',
		marginTop: 5,
		borderRadius: 3,
		borderColor: 'lightgrey',
		borderWidth: 1,
	},

	dataItem: {
		fontSize: 18,
		fontFamily: FontStyle.Regular,
	},

	dataPicker: {
		height: 30,
		width: '95%',
		color: '#979797',
		margin: 5,
	},
	logoCircleStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 42,
		height: 42,
		borderRadius: 21,
		backgroundColor: '#fff',
		zIndex: 1,
	},
	modalRootStyle: {
		flex: 1,
		backgroundColor: 'rgba(52, 52, 52, 0.8)',
		alignItems: 'center',
		justifyContent: 'center',
	},
	modalBoxStyle: {
		width: width - 60,
		height: width / 1.7,
		backgroundColor: Config.white,
		// alignItems: 'center',
		borderRadius: 5,
	},
	infoModalBoxStyle: {
		width: width - 120,
		height: width / 2.5,
		backgroundColor: Config.white,
		// alignItems: 'center',
		borderRadius: 5,
	},
};

function mapStateToProps(state) {
	return {
		component: state.component,
		userData: state.auth.userData,
	};
}

export default connect(mapStateToProps)(SearchResult);
