/* eslint-disable no-unused-vars */
/* eslint-disable block-spacing */
/* eslint-disable semi */
/* eslint-disable react/prop-types */
/* eslint-disable class-methods-use-this */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import HttpRequest from '../../components/HttpRequest';
import LocalData from '../../components/LocalData';
import { Sentry } from 'react-native-sentry';
import { NavigationActions } from 'react-navigation';
import { translate } from '../../i18n';

const { height, width } = Dimensions.get('window');
class GuestRegister extends Component {
    static navigationOptions = {
      header: null,
    };

    constructor(props) {
      super(props);

      this.state = {
        key: this.props.navigation.state.params.key,
        email: this.props.navigation.state.params.email,
        value1: '',
        value2: '',
        value3: '',
        value4: '',
        value5: '',
        value6: '',
        time: 9,
        sec: 59,
        isLoading: false,
        initial: true,
      };
    }
    componentDidMount() {
        console.log('GuestRegister.js');
      this.interval = setInterval(
        () => this.setState(prevState => ({ sec: prevState.sec - 1 })),
        1000,
      );
      this.handleKeyPress = this.handleKeyPress.bind(this);
      this.handleKeyPress1 = this.handleKeyPress1.bind(this);
      this.handleKeyPress2 = this.handleKeyPress2.bind(this);
      this.handleKeyPress3 = this.handleKeyPress3.bind(this);
      this.handleKeyPress4 = this.handleKeyPress4.bind(this);
    }

    handleKeyPress = ({ nativeEvent: { key: keyValue } }) => {
      if (keyValue === 'Backspace') {
        this.refs.input_1.focus();
      }
    };

    handleKeyPress1 = ({ nativeEvent: { key: keyValue } }) => {
      if (keyValue === 'Backspace') {
        this.refs.input_2.focus();
      }
    };

    handleKeyPress2 = ({ nativeEvent: { key: keyValue } }) => {
      if (keyValue === 'Backspace') {
        this.refs.input_3.focus();
      }
    };

    handleKeyPress3 = ({ nativeEvent: { key: keyValue } }) => {
      if (keyValue === 'Backspace') {
        this.refs.input_4.focus();
      }
    };

    handleKeyPress4 = ({ nativeEvent: { key: keyValue } }) => {
      if (keyValue === 'Backspace') {
        this.refs.input_5.focus();
      }
    };

    componentDidUpdate() {
      if (this.state.time === -1) {
        clearInterval(this.interval);
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({
              routeName: 'SignUp',
            }),
          ],
        }));
      }
      if (this.state.sec === 0) {
        this.setState({ sec: 59, time: this.state.time - 1 });
      }
    }

    componentWillUnmount() {
      clearInterval(this.interval);
    }

    submit() {
      const data =
            this.state.value1 +
            this.state.value2 +
            this.state.value3 +
            this.state.value4 +
            this.state.value5 +
            this.state.value6;
      HttpRequest.registered(data, this.state.key)
        .then((response) => {
          if (response.data.status === 'success') {
            this.props.userLogin(response.data.user, response.data.access_token);
            LocalData.setUserData({
              token: response.data.access_token,
              ...response.data.user,
            });
            this.setState({ isLoading: false, initial: true });
            this.props.navigation.dispatch(NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'Dashboard',
                }),
              ],
            }));
          } else {
            alert(response.data.message);
            this.setState({ isLoading: false, initial: true });
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false, initial: true });
          Sentry.captureException(error);
          alert('Network error', error);
        });
    }

    value = (val) => {
      this.setState({ value6: val });
      Keyboard.dismiss();
      this.setState({ isLoading: true });
      setTimeout(() => {
        if (this.state.initial) {
          this.setState({ initial: false });
          this.submit();
        }
      }, 2000);
    };

    resend() {
      HttpRequest.resendCode(this.state.email, this.state.key)
        .then((response) => {
          const result = response.data;
          if (result === 'success') {
            alert(result.message);
          } else {
            // alert(result.message);
              this.props.navigation.dispatch(NavigationActions.reset({
                  index: 0,
                  actions: [
                      NavigationActions.navigate({
                          routeName: 'SignUp',
                      }),
                  ],
              }));
          }
        })
        .catch((error) => {
          Sentry.captureException(error);
          alert(error);
        });
    }

    render() {
      return (
            <View style={styles.rootStyle}>
                <View style={{ height: height / 3.8 }}>
                    <View style={styles.headerStyle}>
                        <TouchableOpacity
                            style={styles.leftArrow}
                        >
                            <SimpleLineIcons
                                size={20}
                                name="arrow-left"
                                color="black"
                            />
                        </TouchableOpacity>
                        <View style={styles.codeText}>
                            <Text style={{
                                color: Config.white,
                                fontFamily: FontStyle.Regular,
                                fontSize: 18,
                            }}>
                                {translate('code_verify')}
                            </Text>
                        </View>
                    </View>
                    <Image
                        source={require('../../../images/inboxletter.png')}
                        style={{
                            height: 82,
                            width: 80,
                            top: -35,
                            alignSelf: 'center',
                        }}
                    />
                </View>
                <KeyboardAvoidingView behavior="position" enabled>
                    <ScrollView>
                        <View style={styles.container}>
                            <View style={styles.upperArea}>
                                <Text style={styles.bottomText}>
                                    {translate('enter_six_digit_code')}
                                </Text>
                                <Text style={styles.bottomText}>
                                    {translate('we_send_you')}.
                                </Text>
                            </View>
                            <View
                                style={styles.bottomArea}
                                keyboardShouldPersistTaps="always"
                            >
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        ref="input_1"
                                        maxLength={1}
                                        keyboardType="phone-pad"
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(value1) => {
                                            this.setState({ value1 });
                                            if (value1) this.refs.input_2.focus();
                                        }}
                                        value={this.state.value1}
                                    />
                                </View>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        maxLength={1}
                                        ref="input_2"
                                        onKeyPress={ this.handleKeyPress }
                                        keyboardType="phone-pad"
                                        autoFocus={this.state.autoFocus1}
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(value2) => {
                                            this.setState({ value2 });
                                            if (value2) { this.refs.input_3.focus(); }
                                        }}
                                        value={this.state.value2}
                                    />
                                </View>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        maxLength={1}
                                        ref="input_3"
                                        onKeyPress={ this.handleKeyPress1 }
                                        keyboardType="phone-pad"
                                        autoFocus={this.state.autoFocus2}
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(value3) => {
                                            this.setState({ value3 });
                                            if (value3) this.refs.input_4.focus();
                                        }}
                                        value={this.state.value3}
                                    />
                                </View>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        maxLength={1}
                                        ref="input_4"
                                        onKeyPress={ this.handleKeyPress2 }
                                        keyboardType="phone-pad"
                                        autoFocus={this.state.autoFocus3}
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(value4) => {
                                            this.setState({ value4 });
                                            if (value4) this.refs.input_5.focus();
                                        }}
                                        value={this.state.value4}
                                    />
                                </View>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        maxLength={1}
                                        ref="input_5"
                                        onKeyPress={ this.handleKeyPress3 }
                                        keyboardType="phone-pad"
                                        autoFocus={this.state.autoFocus4}
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(value5) => {
                                            this.setState({ value5 });
                                            if (value5) this.refs.input_6.focus();
                                        }}
                                        value={this.state.value5}
                                    />
                                </View>
                                <View style={styles.inputText}>
                                    <TextInput
                                        style={styles.inputBox}
                                        maxLength={1}
                                        ref="input_6"
                                        onKeyPress={ this.handleKeyPress4 }
                                        keyboardType="phone-pad"
                                        autoFocus={this.state.autoFocus5}
                                        // secureTextEntry={true}
                                        underlineColorAndroid="transparent"
                                        onChangeText={value6 => this.value(value6)}
                                        onSubmitEditing={() => this.submit()}
                                        value={this.state.value6}
                                    />
                                </View>
                            </View>
                            <View style={styles.expire}>
                                <Text style={{
                                    color: Config.textSecondaryColor,
                                    fontFamily: FontStyle.Light,
                                }}
                                >
                                    {translate('code_expire_in')}{' '}:
                                </Text>
                                <Text
                                    style={{
                                        color: 'red',
                                        fontFamily: FontStyle.Light,
                                        marginLeft: 5,
                                    }}
                                >
                                    {this.state.time} {':'} {this.state.sec}
                                </Text>
                            </View>
                            <View style={styles.footerArea}>
                                <TouchableOpacity
                                    style={styles.continueBtn}
                                    onPress={() => {
                                        this.submit();
                                    }}
                                >
                                    {this.state.isLoading === true && (
                                        <ActivityIndicator color="#fff" />
                                    )}
                                    {this.state.isLoading === false && (
                                        <Text style={{
                                            color: Config.white,
                                            fontFamily: FontStyle.Regular,
                                        }}>
                                            {translate('continue').toUpperCase()}
                                        </Text>
                                    )
                                    }
                                </TouchableOpacity>
                            </View>
                            <View style={styles.resendCode}>
                                <Text
                                    style={{
                                        color: Config.textSecondaryColor,
                                        fontFamily: FontStyle.Light,
                                    }}
                                >
                                    {translate('get_code')}?
                                </Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.resend();
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Light,
                                            marginLeft: 5,
                                        }}
                                    >
                                        {translate('resend')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </View>
      );
    }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
  },
  headerStyle: {
    height: height / 5.5,
    backgroundColor: Config.primaryColor,
  },
  leftArrow: {
    left: 15,
    top: 25,
  },
  codeText: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 5,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 45,
  },
  upperArea: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomArea: {
    marginTop: 10,
    alignSelf: 'center',
    flexDirection: 'row',
    height: 50,
  },
  inputText: {
    marginLeft: 10,
    alignSelf: 'center',
  },
  inputBox: {
    borderBottomWidth: 1,
    fontSize: 24,
    fontFamily: FontStyle.Regular,
    width: 40,
    borderBottomColor: Config.textSecondaryColor,
  },
  footerArea: {
    marginTop: 28,
    alignSelf: 'center',
  },
  bottomText: {
    fontFamily: FontStyle.Light,
    fontSize: 14,
  },
  expire: {
    alignSelf: 'center',
    marginTop: 18,
    flexDirection: 'row',
  },
  resendCode: {
    alignSelf: 'center',
    marginTop: 15,
    flexDirection: 'row',
  },
  continueBtn: {
    backgroundColor: Config.primaryColor,
    width: width / 2,
    height: 45,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
    userLogin: (userData, token) =>
      dispatch({
        type: 'LOGIN_SUCCESS',
        data: { ...userData, token },
      }),
    // userLogin: userData =>
    //   dispatch({
    //     type: 'LOGIN_SUCCESS',
    //     data: userData,
    //   }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GuestRegister);
