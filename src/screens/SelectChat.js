/* eslint-disable max-len */
/* eslint-disable no-lone-blocks */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Alert,
  FlatList,
  Image,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  PermissionsAndroid
} from 'react-native';

import Feather from "react-native-vector-icons/Feather";
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {Input, SearchBar} from 'react-native-elements';
import FooterTabs from './FooterTabs';
import { translate } from '../i18n';

import Config from '../Config';
import HttpRequest from '../components/HttpRequest';
import FontStyle from '../constants/FontStyle';
import axios from 'axios';
import { ResultItem } from '../components/coworking/search';
import Icon from "react-native-fontawesome";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
class SelectChat extends Component {
    static navigationOptions = {
        header: null,
    };

  constructor(props) {
    super(props);

    this.state = {
      list: [],
      filteredList: [],
      isLoading: true,
      access_token: '',
      search: '',
      latitude: '',
      longitude: '',
      onScroll: false,
      nextUrl: '',
      second: true,
      ip4: null,
      resp: '',
      locale: '',
      imgLoading: true,
      showMapIcon: false,
      filteredResult: []
    };

    this.root = this.props.component.root;
  }

  componentDidMount() {
    this.setState({
      showMapIcon: true
    })
    this.requestAccess();
    this.getGeoInfo();
  }

    requestAccess = () => {
      if(Platform.OS==='android'){
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': translate('location_permission'),
            'message': translate('app_need_permission') +
                      translate('show_nearest_offices'),
          },
        ).then((granted) => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            navigator.geolocation.getCurrentPosition(
              (position) => {
                let lat = position.coords.latitude;
                let long = position.coords.longitude;
                this.setState({access_token:this.props.userData.token, latitude: lat, longitude: long });
                this.getChatRooms(this.props.userData.token, lat, long, '');
              },
              (error) => {
                this.getGeoInfo();
              },
              { enableHighAccuracy: false, timeout: 4000, maximumAge: 1000 },
            );
          }
          else {
            this.getGeoInfo();
          }
        })
          .catch((err) => {
            this.getChatRooms(this.props.userData.token, '', '', '');
          });
      }
      else{
        navigator.geolocation.getCurrentPosition(
          (position) => {
            let lat = position.coords.latitude;
            let long = position.coords.longitude;
            this.setState({ latitude: lat, longitude: long });
            this.getChatRooms(this.props.userData.token, lat, long, '');
          },
          (error) => {
            this.getGeoInfo();
          },
          { enableHighAccuracy: false, timeout: 3000, maximumAge: 1000 },
        );
      }

    };

  getGeoInfo = () => {
      const access_token = this.props.userData.token;
      axios.get(Config.getIpAddress, {
        headers: {
          Accept: 'application/json',
        },
      }).then((response) => {
          console.log('response');
          this.setState({ ip4: response.data.ip });
        this.getChatRooms(this.props.userData.token, '', '', response.data.ip);
      })
        .catch((error) => {
          alert(translate('network_error'));
          this.setState({ isLoading: false });
          this.getChatRooms(this.props.userData.token, '', '', '');
        });
    };

  applyFilter = () => {
      let highDistance = this.props.navigation.state.params.highDistance;
      let lowDistance = this.props.navigation.state.params.lowDistance;
      var t = this;
      let filteredResult = this.state.filter(function(item){
          if(item.distance < highDistance || item.distance > lowDistance) {
              return item;
          }
      });

      this.setState({
          filteredResult: filteredResult
      })
  }

  getChatRooms = (access_token, lat, lang, ip) => {
    HttpRequest.getSharedOffices(access_token, lat, lang, ip)
      .then((response) => {
        const result = response.data.data;
        if (response.data.next_page_url !== '') {
          this.setState({ nextUrl: response.data.next_page_url });
        }
        if (response.status === 200) {
          console.log(result, '<< result')
          this.setState({
            list: result,
            filteredList: result,
            listLength: result.length,
            isLoading: false,
          });
          this.applyFilter();
        } else {
          this.setState({ isLoading: false });
          alert(response.data.message);
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
      });
  };

  _renderSeparator = () => (
    <View
      style={{
        height: 10,
        backgroundColor: '#f7f7f7',
        marginLeft: '0%',
      }}
    />
  );

  goToChatRoomDetail = (item) => {
    this.props.navigation.navigate('ChatDetail', { chatData: item, locale: this.state.locale });
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 1,
          borderTopWidth: 1,
          borderColor: '#ecf0f1',
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  onRefresh() {
    this.setState({ isLoading: true });
    this.requestAccess();
  }
  onLoad = () => {
    this.setState({ imgLoading: false });
  };

  _renderResultItems = ({ item }) => (
    <ResultItem
      item={item}
      locale={this.state.locale}
      onPress={() => this.goToChatRoomDetail(item)}/>
  );

  clearSearchText = () => {
  };

  _reachEnd = () => {
      console.log('scroll down to get more offices');
      console.log('this.state.search');
      console.log(this.state.search && this.state.search.length);
      if(this.state.search){
          console.log('only search items shown');
          console.log(this.state.search && this.state.search.length);
      } else {
          console.log('scroll item shown');
          this.setState({onScroll: true});
          let url = '';
          let ip = '';
          if (this.state.latitude === '') {
              ip = this.state.ip4;
              url = `${this.state.nextUrl}&lat=${this.state.latitude}&lang=${this.state.longitude}&ip=${ip}&mobileApp=${true}`;
          } else {
              url = `${this.state.nextUrl}&lat=${this.state.latitude}&lang=${this.state.longitude}&ip=${ip}&mobileApp=${true}`;
          }
          let access_token = this.props.userData.token;
          this.getMoreOffices(access_token, url);
      }
  };
    goBackPage(){
        this.props.navigation.goBack();
    }
  getMoreOffices = (access_token, url) => {
    HttpRequest.getMoreSharedOfficeData(access_token, url)
      .then((response) => {
        let result = response.data.data;
        if (result.last_page_url !== this.state.nextUrl) {
          this.setState({ onScroll: false, nextUrl: response.data.next_page_url });
          const arr = [...this.state.filteredList, ...result];
          this.setState({ list: arr, filteredList: arr, listLength: arr.length });
        } else {
          if (this.state.second === true) {
            this.setState({ onScroll: false, nextUrl: result.next_page_url, second: false });
            const arr = [...this.state.filteredList, ...result];
            this.setState({ list: arr, filteredList: arr, listLength: arr.length });
          } else {
            this.setState({ onScroll: false });
          }
        }
      this.applyFilter();
      }).catch((error) => {
        this.setState({ onScroll: false });
      });
  };


  navigateNow = () => {
      this.props.navigation.navigate('SharedOfficeFiltersMaps', {
          access_token: this.props.userData.token,
          sharedOffices: this.state.list,
          ip4: this.state.ip4,
          lat: this.state.latitude,
          lng: this.state.longitude
      });
  }

  search(text) {
    this.setState({search:text});
    let search = text.toLowerCase();
    let fullList = this.state.list;
    console.log('this is full list');
    console.log(fullList);
    let filteredList = fullList.filter((item) => {
          if (item.office_name !== null) {
              console.log('string match');
              console.log(item.office_name.toLowerCase());
              if (
                  item.location.toLowerCase().match(text) ||
                  item.office_name.toLowerCase().match(text)
              ) {
                  return item;
              }
          }
    });

    if(text === '') {
        console.log('text field is empty get full list');
        console.log(this.state.access_token);
        console.log(this.state.latitude);
        console.log(this.state.longitude);
        console.log(this.state.ip4);
        this.requestAccess()
        // this.getChatRooms(this.state.access_token,this.state.latitude,this.state.longitude,'');
    }
    console.log('filteredList');
    console.log(filteredList);

      this.setState({
          list: filteredList,
          listLength: fullList.length,
          search: '',
      });

      if (!search || search === '') {
          this.setState({
              filteredList: fullList,
              listLength: fullList.length,
              search: '',
          });
      } else if (!filteredList.length) {
          this.setState({
              filteredList,
              listLength: filteredList.length,
              search: text,
          });
      } else if (Array.isArray(filteredList)) {
          this.setState({
              filteredList,
              listLength: filteredList.length,
              search: text,
          });
      }
  }

  render() {
    return (
      <View style={styles.rootStyle}>
          <View style={style.rootStyle}>
              {/* Back Button View */}
              <View style={style.navigationStyle}>
                  <View style={style.navigationWrapper}>
                      <TouchableOpacity
                          onPress={() => this.goBackPage()}
                          style={{
                              marginTop: 8,
                              marginLeft: 4,
                              flexDirection: "row",
                          }}
                      >
                          <Image
                              style={{width:20,height:26,position:'relative',top:-3}}
                              source={require('../../images/arrowLA.png')}
                          />

                          <Text
                              onPress={() => this.goBackPage()}
                              style={{
                                  marginLeft: 10,
                                  marginTop:-1,
                                  fontSize: 19.5,
                                  fontWeight:'normal',
                                  color: Config.primaryColor,
                                  fontFamily: FontStyle.Regular,
                              }}
                          >
                              {translate('cowork_space')}
                          </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                          style={{ marginLeft: 0,marginRight:10,marginTop:8 }}
                          onPress={() => {
                              this.props.navigation.navigate('SharedOfficeFilters');
                          }}
                      >
                          <View style={styles.logoutButtonStyle}>
                              <Feather name="filter" size={28} color={Config.primaryColor}/>
                          </View>
                      </TouchableOpacity>
                  </View>
              </View>
          </View>
        <SearchBar
          lightTheme
          barStyle="default"
          autoCorrect={false}
          searchBarStyle="minimal"
          placeholder={translate('search')}
          searchIcon={{ size: 24 }}
          icon={{
            type: 'font-awesome',
            name: 'search',
            marginTop: 15,
            marginRight: 15,
            marginBottom: 15,
          }}
          leftIconContainerStyle={{
            marginLeft: 0,
            paddingLeft: 8,
            paddingRight: 8,
          }}
          rightIconContainerStyle={{
            justifyContent: "center",
            alignItems: "center",
            marginLeft: 0,
            paddingLeft: 8,
          }}
          cancelIcon={false}
          containerStyle={{
            backgroundColor: '#f7f7f7',
            width: '100%',
            flexDirection: 'row-reverse',
            alignContent: 'space-between',
            justifyContent: 'space-between',
            borderTopWidth: 0,
            borderBottomWidth: 0,
          }}
          inputContainerStyle={{
            backgroundColor: '#ffffff',
            flexDirection: 'row-reverse',
            alignContent: 'flex-start',
            justifyContent: 'flex-start',
            margin: 0,
          }}
          inputStyle={{
            backgroundColor: '#ffffff',
            width: '100%',
            marginLeft: 0
          }}
          value={this.state.search}
          onChangeText={text => this.search(text)}
          onCancel={this.clearSearchText}
        />
        <Text
          style={{
            borderTopColor: '#ecf0f1',
            paddingBottom: 10,
            paddingLeft: 5,
            width: '100%',
          }}
        >
          {' '}
          {this.state.listLength}{' '}{translate('spaces')}{' '}{translate('found')}
        </Text>
        <FlatList
          data={this.state.list}
          removeClippedSubviews={false}
          style={{ width: "100%", paddingLeft: 10, paddingRight: 10 }}
          keyExtractor={(result, key) => key.toString()}
          ItemSeparatorComponent={this._renderSeparator}
          renderItem={this._renderResultItems}
          onEndReached={this._reachEnd}
          onEndReachedThreshold={0.5}
        />
          {this.state.showMapIcon && <TouchableOpacity
              style={{
                  // borderWidth:1,
                  // borderColor:'green',
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: 60,
                  position: 'absolute',
                  bottom: 20,
                  right: 10,
                  height: 60,
                  backgroundColor:'#fff',
                  borderRadius: 100,
              }}
          onPress={() => this.navigateNow()}
          >
              <Image source={require('../../images/sharedOffices/mapview.png')} style={{
                  position: "absolute",
                  bottom: 0,
                  right: 0,
                  width: 60,
                  height: 60
              }}/>
          </TouchableOpacity>}
      </View>
    ); // return
  } // render
} // PaymentRecord

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

const styles = {
  rootStyle: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
    alignItems: 'center',
  },

  boxInsideStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },

  greyedTextDescriptionStyle: {
    color: '#c3c3c3',
    fontSize: 18,
    fontWeight: '100',
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectChat);
