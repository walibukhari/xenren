import React, { Component } from 'react';
import {Modal, View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Config from '../../Config';
const PopUp = ({ callback, showModal}) => {
  // console.log('Camera pop up')
    onSelectImage = (data) => {
        callback(data, false);
    }

    onTakePhoto =() => {
        const options = {
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        // showModal(false);
        // this.setState({ showModal  : false});
    
        ImagePicker.launchCamera(options, (response) => {
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };
            this.onSelectImage(response);
          }
        });
      }
    
      onOpenGallery = () => {
        const options = {
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
        // showModal(false);
        // this.setState({  showModal : false});
        ImagePicker.launchImageLibrary(options, (response) => {
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            const source = { uri: response.uri };
            this.onSelectImage(response);
          }
        });
      }
    

    return(
        <Modal
        animationType="fade"
        transparent={true}
        visible={showModal}
        onRequestClose={() => console.log("Modal Closed")}>
        <View 
        style={styles.verifyDialogStyle}
        >
          <View 
          style={styles.verifyInfoBoxStyle}
          >
            <Text style={{ fontSize: 17, fontFamily: 'Montserrat-Medium', padding: 10, textAlign: 'center', paddingBottom: 25 }}>Uplaod Photo</Text>
            <View style={{ width: '90%', height: 100, alignItems: 'flex-start' }}>
              <TouchableOpacity style={{ width: '90%', height: 40, alignItems: 'flex-start', marginHorizontal: 10 }}
                onPress={() => this.onTakePhoto()}>
                <Text style={{ fontSize: 17, fontFamily: 'Montserrat-Medium', padding: 10, textAlign: 'center', color: Config.primaryColor }}>Take Photo</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: '90%', height: 40, alignItems: 'flex-start', marginHorizontal: 10 }}
                onPress={() => this.onOpenGallery()}>
                <Text style={{ fontSize: 17, fontFamily: 'Montserrat-Medium', padding: 10, textAlign: 'center', color: Config.primaryColor }}>Select from Gallery</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={{ width: '90%', height: 45, borderRadius: 3, alignItems: 'center', justifyContent: 'center', marginHorizontal: 10, backgroundColor: Config.primaryColor }}
              onPress={() => callback(null, false)}
              >
              <Text style={{ fontSize: 17, fontFamily: 'Montserrat-Medium', padding: 10, textAlign: 'center', color: 'white' }}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
}

const styles = StyleSheet.create({
  verifyDialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  verifyInfoBoxStyle: {
    width: '85%',
    height: 220,
    borderRadius: 10,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
})
export default PopUp;