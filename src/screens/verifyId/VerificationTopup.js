import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ActivityIndicator,
  View,
  TouchableOpacity,
  Modal,
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import Color from '../../Config';
import FontStyle from '../../constants/FontStyle';

const style = StyleSheet.create({

  scrollStyle: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  modalContainer: {
    borderRadius: 28,
    borderColor: '#fff',
    borderWidth: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    paddingVertical: 10,
    alignItems: 'center',
  },
  closeContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: 3,
  },
  headerContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 15,
  },
  headerText: {
    paddingVertical: 5,
    fontFamily: FontStyle.Regular,
  },
  userImage: {
    marginVertical: 5,
    height: 120,
    width: 120,
  },
  uploadButton: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    width: 130,
    borderColor: Color.primaryColor,
    borderWidth: 2,
    borderRadius: 17,
  },
  nameBoxStyle: {
    width: '100%',
    marginVertical: 15,
    borderWidth: 1,
    borderColor: '#efefef',
    borderRadius: 3,
  },
  addNameContainer: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 25,
    backgroundColor: '#ececec',
  },
  nameLabelStyle: {
    paddingHorizontal: 10,
    fontSize: 12,
    fontFamily: FontStyle.Regular,
    color: 'grey',
  },
  textInputContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInputStyle: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: 10,
  },
  professionView: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    marginVertical: 10,
  },
  professionButton: {
    height: 35,
    borderRadius: 16,
    marginTop: 5,
    justifyContent: 'center',
  },
  submitButton: {
    width: '75%',
    height: 50,
    backgroundColor: Color.btnDarkColor,
    borderRadius: 18,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  submitTextStyle: {
    color: 'white',
    fontSize: 15,
    fontFamily: FontStyle.Regular,
    fontWeight: 'bold',
  },
});

const icSuccess = require('../../../images/ic_success_check.png');
const icError = require('../../../images/ic_error_cross.png');

class VerificationTopup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // isShow: props.showModal,
    };
  }

  handleOK = () => {
    this.props.closeModal();
    this.props.onOK();
  }

  renderHeader = () => {
    const {
      type,
      title,
    } = this.props;

    if (type === 'success') {
      return (
        <View style={style.headerContainer}>
          <Image
            style={{
              width: 120,
              height: 120,
              marginTop: 30,
              marginBottom: 10,
            }}
            resizeMode='contain'
            source={icSuccess}/>
          <Text
            style={[
              style.headerText,
              { fontSize: 23, color: 'green' },
            ]}
          >
            { title }
          </Text>
        </View>
      );
    }
    if (type === 'error') {
      return (
        <View style={style.headerContainer}>
          <Image
            style={{
              width: 120,
              height: 120,
              marginTop: 30,
              marginBottom: 10,
            }}
            resizeMode='contain'
            source={icError}/>
          <Text
            style={[
              style.headerText,
              { fontSize: 23, color: 'red' },
            ]}
          >
            { title }
          </Text>
        </View>
      );
    }
    if (type === 'loading') {
      return (
        <View style={style.headerContainer}>
          <ActivityIndicator
            style={{
              width: 120,
              height: 120,
              marginTop: 10,
              marginBottom: -20,
            }}
            size={70}
          />
        </View>
      );
    }

    return (<View style={style.headerContainer}></View>);
  }

  render() {
    const {
      showModal,
      showOKButton,
      message,
    } = this.props;

    return (
      <Modal
        visible={showModal}
        animationType="fade"
        transparent={true}
        onRequestClose={() => this.props.closeModal()}
      >
        <View style={style.scrollStyle}>
          <View style={style.modalContainer}>
            { this.renderHeader() }

            <Text
              style={[
                style.headerText,
                {
                  fontSize: 20,
                  color: 'black',
                  marginTop: 30,
                  marginBottom: 30,
                  textAlign: 'center',
                },
              ]}
            >
              { message }
            </Text>

            {
              showOKButton
              && <TouchableOpacity
                style={style.submitButton}
                onPress={() => this.handleOK()}
              >
                <Text style={style.submitTextStyle}>OK</Text>
              </TouchableOpacity>
            }

          </View>
        </View>
      </Modal>
    );
  }
}

VerificationTopup.propTypes = {
  // detail: PropTypes.shape({})
  showModal: PropTypes.bool,
  showOKButton: PropTypes.bool,
  type: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  closeModal: PropTypes.func.isRequired,
  onOK: PropTypes.func,
};

VerificationTopup.defaultProps = {
  showModal: false,
  showOKButton: true,
  type: 'success',
  title: '',
  message: '',
  onOK: () => {},
};

export default VerificationTopup;
