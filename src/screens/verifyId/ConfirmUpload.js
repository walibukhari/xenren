import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ActivityIndicator } from 'react-native';

import Config from '../../Config';
import { translate } from '../../i18n';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import FontStyle from '../../constants/FontStyle';

class ConfirmUpload extends Component {
    static navigationOptions = {
      header: null,
    }
    constructor(props) {
      super(props);

      this.state = {
        inProgress: true,
      };

      this.root = this.props.component.root;
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps) {
        this.setState({ inProgress: nextProps.uploading });
        if (!nextProps.uploading) {
          setTimeout(() => {
            this.props.navigation.dispatch({ type: 'goHome' });
          }, 2000);
        }
      }
    }

    render() {
      return (
        <View style={styles.rootStyle}>
          <ActivityIndicator size="large" color={Config.primaryColor} />
          <Text style={{ fontFamily: FontStyle.Regular, fontSize: 22 }}>{translate('confirming')}</Text>
        </View>);
    }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

const mapStateToDispatch = state => ({
  component: state.component,
  uploading: state.auth.uploading,
});

export default connect(mapStateToDispatch)(ConfirmUpload);
