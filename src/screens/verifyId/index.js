import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Config from '../../Config';
import { translate } from '../../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';

class VerifyId extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('balance'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      activePage: 0,
    };


    this.root = this.props.component.root;
  }

  render() {
    const picCover = require('../../../images/other/hold_id.png');
    return (
      <View style={styles.rootStyle}>
        <View style={styles.navigationStyle}>
          <View style={styles.navigationWrapper}>
            <View>
              <Text style={{
                color: '#6e6e6e',
                fontSize: 22,
                fontFamily: 'Montserrat-Light'
              }}
              > {translate('id_submit')} </Text>
            </View>
          </View>
        </View>
        <View style={{
          paddingHorizontal: 5,
          paddingVertical: 5,
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <Text style={styles.submitInfoStyle}>{translate('submit_info_to_use_service')}</Text>
        </View>

        <View style={{
          paddingHorizontal: 5,
          paddingVertical: 5,
          flexDirection: 'column'
        }}>
          <View style={styles.picInfoWrapper}>
            <Text style={styles.picInfoTitleStyle}>{translate('ID_CARD')}</Text>
            <View style={styles.picHolderStyle}>
              <Image source={picCover} resizeMode='contain' style={{
                width: 240,
                height: 160
              }}/>
              <View style={{
                width: 70,
                height: 70,
                borderRadius: 35,
                backgroundColor: Config.primaryColor,
                position: 'absolute',
                top: '30%',
                borderWidth: 2,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Config.primaryColor
              }}>
                <Feather name="loader" size={38} color="#fff"/>
              </View>
            </View>
          </View>
          <View style={styles.picInfoWrapper}>
            <Text style={styles.picInfoTitleStyle}>{translate('HOLDING_ID_CARD')}</Text>
            <View style={styles.picHolderStyle}>
              <Image source={picCover} resizeMode='contain' style={{
                width: 240,
                height: 160
              }}/>
              <View style={{
                width: 70,
                height: 70,
                borderRadius: 35,
                backgroundColor: Config.primaryColor,
                position: 'absolute',
                top: '30%',
                borderWidth: 2,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Config.primaryColor
              }}>
                <Feather name="check" size={38} color="#fff"/>
              </View>
            </View>
          </View>
          <View style={{ marginTop: 10 }}>
            <TouchableOpacity style={styles.depositButtonStyle}
                              // onPress={() => this.root.navigate('Verify')}
            >
              <Text style={{
                fontSize: 20,
                color: '#fff',
                fontFamily: 'Raleway-Bold'
              }}>{translate('CHANGE')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 5
  },
  navigationWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  backArrowStyle: {
    marginRight: 10,
  },
  backArrowIconStyle: {
    fontFamily: 'Montserrat-Light',
    marginLeft: 10
  },

  navigationStyle: {
    height: 45,
    flexDirection: 'row'
  },

  submitInfoStyle: {
    fontFamily: 'Montserrat-Light',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: 18
  },
  depositButtonStyle: {
    backgroundColor: Config.primaryColor,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  picInfoWrapper: {
    flexDirection: 'column',
    paddingVertical: 2,
    width: '100%'
  },
  picInfoTitleStyle: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#666666'
  },
  picHolderStyle: {
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'center'
  }
};

const mapStateToDispatch = state => ({
  component: state.component
});

export default connect(mapStateToDispatch)(VerifyId);
