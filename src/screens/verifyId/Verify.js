import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal
} from "react-native";
import { Header } from "react-native-elements";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import ImagePicker from "react-native-image-picker";
import { translate } from "../../i18n";
import HttpRequest from "../../components/HttpRequest";
import Config from "../../Config";
import FontStyle from "../../constants/FontStyle";
import SuccessModal from "../../components/SuccessModal";

class Verify extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);

    this.state = {
      imageFor: "HOLD_ID",
      holdIDChanged: false,
      imageIdChanged: false,
      isLoading: false,
      isShowSuccess: false,
      isShowError: false,
      errorMessage: "Error",
      holdIDImage: {},
      imageIDImage: {},
      holdID: require("../../../images/other/holdingIDCard.png"),
      imageID: require("../../../images/other/idCard.png"),
      isShowValidSuccess: false,
      validMessage: "",
      showNextScreen: false,
      isModal: false
    };
    this.confirmUpload = this.confirmUpload.bind(this);
    this.root = this.props.component.root;
    this.goBackButton = this.goBackButton.bind(this);
  }

  goBackButton() {
    if (this.state.showNextScreen) {
      this.setState({ showNextScreen: false });
    } else {
      this.props.navigation.goBack();
    }
  }

  onSelectImage(response) {
    if (response.didCancel) {
      console.log("User cancelled image picker");
    } else if (response.error) {
      console.log("ImagePicker Error: ", response.error);
    } else if (response.customButton) {
      console.log("User tapped custom button: ", response.customButton);
    } else {
      const source = { uri: response.uri };
      let imageObject = response.path;
      imageObject = {
        uri: response.uri,
        name: "photo_id.jpg",
        type: "image/jpg"
      };

      // You can also display the image using data:
      // let source = { uri: 'data:image/jpeg;base64,' + response.data };
      if (this.state.imageFor === "HOLD_ID") {
        this.setState({
          holdID: source,
          holdIDImage: imageObject,
          holdIDChanged: true
        });
      } else {
        this.setState({
          imageID: source,
          imageIDImage: imageObject,
          imageIdChanged: true
        });
      }
    }
  }

  confirmUpload() {
    console.log("this.props.userData", this.props.userData);
    console.log("confirmUpload");
    const { id } = this.props.userData;
    if (!this.state.showNextScreen) {
      if (Object.keys(this.state.imageIDImage).length === 0) {
        this.setState({
          isShowValidSuccess: true,
          validMessage: translate("select_photos")
        });
        setTimeout(() => {
          this.setState({
            isShowValidSuccess: false,
            validMessage: ""
          });
        }, 2000);
      } else {
        HttpRequest.uploadPicture("ID", this.state.imageIDImage, id)
          .then(response => {
            console.log(
              "Check is photo uploaded or not ------>> ",
              response.data
            );
          })
          .catch(error => {
            this.setState({
              isLoading: false,
              isShowError: true,
              errorMessage: translate("issue_photo_upload")
            });
            setTimeout(() => {
              this.setState({
                isShowError: false
              });
            }, 2000);
            console.log("print error ----->> ", error);
          });
        this.setState({ showNextScreen: true });
      }
    } else if (Object.keys(this.state.holdIDImage).length === 0) {
      this.setState({
        isShowValidSuccess: true,
        validMessage: translate("select_photos")
      });
      setTimeout(() => {
        this.setState({
          isShowValidSuccess: false,
          validMessage: ""
        });
      }, 2000);
    } else {
      HttpRequest.uploadPicture("HOLD_ID", this.state.holdIDImage, id)
        .then(response => {
          console.log(
            "Check is photo uploaded or not in Second Condition ------>> ",
            response.data
          );
        })
        .catch(error => {
          this.setState({
            isLoading: false,
            isShowError: true,
            errorMessage: translate("issue_photo_upload")
          });
          setTimeout(() => {
            this.setState({
              isShowError: false
            });
          }, 2000);
          console.log("print error in second condition ----->> ", error);
        });
      this.props.navigation.navigate("Dashboard");
    }
  }

  onTakePhoto = () => {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    this.setState({ isModal: false });

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };
        this.onSelectImage(response);
      }
    });
  };

  onOpenGallery = () => {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    // this.setState({ imageFor: type , isModal: false});
    this.setState({ isModal: false });

    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };
        this.onSelectImage(response);
      }
    });
  };

  renderModal = () => {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.isModal}
        onRequestClose={() => console.log("Modal Closed")}
      >
        <View style={styles.verifyDialogStyle}>
          <View style={styles.verifyInfoBoxStyle}>
            <Text
              style={{
                fontSize: 17,
                fontFamily: FontStyle.Medium,
                padding: 10,
                textAlign: "center",
                paddingBottom: 25
              }}
            >
              Uplaod Photo
            </Text>
            <View
              style={{ width: "90%", height: 100, alignItems: "flex-start" }}
            >
              <TouchableOpacity
                style={{
                  width: "90%",
                  height: 40,
                  alignItems: "flex-start",
                  marginHorizontal: 10
                }}
                onPress={() => this.onTakePhoto()}
              >
                <Text
                  style={{
                    fontSize: 17,
                    fontFamily: FontStyle.Medium,
                    padding: 10,
                    textAlign: "center",
                    color: Config.primaryColor
                  }}
                >
                  Take Photo
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  width: "90%",
                  height: 40,
                  alignItems: "flex-start",
                  marginHorizontal: 10
                }}
                onPress={() => this.onOpenGallery()}
              >
                <Text
                  style={{
                    fontSize: 17,
                    fontFamily: FontStyle.Medium,
                    padding: 10,
                    textAlign: "center",
                    color: Config.primaryColor
                  }}
                >
                  Select from Gallery
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                width: "90%",
                height: 45,
                borderRadius: 3,
                alignItems: "center",
                justifyContent: "center",
                marginHorizontal: 10,
                backgroundColor: Config.primaryColor
              }}
              onPress={() => this.setState({ isModal: false })}
            >
              <Text
                style={{
                  fontSize: 17,
                  fontFamily: FontStyle.Medium,
                  padding: 10,
                  textAlign: "center",
                  color: "white"
                }}
              >
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const { holdID, imageID, isCollapsed } = this.state;
    return (
      <View style={styles.rootStyle}>
        <Header
          backgroundColor="#fff"
          placement="left"
          leftComponent={
            this.state.showNextScreen ? (
              <MaterialIcons
                size={20}
                onPress={this.goBackButton}
                name="arrow-back"
              />
            ) : null
          }
          centerComponent={{
            text: translate("identification"),
            style: { fontSize: 20, color: "grey", fontFamily: FontStyle.Bold }
          }}
          outerContainerStyles={{ height: 60 }}
        />

        <View
          style={{
            paddingHorizontal: 5,
            paddingVertical: 5,
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          {this.state.showNextScreen ? (
            <View style={styles.picInfoWrapper}>
              <Text style={styles.picInfoTitleStyle}>
                {translate("upload_holding_image")}
              </Text>
              <View style={styles.picHolderStyle}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({ imageFor: "HOLD_ID", isModal: true })
                  }
                  // onPress={() => this.launchCamera('HOLD_ID')}
                >
                  <Image
                    source={holdID}
                    resizeMode="contain"
                    style={{ width: 240, height: 150 }}
                  />
                </TouchableOpacity>
              </View>
              <Text style={{ textAlign: "center" }}>
                {translate("upload_photos")}
              </Text>
            </View>
          ) : (
            <View style={styles.picInfoWrapper}>
              <Text style={styles.picInfoTitleStyle}>
                {translate("upload_idcard_image")}
              </Text>
              <View style={styles.picHolderStyle}>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({ imageFor: "IMAGE_ID", isModal: true })
                  }
                  // onPress={() => this.launchCamera('IMAGE_ID')}
                >
                  <Image
                    source={imageID}
                    resizeMode="contain"
                    style={{ width: 240, height: 150 }}
                  />
                </TouchableOpacity>
              </View>
              <Text style={{ textAlign: "center" }}>
                {translate("upload_photos")}
              </Text>
            </View>
          )}

          <View style={styles.buttonWrapper}>
            <TouchableOpacity
              style={styles.submitButtonStyle}
              onPress={this.confirmUpload}
            >
              {this.state.isLoading == true && (
                <ActivityIndicator color="#36a563" />
              )}

              {this.state.isLoading == false && (
                <Text
                  style={{
                    fontSize: 18,
                    color: Config.primaryColor,
                    fontFamily: "Raleway-Bold"
                  }}
                >
                  {translate("submit")}
                </Text>
              )}
            </TouchableOpacity>
          </View>

          {this.state.successMessage && (
            <SuccessModal visible={this.state.successMessage} />
          )}

          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isShowError}
            onRequestClose={() => console.log("Modal Closed")}
          >
            <View style={styles.dialogStyle}>
              <View style={styles.dialogBoxStyle}>
                <Image
                  source={require("../../../images/login/login_error.png")}
                  style={{ height: 90 }}
                  resizeMode="contain"
                />
                <View style={{ height: 20 }} />
                <Text
                  style={{
                    fontFamily: FontStyle.Bold,
                    color: "#ff2057",
                    fontSize: 20,
                    textAlign: "center"
                  }}
                >
                  {this.state.errorMessage}
                </Text>
              </View>
            </View>
          </Modal>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isShowValidSuccess}
            onRequestClose={() => console.log("Modal Closed")}
          >
            <View style={styles.dialogStyle}>
              <View style={styles.dialogBoxStyle}>
                <Image
                  source={require("../../../images/login/login_error.png")}
                  style={{ height: 90 }}
                  resizeMode="contain"
                />
                <View style={{ height: 20 }} />
                <Text
                  style={{
                    fontFamily: FontStyle.Bold,
                    color: "#ff2057",
                    fontSize: 20,
                    textAlign: "center"
                  }}
                >
                  {this.state.validMessage}
                </Text>
              </View>
            </View>
          </Modal>
          {this.renderModal()}
        </View>
      </View>
    );
  }
}

const styles = {
  verifyDialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },
  verifyInfoBoxStyle: {
    width: "85%",
    height: 220,
    borderRadius: 10,
    backgroundColor: "#fff",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  rootStyle: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 10
  },
  navigationWrapper: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  backArrowStyle: {
    marginRight: 10
  },
  backArrowIconStyle: {
    fontFamily: FontStyle.Light,
    marginLeft: 10
  },

  navigationStyle: {
    height: 50,
    flexDirection: "row"
  },

  submitInfoStyle: {
    fontFamily: FontStyle.Light,
    textAlign: "center",
    justifyContent: "center",
    fontSize: 20
  },
  changeButtonStyle: {
    backgroundColor: Config.primaryColor,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  },
  submitButtonStyle: {
    backgroundColor: "#fff",
    height: 45,
    borderColor: Config.primaryColor,
    borderWidth: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
    marginTop: 5
  },
  picInfoWrapper: {
    //  backgroundColor: '#fff',
    flexDirection: "column",
    paddingVertical: 2,
    width: "90%",
    flex: 0.7,
    justifyContent: "flex-end",
    marginBottom: 2,
    elevation: 2,
    marginTop: 10
  },
  buttonWrapper: {
    backgroundColor: "#fff",
    flexDirection: "column",
    width: "90%",
    flex: 0.5,
    justifyContent: "flex-end"
  },
  picInfoTitleStyle: {
    fontFamily: FontStyle.Regular,
    fontSize: 16,
    color: "#808080",
    textAlign: "left"
  },
  picHolderStyle: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around"
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },
  dialogBoxStyle: {
    width: 250,
    height: 200,
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  }
};
function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData
  };
}
const uploadStart = dispatch =>
  dispatch({
    type: "UPLOAD_START"
  });

const uploadDone = dispatch =>
  dispatch({
    type: "UPLOAD_DONE"
  });

const uploadFailed = dispatch =>
  dispatch({
    type: "UPLOAD_FAILED"
  });

const userVerified = dispatch =>
  dispatch({
    type: "USER_VERIFIED_SUCCESS"
  });

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: "set_root",
        root
      }),
    uploadStart: () => uploadStart(dispatch),
    uploadDone: () => uploadDone(dispatch),
    userVerified: () => userVerified(dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Verify);
