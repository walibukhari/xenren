import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, ListView, TextInput, Alert, ScrollView, Image, TouchableOpacity } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Config from '../../Config';
import Submitted from './Submitted';
import Pregressing from './Pregressing';
import Completed from './Completed';
import FooterTabs from '../FooterTabs';
import { translate } from '../../i18n';
class Order extends Component {

    static navigationOptions = {
        header: null,
      };
    constructor(props) {
        super(props);

        this.state = {
            activeTabIndex: 0,
            activeTabName : 'Submited'
        };
    }

    switchScreen(screen) {
        this.setState({ screen });
    }

    renderButton(screen, flex, icon, label, iconWidth, iconHeight) {
        let color = '#95a5a6';
        if (this.state.screen == screen) {
            color = Config.primaryColor;
        }
        return (
            <TouchableOpacity style={[styles.itemButtonStyle, { flex: flex }]}
                onPress={() => { this.switchScreen(screen) }}>
                <Image source={icon}
                    style={{
                        height: iconHeight,
                        width: iconWidth,
                        resizeMode: 'contain',
                        tintColor: color
                    }} />
                <Text style={{ color: color, fontSize: 12 }}> {label}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        return (
           
            <View style={ styles.rootStyle }>
            {/* Back Button View */}
            <View style={styles.navigationStyle}>
                         <View style={styles.navigationWrapper}>
                             <TouchableOpacity onPress={this.goBack} style={{ marginLeft: 2, paddingVertical:5 }}>
                                 <SimpleLineIcons size={16} name="arrow-left" color='#57af2a' />
                             </TouchableOpacity>
                             <Text style={{color:'gray',fontSize: 20,}}>Recieved Order | <Text style={{color:'gray',fontSize: 14,}}>{this.state.activeTabName}</Text></Text>
                             <TouchableOpacity  style={{ marginRight: 8, flexDirection: 'row',paddingVertical:5 }} onPress={this.updateProfile}>
                                 <SimpleLineIcons size={16} name="magnifier" color='gray' />
                             </TouchableOpacity>
                         </View>
                     </View>
                     {/* Profile Image View */}
             <View style={styles.topButtonWrapperStyle}>
                 <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 0,activeTabName : 'Submited' }) }}
                     style={[styles.topButtonStyle, this.state.activeTabIndex == 0 ? styles.topButtonActiveStyle : {}]}>
                     <Text style={this.state.activeTabIndex == 0 ? styles.topTextActiveStyle : styles.topTextStyle}>Submited</Text>
                 </TouchableOpacity>
                
                 <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 1,activeTabName : 'Pregressing' }) }}
                     style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
                     <Text style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : styles.topTextStyle}>Pregressing</Text>
                 </TouchableOpacity>
                
                 <TouchableOpacity onPress={() => { this.setState({ activeTabIndex: 2,activeTabName : 'Completed' }) }}
                     style={[styles.topButtonStyle, this.state.activeTabIndex == 2 ? styles.topButtonActiveStyle : {}]}>
                     <Text style={this.state.activeTabIndex == 2 ? styles.topTextActiveStyle : styles.topTextStyle}>Completed</Text>
                 </TouchableOpacity>
             </View> 
             {this.state.activeTabIndex == 0 && <Submitted />}
             {this.state.activeTabIndex == 1 && <Pregressing />}
             {this.state.activeTabIndex == 2 && <Completed />}
             <FooterTabs navigation={this.props.navigation} currentIndex={3} />
           </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'column',
        paddingTop:20
        
      },
      topButtonWrapperStyle: {
        flexDirection: 'row',
        borderTopWidth: 2,
        borderTopColor: '#ecf0f1',
       
      },
      topButtonStyle: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        
      },
      topButtonActiveStyle: {
        borderBottomColor: Config.primaryColor,
        borderBottomWidth: 2,
      },
      topTextStyle : {fontSize:16,color:'gray'},
      topTextActiveStyle: {
        color: Config.primaryColor,
        fontSize:16,
        
      },
      navigationStyle: {
        height: 50,
        flexDirection: 'row',
        paddingVertical : 10,
    
       
    },
    navigationWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
      
    },
    backArrowStyle: {
      marginRight: 10,
    },
    backArrowIconStyle: {
      fontFamily: 'Montserrat-Light',
      marginLeft: 10
    }
};

function mapStateToProps(state) {
    return {
        component: state.component,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setRoot: (root) => dispatch({
            type: 'set_root',
            root: root
        })
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Order);