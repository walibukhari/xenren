import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, ListView, Image, TouchableOpacity } from 'react-native';
import Config from '../../Config';

class Submitted extends Component {
  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(this.getData()),
    };

    this.root = this.props.component.root;
    this.navigation = this.root.props;
  }

  getData() {
    return [
      {
        image: require('../../../images/notification/icon_list_message.png'),
        title: 'Pinterest standard Graphic Designer Needed :3 ~ 20 USD per Hour ',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_contract.png'),
        title: 'Web Desingers and Developers (1 & 2)',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_payment.png'),
        title: 'I need a 7 page website to support my book launch.',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_message.png'),
        title: 'Pinterest standard Graphic Designer Needed :3 ~ 20 USD per Hour ',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_contract.png'),
        title: 'Web Desingers and Developers (1 & 2)',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_payment.png'),
        title: 'I need a 7 page website to support my book launch.',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_message.png'),
        title: 'Pinterest standard Graphic Designer Needed :3 ~ 20 USD per Hour ',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_contract.png'),
        title: 'Web Desingers and Developers (1 & 2)',
        subtitle: 'Nov 12,2018',
      },
      {
        image: require('../../../images/notification/icon_list_payment.png'),
        title: 'I need a 7 page website to support my book launch.',
        subtitle: 'Nov 12,2018',
      },
    ];
  }

  renderRow(row) {
    return (
      <TouchableOpacity style={styles.rowStyle}>
       
        <View style={styles.informationContainerStyle}>
          <Text style={{ fontSize: 14, color: '#5c6364', marginBottom: 5 }}
            ellipsizeMode='tail' numberOfLines={2}>{row.title}</Text>
          <Text style={{ fontSize: 11, color: '#95a5a6' }}>Hired By Adam</Text>
          <Text style={{ fontSize: 11, color: '#95a5a6',textAlign:'right' }}>{row.subtitle}| Posted 3 days ago</Text>
        </View>
        <View style={styles.imageContainerStyle}>
          {/* <Image source={row.image} style={{
            height: 40,
width: 40,
resizeMode: 'contain',
            tintColor: Config.primaryColor,
          }} /> */}
<Text style={{fontSize:11,color:'gray'}}>Fixed-Price</Text>
<Text style={{fontSize:14,color:'#5c6364'}}>$ 120</Text>
<Text style={{fontSize:11,color:'gray'}}>Project-Type</Text>
<Text style={{fontSize:15,color:'#5c6364',}}>Public</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => this.renderRow(rowData)}
        />
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  rowStyle: {
    height: 80,
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ecf0f1',
  },
  imageContainerStyle: {
      flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
    // width: 60,
    // height: 60,
    borderLeftWidth: 1,
    borderLeftColor: Config.primaryColor,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  informationContainerStyle: {
    flex: 1,
    flexDirection: 'column',
    height: 70,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Submitted);
