import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Modal } from "react-native";
import Config from "../../Config";
import { translate } from "../../i18n";
import { Rating } from "react-native-elements";
import Gallery from "react-native-image-gallery";
import Url from "../../constants/BaseUrl";
import FontStyle from "../../constants/FontStyle";

class Review extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: this.props.review,
      hasAttachments: false,
      showPic: false,
      datas: []
    };
  }

  componentDidMount() {
    if (this.state.item.hasOwnProperty("attachments")) {
      this.setState({ hasAttachments: true });
    }
  }

  renderSkill(skills) {
    return skills.map((item, index) => {
      return (
        <View
          key={index}
          style={{
            borderRadius: 15,
            borderColor: "#f2f2f2",
            borderWidth: 1,
            flexDirection: "row",
            alignItems: "center",
            marginRight: 5,
            padding: 3
          }}
        >
          <Text
            style={{ paddingHorizontal: 10, backgroundColor: "transparent" }}
          >
            {item.skill.name_en}
          </Text>
        </View>
      );
    });
  }

  showImage = item => {
    let imagesArr = [];
    // portfolios.map((item) => {
    imagesArr.push({ source: { uri: `${Url.baseUrl}/${item.path}` } });
    // })
    this.setState({ datas: imagesArr, showPic: true });
  };

  renderAttachments(attachments) {
    return attachments.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={{
            borderColor: "#f1f1f1",
            borderWidth: 1,
            flexDirection: "row",
            alignItems: "center",
            marginRight: 10
          }}
          onPress={() => this.showImage(item)}
        >
          <Image
            source={{ uri: `${Url.baseUrl}/${item.path}` }}
            style={styles.imageAttachment}
          />
        </TouchableOpacity>
      );
    });
  }

  render() {
    const { item, hasAttachments } = this.state;
    // console.log(`www.xenren.co/${item.staff.img_avatar}`);
    return (
      <View style={styles.rootStyle}>
        <View style={{ marginHorizontal: 10, marginVertical: 5 }}>
          <View style={styles.boxInsideStyle}>
            <View style={styles.topSideStyle}>
              <Image
                source={{ uri: `${Url.baseUrl}/${item.staff.img_avatar}` }}
                style={styles.avatarStyle}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 20,
                  alignItems: "flex-start"
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={styles.text1Style}
                  >
                    {item.staff.name}
                  </Text>
                </View>
                <Text style={styles.text3Style}>
                  {translate("project_id")} {item.official_project_id || ""}
                </Text>
                <Text style={styles.text2Style}>{item.updated_at}</Text>
                <Rating
                  type="star"
                  startingValue={3}
                  readonly
                  imageSize={15}
                  style={{ alignContent: "flex-start" }}
                />
              </View>
            </View>
            <View style={styles.middleSideStyle}>
              <Text style={styles.textDescription}>{item.comment}</Text>
            </View>
            <View style={{ marginTop: 5 }}>
              <Text style={styles.textHeader}>{translate("skill_tag")}</Text>
              <View style={styles.middleSideStyle}>
                {this.renderSkill(item.skills)}
              </View>
            </View>
            {item.files.length > 0 && (
              <View style={{ marginTop: 5 }}>
                <Text style={styles.textHeader}>{translate("attachment")}</Text>
                <View style={styles.middleSideStyle}>
                  {this.renderAttachments(item.files)}
                </View>
              </View>
            )}
          </View>
        </View>

        {/* For Images */}
        <Modal
          transparent={true}
          supportedOrientations={["portrait", "landscape"]}
          visible={this.state.showPic}
          onRequestClose={() => console.log("")}
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#2C2C2C",
              alignItems: "flex-end"
            }}
          >
            <TouchableOpacity onPress={() => this.setState({ showPic: false })}>
              <Image
                source={require("../../../images/account_profile/close.png")}
                style={{
                  width: 30,
                  height: 30,
                  marginTop: 10,
                  marginRight: 10
                }}
              />
            </TouchableOpacity>

            {/* For Swiping Images */}
            <Gallery
              style={{ flex: 1, backgroundColor: "transparent" }}
              images={this.state.datas}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: "#fff",
    paddingHorizontal: 5,
    paddingVertical: 10,
    marginBottom: 3
  },
  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff"
  },
  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10,
    marginBottom: 10
  },
  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },
  avatarStyle: {
    width: 70,
    height: 70,
    borderRadius: 35
  },
  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  cardButtonTextStyle: {
    color: Config.textSecondaryColor,
    fontSize: 12,
    fontFamily: FontStyle.Regular
  },

  text1Style: {
    flex: 1,
    fontSize: 16,
    color: "#727272",
    fontFamily: FontStyle.Regular
  },
  text2Style: {
    fontSize: 11,
    color: "#bdc3c7",
    fontFamily: FontStyle.Regular
  },
  text3Style: {
    fontSize: 12,
    fontFamily: FontStyle.Regular
  },

  starStyle: {
    flexDirection: "row"
  },
  textDescription: {
    fontSize: 15,
    color: "grey",
    lineHeight: 35,
    fontFamily: FontStyle.Light
  },
  textHeader: {
    fontFamily: FontStyle.Bold,
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    marginTop: 15
  },
  imageAttachment: {
    width: 60,
    height: 60
  }
};

export default Review;
