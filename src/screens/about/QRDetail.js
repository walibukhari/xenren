import React, {Component} from 'react';
import { View, ScrollView, TouchableOpacity, Text, Image, StyleSheet , TextInput} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

export default class QRDetail extends Component {

      static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            title: 'QR Detail',
            headerTintColor: '#57af2a',
            headerTitleStyle: { color: '#57af2a', fontWeight: 'bold', textAlign:'center', alignSelf:'center', marginRight:40 },
            headerLeft: <MaterialIcons style={{marginLeft:15,color:'#57af2a'}}
            size={20}

            onPress={ () => navigation.goBack()}
            name="arrow-back"
          />
        };
    };
    constructor(props){
        super(props);

    }

    render() {
        return (
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <ScrollView style={styles.rootStyle} contentContainerStyle={{justifyContent:'center', alignItems:'center'}}
            keyboardShouldPersistTaps='always' horizontal={false} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>

                    {/* Profile Image View */} 

                    <View style={styles.photoWrapperStyle}>
                        <Image source={require('../../../images/dummy/user_1.png')}
                            style={{ width: 100, height: 100, borderRadius: 50 }} />
                        <Text style={styles.userNameStyle}>Adam John</Text>
                    </View>

                    <View style={{width:'100%', alignItems:'center'}}>
                        <View style={{width: '100%', justifyContent:'flex-end', alignItems:'center',height:120 , backgroundColor:'#E0E0E0', marginTop:60}}>
                            <Text style={{textAlign:'center', fontSize: 15, color:'#3F3F3F'}}>LEGENDS</Text>
                            <Text style={{textAlign:'center', fontSize: 13, color: ''}}>Product Designer</Text>
                            <Text style={{textAlign:'center', fontSize: 12,paddingBottom:5}}>Designer</Text>
                        </View>

                        <TouchableOpacity style={{position:'absolute',bottom:80}}>
                            <Image style={{height:80,width:80}} source={require('../../../images/dummy/user_1.png')} />
                        </TouchableOpacity>
                    </View>
                        
                    <View style={{margin:5,justifyContent:'center',alignItems:'center'}}>
                        <Text style={{fontWeight:'700', fontSize:16, textAlign:'center', color:'#2E2E2E'}}>
                            Scan to Check Profile Details
                        </Text>
                    </View>

                </View>

                
            </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rootStyle: {
        flexDirection: 'column',
        width:'95%',
        backgroundColor: "#f7f7f7",
        marginTop: 10,
        marginBottom: 10
      },
      container: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: 'center',
        backgroundColor: "#f7f7f7",
        width: '100%',
      },
      photoWrapperStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        marginTop:5
      },
      userNameStyle: {
          fontSize:16,
          textAlign: 'center',
          marginTop:10
      },
})
