import React, { Component } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
import Config from '../../Config';
import _ from 'lodash';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Review from './Review';
import Portifolio from '../jobs/portifolio';
import FontStyle from '../../constants/FontStyle';
import { translate } from '../../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

export default class About extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('personal_info'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: 1,
      star: '4.0',
      skills: [
        {
          id: 'PHP',
          level: 3,
        },
        {
          id: 'HTML',
          level: 3,
        },
        {
          id: 'Javascript',
          level: 2,
        },
        {
          id: 'Angular 5',
          level: 1,
        },
        {
          id: 'React',
          level: 2,
        },
        {
          id: 'MERN',
          level: 1,
        },
        {
          id: 'Node',
          level: 2,
        },
        {
          id: 'Desining',
          level: 3,
        },
        {
          id: 'SEO',
          level: 2,
        },
      ],
      levels: [
        {
          level: 3,
          icon: require('../../../images/share_office/icon_crown.png'),
          backgroundColor: '#f1c40f',
          textColor: '#fff',
        },
        {
          level: 2,
          icon: require('../../../images/share_office/icon_check.png'),
          backgroundColor: Config.primaryColor,
          textColor: '#fff',
        },
        {
          level: 1,
          icon: require('../../../images/share_office/icon_check.png'),
          backgroundColor: '#ecf0f1',
          textColor: '#000',
        },
      ],
      languages: [
        {
          flag: '',
          lang: 'English',
        },
        {
          flag: '',
          lang: 'German',
        },
        {
          flag: '',
          lang: 'French',
        },
        {
          flag: '',
          lang: 'Spanish',
        },
      ]

    };
  }

  renderSkill(skills) {
    return skills.map((item) => {
      let skill = _.find(this.state.levels, { level: item.level });
      // console.log('Skill', skill);
      if (skill == null) {
        return null;
      } else {
        return (
          <View 
          key={item.id}
          style={{
            backgroundColor: skill.backgroundColor,
            borderRadius: 15,
            borderColor: '#fff',
            borderWidth: 1,
            flexDirection: 'row',
            alignItems: 'center',
            marginRight: 5,
            padding: 3,
          }}>
            <View style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderColor: '#fff',
              borderWidth: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <Image style={{
                height: 13,
                width: 13,
                resizeMode: 'contain',
                tintColor: skill.textColor,
              }} source={skill.icon}/>
            </View>
            <Text style={{
              paddingHorizontal: 10,
              color: skill.textColor,
              backgroundColor: 'transparent',
            }}>{item.id}</Text>
          </View>
        );
      }
    });
  }

  renderLanguages(languages) {
    return languages.map((item) => {
      if (item.lang == null) {
        return null;
      } else {
        return (
          <View
          key={item.id}
           style={{
            backgroundColor: 'transparent',
            flexDirection: 'row',
            alignItems: 'center',
            marginRight: 5,
            padding: 3,
          }}>
            {/* <Image style={{ height: 20, width: 24, resizeMode: 'contain' }} source={item.flag} /> */}
            <Text style={{
              paddingHorizontal: 10,
              color: 'gray',
              backgroundColor: 'transparent',
            }}>{item.lang}</Text>
          </View>
        );
      }
    });
  }

  getQRDetails() {
    this.props.navigation.navigate('QRDetail');
  }

  Contact() {
    this.props.navigation.navigate('ChatSingle');
  }


  renderStar(stars) {
    let array = [];
    for (let i = 1; i <= 5; i++) {
      if (i <= Number(stars)) {
        array.push(<Ionicons key={'star_' + i} name='md-star' color='#f1c40f' size={15}/>);
      } else {
        array.push(<Ionicons key={'star_' + i} name='md-star' color='#7f8c8d' size={15}/>);
      }
    }
    return (<View style={styles.starStyle}>{array}</View>);
  }

  render() {
    return (
      <ScrollView
        style={styles.rootStyle} contentContainerStyle={{
        justifyContent: 'center',
        alignItems: 'center',
      }}
        keyboardShouldPersistTaps='always' horizontal={false}
        showsVerticalScrollIndicator={false}>
        <View style={styles.container}>

          {/* Profile Image View */}

          <View style={styles.photoWrapperStyle}>
            <Image source={require('../../../images/dummy/user_1.png')}
                   style={{
                     width: 100,
                     height: 100,
                     borderRadius: 50,
                   }}/>
            <Text style={styles.userNameStyle}>Adam John</Text>
            {/* Profile Name Below Image */}

            <TouchableOpacity style={styles.contactStyle} onPress={() => this.Contact()}>
              <Text style={{
                color: '#57af2a',
                fontSize: 15,
                fontFamily: FontStyle.Regular,
              }}>Contact</Text>
            </TouchableOpacity>
          </View>

          {/* QR View*/}

          <View style={styles.qrStyle}>
            <View style={{ marginLeft: 10 }}>
              <Text style={{
                fontSize: 14,
                fontFamily: FontStyle.Medium,
              }}>LEGENDS</Text>
              <Text style={{
                fontSize: 12,
                color: '#57af2a',
                fontFamily: FontStyle.Regular,
              }}>Product Designer</Text>
              <Text style={{
                fontSize: 10,
                color: 'gray',
                fontFamily: FontStyle.Regular,
              }}>Designer</Text>
            </View>

            <TouchableOpacity style={{
              width: 50,
              height: 50,
              marginRight: 5,
            }} onPress={() => this.getQRDetails()}>
              <Image source={require('../../../images/dummy/user_1.png')}
                     style={{
                       width: 50,
                       height: 50,
                     }}/>
              <Text style={{
                color: '#57af2a',
                fontSize: 15,
                fontFamily: FontStyle.Medium,
              }}>Contact</Text>
            </TouchableOpacity>
          </View>

          {/* Info */}

          <View style={styles.infoStyle}>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginLeft: 10,
              marginTop: 10,
            }}>
              <Text style={{
                fontSize: 11,
                width: 100,
              }}>Status:</Text>
              <Text style={{
                fontSize: 12,
                color: '#57af2a',
              }}>Looking for Project:</Text>
            </View>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginLeft: 10,
              marginTop: 10,
            }}>
              <Text style={{
                fontSize: 11,
                width: 100,
              }}>Rate:</Text>
              <Text style={{
                fontSize: 12,
                color: '#57af2a',
              }}>$10.000 /hr</Text>
            </View>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginLeft: 10,
              marginTop: 10,
            }}>
              <Text style={{
                fontSize: 11,
                width: 100,
              }}>Age:</Text>
              <Text style={{
                fontSize: 12,
                color: '#57af2a',
              }}>36</Text>
            </View>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              marginLeft: 10,
              marginTop: 10,
            }}>
              <Text style={{
                fontSize: 11,
                width: 100,
              }}>Experience</Text>
              <Text style={{
                fontSize: 12,
                color: '#57af2a',
              }}>3 years</Text>
            </View>

            {/* Info section */}

            <View>
              {/* <Image style={{width:30,height:3000}} source={require('')}/> */}
              <Text>Info</Text>
            </View>
            <Text>
              Contrary to popular belief, Lorem Ipsum is simply dummy text of the printing and
              typesetting industry. Lorem Ipsum has roots in a piece of classical Latin Literature
              from 45 BC, making it over
            </Text>

            {/* Skills section */}

            <View>
              {/* <Image style={{width:30,height:3000}} source={require('')}/> */}
              <Text>Skills</Text>
            </View>

            <View style={styles.middleSideStyle}>
              {this.renderSkill(this.state.skills)}
            </View>

            {/* Languages */}
            <View>
              {/* <Image style={{width:30,height:3000}} source={require('')}/> */}
              <Text>Languages</Text>
            </View>

            <View style={styles.languageStyle}>
              {this.renderLanguages(this.state.languages)}
            </View>

            {/* Ratings */}

            <View>
              {/* <Image style={{width:30,height:3000}} source={require('')}/> */}
              <Text>Ratings</Text>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{
                flexDirection: 'column',
                width: '40%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
                <Text style={{
                  fontSize: 35,
                  fontFamily: '900',
                  color: '#57af2a',
                }}>
                  {this.state.star}
                </Text>
                {this.renderStar(this.state.star)}
              </View>
              {/* show Rating Bars */}
              <View style={{
                flexDirection: 'column',
                width: '60%',
              }}>

              </View>

            </View>

          </View>

          {/* Tabs */}

          <View style={styles.topButtonWrapperStyle}>
            <View style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}/>
            <TouchableOpacity onPress={() => {
              this.setState({ activeTabIndex: 1 });
            }}
                              style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
              <Text style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {}}>Platform
                Review</Text>
            </TouchableOpacity>
            <View style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}/>
            <TouchableOpacity onPress={() => {
              this.setState({ activeTabIndex: 2 });
            }}
                              style={[styles.topButtonStyle, this.state.activeTabIndex == 2 ? styles.topButtonActiveStyle : {}]}>
              <Text
                style={this.state.activeTabIndex == 2 ? styles.topTextActiveStyle : {}}>Review</Text>
            </TouchableOpacity>
            <View style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}/>
            <TouchableOpacity onPress={() => {
              this.setState({ activeTabIndex: 3 });
            }}
                              style={[styles.topButtonStyle, this.state.activeTabIndex == 3 ? styles.topButtonActiveStyle : {}]}>
              <Text
                style={this.state.activeTabIndex == 3 ? styles.topTextActiveStyle : {}}>Portifolio</Text>
            </TouchableOpacity>
          </View>
          {this.state.activeTabIndex == 1 && <Portifolio navigation={this.props.navigation}/>}
          {this.state.activeTabIndex == 2 && <Review/>}
          {this.state.activeTabIndex == 3 && <Portifolio navigation={this.props.navigation}/>}

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    width: '95%',
    marginTop: 10,
  },

  photoWrapperStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 5,
  },

  userNameStyle: {
    fontSize: 16,
    textAlign: 'center',
  },

  contactStyle: {
    borderColor: '#57af2a',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    width: 150,
    margin: 5,
  },

  qrStyle: {
    width: '95%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#E0E0E0',
  },

  infoStyle: {
    width: '100%',
    justifyContent: 'center',
  },

  middleSideStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 10,
  },

  languageStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 5,
  },

  starStyle: {
    flexDirection: 'row',
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },
});
