/* eslint-disable no-undef */
/* eslint-disable global-require */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  Dimensions,
} from 'react-native';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';

const { width, height } = Dimensions.get('window');

class TimerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  componentDidMount() {
    this.setState({ showModal: this.props.value });
  }

  stopTimer = () => {
    this.props.timerCallback('yes');
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.showModal}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View
            style={{
              width,
              height,
              flexDirection: 'column',
              backgroundColor: 'rgba(44, 62, 80, 0.6)',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                alignSelf: 'center',
                width: '85%',
                height: 400,
                backgroundColor: 'transparent',
                elevation: 20,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}
            >
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 5,
                  height: 300,
                  width: '100%',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    width: '85%',
                    height: 60,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginVertical: 15,
                  }}
                >
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: '49%',
                    }}
                  >
                    <Text
                      style={{
                        color: '#1F1F1F',
                        fontSize: 19,
                        fontFamily: FontStyle.Medium,
                      }}
                    >
                      THE COST
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text
                        style={{
                          color: '#1F1F1F',
                          fontSize: 25,
                          fontFamily: FontStyle.Bold,
                        }}
                      >
                        {this.props.totalAmount}{' '}
                      </Text>
                      <View
                        style={{
                          width: 22,
                          height: 22,
                          borderRadius: 11,
                          borderWidth: 3,
                          borderColor: 'black',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <Text
                          style={{
                            color: '#1F1F1F',
                            fontSize: 15,
                            fontFamily: FontStyle.Bold,
                          }}
                        >
                          {this.props.currency}
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{ width: 1, height: 60, backgroundColor: '#C5C5C5' }}
                  />

                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: '49%',
                    }}
                  >
                    <Text
                      style={{
                        color: '#1F1F1F',
                        fontSize: 19,
                        fontFamily: FontStyle.Medium,
                      }}
                    >
                      TIME SPENT
                    </Text>
                    <Text
                      style={{
                        color: '#1F1F1F',
                        fontSize: 25,
                        fontFamily: FontStyle.Bold,
                      }}
                      numberOfLines={1}
                    >
                      {this.props.totalTime}{' '}
                      <Text
                        style={{
                          color: '#1F1F1F',
                          fontSize: 23,
                          fontFamily: FontStyle.Bold,
                        }}
                        numberOfLines={1}
                      >
                        {/*{this.props.timeType === true ? 'HRS' : 'MIN'}*/}
                      </Text>
                    </Text>
                  </View>
                </View>

                <TouchableOpacity
                  onPress={() => this.stopTimer()}
                  style={{
                    width: '90%',
                    height: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 3,
                    borderColor: Config.primaryColor,
                    borderWidth: 1,
                    marginTop: 10,
                  }}
                >
                  <Text
                    style={{
                      color: Config.primaryColor,
                      fontSize: 20,
                      fontFamily: FontStyle.Medium,
                    }}
                  >
                    CONFIRM
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.props.timerCallback('no')}
                  style={{
                    width: '90%',
                    height: 45,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 3,
                    backgroundColor: Config.primaryColor,
                    marginVertical: 10,
                  }}
                >
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 20,
                      fontFamily: FontStyle.Medium,
                    }}
                  >
                    CANCEL
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: 140,
                  height: 140,
                  borderRadius: 70,
                  position: 'absolute',
                  top: 0,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <View
                  style={{
                    width: 120,
                    height: 120,
                    borderRadius: 60,
                    backgroundColor: Config.primaryColor,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Image
                    style={{ width: 60, height: 70 }}
                    resizeMode="contain"
                    source={require('../../../images/select_seat/chair.png')}
                  />
                  <View
                    style={{
                      position: 'absolute',
                      top: 27,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text
                      style={{
                        textAlign: 'center',
                        color: 'white',
                        fontSize: 12,
                        fontFamily: FontStyle.Medium,
                        width: 35,
                        paddingVertical: 8,
                      }}
                      ellipsizeMode="tail"
                      numberOfLines={1}
                    >
                      {this.props.seat}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const officeId = state.sharedOffice.get('selected_office');
  return {
    officeId,
    seatId: state.sharedOffice.get('selected_seat'),
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
    stopTimer: id => dispatch(actions.sharedOfficeStopTimer(id)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimerComponent);
