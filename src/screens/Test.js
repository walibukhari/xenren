import React from 'react'
import {
    Text,
    View,
    Image,
} from 'react-native'

import GlowEffectComponent from '../screens/IndoorChat/GlowEffectComponent'
import Config from '../Config';


// const HeaderComponent = (props) => { 
//     // https://github.com/879479119/react-native-shadow
//         APPBAR_HEIGHT = Platform.select({
//             ios: 44,
//             android: 40,
//             default: 64,
//         });
//     const shadowOpt = {
//             width: 160,
//             height: APPBAR_HEIGHT,
//             color: Config.primaryColor,
//             border: 5,
//             radius: 20,
//             opacity: 0.2,
//             x: 0,
//             y: 0,
//             style: { marginVertical: 5 }
//         }
//         return (<BoxShadow setting={shadowOpt} color>
//             <TouchableOpacity style={{
//                 position: "relative",
//                 width: 160,
//                 height: APPBAR_HEIGHT,
//                 backgroundColor: "#fff",
//                 borderRadius: 50,
//                 overflow: "hidden"
//             }}>
//                 <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly'}}>
//                   {props.children}
//                 </View>
//             </TouchableOpacity>
//         </BoxShadow>)
// }

export default class Test extends React.Component { 
    static navigationOptions = {
        headerStyle: {
            justifyContent: 'center',
            alignItems: 'center'
        },
        headerTitle: <GlowEffectComponent>
            <Text style={{ alignSelf: 'center' }}>Silicon Valley</Text>
            <Image source={require('../../images/other/icon_location.png')} style={{
                marginLeft: 3,
                tintColor: Config.primaryColor,
                width: 22,
                height: 22,
                paddingRight: 5,
            }} />
        </GlowEffectComponent>
    }
    render() { 
        

		return (
            <View>
                <Text>Testing</Text>
            </View>
        )
    }
}