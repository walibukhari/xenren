/* eslint-disable global-require */
/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Dimensions,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    Image,
    Platform,
    Alert,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    LoginManager,
    AccessToken,
    GraphRequest,
    GraphRequestManager,
} from 'react-native-fbsdk';
import Pushy from 'pushy-react-native';
import FontStyle from '../constants/FontStyle';
import LocalData from '../components/LocalData';
import { translate } from '../i18n';
import Config from '../Config';
import HttpRequest from '../components/HttpRequest';
import validate from '../utilities/Validations';
import VerificationTopup from './verifyId/VerificationTopup';

const { width } = Dimensions.get('window');

class Login extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            password: '',
            nameFocus: false,
            error: [],
            emailFocus: false,
            passwordFocus: false,
            isLoading: false,
            isFbLoading: false,
            isShowSuccess: false,
            isShowError: false,
            successMessage: 'Login Successfully',
            errorMessage: 'Error',
        };
        this.root = this.props.component.root;
    }

    componentDidMount() {}

    render() {
        let text ;
        if(Platform.OS === 'android') {
            text = 'Please upgrade your apk in google play store';
        } else {
            text = 'Please upgrade your apk in apple app store';
        }
        return (
            <View style={styles.rootStyle}>
                <View style={{width: '100%', justifyContent:'center', textAlign:'center', height: 100}} >
                    <Text
                        style={styles.upgradeScreen}
                    >
                        {text}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    upgradeScreen: {
        fontSize:15,
        fontWeight:'bold',
        textAlign: 'center',
        width:'100%',
        color:Config.primaryColor,
    },

    headerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
    },

    headerLineStyle: {
        width: 20,
        height: 2,
        marginHorizontal: 5,
        backgroundColor: Config.primaryColor,
    },

    headerTextStyle: {
        color: Config.textColor,
        fontSize: 35,
        fontWeight: 'bold',
    },

    formInputStyle: {
        flexDirection: 'column',
        alignItems: 'center',
        width: '75%',
    },

    inputWrapperStyle: {
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: Config.textColor,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        marginVertical: 10,
    },

    inputStyle: {
        flex: 1,
        height: 40,
        marginLeft: 10,
    },

    signInButtonStyle: {
        backgroundColor: Config.primaryColor,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },

    signInTextStyle: {
        color: '#ffffff',
        fontSize: 15,
    },

    socialButtonContainerStyle: {
        flexDirection: 'row',
    },

    socialButtonStyle: {
        height: 60,
        width: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Config.textColor,
        margin: 5,
    },

    forgotPasswordWrapperStyle: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    forgotPasswordButtonStyle: {},

    forgotPasswordTextStyle: {
        fontSize: 15,
        fontFamily: FontStyle.Medium,
        color: Config.textColor,
    },

    signUpWrapperStyle: {
        borderBottomWidth: 1,
        borderBottomColor: Config.textColor,
        padding: 10,
        flexDirection: 'row',
    },

    signUpButtonStyle: {
        fontWeight: 'bold',
        color: Config.primaryColor,
    },

    footerStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        margin: 12,
    },

    footerButtonStyle: {
        fontWeight: 'bold',
        color: Config.textColor,
        textAlign: 'center',
    },

    dialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },

    dialogBoxStyle: {
        width: 240,
        height: 220,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
};

const mapStateToProps = (state) => ({
    component: state.component,
});

const mapDispatchToProps = (dispatch) => ({
    setRoot: (root) => dispatch({
        type: 'set_root',
        root,
    }),
    userLogin: (userData, token) => dispatch({
        type: 'LOGIN_SUCCESS',
        data: {
            ...userData,
            token,
        },
    }),
    sharedOffice: (sharedOffice) => dispatch({
        type: 'SHARED_OFFICE',
        data: sharedOffice,
    }),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Login);
