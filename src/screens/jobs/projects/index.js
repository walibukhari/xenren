/* eslint-disable no-mixed-spaces-and-tabs */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, FlatList } from 'react-native';
import { SearchBar } from 'react-native-elements';
import Config from '../../../Config';
import Review from '../subs/Review';
import { translate } from '../../../i18n';
import HttpRequest from '../../../components/HttpRequest';

class Projects extends Component {
  constructor(props) {
    super(props);

    this.state = {
      commentData: [],
      activeTabIndex: 0,
      query: '',
      filteredList: [],
      isLoading: true,
      access_token: '',
      search: '',
      projectType: null,
      skill_ids: '',
      filter_string: '',
      skillList: '',
      // filterOptions: ''
    };
  }

  componentDidMount() {
    const arr = [];
    this.props.skills.map((item) => {
      arr.push(item.value);
    });
    this.setState(
      {
        skillList: arr.toString(),
      },
      () => {
        this.getJobsList(`skill_id_list=${this.state.skillList}`);
      },
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ isLoading: true, listLength: '' });
    if (nextProps.filterString !== '') {
      this.getJobsList(nextProps.filterString);
    }
  }

	getJobsList = (filterStr) => {
	  this.setState({ isRefreshing: true });
	  HttpRequest.searchJobs('1', filterStr)
	    .then((response) => {
		    if (response.data.data.commonProjects.length === 0) {
	        alert(translate('no_job_posts'));
	        this.setState({
	          listLength: response.data.data.commonProjects.length,
	          isLoading: false,
	          isRefreshing: false,
	        });
	      } else {
			let arr = [];
			let arr1 = [];
			response.data.data.commonProjects.map((item) => {
				if (item.thumb_down !== null) {
					item.down = true;
					arr.push(item);
				} else {
					item.down = false;
					arr.push(item);
				}
			});
			arr.map((item) => {
				if (item.favorite_job !== null) {
					item.fav = true;
					arr1.push(item);
				} else {
					item.fav = false;
					arr1.push(item);
				}
			});
	        this.setState({
	          commentData: response.data.data.commonProjects,
	          filteredList: arr1,
	          listLength: response.data.data.commonProjects.length,
	          projectType: response.data.data.projectType,
	          isLoading: false,
	          isRefreshing: false,
	        });
	      }
	    })
	    .catch((error) => {
	      this.setState({
	        isLoading: false,
	        isRefreshing: false,
	      });
	      alert(translate('network_error'));
	    });
	};


	callbackFromReview = (job) => {
	  const temp = this.state.commentData;
	  temp[job.index] = job.item;
	  this.setState({ commentData: temp });
	};

	onRefresh() {
	  this.setState({ isLoading: true });
	  this.getJobsList();
	}

	handleSearchInput = (e) => {
	  let text = e.toLowerCase();
	  let fullList = this.state.commentData;
	  let filteredList = fullList.filter((item) => {
	    if (item.name.toLowerCase()
	      .match(text)) {
	      return item;
	    }
	  });

	  if (!text || text === '') {
	    this.setState({
	      filteredList: fullList,
	      listLength: fullList.length,
	      search: '',
	    });
	  } else if (!filteredList.length) {
	    this.setState({
	      filteredList,
	      listLength: filteredList.length,
	      search: text,
	    });
	  } else if (Array.isArray(filteredList)) {
	    this.setState({
	      filteredList: filteredList,
	      listLength: filteredList.length,
	      search: text,
	    });
	  }
	};

	renderRow = (review) => {
	  if (this.state.isLoading) {
	    return (<View />);
	  }
	  return (
        <Review review={review} projectType={this.state.projectType}
                callbackToParent={this.callbackFromReview}
                navigation={this.props.navigation
				        }/>
	  );
	};

	render() {
	  return (
      <View style={styles.rootStyle}>
        <SearchBar darkTheme
                   barStyle="default"
				   autoCorrect={false}
                   searchBarStyle="minimal"
                   placeholder={translate('search')}
                   searchIcon={{ size: 24 }}
                   icon={{
					           type: 'font-awesome',
					           name: 'search',
					           marginTop: 15,
					           marginBottom: 15,
				           }}
                   cancelIcon={false}
                   containerStyle={{
					           backgroundColor: '#f7f7f7',
					           width: '100%',
					           flexDirection: 'row-reverse',
					           alignContent: 'space-between',
					           justifyContent: 'space-between',
					           borderTopWidth: 0,
					           borderBottomWidth: 0,
				           }}
                   inputContainerStyle={{
					           backgroundColor: '#ffffff',
					           flexDirection: 'row-reverse',
					           alignContent: 'flex-start',
					           justifyContent: 'flex-start',
					           margin: 0,
				           }}
                   inputStyle={{
					           backgroundColor: '#ffffff',
					           width: '100%',
				           }}
                   onChangeText={text => this.handleSearchInput(text)}
				   value={this.state.search}
                   onCancel={this.clearSearchText}
		/>
        <Text style={{
					paddingTop: 1,
					borderTopColor: '#f7f7f7',
					paddingBottom: 10,
					paddingLeft: 5,
				}}>  {this.state.listLength} {translate('jobs_found')}</Text>
        <View style={styles.rootStyle2}>
          <FlatList
            style={{ flex: 1 }}
            data={this.state.search.length == 0 ? this.state.commentData : this.state.filteredList}
            renderItem={item => this.renderRow(item)}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
	  );
	}
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
  },

  rootStyle2: {
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 15,
    fontWeight: '300',
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },
};

const mapStateToProps = state => ({
  component: state.component,
});


export default connect(mapStateToProps)(Projects);
