/* eslint-disable no-unused-vars */

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Modal,
  Dimensions
} from "react-native";
import Config from "../../Config";
import Portifolio from "./portifolio";
import Projects from "./projects";
import Payment from "./subs/Payment";
import FilterModel from "./subs/FilterModel";
import FilterModelType from "./subs/FilterModelType";
import { translate } from "../../i18n";
import HttpRequest from "../../components/HttpRequest";
import FontStyle from "../../constants/FontStyle";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import Feather from "react-native-vector-icons/Feather";

const { width, height } = Dimensions.get("window");

class JobsList extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    const jobsSourcesType = [
      {
        id: 1,
        name: translate('all_jobs'),
      },
      {
        id: 2,
        name: translate('recommend_jobs'),
      },
      {
        id: 3,
        name: translate('fixed_price_job'),
      },
      {
        id: 4,
        name: translate('hourly_jobs'),
      },
    ];
    const jobsSourcesOType = [
      {
        id: 1,
        name: translate('recommend_jobs'),
      },
      {
        id: 2,
        name: translate('all_jobs'),
      },
    ];
    const jobsSourcesType1 = [
      {
        id: 1,
        name: translate('latest_jobs'),
      },
      {
        id: 2,
        name: translate('oldest_jobs'),
      },
      {
        id: 5,
        name: translate('quoated_price_asc'),
      },
      {
        id: 6,
        name: translate('quoated_price_dsc'),
      },
    ];
    this.state = {
      activeTabIndex: 1,
      isShowFilter: false,
      isShowFilterOfficial: false,
      jobsSourcesType,
      jobsSourcesOType,
      jobsSourcesType1,
      languages: [],
      countries: [],
      filterString: '',
      filterCountry: '',
      filterLang: '',
      filterHourlyRate: '',
      showOption: '',
      orderOption: '',
      filterOptions: null,
      skillIDs: [],
      skills: this.props.navigation.state.params.selectedSkills
    };
    this.root = this.props.component.root;
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    console.log('JobList.js');
    // Set route params
    this.props.navigation.setParams({
      headerRight: (
        <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              marginRight: 15,
              alignSelf: 'center',
            }}
            onPress={this.handleFilter}
        >
          <Feather name="filter" size={28} color={Config.primaryColor}/>
        </TouchableOpacity>
      )
    });
    this.getLanguages();
    this.getCountries();
  }
  goBackPage() {
    this.props.navigation.goBack();
  }
  getLanguages = () => {
    HttpRequest.getLanguages()
      .then(response => {
        this.setState({
          languages: response.data
        });
      })
      .catch(error => {});
  };

  getCountries() {
    HttpRequest.getOnlyCountries()
      .then((response) => {
        this.setState({
          countries: response.data,
        });
      })
      .catch(error => {});
  }

  handleFilter() {
    if (this.state.activeTabIndex === 1) {
      this.setState({
        isShowFilter: true,
        isShowFilterOfficial: false
      });
    } else {
      this.setState({
        isShowFilterOfficial: true,
        isShowFilter: false
      });
    }
  }

  applyFilters = () => {
    const {
      orderOption,
      showOption,
      filterCountry,
      filterLang,
      filterHourlyRate,
    } = this.state;
    this.setState({
      filterString: `order=${orderOption}&type=${showOption}${filterCountry}${filterLang}${filterHourlyRate}`,
      isShowFilter: false,
      isShowFilterOfficial: false
    });
  };

  getDropDown = data => {
    let selectedValue = this.state.filterString;
    if (data.type == "country") {
      this.setState({ filterCountry: `&country=${data.value}` });
    } else if (data.type == "language") {
      this.setState({ filterLang: `&language=${data.value}` });
    } else if (data.type == "hourly_rate") {
      this.setState({ filterHourlyRate: `&hourly_range=${data.value}` });
    } else {
      console.log("Nothing selected");
    }
  };

  showOption = (type, id) => {
    if (type === "order") {
      this.setState({ orderOption: id });
    } else {
      this.setState({ showOption: id });
    }
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={style.rootStyle}>
          {/* Back Button View */}
          <View style={style.navigationStyle}>
            <View style={style.navigationWrapper}>
              <TouchableOpacity
                  onPress={() => this.goBackPage()}
                  style={{
                    marginTop: 8,
                    marginLeft: 10,
                    flexDirection: "row",
                  }}
              >
                <Image
                    style={{width:20,height:26,position:'relative',top:-3}}
                    source={require('../../../images/arrowLA.png')}
                />

                <Text
                    onPress={() => this.goBackPage()}
                    style={{
                      marginLeft: 10,
                      marginTop:-1,
                      fontSize: 19.5,
                      fontWeight:'normal',
                      color: Config.primaryColor,
                      fontFamily: FontStyle.Regular,
                    }}
                >
                  {translate('job_listing')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                  style={{ marginLeft: 0,marginRight:10,marginTop:8 }}
                  onPress={() => this.handleFilter()}
              >
                <View style={styles.logoutButtonStyle}>
                  <Feather name="filter" size={28} color={Config.primaryColor}/>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={styles.topButtonWrapperStyle}>
          <View
            style={{
              width: 1,
              height: 50,
              backgroundColor: "#f7f7f7"
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({ activeTabIndex: 1 });
            }}
            style={[
              styles.topButtonStyle,
              this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}
            ]}
          >
            <Text
              style={
                this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {}
              }
            >
              {translate("common_project")}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              width: 1,
              height: 50,
              backgroundColor: "#f7f7f7"
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({ activeTabIndex: 2 });
            }}
            style={[
              styles.topButtonStyle,
              this.state.activeTabIndex == 2 ? styles.topButtonActiveStyle : {}
            ]}
          >
            <Text
              style={
                this.state.activeTabIndex == 2 ? styles.topTextActiveStyle : {}
              }
            >
              {translate("official_project")}
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.activeTabIndex == 1 && (
          <Projects
            navigation={this.props.navigation}
            filterString={this.state.filterString}
            skills={this.props.navigation.state.params.selectedSkills}
          />
        )}
        {this.state.activeTabIndex == 2 && (
          <Portifolio
            navigation={this.props.navigation}
            filterString={this.state.filterString}
            skills={this.props.navigation.state.params.selectedSkills}
          />
        )}

        {/* For Common Projects Filter */}
        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
            this.setState({
              isShowFilter: false
            });
          }}
          visible={this.state.isShowFilter}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <View style={styles.bottomSideStyle}>
                <Image
                  source={require("../../../images/jobs/filter.png")}
                  style={styles.avatarStyle}
                />
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text5Style}>{translate("filter")}</Text>
                </View>

                <TouchableOpacity
                  onPress={() => this.setState({ isShowFilter: false })}
                  style={styles.crossButtonStyle}
                >
                  <Ionicons
                    name="md-close-circle"
                    size={24}
                    color={Config.primaryColor}
                  />
                </TouchableOpacity>
              </View>

              <ScrollView>
                <FilterModel
                  action={translate("WITHDRAW")}
                  languages={this.state.languages}
                  countries={this.state.countries}
                  locale={this.props.navigation.state.params.locale}
                  callbackDropDown={this.getDropDown}
                />
                <Payment
                  action={translate("SHOW_OPTION")}
                  data={this.state.jobsSourcesType}
                  callbackShowOption={this.showOption}
                  type={"show"}
                />
                <Payment
                  action={translate("ORDERING_OPTION")}
                  data={this.state.jobsSourcesType1}
                  callbackShowOption={this.showOption}
                  type={"order"}
                />
                  <TouchableOpacity onPress={() => this.applyFilters()}>
                    <View style={styles.buttonWrapper}>
                    <Text style={styles.fontC}>{translate("apply")}</Text>
                    </View>
                  </TouchableOpacity>
              </ScrollView>
            </View>
          </View>
        </Modal>

        {/* For Offical Project Filter */}
        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
            this.setState({
              isShowFilterOfficial: false
            });
          }}
          visible={this.state.isShowFilterOfficial}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyleOfficial}>
              <View style={styles.bottomSideStyle}>
                <Image
                  source={require("../../../images/jobs/filter.png")}
                  style={styles.avatarStyle}
                />
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text5Style}>{translate("filter")}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.setState({ isShowFilterOfficial: false })}
                  style={styles.crossButtonStyle}
                >
                  <Ionicons
                    name="md-close-circle"
                    size={24}
                    color={Config.primaryColor}
                  />
                </TouchableOpacity>
              </View>
              <ScrollView>
                <FilterModelType
                  action={translate("WITHDRAW")}
                  languages={this.state.languages}
                  countries={this.state.countries}
                  callbackDropDown={this.getDropDown}
                />
                <Payment
                  action={translate("SHOW_OPTION")}
                  data={this.state.jobsSourcesOType}
                  callbackShowOption={this.showOption}
                  type={"show"}
                />
                <Payment
                  action={translate("ORDERING_OPTION")}
                  data={this.state.jobsSourcesType1}
                  callbackShowOption={this.showOption}
                  type={"order"}
                />
              </ScrollView>
              <View style={styles.buttonWrapper}>
                <TouchableOpacity onPress={() => this.applyFilters()}>
                  <Text style={styles.fontC}>{translate("apply")}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const style = {
  rootStyle: {
    backgroundColor: "white",
    // paddingHorizontal: 10
  },

  navigationWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  navigationStyle: {
    height: 55,
    flexDirection: "row",
    marginTop: 10,
    paddingHorizontal: 5,
    // alignItems:'center'
  },
}

const styles = {
  bottomSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 5,
    paddingBottom: 5,
    borderColor: "#f7f7f7",
    borderBottomWidth: 1
  },

  avatarStyle: {
    width: 25,
    height: 25,
    paddingTop: 10
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    resizeMode: "contain",
    position: "absolute",
    right: 0
  },

  text5Style: {
    fontSize: 16,
    color: "#6e6e6e"
  },

  fontC: {
    fontSize: 12,
    color: "#fff"
  },

  rootStyle: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column"
  },

  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: "95%",
    marginTop: 15,
    marginLeft: 5,
    paddingHorizontal: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    // color: '#fff',
    borderRadius: 5,
    marginBottom: 10
  },

  topButtonWrapperStyle: {
    flexDirection: "row",
    shadowColor: "#ccc",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0,
    shadowRadius: 2,
    elevation: 1
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderBottomColor: "#ffffff",
    borderBottomWidth: 2
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor
  },

  topTextActiveStyle: {
    color: Config.primaryColor
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },

  dialogBoxStyle: {
    width: width - 40,
    height: height / 1.5,
    backgroundColor: "#fff",
    borderRadius: 3
  },

  dialogBoxStyleOfficial: {
    width: width - 40,
    height: height / 1.5,
    backgroundColor: "#fff",
    borderRadius: 3
  },
  crossButtonStyle: {
    position: "absolute",
    right: 5,
    top: 0
  }
};

function mapStateToProps(state) {
  return {
    component: state.component
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: "set_root",
        root: root
      })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobsList);
