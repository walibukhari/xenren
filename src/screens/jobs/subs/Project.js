import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Rating } from "react-native-elements";
import Config from "../../../Config";
import { translate } from "../../../i18n";
import FontStyle from "../../../constants/FontStyle";

class Project extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: this.props.review,
      showPic: false,
      datas: [],
      locale: this.props.navigation.state.params.locale,
    };
  }

  renderIcon() {
    const array = [];
    array.push(
      <Ionicons
        name="ios-thumbs-down"
        size={15}
        color="grey"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-heart"
        size={15}
        color="grey"
        style={{ marginLeft: 10 }}
      />
    );
    return <View style={styles.iconStyle}>{array}</View>;
  }

  renderWhiteIcon() {
    const array = [];
    array.push(
      <Ionicons
        name="ios-thumbs-down"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-heart"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-share-alt"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    return <View style={styles.iconWhiteStyle}>{array}</View>;
  }

  renderSkill(skills) {
    return skills.map((item, index) => (
      <View
        key={index}
        style={{
          borderRadius: 15,
          backgroundColor: "#f1c40f",
          flexDirection: "column",
          alignItems: "center",
          marginRight: 5,
          paddingVertical: 3,
          paddingHorizontal: 10
        }}
      >
        <Text
          style={{
            paddingHorizontal: 5,
            backgroundColor: "transparent",
            color: "#fff"
          }}
        >
          {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.skill.name_cn : item.skill.name_en}
        </Text>
      </View>
    ));
  }

  handleProjectDetail = item => {
    this.props.navigation.navigate("OfficialProjectDetail", { project: item });
  };

  showImage = item => {
    const imagesArr = [];
    // portfolios.map((item) => {
    imagesArr.push({ source: { uri: `http://www.xenren.co/${item.path}` } });
    // })
    this.setState({
      datas: imagesArr,
      showPic: true
    });
  };

  renderAttachments(attachments) {
    return attachments.map((item, index) => (
      <TouchableOpacity
        key={index}
        style={{
          borderColor: "#f1f1f1",
          borderWidth: 1,
          flexDirection: "row",
          alignItems: "center",
          marginRight: 10
        }}
        onPress={() => this.showImage(item)}
      >
        <Image
          source={{ uri: `http://www.xenren.co/${item.path}` }}
          style={styles.imageAttachment}
        />
      </TouchableOpacity>
    ));
  }

  render() {
    const item = this.props.project.item;
    return (
      <View style={styles.rootStyle}>
        <View
          style={{
            marginHorizontal: 0,
            marginTop: 0,
            marginBottom: 10
          }}
        >
          <View style={styles.boxInsideStyle}>
            <TouchableOpacity onPress={() => this.handleProjectDetail(item)}>
              <ImageBackground
                source={{ uri: `${Config.webUrl}${item.banner}` }}
                style={styles.itemImageStyle}
                resizeMode="cover"
              >
                <View style={styles.topSideStyle}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 5
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      <Image
                        source={item.avatarM}
                        style={styles.avatarmStyle}
                      />
                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={2}
                        style={styles.text1Style}
                      >
                        {item.project.name}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    backgroundColor: "rgba(52,52,52,0.2)"
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 20,
                      marginTop: 10
                    }}
                  >
                    <Text style={styles.textWhiteStyle}>
                      {translate("PROJECT_TYPE")}
                      <Text style={styles.textWhiteStyle}>
                        {" "}
                        {item.projectId}
                      </Text>
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 20,
                      marginTop: 10
                    }}
                  >
                    <Text style={styles.textWhiteStyle}>
                      {translate("END_DATE")}
                      <Text style={styles.textWhiteStyle}>
                        {" "}
                        {item.recruit_end}
                      </Text>
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    backgroundColor: "rgba(52,52,52,0.2)"
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 20
                    }}
                  >
                    <Text style={styles.textWhiteStyle}>
                        {translate('FUNDING_STAGE')}
                      {item.funding_stage === 1 ? translate('seed') : translate('excecute')}
                      <Text style={styles.textWhiteStyle}>
                        {' '}
                        {item.projectId}
                      </Text>
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 20
                    }}
                  >
                    <Text style={styles.textWhiteStyle}>
                      {translate("LOCATION")}
                      <Text style={styles.textWhiteStyle}>
                        {" "}
                        {item.location_address}
                      </Text>
                    </Text>
                  </View>
                </View>
                {/* <View style={{ flex: 1,  flexDirection: 'row',backgroundColor: 'rgba(52,52,52,0.2)'}}>
								 <View style={{ flex: 1, flexDirection: 'column', marginLeft: 20 }}>
									<Text style={styles.textWhiteStyle}>{translate('TIME_FRAME')}<Text style={styles.textWhiteStyle}> {item.projectId}</Text></Text>
								 </View>
                            </View> */}
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    backgroundColor: "rgba(52,52,52,0.2)"
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "column",
                      marginLeft: 10
                    }}
                  >
                    {this.renderWhiteIcon()}
                  </View>
                </View>
              </ImageBackground>
            </TouchableOpacity>
            <View style={{ height: 100, padding: 10 }}>
              <Text  style={styles.textDescription}
                     numberOfLines={5}
              >
                  {item.description}
              </Text>
            </View>
            <View style={styles.bottomSideStyle}>
              <Image
                source={{ uri: `${Config.webUrl}${item.icon}` }}
                style={styles.avatarStyle}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 10
                }}
              >
                <Text style={styles.textPriceStyle}>
                  {item.pay_type === 1 ? translate('pay_hourly') : translate('pay_fixed')} -{" "}
                  <Text style={styles.textPriceStyle}>
                    {" "}
                    {item.project.reference_price}
                  </Text>
                </Text>
                {/* <Text style={styles.text3Style}>{item.number}:<Text style={styles.text4Style}> {item.numberpercent}</Text></Text>
									<Text style={styles.text5Style}>{item.price}</Text> */}
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 0,
                  alignItems: "flex-end"
                }}
              >
                <TouchableOpacity
                  style={styles.button2}
                  onPress={() => this.handleProjectDetail(item)}
                >
                  <Text
                    style={{
                      fontSize: 11,
                      color: "#fff",
                      fontFamily: FontStyle.Regular
                    }}
                  >
                    {translate("JOIN_DISCUSS")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.bottomSideStyle}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5
                }}
              >
                <Text style={styles.text6Style}>{item.type}</Text>
                <Rating
                  type="star"
                  startingValue={3}
                  readonly
                  imageSize={15}
                  style={{ alignContent: "flex-start" }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5,
                  borderColor: "#f7f7f7",
                  borderLeftWidth: 1,
                  height: 40
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text6Style}>{item.position}</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text7Style}>{item.position_type}</Text>
                </View>
              </View>
            </View>
            <View style={styles.bottomSideStyle1}>
              <View
                style={{
                  marginBottom: 5
                }}
              >
                <Text style={styles.textHeader}>
                  {translate("SKILL_REQUEST")}
                </Text>
                <View style={styles.middleSideStyle}>
                  {this.renderSkill(item.project.project_skills)}
                </View>
              </View>
            </View>

            {/* {item.project_images.length > 0 &&
                            <View style={{ marginTop: 5 }}>
                                <Text style={styles.textHeader}>{translate('attachment')}</Text>
                                <View style={styles.middleSideStyle}>
                                    {this.renderAttachments(item.project_images)}
                                </View>
                            </View>
                        } */}
          </View>
        </View>
      </View>
    );
  }

  renderAttachments(files) {
    return files.map((item, index) => (
      <View
        key={index}
        style={{
          flexDirection: "column",
          alignItems: "center",
          marginRight: 10
        }}
      >
        <TouchableOpacity
          style={{
            borderColor: "#f1f1f1",
            borderWidth: 1,
            marginBottom: 5
          }}
          onPress={() => this.showImage(item)}
        >
          <Image
            source={{ uri: `http://www.xenren.co/${item.file}` }}
            style={styles.imageAttachment}
          />
        </TouchableOpacity>
        {/* <View>
						<Text style={{ marginBottom: 10 }}>{item.text}</Text>
					</View> */}
      </View>
    ));
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 0,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#f7f7f7"
  },

  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderRadius: 5
  },

  boxInsideStyle1: {
    flexDirection: "row",
    backgroundColor: "rgba(52,52,52,0.2)"
  },

  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10,
    backgroundColor: "rgba(52,52,52,0.2)",
    marginBottom: 0
  },

  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },

  bottomSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
    borderColor: "#f7f7f7",
    borderTopWidth: 1
  },

  bottomSideStyle1: {
    flexDirection: "row",
    alignItems: "center",
    borderColor: "#f7f7f7",
    borderTopWidth: 1
  },

  avatarStyle: {
    width: 50,
    height: 50,
    borderRadius: 15,
    marginTop: 10
  },

  avatarmStyle: {
    width: 25,
    height: 25,
    resizeMode: "contain"
    //  marginTop: 15,
  },

  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },

  cardButtonTextStyle: {
    color: Config.primaryColor,
    fontSize: 12,
    fontFamily: FontStyle.Regular
  },

  itemImageStyle: {
    width: "100%",
    height: 250,
    backgroundColor: "rgba(52,52,52,0.4)"
  },

  text1Style: {
    flex: 1,
    color: "#fff",
    marginLeft: 5,
    marginTop: 5,
    fontSize: 18,
    fontFamily: FontStyle.Bold
  },

  text2Style: {
    fontSize: 15,
    color: "#bdc3c7",
    fontFamily: FontStyle.Regular
  },

  text3Style: {
    fontSize: 10,
    fontFamily: FontStyle.Light,
    color: "#bdc3c7"
  },

  textWhiteStyle: {
    fontSize: 14,
    fontFamily: FontStyle.Light,
    color: "#fff"
  },

  textPriceStyle: {
    fontSize: 12,
    fontFamily: FontStyle.Regular,
    color: "#4b4b4b"
  },

  text4Style: {
    fontSize: 10,
    fontFamily: FontStyle.Light,
    color: Config.primaryColor
  },

  text5Style: {
    fontSize: 14,
    fontFamily: FontStyle.Regular,
    color: Config.primaryColor
  },

  text6Style: {
    fontSize: 14,
    fontFamily: FontStyle.Regular,
    color: "#4b4b4b",
    paddingTop: 3
  },

  text7Style: {
    fontSize: 13,
    fontFamily: FontStyle.Regular,
    color: Config.primaryColor,
    paddingTop: 0
  },

  text8Style: {
    fontSize: 13,
    fontFamily: FontStyle.Light,
    color: Config.primaryColor,
    paddingTop: 4,
    marginLeft: 5
  },

  starStyle: {
    flexDirection: "row",
    paddingTop: 0,
    paddingBottom: 3
  },

  iconStyle: {
    flexDirection: "row",
    paddingTop: 8,
    marginLeft: 8,
    borderColor: "#f7f7f7",
    borderLeftWidth: 1,
    height: 35
  },

  iconWhiteStyle: {
    flexDirection: "row",
    paddingTop: 8,
    marginLeft: 8,
    height: 35
  },

  textDescription: {
    fontSize: 18,
    lineHeight: 20,
    fontFamily: FontStyle.Regular,
  },

  textHeader: {
    fontFamily: FontStyle.Bold,
    fontSize: 20,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 5
  },

  imageAttachment: {
    width: 80,
    height: 80
  },

  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },

  button1: {
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: Config.primaryColor,
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },

  button2: {
    backgroundColor: Config.primaryColor,
    width: 100,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    marginTop: 5,
    borderColor: Config.primaryColor,
    borderWidth: 1
  },

  signUpButtonStyle: {
    fontFamily: FontStyle.Bold,
    color: Config.primaryColor
  }
};

export default Project;
