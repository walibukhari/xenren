/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  TextInput,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Config from '../Config';
import { translate } from '../../../i18n';

class ApplyJob extends Component {
  static propTypes = {
    children: PropTypes.node,
    action: PropTypes.string.isRequired,
  };

  constructor(props) {
	
    super(props);
    this.state = {
      paymentGateways: [],
      activePay: 'WeChat Pay',
    };
  }



  render() {
    return (
      <View>
        <View style={{ marginTop: 5 ,paddingHorizontal: 8}}>
			   <View style={styles.amountInputWrapper}>
			  <Text style={styles.amountInputTitle}>{translate('price')} ($)</Text>
			  <TextInput
				underlineColorAndroid="transparent"
				placeholder="XXXX"/>
			</View>
			 <View style={styles.amountInputWrapper}>
			  <Text style={styles.amountInputTitle}>{translate('how_are_you')}</Text>
			  <TextInput
				underlineColorAndroid="transparent"
				placeholder="Please Write"/>
			</View>
			 <View style={styles.amountInputWrapper}>
			  <Text style={styles.amountInputTitle}>{translate('about')}</Text>
			  <TextInput
				underlineColorAndroid="transparent"
				placeholder="Please Write"/>
			</View>
        </View>
        <View style={styles.footerWrapper}>
          <Text style={styles.footerTextDescription}>
           {translate('click_agree_to_recharge_agreement')}
          </Text>
          <TouchableOpacity
            onPress={this.handlePayment}
            style={styles.buttonWrapper}>
            <Text
              style={{
                color: '#fff',
                fontFamily: 'Montserrat-Medium',
                fontSize: 20,
              }}>
              {this.props.action}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  footerWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerTextDescription: {
    justifyContent: 'center',
    textAlign: 'center',
    color: '#888888',
    fontFamily: 'Montserrat-Light',
    fontSize: 15,
  },
  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: '100%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
    rootStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  topWrapperStyle: {
    backgroundColor: '#f4f4f4',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  normalTopWrapper: {
    backgroundColor: '#5fc229',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: 'Montserrat-Medium',
    fontSize: 32,
    color: '#000',
  },
  priceDescription: {
    fontFamily: 'Montserrat-Light',
    fontSize: 20,
    color: '#5c5c5c',
    marginTop: 5,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },
  balanceText: {
    color: '#fff',
    fontSize: 24,
  },
  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: '#e6e6e6',
  },
  amountInputTitle: {
    color: '#979797',
    backgroundColor: '#f6f6f6',
    padding: 2,
  },
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});

export default CreditCardInner;
