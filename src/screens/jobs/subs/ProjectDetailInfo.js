/* eslint-disable no-dupe-keys */
/* eslint-disable object-shorthand */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable object-property-newline */
/* eslint-disable max-len */
/* eslint-disable arrow-parens */
/* eslint-disable global-require */
/* eslint-disable semi */
/* eslint-disable block-spacing */
/* eslint-disable consistent-return */
/* eslint-disable object-curly-spacing */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-key */
/* eslint-disable indent */
/* eslint-disable no-tabs */
/* eslint-disable array-callback-return */
/* eslint-disable prefer-const */
/* eslint-disable import/first */

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  TextInput,
  ScrollView,
  Dimensions,
  SafeAreaView,
  PermissionsAndroid,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Config from '../../../Config';
import { translate } from '../../../i18n';
import HttpRequest from '../../../components/HttpRequest';
import Gallery from 'react-native-image-gallery';
import RNFetchBlob from 'react-native-fetch-blob';

const { width, height } = Dimensions.get('window');

class ProjectDetailInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.state.params.project,
      isJobApply: false,
      price: '',
      showPic: false,
      selImage: '',
      questions: [],
      aboutMe: '',
      payTypeActiveTabIndex: 0,
      locale: this.props.navigation.state.params.locale,
    };
  }

  componentDidMount() {
    console.log('data--<', this.props.navigation.state.params.project);
    let arr = [];
    if (this.state.data !== '') {
        if (this.state.data.item !== null) {
            this.state.data.project_questions.map(item => {
                arr.push({
                    question: item.question,
                    answer: '',
                    id: item.id,
                });
            });
        }
    }
    this.setState({
      questions: [...this.state.questions, ...arr],
    });
  }

  renderSkill = skills => {
    return skills.map((item, index) => {
      return (
        <View
          key={index}
          style={{
            borderRadius: 15,
            borderColor: "#f3f3f3",
            borderWidth: 1,
            flexDirection: "column",
            alignItems: "center",
            marginRight: 5,
            padding: 3
          }}
        >
          <Text
            style={{
              paddingHorizontal: 5,
              backgroundColor: "transparent"
            }}
          >
            {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.skill.name_cn : item.skill.name_en}
          </Text>
        </View>
      );
    });
  };

  handleApplyJob = () => {
    this.setState({
      isJobApply: true
    });
  };

  onQqPress() {
    this.setState({ showModal: true });
  }

  renderAttachments(files) {
    let arr = [];
    files.map((item) => {
        let ext = this.getExtension(item.file);
        arr.push(ext[0]);
    });
    return files.map((item, index) => {
      if (arr[index] === 'png' || arr[index] === 'jpg' || arr[index] === 'jpeg') {
          return (
              <View
                  key={index}
                  style={{
                      flexDirection: "column",
                      alignItems: "center",
                      marginRight: 10
                  }}
              >
                  <TouchableOpacity
                      style={{
                          borderColor: "#f1f1f1",
                          borderWidth: 1,
                          marginBottom: 5,
                      }}
                      onPress={() => this.showImage(item)}
                  >
                      <Image
                          source={{uri: `${Config.webUrl}${item.file}`}}
                          style={styles.imageAttachment}
                      />
                  </TouchableOpacity>
              </View>
          );
      } else if (arr[index] === 'pdf') {
          return (
              <View
                  key={index}
                  style={{
                      flexDirection: "column",
                      alignItems: "center",
                      marginRight: 10
                  }}
              >
                  <View
                      style={{
                          borderColor: "#f1f1f1",
                          borderWidth: 1,
                          marginBottom: 5,
                      }}
                  >
                      <Image
                          source={require("../../../../images/pdf.png")}
                          style={styles.imageAttachment}
                      />
                  </View>
              </View>
          );
      } else {
          return (
              <View
                  key={index}
                  style={{
                      flexDirection: "column",
                      alignItems: "center",
                      marginRight: 10
                  }}
              >
                  <View
                      style={{
                          borderColor: "#f1f1f1",
                          borderWidth: 1,
                          marginBottom: 5,
                      }}
                  >
                      <Image
                          source={require("../../../../images/doc.png")}
                          style={styles.imageAttachment}
                      />
                  </View>
              </View>
          );
      }
    });
  }

  updateAnswer = (index, msg) => {
    let arr = this.state.questions;
    arr[index].answer = msg;
    this.setState({ questions: arr });
  };
    getExtension = (filename) => {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) :
            undefined;
    };

    downloadAll = (files) => {
        const url = Config.webUrl;
        files.map((item, index) => {
            let e = `${url}${item.file}`;
            let ext = this.getExtension(e);
            let path = '';
            if (Platform.OS === 'android') {
                path = RNFetchBlob.fs.dirs.DownloadDir;
                const granted = PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Download File',
                        message:
                            'Xenren want to access your storage',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                granted.then((res) => {
                    if (res === PermissionsAndroid.RESULTS.GRANTED) {
                        RNFetchBlob.config({
                            addAndroidDownloads: {
                                useDownloadManager: true, // <-- this is the only thing required
                                // Optional, override notification setting (default to true)
                                notification: false,
                                path: path + `/project-file-${index}.${ext[0]}`,
                                // Optional, but recommended since android DownloadManager will fail when
                                // the url does not contains a file extension, by default the mime type will be text/plain
                                mime: 'text/plain',
                                description: 'File downloaded by download manager.'
                            }
                        }).fetch('GET', `${url}${item.file}`).then((res) => {
                            console.log('res---<<<', res);
                        })
                    } else {
                        alert('Permission has been Denied!!!\nPlease go to app setting and allow read memory');
                    }
                });
            } else {
                path = RNFetchBlob.fs.dirs.DocumentDir;
                // RNFetchBlob.fetch('GET', `${url}${item.file}`).then((res) => {
                //     console.log('res----<<<', res);
                    // let imageView = <Image source={{ uri : 'data:image/png;base64,' + res.data }}/>;
                    // RNFetchBlob.ios.previewDocument(res.path());
                    // console.log('Base64Code---<<', Base64Code[0]);
                    // RNFetchBlob.fs.writeFile(path, Base64Code[0], 'base64')
                    //     .then((resp) => {
                    //         console.log('resp----<', resp);
                    //         RNFetchBlob.ios.openDocument(path +'/'+ res +'.'+ext[0]);
                    //     })
                    //     .catch((e) => {
                    //         console.log(e);
                    //     });
                // });
                RNFetchBlob
                    .config({
                        path : path + `/project-file-${index}.${ext[0]}`,
                        IOSBackgroundTask: true,
                        overwrite: true,
                        indicator: true,
                    })
                    .fetch('GET', e)
                    .progress( (received, total) => {
                        console.log('progress : '+ received + ' / ' + total);
                    })
                    .then((res) => {
                        if (index === 1) alert('File saved');
                        console.log('The file saved to :', res);
                    });
            }
        });
    };

  renderQuestion = () => {
    if (this.state.data.project_questions !== null) {
      return this.state.data.project_questions.map((item, index) => {
        return (
          <View key={index} style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}>
              Question {index + 1}: {item.question}
            </Text>
            <TextInput
              multiline={true}
              onChangeText={text => {
                this.updateAnswer(index, text);
              }}
              underlineColorAndroid="transparent"
              placeholder="Your Answer"
            />
          </View>
        );
      });
    }
  };

  showImage = item => {
    let imagesArr = [];
    // portfolios.map((item) => {
    imagesArr.push({ source: { uri: `${Config.webUrl}${item.file}` } });
    // })
    this.setState({
      datas: imagesArr,
      showPic: true
    });
  };

  render() {
    const data = this.props.navigation.state.params.project;
    if (data !== '') {
        return (
            <ScrollView style={styles.rootStyle}>
                <View
                    style={{
                        marginVertical: 10
                    }}
                >
                    <View style={styles.boxInsideStyle}>
                        <TouchableOpacity onPress={this.handleProjectDetail}>
                            <View style={styles.topSideStyle}>
                                <View
                                    style={{
                                        flex: 1,
                                        flexDirection: "column",
                                        marginLeft: 5
                                    }}
                                >
                                    <View style={{flexDirection: "row"}}>
                                        <Text
                                            ellipsizeMode="tail"
                                            numberOfLines={1}
                                            style={styles.text1Style}
                                        >
                                            {data.name}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection: "row"}}>
                                        <View style={{flexDirection: "column"}}>
                                            <Text style={styles.text3Style}>
                                                {translate("posted_by")}
                                            </Text>
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: "column",
                                                marginLeft: 5
                                            }}
                                        >
                                            <Image
                                                source={{
                                                    uri: `${data.creator.img_avatar}`,
                                                }}
                                                style={styles.avatarStyle}
                                            />
                                        </View>
                                        <View
                                            style={{
                                                flexDirection: "column",
                                                marginLeft: 5
                                            }}
                                        >
                                            <Text style={styles.text3Style}>
                                                {data.creator.real_name}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.container}>
                            <View style={styles.payHour}>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: "#afafaf"
                                    }}
                                >
                                    {data.pay_type === 1 ? translate('hourly_price') : translate('fixed_price')} -{" "}
                                    <Text
                                        style={{
                                            fontSize: 13,
                                            color: Config.primaryColor
                                        }}
                                    >
                                        ${data.reference_price}{" "}
                                    </Text>
                                </Text>
                            </View>
                            <View style={styles.projectType}>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: "#afafaf"
                                    }}
                                >
                                    {translate("project_type")} -{" "}
                                    <Text
                                        style={{
                                            fontSize: 13,
                                            color: Config.primaryColor
                                        }}
                                    >
                                        {data.type === 1 ? translate('public') : translate('private')}
                                    </Text>
                                </Text>
                            </View>
                        </View>

                        <View
                            style={{
                                marginBottom: 20
                            }}
                        >
                            <View style={styles.middleSideStyle}>
                                <Text style={styles.textHeaderDesc}>
                                    {translate("DESCRIPTION")}:{" "}
                                </Text>
                                <Text
                                    style={styles.textDescription}>{data.description.replace(/(<([^>]+)>)/ig, '')}</Text>
                            </View>
                        </View>

                        <View
                            style={{
                                marginBottom: 20,
                                borderColor: "#ecf0f1",
                                borderTopWidth: 1
                            }}
                        >
                            <Text style={styles.textHeader}>
                                {translate("SKILL_REQUEST")}
                            </Text>
                            <View style={styles.middleSideStyle}>
                                {this.renderSkill(data.project_skills)}
                            </View>
                        </View>

                        <View
                            style={{
                                borderColor: "#ecf0f1",
                                borderTopWidth: 1
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    margin: 5
                                }}
                            >
                                {/* <View style={{ flexDirection: 'column' }}> */}
                                <Text style={styles.textHeader}>{translate("ATTACHMENT")}</Text>
                                {/* </View> */}
                                {data.project_files !== null && (
                                    <TouchableOpacity
                                        onPress={() => this.downloadAll(data.project_files)}
                                    >
                                        <Text style={styles.textDownload}>
                                        {translate("download_all")} (
                                        {data.project_files !== null
                                            ? data.project_files.length
                                            : "0"}
                                        )
                                        </Text>
                                    </TouchableOpacity>
                                )}
                            </View>

                            <View style={styles.middleSideStyle}>
                                {data.project_files !== null &&
                                this.renderAttachments(data.project_files)}
                            </View>
                        </View>
                    </View>

                    <TouchableOpacity onPress={this.handleApplyJob}>
                        <View style={styles.container}>
                            <View style={styles.button1}>
                                <Text
                                    style={{
                                        fontSize: 13,
                                        color: "#fff"
                                    }}
                                >
                                    {translate("MAKE_OFFER")}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    onRequestClose={() => {
                        this.setState({
                            isJobApply: false
                        });
                    }}
                    visible={this.state.isJobApply}
                >
                    <View style={styles.dialogStyle}>
                        <View style={styles.dialogBoxStyle}>
                            <View style={styles.applyStyle}>
                                <Image
                                    source={require("../../../../images/jobs/applyjob.png")}
                                    style={styles.applyavatarStyle}
                                />
                                <View
                                    style={{
                                        flex: 1,
                                        flexDirection: "column",
                                        marginLeft: 10
                                    }}
                                >
                                    <Text style={styles.text6Style}>
                                        {translate("apply_job")}
                                    </Text>
                                </View>
                                <TouchableOpacity
                                    style={{
                                        flex: 1,
                                        flexDirection: "column",
                                        marginTop: 5
                                    }}
                                    onPress={() => this.setState({isJobApply: false})}
                                >
                                    <View
                                        style={{
                                            flex: 1,
                                            flexDirection: "column",
                                            marginTop: 0
                                        }}
                                    >
                                        <Image
                                            source={require("../../../../images/jobs/cross.png")}
                                            style={styles.avatarStyle1}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <ScrollView
                                style={{width: "100%"}}
                                contentContainerStyle={{
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <View
                                    style={{
                                        width: "95%",
                                        marginTop: 5,
                                        paddingHorizontal: 0
                                    }}
                                >
                                    <View style={styles.amountInputWrapper}>
                                        <Text style={styles.amountInputTitle}>
                                            {translate("price")} ($)
                                        </Text>

                                        <TextInput
                                            underlineColorAndroid="transparent"
                                            placeholder="XXXX"
                                            onChangeText={price => this.setState({
                                                price: price
                                            })}
                                        />
                                    </View>
                                    {this.renderQuestion()}

                                    <View style={styles.amountInputWrapper}>
                                        <Text style={styles.amountInputTitle}>
                                            {translate("about")}
                                        </Text>
                                        <TextInput
                                            multiline={true}
                                            onChangeText={text => this.setState({aboutMe: text})}
                                            underlineColorAndroid="transparent"
                                            value={this.state.aboutMe}
                                            placeholder={translate('please_write')}
                                        />
                                    </View>
                                </View>

                                <View style={styles.payTypeStyle}>
                                    <View style={styles.payTypeStyle2}>
                                        <Text style={styles.payTypeTextStyle}>
                                            {translate("PAY_TYPE")}
                                        </Text>
                                    </View>

                                    <View
                                        style={{
                                            flexDirection: "row",
                                            width: "85%",
                                            height: 50,
                                            justifyContent: "flex-start",
                                            alignItems: "center",
                                            marginHorizontal: 15,
                                            marginBottom: 10
                                        }}
                                    >
                                        <View
                                            style={{
                                                flexDirection: "row",
                                                justifyContent: "flex-start",
                                                alignItems: "center",
                                                width: "60%",
                                                height: 50
                                            }}
                                        >
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({payTypeActiveTabIndex: 0});
                                                }}
                                                style={[
                                                    styles.topButtonStyle,
                                                    this.state.payTypeActiveTabIndex === 0
                                                        ? styles.topButtonActiveStyle
                                                        : {}
                                                ]}
                                            >
                                                <Text
                                                    style={
                                                        this.state.payTypeActiveTabIndex === 0
                                                            ? styles.topTextActiveStyle
                                                            : {}
                                                    }
                                                >
                                                    {translate("pay_hourly")}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({payTypeActiveTabIndex: 1});
                                                }}
                                                style={[
                                                    styles.topButtonStyle,
                                                    this.state.payTypeActiveTabIndex === 1
                                                        ? styles.topButtonActiveStyle
                                                        : {}
                                                ]}
                                            >
                                                <Text
                                                    style={
                                                        this.state.payTypeActiveTabIndex === 1
                                                            ? styles.topTextActiveStyle
                                                            : {}
                                                    }
                                                >
                                                    {translate('fixed')}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                        {/*<View style={{*/}
                                        {/*marginLeft: 15,*/}
                                        {/*width: '40%',*/}
                                        {/*height: 40,*/}
                                        {/*flexDirection: 'row',*/}
                                        {/*alignItems: 'center',*/}
                                        {/*borderColor: '#F6F6F6',*/}
                                        {/*borderWidth: 2,*/}
                                        {/*}}>*/}
                                        {/*<TextInput style={{*/}
                                        {/*multiline: false,*/}
                                        {/*height: 40,*/}
                                        {/*width: '100%',*/}
                                        {/*}}*/}
                                        {/*underlineColorAndroid="transparent"*/}
                                        {/*placeholder="$ 0.0 "*/}
                                        {/*/>*/}
                                        {/*</View>*/}
                                    </View>
                                </View>

                                {/* Daily Updates */}
                                <View
                                    style={{
                                        width: "100%",
                                        height: 60,
                                        marginTop: 5,
                                        marginBottom: 5,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                >
                                    <View style={styles.dailyUpdatesStyle}>
                                        <Text style={styles.dailyUpdatesTextStyle}>
                                            {translate('updates_daily')}
                                        </Text>
                                    </View>
                                    <View
                                        style={{
                                            width: "100%",
                                            flexDirection: "row",
                                            justifyContent: "space-between",
                                            alignItems: "center",
                                            marginBottom: 10,
                                            marginLeft: 30
                                        }}
                                    >
                                        <View
                                            style={{
                                                width: "50%",
                                                flexDirection: "row",
                                                justifyContent: "flex-start",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    fontSize: 18,
                                                    fontWeight: "400"
                                                }}
                                            >
                                                {translate('yes')}
                                            </Text>
                                            <TouchableOpacity
                                                style={{
                                                    marginLeft: 10,
                                                    height: 28,
                                                    width: 28,
                                                    borderRadius: 14,
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                    backgroundColor:
                                                        this.state.selected === 1
                                                            ? Config.primaryColor
                                                            : "#ebebeb"
                                                }}
                                                onPress={() => {
                                                    this.dailyUpdates(1);
                                                }}
                                            >
                                                <Ionicon name="ios-checkmark" size={25} color="#fff"/>
                                            </TouchableOpacity>
                                        </View>
                                        <View
                                            style={{
                                                width: "50%",
                                                flexDirection: "row",
                                                justifyContent: "flex-start",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    fontSize: 18,
                                                    fontWeight: "400"
                                                }}
                                            >
                                                {translate('no')}
                                            </Text>
                                            <TouchableOpacity
                                                style={{
                                                    marginLeft: 10,
                                                    height: 28,
                                                    width: 28,
                                                    borderRadius: 14,
                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                    backgroundColor:
                                                        this.state.selected === 2
                                                            ? Config.primaryColor
                                                            : "#ebebeb"
                                                }}
                                                onPress={() => {
                                                    this.dailyUpdates(2);
                                                }}
                                            >
                                                <Ionicon name="ios-checkmark" size={25} color="#fff"/>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.contactStyle}>
                                    <Text style={styles.contactTextStyle}>
                                        {translate("CONTACT")}
                                    </Text>
                                </View>

                                {this.state.data.item !== null && (
                                    <View style={styles.headerStyle}>
                                        <TouchableOpacity
                                            style={styles.socialButtonStyle}
                                            onPress={() => {
                                                this.onQqPress();
                                            }}
                                        >
                                            <Image
                                                source={require("./../../../../images/private_chat/icon_qq.png")}
                                                style={styles.socialIconStyle}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 12,
                                                    color: "#000"
                                                }}
                                            >
                                                {translate("qq")}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize: 10,
                                                    color: "#555"
                                                }}
                                            >
                                                {this.state.data.creator.qq_id !== null
                                                    ? this.state.data.creator.qq_id
                                                    : ""}
                                            </Text>
                                        </TouchableOpacity>
                                        <View
                                            style={{
                                                width: 1,
                                                backgroundColor: "#f7f7f7"
                                            }}
                                        />
                                        <TouchableOpacity
                                            style={styles.socialButtonStyle}
                                            onPress={() => {
                                                this.onQqPress();
                                            }}
                                        >
                                            <Image
                                                source={require("./../../../../images/private_chat/icon_skype.png")}
                                                style={styles.socialIconStyle}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 12,
                                                    color: "#000"
                                                }}
                                            >
                                                {translate("skype")}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize: 10,
                                                    color: "#555"
                                                }}
                                            >
                                                {this.state.data.creator.skype_id !== null
                                                    ? this.state.data.creator.skype_id
                                                    : ""}
                                            </Text>
                                        </TouchableOpacity>
                                        <View
                                            style={{
                                                width: 1,
                                                backgroundColor: "#f7f7f7"
                                            }}
                                        />
                                        <TouchableOpacity
                                            style={styles.socialButtonStyle}
                                            onPress={() => {
                                                this.onQqPress();
                                            }}
                                        >
                                            <Image
                                                source={require("./../../../../images/private_chat/icon_wechat.png")}
                                                style={styles.socialIconStyle}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 12,
                                                    color: "#000"
                                                }}
                                            >
                                                {translate("wechat")}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize: 10,
                                                    color: "#555"
                                                }}
                                            >
                                                {this.state.data.creator.wechat_id !== null
                                                    ? this.state.data.creator.wechat_id
                                                    : ""}
                                            </Text>
                                        </TouchableOpacity>
                                        <View
                                            style={{
                                                width: 1,
                                                backgroundColor: "#f7f7f7"
                                            }}
                                        />
                                        <TouchableOpacity
                                            style={styles.socialButtonStyle}
                                            onPress={() => {
                                                this.onQqPress();
                                            }}
                                        >
                                            <Image
                                                source={require("./../../../../images/private_chat/icon_phone.png")}
                                                style={styles.socialIconStyle}
                                            />
                                            <Text
                                                style={{
                                                    fontSize: 12,
                                                    color: "#000"
                                                }}
                                            >
                                                {translate("phone")}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize: 10,
                                                    color: "#555"
                                                }}
                                            >
                                                {this.state.data.creator.handphone_no !== null
                                                    ? this.state.data.creator.handphone_no
                                                    : ""}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            </ScrollView>

                            <TouchableOpacity
                                onPress={() => this.makeOffer()}
                                style={styles.buttonWrapper}
                            >
                                <Text
                                    style={{
                                        color: "white",
                                        fontWeight: "400",
                                        fontSize: 16,
                                        textAlign: "center"
                                    }}
                                >
                                    {translate("MAKE_OFFER")}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                {/* For Images */}
                <Modal
                    transparent={true}
                    supportedOrientations={["portrait", "landscape"]}
                    visible={this.state.showPic}
                    onRequestClose={() => console.log("")}
                >
                    <SafeAreaView
                        style={{
                            flex: 1,
                            backgroundColor: "#2C2C2C",
                            alignItems: "flex-end"
                        }}
                    >
                        <TouchableOpacity onPress={() => this.setState({showPic: false})}>
                            <Image
                                source={require("../../../../images/account_profile/close.png")}
                                style={{
                                    width: 30,
                                    height: 30,
                                    marginTop: 10,
                                    marginRight: 10
                                }}
                            />
                        </TouchableOpacity>

                        {/* For Swiping Images */}
                        <Gallery
                            style={{
                                flex: 1,
                                backgroundColor: "transparent"
                            }}
                            images={this.state.datas}
                        />
                    </SafeAreaView>
                </Modal>
            </ScrollView>
        );
    } else {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text>No project Record Found</Text>
            </View>
        );
    }
  }

  closeModal = () => {
    this.setState({ showPic: false });
  };

  dailyUpdates = value => {
    this.setState({ selected: value });
  };

  makeOffer = () => {
    let self = this;
    let questIds = [];
    let answers = [];
    this.state.questions.map(item => {
      questIds.push(item.id);
      answers.push(item.answer);
    });
    let body = {
      price: this.state.data.reference_price,
      project_id: this.state.data.id,
      inbox_id: "0",
      about_me: this.state.aboutMe,
      pay_type: "1",
      daily_update: this.state.selected,
      questionId: questIds,
      answer: answers
    };
    if (this.state.data.creator.wechat_id !== null) {
      body.wechat_id = this.state.data.creator.wechat_id;
    }
    if (this.state.data.creator.skype_id !== null) {
      body.skype_id = this.state.data.creator.skype_id;
    }
    if (this.state.data.creator.qq_id !== null) {
      body.qq_id = this.state.data.creator.qq_id;
    }
    if (this.state.data.creator.handphone_no !== null) {
      body.handphone_no = this.state.data.creator.handphone_no;
    }
    HttpRequest.makeOffer(this.props.userData.token, body)
      .then(response => {
          console.log(response);
          alert(response.data.msg);
        self.setState({ isJobApply: false });
      })
      .catch(error => {
          console.log('r');
          console.log(error);
          self.setState({ isJobApply: false });
        alert(error);
      });
  };
}

const styles = {
  docModalContainer: {
    width: width,
    height: height,
    backgroundColor: "#fff"
  },

  avatarStyle: {
    width: 20,
    height: 20,
    borderRadius: 15
  },

  modalContainerStyle: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    flex: 1,
    justifyContent: "flex-start"
  },

  rootStyle: {
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },

  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff",
    width: "100%",
    borderRadius: 5
  },

  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10,
    marginBottom: 0
  },

  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },

  bottomSideStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    margin: 10,
    height: 60
  },

  applyStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 5,
    paddingBottom: 5,
    borderColor: "#ecf0f1",
    borderBottomWidth: 1
  },

  applyavatarStyle: {
    width: 25,
    height: 25,
    paddingTop: 10,
    resizeMode: "contain"
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    resizeMode: "contain",
    position: "absolute",
    right: 0
  },

  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },

  cardButtonTextStyle: {
    color: Config.primaryColor,
    fontSize: 12
  },

  text1Style: {
    flex: 1,
    color: "black",
    fontSize: 24
  },

  text2Style: {
    fontSize: 15,
    color: "#bdc3c7"
  },

  text3Style: {
    fontSize: 13,
    fontWeight: "200",
    color: "#727272"
  },

  text4Style: {
    fontSize: 13,
    fontWeight: "200",
    color: Config.primaryColor
  },

  text5Style: {
    fontSize: 11,
    color: "#bdc3c7"
  },

  text6Style: {
    fontSize: 16,
    color: "#808283"
  },

  starStyle: {
    flexDirection: "row",
    paddingTop: 8
  },

  iconStyle: {
    flexDirection: "row",
    paddingTop: 8,
    marginLeft: 8,
    borderColor: "#ecf0f1",
    borderLeftWidth: 1,
    height: 35
  },

  textDescription: {
    fontSize: 16,
    color: "grey",
    fontWeight: "normal"
  },

  textHeader: {
    fontWeight: "bold",
    fontSize: 18,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    marginTop: 15
  },

  textDownload: {
    fontSize: 15,
    color: Config.primaryColor,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    marginTop: 15
  },

  textHeaderDesc: {
    fontWeight: "bold",
    fontSize: 18,
    marginBottom: 5,
    marginTop: 15,
    width: "100%"
  },

  imageAttachment: {
    width: 80,
    height: 80
  },

  container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 5,
    paddingVertical: 10
  },

  button1: {
    backgroundColor: Config.primaryColor,
    borderWidth: 1,
    marginTop: 5,
    borderColor: Config.primaryColor,
    width: "100%",
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3
  },

  buttonPaytype: {
    backgroundColor: Config.primaryColor,
    borderWidth: 1,
    borderColor: "#ecf0f1",
    flex: 1,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3
  },

  button2: {
    backgroundColor: Config.primaryColor,
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },

  payHour: {
    backgroundColor: "#fff",
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#ecf0f1",
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },

  projectType: {
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#ecf0f1",
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },

  signUpButtonStyle: {
    fontWeight: "bold",
    color: Config.primaryColor
  },

  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: "#e6e6e6"
  },

  amountInputTitle: {
    color: "#979797",
    backgroundColor: "#f6f6f6",
    padding: 2
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },

  dialogBoxStyle: {
    width: width * 0.85,
    height: height * 0.65,
    backgroundColor: "#fff",
    borderRadius: 3
  },

  fontC: {
    fontSize: 12,
    color: "#fff"
  },

  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: "95%",
    marginTop: 10,
    marginLeft: 7,
    marginBottom: 10,
    paddingHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5
  },

  contactStyle: {
    flexDirection: "row",
    width: "100%",
    height: 45,
    borderColor: "#ecf0f1",
    borderTopWidth: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },

  payTypeStyle2: {
    flexDirection: "row",
    width: "100%",
    height: 45,
    borderColor: "#ecf0f1",
    borderTopWidth: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },

  dailyUpdatesStyle: {
    flexDirection: "row",
    width: "100%",
    height: 45,
    borderColor: "#ecf0f1",
    borderTopWidth: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },

  payTypeStyle: {
    width: "100%",
    height: 100,
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: 2
  },

  contactTextStyle: {
    fontSize: 17,
    color: "#6e6e6e",
    fontWeight: "bold",
    textAlign: "center",
    paddingLeft: 13
  },

  dailyUpdatesTextStyle: {
    fontSize: 17,
    color: "#6e6e6e",
    fontWeight: "bold",
    textAlign: "center",
    paddingLeft: 12
  },

  payTypeTextStyle: {
    fontSize: 17,
    color: "#6e6e6e",
    fontWeight: "bold",
    textAlign: "center",
    paddingLeft: 10
  },

  headerStyle: {
    flexDirection: "row",
    height: 70,
    borderColor: "#ecf0f1",
    borderTopWidth: 1,
    borderBottomWidth: 1
  },

  socialButtonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  socialIconStyle: {
    height: 25,
    width: 25,
    resizeMode: "contain",
    marginBottom: 5
  },

  topButtonStyle: {
    width: "50%",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#ecf0f1",
    borderWidth: 2
  },

  topButtonActiveStyle: {
    backgroundColor: Config.primaryColor
  },

  topTextActiveStyle: {
    color: "white"
  }
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData
  };
}

export default connect(
  mapStateToProps,
  null
)(ProjectDetailInfo);
