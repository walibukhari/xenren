/* eslint-disable global-require */
////#CDCECF
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Picker,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Config from '../../../Config';

class Payment extends Component {
  static propTypes = {
    children: PropTypes.node,
    action: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      activePay: 'WeChat Pay',
    };
  }

  onSelection = (item) => {
    this.setState({ activePay: item.name });
    this.props.callbackShowOption(this.props.type, item.id)
  };

  renderRow({ item }) {
    const selected = this.state.activePay === item.name;
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 0, marginVertical: 0 }}
        // onPress={() => this.setState({ activePay: item.name })}>
        onPress={() => this.onSelection(item, this.props.type)}>

        <View style={styles.boxInsideStyle}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'column', margin: 5 }}>
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: 'Montserrat-Light',
                  color:
                    selected ? Config.primaryColor
                      : '#000',
                }}>
                {item.name}
              </Text>
            </View>
          </View>
          <View>
            <View
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor:
                  selected ? Config.primaryColor
                    : '#ebebeb',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Ionicon name="ios-checkmark" size={25} color="#fff" />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (

      <View style={{ marginTop: 0 }}>
        <Text style={styles.text5Style}>{this.props.action}</Text>
        <FlatList
          data={this.props.data}
          extraData={this.state}
          renderItem={rowData => this.renderRow(rowData)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 3,
    paddingVertical: 2,
    borderTopWidth: 1,
    borderColor: '#f1f1f1',
  },
  text5Style: {
    fontSize: 16,
    color: '#000',
    marginLeft: 10,
    marginTop: 5,
  },
  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 45,
    width: '100%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Payment;
