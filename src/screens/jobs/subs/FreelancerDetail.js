import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  TextInput
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Config from "../../../Config";
import { translate } from "../../../i18n";
import { connect } from "react-redux";
import HttpRequest from "../../../components/HttpRequest";

class FreelancerDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isJobApply: false
    };
  }

  thumbUpFreelancer = item => {
    item.item.thumb_status = 1;
    HttpRequest.freelancerThumbUP(this.props.userData.token, item.item.id)
      .then(response => {
        if (response.data.status === "success") {
          alert(response.data.message);
          this.props.callBackToParent(item);
        } else if (response.data.status === "Error") {
          alert(response.data.message);
        } else {
          // this.showAlert();
          alert("Error occurred. Please try again!");
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  thumbDownFreelancer = item => {
    item.item.thumb_status = 2;
    HttpRequest.freelancerThumbDown(this.props.userData.token, item.item.id)
      .then(response => {
        if (response.data.status === "OK") {
          this.props.callBackToParent(item);
          alert(response.data.message);
        } else if (response.data.status === "Error") {
          alert(response.data.message);
        } else {
          // this.showAlert();
          alert("Error occurred. Please try again!");
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  renderIcon = item => {
    if (item.item.thumb_status === 1) {
      return (
        <TouchableOpacity
          style={styles.iconStyle}
          onPress={() => {
            this.thumbDownFreelancer(item);
          }}
        >
          <Ionicons
            name="ios-thumbs-up"
            size={15}
            color="tomato"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          style={styles.iconStyle}
          onPress={() => {
            this.thumbUpFreelancer(item);
          }}
        >
          <Ionicons
            name="ios-thumbs-down"
            size={15}
            color="grey"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
      );
    }
  };

  renderSkill(skills) {
    return skills.map((item, index) => {
      return (
        <View
          key={index}
          style={{
            borderRadius: 15,
            borderColor: "#f1f1f1",
            borderWidth: 1,
            flexDirection: "column",
            alignItems: "center",
            marginRight: 5,
            padding: 3
          }}
        >
          <Text
            style={{
              paddingHorizontal: 5,
              backgroundColor: "transparent"
            }}
          >
            {item.id}
          </Text>
        </View>
      );
    });
  }

  calculateAge(birthday) {
    birthday = new Date(birthday);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  goToPortfolio = item => {
    this.props.navigation.navigate("OtherUserProfile", {
      user_id: item.user.id
    });
  };

  handleApplyJob = () => {
    this.setState({ isJobApply: true });
  };

  render() {
    const item = this.props.freelancerDetail.item;
    return (
      <View style={styles.rootStyle}>
        <View
          style={{
            marginVertical: 4
          }}
        >
          <View style={styles.boxInsideStyle}>
            <TouchableOpacity onPress={this.handleProjectDetail}>
              <View style={styles.topSideStyle}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 0
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    <View
                      style={{
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center",
                        width: 70,
                        height: 70
                      }}
                    >
                      <Image
                        source={{ uri: item.user.user_image }}
                        style={styles.avatarStyle}
                        resizeMode="cover"
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: "column",
                        marginLeft: 0,
                        alignItems: "flex-start"
                      }}
                    >
                      <Text style={styles.text1Style}>
                        {item.user.real_name}
                      </Text>
                      <Text style={styles.text3Style}>{item.user.title}</Text>
                      <Text style={styles.text5Style}>
                        {translate("last_login_time")} :{" "}
                        {item.user.last_login_at}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: "column",
                        marginLeft: 0
                      }}
                    >
                      {this.renderIcon(this.props.freelancerDetail)}
                    </View>
                  </View>
                </View>
              </View>
            </TouchableOpacity>

            <View style={styles.container}>
              <View style={styles.payHour}>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#727272"
                  }}
                >
                  {translate("age")}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#5ec329"
                  }}
                >
                  {/*{item.user.date_of_birth !== ""*/}
                    {/*? this.calculateAge(item.user.date_of_birth)*/}
                    {/*: "--"}*/}
                  {item.user.date_of_birth ? this.calculateAge(item.user.date_of_birth) : '--' }
                </Text>
              </View>
              <View style={styles.projectType}>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#727272"
                  }}
                >
                  {translate("experience_year")}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#5ec329"
                  }}
                >
                  {item.user.experience ? item.user.experience : '--' }
                </Text>
              </View>
              <View style={styles.projectType}>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#727272"
                  }}
                >
                  {translate("project_completed")}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "#5ec329"
                  }}
                >
                  {item.user.projectCompleted
                    ? item.user.projectCompleted
                    : "0"}
                  {}
                </Text>
              </View>
            </View>

            <View>
              <View style={styles.middleSideStyle}>
                <Text style={styles.textDescription} numberOfLines={5}>
                  {item.user.about_me}
                </Text>
              </View>
            </View>

            <TouchableOpacity onPress={() => this.goToPortfolio(item)}>
              <View style={styles.container}>
                <View style={styles.button1}>
                  <Text
                    style={{
                      fontSize: 13,
                      color: "#fff"
                    }}
                  >
                    {translate("CHECK_DETAIL")}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingVertical: 5,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },

  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.8,
    shadowRadius: 2
    // boxShadow: '0 1px 2px 0 rgba(0, 0, 0, 0.1)',
    //paddingHorizontal: 5,
  },

  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 5,
    paddingTop: 10,
    marginBottom: 0
  },

  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },

  bottomSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 10,
    borderColor: "#ecf0f1",
    borderTopWidth: 1
  },

  applyStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    alignItems: "center",
    marginTop: 5,
    paddingBottom: 5,
    borderColor: "#ecf0f1",
    borderBottomWidth: 1
  },

  avatarStyle: {
    width: 60,
    height: 60,
    borderRadius: 30
    //paddingTop: 10,
  },

  applyavatarStyle: {
    width: 25,
    height: 25,
    paddingTop: 10,
    resizeMode: "contain"
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    // top: -15,
    resizeMode: "contain",
    position: "absolute",
    right: 0
  },

  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },

  cardButtonTextStyle: {
    color: Config.primaryColor,
    fontSize: 12
  },

  text1Style: {
    flex: 1,
    color: "#404040",
    fontSize: 24
  },

  text2Style: {
    fontSize: 15,
    color: "#bdc3c7"
  },

  text3Style: {
    fontSize: 13,
    fontWeight: "200",
    color: "#727272"
  },

  text4Style: {
    fontSize: 13,
    fontWeight: "200",
    color: Config.primaryColor
  },

  text5Style: {
    fontSize: 11,
    color: "#727272"
  },

  text6Style: {
    fontSize: 16,
    color: "#bdc3c7"
  },

  starStyle: {
    flexDirection: "row",
    paddingTop: 8
  },

  iconStyle: {
    flexDirection: "row",
    paddingTop: 2,
    marginLeft: 25,
    right: 0
    //height:35,
  },

  textDescription: {
    fontSize: 16,
    color: "#808080",
    fontWeight: "400"
  },

  textHeader: {
    fontWeight: "Bold",
    fontSize: 18,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    maringTop: 15
  },

  textDownload: {
    fontWeight: "Italic",
    fontSize: 15,
    color: Config.primaryColor,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5
  },

  textHeaderDesc: {
    fontWeight: "Bold",
    fontSize: 18,
    marginBottom: 5
  },

  imageAttachment: {
    width: 80,
    height: 80
  },

  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    marginBottom: 10
  },

  button1: {
    backgroundColor: Config.primaryColor,
    borderWidth: 1,
    marginTop: 5,
    marginLeft: 8,
    //	marginRight:10,
    borderColor: Config.primaryColor,
    width: "95%",
    height: 42,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3
  },

  buttonPaytype: {
    backgroundColor: Config.primaryColor,
    borderWidth: 1,
    borderColor: "#ecf0f1",
    flex: 1,
    flexDirection: "row",
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 3
  },

  button2: {
    backgroundColor: Config.primaryColor,
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },

  payHour: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    //borderLeftWidth:1,
    borderColor: "#ecf0f1",
    width: "33.4%",
    height: 40,
    alignItems: "center",
    justifyContent: "flex-start"
  },

  projectType: {
    borderColor: "#ecf0f1",
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    width: "33.3%",
    flexDirection: "column",
    height: 40,
    alignItems: "center",
    justifyContent: "flex-start"
  },

  signUpButtonStyle: {
    fontWeight: "bold",
    color: Config.primaryColor
  },

  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: "#e6e6e6"
  },

  amountInputTitle: {
    color: "#979797",
    backgroundColor: "#f6f6f6",
    padding: 2
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },

  dialogBoxStyle: {
    width: 320,
    height: 510,
    backgroundColor: "#fff",
    borderRadius: 3
    //  alignItems: 'center',
    //  justifyContent: 'center',
  },

  fontC: {
    fontSize: 12,
    color: "#fff"
  },

  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: "95%",
    marginTop: 10,
    marginLeft: 7,
    paddingHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    color: "#fff",
    borderRadius: 5
  },

  contactStyle: {
    flexDirection: "row",
    height: 45,
    borderColor: "#ecf0f1",
    borderTopWidth: 1
    //backgroundColor: '#fff',
  },

  payTypeStyle: {
    flexDirection: "row",
    height: 45
    //backgroundColor: '#fff',
  },

  contactTextStyle: {
    fontSize: 18,
    color: "#555",
    marginLeft: 15,
    marginTop: 10,
    fontWeight: "bold"
    //backgroundColor: '#fff',
  },

  headerStyle: {
    flexDirection: "row",
    height: 70,
    borderColor: "#ecf0f1",
    borderTopWidth: 1,
    borderBottomWidth: 1
    //  backgroundColor: '#fff',
  },

  socialButtonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  socialIconStyle: {
    height: 25,
    width: 25,
    resizeMode: "contain",
    marginBottom: 5
  },

  onlineIndicatorStyle: {
    width: 14,
    height: 14,
    backgroundColor: Config.primaryColor,
    borderRadius: 7,
    position: "absolute",
    right: 7,
    bottom: 5,
    borderWidth: 1,
    borderColor: "#fff"
  }
};

const mapStateToProps = state => ({
  component: state.component,
  userData: state.auth.userData
});

export default connect(mapStateToProps)(FreelancerDetail);
