/* eslint-disable consistent-return */
/* eslint-disable no-dupe-keys */
/* eslint-disable global-require */
/* eslint-disable class-methods-use-this */
/* eslint-disable object-shorthand */
/* eslint-disable react/jsx-key */
/* eslint-disable max-len */
/* eslint-disable no-else-return */
/* eslint-disable prefer-destructuring */
/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */
/* eslint-disable semi */
/* eslint-disable array-callback-return */
/* eslint-disable prefer-const */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */

import React, { Component } from "react";
import {
	View,
	Text,
	Image,
	TouchableOpacity,
	TextInput,
	Modal,
	Dimensions,
	Alert,
	ScrollView,
	Clipboard
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";
import { Rating } from "react-native-elements";
import Config from "../../../Config";
import { translate } from "../../../i18n";
import HttpRequest from "../../../components/HttpRequest";

const { width, height } = Dimensions.get("window");

class Review extends Component {
	constructor(props) {
		super(props);

		this.state = {
			item: this.props.review,
			hasAttachments: false,
			isMakeOffer: false,
			locale: this.props.navigation.state.params.locale,
			offerJob: null,
			payTypeActiveTabIndex: 0,
			questions: [],
			aboutMe: "",
		};
	}

	componentDidMount() {
		let arr = [];
		if (this.props.review.item.project_questions !== null) {
			this.props.review.item.project_questions.map((item) => {
				arr.push({
					question: item.question,
					answer: "",
					id: item.id,
				});
			});
		}
		this.setState({
			questions: [...this.state.questions, ...arr],
		});
	}

	onDislikeProject = (project) => {
		project.item.thumb_down = {
			project_id: project.item.id,
			user_id: project.item.user_id,
		};
		const data = {
			projectId: project.item.id,
			projectType: this.props.projectType,
		};

		HttpRequest.dislikeProject(this.props.userData.token, data)
			.then((response) => {
				if (response.data.status === "success") {
					alert(response.data.message);
					this.props.callbackToParent(project);
				} else if (response.data.status === "error") {
					// alert(response.data.message);
				} else {
					// this.showAlert();
				}
			})
			.catch((error) => {
				alert(error);
			});
	};

	onFavouriteProject = (project) => {
		project.item.favorite_job = {
			project_id: project.item.id,
			user_id: project.item.user_id,
		};
		HttpRequest.addFavouriteProject(
			this.props.userData.token,
			this.props.projectType,
			project.item.id,
			)
			.then((response) => {
				if (response.data.status === "OK") {
					this.props.callbackToParent(project);
					alert(response.data.message);
				} else if (response.data.status === "Error") {
					alert(response.data.message);
				} else {
					// this.showAlert();
				}
			})
			.catch((error) => {
				alert(error);
			});
	};

	joinDiscussionAPI(job) {
		const token = this.props.userData.token;
		HttpRequest.getDiscussRoomChat(token, job.item.id)
			.then((response) => {
				if (response.data.status === "success") {
					this.props.navigation.navigate("ProjectDetail", {
						projectDetail: response.data,
						job: job,
					});
				}
			})
			.catch((error) => {
				this.setState({
					isLoading: false,
				});
			});
	}

	onRemove = (item, index) => {
		let tmpList = this.state.item;
		tmpList.item.down = false;
		this.setState({ item: tmpList });
		this.onDislikeProject(item);
	};

	onAdd = (item, index) => {
		let tmpList = this.state.item;
		tmpList.item.down = true;
		this.setState({ item: tmpList });
		this.onDislikeProject(item);
	};

	onRemoveFav = (item, index) => {
		let tmpList = this.state.item;
		tmpList.item.fav = false;
		this.setState({ item: tmpList });
		this.onFavouriteProject(item)
	};

	onAddFav = (item, index) => {
		let tmpList = this.state.item;
		tmpList.item.fav = true;
		this.setState({ item: tmpList });
		this.onFavouriteProject(item)
	};

	renderIcon = (item, index) => {
		if (item.item.down === true) {
			return (
        <TouchableOpacity
			key={index}
            style={styles.iconStyle}
            onPress={() => { this.onRemove(item, index); }}
        >
          <Ionicons
            name="ios-thumbs-down"
            size={15}
            color="tomato"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
			);
		} else {
			return (
        <TouchableOpacity
          style={styles.iconStyle}
		  key={index}
          onPress={() => {
						this.onAdd(item, index);
					}}
        >
          <Ionicons
            name="ios-thumbs-down"
            size={15}
            color="grey"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
			);
		}
	};

	renderIcon1 = (item, index) => {
		if (item.item.fav === true) {
			return (
        <TouchableOpacity
			style={styles.iconStyle1}
			onPress={() => { this.onRemoveFav(item, index); }}
		>
          <Ionicons
            name="ios-heart"
            size={15}
            color="tomato"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
			);
		} else {
			return (
        <TouchableOpacity
          style={styles.iconStyle1}
          onPress={() => {
						this.onAddFav(item, index);
					}}
        >
          <Ionicons
            name="ios-heart"
            size={15}
            color="grey"
            style={{ marginLeft: 0 }}
          />
        </TouchableOpacity>
			);
		}
	};

	renderSkill = skills =>
		skills.map((item, index) => (
      <View
        key={index}
        style={{
					borderRadius: 15,
					borderColor: "#f3f3f3",
					borderWidth: 1,
					flexDirection: "column",
					alignItems: "center",
					marginRight: 5,
					padding: 3,
				}}
      >
        <Text
          style={{
						paddingHorizontal: 5,
						backgroundColor: "transparent",
					}}
        >
					{this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.skill.name_cn : item.skill.name_en}
        </Text>
      </View>
		));

	handleProjectDetail = (item) => {
		this.joinDiscussionAPI(item);
	};

	renderAttachments(attachments) {
		return attachments.map((item) => {
			const type = item.type;
			let imageSource = "";
			if (type === "picture") {
				imageSource = item.source;
			} else {
				imageSource = require("../../../../images/account_profile/doc_icon.png");
			}

			return (
        <View
          style={{
						borderColor: "#f1f1f1",
						borderWidth: 1,
						flexDirection: "row",
						alignItems: "center",
						marginRight: 10,
					}}
        >
          <Image source={imageSource} style={styles.imageAttachment} />
        </View>
			);
		});
	}

	onJoinDiscussPressed = (item) => {
		this.props.navigation.navigate("ProjectDetail", { project: item, locale: this.state.locale });
	};

	writeToClipboard = async (item, string) => {
		if (item === null) {
			alert(`${string} Not Found`);
		} else {
			await Clipboard.setString(item);
			alert(`${string} Copied to Clipboard!`);
		}
	};

	onQqPress = (item) => {
		this.writeToClipboard(item, "QQ Id");
	};

	onWeChatPress = (item) => {
		this.writeToClipboard(item, "WeChat Id");
	};

	onSkypePress = (item) => {
		this.writeToClipboard(item, "Skype Id");
	};

	onPhonePress = (item) => {
		this.writeToClipboard(item, "Phone");
	};

	onLayout = (e) => {
		// this.setState({
		//   width: e.nativeEvent.layout.width,
		//   height: e.nativeEvent.layout.height,
		//   x: e.nativeEvent.layout.x,
		//   y: e.nativeEvent.layout.y
		// })
	};

	render() {
		const item = this.props.review;
		const rating = Number(this.props.review.item.creator.total_rating);
		return (
      <View style={styles.rootStyle}>
        <View
          style={{
						marginHorizontal: 0,
						marginVertical: 5,
					}}
        >
          <View style={styles.boxInsideStyle}>
            <View>
              <View style={styles.topSideStyle}>
                <View
                  style={{
										flex: 1,
										flexDirection: "column",
										marginLeft: 5,
									}}
                >
                  <View style={{ flexDirection: "row" }}>
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={styles.text1Style}
                    >
											{item.item.name}
                    </Text>
                  </View>
                  <Text style={styles.text3Style}>
										{item.item.pay_type === 1 ? translate('pay_hourly') : translate('pay_fixed')} -{" "}
                    <Text style={styles.text4Style}>
                      ${item.item.reference_price}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>

            <View>
              <View style={{ height: 100, padding: 10 }}>
								{
                  <ScrollView >
                    <Text style={styles.textDescription}
                          numberOfLines={5}>{item.item.description.replace(/[^a-zA-Z 0-9]+/g,'').replace('nbsp', '')/*.substring(0, 80)*/}
                    </Text>
                  </ScrollView>
								}
              </View>

              <View style={{ marginTop: 5 }}>
                <View style={styles.middleSideStyle}>
									{this.renderSkill(item.item.project_skills)}
                </View>
              </View>
            </View>

            <View style={styles.bottomSideStyle}>
              <Image
                source={{
									uri: `${item.item.creator.img_avatar}`,
								}}
                style={styles.avatarStyle}
              />
              <View
                style={{
									flex: 1,
									flexDirection: "column",
									marginLeft: 10,
									paddingVertical: 5,
								}}
              >
                <Text style={styles.text5Style}>
									{item.item.creator.real_name}
                </Text>
                <Text style={styles.text5Style}>{item.item.updated_at}</Text>
              </View>
              <View
                style={{
									flex: 1,
									flexDirection: "column",
									marginLeft: 10,
								}}
              >
                <View
                  style={{
										flex: 1,
										flexDirection: "row",
										marginLeft: 0,
									}}
                >
                  <View
                    style={{
											flexDirection: "column",
											width: 30,
											alignItems: "center",
											justifyContent: "center",
											marginLeft: 1,
											borderColor: "#f7f7f7",
											borderLeftWidth: 1,
										}}
                  >
										{this.renderIcon(item)}
                  </View>
                  <View
                    style={{
											flexDirection: "column",
											width: 30,
											alignItems: "center",
											justifyContent: "center",
											marginLeft: 1,
											borderColor: "#f7f7f7",
											borderLeftWidth: 1,
										}}
                  >
										{this.renderIcon1(item)}
                  </View>
                  <View
                    style={{
											width: 80,
											flexDirection: "column",
											justifyContent: "center",
											alignItems: "center",
											marginLeft: 1,
											borderColor: "#f7f7f7",
											borderLeftWidth: 1,
										}}
                  >
                    <Rating
                      type="star"
                      startingValue={rating}
                      readonly
                      imageSize={15}
                      style={{ alignContent: "flex-start" }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.container}>
              <TouchableOpacity
                onPress={() => this.onJoinDiscussPressed(item.item)}
                style={styles.button1}
              >
                <Text
                  style={{
										fontSize: 13,
										color: Config.primaryColor,
									}}
                >
									{translate("JOIN_DISCUSS")}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
									this.onMakeOfferPressed(item);
								}}
                style={styles.button2}
              >
                <Text
                  style={{
										fontSize: 13,
										color: "white",
									}}
                >
									{translate("MAKE_OFFER")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

				{/* Make Offer Modal */}
        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
						this.setState({
							isMakeOffer: false,
						});
					}}
          visible={this.state.isMakeOffer}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <View style={styles.applyStyle}>
                <Image
                  source={require("../../../../images/jobs/applyjob.png")}
                  style={styles.applyavatarStyle}
                />
                <View
                  style={{
										flex: 1,
										flexDirection: "column",
										marginLeft: 10,
									}}
                >
                  <Text style={styles.text6Style}>
										{translate("apply_job")}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
										flex: 1,
										flexDirection: "column",
										marginTop: 5,
									}}
                  onPress={() => this.setState({ isMakeOffer: false })}
                >
                  <View
                    style={{
											flex: 1,
											flexDirection: "column",
											marginTop: 0,
										}}
                  >
                    <Image
                      source={require("../../../../images/jobs/cross.png")}
                      style={styles.avatarStyle1}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <ScrollView
                style={{ width: "100%" }}
                contentContainerStyle={{
									justifyContent: "center",
									alignItems: "center",
								}}
              >
                <View
                  style={{
										width: "95%",
										marginTop: 5,
										paddingHorizontal: 0,
									}}
                >
                  <View style={styles.amountInputWrapper}>
                    <Text style={styles.amountInputTitle}>
											{translate("price")} ($)
                    </Text>
                    <TextInput
                      underlineColorAndroid="transparent"
                      placeholder="XXXX"
					  keyboardType='numeric'
                    />
                  </View>
									{this.renderQuestion()}
                  <View style={styles.amountInputWrapper}>
                    <Text style={styles.amountInputTitle}>
											{translate("about")}
                    </Text>
                    <TextInput
                      multiline={true}
                      underlineColorAndroid="transparent"
                      placeholder={translate('please_write')}
                      onChangeText={aboutMe => this.setState({ aboutMe })}
                      value={this.state.aboutMe}
                    />
                  </View>
                </View>
                <View style={styles.payTypeStyle}>
                  <View style={styles.contactStyle}>
                    <Text style={styles.contactTextStyle}>
											{translate("PAY_TYPE")}
                    </Text>
                  </View>
                  <View
                    style={{
											flexDirection: "row",
											width: "85%",
											height: 50,
											justifyContent: "flex-start",
											alignItems: "center",
											marginLeft: 15,
											marginRight: 15,
										}}
                  >
                    <View
                      style={{
												flexDirection: "row",
												justifyContent: "flex-start",
												alignItems: "center",
												width: "60%",
												height: 50,
												marginRight: 5,
											}}
                    >
                      <TouchableOpacity
                        onPress={() => {
													this.setState({ payTypeActiveTabIndex: 0 });
												}}
                        style={[
													styles.topButtonStyle,
													this.state.payTypeActiveTabIndex === 0
														? styles.topButtonActiveStyle
														: {},
												]}
                      >
                        <Text
                          style={
														this.state.payTypeActiveTabIndex === 0
															? styles.topTextActiveStyle
															: {}
													}
                        >
													{translate("pay_hourly")}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => {
													this.setState({ payTypeActiveTabIndex: 1 });
												}}
                        style={[
													styles.topButtonStyle,
													this.state.payTypeActiveTabIndex === 1
														? styles.topButtonActiveStyle
														: {},
												]}
                      >
                        <Text
                          style={
														this.state.payTypeActiveTabIndex === 1
															? styles.topTextActiveStyle
															: {}
													}
                        >
                          Fixed
                        </Text>
                      </TouchableOpacity>
                    </View>
										{/* {this.state.payTypeActiveTabIndex == 0 && */}
										{/* <View style={{ */}
										{/* marginLeft: 15, */}
										{/* width: '40%', */}
										{/* height: 40, */}
										{/* flexDirection: 'row', */}
										{/* alignItems: 'center', */}
										{/* borderColor: '#F6F6F6', */}
										{/* borderWidth: 2, */}
										{/* }}> */}
										{/* <TextInput style={{ */}
										{/* height: 40, */}
										{/* width: '100%', */}
										{/* }} */}
										{/* underlineColorAndroid="transparent" */}
										{/* placeholder="$ 0.0 " */}
										{/* multiline={true} */}
										{/* /> */}
										{/* </View> */}
										{/* } */}
                  </View>
                </View>

								{/* Daily Updates */}
                <View
                  style={{
										width: "100%",
										height: 60,
										marginTop: 5,
										marginBottom: 5,
										justifyContent: "center",
										alignItems: "center",
									}}
                >
                  <View style={styles.contactStyle}>
                    <Text style={styles.contactTextStyle}>{translate('updates_daily')}</Text>
                  </View>
                  <View
                    style={{
											width: "90%",
											flexDirection: "row",
											justifyContent: "space-between",
											alignItems: "center",
											marginBottom: 10,
										}}
                  >
                    <View
                      style={{
												width: "50%",
												flexDirection: "row",
												justifyContent: "flex-start",
												alignItems: "center",
											}}
                    >
                      <Text
                        style={{
													fontSize: 18,
													fontWeight: "400",
												}}
                      >
						  {translate('yes')}
                      </Text>
                      <TouchableOpacity
                        style={{
													marginLeft: 10,
													height: 28,
													width: 28,
													borderRadius: 14,
													justifyContent: "center",
													alignItems: "center",
													backgroundColor:
														this.state.selected === 1
															? Config.primaryColor
															: "#ebebeb",
												}}
                        onPress={() => {
													this.dailyUpdates(1);
												}}
                      >
                        <Ionicons name="ios-checkmark" size={25} color="#fff" />
                      </TouchableOpacity>
                    </View>
                    <View
                      style={{
												width: "50%",
												flexDirection: "row",
												justifyContent: "flex-start",
												alignItems: "center",
											}}
                    >
                      <Text
                        style={{
													fontSize: 18,
													fontWeight: "400",
												}}
                      >
						  {translate('no')}
                      </Text>
                      <TouchableOpacity
                        style={{
													marginLeft: 10,
													height: 28,
													width: 28,
													borderRadius: 14,
													justifyContent: "center",
													alignItems: "center",
													backgroundColor:
														this.state.selected === 2
															? Config.primaryColor
															: "#ebebeb",
												}}
                        onPress={() => {
													this.dailyUpdates(2);
												}}
                      >
                        <Ionicons name="ios-checkmark" size={25} color="#fff" />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={styles.contactStyle}>
                  <Text style={styles.contactTextStyle}>
										{translate("contact")}
                  </Text>
                </View>

								{this.state.offerJob !== null && (
                  <View style={styles.headerStyle}>
                    <TouchableOpacity
                      style={styles.socialButtonStyle}
                      onPress={() => {
												this.onQqPress(this.state.offerJob.creator.qq_id);
											}}
                    >
                      <Image
                        source={require("./../../../../images/private_chat/icon_qq.png")}
                        style={styles.socialIconStyle}
                      />
                      <Text
                        style={{
													fontSize: 12,
													color: "#000",
												}}
                      >
												{translate("qq")}
                      </Text>
                      <Text
                        style={{
													fontSize: 10,
													color: "#555",
													textAlign: "center",
												}}
                      >
												{this.state.offerJob.creator.qq_id !== null
													? this.state.offerJob.creator.qq_id
													: ""}
                      </Text>
                    </TouchableOpacity>
                    <View
                      style={{
												width: 1,
												backgroundColor: "#f7f7f7",
											}}
                    />
                    <TouchableOpacity
                      style={styles.socialButtonStyle}
                      onPress={() => {
												this.onSkypePress(this.state.offerJob.creator.skype_id);
											}}
                    >
                      <Image
                        source={require("./../../../../images/private_chat/icon_skype.png")}
                        style={styles.socialIconStyle}
                      />
                      <Text
                        style={{
													fontSize: 12,
													color: "#000",
												}}
                      >
												{translate("skype")}
                      </Text>
                      <Text
                        style={{
													fontSize: 10,
													color: "#555",
													textAlign: "center",
												}}
                      >
												{this.state.offerJob.creator.skype_id !== null
													? this.state.offerJob.creator.skype_id
													: ""}
                      </Text>
                    </TouchableOpacity>
                    <View
                      style={{
												width: 1,
												backgroundColor: "#f7f7f7",
											}}
                    />
                    <TouchableOpacity
                      style={styles.socialButtonStyle}
                      onPress={() => {
												this.onWeChatPress(this.state.offerJob.creator.wechat_id);
											}}
                    >
                      <Image
                        source={require("./../../../../images/private_chat/icon_wechat.png")}
                        style={styles.socialIconStyle}
                      />
                      <Text
                        style={{
													fontSize: 12,
													color: "#000",
												}}
                      >
												{translate("wechat")}
                      </Text>
                      <Text
                        style={{
													fontSize: 10,
													color: "#555",
													textAlign: "center",
												}}
                      >
												{this.state.offerJob.creator.wechat_id !== null
													? this.state.offerJob.creator.wechat_id
													: ""}
                      </Text>
                    </TouchableOpacity>
                    <View
                      style={{
												width: 1,
												backgroundColor: "#f7f7f7",
											}}
                    />
                    <TouchableOpacity
                      style={styles.socialButtonStyle}
                      onPress={() => {
												this.onPhonePress(this.state.offerJob.creator.handphone_no);
											}}
                    >
                      <Image
                        source={require("./../../../../images/private_chat/icon_phone.png")}
                        style={styles.socialIconStyle}
                      />
                      <Text
                        style={{
													fontSize: 12,
													color: "#000",
												}}
                      >
												{translate("phone")}
                      </Text>
                      <Text
                        style={{
													fontSize: 10,
													color: "#555",
													textAlign: "center",
												}}
                      >
												{this.state.offerJob.creator.handphone_no !== null
													? this.state.offerJob.creator.handphone_no
													: ""}
                      </Text>
                    </TouchableOpacity>
                  </View>
								)}
              </ScrollView>
              <TouchableOpacity
                onPress={() => this.makeOffer()}
                style={styles.buttonWrapper}
              >
                <Text
                  style={{
										color: "white",
										fontWeight: "400",
										fontSize: 16,
										textAlign: "center",
									}}
                >
									{translate("MAKE_OFFER")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
		);
	}

	renderQuestion = () => {
		if (this.state.offerJob !== null) {
			return this.state.offerJob.project_questions.map((item, index) => (
        <View key={index} style={styles.amountInputWrapper}>
          <Text style={styles.amountInputTitle}>
              {translate('question')}{' '}{index + 1}: {item.question}
          </Text>
          <TextInput
            multiline={true}
            underlineColorAndroid="transparent"
            placeholder={translate('your_answer')}
          />
        </View>
			));
		}
	};

	onMakeOfferPressed = (item) => {
		this.setState({
			isMakeOffer: true,
			offerJob: item.item,
		});
	};

	dailyUpdates = (value) => {
		this.setState({ selected: value });
	};

	makeOffer = () => {
		let questIds = [];
		let answers = [];
		this.state.questions.map((item) => {
			questIds.push(item.id);
			answers.push(item.answer);
		});
		let body = {
			price: this.state.item.item.reference_price,
			project_id: this.state.item.item.id,
			inbox_id: "0",
			about_me: this.state.aboutMe,
			pay_type: "1",
			daily_update: this.state.selected,
			questionId: questIds,
			answer: answers,
		};
		if (this.state.item.item.creator.wechat_id !== null) {
			body.wechat_id = this.state.item.item.creator.wechat_id;
		}
		if (this.state.item.item.creator.skype_id !== null) {
			body.skype_id = this.state.item.item.creator.skype_id;
		}
		if (this.state.item.item.creator.qq_id !== null) {
			body.qq_id = this.state.item.item.creator.qq_id;
		}
		if (this.state.item.item.creator.handphone_no !== null) {
			body.handphone_no = this.state.item.item.creator.handphone_no;
		}
		HttpRequest.makeOffer(this.props.userData.token, body)
			.then((response) => {
				if (response.data.status === "success") {
					alert(response.data.msg);
					this.setState({ isMakeOffer: false });
				} else {
					alert(response.data.msg);
				}
			})
			.catch((error) => {
				alert("Error occurred. Please try again!");
			});
	};
}

const styles = {
	rootStyle: {
		paddingHorizontal: 0,
		paddingVertical: 0,
		flex: 1,
		flexDirection: "column",
		backgroundColor: "#f7f7f7",
	},

	topButtonStyle: {
		width: "50%",
		height: 40,
		justifyContent: "center",
		alignItems: "center",
		borderColor: "#f7f7f7",
		borderWidth: 2,
	},

	topButtonActiveStyle: {
		backgroundColor: Config.primaryColor,
	},

	topTextActiveStyle: {
		color: "white",
	},

	boxInsideStyle: {
		flexDirection: "column",
		backgroundColor: "#fff",
		borderBottomLeftRadius: 5,
		borderBottomRightRadius: 5,
	},

	topSideStyle: {
		flexDirection: "row",
		paddingHorizontal: 10,
		paddingTop: 10,
		marginBottom: 0,
	},

	middleSideStyle: {
		flexDirection: "row",
		flexWrap: "wrap",
		// flex: 1,
		paddingHorizontal: 10,
	},

	bottomSideStyle: {
		flexDirection: "row",
		paddingHorizontal: 10,
		alignItems: "center",
		marginTop: 10,
		borderColor: "#f7f7f7",
		borderTopWidth: 1,
	},

	avatarStyle: {
		width: 30,
		height: 30,
		borderRadius: 15,
		paddingTop: 10,
	},

	cardButtonStyle: {
		flex: 1,
		height: 30,
		alignItems: "center",
		justifyContent: "center",
		flexDirection: "row",
	},

	cardButtonTextStyle: {
		color: Config.primaryColor,
		fontSize: 12,
	},

	text1Style: {
		flex: 1,
		color: "black",
		fontSize: 24,
	},

	text2Style: {
		fontSize: 15,
		color: "#808283",
	},

	text3Style: {
		fontSize: 13,
		fontWeight: "200",
		color: "#808283",
	},

	text4Style: {
		fontSize: 13,
		fontWeight: "200",
		color: Config.primaryColor,
	},

	text5Style: {
		fontSize: 12,
		color: "#808283",
	},
	starStyle: {
		flexDirection: "row",
		paddingTop: 8,
	},

	iconStyle: {
		flexDirection: "row",
		paddingTop: 8,
		height: 35,
	},

	iconStyle1: {
		flexDirection: "row",
		paddingTop: 8,
		height: 35,
	},
	textDescription: {
		fontSize: 18,
		color: "grey",
		lineHeight: 35,
		fontWeight: "normal",
		paddingLeft: 5,
	},

	textHeader: {
		fontWeight: "bold",
		fontSize: 20,
		paddingHorizontal: 10,
		paddingVertical: 5,
		marginBottom: 5,
	},

	imageAttachment: {
		width: 80,
		height: 80,
	},

	container: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "space-between",
	},

	button1: {
		backgroundColor: "#fff",
		borderWidth: 1,
		borderColor: Config.primaryColor,
		borderBottomLeftRadius: 5,
		width: "50%",
		height: 40,
		alignItems: "center",
		justifyContent: "center",
	},

	button2: {
		backgroundColor: Config.primaryColor,
		borderBottomRightRadius: 5,
		width: "50%",
		height: 40,
		alignItems: "center",
		justifyContent: "center",
	},

	signUpButtonStyle: {
		fontWeight: "bold",
		color: Config.primaryColor,
	},

	applyStyle: {
		flexDirection: "row",
		paddingHorizontal: 10,
		alignItems: "center",
		marginTop: 5,
		paddingBottom: 5,
		borderColor: "#f7f7f7",
		borderBottomWidth: 1,
	},

	dialogStyle: {
		flex: 1,
		backgroundColor: "rgba(44, 62, 80, 0.6)",
		alignItems: "center",
		justifyContent: "center",
	},

	dialogBoxStyle: {
		width: width * 0.85,
		height: height * 0.65,
		backgroundColor: "#fff",
		borderRadius: 3,
	},

	applyavatarStyle: {
		width: 25,
		height: 25,
		paddingTop: 10,
		resizeMode: "contain",
	},

	avatarStyle1: {
		width: 10,
		height: 10,
		resizeMode: "contain",
		position: "absolute",
		right: 0,
	},

	text6Style: {
		fontSize: 16,
		color: "#808283",
	},

	amountInputWrapper: {
		margin: 5,
		borderWidth: 1,
		borderColor: "#e6e6e6",
	},

	amountInputTitle: {
		color: "#979797",
		backgroundColor: "#f6f6f6",
		padding: 2,
	},

	payTypeStyle: {
		// flexDirection: 'row',
		width: "100%",
		height: 100,
		// paddingTop: 10,
		justifyContent: "center",
		alignItems: "flex-start",
		// justifyContent:'space-between',
		// alignItems:'center',
	},

	contactTextStyle: {
		fontSize: 17,
		color: "#555",
		fontWeight: "bold",
		textAlign: "center",
		paddingLeft: 10,
	},

	buttonPaytype: {
		backgroundColor: Config.primaryColor,
		borderWidth: 1,
		borderColor: "#f7f7f7",
		flex: 1,
		flexDirection: "row",
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 5,
		alignItems: "center",
		justifyContent: "center",
		borderRadius: 3,
	},

	contactStyle: {
		flexDirection: "row",
		width: "95%",
		height: 45,
		borderColor: "#f7f7f7",
		borderTopWidth: 1,
		justifyContent: "flex-start",
		alignItems: "center",
	},

	headerStyle: {
		flexDirection: "row",
		// height: 70,
		borderColor: "#f7f7f7",
		borderTopWidth: 1,
		borderBottomWidth: 1,
	},

	socialButtonStyle: {
		flex: 1,
		justifyContent: "flex-start",
		alignItems: "center",
		paddingVertical: 3,
	},

	socialIconStyle: {
		height: 25,
		width: 25,
		resizeMode: "contain",
		marginBottom: 5,
	},

	buttonWrapper: {
		backgroundColor: Config.primaryColor,
		height: 40,
		width: "95%",
		marginTop: 10,
		marginLeft: 7,
		marginBottom: 10,
		paddingHorizontal: 10,
		justifyContent: "center",
		alignItems: "center",
		// color: '#fff',
		borderRadius: 5,
	},
};

const mapStateToProps = state => ({
	userData: state.auth.userData,
});

export default connect(
	mapStateToProps,
	null,
)(Review);
// export default Review;
