/* eslint-disable global-require */
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  StyleSheet,
  View,
  Picker,
  FlatList,
  TouchableOpacity
} from "react-native";
import Config from "../../../Config";
import { translate } from "../../../i18n";
import { Dialog } from "../../../components/Dialog/index";

class FilterModel extends Component {
  static propTypes = {
    children: PropTypes.node,
    action: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      paymentGateways: [],
      activePay: "WeChat Pay",
      language: "",
      country: "",
      hourly_rate: "",
      languageTxt: "",
      countryTxt: "",
      hourly_rateTxt: "",
      dialogLanguageVisible: false,
      dialogRateVisible: false,
      dialogCountryVisible: false
    };
  }

  callbackToParent = (lang, item) => {
    this.setState({
      dialogLanguageVisible: false,
      dialogRateVisible: false,
      dialogCountryVisible: false
    });
    if (lang === "language") {
      this.setState({ language: item.id, languageTxt: item.name });
    } else if (lang === "country") {
      this.setState({ country: item.id, countryTxt: item.name });
    } else {
      this.setState({ hourly_rate: item.id, hourly_rateTxt: item.name });
    }
    this.props.callbackDropDown({ type: lang, value: item.id });
  };

  render() {
    return (
      <View>
        <View style={{ marginTop: 5, paddingHorizontal: 5 }}>
          <View style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}>
              {" "}
              {translate("LANGUAGE")}
            </Text>
            <TouchableOpacity
              onPress={() => this.setState({ dialogLanguageVisible: true })}
            >
              <Text style={{ padding: 5, marginBottom: 6 }}>
                {this.state.languageTxt ? this.state.languageTxt : "Select"}
              </Text>
            </TouchableOpacity>

            <Dialog
              visible={this.state.dialogLanguageVisible}
              title={translate("LANGUAGE")}
              onTouchOutside={() =>
                this.setState({ dialogLanguageVisible: false })
              }
            >
              <FlatList
                style={{ height: "80%" }}
                data={this.props.languages.data}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.callbackToParent("language", item)}
                  >
                    <Text style={{ padding: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </Dialog>
          </View>

          <View style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}>
              {" "}
              {translate("HOURLY_RATE")}
            </Text>

            <TouchableOpacity
              onPress={() => this.setState({ dialogRateVisible: true })}
            >
              <Text style={{ padding: 5, marginBottom: 6 }}>
                {this.state.hourly_rateTxt
                  ? this.state.hourly_rateTxt
                  : "Select"}
              </Text>
            </TouchableOpacity>

            <Dialog
              visible={this.state.dialogRateVisible}
              title={translate("HOURLY_RATE")}
              onTouchOutside={() => this.setState({ dialogRateVisible: false })}
            >
              <FlatList
                data={this.props.languages.data}
                data={[
                  { name: "1 - 10 USD", id: "1" },
                  { name: "11 - 20 USD", id: "2" },
                  { name: "21 - 30 USD", id: "3" },
                  { name: "31 - 40 USD", id: "4" },
                  { name: "41 - 50 USD", id: "5" },
                  { name: "51+USD", id: "6" }
                ]}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.callbackToParent("hourly_rate", item)}
                  >
                    <Text style={{ padding: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </Dialog>
          </View>

          <View style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}> {translate("COUNTRY")}</Text>

            <TouchableOpacity
              onPress={() => this.setState({ dialogCountryVisible: true })}
            >
              <Text style={{ padding: 5, marginBottom: 6 }}>
                {this.state.countryTxt ? this.state.countryTxt : "Select"}
              </Text>
            </TouchableOpacity>

            <Dialog
              visible={this.state.dialogCountryVisible}
              title={translate("COUNTRY")}
              onTouchOutside={() =>
                this.setState({ dialogCountryVisible: false })
              }
            >
              <FlatList
                style={{ height: "80%" }}
                data={this.props.countries}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    key={item.id}
                    onPress={() => this.callbackToParent("country", item)}
                  >
                    <Text style={{ padding: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </Dialog>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: "80%",
    marginTop: 10,
    color: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  rootStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: "#fff"
  },
  topWrapperStyle: {
    backgroundColor: "#f4f4f4",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20
  },
  normalTopWrapper: {
    backgroundColor: "#5fc229",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: "Montserrat-Medium",
    fontSize: 32,
    color: "#000"
  },
  priceDescription: {
    fontFamily: "Montserrat-Light",
    fontSize: 20,
    color: "#5c5c5c",
    marginTop: 5
  },
  headerText: {
    color: "#fff",
    fontSize: 16
  },
  balanceText: {
    color: "#fff",
    fontSize: 24
  },
  amountInputWrapper: {
    marginBottom: 6,
    borderWidth: 1,
    borderColor: "#e6e6e6"
  },
  amountInputTitle: {
    color: "#979797",
    backgroundColor: "#f6f6f6",
    padding: 1,
    fontSize: 10
  }
});

export default FilterModel;
