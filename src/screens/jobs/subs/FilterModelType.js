/* eslint-disable global-require */
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  TextInput,
  Picker,
} from "react-native";
import Ionicon from "react-native-vector-icons/Ionicons";
import Config from "../../../Config";
import { translate } from "../../../i18n";
import HttpRequest from "../../../components/HttpRequest";

import { Dialog } from "../../../components/Dialog/index";

class FilterModelType extends Component {
  static propTypes = {
    children: PropTypes.node,
    action: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      paymentGateways: [],
      activePay: "WeChat Pay",
      languages: [],
      dialogLanguageVisible: false,
      dialogCountryVisible: false,
      languageTxt:'',
      countryTxt:''
    };
  }

  renderRow = item => {
    return (
      <TouchableOpacity>
        {" "}
        <Text>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View>
        <View style={{ marginTop: 5, paddingHorizontal: 5 }}>
          <View style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}>
              {" "}
              {translate("language")}
            </Text>

            <TouchableOpacity
              onPress={() => this.setState({ dialogLanguageVisible: true })}
            >
              <Text style={{ padding: 5, marginBottom: 6 }}>
                {this.state.languageTxt ? this.state.languageTxt : "Select"}
              </Text>
            </TouchableOpacity>

            <Dialog
              visible={this.state.dialogLanguageVisible}
              title={translate("language")}
              onTouchOutside={() =>
                this.setState({ dialogLanguageVisible: false })
              }
            >
              <FlatList
                style={{ height: "80%" }}
                data={this.props.languages.data}
                renderItem={({ item }) => (
                  <TouchableOpacity
                    onPress={() => this.setState({ language: item.code ,languageTxt:item.name,dialogLanguageVisible: false})}
                  >
                    <Text style={{ padding: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </Dialog>
          </View>

          <View style={styles.amountInputWrapper}>
            <Text style={styles.amountInputTitle}> {translate("country")}</Text>

            
            <TouchableOpacity
              onPress={() => this.setState({ dialogCountryVisible: true })}
            >
              <Text style={{ padding: 5, marginBottom: 6 }}>
                {this.state.countryTxt ? this.state.countryTxt : "Select"}
              </Text>
            </TouchableOpacity>

            <Dialog
              visible={this.state.dialogCountryVisible}
              title={translate("country")}
              onTouchOutside={() =>
                this.setState({ dialogCountryVisible: false })
              }
            >
              <FlatList
                style={{ height: "80%" }}
                data={this.props.countries}
                renderItem={({ item }) => (
                  <TouchableOpacity
                  key={item.id}
                    onPress={() => this.setState({ country: item.id ,countryTxt:item.name,dialogCountryVisible:false})}
                  >
                    <Text style={{ padding: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              />
            </Dialog>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: "80%",
    marginTop: 10,
    color: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  rootStyle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    backgroundColor: "#fff"
  },
  topWrapperStyle: {
    backgroundColor: "#f4f4f4",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20
  },
  normalTopWrapper: {
    backgroundColor: "#5fc229",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 20
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: "Montserrat-Medium",
    fontSize: 32,
    color: "#000"
  },
  priceDescription: {
    fontFamily: "Montserrat-Light",
    fontSize: 20,
    color: "#5c5c5c",
    marginTop: 5
  },
  headerText: {
    color: "#fff",
    fontSize: 16
  },
  balanceText: {
    color: "#fff",
    fontSize: 24
  },
  amountInputWrapper: {
    marginBottom: 5,
    borderWidth: 1,
    borderColor: "#e6e6e6"
  },
  amountInputTitle: {
    color: "#979797",
    backgroundColor: "#f6f6f6",
    padding: 1,
    fontSize: 12
  }
});

export default FilterModelType;
