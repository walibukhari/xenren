import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Modal
} from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import Config from "../../../Config";
import { translate, USER_PERFERRED_LOCALE } from "../../../i18n";
import Gallery from "react-native-image-gallery";
import { Rating } from "react-native-elements";

class OfficialProjectDetailInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      item: this.props.review,
      showPic: false,
      datas: []
    };
  }

  componentDidMount() {}

  renderIcon() {
    let array = [];
    array.push(
      <Ionicons
        name="ios-thumbs-down"
        size={15}
        color="grey"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-heart"
        size={15}
        color="grey"
        style={{ marginLeft: 10 }}
      />
    );
    return <View style={styles.iconStyle}>{array}</View>;
  }

  renderWhiteIcon() {
    let array = [];
    array.push(
      <Ionicons
        name="ios-thumbs-down"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-heart"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    array.push(
      <Ionicons
        name="ios-share-alt"
        size={15}
        color="#fff"
        style={{ marginLeft: 10 }}
      />
    );
    return <View style={styles.iconWhiteStyle}>{array}</View>;
  }

  renderSkill(skills) {
    return skills.map((item, index) => {
      return (
        <View
          key={index}
          style={{
            borderRadius: 15,
            backgroundColor: "#f1c40f",
            flexDirection: "column",
            alignItems: "center",
            marginRight: 5,
            padding: 3
          }}
        >
          <Text
            style={{
              paddingHorizontal: 5,
              backgroundColor: "transparent",
              color: "#fff"
            }}
          >
            {USER_PERFERRED_LOCALE.includes('zh') ? item.skill.name_cn : item.skill.name_en}
          </Text>
        </View>
      );
    });
  }

  handleProjectDetail = () => {
    this.props.navigation.navigate("OfficialProjectDetail");
  };

  showImage = item => {
    let imagesArr = [];
    // portfolios.map((item) => {
    imagesArr.push({ source: { uri: `http://www.xenren.co/${item.path}` } });
    // })
    this.setState({
      datas: imagesArr,
      showPic: true
    });
  };

  renderAttachments(attachments) {
    return attachments.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={{
            borderColor: "#f1f1f1",
            borderWidth: 1,
            flexDirection: "row",
            alignItems: "center",
            marginRight: 10
          }}
          onPress={() => this.showImage(item)}
        >
          <Image
            source={{ uri: `http://www.xenren.co/${item.path}` }}
            style={styles.imageAttachment}
          />
        </TouchableOpacity>
      );
    });
  }

  render() {
    const item = this.props.project;
    return (
      <ScrollView style={styles.rootStyle}>
        <View
          style={{
            marginHorizontal: 0,
            marginVertical: 5
          }}
        >
          <View style={styles.boxInsideStyle}>
            <TouchableOpacity
            // onPress={this.handleProjectDetail}
            >
              <Image
                source={{ uri: `${Config.webUrl}${item.banner}` }}
                style={styles.itemImageStyle}
                resizeMode="cover"
              />

              <View style={styles.topSideStyle}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 5
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center"
                    }}
                  >
                    <Image source={item.avatarM} style={styles.avatarmStyle} />
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={2}
                      style={styles.text1Style}
                    >
                      {item.project.name}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  backgroundColor: "rgba(52,52,52,0.2)"
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 5
                  }}
                >
                  <Text style={styles.textWhiteStyle}>
                    {translate("PROJECT_TYPE")}
                    <Text style={styles.textWhiteStyle}> {item.projectId}</Text>
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 5
                  }}
                >
                  <Text style={styles.textWhiteStyle}>
                    {translate("END_DATE")}
                    <Text style={styles.textWhiteStyle}>
                      {" "}
                      {item.recruit_end}
                    </Text>
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  backgroundColor: "rgba(52,52,52,0.2)"
                }}
              >
                {/* <View style={{ flexDirection: 'column', marginLeft: 20,marginTop: 5}}>
									<Text style={styles.textWhiteStyle}>{translate('TIME_FRAME')}<Text style={styles.textWhiteStyle}> {item.time_frame}</Text></Text>
								 </View> */}
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 5
                  }}
                >
                  <Text style={styles.textWhiteStyle}>
                    {translate("LOCATION")}
                    <Text style={styles.textWhiteStyle}>
                      {" "}
                      {item.location_address}
                    </Text>
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  backgroundColor: "rgba(52,52,52,0.2)"
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20,
                    marginTop: 5
                  }}
                >
                  <Text style={styles.textWhiteStyle}>
                      {translate('FUNDING_STAGE')}
                    {item.funding_stage === 1 ? ' ' + translate('seed') : translate('excecute')}
                    <Text style={styles.textWhiteStyle}> {item.projectId}</Text>
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  backgroundColor: "rgba(52,52,52,0.2)"
                }}
              >
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20
                  }}
                >
                  <Text style={styles.textGreenStyle}>
                    {translate("SHARES")}
                    <Text style={styles.textWhiteLargeStyle}>
                      {" "}
                      {item.shared}
                    </Text>
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    marginLeft: 20
                  }}
                >
                  <Text style={styles.textGreenStyle}>
                    {translate('ISSUED')}
                    <Text style={styles.textWhiteLargeStyle}>
                      {' '}
                      {item.numberpercent}
                    </Text>
                  </Text>
                </View>
              </View>

              {/*</Image>*/}
            </TouchableOpacity>
            <View style={styles.bottomSideStyle}>
              <Image
                source={{ uri: `${Config.webUrl}${item.icon}` }}
                style={styles.avatarStyle}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 10
                }}
              >
                <Text style={styles.textPriceStyle}>
                  {item.pay_type === 1 ? translate('pay_hourly') : translate('pay_fixed')} -{' '}
                  <Text style={styles.textPriceStyle}>
                    {' '}
                    {item.project.reference_price}
                  </Text>
                </Text>
                {/* <Text style={styles.text3Style}>{item.number}:<Text style={styles.text4Style}> {item.numberpercent}</Text></Text>
									<Text style={styles.text5Style}>{item.price}</Text> */}
              </View>

              {/* <View style={{ flex: 1, flexDirection: 'column', marginLeft: 0,alignItems: 'center'}}>
							<View style={styles.button2} >
							<Text style={{ fontSize: 10, color: '#fff'}}>{translate('DOWNLOAD')}</Text>
							<Text style={{ fontSize: 10, color: '#fff'}}>{translate('ATTACHMENT')}</Text>
							</View>

							</View> */}
            </View>

            <View style={styles.bottomSideStyle}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5
                }}
              >
                <Text style={styles.text6Style}>{item.type}</Text>
                {/* {this.renderStar(item.star)} */}
                <Rating
                  type="star"
                  startingValue={3}
                  readonly
                  imageSize={15}
                  style={{ alignContent: "flex-start" }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5,
                  borderColor: "#ecf0f1",
                  borderLeftWidth: 1,
                  height: 40
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text6Style}>{item.position}</Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "column",
                    marginLeft: 10
                  }}
                >
                  <Text style={styles.text7Style}>{item.position_type}</Text>
                </View>
              </View>
            </View>

            <View style={styles.bottomSideStyle}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5
                }}
              >
                <Text style={styles.text10Style}>
                  {translate('PROJECT_STATUS')}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor,
                    }}
                  >
                      {translate('seed')}
                  </Text>
                </Text>
                <Text style={styles.text10Style}>
                  {translate('PROJECT_STAGE')}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor,
                    }}
                  >
                      {translate('a')}
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5
                }}
              >
                <Text style={styles.text10Style}>
                  {translate('PROJECT_VALUE')}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor,
                    }}
                  >
                      {isNaN(parseInt(item.budget_to, 10)) ? '--' : parseInt(item.budget_to, 10)}
                  </Text>
                </Text>
                <Text style={styles.text10Style}>
                  {translate('ATTANDANCE')}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor,
                    }}
                  >
                    32
                  </Text>
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View
          style={{
            marginHorizontal: 0,
            marginVertical: 5,
            marginBottom: 10
          }}
        >
          <View style={styles.boxInsideStyle}>
            <View style={styles.topSideStyle1}>
              <View
                style={{
                  flex: 1,
                  flexDirection: "column",
                  marginLeft: 5
                }}
              >
                <View style={{ flexDirection: "row" }}>
                  <Text
                    ellipsizeMode="tail"
                    numberOfLines={1}
                    style={styles.text1oStyle}
                  >
                    {item.user}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.container}>
              <View style={styles.payHour}>
                <Text
                  style={{
                    fontSize: 13,
                    color: "grey"
                  }}
                >
                  {translate("pay_hourly")} -{" "}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor
                    }}
                  >
                    {item.projectId}
                  </Text>
                </Text>
              </View>
              <View style={styles.projectType}>
                <Text
                  style={{
                    fontSize: 13,
                    color: "grey"
                  }}
                >
                  {translate("project_type")} -{" "}
                  <Text
                    style={{
                      fontSize: 13,
                      color: Config.primaryColor
                    }}
                  >
                    {item.projectType}
                  </Text>
                </Text>
              </View>
            </View>
            <View style={styles.middleSideStyle2}>
              <Text style={styles.textHeader}>{translate("DESCRIPTION")}</Text>
              <Text style={styles.textDescription}>{item.description}</Text>
            </View>
            <View
              style={{
                marginTop: 5,
                borderColor: "#ecf0f1",
                borderTopWidth: 1,
                marginBottom: 5
              }}
            >
              <Text style={styles.textHeader}>
                {translate("SKILL_REQUEST")}
              </Text>
              <View style={styles.middleSideStyle}>
                {this.renderSkill(item.project.project_skills)}
              </View>
            </View>

            {/* Render Project Images */}

            <View
              style={{
                marginTop: 5,
                borderColor: "#ecf0f1",
                borderTopWidth: 1
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: "90%",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginVertical: 10
                }}
              >
                <Text style={styles.textHeader}>
                  {translate("attachment")}{" "}
                </Text>
                <TouchableOpacity>
                  <Text style={styles.textDownload}>
                      {translate('download_all')} (
                    {item.project_images.length > 0
                      ? item.project_images.length
                      : "0"}
                    )
                  </Text>
                </TouchableOpacity>
              </View>
              {item.project_images.length > 0 ? (
                <View style={styles.middleSideStyle}>
                  {this.renderAttachments(item.project_images)}
                </View>
              ) : (
                <View style={styles.middleSideStyle}>
                  <Text
                    style={{
                      paddingTop: 5,
                      paddingBottom: 20
                    }}
                  >
                      {translate('no_attachment_found')}
                  </Text>
                </View>
              )}
            </View>
          </View>
        </View>

        {/* For Images */}
        <Modal
          transparent={true}
          supportedOrientations={["portrait", "landscape"]}
          visible={this.state.showPic}
          onRequestClose={() => console.log("")}
        >
          <View
            style={{
              height: "100%",
              width: "100%",
              backgroundColor: "#2C2C2C",
              alignItems: "flex-end"
            }}
          >
            <TouchableOpacity onPress={() => this.setState({ showPic: false })}>
              <Image
                source={require("../../../../images/account_profile/close.png")}
                style={{
                  width: 30,
                  height: 30,
                  marginTop: 10,
                  marginRight: 10
                }}
              />
            </TouchableOpacity>

            {/* For Swiping Images */}
            <Gallery
              style={{
                flex: 1,
                backgroundColor: "transparent"
              }}
              images={this.state.datas}
            />
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 0,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },
  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff"
    //paddingHorizontal: 5,
  },
  boxInsideStyle1: {
    flexDirection: "row",
    backgroundColor: "rgba(52,52,52,0.2)"
  },
  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10,
    backgroundColor: "rgba(52,52,52,0.2)",
    marginBottom: 0
  },
  topSideStyle1: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10,
    marginBottom: 0
  },
  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },
  middleSideStyle2: {
    flexDirection: "row",
    flexWrap: "wrap"
  },
  bottomSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    //   paddingTop: 10,
    // paddingBottom: 5,
    alignItems: "center",
    marginTop: 10,
    borderColor: "#ecf0f1",
    borderTopWidth: 1
  },
  bottomSideStyle1: {
    flexDirection: "row",
    alignItems: "center"
    //borderColor:'#ecf0f1',
    //borderTopWidth:1
  },
  avatarStyle: {
    width: 50,
    height: 50,
    borderRadius: 15,
    marginTop: 10
  },
  avatarmStyle: {
    width: 25,
    height: 25,
    resizeMode: "contain"
    //  marginTop: 15,
  },
  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  cardButtonTextStyle: {
    color: Config.primaryColor,
    fontSize: 12
  },
  itemImageStyle: {
    width: "100%",
    height: 180,
    backgroundColor: "rgba(52,52,52,0.4)"
  },

  text1Style: {
    flex: 1,
    color: "#fff",
    marginLeft: 5,
    marginTop: 5,
    fontSize: 18
  },
  text1oStyle: {
    flex: 1,
    color: "black",
    fontSize: 24
  },
  text2Style: {
    fontSize: 15,
    color: "#bdc3c7"
  },
  text3Style: {
    fontSize: 10,
    fontWeight: "200",
    color: "#bdc3c7"
  },
  textWhiteStyle: {
    fontSize: 10,
    fontWeight: "200",
    color: "#fff"
  },
  textWhiteLargeStyle: {
    fontSize: 13,
    fontWeight: "bold",
    color: "#fff"
  },
  textGreenStyle: {
    fontSize: 13,
    fontWeight: "bold",
    color: Config.primaryColor
  },
  textPriceStyle: {
    fontSize: 12,
    fontWeight: "300",
    color: "#000"
  },
  text4Style: {
    fontSize: 10,
    fontWeight: "200",
    color: Config.primaryColor
  },
  text5Style: {
    fontSize: 14,
    fontWeight: "300",
    color: Config.primaryColor
  },
  text6Style: {
    fontSize: 14,
    fontWeight: "400",
    color: "#000",
    paddingTop: 3
  },
  text8Style: {
    fontSize: 14,
    fontWeight: "400",
    color: "#000",
    paddingTop: 4,
    marginLeft: 5
  },
  text7Style: {
    fontSize: 13,
    fontWeight: "250",
    color: Config.primaryColor,
    paddingTop: 0
    //marginLeft: 5,
  },
  text10Style: {
    fontSize: 13,
    fontWeight: "bold",
    color: "#000",
    paddingTop: 0
    //marginLeft: 5,
  },
  text8Style: {
    fontSize: 13,
    fontWeight: "250",
    color: Config.primaryColor,
    paddingTop: 4
  },
  starStyle: {
    flexDirection: "row",
    paddingTop: 0,
    paddingBottom: 3
  },
  iconStyle: {
    flexDirection: "row",
    paddingTop: 8,
    marginLeft: 8,
    borderColor: "#ecf0f1",
    borderLeftWidth: 1,
    height: 35
  },
  iconWhiteStyle: {
    flexDirection: "row",
    paddingTop: 8,
    marginLeft: 8,
    height: 35
  },
  textDescription: {
    fontSize: 18,
    color: "grey",
    lineHeight: 35,
    fontWeight: "Thin",
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10
  },
  textHeader: {
    fontWeight: "Bold",
    fontSize: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    maringTop: 20
  },
  textHeader1: {
    fontWeight: "Bold",
    fontSize: 14,
    color: "#000",
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    maringTop: 20
  },
  imageAttachment: {
    width: 80,
    height: 80
  },
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button1: {
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: Config.primaryColor,
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  button2: {
    backgroundColor: Config.primaryColor,
    width: 100,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    maringTop: 5,
    borderColor: Config.primaryColor,
    borderWidth: 1
  },
  signUpButtonStyle: {
    fontWeight: "bold",
    color: Config.primaryColor
  },
  payHour: {
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#ecf0f1",
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
    //borderRadius: 3,
  },
  projectType: {
    backgroundColor: "#fff",
    borderColor: "#ecf0f1",
    //	borderRightWidth:1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    width: "50%",
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  textDownload: {
    fontWeight: "Italic",
    fontSize: 15,
    color: Config.primaryColor,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 5,
    maringTop: 30
  }
};

export default OfficialProjectDetailInfo;
