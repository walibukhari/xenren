import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
// import { List, ListItem, SearchBar, Avatar } from "react-native-elements";
import HttpRequest from '../../../components/HttpRequest';
import Config from '../../../Config';
import FreelancerDetail from '../subs/FreelancerDetail';

class FreelancerDetailTab extends Component {
   constructor(props) {
        super(props);

        this.state = {
            activeTabIndex: 0,
            projectDetail: this.props.navigation.state.params.projectDetail,
            activeTabIndex: 0,
            chatText: '',
            is_set: 1,
            isJobApply: false,
            isSetting: false,
            falseSwitchIsOn: false,
            files:[],
            isLoading: false,
            chatRoomId: '',
            freelancers: [],
            isRefreshing:false
        }
    }

    componentDidMount() {
        this.setState({
            isLoading: true,
        })
        this.getFreelancersDetail();
    }

    getFreelancersDetail() {
        const token = this.props.userData.token
        const chatRoomId = this.props.project === 'official' ? this.props.navigation.state.params.project.project_id : this.props.navigation.state.params.project.id
        this.setState ({ chatRoomId: chatRoomId});
        HttpRequest.getDiscussRoomChat(token, chatRoomId)
        .then((response) => {
            console.log('freelancer-->', response);
            if (response.data.status === 'success') {
                this.setState({
                    isLoading: false,
                    isRefreshing:false,
                    chatRoomId: response.data.projectChatRoomId,
                    freelancers: response.data.projectChatFollowers,
                 })
            } else {
                this.setState({
                    isLoading: false,
                    isRefreshing:false
                })
            }
        })
        .catch(error => {
            this.setState({
                isLoading: false,
                isRefreshing:false
             })
        })
    }

    showAlert() {
        // Show Dialog
        global.setTimeout(() => {
            Alert.alert(
                'Warning',
                'Token expire!',
                [
                    { text: 'OK', onPress: () => { this.logout() } }
                ]
            );
        }, 200);
    }

    logout() {
        LocalData.setUserData(null);
        this.props.navigation.dispatch(
            NavigationActions.reset({
                index: 0,
                actions: [
                    NavigationActions.navigate({
                        routeName: "Login"
                    })
                ]
            })
        );
    }

    callBackFromDetail = (item) => {
        let temp = this.state.freelancers;
        temp[item.index] = item.item
        this.setState({ freelancers: temp })
    }

    onRefresh() {
        this.setState({ isRefreshing: true });
         this.getFreelancersDetail();
    }

    renderRow = (item) => {
        if(this.state.isLoading){
            return ( <ActivityIndicator />);
        } else {if (this.props.userData !== item.item.user_id){
            if (item.item.user !== null) {
                return (
                    <FreelancerDetail freelancerDetail={item} navigation={this.props.navigation}
                                      callBackToParent={this.callBackFromDetail}
                    />
                )
            }
        }
        }
    };

    render() {
        return (
            <View style={styles.rootStyle}>
                    <View style={styles.rootStyle2}>
                    {this.state.activeTabIndex === 0 &&
                        <FlatList
                            style={{ flex: 1 }}
                            data={this.state.freelancers}
                            renderItem={ (item) =>  this.renderRow(item)  }
                            onRefresh={() => this.onRefresh()}
                            refreshing={this.state.isLoading}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    }
                    </View>

            </View>
        );
    }
}

const styles = {
    rootStyle: {
        paddingHorizontal: 10,
        paddingVertical: 0,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ecf0f1'
    },
    rootStyle2: {
      //  paddingHorizontal: 10,
        paddingVertical: 0,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ecf0f1'
    },
    topTabWrapperStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 5
    },

    tabControlStyle: {
        backgroundColor: '#808080',
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    },

    tabControlText: {
        color: '#fff',
        fontSize: 15,
        fontWeight: '300'
    },

    activeTabControlStyle: {
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    }
}

const mapStateToProps = state => ({
    component: state.component,
    userData: state.auth.userData
})


export default connect(mapStateToProps)(FreelancerDetailTab);
