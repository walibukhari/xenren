import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, ListView } from 'react-native';
import { List, ListItem, SearchBar, Avatar } from 'react-native-elements';

import { translate } from '../../../i18n';
import Config from '../../../Config';
import Review from '../subs/Review';


class commonProject extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      officialComment: this.convertToDataSource(this.getOfficialComments()),
      comments: this.convertToDataSource(this.getNormalComments()),
    };
  }


  renderRow(review) {
    return (
      <Review
        review={review}
      />
    );
  }

  getOfficialComments() {
    const officialComment = [
      {
        id: 1301,
        project: 'User Submit Project 1',
        priceText: 'Fixed Price',
        name: 'Peter Jude',
        projectId: '$15',
        date: 'Posted,9 months ago',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'HTML5' },
          { id: 'Photoshop' },
          { id: 'Adobe Ilustrator' },
        ],
        attachments: [
          {
            type: 'picture',
            source: require('../../../../images/other/icon_picture.png'),
          },
          {
            type: 'doc',
            source: 'some source',
          },
          {
            type: 'xls',
            source: 'some source',
          },
        ],
      },
      {
        id: 1302,
        project: 'User Submit Project 2',
        name: 'Peter Jude',
        priceText: 'Fixed Price',
        projectId: '$15',
        date: 'Posted,9 months ago',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'HTML5' },
          { id: 'Photoshop' },
          { id: 'Adobe Ilustrator' },
        ],
        attachments: [
          {
            type: 'picture',
            source: require('../../../../images/other/icon_picture.png'),
          },
          {
            type: 'doc',
            source: 'some source',
          },
          {
            type: 'xls',
            source: 'some source',
          },
        ],
      },
      {
        id: 1303,
        project: 'User Submit Project 3',
        name: 'Peter Jude',
        priceText: 'Fixed Price',
        projectId: '$15',
        date: 'Posted,9 months ago',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'HTML5' },
          { id: 'Photoshop' },
          { id: 'Adobe Ilustrator' },
        ],
        attachments: [
          {
            type: 'picture',
            source: require('../../../../images/other/icon_picture.png'),
          },
          {
            type: 'doc',
            source: 'some source',
          },
          {
            type: 'xls',
            source: 'some source',
          },
        ],
      },
    ];
    return officialComment;
  }

  getNormalComments() {
    const comments = [
      {
        id: 1301,
        name: 'Peter Jude',
        date: '07 Nov 2017 06: 33',
        projectId: '32333233',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'PHP' },
          { id: 'Javascript' },
          { id: 'Adobe Ilustrator' },
          { id: 'Photoshop' },
        ],
      },
      {
        id: 1302,
        name: 'Peter Jude',
        date: '07 Nov 2017 06: 33',
        projectId: '32333233',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'PHP' },
          { id: 'Javascript' },
          { id: 'Adobe Ilustrator' },
          { id: 'Photoshop' },
        ],
      },
      {
        id: 1303,
        name: 'Peter Jude',
        date: '07 Nov 2017 06: 33',
        projectId: '32333233',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over',
        star: 5,
        avatar: require('../../../../images/share_office/user_dummy.png'),
        skills: [
          { id: 'PHP' },
          { id: 'Javascript' },
          { id: 'Adobe Ilustrator' },
          { id: 'Photoshop' },
        ],
      },
    ];
    return comments;
  }

  convertToDataSource(array) {
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    return ds.cloneWithRows(array);
  }

  renderHeader = () => {
    return <SearchBar darkTheme
                      barStyle="default"
                      searchBarStyle="minimal"
                      placeholder={translate('search')}
                      searchIcon={{ size: 24 }}
                      icon={{
                        type: 'font-awesome',
                        name: 'search',
                        marginTop: 15,
                        marginBottom: 15,
                      }}
                      cancelIcon={false}
                      containerStyle={{
                        backgroundColor: '#f7f7f7',
                        width: '100%',
                        flexDirection: 'row-reverse',
                        alignContent: 'space-between',
                        justifyContent: 'space-between',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                      }}
                      inputContainerStyle={{
                        backgroundColor: '#ffffff',
                        flexDirection: 'row-reverse',
                        alignContent: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                      }}
                      inputStyle={{
                        backgroundColor: '#ffffff',
                        width: '100%',
                      }}/>;
  };

  render() {
    const { officialComment, comments } = this.state;
    return (
      <View style={styles.rootStyle}>
        <SearchBar darkTheme
                   barStyle="default"
                   searchBarStyle="minimal"
                   placeholder={translate('search')}
                   searchIcon={{ size: 24 }}
                   icon={{
                     type: 'font-awesome',
                     name: 'search',
                     marginTop: 15,
                     marginBottom: 15,
                   }}
                   cancelIcon={false}
                   containerStyle={{
                     backgroundColor: '#f7f7f7',
                     width: '100%',
                     flexDirection: 'row-reverse',
                     alignContent: 'space-between',
                     justifyContent: 'space-between',
                     borderTopWidth: 0,
                     borderBottomWidth: 0,
                   }}
                   inputContainerStyle={{
                     backgroundColor: '#ffffff',
                     flexDirection: 'row-reverse',
                     alignContent: 'flex-start',
                     justifyContent: 'flex-start',
                     margin: 0,
                   }}
                   inputStyle={{
                     backgroundColor: '#ffffff',
                     width: '100%',
                   }}/>
        <Text style={{
          paddingTop: 1,
          borderTopColor: '#f7f7f7',
          paddingBottom: 10,
          paddingLeft: 5,
        }}> 20 {translate('jobs_found')}</Text>
        <View style={styles.rootStyle2}>
          {this.state.activeTabIndex === 0 &&
          <ListView
            style={{ flex: 1 }}
            dataSource={officialComment}
            renderRow={(rowData) => {
              return this.renderRow(rowData);
            }}
          />
          }
        </View>

      </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
  },

  rootStyle2: {
    //  paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f7f7f7',
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 15,
    fontWeight: '300',
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },
};

const mapStateToProps = state => ({
  component: state.component,
});

export default connect(mapStateToProps)(commonProject);
