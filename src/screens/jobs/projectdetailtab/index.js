import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, TouchableOpacity, ListView, FlatList } from "react-native";
import { List, ListItem, SearchBar, Avatar } from "react-native-elements";

import Config from "../../../Config";
import ProjectDetailInfo from "../subs/ProjectDetailInfo";

class ProjectTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      projectData: this.props.navigation.state.params.data
    };
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.rootStyle2}>
          <ProjectDetailInfo navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },
  rootStyle2: {
    //  paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },
  topTabWrapperStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5
  },

  tabControlStyle: {
    backgroundColor: "#808080",
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  },

  tabControlText: {
    color: "#fff",
    fontSize: 15,
    fontWeight: "300"
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  }
};

const mapStateToProps = state => ({
  component: state.component
});

export default connect(mapStateToProps)(ProjectTab);
