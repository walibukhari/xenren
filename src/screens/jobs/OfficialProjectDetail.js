import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ToastAndroid, Text, TouchableOpacity, Image, Modal } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../../Config';
import Portifolio from './portifolio';
import DiscussRoom from './discussroom';
import Projects from './projects';
import OfficialProjectTab from './officialprojectdetailtab';
import FreelancerDetailTab from './freelancerdetailtab';
import comment from './commonProject';
import Payment from './subs/Payment';
import FilterModel from './subs/FilterModel';
import FilterModelType from './subs/FilterModelType';
import { translate } from '../../i18n';
import HttpRequest from '../../components/HttpRequest';

class OfficialProjectDetail extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTintColor: Config.primaryColor,
    title: translate('official_project'),
    headerRight: navigation.state.params ? navigation.state.params.headerRight : null,
    headerTitleStyle: {
      color: Config.textSecondaryColor,
      fontWeight: 'bold',
    },
  });


  constructor(props) {
    super(props);
    const jobsSourcesType = [
      {
        id: 1,
        name: 'Recommend Jobs',
      },
      {
        id: 2,
        name: 'All Jobs',
      },
      {
        id: 3,
        name: 'Fixed Price Jobs',
      },
      {
        id: 4,
        name: 'Hourly Jobs',
      },
    ];
    const jobsSourcesOType = [
      {
        id: 1,
        name: 'Recommend Jobs',
      },
      {
        id: 2,
        name: 'All Jobs',
      },
    ];


    const jobsSourcesType1 = [
      {
        id: 1,
        name: 'Latest Jobs',
      },
      {
        id: 2,
        name: 'Oldest Jobs',
      },
      {
        id: 3,
        name: 'Quoated Price Ascending',
      },
      {
        id: 4,
        name: 'Quoated Price Descending',
      },
    ];
    this.state = {
      activeTabIndex: 1,
      isShowFilter: false,
      // isShowFilter: false,
      isShowFilterOfficial: false,
      jobsSourcesType,
      jobsSourcesOType,
      jobsSourcesType1,
      languages: [],
      countries: [],
    };
    this.root = this.props.component.root;
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    this.getCountries();
    this.getLanguages();
    // Set route params
    this.props.navigation.setParams({
      headerRight: (
        <View style={{
          alignItems: 'center',
          flexDirection: 'row',
        }}>
          {/* <View style={{ flexDirection: 'column'}}>

       <Image
            source={require('../../../images/jobs/star.png')}
            style={{
              width: 25,
              height: 25,
              resizeMode: 'contain',
              marginRight: 10,
              tintColor: Config.textSecondaryColor,
            }}
        />
	  </View>
	   <View style={{ flexDirection: 'column'}}>
       <Image
            source={require('../../../images/jobs/share.png')}
            style={{
              width: 25,
              height: 25,
              resizeMode: 'contain',
              marginRight: 10,
              tintColor: Config.textSecondaryColor,
            }}
        />
	    </View>
	   <View style={{ flexDirection: 'column'}}>
       <Image
            source={require('../../../images/jobs/pin.png')}
            style={{
              width: 25,
              height: 25,
              resizeMode: 'contain',
              marginRight: 10,
             tintColor: Config.textSecondaryColor,
            }}
        />
	    </View> */}
        </View>
      )
    });
  }

  getLanguages = () => {
    HttpRequest.getLanguages()
      .then((response) => {
        this.setState({
          languages: response.data,
        });
      })
      .catch(error => {
      });
  };

  getCountries() {
    HttpRequest.getCountries()
      .then((response) => {
        this.setState({
          countries: response.data,
        });
      })
      .catch(error => {
      });
  }

  handleFilter() {
    if (this.state.activeTabIndex == 1) {
      this.setState({
        isShowFilter: true,
        isShowFilterOfficial: false,
      });

    } else {
      this.setState({
        isShowFilterOfficial: true,
        isShowFilter: false,
      });
    }

  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.topButtonWrapperStyle}>
          <View style={{
            width: 1,
            height: 50,
            backgroundColor: '#ecf0f1',
          }}/>
          <TouchableOpacity onPress={() => {
            this.setState({ activeTabIndex: 1 });
          }}
                            style={[styles.topButtonStyle, this.state.activeTabIndex == 1 ? styles.topButtonActiveStyle : {}]}>
            <Text
              style={this.state.activeTabIndex == 1 ? styles.topTextActiveStyle : {}}>{translate('project')}</Text>
          </TouchableOpacity>
          <View style={{
            width: 1,
            height: 50,
            backgroundColor: '#ecf0f1',
          }}/>
          <TouchableOpacity onPress={() => {
            this.setState({ activeTabIndex: 2 });
          }}
                            style={[styles.topButtonStyle, this.state.activeTabIndex == 2 ? styles.topButtonActiveStyle : {}]}>
            <Text
              style={this.state.activeTabIndex == 2 ? styles.topTextActiveStyle : {}}>{translate('discuss_room')}</Text>
          </TouchableOpacity>
          <View style={{
            width: 1,
            height: 50,
            backgroundColor: '#ecf0f1',
          }}/>
          <TouchableOpacity onPress={() => {
            this.setState({ activeTabIndex: 3 });
          }}
                            style={[styles.topButtonStyle, this.state.activeTabIndex == 3 ? styles.topButtonActiveStyle : {}]}>
            <Text
              style={this.state.activeTabIndex == 3 ? styles.topTextActiveStyle : {}}>{translate('freelancer_details')}</Text>
          </TouchableOpacity>
        </View>
        {this.state.activeTabIndex == 1 && <OfficialProjectTab navigation={this.props.navigation}
                                                               project={this.props.navigation.state.params.project}/>}
        {this.state.activeTabIndex == 2 &&
        <DiscussRoom navigation={this.props.navigation} project='official'/>}
        {this.state.activeTabIndex == 3 &&
        <FreelancerDetailTab navigation={this.props.navigation} project='official'/>}
        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
            this.setState({
              isShowFilter: false,
            });
          }}
          visible={this.state.isShowFilter}>
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>

              <View style={styles.bottomSideStyle}>
                <Image source={require('../../../images/jobs/filter.png')}
                       style={styles.avatarStyle}/>
                <View style={{
                  flex: 1,
                  flexDirection: 'column',
                  marginLeft: 10,
                }}>
                  <Text style={styles.text5Style}>{translate('filter')}</Text>

                </View>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 5,
                  }}
                  onPress={() => this.setState({ isShowFilter: false })}>
                  <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 0,
                  }}>
                    <Image source={require('../../../images/jobs/cross.png')}
                           style={styles.avatarStyle1}/>
                  </View>
                </TouchableOpacity>
              </View>
              <FilterModel action={translate('WITHDRAW')}/>
              <Payment action={translate('SHOW_OPTION')} data={this.state.jobsSourcesType}/>
              <Payment action={translate('ORDERING_OPTION')} data={this.state.jobsSourcesType1}/>
              <View style={styles.buttonWrapper}>
                <TouchableOpacity>
                  <Text style={styles.fontC}>{translate('apply')}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="fade"
          transparent={true}
          onRequestClose={() => {
            this.setState({
              isShowFilter: false,
            });
          }}
          visible={this.state.isShowFilterOfficial}>
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyleOfficial}>

              <View style={styles.bottomSideStyle}>
                <Image source={require('../../../images/jobs/filter.png')}
                       style={styles.avatarStyle}/>
                <View style={{
                  flex: 1,
                  flexDirection: 'column',
                  marginLeft: 10,
                }}>
                  <Text style={styles.text5Style}>{translate('filter')}</Text>

                </View>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 5,
                  }}
                  onPress={() => this.setState({ isShowFilterOfficial: false })}>
                  <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 0,
                  }}>
                    <Image source={require('../../../images/jobs/cross.png')}
                           style={styles.avatarStyle1}/>
                  </View>
                </TouchableOpacity>
              </View>
              <FilterModelType action={translate('WITHDRAW')} languages={this.state.languages}
                               countries={this.state.countries}/>
              <Payment action={translate('SHOW_OPTION')} data={this.state.jobsSourcesOType}/>
              <Payment action={translate('ORDERING_OPTION')} data={this.state.jobsSourcesType1}/>
              <View style={styles.buttonWrapper}>
                <TouchableOpacity>
                  <Text style={styles.fontC}>{translate('apply')}</Text>
                </TouchableOpacity>
              </View>


            </View>
          </View>
        </Modal>

      </View>
    );
  }
}

const styles = {
  bottomSideStyle: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    marginTop: 5,
    paddingBottom: 5,
    borderColor: '#ecf0f1',
    borderBottomWidth: 1,
  },

  avatarStyle: {
    width: 25,
    height: 25,
    paddingTop: 10,
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
    position: 'absolute',
    right: 0,
  },

  text5Style: {
    fontSize: 16,
    color: '#bdc3c7',
  },

  fontC: {
    fontSize: 12,
    color: '#fff',
  },

  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },

  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: '90%',
    marginTop: 10,
    marginLeft: 15,
    paddingHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    borderRadius: 10,
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ffffff',
    borderBottomWidth: 2,
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
  },

  topTextActiveStyle: {
    color: Config.primaryColor,
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogBoxStyle: {
    width: 320,
    height: 505,
    backgroundColor: '#fff',
    borderRadius: 3,
  },

  dialogBoxStyleOfficial: {
    width: 320,
    height: 405,
    backgroundColor: '#fff',
    borderRadius: 3,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root: root,
    })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfficialProjectDetail);
