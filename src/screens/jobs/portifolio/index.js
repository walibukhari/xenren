import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, Image, FlatList } from "react-native";
import { SearchBar } from "react-native-elements";
import Config from "../../../Config";
import Project from "../subs/Project";
import { translate } from "../.././../i18n";
import HttpRequest from "../../../components/HttpRequest";

class Portifolio extends Component {
  constructor(props) {
    super(props);

    const portifolioData = [
      {
        name: "Graphic Design for Legend Lair Website",
        link: "www.behance.com/legendlair",
        images: [
          require("../../../../images/account_profile/pot1.png"),
          require("../../../../images/account_profile/pot2.png"),
          require("../../../../images/account_profile/pot3.png")
        ]
      }
    ];

    this.state = {
      activeTabIndex: 0,
      isLoading: true,
      isRefreshing: false,
      commentData: [],
      filteredList: [],
      search: '',
      skillIDs: [],
      skillList: '',
    };
  }

  componentDidMount() {
    const arr = [];
    this.props.skills.map((item) => {
      arr.push(item.value);
    });
    this.setState(
      {
        skillList: arr.toString(),
      },
      () => {
        this.getJobsList(`skill_id_list=${this.state.skillList}`);
      },
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ isLoading: true,  listLength: '' });
    this.getJobsList(nextProps.filterString);
  }

  getJobsList = (filter) => {
    HttpRequest.searchJobs('2', filter)
      .then((response) => {
        if (response.data.data.officialProjects.length === 0) {
          alert(translate('no_job_posts'));
          this.setState({
            listLength: response.data.data.officialProjects.length,
            isLoading: false,
            isRefreshing: false,
          });
        } else {
          this.setState({
            commentData: response.data.data.officialProjects,
            filteredList: response.data.data.officialProjects,
            listLength: response.data.data.officialProjects.length,
            projectType: response.data.data.projectType,
            isLoading: false,
            isRefreshing: false,
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          isRefreshing: false,
        });
        alert(translate('network_error'));
      });
  };

  callbackFromProject = job => {
    let temp = this.state.commentData;
    temp[job.index] = job.item;
    this.setState({ commentData: temp });
  };

  onRefresh() {
    this.setState({ isRefreshing: true });
    this.getJobsList();
  }

  handleSearchInput = e => {
    let text = e.toLowerCase();
    let fullList = this.state.commentData;
    let filteredList = fullList.filter(item => {
      // console.log('Filtereed Text offical --- ',item);
      if (
        item.title.toLowerCase().match(text) ||
        item.description.toLowerCase().match(text)
      ) {
        return item;
      }
    });

    if (!text || text === "") {
      this.setState({
        filteredList: fullList,
        listLength: fullList.length,
        search: ""
      });
    } else if (!filteredList.length) {
      this.setState({
        filteredList: filteredList,
        listLength: filteredList.length,
        search: text
      });
    } else if (Array.isArray(filteredList)) {
      this.setState({
        filteredList: filteredList,
        listLength: filteredList.length,
        search: text
      });
    }
  };

  renderRow(project) {
    return (
      <Project
        project={project}
        navigation={this.props.navigation}
        callbackToParent={this.callbackFromProject}
      />
    );
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <SearchBar
          darkTheme
          barStyle="default"
          searchBarStyle="minimal"
          placeholder={translate('search')}
          searchIcon={{ size: 24 }}
          icon={{
            type: "font-awesome",
            name: "search",
            marginTop: 15,
            marginBottom: 15
          }}
          cancelIcon={false}
          containerStyle={{
            backgroundColor: "#f7f7f7",
            width: "100%",
            flexDirection: "row-reverse",
            alignContent: "space-between",
            justifyContent: "space-between",
            borderTopWidth: 0,
            borderBottomWidth: 0
          }}
          inputContainerStyle={{
            backgroundColor: "#ffffff",
            flexDirection: "row-reverse",
            alignContent: "flex-start",
            justifyContent: "flex-start",
            margin: 0
          }}
          inputStyle={{
            backgroundColor: "#ffffff",
            width: "100%"
          }}
          onChangeText={text => this.handleSearchInput(text)}
          onCancel={this.clearSearchText}
        />
        <Text
          style={{
            paddingTop: 1,
            borderTopColor: "#f7f7f7",
            paddingBottom: 10,
            paddingLeft: 5
          }}
        >
          {" "}
          {this.state.listLength} {translate("jobs_found")}{" "}
        </Text>
        <View style={styles.rootStyle2}>
          <FlatList
            style={{ flex: 1 }}
            data={
              this.state.search.length == 0
                ? this.state.commentData
                : this.state.filteredList
            }
            renderItem={item => this.renderRow(item)}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isLoading}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#f7f7f7"
  },

  rootStyle2: {
    //  paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#f7f7f7"
  },

  paragraph: {
    textAlign: "center"
  },

  topTabWrapperStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5
  },

  tabControlStyle: {
    backgroundColor: "#808080",
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  },

  tabControlText: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "Normal"
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  },

  createProjectWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: 50
  },

  createProjectText: {
    paddingHorizontal: 5,
    color: Config.primaryColor,
    fontSize: 18
  },

  portifolioWrapper: {
    backgroundColor: "#fff",
    paddingHorizontal: 5,
    paddingVertical: 10
  },

  boxInsideStyle: {
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center"
  },

  itemNameStyle: {
    fontSize: 18
  },

  itemImageStyle: {
    width: 350,
    height: 250,
    opacity: 0.2
  },

  itemLinkStyle: {
    flexDirection: "row",
    backgroundColor: "#59af24",
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 15
  },

  itemLinkTextStyle: {
    color: "#fff",
    fontSize: 16,
    marginRight: 10
  }
};

const mapStateToProps = state => ({
  component: state.component
});

export default connect(mapStateToProps)(Portifolio);
