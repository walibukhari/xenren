import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator,
  TextInput,
  FlatList,
  Switch,
  Modal
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../../../Config';
import { translate } from '../../../i18n';
import FilePickerManager from 'react-native-file-picker';
import HttpRequest from '../../../components/HttpRequest';
import Gallery from 'react-native-image-gallery';
import ImagePicker from 'react-native-image-picker';


const { width, height } = Dimensions.get('window');

class DiscussRoom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projectDetail: this.props.navigation.state.params.project,
      activeTabIndex: 0,
      chatText: '',
      is_set: true,
      isJobApply: false,
      isSetting: false,
      falseSwitchIsOn: false,
      file: null,
      chatData: [],
      isLoading: true,
      chatRoomId: '',
      chatRoomMembers: [],
      showPic: false,
      tmpImg: null,
      fileName: '',
    };
  }

  componentDidMount() {
    // this.hfet
    this.getMessages();
  }

  handleUserProfile() {

  }

  getMessages() {
    const token = this.props.userData.token;
    const chatRoomId = this.props.project === 'official' ? this.props.navigation.state.params.project.project_id : this.props.navigation.state.params.project.id;
    this.setState({ chatRoomId: chatRoomId });
    HttpRequest.getDiscussRoomChat(token, chatRoomId)
      .then((response) => {
        if (response.data.status === 'success') {
          this.setState({
            chatData: response.data.chat_messages,
            isLoading: false,
            chatRoomId: response.data.projectChatRoomId,
            chatRoomMembers: response.data.projectChatFollowers,
          });
        } else if (response.data.message === '404 not found') {
          alert('No Room found');
        } else {
          alert(translate('network_error'));
        }
      })
      .catch((error) => {
        this.setState({
          isLoading: false,
        });
      });
  }

  uploadImage() {
    let self = this;
    let imageData = {
      uri: this.state.tmpImg.uri,
      name: this.state.fileName,
      type: 'image/jpg',
    };
    console.log(imageData);
    const chatRoomId = this.props.project === 'official' ? this.props.navigation.state.params.project.project_id : this.props.navigation.state.params.project.id;
    console.log('chat room id --', chatRoomId);
    HttpRequest.uploadFileDiscussRoom(this.props.userData.token, imageData, chatRoomId, '')
      .then((response) => {
        console.log('response --- ', response);
        self.setState({
          tmpImg: null,
          fileName: '',
        });
        self.setState({
          isJobApply: false,
        });
        this.getMessages();
      })
      .catch((error) => {
        let message = 'Cannot Fetch, Please Try Again.';
        console.log('Error --- ', error);
        self.setState({
          isJobApply: false,
        });
        this.getMessages();
      });
  }

  sendMsgAPI(msg) {
    HttpRequest.sendMsgDiscussRoom(this.props.userData.token, this.state.chatRoomId, msg)
      .then((response) => {
        console.log('reponse send msg --- ', response);
        if (response.data.status === 'Success') {
          this.setState({ chatText: '' });
          this.getMessages();
        } else {
          alert(response.data.message);
        }
      })
      .catch(error => {
        console.log('Error --- ', error);
      });
  }

  sendMessage = (image, file) => {
    let self = this;
    if (this.state.chatText !== '') {
      let newData = {
        id: this.props.userData.id,
        content: this.state.chatText,
        email: this.props.userData.real_name,
        isImage: image,
        file: file,
        // time:
      };
      this.setState((prevState) => ({
        chatData: [...prevState.chatData, newData],
      }));
      if (image == 0) {
        this.sendMsgAPI(this.state.chatText);
      } else {
        this.uploadImage();
      }
      self.setState({
        isJobApply: false,
      });
      this.getMessages();
    }
  };

  renderSendMessgageIcon(quit) {
    if (quit) {
      return (
        <Image source={require('../../../../images/jobs/message.png')} style={{ marginTop: 5 }}/>);
    } else {
      return 'Quit';
    }
  }

  renderSendIcon() {
    let array = [];
    array.push(<Ionicons name="md-send" size={18} color={Config.primaryColor}
                         style={{ marginLeft: 2 }}/>);
    return (<View style={styles.iconStyle}>{array}</View>);
  }

  settingOpen(is_set) {
    if (is_set) {
      this.setState({
        isSetting: true,
        is_set: false,
      });
    } else {
      this.setState({
        isSetting: false,
        is_set: true,
      });
    }
  }

  handleApplyJob = () => {
    this.setState({
      isJobApply: true,
    });
  };

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      let source = { uri: response.uri };
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
      this.setState({
        tmpImg: response,
        fileName: response.name,
        ImageShow: false,
      });
    }
    });
  }

  changeFileName = (text) => {
    this.setState({ fileName: text });
  };

  openMsgImage = (item) => {
    // let imagesArr = [];
    // imagesArr.push({ source: { uri: `http://www.xenren.co/${item.uri}` } })
    // this.setState({ datas: imagesArr, showPic: true });
  };

  renderFiles = () => {
    // this.state.file.map((file) => {
    return (
      <View style={{
        width: 40,
        height: 50,
        justifyContent: 'center',
      }}>
        <Image style={{
          width: 35,
          height: 45,
        }} resizeMode='contain' source={{ uri: this.state.file.uri }}/>
      </View>
    );
    // })
  };

  renderRow = (item) => {
    if (this.state.chatData) {
      if (item.item.id == this.props.userData.id) {
        return (
          <View>
            <View style={styles.rowLeftStyle}>
              <Image source={{ uri: this.props.userData.img_avatar }} style={styles.avatarStyle}/>
              <Image source={require('./../../../../images/private_chat/corner_1.png')}
                     style={{
                       height: 10,
                       width: 10,
                       tintColor: '#fff',
                       marginRight: -2,
                     }}/>
              {item.item.isImage !==0 ? <View style={styles.infoStyle}>
                <View style={styles.bubbleLeftStyle}>
                  <Text style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>{item.item.email}</Text>
                  <Text>Image Uploaded:</Text>
                  <Image source={{ uri: item.item.file }} style={styles.avatarStyle}/>
                </View>
              </View> : <View style={styles.infoStyle}>
                <View style={styles.bubbleLeftStyle}>
                  <Text style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                  }}>{item.item.email}</Text>
                  <Text>{item.item.content}</Text>
                </View>
              </View>}

            </View>

            {/*{item.item.isImage !== 0 &&*/}
            {/*/!*<View style={styles.rowLeftStyle}>*!/*/}
            {/*/!*  /!*<Image source={{ uri: this.props.userData.img_avatar }} style={styles.avatarStyle}/>*!/*!/*/}
            {/*/!*  /!*<Image source={require('./../../../../images/private_chat/corner_1.png')}*!/*!/*/}
            {/*/!*  /!*       style={{*!/*!/*/}
            {/*/!*  /!*         height: 10,*!/*!/*/}
            {/*/!*  /!*         width: 10,*!/*!/*/}
            {/*/!*  /!*         tintColor: '#fff',*!/*!/*/}
            {/*/!*  /!*         marginRight: -2,*!/*!/*/}
            {/*/!*  /!*       }}/>*!/*!/*/}
            {/*/!*  <View style={styles.infoStyle}>*!/*/}
            {/*/!*    <Text>HI</Text>*!/*/}
            {/*/!*    /!*<TouchableOpacity onPress={() => this.openMsgImage(item)}*!/*!/*/}
            {/*/!*    /!*                  style={{*!/*!/*/}
            {/*/!*    /!*                    width: 50,*!/*!/*/}
            {/*/!*    /!*                    height: 60,*!/*!/*/}
            {/*/!*    /!*                    borderRadius: 5,*!/*!/*/}
            {/*/!*    /!*                    justifyContent: 'center',*!/*!/*/}
            {/*/!*    /!*                    alignItems: 'center',*!/*!/*/}
            {/*/!*    /!*                  }}>*!/*!/*/}
            {/*/!*    /!*  <Image source={require('../../../../images/dummy/office_1.jpg')}*!/*!/*/}
            {/*/!*    /!*         resizeMode='cover'/>*!/*!/*/}
            {/*/!*    /!*</TouchableOpacity>*!/*!/*/}
            {/*/!*  </View>*!/*/}
            {/*/!*</View>*!/*/}
            {/*}*/}
          </View>
        );
      } else {
        return (
          <View>
            <View style={styles.rowRightStyle}>
              <View style={[styles.infoStyle, { alignItems: 'flex-end' }]}>
                <View style={styles.bubbleRightStyle}>
                  <Text style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    color: '#fff',
                  }}>{item.item.email}</Text>
                  <Text style={{ color: '#fff' }}>{item.item.content}</Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => {
                this.handleUserProfile();
              }}>
                <Image source={{ uri: item.item.avatar }} style={styles.avatarStyle}/>
              </TouchableOpacity>
            </View>
            {item.item.isImage === 0 &&
            <View style={styles.rowRightStyle}>
              <Image source={{ uri: this.props.userData.img_avatar }} style={styles.avatarStyle}/>
              <Image source={require('./../../../../images/private_chat/corner_1.png')}
                     style={{
                       height: 10,
                       width: 10,
                       tintColor: '#fff',
                       marginRight: -2,
                     }}/>
              <View style={styles.infoStyle}>
                <TouchableOpacity onPress={() => this.openMsgImage(item)}
                                  style={{
                                    width: 50,
                                    height: 60,
                                    borderRadius: 5,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}>
                  <Image source={require('../../../../images/dummy/office_1.jpg')}
                         resizeMode='cover'/>
                </TouchableOpacity>
              </View>
            </View>
            }
          </View>
        );
      }
    }
  };

  render() {
    const setValue = this.state.is_set;
    if (this.state.isLoading) {
      return (
      <View style={{
        position: 'absolute',
        top: height * 0.5 - 80,
        left: width * 0.5 - 10,
      }}>
        <ActivityIndicator />
      </View>
      );
    } else {
      return (
        <View style={styles.rootStyle}>
          <View style={styles.rootStyle2}>
            <Text style={{
              paddingTop: 10,
              paddingBottom: 5,
              paddingLeft: 5,
              fontSize: 14,
              fontWeight: 'bold',
            }}>{translate('CHAT_ROOM_MEMBERS')} ({this.state.chatRoomMembers.length})</Text>
            <View style={styles.container1}>
              <FlatList
                horizontal
                data={this.state.chatRoomMembers}
                renderItem={({ item }) => {
                  if (item.user !== null) {
                  return (
                      <View style={{
                        backgroundColor: '#fff',
                        flexDirection: 'column',
                        borderColor: '#ecf0f1',
                        borderRightWidth: 5,
                      }}>
                        <View style={{
                          padding: 5,
                          height: 60,
                          marginLeft: 2,
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                        }}>

                          <View style={{flexDirection: 'column'}}>
                            <Image source={{uri: `${item.user.img_avatar}`}}
                                   style={styles.avatarChatStyle}/>
                            <View
                                style={[styles.onlineIndicatorStyle, {backgroundColor: Config.primaryColor}]}/>
                          </View>
                          <View style={{
                            flexDirection: 'column',
                            marginLeft: 0,
                          }}>
                            <Text style={{
                              fontSize: 12,
                              fontWeight: 'bold',
                            }}>{item.user.real_name ? item.user.real_name : ''}</Text>
                            <Text style={{fontSize: 10}}>{item.user.title ? item.user.title : ''}</Text>
                          </View>

                        </View>
                      </View>
                  );
                }
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

            <Text style={{
              paddingTop: 1,
              paddingBottom: 3,
              paddingLeft: 5,
              fontSize: 14,
              fontWeight: 'bold',
            }}>{translate('CHAT_ROOM')}</Text>

            <FlatList
              ref={ref => this.flatList = ref}
              onContentSizeChange={() => this.flatList.scrollToEnd({ animated: true })}
              onLayout={() => this.flatList.scrollToEnd({ animated: true })}
              showsVerticalScrollIndicator={false}
              data={this.state.chatData}
              keyExtractor={(item, index) => index.toString()}
              renderItem={(item) => this.renderRow(item)}
            />

          </View>

          <View style={styles.inputTextWrapperStyle}>

            <View style={styles.inputBoxStyle1}>
              <View style={styles.iconStyle}>
                <TouchableOpacity onPress={() => {
                  this.handleApplyJob();
                }}>
                  <Ionicons name="md-attach" size={18} color={Config.primaryColor}
                            style={{ marginLeft: 20 }}/>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.inputBoxStyle1}>
              <View style={styles.iconStyle}>

                <TouchableOpacity onPress={() => {
                  this.settingOpen(setValue);
                }}>
                  <Ionicons name="md-settings" size={18} color={Config.primaryColor}
                            style={{ marginLeft: 5 }}/>
                </TouchableOpacity>

              </View>
            </View>
            <View style={styles.inputBoxStyle}>

              <TextInput style={styles.inputTextStyle}
                         placeholder={translate('type_your_message')}
                         autoCapitalize='none'
                         autoCorrect={false}
                         multiline={true}
                         underlineColorAndroid='transparent'
                         onChangeText={(chatText) => this.setState({ chatText })}
                         value={this.state.chatText}/>

            </View>
            <View style={styles.inputBoxStyle2}>
              <TouchableOpacity onPress={() => {
                this.sendMessage(0,);
              }}>
                {this.renderSendIcon()}
              </TouchableOpacity>
            </View>
          </View>

          {/* For Adding Files  */}
          <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={() => {
              this.setState({
                isJobApply: false,
              });
            }}
            visible={this.state.isJobApply}>
            <View style={styles.dialogStyle}>
              <View style={styles.dialogBoxStyle}>

                <View style={styles.applyStyle}>

                  <Ionicons name="md-attach" size={30} color='#bdc3c7' style={{ marginLeft: 10 }}/>
                  <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginLeft: 10,
                  }}>
                    <Text style={styles.text6Style}>{translate('add_file')}</Text>
                  </View>
                  <TouchableOpacity style={{
                    flex: 1,
                    flexDirection: 'column',
                    marginTop: 5,
                  }} onPress={() => this.setState({ isJobApply: false })}>
                    <View style={{
                      flex: 1,
                      flexDirection: 'column',
                      marginTop: 0,
                    }}>
                      <Image source={require('../../../../images/jobs/cross.png')}
                             style={styles.avatarStyle1}/>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{
                  marginTop: 5,
                  paddingHorizontal: 0,
                }}>

                  <View style={styles.amountInputWrapper}>
                    <Text style={styles.amountInputTitle}>{translate('FILE_NAME')}</Text>
                    <TextInput
                      underlineColorAndroid="transparent"
                      placeholder={translate('please_write')}
                      onChangeText={(text) => this.changeFileName(text)}
                      value={this.state.fileName}
                      style={{ paddingLeft: 5 }}/>
                  </View>

                  <View style={styles.amountInputWrapper}>
                    <Text style={styles.amountInputTitle}>{translate('FILE')}</Text>

                    <View style={styles.amountFileWrapper}>
                      {/* Show Added Files Here */}
                      {this.state.tmpImg !== null &&
                      <View style={{
                        flexDirection: 'row',
                        width: '100%',
                        padding: 5,
                        flexWrap: 'wrap',
                      }}>
                        <View style={{
                          width: 40,
                          height: 50,
                          justifyContent: 'center',
                        }}>
                          <Image style={{
                            width: 35,
                            height: 45,
                          }} resizeMode='contain' source={{ uri: this.state.tmpImg.uri }}/>
                        </View>
                      </View>
                      }
                      <TouchableOpacity style={styles.amountFileTitle}
                                        onPress={() => this.launchCamera()}>
                        {/* onPress={() => this.openFilePicker()} > */}
                        <Text style={{
                          color: '#fff',
                          fontSize: 12,
                        }}>{translate('BROWSE')}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                </View>

                <View style={styles.buttonWrapper}>
                  <TouchableOpacity onPress={() => this.uploadImage()}>
                    <Text style={styles.fontC}>{translate('CONFIRM_ADD')}</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </Modal>


          {/* For Settings */}
          { this.state.isSetting === true && (
            <View style={styles.dialogSettingStyle}>
              <View style={styles.dialogSettingBoxStyle}>
                <View style={{
                  marginTop: 5,
                  paddingHorizontal: 0,
                }}>

                  <View style={styles.notificationStyle}>
                    <View style={styles.notificationColumnStyle}>
                      <Text style={{
                        paddingTop: 1,
                        paddingBottom: 3,
                        paddingLeft: 20,
                        fontSize: 13,
                      }}>{translate('block_user')}</Text>
                    </View>
                    <View style={styles.notificationColumn2Style}>
                      <Switch
                        onValueChange={(value) => this.setState({ falseSwitchIsOn: value })}
                        onTintColor="#228b22" thumbTintColor="#fff"
                        value={this.state.falseSwitchIsOn}/>
                    </View>

                  </View>
                  <View style={styles.notificationStyle}>
                    <View style={styles.notificationColumnStyle}>
                      <Text style={{
                        paddingTop: 1,
                        paddingBottom: 3,
                        paddingLeft: 20,
                        fontSize: 13,
                      }}>{translate('user_mail_notification')}</Text>
                    </View>
                    <View style={styles.notificationColumn2Style}>
                      <Switch
                        onValueChange={(value) => this.setState({ falseSwitchIsOn: value })}
                        onTintColor="#228b22" thumbTintColor="#fff"
                        value={this.state.falseSwitchIsOn}/>
                    </View>
                  </View>
                  <View style={styles.notificationStyle}>
                    <View style={styles.notificationColumnStyle}>
                      <Text style={{
                        paddingTop: 1,
                        paddingBottom: 3,
                        paddingLeft: 20,
                        fontSize: 13,
                      }}>{translate('user_wechat_notification')}</Text>
                    </View>
                    <View style={styles.notificationColumn2Style}>
                      <Switch
                        onValueChange={(value) => this.setState({ falseSwitchIsOn: value })}
                        onTintColor="#228b22" thumbTintColor="#fff"
                        value={this.state.falseSwitchIsOn}/>
                    </View>
                  </View>
                  <View style={styles.notificationStyle}>
                    <View style={styles.notificationColumnStyle}>
                      <Text style={{
                        paddingTop: 1,
                        paddingBottom: 3,
                        paddingLeft: 20,
                        fontSize: 13,
                      }}>{translate('report_abuse')}</Text>
                    </View>
                  </View>
                  <View style={styles.notificationStyle}>
                    <View style={styles.notificationColumnStyle}>
                      <Text style={{
                        paddingTop: 1,
                        paddingBottom: 3,
                        paddingLeft: 20,
                        fontSize: 13,
                      }}>{translate('comment')}</Text>
                    </View>
                  </View>


                </View>


              </View>
            </View>
          )}

          {/* For Images */}
          <Modal
            transparent={true}
            supportedOrientations={['portrait', 'landscape']}
            visible={this.state.showPic}
            onRequestClose={() => console.log('')}>
            <View
              style={{
                height: '100%',
                width: '100%',
                backgroundColor: '#2C2C2C',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity onPress={() => this.setState({ showPic: false })}>
                <Image source={require('../../../../images/account_profile/close.png')} style={{
                  width: 30,
                  height: 30,
                  marginTop: 10,
                  marginRight: 10,
                }}/>
              </TouchableOpacity>

              {/* For Swiping Images */}
              <Gallery
                style={{
                  flex: 1,
                  backgroundColor: 'transparent',
                }}
                images={this.state.datas}
              />
            </View>
          </Modal>


        </View>
      );
    }
  }
}

const styles = {
  rootStyle: {
    // paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
  },

  rootStyle2: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ecf0f1',
  },

  rootStyleOpacity: {
    opacity: 0.3,
  },

  paragraph: {
    textAlign: 'center',
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'Normal',
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  createProjectWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },

  createProjectText: {
    paddingHorizontal: 5,
    color: Config.primaryColor,
    fontSize: 18,
  },

  portifolioWrapper: {
    backgroundColor: '#fff',
    paddingHorizontal: 5,
    paddingVertical: 10,
  },

  boxInsideStyle: {
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
  },

  itemNameStyle: {
    fontSize: 18,
  },

  itemImageStyle: {
    width: 350,
    height: 250,
    opacity: .2,
  },

  itemLinkStyle: {
    flexDirection: 'row',
    backgroundColor: '#59af24',
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 15,
  },

  itemLinkTextStyle: {
    color: '#fff',
    fontSize: 16,
    marginRight: 10,
  },

  rowLeftStyle: {
    flexDirection: 'row',
    marginVertical: 5,
  },

  rowRightStyle: {
    flexDirection: 'row',
    marginVertical: 5,
  },

  avatarStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginHorizontal: 0,
  },

  avatarChatStyle: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginHorizontal: 5,
  },

  infoStyle: {
    flexDirection: 'column',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  bubbleLeftStyle: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 8,
    borderTopLeftRadius: 0,
  },

  bubbleRightStyle: {
    backgroundColor: Config.primaryColor,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderTopRightRadius: 0,
    marginRight: 5,
  },

  inputTextWrapperStyle: {
    backgroundColor: '#fff',
    paddingVertical: 10,
    //   paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },

  inputBoxStyle: {
    //  flex: 1,
    width: '70%',
    height: 40,
    flexDirection: 'column',
    backgroundColor: '#f3f3f3',
    //  paddingVertical: 5,
    paddingHorizontal: 5,
  },

  inputBoxStyle1: {
    width: '10%',
    height: 40,
    flexDirection: 'column',
    backgroundColor: '#fff',
    paddingVertical: 5,
    // paddingHorizontal: 2,
  },

  inputBoxStyle2: {
    width: '10%',
    height: 40,
    flexDirection: 'column',
    backgroundColor: '#fff',
    paddingVertical: 5,
    paddingHorizontal: 5,
  },

  inputTextStyle: {
    fontSize: 14,
  },

  iconStyle: {
    flexDirection: 'row',
    paddingTop: 8,
    marginLeft: 2,
    height: 35,
  },

  container1: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
  },

  onlineIndicatorStyle: {
    width: 14,
    height: 14,
    backgroundColor: Config.primaryColor,
    borderRadius: 7,
    position: 'absolute',
    right: 9,
    bottom: 0,
    borderWidth: 1,
    borderColor: '#fff',
  },

  floatView: {
    position: 'absolute',
    width: '100%',
    //  height: 120,
    bottom: 60,
    //  left: 40,
    zIndex: 5,
  },

  notificationStyle: {
    //  backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    height: 35,
    borderColor: '#f3f3f3',
  },

  notificationColumnStyle: {
    flexDirection: 'column',
    width: '75%',
    marginLeft: 10,
  },

  notificationColumn2Style: {
    flexDirection: 'column',
    paddingRight: 15,
    width: '25%',
  },

  fontC: {
    fontSize: 12,
    color: '#fff',
  },

  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: '95%',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 7,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    borderRadius: 5,
  },

  dialogSettingStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 60,
  },

  dialogSettingBoxStyle: {
    width: '100%',
    //  height: 250,
    backgroundColor: '#fff',
    borderRadius: 3,
    //  alignItems: 'center',
    //  justifyContent: 'center',
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogBoxStyle: {
    width: 300,
    // height: 270,
    backgroundColor: '#fff',
    borderRadius: 3,
    //  alignItems: 'center',
    //  justifyContent: 'center',
  },

  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: '#e6e6e6',
  },

  amountFileWrapper: {
    margin: 5,
    height: 30,
  },

  amountInputTitle: {
    color: '#979797',
    backgroundColor: '#f6f6f6',
    paddingVertical: 2,
    paddingHorizontal: 5,
  },

  amountFileTitle: {
    right: 0,
    height: 30,
    width: 60,
    position: 'absolute',
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Config.primaryColor,
    //paddingTop: -20,
    //top: -20,
  },

  applyStyle: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    alignItems: 'center',
    marginTop: 5,
    paddingBottom: 5,
    borderColor: '#ecf0f1',
    borderBottomWidth: 1,
  },

  avatarStyle1: {
    width: 10,
    height: 10,
    // top: -15,
    resizeMode: 'contain',
    position: 'absolute',
    right: 0,
  },
};

// const mapStateToProps = state => ({
//   component: state.component
// })

function mapStateToProps(state) {
  return {
    userData: state.auth.userData
  };
}

export default connect(mapStateToProps)(DiscussRoom);
