import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { List, ListItem, SearchBar, Avatar } from "react-native-elements";

import Config from '../../../Config';
import ProjectDetailInfo from '../subs/ProjectDetailInfo';
import OfficialProjectDetailInfo from '../subs/OfficialProjectDetailInfo';

class OfficialProjectTab extends Component {
   constructor(props) {
        super(props);
        
        this.state = {
            activeTabIndex: 0,
            data: []

        }
    }


	renderOfficialRow(review) {
        return (
          <OfficialProjectDetailInfo
          review={review} navigation={this.props.navigation}
        />
        )
    }


    render() {
        return (
            <View style={styles.rootStyle}>         
                    <View style={styles.rootStyle2}>
                    <OfficialProjectDetailInfo  project={this.props.project} navigation={this.props.navigation} />
                    {/* {this.state.activeTabIndex === 0 &&
					<FlatList
                            style={{ flex: 1 }}
                            data={this.state.data}
                            renderItem={ (item) => { return this.renderOfficialRow(item) } }
                        />
						 } */}
                    </View>
               
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        paddingHorizontal: 10,
        paddingVertical: 0,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ecf0f1'
    },
    rootStyle2: {
      //  paddingHorizontal: 10,
        paddingVertical: 0,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ecf0f1'
    },
    topTabWrapperStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 5
    },

    tabControlStyle: {
        backgroundColor: '#808080',
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    },

    tabControlText: {
        color: '#fff',
        fontSize: 15,
        fontWeight: '300'
    },

    activeTabControlStyle: {
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    }
}

const mapStateToProps = state => ({
    component: state.component
})


export default connect(mapStateToProps)(OfficialProjectTab);
