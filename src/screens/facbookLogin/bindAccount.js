import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import { connect } from 'react-redux';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import HttpRequest from '../../components/HttpRequest';

const { width, height } = Dimensions.get('window');

class bindAccount extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  onBindAccount = () => {
    this.setState({ isLoading: true });
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
          // console.log('Login cancelled')
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const { accessToken } = data;

            const infoRequest = new GraphRequest(
              '/me?fields=id,name,email',
              null,
              (err, res) => {
                if (err) {
                  alert(`Error fetching data: ${err.toString()}`);
                } else {
                  // this.setState(
                  // { isLoading: false}, () => {
                  //   this.props.callbackToMain(2, '1010121212232323');
                  // })
                  this.setState({ isLoading: false }, () => {
                    this.bindFacebookAPI(res.id);
                    // this.props.callbackToMain(2, res.id);
                  });
                }
              },
            );
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      })
      .catch(() => {});
    LoginManager.logOut();
  };

  bindFacebookAPI(id) {
    this.setState({ isLoading: true });
    HttpRequest.loginWithFacebook(this.props.userData.token, id)
      .then((response) => {
        this.setState({ isLoading: false });
        if (response.data.status === 'success') {
          this.props.userLogin(this.props.userData, this.props.userData.token);
          this.props.callbackToMain(2);
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({ isLoading: false });
      });
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        {this.state.isLoading === true && (
          <View
            style={{
              top: height / 2 + 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <ActivityIndicator size="small" color={Config.primaryColor} />
          </View>
        )}

        <View>
          <View style={styles.closeButtonContainer}>
            <TouchableOpacity
              onPress={() => {
                this.props.callbackToMain('cancel');
              }}
            >
              <Image
                style={{ width: 30, height: 30, margin: 10 }}
                resizeMode="contain"
                source={require('../../../images/facebook/cross.png')}
              />
            </TouchableOpacity>
          </View>

          <View>
            <Image
              style={{ width: '100%', height: 300 }}
              resizeMode="contain"
              source={require('../../../images/facebook/fb.png')}
            />
            <View style={styles.textContainerStyle}>
              <Text style={styles.textStyle}>Email Register Successfully</Text>
              <Text style={styles.textLightStyle}>
                Now bind Facebook to enjoy access in one click
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          onPress={() => this.onBindAccount()}
          style={styles.submitButtonStyle}
        >
          <Text style={styles.submitTextStyle}>BIND YOUR ACCOUNT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
  },
  closeButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  textStyle: {
    fontSize: 24,
    // fontWeight:'500',
    fontFamily: FontStyle.Bold,
    textAlign: 'center',
    paddingTop: 10,
  },
  textLightStyle: {
    fontSize: 20,
    // fontWeight:'300',
    fontFamily: FontStyle.Regular,
    textAlign: 'center',
    padding: 20,
    paddingTop: 10,
  },
  submitButtonStyle: {
    width: width * 0.85,
    height: 50,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Config.primaryColor,
    marginBottom: 30,
  },
  submitTextStyle: {
    color: 'white',
    fontSize: 16,
    // fontWeight:'400'
    fontFamily: FontStyle.Medium,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userLogin: (userData, token) => dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(bindAccount);
