import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';

const { width, height } = Dimensions.get('window');

class CheckEmail extends Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    onBindAccount = () => {
        this.props.callbackToMain(5);
    }

    render() {
        return (
            <View style={styles.rootStyle}>

                <View>
                    <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', backgroundColor: 'transparent' }}>
                        <TouchableOpacity onPress={() => this.props.callbackToMain('close')}>
                            <Image style={{ width: 30, height: 30, margin: 10 }} resizeMode='contain' source={require('../../../images/facebook/cross.png')} />
                        </TouchableOpacity>
                    </View>

                    <View>
                        <Image style={{ width: '100%', height: 300 }} resizeMode='contain' source={require('../../../images/facebook/img.png')} />
                        <View style={{ justifyContent: 'center', alignItems: 'center', margin: 15 }}>
                            <Text style={styles.textStyle}>Please check your email</Text>
                            <Text style={styles.textLightStyle}>to complete registration</Text>
                        </View>

                    </View>
                </View>


                <TouchableOpacity onPress={() => this.onBindAccount()}
                    style={{ width: width * 0.85, height: 50, borderRadius: 3, justifyContent: 'center', alignItems: 'center', backgroundColor: Config.primaryColor, marginBottom: 30 }}>
                    <Text style={{ color: 'white', fontSize: 16, fontFamily: FontStyle.Medium }}>CLICK HERE</Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = {
    rootStyle: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textStyle: {
        fontSize: 24,
        fontFamily: FontStyle.Bold,
        textAlign: 'center',
        paddingTop: 10
    },
    textLightStyle: {
        fontSize: 20,
        fontFamily: FontStyle.Regular,
        textAlign: 'center',
        padding: 20,
        paddingTop: 10
    },

};

export default CheckEmail;
