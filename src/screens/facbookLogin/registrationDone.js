import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, ActivityIndicator } from 'react-native';
import Config from '../../Config';
import { connect } from 'react-redux';
import HttpRequest from '../../components/HttpRequest';
import FontStyle from '../../constants/FontStyle';

const { width } = Dimensions.get('window');

class RegistrationSuccess extends Component {
    static navigationOptions = {
      header: null,
    };

    constructor(props) {
      super(props);

      this.state = {
        isLoading: false,
      };
    }

    bindFacebookAPI() {
      // this.setState({ isLoading: true });
      // HttpRequest.loginWithFacebook(this.props.userData.token, this.props.facebook_id)
      //   .then((response) => {
      //     this.setState({ isLoading: false });
      //     if (response.data.status === 'success') {
      //       this.props.userLogin(this.props.userData, this.props.userData.token);
      //       this.props.callbackToMain(5);
      //     } else {
      //       alert(response.data.message);
      //     }
      //   })
      //   .catch((error) => {
      //     alert(error);
      //     this.setState({ isLoading: false });
      //   });
      this.props.callbackToMain(5);
    }

    render() {
      if (this.state.isLoading === true) {
        return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                    <ActivityIndicator />
                </View>
        );
      }
      return (
                <View style={styles.rootStyle}>
                    <View>
                        <View style={{
 width: '100%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', backgroundColor: 'transparent',
}}>
                            <View style={{ width: 30, height: 30, margin: 10 }}/>
                            {/* <TouchableOpacity onPress={() => this.props.callbackToMain('close')}> */}
                                {/* <Image style={{ width: 30, height: 30, margin: 10 }} resizeMode='contain' source={require('../../../images/facebook/cross.png')} /> */}
                            {/* </TouchableOpacity> */}
                        </View>

                        <View>
                            <Image style={{ width: '100%', height: 300 }} resizeMode='contain' source={require('../../../images/facebook/fb.png')} />
                            <View style={{ justifyContent: 'center', alignItems: 'center', margin: 15 }}>
                                <Text style={styles.textStyle}>Bind Facebook Successful</Text>
                                <Text style={styles.textLightStyle}>Now enjoy access in one click</Text>
                            </View>

                        </View>
                    </View>

                    <TouchableOpacity onPress={() => this.bindFacebookAPI()}
                        style={{
 width: width * 0.85, height: 50, borderRadius: 3, justifyContent: 'center', alignItems: 'center', backgroundColor: Config.primaryColor, marginBottom: 30,
}}>
                        <Text style={{ color: 'white', fontSize: 16, fontFamily: FontStyle.Medium }}>CONFIRM</Text>
                    </TouchableOpacity>

                </View>
      );
    }
}

const styles = {
  rootStyle: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 24,
    fontFamily: FontStyle.Medium,
    textAlign: 'center',
    paddingTop: 10,
  },
  textLightStyle: {
    fontSize: 20,
    fontFamily: FontStyle.Regular,
    textAlign: 'center',
    padding: 20,
    paddingTop: 10,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userLogin: (userData, token) => dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationSuccess);
