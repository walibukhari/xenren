/* eslint-disable global-require */
/* eslint-disable camelcase */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable max-len */
import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  Dimensions,
  ScrollView,
  ActivityIndicator,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import HttpRequest from '../../components/HttpRequest';
import FontStyle from '../../constants/FontStyle';
import Config from '../../Config';

const { width, height } = Dimensions.get('window');

class RegisterUser extends Component {
    static navigationOptions = {
      header: null,
    };

    constructor(props) {
      super(props);
      console.log('registerUser_props--->', this.props);
      this.state = {
        email: this.props.data.email ? this.props.data.email : '',
        password: '',
        confirmPassword: '',
        isLoading: false,
      };
    }

    registerUser = () => {
      if (this.state.password && this.state.confirmPassword !== '') {
        if (this.state.password === this.state.confirmPassword) {
          this.setState({ isLoading: true });
          HttpRequest.setFacebookPassword(this.props.data.token, this.state.password)
            .then((response) => {
              console.log('response--->', response);
              const result = response.data;
              if (result.status === 'success') {
                this.setState({ isLoading: false });
                this.props.userLogin(this.props.data, this.props.data.token);
                this.props.callbackToMain(5);
              } else {
                alert(result.message);
                this.setState({ isLoading: false });
              }
            })
            .catch((error) => {
              alert('Network Error', error);
              this.setState({ isLoading: false });
            });
        } else {
          alert('Password doesn\'t match');
        }
      } else { alert('Please enter password'); }
    };

    render() {
      return (
            <ScrollView style={styles.rootStyle} contentContainerStyle={{
                justifyContent: 'space-between',
                alignItems: 'center',
            }} showsVerticalScrollIndicator={false}>
                {this.state.isLoading === true &&
                    <View style={{
                        position: 'absolute', justifyContent: 'center', alignItems: 'center', top: height / 2,
                        }}>
                        <ActivityIndicator />
                    </View>
                }
                <View>
                    <View style={styles.closeButtonContainer}>
                        <TouchableOpacity onPress={() => this.props.callbackToMain('close')}>
                            <Image style={{ width: 30, height: 30, margin: 10 }} resizeMode='contain' source={require('../../../images/facebook/cross.png')} />
                        </TouchableOpacity>
                    </View>
                    <KeyboardAvoidingView behavior="position" enabled>
                    <View style={{ justifyContent: 'flex-start', alignItems: 'center' }}>
                        <Image style={{ width: '100%', height: 200 }} resizeMode='contain'
                            source={require('../../../images/facebook/img.png')} />
                        <View style={styles.textInputContainer}>
                            <View style={styles.textHeadingContainer}>
                                <Text style={styles.textHeadingStyle}>EMAIL</Text>
                            </View>
                            <TextInput style={styles.textInputStyle}
                                secureTextEntry={false}
                                underlineColorAndroid='transparent'
                                value={this.state.email}
                                onChangeText={email => this.setState({ email })}
                                editable={ this.props.data.email ? false : true }
                            />
                        </View>

                        <View style={styles.textInputContainer}>
                            <View style={styles.textHeadingContainer}>
                                <Text style={styles.textHeadingStyle}>PASSWORD</Text>
                            </View>
                            <TextInput style={styles.textInputStyle}
                                secureTextEntry={true}
                                placeholder='Please Write'
                                underlineColorAndroid='transparent'
                                onChangeText={password => this.setState({ password })}
                                value={this.state.password}
                            />
                        </View>

                        <View style={styles.textInputContainer}>
                            <View style={styles.textHeadingContainer}>
                                <Text style={styles.textHeadingStyle}>RETYPE  PASSWORD</Text>
                            </View>
                            <TextInput style={styles.textInputStyle}
                                placeholder='Please Write'
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                onChangeText={confirmPassword => this.setState({ confirmPassword })}
                                value={this.state.confirmPassword}
                            />
                        </View>

                    </View>
                    </KeyboardAvoidingView>
                </View>

                <TouchableOpacity onPress={() => this.registerUser()}
                    style={styles.submitButtonStyle}>
                    <Text style={styles.submitTextStyle}>SUBMIT</Text>
                </TouchableOpacity>
            </ScrollView>
      );
    }
}

const styles = {
  rootStyle: {
    flexDirection: 'column',
    flex: 1,
    // justifyContent:'space-between',
    // alignItems:'center'
  },
  closeButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  textInputContainer: {
    width: width * 0.85,
    justifyContent: 'center',
    alignItems: 'flex-start',
    margin: 15,
    borderColor: '#F4F4F4',
    borderWidth: 1,
    borderRadius: 3,
  },
  textHeadingContainer: {
    width: '100%',
    backgroundColor: '#F4F4F4',
  },
  textHeadingStyle: {
    width: '100%',
    fontSize: 10,
    fontFamily: FontStyle.Medium,
    textAlign: 'left',
    padding: 5,
    color: '#707070',
    paddingLeft: 10,
  },
  textInputStyle: {
    width: '100%',
    height: 35,
    fontSize: 13,
    fontFamily: FontStyle.Regular,
    textAlign: 'left',
    padding: 10,
  },
  submitButtonStyle: {
    width: width * 0.85,
    height: 50,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Config.primaryColor,
    marginTop: 12,
  },
  submitTextStyle: {
    color: 'white',
    fontSize: 16,
    fontFamily: FontStyle.Medium,
  },
};

function mapDispatchToProps(dispatch) {
  return {
    userLogin: (userData, token) => dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
  };
}

export default connect(null, mapDispatchToProps)(RegisterUser);
