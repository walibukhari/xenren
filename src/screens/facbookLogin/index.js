/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { NavigationActions } from 'react-navigation';
import BindAccount from './bindAccount';
import RegisterUser from './registerUser';
import RegistrationSuccess from './registrationDone';
import CheckEmail from './checkEmail';
import LocalData from '../../components/LocalData';

class facebookLogin extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      count: this.props.navigation.state.params.count,
      data: this.props.userData,
      facebook_id: '',
    };
  }

  countCallback = (value, data) => {
    if (value === 'close') {
      this.goToVerify();
    } else if (value === 5) {
      LocalData.setAppData({ userInfo: true });
      this.goToVerify();
    } else if (value === 'cancel') {
      LocalData.setAppData({
        bind: 'canceled',
        userInfo: true,
      });
      // this.props.navigation.goBack();
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Dashboard',
          }),
        ],
      }));
    } else {
      value === 2
        ? this.setState({ count: value, facebook_id: data })
        : this.setState({ count: value });
    }
  };

  goToVerify() {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Dashboard',
        }),
      ],
    }));
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        {this.state.count === 1 ? (
          <BindAccount callbackToMain={this.countCallback} />
        ) : this.state.count === 2 ? (
          <RegistrationSuccess
            callbackToMain={this.countCallback}
            // facebook_id={this.state.facebook_id}
          />
        ) : this.state.count === 3 ? (
          <RegisterUser
            callbackToMain={this.countCallback}
            data={this.state.data}
          />
        ) : (
          <CheckEmail callbackToMain={this.countCallback} />
        )}
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const mapStateToProps = state => ({
  userData: state.auth.userData,
});

const mapDispatchToProps = dispatch => ({
  userLogin: (userData, token) =>
    dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(facebookLogin);
