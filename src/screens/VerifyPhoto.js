/* eslint-disable react/prop-types */
/* eslint-disable global-require */
/* eslint-disable max-len */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Config from '../Config';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import HttpRequest from "../components/HttpRequest";


class VerifyPhoto extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'Verify',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const status = 0;
    HttpRequest.userIdentity(this.props.userData.token, status)
      .then((response) => {
        console.log('resp----->', response);
      })
      .catch((error) => {
        alert(error);
      });
  }

  goToMyProfile() {
    this.props.navigation.navigate('Profile');
  }

  render() {
    return (
            <View style={styles.rootStyle}>
                <View style={styles.rootStyle}>
                    <Image style={{ width: 190, height: 180 }}
                    source={require('../../images/other/verify_pic.png')} />
                    <Text style={styles.verifyPhotoTextStyle}> {translate('verify_photo')} </Text>
                    <View style={{ paddingTop: 10, width: '85%', marginTop: 15 }}>
                        <Text style={styles.verifyTextStyle}> {translate('photo_verified')} </Text>
                    </View>
                </View>
                <TouchableOpacity style={styles.okButtonStyle} onPress={() => this.goToMyProfile() }>
                    <View>
                        <Text style={{ color: 'white', fontFamily: 'Raleway-Bold', fontSize: 20 }}> {translate('OK')} </Text>
                    </View>
                </TouchableOpacity>
            </View>

    ); // return
  } // render
} // VerifyPhoto

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    paddingVertical: 20,
    alignItems: 'center',
  },
  okButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '90%',
    height: '8%',
    borderRadius: 5,
    backgroundColor: Config.primaryColor,
  },
  verifyTextStyle: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Light',
    fontSize: 25,
    justifyContent: 'center',
  },
  verifyPhotoTextStyle: {
    fontSize: 38,
    fontFamily: 'Raleway-Bold',
    color: '#404040',
    letterSpacing: 10,
    justifyContent: 'center',
    textAlign: 'center',
    paddingTop: 10,
    marginTop: 18,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
    component: state.component,
  };
}

export default connect(mapStateToProps)(VerifyPhoto);
