/* eslint-disable comma-dangle */
/* eslint-disable no-undef */
/* eslint-disable quotes */
import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { List, ListItem, SearchBar } from 'react-native-elements';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { translate } from '../i18n';

class FlatListDemo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then((res) => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false,
        });
      })
      .catch((error) => {
        this.setState({
          error,
          loading: false,
        });
      });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        seed: this.state.seed + 1,
        refreshing: true,
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 0,
          width: '86%',
          backgroundColor: '#EEEEEE',
          marginLeft: '14%',
        }}
      />
    );
  };

  renderHeader = () => {
    return <SearchBar darkTheme
                      barStyle="default"
                      searchBarStyle="minimal"
                      placeholder={translate('search')}
                      searchIcon={{ size: 24 }}
                      icon={{
                        type: 'font-awesome',
                        name: 'search',
                        marginTop: 15,
                        marginBottom: 15,
                      }}
                      cancelIcon={false}
                      containerStyle={{
                        backgroundColor: '#f7f7f7',
                        width: '100%',
                        flexDirection: 'row-reverse',
                        alignContent: 'space-between',
                        justifyContent: 'space-between',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                      }}
                      inputContainerStyle={{
                        backgroundColor: '#ffffff',
                        flexDirection: 'row-reverse',
                        alignContent: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                      }}
                      inputStyle={{
                        backgroundColor: '#ffffff',
                        width: '100%',
                      }}/>;
  };

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 1,
          borderTopWidth: 1,
          borderColor: '#EEEEEE'
        }}
      >
        <ActivityIndicator animating size="large"/>
      </View>
    );
  };

  render() {
    const lista = [
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons1.jpg',
        status: 'Open To All',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has',
      },
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons2.jpg',
        status: 'You are member',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum',
      },
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons3.jpg',
        status: 'Private',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem',
      },
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons1.jpg',
        status: 'Private',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem5',
      },
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons2.jpg',
        status: 'You are member',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      },
      {
        name: 'Chat room name',
        avatar_url: 'http://biggsources.com/img/icons3.jpg',
        status: 'Open To All',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
      },
    ];

    return (

      <List containerStyle={{
        borderTopWidth: 0,
        borderBottomWidth: 0,
        borderLeftWidth: 10,
        borderRightWidth: 10,
      }}>
        <FlatList
          data={lista}
          renderItem={({ item }) => (
            <ListItem
              roundAvatar
              title={<View style={{
                flexDirection: 'row',
                paddingLeft: 12,
                paddingTop: 0,
              }}>
                <Text style={{
                  paddingLeft: 0,
                  fontSize: 18,
                  color: 'black',
                }} h2> {`${item.name}`}
                </Text>
              </View>}
              subtitle={
                <View style={{
                  flexDirection: 'row',
                  paddingLeft: 10,
                  paddingTop: 0,
                }}>
                  <Text style={{
                    paddingLeft: 0,
                    fontSize: 12,
                    color: 'grey',
                  }}> <FontAwesome>{Icons.users}</FontAwesome> <Text style={{
                    paddingLeft: 0,
                    color: 'grey',
                  }}> 5 </Text><Text style={{
                    paddingLeft: 0,
                    color: 'green',
                  }}> {`${item.status}`}</Text> <Text> {'\n'} {`${item.subtitle}`}</Text>
                  </Text>
                </View>
              }
              avatar={{ uri: item.avatar_url }}
              avatarStyle={{
                justifyContent: 'center',
                height: 70,
                width: 70,
              }}
              containerStyle={{
                borderBottomWidth: 10,
                height: 100,
                flex: 1,
                justifyContent: 'center',
              }}
            />
          )}
          keyExtractor={item => item.subtitle}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReached={this.handleLoadMore}
          onEndReachedThreshold={2}
        />
      </List>
    );
  }
}

export default FlatListDemo;
