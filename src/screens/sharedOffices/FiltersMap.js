import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
    KeyboardAvoidingView
} from 'react-native';
import axios from 'axios';
import Config from '../../Config';
import {translate} from '../../i18n';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import CustomMarker from "../profile/CustomMarker";
import Entypo from "react-native-vector-icons/Entypo";
import MultiSelectModal from "../../components/MultiSelectModal";
import HttpRequest from "../../components/HttpRequest";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import MapView, { Marker } from 'react-native-maps';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import Feather from "react-native-vector-icons/Feather";
import {WebView} from "react-native-webview"; // remove PROVIDER_GOOGLE import if not using Google Maps


const {width, height} = Dimensions.get("window");

const MyMarker = ({ item }) => (
    <Marker
        coordinate={{
            latitude: Number(item.lat),
            longitude: Number(item.lng)
        }}>
        <View key={item.id} style={{ padding: 20, backgroundColor: 'rgba(18, 117, 142, 0.3)', borderRadius: 100 }}>
            <View style={{ borderWidth: 2, borderColor: '#fff', height: 15, width: 15, borderRadius: 100, backgroundColor: '#00BFF3'}}>
            </View>
        </View>
    </Marker>
)
const OfficeMarkers = ({ item, onPressHandler}) => (
    <Marker
        coordinate={{
            latitude: item.lat,
            longitude: item.lng
        }}
        title={item.title}
        description={item.title}
        onPress={onPressHandler}>
        <TouchableOpacity style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <View style={{
                paddingVertical: 5,
                paddingHorizontal: 15,
                borderRadius: 100,
                borderWidth: 2,
                backgroundColor: '#C4EDC5',
                borderColor: '#00B100',
                alignItems: 'center',
            }}>
                <Text style={styles.markersText}>{item.no_of_peoples} <Ionicons name="md-thumbs-up" size={15} color="#00B100" /></Text>
                <Text style={styles.markersText}>${item.monthly_price}</Text>
            </View>
            <View style={styles.arrowDown}></View>
        </TouchableOpacity>
    </Marker>
)
class Filters extends Component {
    static navigationOptions = {
        header: null,
    };


    constructor(props) {
        super(props);
        this.state = {
            geo: null,
            access_token: null,
            ip4: null,
            markers: [],
            loading: false,
            location: null,
            self: this,
            lat: null,
            lng: null,
            userInfo: this.props.userData,
            mapView:'https://maps.google.com/maps?q= 203/A Via Vincenzo Giuffrida, Catania, Italy',
            region: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0.05,
                longitudeDelta: 0.05
            },
            headerTitle: '',
            routes: [
                {
                    name: 'ChatDetail',
                    params: { chatData: '1', locale: 'en' },
                },
            ],
        }

    }

    componentDidMount() {
        let { access_token, ip4, lat, lng } = this.props.navigation.state.params;
        this.setState({
            access_token: access_token,
            ip4: ip4,
            lat: lat,
            lng: lng,
            region: {
                latitude: lat ? lat : 30.0998899,
                longitude: lng ? lng : 70.90876790,
                latitudeDelta: 0.05 / 1.1,
                longitudeDelta: 0.05 / 1.1
            }
        });
    }
    goBackPage(){
        this.props.navigation.goBack();
    }
    refreshMap() {
        this.myWebView.reload()
    }
    onNavigationStateChange = (webViewState) => {
        let self = this;
        console.log('state changed');
        console.log(webViewState.url);
        if(webViewState.url.includes('coworking')) {
            let uri = webViewState.url.split('/');
            let officeId = uri[uri.length - 1]
            console.log('office id');
            console.log(officeId);
            console.log(self.state.userInfo);
            setTimeout(() => {
                HttpRequest.getSharedOfficeDetail(self.state.userInfo.token, self.state.userInfo.id, officeId)
                    .then((response) => {
                        const result = response.data[0];
                        const imagesArr = [];
                        if (result.success === true) {

                            console.log('going to ChatDetail now');
                            self.props.navigation.navigate('ChatDetail', {chatData: result.office_details, locale: self.state.locale});
                        }
                    })
                    .catch((error) => {
                    });
            },100);
        }
    }
    render() {
        let WebViewRef;
        console.log('this.state.loading');
        console.log(this.state.loading);
        let marker = {
            'latlng': {
                latitude: 30.1575,
                longitude: 71.5249,
            },
        }
        return (
            <View style={{margin:0,backgroundColor:'transparent',borderWidth:0,borderColor:'none'}}>
                <View
                    style={{
                        backgroundColor:'transparent',borderWidth:0,borderColor:'none',
                        margin:0,
                        width: '100%',
                        position:'relative',
                        height: '100%',
                        overflow:'hidden'
                    }}
                >
                    <TouchableOpacity
                        onPress={() => this.goBackPage()}
                        style={{
                            margin:0,backgroundColor:'transparent',borderWidth:0,borderColor:'none',
                            position:'absolute',
                            top: 10,
                            zIndex:999,
                            marginLeft: 10,
                            flexDirection: "row",
                        }}
                    >
                        <Image
                            style={{width:20,height:26,backgroundColor:'#00000000',position:'relative',top:-1}}
                            source={require('../../../images/arrowLA.png')}
                        />

                        <Text
                            onPress={() => this.goBackPage()}
                            style={{
                                marginLeft: 10,
                                marginTop:0,
                                fontSize: 19.5,
                                fontWeight:'normal',
                                color: Config.primaryColor,
                                fontFamily: FontStyle.Regular,
                            }}
                        >
                            {translate('cowork_space')}
                        </Text>
                    </TouchableOpacity>
                    <WebView
                        onNavigationStateChange={this.onNavigationStateChange.bind(this)}
                        javaScriptEnabled={true}
                        startInLoadingState={true}
                        ref={(ref) => this.myWebView = ref}
                        source={{ uri: 'https://www.xenren.co/mmap' }}
                    />
                </View>
                <TouchableOpacity style={{
                    margin:0,
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: 60,
                    position: 'absolute',
                    bottom: 80,
                    right: 10,
                    height: 60,
                    backgroundColor: '#57B029',
                    borderRadius: 100,
                }}
                                  onPress={()=>this.refreshMap()}>
                    <Image source={require('../../../images/sharedOffices/refresh.png')} style={{
                        width: 60,
                        height: 60
                    }}/>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        // borderWidth:1,
                        // borderColor:'green',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        position: 'absolute',
                        bottom: 0,
                        right: 0,
                        height: 60,
                        // backgroundColor:'#fff',
                        borderRadius: 100,
                    }}
                    onPress={() => this.props.navigation.goBack(null)}
                >
                    <Image source={require('../../../images/sharedOffices/icon-2-psd.png')} style={{
                        position: "absolute",
                        bottom: 10,
                        right: 10,
                        width: 60,
                        height: 60
                    }}/>
                </TouchableOpacity>
            </View>
        );
    }
}

const style = {
    rootStyle: {
        backgroundColor: '#00000000',
        opacity:0.1
        // paddingHorizontal: 10
    },

    facilityContainer: {
        width: '95%',
        marginTop: 10,
        paddingBottom: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        flexDirection: "row",
        // alignItems:'center'
    },
}

const borderColor = '#f7f7f7';
const textAreaBorderColor = '#d4d4d4';
const multiTextColor = '#363636';
const infoTextColor = '#353635';
const checkboxBackgroundColor = '#b7b7b7';
const starsColor = '#d7d7d7';
const applyFilterBackground = '#f7f7f7';
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    arrowDown: {
        width: 10,
        height: 0,
        marginTop: -1,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderBottomWidth: 10,
        borderBottomColor: '#00B100',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderColor: '#00B100',
        transform: [{ rotate: '180deg' }]
    },
    markersText: {
        color: '#00B100',
        fontSize: 13,
        fontWeight: "bold"
    }
});

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        })
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
