/* eslint-disable max-len */
/* eslint-disable no-lone-blocks */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Text,
    View,
    Image,
    TouchableOpacity,
    Platform,
    FlatList,
    ActivityIndicator,
    PermissionsAndroid, StyleSheet,
    Dimensions,
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';
import Feather from "react-native-vector-icons/Feather";
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import {SearchBar} from 'react-native-elements';
import FooterTabs from '../FooterTabs';
import {translate} from '../../i18n';
import Config from '../../Config';
import HttpRequest from '../../components/HttpRequest';
import FontStyle from '../../constants/FontStyle';
import axios from 'axios';
import LocalData from '../../components/LocalData';
import MapView from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {Marker} from 'react-native-maps';
import { SliderBox } from 'react-native-image-slider-box';

class FilterResults extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: "Coworking Space",
            headerTintColor: Config.topNavigation.headerIconColor,
            headerTitleStyle: {
                color: Config.primaryColor,
                alignSelf: 'center',
                fontFamily: FontStyle.Regular,
                width: '100%',
            },
            headerLeft: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack();
                    }}
                    style={{
                        marginLeft: 10,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        padding: 5,
                    }}
                >
                    <SimpleLineIcons
                        size={16}
                        name="arrow-left"
                        color={Config.topNavigation.headerIconColor}
                    />
                </TouchableOpacity>
            ),
            headerRight: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('SharedOfficeFilters');
                    }}
                    style={{
                        marginLeft: 10,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        padding: 5,
                    }}
                >
                    <Feather name="filter" size={28} color={Config.primaryColor}/>
                    {/*  <FontAwesome name="filter" size={16} color={Config.primaryColor}>  </FontAwesome>*/}
                </TouchableOpacity>
            ),
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            filteredList: [],
            isLoading: true,
            access_token: '',
            search: '',
            latitude: '',
            longitude: '',
            onScroll: false,
            nextUrl: '',
            second: true,
            ip4: '',
            resp: '',
            locale: '',
            imgLoading: true,
            showMapIcon: false,
            filteredResult: [],
            view: 1,
            mapLatitude: 0,
            mapLongitude: 0,
            images: [
                'https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg',
                'https://images.pexels.com/photos/326055/pexels-photo-326055.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'https://images.unsplash.com/photo-1568909218940-9ca084ad57de?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=768&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=1024',
                'https://images.unsplash.com/photo-1545912452-8aea7e25a3d3?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=768&ixid=eyJhcHBfaWQiOjF9&ixlib=rb-1.2.1&q=80&w=1024'
            ]
        };

        this.root = this.props.component.root;
        console.log(Dimensions.get('window').width * 0.5);
    }

    componentDidMount() {
        if (this.props.navigation.state.params && this.props.navigation.state.params.filterApplied) {
            this.setState({
                showMapIcon: true
            })
        }
        LocalData.getLocale().then((response) => {
            const resp = JSON.parse(response);
            this.setState({locale: resp.locale});
        });
        this.requestAccess();
    }

    requestAccess = () => {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': translate('location_permission'),
                    'message': translate('app_need_permission') +
                        translate('show_nearest_offices'),
                },
            ).then((granted) => {
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            let lat = position.coords.latitude;
                            let long = position.coords.longitude;
                            this.setState({
                                latitude: lat,
                                longitude: long,
                                mapLatitude: lat,
                                mapLongitude: long
                            });
                            this.getAllChatRooms(this.props.userData.token, lat, long, '');
                        },
                        (error) => {
                            this.getGeoInfo();
                        },
                        {enableHighAccuracy: false, timeout: 4000, maximumAge: 1000},
                    );
                } else {
                    this.getGeoInfo();
                }
            })
                .catch((err) => {
                    this.getAllChatRooms(this.props.userData.token, '', '', '');
                });
        } else {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    let lat = position.coords.latitude;
                    let long = position.coords.longitude;
                    this.setState({
                        latitude: lat,
                        longitude: long,
                        mapLatitude: lat,
                        mapLongitude: long
                    });
                    this.getAllChatRooms(this.props.userData.token, lat, long, '');
                },
                (error) => {
                    this.getGeoInfo();
                },
                {enableHighAccuracy: false, timeout: 3000, maximumAge: 1000},
            );
        }

    };

    getGeoInfo = () => {
        const access_token = this.props.userData.token;
        axios.get(Config.getIpAddress, {
            headers: {
                Accept: 'application/json',
            },
        }).then((response) => {
            this.setState({ip4: response.data.ip});
            this.getAllChatRooms(access_token, '', '', response.data.ip);
        })
            .catch((error) => {
                this.setState({isLoading: false});
            });
    };

    applyFilter = () => {
        let highDistance = this.props.navigation.state.params.highDistance;
        let lowDistance = this.props.navigation.state.params.lowDistance;
        let countries = this.props.navigation.state.params.countries;
        let highPriceChange = this.props.navigation.state.params.highPriceChange;
        let lowPriceChange = this.props.navigation.state.params.lowPriceChange;
        let payBy = this.props.navigation.state.params.payBy;
        let payType = this.props.navigation.state.params.payType;
        console.log('enter apply filter');
        var t = this;
        let filteredResult = this.state.list.filter(function (item) {
            console.log('item');
            let countries = countries[0];
            let country =toString(item.country);
            let returnResult1 = false;
            let returnResult2 = false;
            let returnResult3 = false;
            let returnResult4 = false;
            let renderResult5 = false;

            if ((highDistance && lowDistance)) {
                if (item.total_distance < highDistance && item.total_distance > lowDistance) {
                    returnResult1 = true;
                } else {
                    returnResult1 = false;
                }
            } else {
                returnResult1 = true
            }

            if ((highPriceChange && lowPriceChange)) {
                if ((item.min_price < highPriceChange && item.max_price > lowPriceChange)) {
                    returnResult2 = true;
                } else {
                    returnResult2 = false
                }
            } else {
                returnResult2 = true
            }

            if (payType != 0) {
                if (payType == 1 && item.hot_desk > 0) {
                    returnResult3 = true
                } else if (payType == 2 && item.dedicated_desk > 0) {
                    returnResult3 = true
                } else if (payType == 3 && item.meeting_room_desk > 0) {
                    returnResult3 = true
                } else {
                    returnResult3 = false
                }
            } else {
                returnResult3 = true
            }

            if (payBy != 0) {
                if (payBy == 1 && (item.hourly_price && item.hourly_price > 0)) {
                    returnResult4 = true
                } else if (payBy == 2 && (item.hourly_price && item.daily_price > 0)) {
                    returnResult4 = true
                } else if (payBy == 3 && (item.hourly_price && item.weekly_price > 0)) {
                    returnResult4 = true
                } else if (payBy == 4 && (item.hourly_price && item.monthly_price > 0)) {
                    returnResult4 = true
                } else {
                    returnResult4 = false;
                }
            } else {
                returnResult4 = true
            }

            if (countries) {
                console.log('enter in countries');
                if (countries === country) {
                    renderResult5 = true
                } else {
                    renderResult5 = false
                }
            } else {
                renderResult5 = true;
            }

            if (returnResult1 && returnResult2 && returnResult3 && returnResult4 && renderResult5) {
                return item;
            }
        });

        console.log('filteredResult');
        console.log(filteredResult);
        this.setState({
            list: filteredResult
        })
    };

    getAllChatRooms = (access_token, lat, lang, ip) => {
        HttpRequest.getAllSharedOffices(access_token, lat, lang, ip)
            .then((response) => {
                const result = response.data.data;
                if (response.data.next_page_url !== '') {
                    this.setState({nextUrl: response.data.next_page_url});
                }
                if (response.status === 200) {
                    this.setState({
                        list: result,
                        filteredList: result,
                        listLength: result.length,
                        isLoading: false,
                    });
                    this.applyFilter();
                } else {
                    this.setState({isLoading: false});
                }
            })
            .catch((error) => {
                this.setState({isLoading: false});
            });
    };

    renderSeparator = () => (
        <View
            style={{
                height: 10,
                width: '100%',
                backgroundColor: '#f7f7f7',
                marginLeft: '0%',
            }}
        />
    );

    goToChatRoomDetail = (item) => {
        this.props.navigation.navigate('ChatDetail', {chatData: item, locale: this.state.locale});
    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 1,
                    borderTopWidth: 1,
                    borderColor: '#ecf0f1',
                }}
            >
                <ActivityIndicator animating size="large"/>
            </View>
        );
    };

    onRefresh() {
        this.setState({isLoading: true});
        this.requestAccess();
    }

    onLoad = () => {
        this.setState({imgLoading: false});
    };

    filterData = (data) => {
        this.renderRow(data);
    }

    renderRow = ({item}) => (
        <TouchableOpacity
            onPress={() => {
                this.goToChatRoomDetail(item);
            }}
            style={{
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
        {this.state.list.map(item => (
            <View style={rowStyles.wrapper}>
                <View style={[rowStyles.viewTop]}>
                    <View style={[rowStyles.box_img, rowStyles.boxImage]}>
                        <SliderBox images={item.image} circleLoop={true} />
                    </View>
                    <View style={rowStyles.container}>
                        <View style={[rowStyles.box, rowStyles.box1]}>
                            <Text style={[rowStyles.officeName]}>
                                {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.version_chinese === 'true' ? item.office_name_cn : item.office_name : item.version_english === 'false' || item.version_english === null ? item.office_name_cn : item.office_name}
                            </Text>
                            <Text style={[rowStyles.officeLocation]}>
                                <Feather name="map-pin" size={11}/>
                                {" "} {item.city + "," + item.country}
                            </Text>
                            <Text style={[rowStyles.dayAgo]}>{item.updated_at}</Text>
                        </View>
                        <View style={[rowStyles.box, rowStyles.box3]}>
                            <Text style={[rowStyles.perMonth]}>${item.monthly_price}/Month</Text>
                            <Text style={[rowStyles.review]}>({item.review_count} reviews))</Text>
                            <Rating
                                type="star"
                                startingValue={item.avg}
                                readonly
                                imageSize={15}
                                style={{
                                    right: -115,
                                    marginTop: 12
                                }}
                            />
                            {item.wifi && <Image
                                style={{
                                    width: 15,
                                    height: 15,
                                    position: "absolute",
                                    right: 0,
                                    marginTop: 70
                                }}
                                source={{uri: 'https://coworkspace.xenren.co/assets/office/wifi.png'}}
                            />}
                            {item.printer && <Image
                                style={{
                                    width: 15,
                                    height: 15,
                                    position: "absolute",
                                    right: 20,
                                    marginTop: 70
                                }}
                                source={{uri: 'https://coworkspace.xenren.co/assets/office/printer.png'}}
                            />}
                            {item.lunch && <Image
                                style={{
                                    width: 15,
                                    height: 15,
                                    position: "absolute",
                                    right: 40,
                                    marginTop: 70
                                }}
                                source={{uri: 'https://coworkspace.xenren.co/assets/office/lunch.png'}}
                            />}
                            {item.vending_machine && <Image
                                style={{
                                    width: 15,
                                    height: 15,
                                    position: "absolute",
                                    right: 60,
                                    marginTop: 70
                                }}
                                source={{uri: 'https://coworkspace.xenren.co/assets/office/vendor_machine.png'}}
                            />}
                        </View>
                    </View>
                </View>
            </View>
        ))}
            {/*<View*/}
            {/*    style={{*/}
            {/*        width: '96%',*/}
            {/*        flexDirection: 'row',*/}
            {/*        justifyContent: 'space-between',*/}
            {/*        alignItems: 'center',*/}
            {/*        marginHorizontal: 5,*/}
            {/*        marginVertical: 2,*/}
            {/*        backgroundColor: 'white',*/}
            {/*        borderWidth: 1,*/}
            {/*        borderStyle: 'solid',*/}
            {/*        borderColor: 'white',*/}
            {/*        borderRadius: 5,*/}
            {/*        shadowColor: '#000',*/}
            {/*        shadowOffset: {*/}
            {/*            width: 0,*/}
            {/*            height: 1,*/}
            {/*        },*/}
            {/*        shadowOpacity: 0.1,*/}
            {/*        shadowRadius: 2,*/}
            {/*    }}*/}
            {/*>*/}
            {/*    <View*/}
            {/*        style={{*/}
            {/*            width: '30%',*/}
            {/*            justifyContent: 'center',*/}
            {/*            alignItems: 'center',*/}
            {/*        }}*/}
            {/*    >*/}
            {/*        {this.state.imgLoading === true && (*/}
            {/*            <ActivityIndicator size="small" style={{top: 45}} color={Config.primaryColor}/>*/}
            {/*        )}*/}
            {/*        <Image*/}
            {/*            style={{*/}
            {/*                width: 60,*/}
            {/*                height: 60,*/}
            {/*                borderRadius: 30,*/}
            {/*                margin: 10,*/}
            {/*            }}*/}
            {/*            resizeMode="cover"*/}
            {/*            onLoad={this.onLoad}*/}
            {/*            source={{uri: `${Config.webUrl}${item.image}`, cache: 'force-cache'}}*/}
            {/*        />*/}
            {/*    </View>*/}

            {/*    <View*/}
            {/*        style={{*/}
            {/*            justifyContent: 'flex-start',*/}
            {/*            alignItems: 'flex-start',*/}
            {/*            width: '60%',*/}
            {/*        }}*/}
            {/*    >*/}
            {/*        <Text*/}
            {/*            style={{*/}
            {/*                marginLeft: 5,*/}
            {/*                paddingLeft: 0,*/}
            {/*                fontSize: 18,*/}
            {/*                fontWeight: '200',*/}
            {/*                color: '#4b4b4b',*/}
            {/*            }}*/}
            {/*        >*/}
            {/*            {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.version_chinese === 'true' ? item.office_name_cn : item.office_name : item.version_english === 'false' || item.version_english === null ? item.office_name_cn : item.office_name}*/}
            {/*        </Text>*/}
            {/*        <View*/}
            {/*            style={{*/}
            {/*                flexDirection: 'row',*/}
            {/*                marginLeft: 5,*/}
            {/*                marginRight: 5,*/}
            {/*                justifyContent: 'flex-start',*/}
            {/*                alignItems: 'center',*/}
            {/*                // color: '#adadad',*/}
            {/*            }}*/}
            {/*        >*/}
            {/*            <Text*/}
            {/*                style={{*/}
            {/*                    color: Config.primaryColor,*/}
            {/*                    fontSize: 12,*/}
            {/*                    marginLeft: 0,*/}
            {/*                }}*/}
            {/*            >*/}
            {/*                {item.location}*/}
            {/*            </Text>*/}
            {/*        </View>*/}
            {/*        <Text*/}
            {/*            numberOfLines={2}*/}
            {/*            style={{*/}
            {/*                color: '#808080',*/}
            {/*                fontSize: 13,*/}
            {/*                marginTop: 5,*/}
            {/*                marginLeft: 5,*/}
            {/*                marginBottom: 10,*/}
            {/*            }}*/}
            {/*        >*/}
            {/*            {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.version_chinese === 'true' ? item.office_description_cn : item.description : item.version_english === 'false' || item.version_english === null ? item.office_description_cn : item.description}*/}
            {/*        </Text>*/}
            {/*    </View>*/}
            {/*    <SimpleLineIcons*/}
            {/*        style={{marginRight: 10}}*/}
            {/*        size={20}*/}
            {/*        name="arrow-right"*/}
            {/*        color="gray"*/}
            {/*    />*/}
            {/*</View>*/}
        </TouchableOpacity>
    );

    xrenderRow = ({item}) => (
        <TouchableOpacity
            onPress={() => {
                this.goToChatRoomDetail(item);
            }}
            style={{
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <View
                style={{
                    width: '96%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginHorizontal: 5,
                    marginVertical: 2,
                    backgroundColor: 'white',
                    borderWidth: 1,
                    borderStyle: 'solid',
                    borderColor: 'white',
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.1,
                    shadowRadius: 2,
                }}
            >
                <View
                    style={{
                        width: '30%',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    {this.state.imgLoading === true && (
                        <ActivityIndicator size="small" style={{top: 45}} color={Config.primaryColor}/>
                    )}
                    <Image
                        style={{
                            width: 60,
                            height: 60,
                            borderRadius: 30,
                            margin: 10,
                        }}
                        resizeMode="cover"
                        onLoad={this.onLoad}
                        source={{uri: `${Config.webUrl}${item.image}`, cache: 'force-cache'}}
                    />
                </View>

                <View
                    style={{
                        justifyContent: 'flex-start',
                        alignItems: 'flex-start',
                        width: '60%',
                    }}
                >
                    <Text
                        style={{
                            marginLeft: 5,
                            paddingLeft: 0,
                            fontSize: 18,
                            fontWeight: '200',
                            color: '#4b4b4b',
                        }}
                    >
                        {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.version_chinese === 'true' ? item.office_name_cn : item.office_name : item.version_english === 'false' || item.version_english === null ? item.office_name_cn : item.office_name}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginLeft: 5,
                            marginRight: 5,
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            // color: '#adadad',
                        }}
                    >
                        <Text
                            style={{
                                color: Config.primaryColor,
                                fontSize: 12,
                                marginLeft: 0,
                            }}
                        >
                            {item.location}
                        </Text>
                    </View>
                    <Text
                        numberOfLines={2}
                        style={{
                            color: '#808080',
                            fontSize: 13,
                            marginTop: 5,
                            marginLeft: 5,
                            marginBottom: 10,
                        }}
                    >
                        {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.version_chinese === 'true' ? item.office_description_cn : item.description : item.version_english === 'false' || item.version_english === null ? item.office_description_cn : item.description}
                    </Text>
                </View>
                <SimpleLineIcons
                    style={{marginRight: 10}}
                    size={20}
                    name="arrow-right"
                    color="gray"
                />
            </View>
        </TouchableOpacity>
    );

    clearSearchText = () => {
    };

    reachEnd = () => {
        this.setState({onScroll: true});
        let url = '';
        let ip = '';
        if (this.state.latitude === '') {
            ip = this.state.ip4;
            url = `${this.state.nextUrl}&lat=${this.state.latitude}&lang=${this.state.longitude}&ip=${ip}&mobileApp=${true}`;
        } else {
            url = `${this.state.nextUrl}&lat=${this.state.latitude}&lang=${this.state.longitude}&ip=${ip}&mobileApp=${true}`;
        }
        let access_token = this.props.userData.token;
        this.getMoreOffices(access_token, url);
    };

    getMoreOffices = (access_token, url) => {
        HttpRequest.getMoreSharedOfficeData(access_token, url)
            .then((response) => {
                let result = response.data.data;
                if (result.last_page_url !== this.state.nextUrl) {
                    this.setState({onScroll: false, nextUrl: response.data.next_page_url});
                    const arr = [...this.state.filteredList, ...result];
                    this.setState({list: arr, filteredList: arr, listLength: arr.length});
                } else {
                    if (this.state.second === true) {
                        this.setState({onScroll: false, nextUrl: result.next_page_url, second: false});
                        const arr = [...this.state.filteredList, ...result];
                        this.setState({list: arr, filteredList: arr, listLength: arr.length});
                    } else {
                        this.setState({onScroll: false});
                    }
                }
                this.applyFilter();
            }).catch((error) => {
            this.setState({onScroll: false});
        });
    };

    gotoMap = () => {
        navigator.geolocation.getCurrentPosition((position) => {
            console.log(position);
            this.props.navigation.navigate('SharedOfficeFiltersMaps', {
                sharedOffices: this.state.list,
                userLat: position.coords.latitude,
                userLng: position.coords.longitude
            })
        }, (error) => {
            this.props.navigation.navigate('SharedOfficeFiltersMaps', {
                sharedOffices: this.state.list,
                userLat: 24.2472,
                userLng: 89.920914
            })
        }, {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 1000
        });
    };

    search = (text) => {
        this.setState({search:text});
        let search = text.toLowerCase();
        let fullList = this.state.list;
        let filteredList = fullList.filter((item) => {
            if (item.office_name !== null) {
                if (
                    item.location.toLowerCase().match(text) ||
                    item.office_name.toLowerCase().match(text)
                ) {
                    return item;
                }
            }
        });
        console.log('filteredList');
        console.log(filteredList);

        this.setState({
            list: filteredList,
            listLength: fullList.length,
            search: '',
        });

        if (!search || search === '') {
            this.setState({
                filteredList: fullList,
                listLength: fullList.length,
                search: '',
            });
        } else if (!filteredList.length) {
            this.setState({
                filteredList,
                listLength: filteredList.length,
                search: text,
            });
        } else if (Array.isArray(filteredList)) {
            this.setState({
                filteredList,
                listLength: filteredList.length,
                search: text,
            });
        }
    };

    render() {
        if (this.state.view == 1) {
            return <View style={styles.rootStyle}>
                <SearchBar
                    darkTheme
                    barStyle="default"
                    autoCorrect={false}
                    searchBarStyle="minimal"
                    placeholder={translate('search')}
                    searchIcon={{size: 24}}
                    icon={{
                        type: 'font-awesome',
                        name: 'search',
                        marginTop: 15,
                        marginBottom: 15,
                    }}
                    cancelIcon={false}
                    containerStyle={{
                        backgroundColor: '#f7f7f7',
                        width: '100%',
                        flexDirection: 'row-reverse',
                        alignContent: 'space-between',
                        justifyContent: 'space-between',
                        borderTopWidth: 0,
                        borderBottomWidth: 0,
                    }}
                    inputContainerStyle={{
                        backgroundColor: '#ffffff',
                        flexDirection: 'row-reverse',
                        alignContent: 'flex-start',
                        justifyContent: 'flex-start',
                        margin: 0,
                    }}
                    inputStyle={{
                        backgroundColor: '#ffffff',
                        width: '100%',
                    }}
                    value={this.state.search}
                    onChangeText={text => this.search(text)}
                    onCancel={this.clearSearchText}
                />
                <Text
                    style={{
                        borderTopColor: '#ecf0f1',
                        paddingBottom: 10,
                        paddingLeft: 5,
                        width: '90%',
                    }}
                >
                    {' '}
                    {this.state.list.length}{' '}{translate('room_found')}{' '}
                </Text>
                {this.state.isLoading ? (
                    <View style={[styles.rootStyle, {justifyContent: 'center'}]}>
                        <ActivityIndicator/>
                    </View>
                ) : (
                    <FlatList
                        data={this.state.list}
                        onRefresh={() => this.onRefresh()}
                        removeClippedSubviews={false}
                        refreshing={this.state.isLoading}
                        renderItem={item => this.filterData(item)}
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                        ListFooterComponent={this.renderFooter}
                        onEndReached={this.reachEnd}
                        onEndReachedThreshold={0.5}
                    />
                )}
                {this.state.onScroll === true && (
                    <View style={{alignItems: 'center', height: 40, width: 40}}>
                        <ActivityIndicator/>
                    </View>
                )}

                <FooterTabs navigation={this.props.navigation} currentIndex={0}/>

                {this.state.showMapIcon && <TouchableOpacity
                    style={{
                        // borderWidth:1,
                        // borderColor:'green',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        position: 'absolute',
                        bottom: 57,
                        right: 10,
                        height: 60,
                        // backgroundColor:'#fff',
                        borderRadius: 100,
                    }}
                    onPress={() => this.setState({
                        view: 2
                    })}
                >
                    <Image source={require('../../../images/sharedOffices/icon-2-psd.png')} style={{
                        position: "absolute",
                        bottom: 0,
                        right: 0,
                        width: 60,
                        height: 60
                    }}/>
                </TouchableOpacity>}
            </View>
        } else {
            return <View style={styles.mapContainer}>
                <MapView
                    style={styles.map}
                    region={{
                        // latitude: this.props.navigation.state.params.userLat,
                        // longitude: this.props.navigation.state.params.userLng,
                        // latitudeDelta: 0.175441674,
                        // longitudeDelta: 0.175441674
                        latitude: this.state.mapLatitude,
                        longitude: this.state.mapLongitude,
                        latitudeDelta: 0.175441674,
                        longitudeDelta: 0.175441674
                    }}
                >
                    {this.state.list.map(item => (
                        <Marker
                            coordinate={ {
                                latitude: item.lat,
                                longitude: item.lng
                            }}
                            title={item.title}
                            description={item.title}
                            onPress={() => this.props.navigation.navigate('ChatDetail', { chatData: item, locale: 'en' })}
                        >
                            <TouchableOpacity style={{
                                width: 50
                            }}>
                                <Image source={require('../../../images/sharedOffices/falgxsmall.png')}/>
                            </TouchableOpacity>
                        </Marker>
                    ))}
                </MapView>

                <TouchableOpacity
                    style={{
                        // borderWidth:1,
                        // borderColor:'green',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        position: 'absolute',
                        bottom: 0,
                        right: 0,
                        height: 60,
                        // backgroundColor:'#fff',
                        borderRadius: 100,
                    }}
                    onPress={() => this.setState({
                        view: 1
                    })}
                >
                    <Image source={require('../../../images/sharedOffices/icon-2-psd.png')} style={{
                        position: "absolute",
                        bottom: 10,
                        right: 10,
                        width: 60,
                        height: 60
                    }}/>
                </TouchableOpacity>
            </View>
        }; // return
    } // render
} // PaymentRecord

const styles = {
    rootStyle: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f7f7f7',
        alignItems: 'center',
    },

    boxInsideStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
    },

    greyedTextDescriptionStyle: {
        color: '#c3c3c3',
        fontSize: 18,
        fontWeight: '100',
    },

    mapContainer: {
        ...StyleSheet.absoluteFillObject,
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
};

const rowStyles = StyleSheet.create({
    wrapper: {
        flex: 1,
        alignItems: 'center',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    viewTop: {
        // top: 40,
    },
    container: {
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        backgroundColor: '#fff',
        maxHeight: 130,
        minHeight: 100,
        flex: 0.5,
        flexWrap: 'wrap',
        alignContent: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'flex-start', //replace with flex-end or center
    },
    box_img: {
        width: Dimensions.get('window').width - 30,
        height: 180,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    officeName: {
        fontSize: 12,
        textAlign: 'left',
        fontFamily: FontStyle.Regular,
        padding: 5
    },
    officeLocation: {
        top: 2,
        fontSize: 12,
        textAlign: 'left',
        padding: 5
    },
    dayAgo: {
        top: 3,
        fontSize: 12,
        textAlign: 'left',
        fontFamily: FontStyle.Light,
        padding: 5
    },
    perMonth: {
        textAlign: 'right',
        fontSize: 12,
        color: Config.primaryColor,
        fontFamily: FontStyle.Regular
    },
    review: {
        textAlign: 'right',
        fontSize: 10,
        fontWeight: '100',
        top: 2,
        fontFamily: FontStyle.Regular
    },
    box: {
        width: 200,
    },
    box1: {
        paddingTop: 5,
        paddingLeft: 8,
        paddingRight: 0,
        paddingBottom: 5,
    },
    box3: {
        paddingTop: 5,
        paddingLeft: 5,
        paddingRight: 8,
        paddingBottom: 5,
    },
    boxImage: {
        backgroundColor: 'red',
    },
    stretch: {
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignSelf: 'center',
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
    },
});

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setRoot: root =>
            dispatch({
                type: 'set_root',
                root,
            }),
    };
}


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FilterResults);
