import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Text,
    View,
    TouchableOpacity,
    ScrollView, TextInput, Dimensions, Image
} from 'react-native';
import Config from '../../Config';
import {translate} from '../../i18n';
import _ from "lodash";
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import CustomMarker from "../profile/CustomMarker";
import MultiSelectModal from "../../components/MultiSelectModal";
import HttpRequest from "../../components/HttpRequest";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import Feather from "react-native-vector-icons/Feather";
const {width, height} = Dimensions.get("window");
class Filters extends Component {
    static navigationOptions = {
        header: null,
    };
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            modalVisible: false,
            countries: [],
            searchCountry: '',
            selectedCountries: [],
            selectedCountriesNames: [],
            payType: 0,
            payBy: 0,
            lowDistance: 0,
            highDistance: 20,
            lowPriceChange: 10,
            highPriceChange: 60,
            daily: 0,
            hourly: 0,
            weekly: 0,
            monthly: 0,
            hotDesk: 0,
            dedicatedDesk: 0,
            meetingRoom: 0,
        };
    }

    componentDidMount(): void {
        // this.getSkillsList();
        console.log('Filters.js');
        this.getCountries();
        this.getFilterRecord(this.props.userData.token);
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        // this.render();
    }

    closeCountryModal = () => {
        this.setState({
            modalVisible: false,
        });
        return true;
    };

    goBack = () => {
        this.props.navigation.navigate('Root');
    };

    distanceRangeChange = values => {
        this.setState({
            lowDistance: values[0],
            highDistance: values[1]
        })
    };

    priceRangeChange = values => {
        this.setState({
            lowPriceChange: values[0],
            highPriceChange: values[1]
        })
    };

    getCountries() {
        HttpRequest.getOnlyCountries()
            .then((response) => {
                this.setState({
                    countries: response.data,
                });
            })
            .catch(error => {
            });
    };

    selectCountry = (item) => {
        let {countries, selectedCountries} = this.state;
        let emptyArr = selectedCountries;
        countries.map(subItem => {
            if (item.id === subItem.id) {
                !subItem.isSelect
                    ? emptyArr.push(subItem.id)
                    : emptyArr.splice(emptyArr.indexOf(subItem.id), 1);
                subItem.isSelect = !subItem.isSelect;
            }
        });
        this.setState({countries: countries, selectedCountries: emptyArr});
    };
    goBackPage(){
        this.props.navigation.goBack();
    }
    getFilterRecord = (access_token) => {
        let t = this;
        HttpRequest.getSharedOfficeFilterRecord(access_token)
            .then((response) => {
                let data = response.data;
                t.setState({
                    daily: data.daily,
                    hourly: data.hourly,
                    weekly: data.weekly,
                    monthly: data.monthly,
                    hotDesk: data.hotDeskCount,
                    dedicatedDesk: data.dedicatedDeskCount,
                    meetingRoom: data.dedicatedDeskCount,
                });
            })
            .catch((error) => {
                this.setState({ isLoading: false });
            });
    };

    renderSkills = item => {
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <TouchableOpacity
                    onPress={() => this.selectCountry(item)}
                    style={styles.renderContainer}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            color: item.isSelect ? Config.primaryColor : null
                        }}
                    >
                        {item.text}
                    </Text>

                    {item.isSelect && (
                        <MaterialIcons
                            name={"check"}
                            size={20}
                            style={{marginRight: 15}}
                            color={Config.primaryColor}
                        />
                    )}
                </TouchableOpacity>
            </ScrollView>
        );
    };

    renderSelectedCountries = (data) => {
        var t = this;
        var sc = this.state.countries.filter(function (item) {
            if (data.indexOf(item.id) > -1) {
                return item;
            }
        });
        return _.map(sc, (item, key) => {
            return (
                <View
                    style={[styles.tagInput, {
                        color: "black",
                        shadowColor: multiTextColor,
                        shadowOpacity: 0.2,
                        shadowRadius: 5,
                        shadowOffset: {
                            height: 1,
                            width: 1,
                        },
                        borderColor: '#fff'
                    }]}
                    key={key}
                    elevation={3}
                >
                    <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 12
                    }}>
                        {item.name}
                    </Text>
                </View>
            );
        });
    };

    render() {
        if (this.state.loading === true) {
            return (
                <View style={{alignItems: 'center', justifyContent: 'center', marginTop: height / 2}}>
                    <ActivityIndicator/>
                </View>
            )
        } else {
            return (
                <View>
                    <View style={style.rootStyle}>
                        {/* Back Button View */}
                        <View style={style.navigationStyle}>
                            <View style={style.navigationWrapper}>
                                <TouchableOpacity
                                    onPress={() => this.goBackPage()}
                                    style={{
                                        marginTop: 8,
                                        marginLeft: 3,
                                        flexDirection: "row",
                                    }}
                                >
                                    <Image
                                        style={{width:20,height:26,position:'relative',top:-3}}
                                        source={require('../../../images/arrowLA.png')}
                                    />

                                    <Text
                                        onPress={() => this.goBackPage()}
                                        style={{
                                            marginLeft: 10,
                                            marginTop:-1,
                                            fontSize: 19.5,
                                            fontWeight:'normal',
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Regular,
                                        }}
                                    >
                                        {translate('filter')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                <View style={styles.rootStyle}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        alignItems: 'stretch',
                        marginTop: 10
                    }}>
                        <View style={{
                            width: width,
                            height: 90,
                        }}>
                            <Text style={styles.textStyle}>Country</Text>
                            <ScrollView
                                showsHorizontalScrollIndicator={true}
                                horizontal={true}
                                style={{
                                    flexDirection: "row",
                                    marginRight: 5,
                                    border: 1,
                                    borderWidth: 1,
                                    borderRadius: 3,
                                    width: width * .9,
                                    marginLeft: 10,
                                    marginTop: 10,
                                    borderColor: textAreaBorderColor
                                }}
                            >
                                {this.renderSelectedCountries(this.state.selectedCountries)}
                                <View>
                                    <TextInput
                                        style={{
                                            flex: 1,
                                            width: 300,
                                            padding: 8,
                                            fontSize: 12,
                                            fontFamily: FontStyle.Regular
                                        }}
                                        placeholder={'Select Country'}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        underlineColorAndroid="transparent"
                                        value={''}
                                        ref={(c: any) => {
                                            this.textInputRef = c;
                                        }}
                                        onFocus={() => {
                                            let visible = this.state.modalVisible;
                                            if(visible) {
                                                this.setState({
                                                    modalVisible: false
                                                });
                                            } else {
                                                this.setState({
                                                    modalVisible: true
                                                });
                                                this.textInputRef.blur()
                                            }
                                        }}
                                    />
                                </View>

                            </ScrollView>
                        </View>
                        <View style={{
                            width: '100%',
                            height: 110,
                            borderTopWidth: 2,
                            borderColor: borderColor,
                        }}>
                            <Text style={styles.textStyle}>Distance Range</Text>
                            <TouchableOpacity style={{
                                marginLeft: 14,
                                width: width,
                                left: 0,
                            }}>
                                <MultiSlider
                                    selectedStyle={{
                                        backgroundColor: Config.primaryColor,
                                    }}
                                    unselectedStyle={{
                                        backgroundColor: '#B7B7B7',
                                    }}
                                    trackStyle={{
                                        backgroundColor: 'red',
                                    }}
                                    values={[
                                        this.state.lowDistance,
                                        this.state.highDistance,
                                    ]}
                                    touchDimensions={{
                                        height: 50,
                                        width: 50,
                                        borderRadius: 15,
                                        slipDisplacement: 200
                                    }}
                                    onValuesChange={this.distanceRangeChange}
                                    // onValuesChangeFinish={this.saveNotificationSettings}
                                    sliderLength={330}
                                    // onValuesChange={this.atleastSpentChange}
                                    min={0}
                                    max={1000}
                                    customMarker={CustomMarker}
                                    step={1}
                                    allowOverlap
                                    snapped
                                />
                            </TouchableOpacity>
                            <Text style={{
                                marginLeft: '35%',
                                color: infoTextColor,
                                fontFamily: FontStyle.Regular
                            }}>{this.state.lowDistance}km - {this.state.highDistance}km</Text>

                        </View>
                        <View style={{
                            width: '100%',
                            height: 150,
                            borderTopWidth: 2,
                            borderColor: borderColor,
                        }}>
                            <Text style={styles.textStyle}>Category</Text>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                              onPress={() => this.setState({
                                                  payType: 1
                                              })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payType == 1 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payType == 1 ? Config.primaryColor : 'gray'
                                    }]}
                                    >Hot Desk</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.hotDesk})</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                              onPress={() => this.setState({
                                                  payType: 2
                                              })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payType == 2 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payType == 2 ? Config.primaryColor : 'gray'
                                    }]}>Dedicated Desk</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.dedicatedDesk})</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}}
                                              onPress={() => this.setState({
                                                  payType: 3
                                              })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payType == 3 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payType == 3 ? Config.primaryColor : 'gray'
                                    }]}>Meeting Room</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor
                                    }]}>({this.state.meetingRoom})</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                        <View style={{
                            width: '100%',
                            height: 180,
                            borderTopWidth: 2,
                            borderColor: borderColor,
                        }}>
                            <Text style={styles.textStyle}>Pay By</Text>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={() => this.setState({
                                payBy: 1
                            })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        // color={Config.primaryColor}
                                        color={this.state.payBy == 1 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payBy == 1 ? Config.primaryColor : 'gray'
                                    }]}>Hourly</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.hourly})</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={() => this.setState({
                                payBy: 2
                            })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payBy == 2 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payBy == 2 ? Config.primaryColor : 'gray'
                                    }]}>Daily</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.daily})</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={() => this.setState({
                                payBy: 3
                            })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payBy == 3 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payBy == 3 ? Config.primaryColor : 'gray'
                                    }]}>Weekly</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.weekly})</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, flexDirection: 'row'}} onPress={() => this.setState({
                                payBy: 4
                            })}>
                                <View style={{width: 50, height: 50}}>
                                    <Ionicons
                                        size={20}
                                        color={this.state.payBy == 4 ? Config.primaryColor : checkboxBackgroundColor}
                                        name="md-checkbox"
                                        style={{
                                            position: "absolute",
                                            marginLeft: 12,
                                            marginTop: 10
                                        }}
                                    />
                                </View>
                                <View style={{width: width * .7, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 5,
                                        marginTop: 13,
                                        color: this.state.payBy == 4 ? Config.primaryColor : 'gray'
                                    }]}>Monthly</Text>
                                </View>
                                <View style={{width: 50, height: 50}}>
                                    <Text style={[styles.textStyle, {
                                        fontSize: 12,
                                        marginLeft: 20,
                                        marginTop: 13,
                                        color: checkboxBackgroundColor,
                                    }]}>({this.state.monthly})</Text>
                                </View>
                            </TouchableOpacity>

                        </View>

                        <View style={{
                            width: '100%',
                            height: 110,
                            borderTopWidth: 2,
                            borderColor: borderColor,
                        }}>
                            <Text style={styles.textStyle}>Price Range</Text>
                            <TouchableOpacity style={{
                                marginLeft: 14,
                                width: width,
                                left: 0,
                                // marginTop: 25
                            }}>
                                <MultiSlider
                                    selectedStyle={{
                                        backgroundColor: Config.primaryColor,
                                    }}
                                    unselectedStyle={{
                                        backgroundColor: '#B7B7B7',
                                    }}
                                    trackStyle={{
                                        backgroundColor: 'red',
                                    }}
                                    values={[
                                        this.state.lowPriceChange,
                                        this.state.highPriceChange,
                                    ]}
                                    touchDimensions={{
                                        height: 50,
                                        width: 50,
                                        borderRadius: 15,
                                        slipDisplacement: 200
                                    }}
                                    onValuesChange={this.priceRangeChange}
                                    sliderLength={330}
                                    // onValuesChange={this.atleastSpentChange}
                                    min={0}
                                    max={200}
                                    customMarker={CustomMarker}
                                    step={1}
                                    allowOverlap
                                    snapped
                                />
                            </TouchableOpacity>
                            <Text style={{
                                marginLeft: '35%',
                                fontFamily: FontStyle.Regular,
                                color: infoTextColor
                            }}>{this.state.lowPriceChange}USD - {this.state.highPriceChange}USD</Text>

                        </View>

                        <View style={{
                            width: '100%',
                            height: 70,
                            borderTopWidth: 2,
                            borderColor: applyFilterBackground,
                            backgroundColor: applyFilterBackground
                        }}>
                            <View
                                style={{
                                    borderBottomColor: '#e6e6e6',
                                    borderBottomWidth: 8,
                                    top: 0,
                                    padding: 10,
                                    width: "100%",
                                }}
                            >
                                <TouchableOpacity
                                    style={{
                                        width: '100%',
                                        height: 40,
                                        borderRadius: 3,
                                        backgroundColor: Config.primaryColor,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                    onPress={() => {
                                        let t  = this;
                                        let data = this.state.selectedCountries;
                                        let sc = this.state.countries.filter(function (item) {
                                            if (data.indexOf(item.id) > -1) {
                                                return item.text;
                                            }
                                        });
                                        let sc1 = sc.map(function (item) {
                                            return item.text;
                                        });
                                        this.props.navigation.navigate('FilterResults', {
                                            filterApplied: true,
                                            countries: sc1,
                                            payType: this.state.payType,
                                            payBy: this.state.payBy,
                                            lowDistance: this.state.lowDistance,
                                            highDistance: this.state.highDistance,
                                            lowPriceChange: this.state.lowPriceChange,
                                            highPriceChange: this.state.highPriceChange,
                                        });
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            color: 'white',
                                            textAlign: 'center',
                                        }}
                                    >
                                        {"Apply Filter"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>


                {this.state.modalVisible && (
                    <MultiSelectModal
                        visible={this.state.modalVisible}
                        onRequestClose={() => this.closeCountryModal()}
                        title={'Select Countries'}
                        onChangeText={searchCountry =>
                            this.setState({searchCountry})
                        }
                        onCrossPressed={() => this.closeCountryModal()}
                        value={this.state.searchCountry}
                        data={
                            this.state.countries.filter(item =>
                                item.text.includes(this.state.searchCountry)
                            )
                        }
                        renderItem={({item, index}) =>
                            this.renderSkills(item, index)
                        }
                        extraData={this.state}
                        submitPressed={() => this.closeCountryModal()}
                    />
                )}
            </ScrollView>
                </View>
            );
        }
    }
}

const borderColor = '#f7f7f7';
const textAreaBorderColor = '#d4d4d4';
const multiTextColor = '#363636';
const infoTextColor = '#353635';
const checkboxBackgroundColor = '#b7b7b7';
const starsColor = '#d7d7d7';
const applyFilterBackground = '#f7f7f7';

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

const styles = {
    rootStyle: {
        backgroundColor: "#fff",
        flex: 1,
        flexDirection: "column"
    },
    navigationStyle: {
        height: 40,
        flexDirection: "row",
        marginTop: 20,
        paddingHorizontal: 5
    },
    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%"
    },
    textStyle: {
        color: Config.primaryColor,
        fontFamily: FontStyle.Medium,
        fontSize: 16,
        marginTop: 10,
        marginLeft: 10
    },
    tagInput: {
        height: 30,
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: '#fff',
        borderColor: '#B7B7B7',
        borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 10,
        marginLeft: 7,
        marginTop: 5,
        width: 70
    },
    renderContainer: {
        height: 45,
        justifyContent: "space-between",
        flexDirection: "row",
        alignItems: "center"
    },
}

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        })
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
