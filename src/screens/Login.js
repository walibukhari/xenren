/* eslint-disable global-require */
/* eslint-disable no-undef */
/* eslint-disable prefer-destructuring */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Dimensions,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  Image,
  Platform,
  Alert,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import Pushy from 'pushy-react-native';
import FontStyle from '../constants/FontStyle';
import LocalData from '../components/LocalData';
import { translate } from '../i18n';
import Config from '../Config';
import HttpRequest from '../components/HttpRequest';
import validate from '../utilities/Validations';
import VerificationTopup from './verifyId/VerificationTopup';
import DeviceInfo from "react-native-device-info";
import VersionCheck from "react-native-version-check";


const { width } = Dimensions.get('window');

class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      password: '',
      nameFocus: false,
      error: [],
      emailFocus: false,
      passwordFocus: false,
      isLoading: false,
      isFbLoading: false,
      isShowSuccess: false,
      isShowError: false,
      successMessage: 'Login Successfully',
      errorMessage: 'Error',
    };
    this.root = this.props.component.root;
  }

  componentDidMount() {}

  gotoForgotPassword() {
    this.props.navigation.navigate('ForgotPassword');
  }

  gotoSignUp() {
    this.props.navigation.navigate('SignUp');
  }

  gotoPrivacyPolicy = () => {
    this.props.navigation.navigate('PrivacyPolicy');
  };

  gotoTermOfService() {
    this.props.navigation.navigate('TermOfService');
  }

  authenticated(result){
    if (result.status === 'success') {

      this.saveToken(result.access_token);
      // Show Dialog
      this.setState({
        isLoading: false,
        isShowSuccess: true,
        // successMessage: 'Login Successfully',
      });

      if (result.shared_office !== null) {
        this.props.sharedOffice(result.shared_office);
      } else {
        this.props.sharedOffice(null);
      }
      result.user.isBind = result.user.facebook_id !== null;
      result.user.seat_number = result.seat_number;
      this.props.userLogin(result.user, result.access_token);
      LocalData.setUserData({
        token: result.access_token,
        seat_number: result.seat_number,
        ...result.user,
      });
      setTimeout(() => {
        this.setState({
          isShowSuccess: false,
        }, () => {
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Dashboard',
              }),
            ],
          }));
        });
      }, 2000);
    } else {
      // Show Dialog
      this.setState({
        isLoading: false,
        isShowError: true,
        errorMessage: result.message,
      });
      // Hide Dialog
      setTimeout(() => {
        this.setState({
          isShowError: false,
        });
      }, 3000);
    }
  }


  login() {
    const { email, password, error } = this.state;
    error.email = validate('email', email);
    error.password = validate('password', password);
    if (error.email || error.password) {
      this.setState({ error });
      Alert.alert('Error', translate('invalid_email_password'), [
        {
          text: 'OK',
        },
      ]);
    } else {
      this.setState({ isLoading: true });
      HttpRequest.login(this.state.email, this.state.password)
        .then((response) => {
          console.log('response login');
          this.setState({ isLoading: false });
          const result = response.data;
          if(Platform.OS === 'android') {
            let deviceApkVersion = DeviceInfo.getVersion()
            console.log('android device');
            //android version check
            let androidVersion = VersionCheck.getLatestVersion({
              provider: 'playStore'
            }).then(latestVersion => {
              let myPromise = Promise.resolve(latestVersion);
              myPromise.then(
                  value => {
                    console.log('devices version: '+deviceApkVersion);
                    console.log('store version: '+ value);
                    if(deviceApkVersion !== value) {
                      console.log('version old upgrade version');
                      this.props.navigation.navigate('Upgrade');
                    } else {
                      this.authenticated(result);
                    }
                  })
            });
          } else {
            console.log('ios device');
            let deviceApkVersion = DeviceInfo.getVersion();
            console.log(deviceApkVersion);
            //android version check
            let iOSVersion = VersionCheck.getLatestVersion({
              provider: 'appStore'
            }).then(latestVersion => {
              let myPromise = Promise.resolve(latestVersion);
              myPromise.then(
                  value => {
                    console.log('devices version: '+deviceApkVersion);
                    console.log('store version: '+ value);
                    if(deviceApkVersion !== value) {
                      console.log('version old upgrade version');
                      this.props.navigation.navigate('Upgrade');
                    } else {
                      this.authenticated(result);
                    }
                  })
            });
          }
        })
        .catch((error) => {
          // alert(JSON.stringify(error))
          console.log('response login error');
          console.log(error.response.data);
          let message = 'Cannot Login, Please Try Again.';
          if (error.response !== null && error.response.data !== null) {
            message = error.response.data.message;
          }

          // Show Dialog
          this.setState({
            isLoading: false,
            isShowError: true,
            errorMessage: message,
          });

          // Hide Dialog
          setTimeout(() => {
            this.setState({
              isShowError: false,
            });
          }, 3000);
        });
    }
  }

  saveToken(token) {
    let os = 'IOS';
    if (Platform.OS === 'android') {
      os = 'Android';
    }
    Pushy.register().then(async (deviceToken) => {
      HttpRequest.deviceToken(token, deviceToken, os)
        .then((response) => {
          const result = response.data;
          if (result.status === 'success') {
            console.log('Token Saved');
          } else {
            console.log('token not saved');
          }
        });
    }).catch((err) => {
      // Handle registration errors
      alert(err);
    });
  }

  fbLogin = () => {
    LoginManager.logOut();
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then((result) => {
        if (result.isCancelled) {
          Alert.alert('Error', 'Facebook Login Cancelled', [
            {
              text: 'OK',
            },
          ]);
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const accessToken = data.accessToken;
            const infoRequest = new GraphRequest(
              '/me?fields=id,name,email',
              null,
              (err, res) => {
                if (err) {
                  Alert.alert('Error', `${err.toString()}`, [
                    {
                      text: 'OK',
                    },
                  ]);
                } else {
                  this.setState({ isFbLoading: true }, () => {
                    this.authFacebookUser(res);
                  });
                }
              },
            );
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
    LoginManager.logOut();
  };

  authFacebookUser = (data) => {
    HttpRequest.registerWithFacebook(data.name, data.email, data.id)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          // Show Dialog
          this.setState({
            isFbLoading: false,
            // isShowSuccess: true,
            // successMessage: result.message,
          });
          this.props.userLogin(result.user, result.access_token);
          LocalData.setUserData({
            token: result.access_token,
            ...result.user,
          });
          // this.setState({
          //   isShowSuccess: false,
          // });
          if (result.user.hint !== null) {
            this.props.navigation.dispatch(NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'Dashboard',
                }),
              ],
            }));
          } else {
            this.props.navigation.navigate('BindFacebook', { data, count: 3 });
          }
        } else {
          // Show Dialog
          this.setState({
            isFbLoading: false,
            isShowError: true,
            errorMessage: result.message,
          });
          // Hide Dialog
          setTimeout(() => {
            this.setState({
              isShowError: false,
            });
          }, 3000);
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({ isFbLoading: false });
      });
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        <VerificationTopup
          showModal={this.state.isFbLoading}
          showOKButton={false}
          type={'loading'}
          message={'Please Wait'}
          closeModal={() => {
            this.setState({ isFbLoading: false });
          }}
        />
        <View style={styles.headerStyle}>
          <View style={styles.headerLineStyle} />
          <Text style={styles.headerTextStyle}>{translate('sign_in')}</Text>
          <View style={styles.headerLineStyle} />
        </View>
        <View style={{ height: width / 7 }} />
        <View style={styles.formInputStyle}>
          <View
            style={[
              styles.inputWrapperStyle,
              this.state.emailFocus
                ? { borderBottomColor: Config.primaryColor }
                : {},
            ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-person" size={25} color="#000"/>
              : < Ionicons name = "ios-person" size={25} color="#000" style={{ marginBottom: -10 }} />
            }
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('email')}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              returnKeyType="next"
              underlineColorAndroid="transparent"
              onFocus={() => {
                this.setState({ emailFocus: true });
              }}
              onBlur={() => {
                this.setState({ emailFocus: false });
              }}
              onChangeText={(email) => this.setState({ email })}
              onSubmitEditing={() => {
                this.refs.input_1.focus();
              }}
              value={this.state.email}
            />
          </View>
          <View
            style={[
              styles.inputWrapperStyle,
              this.state.passwordFocus
                ? { borderBottomColor: Config.primaryColor }
                : {},
            ]}
          >
            { Platform.OS === 'android' ? <Ionicons name="ios-lock" size={25} color="#000"/> :
                <Ionicons name="ios-lock" size={30} color="#000" style={{ marginBottom: -5 }}/>
            }
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('password')}
              ref="input_1"
              autoCapitalize="none"
              autoCorrect={false}
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onFocus={() => {
                this.setState({ passwordFocus: true });
              }}
              onBlur={() => {
                this.setState({ passwordFocus: false });
              }}
              onChangeText={(password) => this.setState({ password })}
              onSubmitEditing={() => this.login()}
              value={this.state.password}
            />
          </View>
          <View style={{ height: width / 18 }} />
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={styles.signInButtonStyle}
              onPress={() => {
                this.login();
              }}
            >
              {this.state.isLoading === true && (
                <ActivityIndicator color="#fff" />
              )}
              {this.state.isLoading === false && (
                <Text style={styles.signInTextStyle}>
                  {translate('sign_in')}
                </Text>
              )}
            </TouchableOpacity>
          </View>

          <View style={{ height: width / 34 }} />

          <View
            style={{
              justifyContent: 'center',
            }}
          >
            <Text>{translate('or')}</Text>
          </View>

          <View style={{ height: width / 36 }} />

          <View style={styles.socialButtonContainerStyle}>
            <TouchableOpacity
              style={styles.socialButtonStyle}
              onPress={this.fbLogin}
            >
              <Ionicons name="logo-facebook" size={30} color="#fff" />
            </TouchableOpacity>
            {/* <TouchableOpacity style={styles.socialButtonStyle}>
              <Ionicons name="logo-twitter" size={30} color="#fff" />
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButtonStyle}>
              <Ionicons name="logo-googleplus" size={30} color="#fff" />
            </TouchableOpacity> */}
          </View>

          <View style={{ height: width / 34 }} />

          <View style={styles.forgotPasswordWrapperStyle}>
            <TouchableOpacity
              onPress={() => {
                this.gotoForgotPassword();
              }}
            >
              <Text style={styles.forgotPasswordTextStyle}>
                {translate('forgot_password')} ?
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ height: width / 6 }} />

          <View style={styles.signUpWrapperStyle}>
            <Text>{translate('dont_have_an_account')} ? </Text>
            <TouchableOpacity
              onPress={() => {
                this.gotoSignUp();
              }}
            >
              <Text style={styles.signUpButtonStyle}>
                {' '}
                {translate('sign_up')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <View style={{ height: 30 }} /> */}
        <View style={styles.footerStyle}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 12,
            }}
          >
            <Text>{translate('by_signing_up_you_agree_to_the')} </Text>
            <Text
              style={styles.footerButtonStyle}
              onPress={() => this.props.navigation.navigate('PrivacyPolicy')}
            >
              {translate('privacy_policy')}{' '}
            </Text>
            <Text>
              {translate('and')}
              {' \n'}
            </Text>
            <Text
              style={styles.footerButtonStyle}
              onPress={() => {
                this.gotoTermOfService();
              }}
            >
              {translate('term_service')}
            </Text>
          </Text>
        </View>

        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isShowSuccess}
          onRequestClose={() => this.setState({ isShowSuccess: false })}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                source={require('../../images/login/ticklogo.png')}
                style={{ height: 85 }}
                resizeMode="contain"
              />
              <View style={{ height: 12 }} />
              <Text
                style={{
                  fontFamily: FontStyle.Regular,
                  color: Config.primaryColor,
                  fontSize: 25,
                  textAlign: 'center',
                }}
              >
                Login
              </Text>
              <Text style={{
                fontFamily: FontStyle.Regular,
                color: Config.primaryColor,
                fontSize: 25,
              }}>
                Successfully
              </Text>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isShowError}
          onRequestClose={() => console.log('Modal Close')}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                source={require('../../images/login/login_error.png')}
                style={{ height: 90 }}
                resizeMode="contain"
              />
              <View style={{ height: 20 }} />
              <Text
                style={{
                  fontWeight: 'bold',
                  color: '#ff2057',
                  fontSize: 20,
                  textAlign: 'center',
                }}
              >
                {this.state.errorMessage}
              </Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },

  headerLineStyle: {
    width: 20,
    height: 2,
    marginHorizontal: 5,
    backgroundColor: Config.primaryColor,
  },

  headerTextStyle: {
    color: Config.textColor,
    fontSize: 35,
    fontWeight: 'bold',
  },

  formInputStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    width: '75%',
  },

  inputWrapperStyle: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: Config.textColor,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    marginVertical: 10,
  },

  inputStyle: {
    flex: 1,
    height: 40,
    marginLeft: 10,
  },

  signInButtonStyle: {
    backgroundColor: Config.primaryColor,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  signInTextStyle: {
    color: '#ffffff',
    fontSize: 15,
  },

  socialButtonContainerStyle: {
    flexDirection: 'row',
  },

  socialButtonStyle: {
    height: 60,
    width: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Config.textColor,
    margin: 5,
  },

  forgotPasswordWrapperStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  forgotPasswordButtonStyle: {},

  forgotPasswordTextStyle: {
    fontSize: 15,
    fontFamily: FontStyle.Medium,
    color: Config.textColor,
  },

  signUpWrapperStyle: {
    borderBottomWidth: 1,
    borderBottomColor: Config.textColor,
    padding: 10,
    flexDirection: 'row',
  },

  signUpButtonStyle: {
    fontWeight: 'bold',
    color: Config.primaryColor,
  },

  footerStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: 12,
  },

  footerButtonStyle: {
    fontWeight: 'bold',
    color: Config.textColor,
    textAlign: 'center',
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogBoxStyle: {
    width: 240,
    height: 220,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

const mapStateToProps = (state) => ({
  component: state.component,
});

const mapDispatchToProps = (dispatch) => ({
  setRoot: (root) => dispatch({
    type: 'set_root',
    root,
  }),
  userLogin: (userData, token) => dispatch({
    type: 'LOGIN_SUCCESS',
    data: {
      ...userData,
      token,
    },
  }),
  sharedOffice: (sharedOffice) => dispatch({
    type: 'SHARED_OFFICE',
    data: sharedOffice,
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
