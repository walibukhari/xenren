/* eslint-disable no-param-reassign */
/* eslint-disable react/jsx-key */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Linking,
  Modal,
  Dimensions,
  ActivityIndicator,
  Alert,
  FlatList,
  SafeAreaView
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import Gallery from 'react-native-image-gallery';
import Config from '../../../Config';
import HttpRequest from '../../../components/HttpRequest';
import Input from '../../profile/Input';
import { translate, USER_PERFERRED_LOCALE } from '../../../i18n';
import FontStyle from '../../../constants/FontStyle';
import Entypo from 'react-native-vector-icons/Entypo';

const { width, height } = Dimensions.get('window');

class Portifolio extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      filteredPortfolio: [],
      userPortfolio: [],
      jobPosition: [],
      addPortfolio: false,
      jobPositionId: '',
      projectTitle: '',
      projectUrl: '',
      projectImg: [],
      access_token: '',
      images: [],
      imageCount: 0,
      addButtonShow: false,
      showPic: false,
      datas: [],
      portfoliosArr: [],
      isModal: false,
      creatingPortFolio: false,
      isLoadings: true,
      empty: true,
      isShowSuccess: false,
      buttonColor: false,
      activeBit: this.props.userPortfolio,
    };
  }

  componentDidMount() {
    this.setState({ access_token: this.props.userData.token })
    this.getUsersPortfolio(this.props.userData.token);
  }

  getUsersPortfolio(access_token) {
    access_token = `bearer ${access_token}`;
    HttpRequest.getUsersPortfolio(access_token)
      .then((response) => {
        const result = response;
        if (result.status === 'success') {
          this.setState({
            jobPosition: result.data.job_positions,
            jobPositionId: result.data.job_positions[0].id,
            userPortfolio: result.data.portfolios,
          });
          this.setState({
            filteredPortfolio: result.data.portfolios,
            portfoliosArr: result.data.portfolios,
            isLoadings: false,
          });
          if (this.state.filteredPortfolio.length > 0) {
            this.setState({ empty: false });
          }
        }
      })
      .catch((error) => {
        this.setState({ isLoadings: false });
        console.log(error);
        alert(error);
      });
  }

  createPortfolio() {
    const access_token = `bearer ${this.state.access_token}`;
    const projectTitle = this.state.projectTitle;
    const projectUrl = this.state.projectUrl;
    const jobPositionId = this.state.jobPositionId;
    const projectImg = this.state.projectImg;
    if (projectImg.length === 0) {
        Alert.alert("Error", "Project Image is required", [
            {
                text: "OK",
            }
        ]);
    } else {
      this.setState({ creatingPortFolio: true });
      HttpRequest.portfolioCreate(
        access_token,
        jobPositionId,
        projectTitle,
        projectUrl,
        projectImg,
      )
        .then((response) => {
          if (response.data.status === 'success') {
            this.closeModal();
            this.setState({ creatingPortFolio: false, isShowSuccess: true });
            this.getUsersPortfolio(this.state.access_token);
            setTimeout(() => {
              this.setState({
                isShowSuccess: false,
              });
            }, 2000);
          } else {
              Alert.alert("Error", response.data.data.errors, [
                  {
                      text: "OK",
                  }
              ]);
            this.setState({ creatingPortFolio: false });
          }
        })
        .catch((error) => {
          this.setState({ creatingPortFolio: false });
          alert(error);
        });
    }
  }

  openPic(image) {
    const arr = this.state.portfoliosArr;
    let portfolios;
    let i;
    for (i = 0; i <= arr.length; i++) {
      if (image.portfolio_id == arr[i].id) {
        portfolios = arr[i].files;
        // console.log(portfolios);
        break;
      } else {
        // console.log(`else${arr[i].id}`);
      }
    }
    const imagesArr = [];
    portfolios.map((item) => {
      imagesArr.push({ source: { uri: item.file } });
    });
    this.setState({
      datas: imagesArr,
      showPic: true,
    });
  }

  renderImages(images) {
    return images.map(image => (
      <View
        key={image.id}
        style={{
          flexDirection: 'column',
          alignItems: 'center',
          marginBottom: 10,
        }}
      >
        <TouchableOpacity onPress={() => this.openPic(image)}>
          <Image
            source={{ uri: image.file, cache: 'force-cache' }}
            style={styles.itemImageStyle}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    ));
  }

  jobPositionFun(job_positions) {
    return this.state.jobPosition.map((item, i) => (
        <TouchableOpacity
          key={item.id}
          style={
            this.state.activeTabIndex === i
              ? {
              backgroundColor: this.state.buttonColor === true ? Config.primaryColor : '#808080',
              flexDirection: 'row',
              paddingHorizontal: 12,
              paddingVertical: 5,
              borderRadius: 15,
              marginRight: 5,
              marginTop: 2,
                }
              : styles.tabControlStyle
          }
          onPress={() => {
            this.setState({
              buttonColor: true,
              activeTabIndex: i,
              jobPositionId: item.id,
            });
            this.SearchFilterFunction(item);
          }}
        >
          <Text style={styles.tabControlText}>{item.name_en}</Text>
        </TouchableOpacity>
    ));
  }

  renderRow = (portfolio) => {
    let item = portfolio.item;
    return (
      <View style={styles.portifolioWrapper}>
        <View>
          <Text style={styles.itemNameStyle}>{item.title}</Text>
        </View>
        <View style={{ marginVertical: 10 }}>
          {this.renderImages(item.files)}
        </View>
        <View style={{ marginVertical: 10 }}>
          <TouchableOpacity
            onPress={() => this.openLink(item.url)}
            style={styles.itemLinkStyle}
          >
            <Text style={styles.itemLinkTextStyle}>{item.url}</Text>
            <FontAwesome name="external-link" color="#fff" size={20} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  openLink = (url) => {
    Linking.canOpenURL(`https://${url}`)
      .then((supported) => {
        if (supported) {
          Linking.openURL(`https://${url}`);
        } else {
            Alert.alert("Error", translate('cannot_open_link'), [
                {
                    text: "OK",
                }
            ]);
        }
      })
      .catch(err => console.error(err));
  };

  renderModal = () => (
    <Modal
      animationType="fade"
      transparent={true}
      visible={this.state.isModal}
      onRequestClose={() => console.log('Modal Closed')}
    >
      <View style={styles.verifyDialogStyle}>
        <View style={styles.verifyInfoBoxStyle}>
          <Text
            style={{
              fontSize: 17,
              fontFamily: FontStyle.Medium,
              padding: 10,
              textAlign: 'center',
              paddingBottom: 25,
            }}
          >
              {translate('upload_photo')}
          </Text>
          <View
            style={{
              width: '90%',
              height: 100,
              alignItems: 'flex-start',
            }}
          >
            <TouchableOpacity
              style={{
                width: '90%',
                height: 40,
                alignItems: 'flex-start',
                marginHorizontal: 10,
              }}
              onPress={() => this.onTakePhoto()}
            >
              <Text
                style={{
                  fontSize: 17,
                  fontFamily: FontStyle.Medium,
                  padding: 10,
                  textAlign: 'center',
                  color: Config.primaryColor,
                }}
              >
                  {translate('take_photo')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: '90%',
                height: 40,
                alignItems: 'flex-start',
                marginHorizontal: 10,
              }}
              onPress={() => this.onOpenGallery()}
            >
              <Text
                style={{
                  fontSize: 17,
                  fontFamily: FontStyle.Medium,
                  padding: 10,
                  textAlign: 'center',
                  color: Config.primaryColor,
                }}
              >
                Select from Gallery
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={{
              width: '90%',
              height: 45,
              borderRadius: 3,
              alignItems: 'center',
              justifyContent: 'center',
              marginHorizontal: 10,
              backgroundColor: Config.primaryColor,
            }}
            onPress={() => this.setState({ isModal: false })}
          >
            <Text
              style={{
                fontSize: 17,
                fontFamily: FontStyle.Medium,
                padding: 10,
                textAlign: 'center',
                color: 'white',
              }}
            >
                {translate('cancel')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );

  onSelectImage(response) {
    if (response.didCancel) {
      // console.log('User cancelled image picker');
    } else if (response.error) {
      // console.log('ImagePicker Error: ', response.error);
    } else if (response.customButton) {
      // console.log('User tapped custom button: ', response.customButton);
    } else {
      const source = { uri: response.uri };
      const imageObject = this.state.projectImg;
      if (response.fileSize <= 50000) {
          Alert.alert("Error", "Image Size is too small", [
              {
                  text: "OK",
              }
          ]);
      } else if (response.fileSize > 300000) {
          Alert.alert("Error", "File Size exceeded form 3 MB", [
              {
                  text: "OK",
              }
          ]);
      } else {
        imageObjectArr = {
          uri: response.uri,
          name: 'photo_id.jpg',
          type: 'image/jpg',
        };
        imageObject.push(imageObjectArr);
        this.setState({ projectImg: imageObject });
        this.setState({ imageCount: imageObject.length });
      }
    }
  }

  onTakePhoto = () => {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    this.setState({ isModal: false });

    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
        // console.log('User cancelled image picker');
      } else if (response.error) {
        // console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        this.onSelectImage(response);
      }
    });
  };

  onOpenGallery = () => {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    // this.setState({ imageFor: type , isModal: false});
    this.setState({ isModal: false });

    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        // console.log('User cancelled image picker');
      } else if (response.error) {
        // console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        this.onSelectImage(response);
      }
    });
  };

  removeIndex(index) {
    const arr = this.state.projectImg;
    arr.splice(index, 1);
    // console.log(arr.length);
    this.setState({
      projectImg: arr,
      imageCount: this.state.projectImg.length,
    });
    this.renderImage();
  }

  renderImage() {
    if (this.state.imageCount > 0) {
      const arrLen = this.state.projectImg.length;
      return this.state.projectImg.map((item, index) => {
        if (arrLen <= 3) {
          return (
            <View
              key={index}
              style={{
                borderColor: '#f1f1f1',
                borderWidth: 1,
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 5,
                padding: 3,
                marginTop: 3,
              }}
            >
              <View style={{ flexDirection: 'column' }}>
                <Image
                  source={{ uri: item.uri }}
                  style={{
                    height: 60,
                    width: 70,
                  }}
                />
                <TouchableOpacity
                  style={{
                    height: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Config.primaryColor,
                    paddingHorizontal: 13,
                  }}
                  onPress={() => this.removeIndex(index)}
                >
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: 12,
                      fontFamily: FontStyle.Regular,
                    }}
                  >
                      {translate('remove')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        }
      });
    }
  }

  addButtonFun() {
    const arrLen = this.state.projectImg.length;
    if (arrLen <= 2) {
      return (
        <View
          style={{
            flexDirection: 'column',
            padding: 6,
            marginTop: 3,
          }}
        >
          <TouchableOpacity
            style={{
              height: 85,
              width: 70,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Config.primaryColor,
              paddingHorizontal: 13,
            }}
            onPress={this.onOpenGallery}
          >
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
                fontFamily: FontStyle.Medium,
              }}
            >
              +
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  }

  closeModal() {
    this.setState({
      addPortfolio: false,
      projectTitle: '',
      projectUrl: '',
      imageCount: 0,
      projectImg: [],
      buttonColor: false,
    });
  }

  SearchFilterFunction(project) {
    const newData = this.state.portfoliosArr.filter((item) => {
      if (project.id === item.job_position_id) {
        return item;
      }
    });
    const newData1 = this.state.portfoliosArr.filter((item) => {
      if (project.id !== item.job_position_id) {
        return item;
      }
    });
    const data = [...newData, ...newData1];
    this.setState({
      filteredPortfolio: data,
    });
  }

  render() {
    return (
      <View style={this.state.activeBit === undefined ? styles.rootStyle : styles.rootStyleSmall}>
        {this.renderModal()}
          <Modal
              visible={this.state.isLoadings}
              animationType="fade"
              transparent={true}
              onRequestClose={() => console.log('Modal Closed')}
          >
              <View
                  style={{
                      backgroundColor: 'rgba(52, 52, 52, 0.8)',
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                  }}
              >
                  <ActivityIndicator color="#fff" />
              </View>
          </Modal>
          <Modal
              animationType="fade"
              transparent={true}
              visible={this.state.isShowSuccess}
              onRequestClose={() => console.log('Modal Close')}
          >
              <View style={styles.dialogStyle}>
                  <View style={styles.dialogBoxStyle}>
                      <Image
                          source={require('../../../../images/login/ticklogo.png')}
                          style={{ height: 85 }}
                          resizeMode="contain"
                      />
                      <View style={{ height: 12 }} />
                      <Text
                          style={{
                              fontFamily: FontStyle.Regular,
                              color: Config.primaryColor,
                              fontSize: 25,
                              textAlign: 'center',
                          }}
                      >
                          {translate('portfolio')}
                      </Text>
                      <Text style={{
                          fontFamily: FontStyle.Regular,
                          color: Config.primaryColor,
                          fontSize: 25,
                          textAlign: 'center',
                      }}>
                          {translate('upload_success')}
                      </Text>
                  </View>
              </View>
          </Modal>
          <Modal
              visible={this.state.addPortfolio}
              animationType="fade"
              transparent={true}
              onRequestClose={() => console.log('Modal Closed')}
          >
              <View
                  style={{
                      backgroundColor: 'rgba(0,0,0,0.5)',
                      flex: 1,
                      flexDirection: 'column',
                      paddingTop: '10%',
                      paddingHorizontal: 10,
                      justifyContent:'center',
                      
                  }}
              >
                  <View
                      style={{
                          borderRadius: 4,
                          height: null,
                          borderColor: '#fff',
                          borderWidth: 1,
                          backgroundColor: '#fff',
                          paddingHorizontal: 10,
                          paddingVertical: 10,
                      }}
                  >
                      <ScrollView
                          style={{ width: '100%' }}
                          contentContainerStyle={{
                              justifyContent: 'center',
                              alignItems: 'center',
                          }}
                      >
                          <View
                              style={{
                                  flexDirection: 'row',
                                  borderBottomWidth: 1,
                                  borderBottomColor: '#7f8c8d',
                              }}
                          >
                              <View style={{ flex: 0.2 }}>
                              </View>
                              <Text
                                  style={{
                                      flex: 0.6,
                                      textAlign: 'center',
                                      color: 'gray',
                                      fontSize: 18,
                                      fontFamily: FontStyle.Bold,
                                  }}
                              >
                                  {translate('add_portfolio')}
                              </Text>
                              <Text
                                  onPress={() => this.closeModal()}
                                  style={{
                                      color: Config.primaryColor,
                                      flex: 0.2,
                                      textAlign: 'right',
                                      paddingRight: 3,
                                      fontSize: 22,
                                      fontFamily: FontStyle.Bold,
                                  }}
                              >
                                  <Entypo name="cross" size={25} />
                              </Text>
                          </View>
                          <View style={styles.topTabWrapperStyle}>
                              {this.jobPositionFun(this.state.jobPosition)}
                          </View>
                          <View
                              style={{
                                  flexDirection: 'column',
                                  flex: 1,
                                  paddingVertical: 20,
                              }}
                          >
                              <Input
                                  title={translate('project_title')}
                                  placeholder={translate('graphic_design_website')}
                                  value={this.state.projectTitle}
                                  onChangeText={(projectTitle) => {
                                      this.setState({ projectTitle });
                                  }}
                              />
                              <Input
                                  title={translate('project_lnk')}
                                  placeholder="www.xenren.co"
                                  value={this.state.projectUrl}
                                  onChangeText={(projectUrl) => {
                                      this.setState({ projectUrl });
                                  }}
                              />
                              {this.state.imageCount == 0 && (
                                  <View style={styles.picHolderStyle}>
                                      <TouchableOpacity
                                          onPress={this.onOpenGallery}
                                          style={{
                                              flexDirection: 'row',
                                              paddingVertical: 10,
                                          }}
                                      >
                                          <Text
                                              style={{
                                                  alignItems: 'flex-start',
                                                  color: '#7f8c8d',
                                              }}
                                          >
                                              {translate('select_profile_image')}{' '}
                                          </Text>

                                          <Image
                                              source={require('../../../../images/account_profile/upload.png')}
                                              style={{
                                                  height: 30,
                                                  width: 30,
                                                  justifyContent: 'flex-end',
                                              }}
                                          />
                                      </TouchableOpacity>
                                  </View>
                              )}

                              {this.state.imageCount != 0 && (
                                  <View
                                      style={{
                                          flexDirection: 'row',
                                          flex: 1,
                                          paddingVertical: 20,
                                          paddingHorizontal: 10,
                                      }}
                                  >
                                      {this.renderImage()}
                                      {this.addButtonFun()}
                                  </View>
                              )}
                              <Text
                                  style={{
                                      paddingHorizontal: 10,
                                      paddingVertical: 3,
                                      color: '#7f8c8d',
                                  }}
                              >
                                  {translate('maximum_upload')}
                              </Text>
                              <TouchableOpacity
                                  style={{
                                      height: 35,
                                      justifyContent: 'center',
                                      alignItems: 'center',
                                      backgroundColor: Config.primaryColor,
                                      borderRadius: 5,
                                      marginHorizontal: 10,
                                      marginTop: 10,
                                  }}
                                  onPress={() => this.createPortfolio()}
                              >
                                  {this.state.creatingPortFolio ? (
                                      <Text style={{ color: '#fff' }}>{translate('submiting')}...</Text>
                                  ) : (
                                      <Text style={{ color: '#fff' }}>{translate('submit')}</Text>
                                  )}
                              </TouchableOpacity>
                          </View>
                      </ScrollView>
                  </View>
              </View>
          </Modal>
          <Modal
              transparent={true}
              supportedOrientations={['portrait', 'landscape']}
              visible={this.state.showPic}
              onRequestClose={() => console.log('Modal Closed')}
          >
              <SafeAreaView
                  style={{
                      height: '100%',
                      width: '100%',
                      backgroundColor: '#2C2C2C',
                  }}
              >
                  <TouchableOpacity onPress={() => this.setState({ showPic: false })}>
                      <Image
                          source={require('../../../../images/account_profile/close.png')}
                          style={{
                              width: 30,
                              height: 30,
                              marginTop: 10,
                              marginLeft: 10,
                          }}
                      />
                  </TouchableOpacity>

                  {/* For Swiping Images */}
                  <Gallery
                      style={{
                          flex: 1,
                          backgroundColor: 'transparent',
                      }}
                      images={this.state.datas}
                  />
              </SafeAreaView>
          </Modal>

        {/* Add Portfolio  */}
          {this.state.activeBit === undefined && (
              <TouchableOpacity
                  style={styles.createProjectWrapper}
                  onPress={() => this.setState({ addPortfolio: true })}
              >
                  <Ionicons name="md-add" color={Config.primaryColor} size={25} />
                  <Text style={styles.createProjectText}>
                      {translate('add_project')}
                  </Text>
              </TouchableOpacity>
          )}
          {this.state.activeBit === undefined && (
              <View style={styles.topTabWrapperStyle}>
                  <ScrollView
                      // style={styles.topTabWrapperStyle}
                      keyboardShouldPersistTaps="always"
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                  >
                      {this.jobPositionFun(this.state.jobPosition)}
                  </ScrollView>
              </View>
          )}
          {this.state.empty === true && (
              <View style={styles.emptyPortfolio}>
                  <View style={this.state.activeBit === undefined ? styles.viewClass : styles.viewClassSmall}>
                      <Text style={{
                          fontFamily: FontStyle.Light,
                          left: this.props.locale === 'cn' || this.props.locale === 'tw' || this.props.locale === 'zh' ? 45 : 0,
                      }}>
                          {translate('got_portfolio_yet')}
                      </Text>
                      <View style={{ width: 200, height: 150 }}>
                          <Image
                              source={require('../../../../images/inboxlogo_transparent.png')}
                              style={this.state.activeBit === undefined ? styles.logoClass : styles.logoClassSmall}
                              resizeMode="contain"
                          />
                      </View>
                  </View>
              </View>
          )}
        <View
          style={{
            flex: 1,
            padding: 10,
            backgroundColor: '#f7f7f7',
          }}
        >
            <FlatList
                style={{ flex: 1, padding: 5 }}
                data={this.state.filteredPortfolio}
                renderItem={item => this.renderRow(item)}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#F7F7F7',
    flex: 1,
    flexDirection: 'column',
  },

  rootStyleSmall: {
    backgroundColor: 'white',
    flex: 1,
    // height: 180,
    flexDirection: 'column',
  },
  emptyPortfolio: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },

  logoClass: {
    width: 120,
    height: 120,
    marginLeft: USER_PERFERRED_LOCALE.includes('zh') ? width / 8 : width / 3.8,
  },

  logoClassSmall: {
    width: 120,
    height: 120,
    marginLeft: USER_PERFERRED_LOCALE.includes('zh') ? width / 7.5 : width / 4,
  },

  viewClass: {
    marginTop: height / 5.5,
    flexDirection: 'column',
  },

  viewClassSmall: {
    marginTop: 20,
    flexDirection: 'column',
  },

  verifyDialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  verifyInfoBoxStyle: {
    width: '85%',
    height: 220,
    borderRadius: 10,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#F7F7F7',
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
    marginTop: 2,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 14,
    // fontWeight: 'normal'
    fontFamily: FontStyle.Regular,
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    flexDirection: 'row',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
    marginTop: 2,
  },

  createProjectWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: Config.lineColor,
  },

  createProjectText: {
    paddingHorizontal: 5,
    color: Config.primaryColor,
    fontSize: 18,
    fontFamily: FontStyle.Regular,
  },

  portifolioWrapper: {
    paddingHorizontal: 5,
    paddingVertical: 10,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 2,
  },

  itemNameStyle: {
    textAlign: 'center',
    fontSize: 18,
    color: 'gray',
    fontFamily: FontStyle.Regular,
  },

  itemHeadStyle: {
    backgroundColor: '#fcf7f7',
    color: Config.primaryColor,
    fontSize: 18,
    paddingVertical: 4,
    fontFamily: FontStyle.Medium,
  },

  itemImageStyle: {
    width: 300,
    height: 200,
    paddingHorizontal: 15,
  },

  itemLinkStyle: {
    flexDirection: 'row',
    backgroundColor: '#59af24',
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 15,
  },

  itemLinkTextStyle: {
    color: '#fff',
    fontSize: 16,
    marginRight: 10,
    fontFamily: FontStyle.Medium,
  },

  picHolderStyle: {
    paddingHorizontal: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#7f8c8d',
    marginHorizontal: 10,
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogBoxStyle: {
    width: 240,
    height: 220,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

const mapStateToProps = state => ({
  component: state.component,
  userData: state.auth.userData,
});

export default connect(mapStateToProps)(Portifolio);
