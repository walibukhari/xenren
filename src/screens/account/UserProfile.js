/* eslint-disable max-len */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Text, TouchableOpacity, Button, Image} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../../Config';
import Portifolio from './portifolio';
import Comment from './comments';
import About from './about';
import FontStyle from '../../constants/FontStyle';
import { translate } from '../../i18n';
import LocalData from "../../components/LocalData";

class UserProfile extends Component {
  static navigationOptions = {
    header: null,
  };
  // static navigationOptions = ({ navigation }) => ({
  //   title: translate('personal_info'),
  //   headerTintColor: Config.topNavigation.headerIconColor,
  //   headerTitleStyle: {
  //     color: Config.topNavigation.headerTextColor,
  //     alignSelf: 'center',
  //     fontFamily: FontStyle.Regular,
  //     width: '100%',
  //   },
  //   headerLeft: (
  //       <TouchableOpacity
  //         onPress={() => {
  //           navigation.navigate('Profile');
  //         }}
  //         style={{
  //           marginLeft: 10,
  //           flexDirection: 'row',
  //           alignSelf: 'center',
  //           padding: 5,
  //         }}
  //       >
  //         <SimpleLineIcons
  //           size={16}
  //           name="arrow-left"
  //           color={Config.topNavigation.headerIconColor}
  //         />
  //       </TouchableOpacity>
  //   ),
  //   headerRight: (
  //       <TouchableOpacity
  //         style={{
  //           marginRight: 4,
  //           flexDirection: 'row',
  //           alignSelf: 'center',
  //         }}
  //         onPress={() => {
  //           navigation.navigate('UpdateProfile');
  //         }}
  //       >
  //         <SimpleLineIcons style={{ marginRight: 8 }} size={18} name="note" color="#57af2a" />
  //       </TouchableOpacity>
  //   ),
  // });

  componentDidMount() {
    console.log('UserProfile.js');
    LocalData.getLocale().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ locale: resp.locale });
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: 0,
      locale: '',
    };
  }

  goBack = () => {
    this.props.navigation.goBack();
  };

  updateProfile = () => {
    this.props.navigation.navigate('UpdateProfile');
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        {/* Back Button View */}
        <View style={styles.navigationStyle}>
          <View style={styles.navigationWrapper}>
            <TouchableOpacity
                onPress={this.goBack}
                style={{
                  marginTop: 8,
                  marginLeft: 10,
                  flexDirection: "row",
                }}
            >
              <Image
                  style={{width:20,height:26,position:'relative',top:-3}}
                  source={require('../../../images/arrowLA.png')}
              />

              <Text
                  style={{
                    marginLeft: 10,
                    marginTop:-1,
                    fontSize: 19.5,
                    fontWeight:'normal',
                    color: Config.primaryColor,
                    fontFamily: FontStyle.Regular,
                  }}
              >
                {translate("personal_info")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* Profile Image View */}
        <View style={styles.topButtonWrapperStyle}>
          <TouchableOpacity
            onPress={() => {
              this.setState({ activeTabIndex: 0 });
            }}
            style={[
              styles.topButtonStyle,
              this.state.activeTabIndex === 0 ? styles.topButtonActiveStyle : {},
            ]}
          >
            <Text
              style={
                this.state.activeTabIndex === 0 ? styles.topTextActiveStyle : {}
              }
            >
              {translate('about')}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({ activeTabIndex: 1 });
            }}
            style={[
              styles.topButtonStyle,
              this.state.activeTabIndex === 1 ? styles.topButtonActiveStyle : {},
            ]}
          >
            <Text
              style={
                this.state.activeTabIndex === 1 ? styles.topTextActiveStyle : {}
              }
            >
              {translate('comments')}
            </Text>
          </TouchableOpacity>
          <View
            style={{
              width: 1,
              height: 50,
              backgroundColor: '#ecf0f1',
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({ activeTabIndex: 2 });
            }}
            style={[
              styles.topButtonStyle,
              this.state.activeTabIndex === 2 ? styles.topButtonActiveStyle : {},
            ]}
          >
            <Text
              style={
                this.state.activeTabIndex === 2 ? styles.topTextActiveStyle : {}
              }
            >
              {translate('portfolio')}
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.activeTabIndex === 0 && (
          <About nav={this.props.navigation} />
        )}
        {this.state.activeTabIndex === 1 && <Comment locale={this.state.locale}/>}
        {this.state.activeTabIndex === 2 && <Portifolio locale={this.state.locale}/>}
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderTopColor: '#ecf0f1',
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },

  topTextActiveStyle: {
    color: Config.primaryColor,
  },

  navigationStyle: {
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },

  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },

  backArrowStyle: {
    marginRight: 10,
  },

  backArrowIconStyle: {
    fontFamily: FontStyle.Light,
    marginLeft: 10,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserProfile);
