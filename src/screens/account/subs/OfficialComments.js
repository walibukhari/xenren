import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ImageBackground, Modal } from 'react-native';
import Swiper from 'react-native-swiper'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../../../Config';
import Gallery from 'react-native-image-gallery';
import Url from '../../../constants/BaseUrl';
import FontStyle from '../../../constants/FontStyle';
import { translate } from '../../../i18n';

class OfficialComments extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item: this.props.review,
            hasAttachments: false,
            showPic: false,
            datas: []
        }
    }

    componentDidMount() {
        if (this.state.item.hasOwnProperty('files')) {
            this.setState({ hasAttachments: true });
        }
    }

    renderStar(jumlah) {
        // console.log('render stars ---- ', jumlah)
        let array = [];
        for (let i = 1; i <= 5; i++) {
            if (i <= jumlah) {
                array.push(<Ionicons key={'star_' + i} name='md-star' color='#f1c40f' size={15} />);
            } else {
                array.push(<Ionicons key={'star_' + i} name='md-star' color='#7f8c8d' size={15} />);
            }
        }
        return (<View style={styles.starStyle}>{array}</View>);
    }

    isAttach(file) {
        if (file.length > 0) {
            return true
        } else {
            return false
        }
    }

    renderSkill(skills) {
        return skills.map((item) => {
            return (
                <View
                key={item.id}
                style={{
                    borderRadius: 15,
                    borderColor: '#f1f1f1', borderWidth: 1,
                    flexDirection: 'row', alignItems: 'center',
                    marginRight: 5,
                    padding: 3, backgroundColor: '#f9d23f'
                }}>
                    <Text style={{ paddingHorizontal: 10, color: '#fff' }}>{item.skill.name_en}</Text>
                </View>
            );
        });
    }

    openPic(item) {
        let imagesArr = [];
        imagesArr.push({ source: { uri: `https://www.xenren.co/${item.path}` } })
        this.setState({ showPic: true, datas: imagesArr }, () => {
            // console.log('Consolle  gallery image ------- ', this.state.datas)
        });
    }

    renderAttachments(attachments) {
        return attachments.map((item) => {

            let imageSource = '';
            if (item.path) {
                imageSource = { uri: `${Url.baseUrl}/` + item.path };
            } else {
                imageSource = require('../../../../images/account_profile/doc_icon.png');
            }
            return (
                <View
                key={item.id}
                style={{
                    borderColor: '#f1f1f1', borderWidth: 1,
                    flexDirection: 'row', alignItems: 'center',
                    marginRight: 10
                }}>
                    <TouchableOpacity onPress={() => this.openPic(item)}>
                        <Image source={imageSource} style={styles.imageAttachment} />
                    </TouchableOpacity>
                </View>
            );
        });
    }

    userImage(source) {
        // console.log(source);
        var imagSrc = `${Url.baseUrl}/images/avatar_xenren.png`;
        if (source) {
            imagSrc = `${Url.baseUrl}/` + source
        } else {
            imagSrc = `${Url.baseUrl}/images/avatar_xenren.png`;
        }
        return (
            <Image source={{ uri: imagSrc }} style={styles.avatarStyle} />
        )
    }

    render() {
        const { item, hasAttachments } = this.state;
        return (
            <View style={styles.rootStyle}>
                <Modal
                    transparent={true}
                    supportedOrientations={['portrait', 'landscape']}
                    visible={this.state.showPic}
                    onRequestClose={() => console.log('')}>
                    <View
                        style={{
                            height: '100%', width: '100%', backgroundColor: '#2C2C2C', alignItems: 'flex-end'
                        }}>
                        <TouchableOpacity onPress={() => this.setState({ showPic: false })}>
                            <Image source={require('../../../../images/account_profile/close.png')} style={{ width: 30, height: 30, marginTop: 10, marginRight: 10 }} />
                        </TouchableOpacity>

                        {/* For Swiping Images */}
                        <Gallery
                            style={{ flex: 1, backgroundColor: 'transparent' }}
                            images={this.state.datas}
                        />

                    </View>
                </Modal>
                <View style={{ marginHorizontal: 10, marginVertical: 5 }}>
                    <View style={styles.boxInsideStyle}>
                        <View style={styles.topSideStyle}>
                            {this.userImage(item.staff.img_avatar)}
                            <View style={{ flex: 1, flexDirection: 'column', marginLeft: 20 }}>
                                {item.is_official == 1 && <View style={{ flexDirection: 'row' }}>
                                    <Text ellipsizeMode='tail' numberOfLines={1} style={styles.text1Style}>{item.staff.username.toUpperCase()}</Text>
                                </View>}

                                <Text style={styles.text2Style}>{item.created_at}</Text>
                                <Text style={styles.text2Style}>{item.date}</Text>
                                {this.renderStar(item.score_overall)}
                            </View>
                        </View>
                        <View style={styles.middleSideStyle}>
                            <Text style={styles.textDescription}>{item.comment}</Text>
                        </View>
                        <View style={{ marginTop: 5 }}>
                            <Text style={styles.textHeader}>{translate('project_need_skill')}</Text>
                            <View style={styles.middleSideStyle}>
                                {this.renderSkill(item.skills)}
                            </View>
                        </View>
                        {this.isAttach(item.files) === true &&
                            <View style={{ marginTop: 5 }}>
                                <Text style={styles.textHeader}>{translate('attachment')}</Text>
                                <View style={styles.middleSideStyle}>
                                    {this.renderAttachments(item.files)}
                                </View>
                            </View>
                        }
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: '#fff',
        paddingHorizontal: 5,
        paddingVertical: 10,
        marginBottom: 3
    },
    boxInsideStyle: {
        flexDirection: 'column',
        backgroundColor: '#fff',
    },
    topSideStyle: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingTop: 10,
        marginBottom: 10
    },
    middleSideStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 10,
    },
    avatarStyle: {
        width: 90,
        height: 90,
        borderRadius: 45,
    },
    cardButtonStyle: {
        flex: 1,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    cardButtonTextStyle: {
        color: Config.textSecondaryColor,
        fontSize: 12,
        fontFamily: FontStyle.Regular
    },
    text1Style: {
        flex: 1,
        fontSize: 16,
        fontFamily: FontStyle.Regular
    },
    text2Style: {
        fontSize: 13,
        color: '#bdc3c7',
        fontFamily: FontStyle.Regular
    },
    text3Style: {
        fontSize: 13,
        fontFamily: FontStyle.Regular
    },
    starStyle: {
        flexDirection: 'row'
    },
    textDescription: {
        fontSize: 16,
        color: 'grey',
        fontFamily: FontStyle.Regular
    },
    textHeader: {
        fontFamily: FontStyle.Bold,
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginBottom: 5,
    },
    imageAttachment: {
        width: 80,
        height: 80
    }
}

export default OfficialComments;
