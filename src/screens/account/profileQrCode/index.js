import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Config from '../../../Config';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { translate } from '../../../i18n';
import FontStyle from '../../../constants/FontStyle';

class Comment extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('qr_details'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      params: {},
    };
  }

  componentDidMount() {
    // eslint-disable-next-line react/prop-types
    this.setState({ params: this.props.navigation.state.params });
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.containerStyle}>
          <View style={styles.profileImage}>
            <Image source={{ uri: this.state.params.img_avatar }} style={{
              width: 100,
              height: 100,
              borderRadius: 50
            }}/>
          </View>
          <View style={{
            flexDirection: 'column',
            paddingHorizontal: 10,
            paddingVertical: 8,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <Text style={{
              fontSize: 16,
              color: 'gray',
              fontFamily: FontStyle.Regular
            }}>{this.state.params.real_name}</Text>
            <View style={{ height: 60 }}/>
            <View style={{
              width: '100%',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fcf7f7',
              paddingVertical: 10,
            }}>
              <Image source={{
                uri:
                this.state.params.profile_qr_code,
              }} style={{
                alignContent: 'flex-end',
                paddingVertical: 10,
                height: 120,
                width: 120,
              }}/>
              <View style={{ height: 20 }}/>
              <Text style={{
                fontSize: 15,
                paddingVertical: 2,
                fontFamily: FontStyle.Regular
              }}>{this.state.params.real_name}</Text>
              <Text style={{
                fontSize: 13,
                color: Config.primaryColor,
                paddingVertical: 2,
                fontFamily: FontStyle.Regular
              }}>{this.state.params.job_position}</Text>
              <Text style={{
                fontSize: 13,
                color: 'gray',
                paddingVertical: 2,
                fontFamily: FontStyle.Regular
              }}>{this.state.params.title}</Text>
            </View>
            <View style={{ height: 20 }}/>
            <Text style={{
              fontSize: 18,
              fontFamily: FontStyle.Medium,
            }}>{translate('scan_to_check_qr')}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#f9f9f9',
    flex: 1,
    flexDirection: 'column',
  },
  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },
  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },
  tabControlText: {
    color: '#fff',
    fontSize: 15,
    // fontWeight: '300'
    fontFamily: FontStyle.Regular,
  },
  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },
  navigationStyle: {
    height: 40,
    flexDirection: 'row',
    paddingVertical: 10,
    marginTop: 20,
    borderBottompWidth: 2,
    borderBottomColor: '#ecf0f1',
  },
  navigationWrapper: {
    flex: 1,
    flexDirection: 'row',

  },
  backArrowStyle: {
    marginRight: 10,
  },
  backArrowIconStyle: {
    fontFamily: FontStyle.Regular,
    marginLeft: 10,
  },
  containerStyle: {

    backgroundColor: '#fff',
    flexDirection: 'column',
    flex: 1,

  },
  profileImage: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
};

const mapStateToProps = state => ({
  component: state.component,
});

export default connect(mapStateToProps)(Comment);
