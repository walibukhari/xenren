/* eslint-disable max-len */

/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable template-curly-spacing */
/* eslint-disable import/first */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
/* eslint-disable spaced-comment */
/* eslint-disable padded-blocks */
/* eslint-disable prefer-const */
/* eslint-disable object-curly-newline */
/* eslint-disable no-unused-vars */
/* eslint-disable global-require */
/* eslint-disable no-console */
/* eslint-disable object-property-newline */
/* eslint-disable semi */
/* eslint-disable comma-dangle */
/* eslint-disable object-shorthand */
/* eslint-disable indent */
/* eslint-disable arrow-parens */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import HttpRequest from '../../../components/HttpRequest';
import Config from '../../../Config';
import TextArea from '../../profile/TextArea';
import countryJsonData from '../../profile/country.json';
import LocalData from '../../../components/LocalData';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MultiSelectModal from '../../../components/MultiSelectModal';
import Loader from '../../../components/Loader';
import SuccessModal from '../../../components/SuccessModal';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { translate } from '../../../i18n';

class AddressInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countryList: countryJsonData,
      address: '',
      LanguageArr: [],
      countryArr: [],
      cityArr: [],
      cityData: '',
      selectedCountry: [],
      selectedCity: [],
      countryId: '',
      cityId: '',
      selectedLanguage: [],
      countryName: '',
      access_token: '',
      searchCountry: '',
      searchCity: '',
      searchLanguage: '',
      isLoadings: true,
      showDialog: false,
      isLoading: false,
      isShowSuccess: false,
      isShowError: false,
      countryModal: false,
      cityModal: false,
      languageModal: false,
      errorMessage: 'Error',
      slice: '',
      collection: '',
      locale: '',
    };
    this.userInfo = this.props.component.userInfo;
    this.root = this.props.component.root;
  }

  componentDidMount() {
    console.log('address tab');
    console.log(this.state.access_token);
    LocalData.getLocale().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ locale: resp.locale });
    });
    const { userInfo } = this.props.component;

    LocalData.getUserData().then(response => {
      response = JSON.parse(response);
      this.setState({ access_token: response.token });
    });

    let countryName = userInfo.country
      ? userInfo.country.name
      : translate('select_your_country');
    let cityName = userInfo.city ? userInfo.city.name : translate('select_your_city');
    let address = userInfo.address ? userInfo.address : '';

    let selectedCountryArr = this.state.selectedCountry;
    selectedCountryArr.push(this.userInfo.country_id);

    let selectedCityArr = this.state.selectedCity;
    selectedCityArr.push(this.userInfo.city_id);

    let selectedLanguage = this.state.selectedLanguage;
    userInfo.languages && userInfo.languages.length >= 1
      ? userInfo.languages.map(item => {
          selectedLanguage.push(item.language_id);
        })
      : [];

    this.setState({
      selectedCountry: selectedCountryArr,
      countryName: countryName,
      cityName: cityName,
      address: address,
      selectedCity: selectedCityArr,
      selectedLanguage: selectedLanguage
    });
    this.getlanguages();
  }

  //Get Languages
  getlanguages = () => {
    HttpRequest.getLanguages()
      .then(response => {
        let languages = response.data.data;
        let idsArr = [];
        languages.map(item => {
          idsArr.push(item.id);
          item.isSelect = false;
        });
        this.setState({ isLoadings: false });

        languages && languages.length
          ? this.userInfo.languages.map(item => {
              idsArr.indexOf(item.language_id) > -1
                ? (languages[idsArr.indexOf(item.language_id)].isSelect = true)
                : null;
            })
          : null;
        this.setState({ LanguageArr: languages });
      })
      .catch(error => {
        this.setState({ isLoadings: false });
        alert(error);
      });
  };

  closeLanguageModal = () => {
    this.setState({ languageModal: false });
  };

  //Languages
  selectedLanguages = item => {
    let { LanguageArr, selectedLanguage } = this.state;
    let emptyArr = selectedLanguage;
    LanguageArr.map(subItem => {
      if (item.id === subItem.id) {
        !subItem.isSelect
          ? emptyArr.push(subItem.id)
          : emptyArr.splice(emptyArr.indexOf(subItem.id), 1);
        subItem.isSelect = !subItem.isSelect;
      }
    });
    this.setState({ LanguageArr: LanguageArr, selectedLanguage: emptyArr });
  };

  renderLanguages = item => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => this.selectedLanguages(item)}
          style={styles.renderContainer}
        >
          <Text
            style={{
              fontSize: 14,
              color: item.isSelect ? Config.primaryColor : null
            }}
          >
            { this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.name_cn : item.name }
          </Text>

          {item.isSelect && (
            <MaterialIcons
              name={"check"}
              size={20}
              style={{ marginRight: 15 }}
              color={Config.primaryColor}
            />
          )}
        </TouchableOpacity>
      </ScrollView>
    );
  };

  //CITIES
  onSelectCity = (data, detail) => {
    if (detail !== null) {
      let city = [];
      let state = [];
      let country = [];
      let selectedPlace = [];
      detail.address_components.map((item) => {
        if (item.types[0] === 'country') {
          this.setState({ countryName: item.long_name });
          country = {
            long_name: item.long_name,
            short_name: item.short_name
          };
        }
        if (item.types[0] === 'administrative_area_level_1') {
          state = {
            long_name: item.long_name,
            short_name: item.short_name
          };
        }
        if (item.types[0] === 'locality') {
          this.setState({ cityName: item.long_name });
          city = {
            long_name: item.long_name,
            short_name: item.short_name
          };
        }
      });
      selectedPlace.push({
        address: detail.address_components,
        formatted_address: detail.formatted_address,
        name: detail.name,
        city: city,
        country: country,
        state: state,
        lat: detail.geometry.location.lat,
        lng: detail.geometry.location.lng
      });
      this.setState({ collection: selectedPlace[0] });
    }
  };

  //USER_INFO
  getUsersInfo = access_token => {
    HttpRequest.getUserProfile(access_token)
      .then(response => {
        const result = response.data;
        if (result.status === 'success') {
          this.props.component.userInfo = result.data;
          this.setState({
            userDetails: result.data,
            loading: false,
            activeTabIndex: 0
          });
        } else {
          this.setState({
            loading: false,
            activeTabIndex: 0
          });
          alert(translate('network_error'));
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({
          loading: false,
          activeTabIndex: 0
        });
      });
  };

  //HIT API
  addAddress() {
    const { countryId, cityId, selectedLanguage, collection } = this.state;
    if (this.state.address === '') {
      this.setState({ isLoading: false });
      alert(translate('address_cant_be_blank'));
    } else if (selectedLanguage.length <= 0) {
      this.setState({ isLoading: false });
      alert(translate('select_alleast_one_language'));
    } else {
      this.setState({ isLoading: true });
      let country = countryId;
      let city = cityId;
      let address = this.state.address;
      let language = selectedLanguage;
      let access_token = `Bearer ${this.state.access_token}`;
      language = language.toString();
      if (country === null || city === null) {
        this.setState({ isLoading: false });
        alert(translate('select_country_city'));
      } else {
        console.log(this.state.access_token);
        HttpRequest.updateUsersAddress(
          access_token,
          collection,
          '',
          address,
          language
        )
          .then(response => {
            const result = response;
            this.setState({
              isLoading: false,
              isShowSuccess: true
            });
            setTimeout(() => {
              this.setState({
                isShowSuccess: false
              });
            }, 3000);
            this.getUsersInfo(access_token);
          })
          .catch((error) => {
           alert(error);
          });
      }
    }
  }

  render() {
    const { languageModal, isLoadings } = this.state;
    return (
      <ScrollView style={styles.rootStyle}>
        <Loader isLoading={isLoadings} />
        <SuccessModal visible={this.state.isShowSuccess} />

        <View style={styles.container}>
          <View style={styles.innerContainer}>
            <View style={styles.subContainer}>
              <Text style={styles.titleTextStyle}>{translate('city')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ cityModal: true })}
                style={styles.selectButtonStyle}
              >
                <GooglePlacesAutocomplete
                    placeholder={translate('type_city')}
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'search'}
                    listViewDisplayed={false}
                    fetchDetails={true}
                    onPress={(data, details = null) => {
                      this.onSelectCity(data, details)
                    }}
                    getDefaultValue={() =>
                        `${this.props.component.userInfo.city !== null ? this.props.component.userInfo.city.name : ''}`
                    }
                    styles={{
                      textInputContainer: {
                        backgroundColor: 'rgba(0,0,0,0)',
                        borderTopWidth: 0,
                        borderBottomWidth: 0
                      },
                      textInput: {
                        marginLeft: 0,
                        marginRight: 0,
                        height: 34,
                        fontSize: 14,
                        color: '#8b8b8b'
                      },
                      container: {
                        right: 10
                      },
                      poweredContainer: {
                        height: 0.1,
                      },
                      powered: {
                        display: 'none',
                      },
                    }}
                    query={{
                      key: Config.GoogleApiKey,
                      language: 'en',
                      types: '(cities)'
                    }}
                    nearbyPlacesAPI='GooglePlacesSearch'
                    debounce={200}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.innerContainer}>
            <View style={styles.subContainer}>
              <Text style={styles.titleTextStyle}>{translate('COUNTRY')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                  onPress={() => this.setState({ countryModal: true })}
                  style={styles.selectButtonStyle}
              >
                <Text style={{ paddingVertical: 12 }}>
                  {this.state.selectedCountry.length
                      ? this.state.countryName
                      : ''}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <TextArea
            title={translate('ADDRESS')}
            placeholder={translate('please_write')}
            value={this.state.address}
            onChangeText={address => {
              this.setState({ address });
            }}
          />
          <View style={styles.innerContainer}>
            <View style={styles.subContainer}>
              <Text style={styles.titleTextStyle}>{translate('LANGUAGES')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ languageModal: true })}
                style={{
                  paddingHorizontal: 10,
                  borderBottomLeftRadius: 5,
                  borderBottomRightRadius: 5,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <Text style={{ paddingVertical: 12 }}>
                  {this.state.selectedLanguage.length + translate('select_languages')}
                </Text>
                <MaterialIcons
                  name={"arrow-drop-down"}
                  size={28}
                  color={"grey"}
                />
              </TouchableOpacity>

              {languageModal && (
                <MultiSelectModal
                  visible={languageModal}
                  onRequestClose={() => this.closeLanguageModal()}
                  title={translate('select_your_languages')}
                  onChangeText={searchLanguage =>
                    this.setState({ searchLanguage })
                  }
                  onCrossPressed={() => this.closeLanguageModal()}
                  value={this.state.searchLanguage}
                  data={this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ?
                      this.state.LanguageArr.filter(item =>
                          item.name_cn
                              .toLowerCase()
                              .includes(this.state.searchLanguage.toLowerCase())
                      )
                      :
                      this.state.LanguageArr.filter(item =>
                          item.name
                              .toLowerCase()
                              .includes(this.state.searchLanguage.toLowerCase())
                      )}
                  renderItem={({ item, index }) =>
                    this.renderLanguages(item, index)
                  }
                  extraData={this.state}
                  submitPressed={() => this.closeLanguageModal()}
                />
              )}
            </View>
          </View>

          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => this.addAddress()}
          >
            {this.state.isLoading === true && (
              <ActivityIndicator color="#fff" />
            )}
            {this.state.isLoading === false && (
              <Text style={{ color: "#fff" }}>{translate('submit')}</Text>
            )}
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    component: state.component
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: "set_root",
        root: root
      })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressInfo);
