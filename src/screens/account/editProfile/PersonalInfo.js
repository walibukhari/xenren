/* eslint-disable global-require */
/* eslint-disable no-empty */
/* eslint-disable max-len */
/* eslint-disable no-useless-concat */
/* eslint-disable prefer-template */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';
import {
  Text,
  View,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
  Platform
} from 'react-native';
import Config from '../../../Config';
import Input from '../../profile/Input';
import TextArea from '../../profile/TextArea';
import countryJsonData from '../../profile/country.json';
import HttpRequest from '../../../components/HttpRequest';
import FontStyle from '../../../constants/FontStyle';
import Url from '../../../constants/BaseUrl';
import { translate } from '../../../i18n';
import SuccessModal from '../../../components/SuccessModal';

class PersonalInfo extends Component {
  constructor(props) {
    super(props);
    this.root = this.props.component.root;
    // this.state.userInfo = this.props.component.userInfo;
    this.state = {
      realName: "",
      gender: "M",
      genderData: [
        {
          id: "M",
          text: "Male"
        },
        {
          id: "F",
          text: "Female"
        }
      ],
      country: "HK",
      countryList: countryJsonData,
      idCard: "",
      address: "",
      birthDate: "2017-01-01",
      handphone: "",
      email: "",
      aboutMe: "",
      nickName: "",
      qq: "",
      wechat: "",
      skype: "",
      saveLoading: false,
      UserImage: `${Config.webUrl}images/image.png`,
      access_token: this.props.userData.token,
      showDialog: false,
      isLoading: false,
      isShowSuccess: false,
      isShowError: false,
      errorMessage: "Error",
      isLoadings: false,
      userInfo: this.props.component && this.props.component.userInfo,
      ImageShow: false,
      tmpImg: null
    };
  }

  componentDidMount() {
    if (this.state.userInfo) {
      var img_avatar;
      if (this.state.userInfo.img_avatar == "") {
        img_avatar = `${Url.baseUrl}/images/avatar_xenren.png`;
      } else {
        img_avatar = this.state.userInfo.img_avatar;
      }

      this.setState({
        handphone: this.state.userInfo.handphone_no,
        email: this.state.userInfo.email,
        aboutMe: this.state.userInfo.about_me,
        nickName: this.state.userInfo.nick_name,
        qq: this.state.userInfo.qq_id,
        wechat: this.state.userInfo.wechat_id,
        skype: this.state.userInfo.skype_id,
        UserImage: img_avatar,
        password: null,
        isLoadings: false,
        tmpImg: img_avatar
      });
    } else {
      //  this.setState({isLoadings:false})
    }
  }

  openModal() {
    this.setState({ showDialog: true });
  }

  closeModal() {
    this.setState({ showDialog: false });
  }

  logout() {
    this.props.component.root.setToken(null);
    this.props.component.root.changePage("login");
  }

  //USER_INFO
  getUsersInfo = access_token => {
    HttpRequest.getUserProfile(access_token)
      .then(response => {
        const result = response.data;
        console.log("Me Api response------------->", result);
        if (result.status === "success") {
          this.props.component.userInfo = result.data;
          this.setState({
            userDetails: result.data,
            loading: false,
            activeTabIndex: 0
          });
        } else {
          this.setState({
            loading: false,
            activeTabIndex: 0
          });
          // Show Dialog
          // console.log('else')
          global.setTimeout(() => {
            Alert.alert("Warning", "Token expire!", [
              {
                text: "OK",
                onPress: () => {
                  this.logout();
                }
              }
            ]);
          }, 200);
        }
      })
      .catch(() => {
        const message = "Cannot Fetch, Please Try Again.";
        alert(message);
        this.setState({
          loading: false,
          activeTabIndex: 0
        });
      });
  };

  //HIT API
  submit() {
    if (this.state.isLoading === false) {
      this.setState({ isLoading: true });
      var access_token = "Bearer" + " " + this.state.access_token;
      var nick_name = this.state.nickName;
      var about_me = this.state.aboutMe;
      var email = this.state.email;
      var qq = this.state.qq;
      var weChat = this.state.wechat;
      var skype = this.state.skype;
      var headphone = this.state.handphone;
      var password = this.state.password;
      HttpRequest.updatePersonalInfo(
        nick_name,
        about_me,
        email,
        password,
        qq,
        weChat,
        skype,
        headphone,
        access_token
      )
        .then(response => {
          const result = response;
          if (result.status == "success") {
            this.getUsersInfo(access_token);
            this.setState({
              isLoading: false,
              isShowSuccess: true
            });
            setTimeout(() => {
              this.setState({
                isShowSuccess: false
              });
            }, 5000);
          } else if (result.status == "failure") {
            // Show Dialog
            this.setState({
              isLoading: false,
              isShowError: true,
              errorMessage: result.message
            });

            // Hide Dialog
            setTimeout(() => {
              this.setState({
                isShowError: false
              });
            }, 3000);
          } else {
            // Show Dialog
            // console.log('else')
            global.setTimeout(() => {
              Alert.alert("Warning", "Token expire!", [
                {
                  text: "OK",
                  onPress: () => {
                    this.logout();
                  }
                }
              ]);
            }, 200);
          }
        })
        .catch(error => {
          const message = "Cannot Fetch, Please Try Again.";
          alert(message);
        });
    } else {
    }
  }

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.launchImageLibrary(options, response => {
      if (response.didCancel) {
        // console.log("User cancelled image picker");
      } else if (response.error) {
        // console.log("ImagePicker Error: ", response.error);
      } else {
        this.setState({
          tmpImg: response.uri,
          ImageShow: true
        });
      }
    });
  }

  saveProfileImage = () => {
    this.setState({ saveLoading: true });
    const accessToken = "Bearer" + " " + this.state.access_token;
    const imageObjectArr = {
      uri: this.state.tmpImg,
      name: "photo_id.png" || "photo_id.jpg" || "photo_id.jpeg",
      type: "image/png" || "image/jpg" || "image.jpeg"
    };

    HttpRequest.updateProfilePicture(accessToken, imageObjectArr)
      .then(response => {
        const result = response;
        if (result.status === "success") {
          this.props.userData.img_avatar = result.data.avatar;
          alert("Avatar changed successfully");
          this.setState({
            isLoading: false,
            showDialog: false,
            saveLoading: false,
            tmpImg: result.data.avatar
          });
        } else if (result.status === "error") {
          alert("Error occurred. Please try again");
          this.setState({
            isLoading: false,
            showDialog: false,
            saveLoading: false,
            errorMessage: result.message
          });
        } else {
          // Show Dialog
          global.setTimeout(() => {
            Alert.alert("Warning", "Token expire!", [
              {
                text: "OK",
                onPress: () => {
                  this.logout();
                }
              }
            ]);
          }, 200);
        }
      })
      .catch(error => {
        const message = "Cannot Fetch, Please Try Again.";
        alert(message);
      });
  };

  onSelectImage(response) {
    if (response.didCancel) {
    } else if (response.error) {
    } else if (response.customButton) {
    } else {
      this.setState({ UserImage: response.uri });
    }
  }

  closeImgModal() {
    this.setState({
      showDialog: false,
      tmpImg: this.state.UserImage,
      ImageShow: false
    });
  }

  render() {
    return (
      <ScrollView style={styles.rootStyle}>
        <Modal
          visible={this.state.isLoadings}
          animationType="fade"
          transparent={true}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View
            style={{
              backgroundColor: "rgba(52, 52, 52, 0.8)",
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator color="#fff" />
          </View>
        </Modal>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.showDialog}
          onRequestClose={() => {
            this.closeModal();
          }}
        >
          <View style={styles.modalContainerStyle}>
            <View style={styles.modalBoxStyle}>
              <View style={{ alignItems: "center" }}>
                <View
                  style={{
                    width: "100%",
                    alignItems: "flex-end",
                    alignSelf: "center"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      margin: 5,
                      flexDirection: "row"
                    }}
                    onPress={() => this.closeImgModal()}
                  >
                    <Ionicons
                      name="md-close-circle"
                      size={24}
                      color={Config.primaryColor}
                    />
                  </TouchableOpacity>
                </View>

                <View style={styles.modalHeaderWrapper}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row"
                    }}
                  >
                    <View style={{ flex: 0.1 }} />
                    <View style={{ flex: 0.85 }}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#000",
                          textAlign: "center",
                          fontFamily: FontStyle.Bold
                        }}
                      >
                          {translate('edit_your_profile')}{' '}
                      </Text>
                      <Text
                        style={{
                          fontSize: 16,
                          color: "gray",
                          textAlign: "center",
                          paddingVertical: 4,
                          fontFamily: FontStyle.Medium,
                        }}
                      >
                          {translate('gain_more_traffic')}{' '}
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "column",
                    height: 225,
                    width: 220,
                    paddingVertical: 8,
                    alignSelf: "center",
                    marginBottom: 12,
                    marginLeft: 8,
                    borderRadius: 110,
                  }}
                >
                  <Image
                    source={{ uri: this.state.tmpImg }}
                    style={{
                      height: 212,
                      width: 215,
                      borderRadius: 105,
                    }}
                    resizeMode="cover"
                  />
                </View>
              </View>

              {this.state.ImageShow === false && (
                <View
                  style={{
                    justifyContent: "center",
                    paddingHorizontal: 15,
                    marginBottom: 10
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      backgroundColor: Config.primaryColor,
                      justifyContent: "center",
                      paddingVertical: 12,
                      borderRadius: 5
                    }}
                    onPress={() => this.launchCamera()}
                  >
                    <Text
                      style={{
                        color: "#fff",
                        textAlign: "center",
                        fontFamily: FontStyle.Medium,
                        fontSize: 17
                      }}
                    >
                        {translate('CHANGE')}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              {this.state.ImageShow === true && (
                <View
                  style={{
                    justifyContent: "center",
                    paddingHorizontal: 15,
                    marginBottom: 10
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      backgroundColor: Config.primaryColor,
                      justifyContent: "center",
                      paddingVertical: 12,
                      borderRadius: 5,
                      marginBottom: 10
                    }}
                    onPress={() => this.saveProfileImage()}
                  >
                    {this.state.saveLoading ? (
                      <ActivityIndicator size="small" color="white" />
                    ) : (
                      <Text
                        style={{
                          color: "#fff",
                          textAlign: "center",
                          fontFamily: FontStyle.Medium,
                          fontSize: 17
                        }}
                      >
                          {translate('save')}
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
        </Modal>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.isShowError}
          onRequestClose={() => console.log("Modal Closed")}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                source={require("../../../../images/login/login_error.png")}
                style={{ height: 90 }}
                resizeMode="contain"
              />
              <View style={{ height: 20 }} />
              <Text
                style={{
                  fontFamily: FontStyle.Medium,
                  color: "#ff2057",
                  fontSize: 20,
                  textAlign: "center"
                }}
              >
                {this.state.errorMessage}
              </Text>
            </View>
          </View>
        </Modal>

        {this.state.isShowSuccess && (
          <SuccessModal visible={this.state.isShowSuccess} />
        )}

        <View style={styles.container}>
          <View style={styles.ImagAvatar}>
            <Text
              style={{
                paddingVertical: 8,
                color: "gray",
                fontSize: 16,
                fontFamily: FontStyle.Medium
              }}
            >
                {translate('real_user_avatar')}
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={{ uri: this.state.UserImage }}
                style={{
                  width: 80,
                  height: 80,
                  borderRadius: 40
                }}
              />
              <View
                style={{
                  flex:1,
                  flexDirection: "column",
                  paddingVertical: 10
                }}
              >
                <TouchableOpacity
                  style={{
                    height: 35,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: Config.primaryColor,
                    marginHorizontal: 10,
                    width: "40%"
                  }}
                  onPress={() => this.openModal()}
                >
                  <Text
                    style={{
                      color: "#fff",
                      paddingHorizontal: 10,
                      fontSize: 12,
                      fontFamily: FontStyle.Medium
                    }}
                  >
                      {translate('modify_avatar')}
                  </Text>
                </TouchableOpacity>
                <Text
                  style={{
                    flex:1,
                    color: "gray",
                    fontSize: 11,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    fontFamily: FontStyle.Medium
                  }}
                >
                    {translate('max_image_support')}{' '}PNG/JPG/GIF
                </Text>
              </View>
            </View>
          </View>
          <Input
            title={translate('nick_name')}
            placeholder={translate('no_special_characters')}
            value={this.state.nickName}
            onChangeText={nickName => {
              this.setState({ nickName });
            }}
          />
          <TextArea
            title={translate('about_me')}
            placeholder={translate('please_write')}
            value={this.state.aboutMe}
            onChangeText={aboutMe => {
              this.setState({ aboutMe });
            }}
          />
          <Input
            title={translate('EMAIL')}
            editable={false}
            placeholder="abc@gmail.com"
            value={this.state.email}
            keyboardType="email-address"
            onChangeText={email => {
              this.setState({ email });
            }}
          />
          <Input
            title={translate('PASSWORD')}
            placeholder="*********"
            value={this.state.password}
            onChangeText={password => {
              this.setState({ password });
            }}
          />
          <Input
            title={translate('qq')}
            placeholder={translate('please_write')}
            value={this.state.qq}
            onChangeText={qq => {
              this.setState({ qq });
            }}
          />
          <Input
            title={translate('wechat')}
            placeholder={translate('please_write')}
            value={this.state.wechat}
            onChangeText={wechat => {
              this.setState({ wechat });
            }}
          />
          <Input
            title={translate('skype')}
            placeholder={translate('please_write')}
            value={this.state.skype}
            onChangeText={skype => {
              this.setState({ skype });
            }}
          />
          <Input
            title={translate("HANDPHONE")}
            placeholder={translate('enter_your_handphopne_number')}
            value={this.state.handphone}
            keyboardType='numeric'
            onChangeText={handphone => {
              this.setState({ handphone });
            }}
          />
          <TouchableOpacity
            style={{
              height: 35,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: Config.primaryColor,
              borderRadius: 5,
              marginHorizontal: 10,
              marginTop: 10
            }}
            onPress={() => this.submit()}
          >
            {this.state.isLoading === true && (
              <ActivityIndicator color="#fff" />
            )}
            {this.state.isLoading === false && (
              <Text style={{ color: "#fff" }}>{translate('submit')}</Text>
            )}
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: "#fcf7f7",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 12,
    paddingVertical: 12
  },
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 5,
    paddingVertical: 5,
    marginBottom: 15
  },
  ImagAvatar: {
    flexDirection: "column",
    paddingVertical: 5
  },
  modalBoxStyle: {
    width: "90%",
    backgroundColor: "#ffffff",
    borderRadius: 5,
    justifyContent: "space-between"
  },

  modalHeaderWrapper: {
    paddingHorizontal: 10,
    flexDirection: "row"
  },

  modalContainerStyle: {
    backgroundColor: "rgba(52, 52, 52, 0.8)",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },

  headerText: {
    fontFamily: FontStyle.Bold,
    color: "#00",
    fontSize: 18
  },

  searchBoxWrapperStyle: {
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "#fff",
    height: 40,
    width: "100%",
    flexDirection: "row",
    borderColor: "#000",
    borderWidth: 1
  },
  dialogBoxStyle: {
    width: 250,
    height: 200,
    backgroundColor: "#fff",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  }
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: "set_root",
        root
      })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonalInfo);
