/* eslint-disable no-plusplus */
/* eslint-disable global-require */
/* eslint-disable max-len */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable import/first */
import React, { Component } from "react";
import { connect } from "react-redux";
// eslint-disable-next-line no-unused-vars
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import styles from './styles';
import HttpRequest from '../../../components/HttpRequest';
import Config from '../../../Config';
import Input from '../../profile/Input';
import LocalData from '../../../components/LocalData';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MultiSelectModal from '../../../components/MultiSelectModal';
import SuccessModal from '../../../components/SuccessModal';
import { translate } from '../../../i18n';
import { setProfileData } from '../reducers/actions'

class JobPositionInfo extends Component {
  constructor(props) {
    super(props);
    this.root = this.props.component.root;
    this.userInfo = this.props.component.userInfo;
    this.state = {
      jobTitle: this.props.component.userInfo.title,
      expYear: this.props.component.userInfo.experience,
      payHourly: this.props.component.userInfo.hourly_pay,
      skill: [],
      selectedSkills: [],
      searchJobPosition: '',
      searchSkill: '',
      searchCurrency: '',
      currency: [
        {
          id: 1,
          name: translate('usd'),
          isSelect: false,
        },
        {
          id: 2,
          name: translate('rmb'),
          isSelect: false,
        },
      ],
      selectedCurrency: [],
      selectedJobPosition: [],
      jobs: [],
      jobPositionName: translate('select_your_job_position'),
      currencyName: translate('Select_Currency'),
      access_token: '',
      jobPositionModal: false,
      skillsModal: false,
      currencyModal: false,
      showDialog: false,
      isLoading: false,
      isShowSuccess: false,
      isShowError: false,
    };
  }

  componentDidMount() {
    LocalData.getUserData().then((response) => {
      response = JSON.parse(response);
      this.setState({ access_token: response.token });
    });
    const skill = this.userInfo.skills;
    let emptyArr = [];
    skill.map(item => {
      emptyArr.push(item.skill_id);
    });
    this.setState({ selectedSkills: emptyArr });

    this.getSkills();

    let jobPositionName = this.userInfo.job_position
      ? this.userInfo.job_position.name_en
      : translate('select_your_job_position');

    let currName = this.state.currencyName;
    this.state.currency.map(item => {
      if (item.id === this.userInfo.currency) {
        currName = item.name;
      }
    });

    const jobPostion = this.state.selectedJobPosition;
    jobPostion.push(this.userInfo.job_position_id);

    const selectedCurrencyArr = this.state.selectedCurrency;
    selectedCurrencyArr.push(this.userInfo.currency);

    this.props.component.jobPositions.map(item => {
      return jobPostion.indexOf(item.id) !== -1
        ? (item.isSelect = true)
        : (item.isSelect = false);
    });

    this.setState({
      selectedCurrency: selectedCurrencyArr,
      selectedJobPosition: jobPostion,
      jobs: this.props.component.jobPositions,
      jobPositionName: jobPositionName,
      currencyName: currName
    });
  }

  getSkills() {
    HttpRequest.getSkills()
      .then((response) => {
        let skillsArr = response.data;
        let idsArr = [];
        skillsArr.map(item => {
          idsArr.push(item.value);
          item.isSelect = false;
        });

        skillsArr && skillsArr.length
          ? this.props.profile.skills.map(item => {
              idsArr.indexOf(item.skill_id) > -1
                ? (skillsArr[idsArr.indexOf(item.skill_id)].isSelect = true)
                : null;
            })
          : null;
        this.setState({ skill: skillsArr });
      })
      .catch((error) => {
        alert(error);
      });
  }

  //Skills
  selectedSkills = item => {
    let { skill, selectedSkills } = this.state;
    let emptyArr = selectedSkills;
    console.log('skills length');
    console.log(emptyArr);
    if(emptyArr.length >= 10 ){
      alert('you can\'t select more then 10 skills');
      return false;
    }
    skill.map(subItem => {
      if (item.value === subItem.value) {
        !subItem.isSelect
          ? emptyArr.push(subItem.value)
          : emptyArr.splice(emptyArr.indexOf(subItem.value), 1);
        subItem.isSelect = !subItem.isSelect;
      }
    });
    this.setState({ skill: skill, selectedSkills: emptyArr });
  };

  renderSkills = item => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => this.selectedSkills(item)}
          style={styles.renderContainer}
        >
          <Text
            style={{
              fontSize: 14,
              color: item.isSelect ? Config.primaryColor : null
            }}
          >
            {item.text}
          </Text>

          {item.isSelect && (
            <MaterialIcons
              name={"check"}
              size={20}
              style={{ marginRight: 15 }}
              color={Config.primaryColor}
            />
          )}
        </TouchableOpacity>
      </ScrollView>
    );
  };

  //Currency
  onSelectCurrency = (item, index) => {
    let id = [];
    let temp = this.state.currency;
    let name = '';
    temp.map(subItem => {
      if (subItem.id === item.id) {
        subItem.isSelect = true;
        id.push(subItem.id);
        name = subItem.name;
      } else {
        subItem.isSelect = false;
      }
    });
    this.setState({
      selectedCurrency: id,
      currency: temp,
      currencyName: name,
    });
  };

  renderCurrency = (item, index) => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => this.onSelectCurrency(item, index)}
          style={styles.renderContainer}
        >
          <Text
            style={{
              fontSize: 14,
              color: item.isSelect ? Config.primaryColor : null,
            }}
          >
            {item.name}
          </Text>

          {item.isSelect && (
            <MaterialIcons
              name={'check'}
              size={20}
              style={{ marginRight: 15 }}
              color={Config.primaryColor}
            />
          )}
        </TouchableOpacity>
      </ScrollView>
    );
  };

  //Job
  selectedJob = item => {
    let id = [];
    let temp = this.state.jobs;
    let name = '';
    temp.map(jobItem => {
      if (jobItem.id === item.id) {
        jobItem.isSelect = true;
        id.push(jobItem.id);
        name = jobItem.name_en;
      } else {
        jobItem.isSelect = false;
      }
    });
    this.setState({
      selectedJobPosition: id,
      jobs: temp,
      jobPositionName: name,
    });
  };

  renderJobs = (item, index) => {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableOpacity
          onPress={() => this.selectedJob(item, index)}
          style={styles.renderContainer}
        >
          <Text
            style={{
              fontSize: 14,
              color: item.isSelect ? Config.primaryColor : null,
            }}
          >
            { this.props.locale === 'cn' || this.props.locale === 'tw' || this.props.locale === 'zh' ? item.name_cn : item.name_en }
          </Text>

          {item.isSelect && (
            <MaterialIcons
              name={'check'}
              size={20}
              style={{ marginRight: 15 }}
              color={Config.primaryColor}
            />
          )}
        </TouchableOpacity>
      </ScrollView>
    );
  };

  closeSkillsModal = () => {
    this.setState({ skillsModal: false, searchSkill: '' });
  };

  closeCurrencyModal = () => {
    this.setState({ currencyModal: false, searchCurrency: '' });
  };

  closejobPositionModal = () =>
    this.setState({
      jobPositionModal: false,
      searchJobPosition: '',
    });

  //USER_INFO
  getUsersInfo = (access_token) => {
    HttpRequest.getUserProfile(access_token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          this.props.component.userInfo = result.data;
          this.setState({
            userDetails: result.data,
            loading: false,
            activeTabIndex: 0,
          });
          this.props.setProfile(result.data)
        } else {
          this.setState({
            loading: false,
            activeTabIndex: 0,
          });
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({
          loading: false,
          activeTabIndex: 0,
        });
      });
  };

  //Hit API
  addJobPosition() {
    if (this.state.selectedSkills.length <= 10) {
      this.setState({ isLoading: true });
      const job_position = this.state.selectedJobPosition.join();
      const currency = this.state.selectedCurrency.join();
      const skills = this.state.selectedSkills.join();
      const job_title = this.state.jobTitle;
      const expYear = this.state.expYear;
      const pay_hourly = this.state.payHourly;
      const access_token = `Bearer ${this.state.access_token}`;
      HttpRequest.updateJobDetail(
        access_token,
        expYear,
        job_position,
        job_title,
        skills,
        pay_hourly,
        currency,
      )
        .then((response) => {
          this.setState({
            isLoading: false,
            isShowSuccess: true,
          });

          setTimeout(() => {
            this.setState({
              isShowSuccess: false,
            });
          }, 3000);
          this.getUsersInfo(access_token);
        })
        .catch((error) => {
          alert(error);
        });
    } else {
      alert(translate('selected_ten_skills'));
    }
  }

  render() {
    const {
      selectedSkills,
      jobPositionModal,
      skillsModal,
      skill,
      jobs,
      currencyModal,
      isShowSuccess
    } = this.state;

    return (
      <ScrollView style={styles.rootStyle}>
        <SuccessModal visible={isShowSuccess} />

        <View style={styles.container}>
          <Input
            title={translate('experience_year')}
            keyboardType={'phone-pad'}
            placeholder={translate('please_write')}
            value={this.state.expYear}
            onChangeText={expYear => {
              this.setState({ expYear });
            }}
          />

          <View style={styles.innerContainer}>
            <View style={styles.selectButtonStyle}>
              <Text style={styles.multiselectHeader}>{translate('job_position')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ jobPositionModal: true })}
                style={styles.selectButtonStyle}
              >
                <Text style={{ paddingVertical: 12 }}>
                  {this.state.selectedJobPosition.length
                    ? this.state.jobPositionName
                    : translate('select_your_job_position')}
                </Text>
                <MaterialIcons
                  name={'arrow-drop-down'}
                  size={28}
                  color={'grey'}
                />
              </TouchableOpacity>

              {jobPositionModal && (
                <MultiSelectModal
                  visible={jobPositionModal}
                  onRequestClose={() => this.closejobPositionModal()}
                  title={translate('select_job_position')}
                  onChangeText={searchJobPosition =>
                    this.setState({ searchJobPosition })
                  }
                  onCrossPressed={() => this.closejobPositionModal()}
                  value={this.state.searchJobPosition}
                  data={jobs.filter(item =>
                    item.name_en
                      .toLowerCase()
                      .includes(this.state.searchJobPosition.toLowerCase()),
                  )}
                  renderItem={({ item, index }) => this.renderJobs(item, index)}
                  extraData={this.state}
                  submitPressed={() => this.closejobPositionModal()}
                />
              )}
            </View>
          </View>

          <Input
            title={translate('job_title')}
            placeholder={translate('please_write')}
            value={this.state.jobTitle}
            onChangeText={(jobTitle) => {
              this.setState({ jobTitle });
            }}
          />

          <View style={styles.innerContainer}>
            <View style={styles.subContainer}>
              <Text style={styles.multiselectHeader}>{translate('skills')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ skillsModal: true })}
                style={styles.selectButtonStyle}
              >
                <Text style={{ paddingVertical: 12 }}>
                  {selectedSkills.length
                    ? selectedSkills.length + translate('selected_skills')
                    : translate('selected_your_skills')}
                </Text>
                <MaterialIcons
                  name={'arrow-drop-down'}
                  size={28}
                  color={'grey'}
                />
              </TouchableOpacity>

              {skillsModal && (
                <MultiSelectModal
                  visible={skillsModal}
                  onRequestClose={() => this.closeSkillsModal()}
                  onCrossPressed={() => this.closeSkillsModal()}
                  title={translate('skills')}
                  onChangeText={searchSkill => this.setState({ searchSkill })}
                  value={this.state.searchSkill}
                  data={skill.filter(item =>
                    item.text
                      .toLowerCase()
                      .includes(this.state.searchSkill.toLowerCase())
                  )}
                  renderItem={({ item, index }) =>
                    this.renderSkills(item, index)
                  }
                  extraData={this.state}
                  submitPressed={() => this.closeSkillsModal()}
                />
              )}
            </View>
          </View>

          <Input
            title={translate('PAY_hourly')}
            placeholder={translate('please_write')}
            value={this.state.payHourly}
            onChangeText={(payHourly) => {
              this.setState({ payHourly });
            }}
            keyboardType="numeric"
          />

          <View style={styles.innerContainer}>
            <View style={styles.subContainer}>
              <Text style={styles.multiselectHeader}>{translate('select_currency')}</Text>
            </View>

            <View style={styles.multiselectContainer}>
              <TouchableOpacity
                onPress={() => this.setState({ currencyModal: true })}
                style={styles.selectButtonStyle}
              >
                <Text style={{ paddingVertical: 12 }}>
                  {this.state.currencyName}
                </Text>

                <MaterialIcons
                  name={'arrow-drop-down'}
                  size={28}
                  color={'grey'}
                />
              </TouchableOpacity>

              {currencyModal && (
                <MultiSelectModal
                  visible={currencyModal}
                  onRequestClose={() => this.closeCurrencyModal()}
                  onCrossPressed={() => this.closeCurrencyModal()}
                  title={translate('currency')}
                  onChangeText={searchCurrency =>
                    this.setState({ searchCurrency })
                  }
                  value={this.state.searchCurrency}
                  data={this.state.currency.filter(item =>
                    item.name
                      .toLowerCase()
                      .includes(this.state.searchCurrency.toLowerCase()),
                  )}
                  renderItem={({ item, index }) =>
                    this.renderCurrency(item, index)
                  }
                  extraData={this.state}
                  submitPressed={() => this.closeCurrencyModal()}
                />
              )}
            </View>
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => this.addJobPosition()}
          >
            {this.state.isLoading === true && (
              <ActivityIndicator color="#fff" />
            )}
            {this.state.isLoading === false && (
              <Text style={{ color: '#ffffff' }}>{translate('submit')}</Text>
            )}
          </TouchableOpacity>
          <View />
        </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    component: state.component,
    profile: state.profile
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
    setProfile: profile => dispatch(setProfileData(profile))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(JobPositionInfo);
