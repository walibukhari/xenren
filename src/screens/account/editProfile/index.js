/* eslint-disable indent */
/* eslint-disable max-len */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable react/prop-types */
/* eslint-disable block-spacing */
/* eslint-disable semi */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Alert, TouchableOpacity, ActivityIndicator } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import HttpRequest from '../../../components/HttpRequest';
import Config from '../../../Config';
import PersonalInfo from './PersonalInfo';
import AddressInfo from './AddressInfo';
import JobPositionInfo from './JobPositionInfo';
import LocalData from '../../../components/LocalData';
import { translate } from '../../../i18n';
import FontStyle from '../../../constants/FontStyle';

class EditProfile extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('edit_profile'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };


  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: -1,
      userDetails: {},
      loading: true,
      locale: '',
    };
    this.root = this.props.component.root;
  }

  componentDidMount() {
    LocalData.getLocale().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ locale: resp.locale });
    });
    this.getUsersInfo(this.props.userData.token);
    this.getUsersPortfolio(this.props.userData.token);
  }

  getUsersInfo(access_token) {
    access_token = `bearer ${access_token}`;
    HttpRequest.getUserProfile(access_token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          this.props.component.userInfo = result.data;
          this.setState({
            userDetails: result.data,
            loading: false,
            activeTabIndex: 0,
          });
        } else {
          alert(result.status);
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({
          loading: false,
          activeTabIndex: 0,
        });
      });
  }

  getUsersPortfolio(access_token) {
    access_token = `bearer ${access_token}`;
    HttpRequest.getUsersPortfolio(access_token)
      .then((response) => {
        const result = response;
        if (result.status === 'success') {
          this.props.component.jobPositions = result.data.job_positions;
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  goBack = () => {
    this.props.navigation.navigate('UserProfile');
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        {
          this.state.loading
            ? <View/>
            : <View style={styles.topButtonWrapperStyle}>
              <TouchableOpacity onPress={() => {
                this.setState({ activeTabIndex: 0 });
              }}
                                style={[styles.topButtonStyle, this.state.activeTabIndex === 0 ? styles.topButtonActiveStyle : {}]}>
                <Text style={this.state.activeTabIndex === 0 ? styles.topTextActiveStyle : {}}>{translate('personal_details')}</Text>
              </TouchableOpacity>
              <View style={{
                width: 1,
                height: 50,
                backgroundColor: '#ecf0f1'
              }}/>
              <TouchableOpacity onPress={() => {
                this.setState({ activeTabIndex: 1 });
              }}
                                style={[styles.topButtonStyle, this.state.activeTabIndex === 1 ? styles.topButtonActiveStyle : {}]}>
                <Text
                  style={this.state.activeTabIndex === 1 ? styles.topTextActiveStyle : {}}>{translate('address')}</Text>
              </TouchableOpacity>
              <View style={{
                width: 1,
                height: 50,
                backgroundColor: '#ecf0f1'
              }}/>
              <TouchableOpacity onPress={() => {
                this.setState({ activeTabIndex: 2 });
              }}
                                style={[styles.topButtonStyle, this.state.activeTabIndex === 2 ? styles.topButtonActiveStyle : {}]}>
                <Text style={this.state.activeTabIndex === 2 ? styles.topTextActiveStyle : {}}>{translate('job_details')}</Text>
              </TouchableOpacity>
            </View>
        }
        {this.state.loading && <View style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <ActivityIndicator
            size='small' color={Config.primaryColor}
          />
        </View>
        }
        {this.state.activeTabIndex === 0 && <PersonalInfo/>}
        {this.state.activeTabIndex === 1 && <AddressInfo/>}
        {this.state.activeTabIndex === 2 && <JobPositionInfo locale={this.state.locale} />}
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderTopColor: '#ecf0f1',
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },

  topTextActiveStyle: {
    color: Config.primaryColor,
  },

  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%',
    position: 'relative',
  },
  backArrowStyle: {
    marginRight: 10,
  },
  backArrowIconStyle: {
    fontFamily: FontStyle.Light,
    marginLeft: 10,
  },

  navigationStyle: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditProfile);
