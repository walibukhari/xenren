import { StyleSheet } from "react-native";
import FontStyle from "../../../constants/FontStyle";
import Config from "../../../Config";

export default StyleSheet.create({
  rootStyle: {
    backgroundColor: "#fcf7f7",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 12,
    paddingVertical: 12
  },
  container: {
    backgroundColor: "#ffffff",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 5,
    paddingVertical: 5,
    marginBottom: 15
  },
  ImagAvatar: {
    flexDirection: "column",
    paddingVertical: 5
  },
  searchBoxWrapperStyle: {
    marginTop: 10,
    borderRadius: 5,
    backgroundColor: "#fff",
    height: 40,
    width: "100%",
    flexDirection: "row",
    borderColor: "#000",
    borderWidth: 1
  },
  multiselectContainer: {
    paddingHorizontal: 10
  },
  multiselectHeader: {
    fontSize: 12,
    color: "#8b8b8b"
  },
  selectButtonStyle: {
    paddingHorizontal: 10,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  innerContainer: {
    flex: 1,
    flexDirection: "column",
    borderWidth: 1,
    borderColor: "#e5e5e5",
    borderRadius: 5,
    marginVertical: 5,
    marginHorizontal: 10
  },
  subContainer: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: "#fcf7f7",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  titleTextStyle: {
    fontSize: 12,
    color: "#7f8c8d",
    fontFamily: FontStyle.Medium
  },
  buttonStyle: {
    height: 35,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Config.primaryColor,
    borderRadius: 5,
    marginHorizontal: 10,
    marginTop: 10
  },
  renderContainer: {
    height: 45,
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center"
  }
});
