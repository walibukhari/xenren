/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-self-assign */
/* eslint-disable no-restricted-globals */
/* eslint-disable global-require */
/* eslint-disable react/prop-types */
/* eslint-disable no-plusplus */
/* eslint-disable react/jsx-key */
/* eslint-disable no-useless-concat */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-shadow */
/* eslint-disable no-empty */
/* eslint-disable no-param-reassign */
/* eslint-disable no-dupe-keys */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable no-mixed-operators */
/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-const */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  Modal,
  Dimensions,
  ActivityIndicator,
  Alert,
  FlatList,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import ImagePicker from 'react-native-image-picker';
import LocalData from '../../../components/LocalData';
import HttpRequest from '../../../components/HttpRequest';
import Input from '../../profile/Input';
import Portifolio from '../portifolio';
import FontStyle from '../../../constants/FontStyle';
import OfficialComments from '../subs/OfficialComments';
import Config from '../../../Config';
import Review from '../subs/Review';
import { translate } from '../../../i18n';
import SuccessModal from '../../../components/SuccessModal';
import Entypo from 'react-native-vector-icons/Entypo';
import { setProfileData } from '../reducers/actions';

const { width } = Dimensions.get('window');

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: 1,
      isContact: false,
      access_token: this.props.userData.token,
      userDetails: {},
      isUpdateContact: false,
      qq_id: '',
      wechat_id: '',
      skype_id: '',
      handphone_no: '',
      isError: false,
      errorMsg: '',
      user_status: '',
      skills: [],
      job_position: {},
      languages: [],
      reviews: {},
      rating: {},
      portifolio: {},
      getLanguagesArr: [],
      officialCommentSource: [],
      commentSource: [],
      isLoadings: false,
      oneStar: 2,
      twoStar: 2,
      threeStar: 2,
      fourStar: 2,
      fiveStar: 2,
      ImageShow: false,
      imgName: '',
      tmpImg: `${Config.webUrl}images/image.png`,
      isShowSuccess: false,
      showDialog: false,
      device_token: '',
      active: true,
      comment: true,
      locale: '',
    };
  }

  componentDidMount() {
    LocalData.getLocale().then((response) => {
      const resp = JSON.parse(response);
      this.setState({ locale: resp.locale });
    });
    this.setState({ isLoadings: true });
    this.getUsersInfo(this.props.userData.token);
    this.getUsersInfo1(this.props.userData.token);
    this.getLanguages();
  }

  launchCamera() {
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      const source = { uri: response.uri };
      // console.log("response->", response);
      if (response.didCancel === true) {
        // do nothing
        // console.log("cancel Image");
      } else {
        this.setState({
          tmpImg: response.uri,
          ImageShow: true,
        });
      }
    });
  }

  saveProfileImage = () => {
    this.setState({ saveLoading: true });
    let access_token = `${'Bearer' + ' '}${this.state.access_token}`;
    let imageObjectArr = {
      uri: this.state.tmpImg,
      name: 'photo_id.png' || 'photo_id.jpg' || 'photo_id.jpeg',
      type: 'image/png' || 'image/jpg' || 'image.jpeg',
    };
    HttpRequest.updateProfilePicture(access_token, imageObjectArr)
      .then((response) => {
        const result = response;
        if (result.status === 'success') {
          this.props.userData.img_avatar = result.data.avatar;
          setTimeout(() => {
            this.setState({
              isShowSuccess: false,
              saveLoading: false,
            });
          }, 1000);
          this.setState({
            isLoading: false,
            isShowSuccess: true,
            showDialog: false,
            ImageShow: false,
            saveLoading: false,
          });
        } else if (result.status === 'error') {
          Alert.alert("Error", result.data.errors.avatar, [
            {
              text: "OK",
            }
          ]);
          this.setState({
            isLoading: false,
            isShowError: true,
            errorMessage: result.message,
            saveLoading: false,
          });
          setTimeout(() => {
            this.setState({
              isShowError: false,
              saveLoading: false,
            });
          }, 3000);
        } else {
          Alert.alert("Error", translate('network_error'), [
            {
              text: "OK",
            }
          ]);
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  openModal() {
    this.setState({ showDialog: true });
  }

  closeModal() {
    this.setState({ showDialog: false });
  }

  closeImgModal() {
    this.setState({
      showDialog: false,
      ImageShow: false,
    });
  }

  renderStar(jumlah) {
    const array = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 1; i <= 5; i++) {
      if (i <= jumlah) {
        array.push(<Ionicons
            key={`star_${i}`}
            name="md-star"
            color={Config.primaryColor}
            size={15}
          />);
      } else {
        array.push(<Ionicons
            key={`star_${i}`}
            name="md-star"
            color="#7f8c8d"
            size={15}
          />);
      }
    }
    return <View style={styles.starStyle}>{array}</View>;
  }

  renderSkill() {
    return this.props.skills.map((item) => {
       if (item.skill !== null) {
         return (
             <View
                 key={item.id}
                 style={{
                   borderRadius: 15,
                   borderColor: '#f1f1f1',
                   borderWidth: 1,
                   flexDirection: 'row',
                   alignItems: 'center',
                   marginRight: 5,
                   padding: 3,
                   marginTop: 3,
                 }}
             >
               <Text
                   style={{
                     paddingHorizontal: 10,
                     backgroundColor: 'transparent',
                     color: '#bababa',
                   }}
               >
                 {this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? item.skill.name_cn : item.skill.name_en}
               </Text>
             </View>
         );
       }
     });
  }

  getLanguageName(language_id) {
    let languageArr = this.state.getLanguagesArr;
    let i;
    let lngName = '';
    for (i = 0; i < languageArr.length; i++) {
      if (languageArr[i].id === language_id) {
        lngName = languageArr[i].name;
        break;
      }
    }
    return lngName;
  }

  renderLanguage() {
    return this.state.languages.map(item => (
      <View
        key={item.id}
        style={{
          flexDirection: 'row',
          paddingVertical: 3,
          marginHorizontal: 3,
          alignSelf: 'flex-start',
        }}
      >
        <Image
          source={{ uri: item.country_flag }}
          style={{
            width: 24,
            height: 24,
          }}
        />
        <Text
          style={{
            backgroundColor: 'transparent',
            color: '#bababa',
            paddingHorizontal: 3,
          }}
        >
          {this.getLanguageName(item.language_id)}
        </Text>
      </View>
    ));
  }

  updateContact() {
    // eslint-disable-next-line prefer-const
    let self = this;
    let qq_id = this.state.qq_id;
    let wechat_id = this.state.wechat_id;
    let skype_id = this.state.skype_id;
    let handphone_no = this.state.handphone_no;
    if (qq_id === '') {
      self.setState({
        isError: true,
        errorMsg: 'Please enter qq id.',
      });
    } else if (wechat_id === '') {
      self.setState({
        isError: true,
        errorMsg: 'Please enter wechat id.',
      });
    } else if (skype_id === '') {
      self.setState({
        isError: true,
        errorMsg: 'Please enter skype id.',
      });
    } else if (handphone_no === '') {
      self.setState({
        isError: true,
        errorMsg: 'Please enter phone number.',
      });
    } else {
      self.setState({
        isError: false,
        errorMsg: '',
      });
      let access_token = self.state.access_token;
      access_token = `bearer ${access_token}`;
      self.updateContactInfo(
        qq_id,
        wechat_id,
        skype_id,
        handphone_no,
        access_token,
      );
    }
  }

  updateContactInfo(qq_id, wechat_id, skype_id, handphone_no, access_token) {
    let self = this;
    HttpRequest.updateContactInfo(
      qq_id,
      wechat_id,
      skype_id,
      handphone_no,
      access_token,
    )
      .then((response) => {
        const result = response.data;

        if (result.status === 'success') {
          this.props.userData.qq_id = result.data.qq_id;
          this.props.userData.wechat_id = result.data.wechat_id;
          this.props.userData.skype_id = result.data.skype_id;
          this.props.userData.handphone_no = result.data.handphone_no;
          self.setState({
            isUpdateContact: false,
          });
          self.setState({
            qq_id: result.data.qq_id,
            wechat_id: result.data.wechat_id,
            skype_id: result.data.skype_id,
            handphone_no: result.data.handphone_no,
          });
        } else {
          self.setState({
            isUpdateContact: false,
          });
          self.setState({
            qq_id: result.data.qq_id,
            wechat_id: result.data.wechat_id,
            skype_id: result.data.skype_id,
            handphone_no: result.data.handphone_no,
          });
        }
      })
      .catch((e) => {
        self.setState({
          isUpdateContact: false,
        });
        self.setState({
          qq_id: result.data.qq_id,
          wechat_id: result.data.wechat_id,
          skype_id: result.data.skype_id,
          handphone_no: result.data.handphone_no,
        });
        alert(e);
      });
  }

  getUsersInfo(access_token) {
    let user_img_avatar;
    access_token = `bearer ${access_token}`;
    HttpRequest.getUserProfile(access_token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          if (result.data.img_avatar) {
            user_img_avatar = result.data.img_avatar;
          }
          this.setState({
            userDetails: result.data,
            tmpImg: user_img_avatar,
          });
          let user_status = result.data.status;
          if (result.data.status[0] === undefined) {
            this.setState({ user_status: '' });
          } else {
            this.setState({ user_status: user_status[0].user_status });
          }
          this.setState({
            qq_id: result.data.qq_id,
            wechat_id: result.data.wechat_id,
            skype_id: result.data.skype_id,
            handphone_no: result.data.handphone_no,
            skills: result.data.skills,
            job_position: result.data.job_position,
            languages: result.data.languages,
          });
          // update skills here.
          this.props.setProfile({
            skills: result.data.skills
          });

        } else {
          Alert.alert("Error", translate('network_error'), [
            {
              text: "OK",
            }
          ]);
        }
      })
      .catch((error) => {
        user_img_avatar = `${Config.webUrl}images/avatar_xenren.png`;
        this.setState({ isLoadings: false });
        alert(error);
      });
  }

  getUsersInfo1(access_token) {
    access_token = `bearer ${access_token}`;
    HttpRequest.getUserInfo(access_token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          const userRatings = result.data.ratings;
          const userPortfolioData = result.data.data;
          const userReviews = result.data.reviews;
          const adminReviews = result.data.adminReviews;
          this.setState({
            rating: userRatings,
            oneStar: this.calculateHeight(userRatings.totalCount1Star)
              .toString,
            twoStar: this.calculateHeight(userRatings.totalCount2Star),
            threeStar: this.calculateHeight(userRatings.totalCount3Star),
            fourStar: this.calculateHeight(userRatings.totalCount4Star),
            fiveStar: this.calculateHeight(userRatings.totalCount5Star),
            commentSource: userReviews,
            reviews: userReviews,
            officialCommentSource: adminReviews,
            dataBlob: adminReviews,
            activeTabIndex: 1,
            portifolio: userPortfolioData,
            isLoadings: false,
          });
          if (this.state.officialCommentSource.length > 0) {
            this.setState({ active: false });
          }
          if (this.state.commentSource.length > 0) {
            this.setState({ comment: false });
          }
        } else {
          this.setState({ isLoadings: false });
          Alert.alert("Error", response.data.message, [
            {
              text: "OK",
            }
          ]);
        }
      })
      .catch((error) => {
        this.setState({ isLoadings: false });
        alert(error);
      });
  }

  getLanguages() {
    HttpRequest.getLanguages()
      .then((response) => {
        const result = response.data;
        this.setState({ getLanguagesArr: result });
      })
      .catch((error) => {
        alert(error);
      });
  }

  reviewCount(reviews) {
    // reviews = reviews;
    let count = 0;
    // return reviews.length; 7694908785
    if (reviews) {
      count = reviews.length;
    }
    return count;
  }

  _calculateAge(birthday) {
    // birthday is a date
    // 1980-01-01
    //  birthday = '1970-12-12';
    birthday = new Date(birthday);
    let ageDifMs = Date.now() - birthday.getTime();
    let ageDate = new Date(ageDifMs); // miliseconds from epoch
    let date =  Math.abs(ageDate.getUTCFullYear() - 1970);
    if (isNaN(date) === true) {
      date = 'N/A';
    }
    return date;
  }

  renderModal() {
    return (
      <View
        style={{
          backgroundColor: 'rgba(0,0,0,0.5)',
          flex: 1,
          flexDirection: 'column',
          paddingTop: '65%',
          paddingHorizontal: 10,
        }}
      >
        <View
          style={{
            borderRadius: 4,
            borderColor: '#fff',
            borderWidth: 1,
            backgroundColor: '#fff',
            height: 220,
            paddingHorizontal: 10,
            paddingVertical: 10,
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.2 }}>
              <TouchableOpacity
                style={{
                  marginRight: 3,
                  flexDirection: 'row',
                }}
                onPress={() =>
                  this.setState({
                    isContact: false,
                    isUpdateContact: true,
                  })
                }
              >
                <SimpleLineIcons size={16} name="note" color="#57af2a" />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                flex: 0.6,
                textAlign: 'center',
                color: 'gray',
                fontSize: 18,
                fontFamily: FontStyle.Medium,
              }}
            >
              {translate('contact_info')}
            </Text>
            <Text
              onPress={() => this.setState({ isContact: false })}
              style={{
                color: Config.primaryColor,
                flex: 0.2,
                textAlign: 'right',
                paddingRight: 3,
                fontSize: 22,
                fontFamily: FontStyle.Medium,
              }}
            >
              <Entypo name="cross" size={25} color={Config.primaryColor}/>
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              paddingVertical: 20,
              paddingHorizontal: 10,
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                flex: 0.5,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                }}
              >
                <Image
                  source={require('../../../../images/account_profile/Tencent_QQ.png')}
                  style={{
                    height: 20,
                    width: 20,
                  }}
                />
                <Text style={{ textAlign: 'center' }}> {translate('qq')} </Text>
              </View>
              <Text
                style={{
                  color: 'gray',
                  paddingVertical: 10,
                }}
              >
                {this.props.userData.qq_id ? this.props.userData.qq_id : '' }
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                flex: 0.5,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                }}
              >
                <Image
                  source={require('../../../../images/account_profile/wechat-logo.png')}
                  style={{
                    height: 20,
                    width: 20,
                  }}
                />
                <Text style={{ textAlign: 'center' }}> {translate('wechat')} </Text>
              </View>
              <Text
                style={{
                  color: 'gray',
                  paddingVertical: 10,
                }}
              >
                {this.props.userData.wechat_id ? this.props.userData.wechat_id : '' }
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flex: 1,
              paddingVertical: 20,
              paddingHorizontal: 10,
            }}
          >
            <View
              style={{
                flexDirection: 'column',
                flex: 0.5,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignContent: 'center',
                }}
              >
                <Image
                  source={require('../../../../images/account_profile/skype-icon-5.png')}
                  style={{
                    height: 20,
                    width: 20,
                  }}
                />
                <Text style={{ textAlign: 'center' }}> {translate('skype')}</Text>
              </View>
              <Text
                style={{
                  color: 'gray',
                  paddingVertical: 10,
                }}
              >
                {this.props.userData.skype_id ? this.props.userData.skype_id : '' }
              </Text>
            </View>
            <View style={{
              flexDirection: 'column',
              flex: 0.5,
            }}>
              <View style={{
                flexDirection: 'row',
                alignContent: 'center',
              }}>
                <Image
                  source={require('../../../../images/account_profile/MetroUI_Phone.png')}
                  style={{
                    height: 20,
                    width: 20,
                  }}
                />
                <Text style={{ alignContent: 'center' }}> {translate('phone')} </Text>
              </View>
              <Text
                style={{
                  color: 'gray',
                  paddingVertical: 10,
                }}
              >
                {this.props.userData.handphone_no ? this.props.userData.handphone_no : '' }
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }

  calculateHeight(star) {
    let minHeight = 1;
    let rating = this.state.rating;
    let totalStar = rating.totalCount5Star + rating.totalCount4Star;
    if (star > 0) {
      minHeight = 40;
      let percent = (star / totalStar) * 100;
      minHeight = (200 / percent) * 10;
      // alert((isNaN(percent)));
      if (isNaN(percent)) {
        // console.log(`if+++++${percent}`, minHeight);
        minHeight = 40;
      } else {
        // console.log(`else----------${minHeight}`);

        minHeight = minHeight;
      }
    } else {
      minHeight = 1;
    }
    return minHeight;
  }

  renderCommentRow = (item) => {
    return (
    <OfficialComments review={item.item} />
    );
  };

  renderReviewRow = (item) => {
    return (
        <Review review={item.item} />
    );
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        <Modal
          visible={this.state.isContact}
          animationType="fade"
          transparent={true}
          onRequestClose={() => this.setState({ isContact: false })}
        >
          {this.renderModal()}
        </Modal>

        <Modal
          visible={this.state.isLoadings}
          animationType="fade"
          transparent={true}
          onRequestClose={() => console.log('Modal Closed')}
        >
          <View
            style={{
              backgroundColor: 'rgba(52, 52, 52, 0.8)',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <ActivityIndicator color="#fff" />
          </View>
        </Modal>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.showDialog}
          onRequestClose={() => {
            this.closeModal();
          }}
        >
          <View style={styles.modalContainerStyle}>
            <View style={styles.modalBoxStyle}>
              <View style={styles.modalHeaderWrapper}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}
                >
                  <View />
                  <View style={{ width: '100%' }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}
                    >
                      <Text />
                      <Text
                        style={{
                          fontSize: 20,
                          color: '#000',
                          textAlign: 'center',
                        }}
                      >
                        {translate('edit_your_profile')}{' '}
                      </Text>
                      <TouchableOpacity
                        style={{
                          marginRight: 3,
                          flexDirection: 'row',
                        }}
                        onPress={() => this.closeImgModal()}
                      >
                        <Ionicons
                          name="md-close-circle"
                          size={24}
                          color={Config.primaryColor}
                        />
                      </TouchableOpacity>
                    </View>

                    <Text
                      style={{
                        fontSize: 16,
                        color: 'gray',
                        textAlign: 'center',
                        paddingVertical: 10,
                        paddingHorizontal: 20,
                      }}
                    >
                      {translate('gain_more_traffic')}{' '}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 0.1,
                      alignSelf: 'center',
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  paddingVertical: 8,
                  alignSelf: 'center',
                  height: 225,
                  width: 220,
                  marginBottom: 12,
                  marginLeft: 8,
                  borderRadius: 110,
                }}
              >
                <Image
                  source={{ uri: this.state.tmpImg }}
                  style={{
                    height: 212,
                    width: 215,
                    borderRadius: 105,
                    // marginBottom: 10,
                  }}
                  resizeMode="cover"
                />
              </View>

              {this.state.ImageShow === false && (
                <View
                  style={{
                    justifyContent: 'center',
                    paddingHorizontal: 15,
                    marginBottom: 10,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      backgroundColor: Config.primaryColor,
                      justifyContent: 'center',
                      paddingVertical: 12,
                      borderRadius: 5,
                    }}
                    onPress={() => this.launchCamera()}
                  >
                    <Text
                      style={{
                        color: '#fff',
                        textAlign: 'center',
                        fontWeight: 'bold',
                        fontSize: 17,
                        fontFamily: FontStyle.Medium,
                      }}
                    >
                      {translate('CHANGE')}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              {this.state.ImageShow === true && (
                <View
                  style={{
                    justifyContent: 'center',
                    paddingHorizontal: 15,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: 'row',
                      backgroundColor: Config.primaryColor,
                      justifyContent: 'center',
                      paddingVertical: 12,
                      borderRadius: 5,
                      marginBottom: 10,
                    }}
                    onPress={() => this.saveProfileImage()}
                  >
                    {this.state.saveLoading ? (
                      <ActivityIndicator size="small" color="#fff" />
                    ) : (
                      <Text
                        style={{
                          color: '#fff',
                          textAlign: 'center',
                          fontWeight: 'bold',
                          fontSize: 17,
                          fontFamily: FontStyle.Medium,
                        }}
                      >
                        {translate('save')}
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </View>
        </Modal>
        <Modal
          visible={this.state.isUpdateContact}
          animationType="fade"
          transparent={true}
          onRequestClose={() => this.setState({ isUpdateContact: false })}
        >
          <ScrollView
            style={{
              backgroundColor: 'rgba(0,0,0,0.5)',
              flexDirection: 'column',
              paddingHorizontal: 10,
              paddingTop: '10%',
            }}
          >
            <View
              style={{
                flex:1,
                borderRadius: 4,
                borderColor: '#fff',
                borderWidth: 1,
                backgroundColor: '#fff',
                justifyContent:'center',
                paddingHorizontal: 10,
                paddingVertical: 10,
              }}
            >
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 0.2 }}>
                </View>
                <Text
                  style={{
                    flex: 0.6,
                    textAlign: 'center',
                    color: 'gray',
                    fontSize: 18,
                    fontFamily: FontStyle.Medium,
                  }}
                >
                  {translate('contact_info')}
                </Text>
                <Text
                  onPress={() => this.setState({ isUpdateContact: false })}
                  style={{
                    color: Config.primaryColor,
                    flex: 0.2,
                    textAlign: 'right',
                    paddingRight: 3,
                    fontSize: 22,
                    fontFamily: FontStyle.Medium,
                  }}
                >
                  <Entypo name="cross" size={25} color={Config.primaryColor}/>
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 1,
                  paddingVertical: 20,
                }}
              >
                {this.state.isError && (
                  <Text
                    style={{
                      color: 'red',
                      paddingHorizontal: 10,
                      paddingVertical: 3,
                    }}
                  >
                    {this.state.errorMsg}
                  </Text>
                )}
                <Input
                  title={translate('qq')}
                  placeholder=""
                  value={this.state.qq_id}
                  onChangeText={(qq_id) => {
                    this.setState({ qq_id });
                  }}
                />
                <Input
                  title={translate('wechat')}
                  placeholder=""
                  value={this.state.wechat_id}
                  onChangeText={(wechat_id) => {
                    this.setState({ wechat_id });
                  }}
                />
                <Input
                  title={translate('skype')}
                  placeholder=""
                  value={this.state.skype_id}
                  onChangeText={(skype_id) => {
                    this.setState({ skype_id });
                  }}
                />
                <Input
                  title={translate('phone')}
                  placeholder=""
                  keyboardType='numeric'
                  value={this.state.handphone_no}
                  onChangeText={(handphone_no) => {
                    this.setState({ handphone_no });
                  }}
                />
                <TouchableOpacity
                  style={{
                    height: 35,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Config.primaryColor,
                    borderRadius: 5,
                    marginHorizontal: 10,
                    marginTop: 10,
                  }}
                  onPress={() => {
                    this.updateContact();
                  }}
                >
                  <Text style={{ color: '#fff' }}>{translate('save')}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </Modal>

        {this.state.isShowSuccess && (
          <SuccessModal visible={this.state.isShowSuccess} />
        )}

        <ScrollView>
          <View style={styles.containerStyle}>
            <View style={styles.profileImage}>
              <TouchableOpacity onPress={() => this.openModal()}>
                <Image
                  source={{ uri: this.state.tmpImg }}
                  style={{
                    width: 100,
                    height: 100,
                    borderRadius: 50,
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#000',
                  fontFamily: FontStyle.Bold,
                  fontSize: 16,
                  paddingVertical: 6,
                }}
              >
                {this.state.userDetails.real_name}
              </Text>
              <TouchableOpacity
                style={styles.contactBtnStyle}
                onPress={() => {
                  this.setState({ isContact: true });
                }}
              >
                <Text style={styles.contactTextStyle}>{translate('contact')}</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#f7f7f7',
                paddingHorizontal: 10,
              }}
            >
              <View
                style={{
                  flexDirection: 'column',
                  paddingHorizontal: 10,
                  flex: 0.5,
                  paddingVertical: 8,
                }}
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: FontStyle.Medium,
                    color: '#404040',
                  }}
                >
                  {this.state.userDetails.nick_name}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: Config.primaryColor,
                    fontFamily: FontStyle.Regular,
                  }}
                >
                  {this.state.job_position !== null ? this.state.job_position.name_en : '' }
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: 'gray',
                    fontFamily: FontStyle.Medium,
                  }}
                >
                  {this.state.userDetails.title}
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 10,
                  flex: 0.5,
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.props.nav.navigate('QrUser', {
                      real_name: this.state.userDetails.real_name,
                      img_avatar: this.state.tmpImg,
                      nick_name: this.state.userDetails.nick_name,
                      job_position: this.state.job_position ? this.state.job_position.name_en : '',
                      title: this.state.userDetails.title,
                      profile_qr_code: this.state.userDetails.profile_qr_code,
                    })
                  }
                >
                  <Image
                    source={{
                      uri: this.state.userDetails.profile_qr_code,
                    }}
                    style={{
                      alignContent: 'flex-end',
                      height: 50,
                      width: 50,
                    }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: 20,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{
                    flex: 0.3,
                    color: '#999999',
                  }}
                >
                  {translate('status')}{' '} :
                </Text>
                <Text
                  style={{
                    flex: 0.7,
                    color: Config.primaryColor,
                  }}
                >
                  {this.state.user_status ? this.state.user_status : ''}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{
                    flex: 0.3,
                    color: '#999999',
                  }}
                >
                  {translate('rate')}{' '}:
                </Text>
                <Text
                  style={{
                    flex: 0.7,
                    color: '#808080',
                  }}
                >
                  {this.state.userDetails.hourly_pay ? '$' + this.state.userDetails.hourly_pay + ' / hr' : '' }
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{
                    flex: 0.3,
                    color: '#999999',
                  }}
                >
                  {translate('age')}{' '}:
                </Text>
                <Text
                  style={{
                    flex: 0.7,
                    color: '#808080',
                  }}
                >
                  {this.state.userDetails.date_of_birth ? this._calculateAge(this.state.userDetails.date_of_birth) + ' years' : '' }
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{
                    flex: 0.3,
                    color: '#999999',
                  }}
                >
                  {translate('experience')}{' '}:{' '}
                </Text>
                <Text
                  style={{
                    flex: 0.7,
                    color: '#808080',
                  }}
                >
                  {this.state.userDetails.experience ? this.state.userDetails.experience + ' years' : ''}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: 18,
                paddingVertical: 10,
              }}
            >
              <Text style={styles.textHeading}>
                <Ionicons
                  name="md-alert"
                  color={Config.primaryColor}
                  size={18}
                />{' '}
                {translate('info')}
              </Text>
              <Text
                style={{
                  paddingVertical: 10,
                  color: '#808080',
                  fontSize: 16,
                  fontFamily: FontStyle.Regular,
                }}
              >
                {this.state.userDetails.about_me}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: 20,
                paddingVertical: 10,
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  fontFamily: FontStyle.Regular,
                }}
              >
                <Text style={styles.textHeading}>
                  <Ionicons
                    name="md-hand"
                    color={Config.primaryColor}
                    size={18}
                  />{' '}
                  {translate('skills')}
                </Text>
              </Text>
              <View style={styles.middleSideStyle}>{this.renderSkill()}</View>
            </View>
            <View
              style={{
                flexDirection: 'column',
                paddingHorizontal: 20,
                paddingVertical: 10,
              }}
            >
              <Text style={styles.textHeading}>
                <Ionicons
                  name="md-globe"
                  color={Config.primaryColor}
                  size={18}
                />{' '}
                {translate('LANGUAGES')}
              </Text>
              <View style={styles.middleSideStyle}>
                {this.renderLanguage(this.state.userDetails.languages)}
              </View>
            </View>
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 3,
              }}
            >
              <Text style={styles.textHeading}>
                <Ionicons
                  name="md-star"
                  color={Config.primaryColor}
                  size={18}
                />{' '}
                {translate('rating')}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 20,
                paddingVertical: 10,
              }}
            >
              <View
                style={{
                  flex: 0.5,
                  flexDirection: 'column',
                  alignItems: 'center',
                }}
              >
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 54,
                    paddingTop: 55,
                    fontFamily: FontStyle.Medium,
                  }}
                >
                  {(this.state.rating.averageRating)}
                </Text>
                {this.renderStar(this.state.rating.averageRating)}
                <Text
                  style={{
                    paddingVertical: 10,
                    color: '#666666',
                  }}
                >
                  ( {(this.state.rating.totalCountReview)} {translate('reviews')} )
                </Text>
              </View>
              <View
                style={{
                  flex: 0.5,
                  alignItems: 'flex-end',
                  backgroundColor: '#fff',
                  flexDirection: 'row',
                }}
              >
                <View style={styles.mainGraphContainer}>
                  <View style={styles.graphOuterDiv}>
                    <View>
                      <Text style={styles.graphHeadingText}>
                        {this.state.rating.totalCount5Star}
                      </Text>
                    </View>
                    <View
                      style={{
                        maxHeight: 200,
                        paddingVertical: 10,
                        alignItems: 'flex-end',
                      }}
                    >
                      <View
                        style={{
                          width: 20,
                          height: 2,
                          backgroundColor: Config.primaryColor,
                        }}
                      />
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'column',
                      }}
                    >
                      <Ionicons
                        key={'star_1'}
                        name="md-star"
                        color="#f1c40f"
                        size={15}
                      />
                      <Text style={styles.starHeading}>5</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.mainGraphContainer}>
                  <View style={styles.graphOuterDiv}>
                    <View>
                      <Text style={styles.graphHeadingText}>
                        {this.state.rating.totalCount4Star}
                      </Text>
                    </View>
                    <View
                      style={{
                        maxHeight: 200,
                        paddingVertical: 10,
                      }}
                    >
                      <View
                        style={{
                          width: 20,
                          height: 2,
                          backgroundColor: Config.primaryColor,
                        }}
                      />
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'column',
                      }}
                    >
                      <Ionicons
                        key={'star_1'}
                        name="md-star"
                        color="#f1c40f"
                        size={15}
                      />
                      <Text style={styles.starHeading}>4</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.mainGraphContainer}>
                  <View style={styles.graphOuterDiv}>
                    <View>
                      <Text style={styles.graphHeadingText}>
                        {this.state.rating.totalCount3Star}
                      </Text>
                    </View>
                    <View
                      style={{
                        maxHeight: 200,
                        paddingVertical: 10,
                        alignItems: 'flex-end',
                      }}
                    >
                      <View
                        style={{
                          width: 20,
                          height: 2,
                          backgroundColor: Config.primaryColor,
                        }}
                      />
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'column',
                      }}
                    >
                      <Ionicons
                        key={'star_1'}
                        name="md-star"
                        color="#f1c40f"
                        size={15}
                      />
                      <Text style={styles.starHeading}>3</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.mainGraphContainer}>
                  <View style={styles.graphOuterDiv}>
                    <View>
                      <Text style={styles.graphHeadingText}>
                        {this.state.rating.totalCount2Star}
                      </Text>
                    </View>
                    <View
                      style={{
                        maxHeight: 200,
                        paddingVertical: 10,
                      }}
                    >
                      <View
                        style={{
                          width: 20,
                          height: 2,
                          backgroundColor: Config.primaryColor,
                        }}
                      />
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'column',
                      }}
                    >
                      <Ionicons
                        key={'star_1'}
                        name="md-star"
                        color="#f1c40f"
                        size={15}
                      />
                      <Text style={styles.starHeading}>2</Text>
                    </View>
                  </View>
                </View>

                <View style={styles.mainGraphContainer}>
                  <View style={styles.graphOuterDiv}>
                    <View>
                      <Text style={styles.graphHeadingText}>
                        {this.state.rating.totalCount1Star}
                      </Text>
                    </View>
                    <View
                      style={{
                        maxHeight: 200,
                        paddingVertical: 10,
                      }}
                    >
                      <View
                        style={{
                          width: 20,
                          height: 2,
                          backgroundColor: Config.primaryColor,
                          alignContent: 'flex-end',
                        }}
                      />
                    </View>
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'column',
                      }}
                    >
                      <Ionicons
                        key={'star_1'}
                        name="md-star"
                        color="#f1c40f"
                        size={15}
                      />
                      <Text style={styles.starHeading}>1</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.topButtonWrapperStyle}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ activeTabIndex: 1 });
                }}
                style={[
                  styles.topButtonStyle,
                  this.state.activeTabIndex === 1
                    ? styles.topButtonActiveStyle
                    : {},
                  {
                    borderRightColor: '#ecf0f1',
                    borderRightWidth: 2,
                  },
                ]}
              >
                <Text
                  style={
                    this.state.activeTabIndex === 1
                      ? styles.topTextActiveStyle
                      : {}
                  }
                >
                  {translate('official_comment')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ activeTabIndex: 2 });
                }}
                style={[
                  styles.topButtonStyle,
                  this.state.activeTabIndex === 2
                    ? styles.topButtonActiveStyle
                    : {},
                  {
                    borderRightColor: '#ecf0f1',
                    borderRightWidth: 2,
                  },
                ]}
              >
                <Text
                  style={
                    this.state.activeTabIndex === 2
                      ? styles.topTextActiveStyle
                      : {}
                  }
                >
                  {translate('comment')}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.setState({ activeTabIndex: 3 });
                }}
                style={[
                  styles.topButtonStyle,
                  this.state.activeTabIndex === 3
                    ? styles.topButtonActiveStyle
                    : {},
                ]}
              >
                <Text
                  style={
                    this.state.activeTabIndex === 3
                      ? styles.topTextActiveStyle
                      : {}
                  }
                >
                  {translate('portfolio')}
                </Text>
              </TouchableOpacity>
            </View>

            <View>
              {this.state.activeTabIndex === 1 && (
                <View
                  style={{
                    paddingVertical: 10,
                    flex: 1,
                  }}
                >
                  {this.state.active === true && (
                      <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 25,
                      }}>
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={{
                            fontFamily: FontStyle.Light,
                            backgroundColor: Config.white,
                            left: this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? 45 : 0,
                          }}>
                            {translate('comment_yet')}
                          </Text>
                          <View style={{ width: 200, height: 150 }}>
                            <Image
                                source={require('../../../../images/inboxlogo_transparent.png')}
                                style={{
                                  width: 120,
                                  height: 120,
                                  marginLeft: this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? width / 8 : width / 4,
                                  backgroundColor: 'white',
                                }}
                                resizeMode="contain"
                            />
                          </View>
                        </View>
                      </View>
                  )}
                  <FlatList
                      style={{ flex: 1 }}
                      data={this.state.officialCommentSource}
                      renderItem={item => this.renderCommentRow(item)}
                      keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              )}

              {this.state.activeTabIndex === 2 && (
                <View style={{ paddingVertical: 10 }}>
                  {this.state.comment === true && (
                      <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 25,
                      }}>
                        <View style={{ flexDirection: 'column' }}>
                          <Text style={{
                            fontFamily: FontStyle.Light,
                            left: this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? 45 : 0,
                          }}>
                            {translate('comment_yet')}
                          </Text>
                          <View style={{ width: 200, height: 150 }}>
                            <Image
                                source={require('../../../../images/inboxlogo_transparent.png')}
                                style={{
                                  width: 120,
                                  height: 120,
                                  marginLeft: this.state.locale === 'cn' || this.state.locale === 'tw' || this.state.locale === 'zh' ? width / 8 : width / 4,
                                }}
                                resizeMode="contain"
                            />
                          </View>
                        </View>
                      </View>
                  )}
                  <FlatList
                      style={{ flex: 1 }}
                      data={this.state.commentSource}
                      renderItem={item => this.renderReviewRow(item)}
                      keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              )}

              {this.state.activeTabIndex === 3 && (
                <Portifolio userPortfolio={1} />
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#fcf7f7',
    flex: 1,
    flexDirection: 'column',
    padding: 10,
  },

  containerStyle: {
    paddingVertical: 10,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    flex: 1,
  },

  profileImage: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },

  contactBtnStyle: {},

  // middleSideStyle: {
  //   flexDirection: 'row',
  //   paddingVertical: 20,
  // },

  contactTextStyle: {
    color: '#fff',
    borderRadius:2,
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 30,
    paddingVertical: 10,
    fontSize: 15,
    // fontWeight: 'bold',
    marginTop: 10,
    fontFamily: FontStyle.Bold,
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 15,
    // fontWeight: '300'
    fontFamily: FontStyle.Regular,
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  starStyle: {
    flexDirection: 'row',
  },

  textHeading: {
    color: '#666666',
    // fontWeight: 'bold',
    fontFamily: FontStyle.Bold,
    // verticalAlign: "top"
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    paddingHorizontal: 10,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },

  topTextActiveStyle: {
    color: Config.primaryColor,
  },

  topSideStyle: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingTop: 10,
    marginBottom: 10,
  },

  middleSideStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 10,
  },

  avatarStyle: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },

  cardButtonStyle: {
    flex: 1,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },

  cardButtonTextStyle: {
    color: Config.textSecondaryColor,
    fontSize: 12,
    fontFamily: FontStyle.Medium,
  },

  text1Style: {
    flex: 1,
    fontSize: 16,
    fontFamily: FontStyle.Medium,
  },

  boxInsideStyle: {
    flexDirection: 'column',
    backgroundColor: '#fff',
  },

  text2Style: {
    fontSize: 13,
    color: '#bdc3c7',
    fontFamily: FontStyle.Medium,
  },

  text3Style: {
    fontSize: 13,
    // fontWeight: '200'
    fontFamily: FontStyle.Medium,
  },

  textDescription: {
    fontSize: 15,
    color: 'grey',
    // fontWeight: 'normal'
    fontFamily: FontStyle.Regular,
  },

  mainGraphContainer: {
    flexDirection: 'row',
    paddingHorizontal: 4,
  },

  graphUpper: {
    flex: 'row',
    paddingHorizontal: 4,
  },

  graphOuterDiv: {
    flexDirection: 'column',
    maxHeight: 250,
    width: 20,
  },

  graphHeadingText: {
    color: 'gray',
    fontSize: 12,
    textAlign: 'center',
    fontFamily: FontStyle.Regular,
  },

  starHeading: {
    color: 'gray',
    fontSize: 12,
    fontFamily: FontStyle.Regular,
  },

  modalBoxStyle: {
    width: '90%',
    backgroundColor: '#ffffff',
    borderRadius: 5,
  },

  modalHeaderWrapper: {
    paddingHorizontal: 20,
    paddingTop: 20,
    flexDirection: 'row',
  },

  modalContainerStyle: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  dialogBoxStyle: {
    width: 250,
    height: 200,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
};

function mapStateToProps(state) {
  return {
    userData: state.auth.userData,
    component: state.component,
    skills: state.profile.skills
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
    setProfile: profile => dispatch(setProfileData(profile))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(About);
