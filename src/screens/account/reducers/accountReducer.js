import initialState from './initialState';
import actionTypes from '../../../constants';

export default function accountReducer(state = initialState.profile, action) { 
    switch (action.type) {
        case actionTypes.UPDATE_PROFILE_DATA:
            return {
                ...state,
                ...action.profile
            }
        default:
            return state;
    }
}