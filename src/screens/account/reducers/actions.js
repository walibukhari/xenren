import actionTypes from '../../../constants';

export function setProfileData(profile) { 
    return {
        type: actionTypes.UPDATE_PROFILE_DATA,
        profile,
    }   
}