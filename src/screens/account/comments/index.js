import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Text, TouchableOpacity, Alert, Image, ActivityIndicator, Modal, Dimensions, FlatList} from 'react-native';
import Config from '../../../Config';
import Review from '../subs/Review';
import OfficialComments from '../subs/OfficialComments';
import HttpRequest from '../../../components/HttpRequest';
import LocalData from '../../../components/LocalData';
import FontStyle from '../../../constants/FontStyle';
import { translate } from '../../../i18n';

const { width, height } = Dimensions.get('window');
class Comment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      adminReviews: [],
      dataSource: [],
      commentDataSource: [],
      officialCommentCount: 0,
      normalCommentCount: 0,
      isLoadings: true,
      officialComments: true,
      text: translate('comment_yet'),
    };
  }

  componentDidMount() {
    LocalData.getUserData()
      .then((response) => {
        response = JSON.parse(response);
        this.getUsersComments(response.token);
        this.getUsersReviews(response.token);
      });
  }

  getUsersComments(access_token) {
    access_token = 'bearer ' + access_token;
    HttpRequest.getUsersComments(access_token)
      .then((response) => {
        const result = response;
        if (result.status === 'success') {
          this.setState({
            dataSource: result.data.adminReviews,
            officialCommentCount: result.data.adminReviews.length,
            isLoadings: false,
            adminReviews: result.data.adminReviews,
          });
          if (this.state.dataSource.length > 0) {
            this.setState({ officialComments: false });
          }
        }
      })
      .catch((error) => {
        alert(error);
        this.setState({ isLoadings: false });
      });
  }

  getUsersReviews(access_token) {
    access_token = 'bearer ' + access_token;
    HttpRequest.getUsersReviews(access_token)
      .then((response) => {
        const result = response;
        if (result.status === 'success') {
          var results = result.data;
          var len = results.reviews.length;
          this.setState({
            commentDataSource: result.data.reviews,
            normalCommentCount: len,
            isLoadings: false,
          });
        } else if (result.status === 'failure') {
          this.setState({ isLoadings: false });
          Alert.alert("Error", translate('network_error'), [
            {
              text: "OK",
            }
          ]);
        } else {
          Alert.alert("Error", translate('network_error'), [
            {
              text: "OK",
            }
          ]);
        }
      })
      .catch((error) => {
        this.setState({ isLoadings: false });
        alert(error);
      });
  }

  renderRow = (item) => {
    return (
      <Review
        review={item.item}
      />
    );
  };

  renderOfficialRow = (item) => {
    return (
        <OfficialComments review={item.item}/>
    );
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        <Modal
            visible={this.state.isLoadings}
            animationType="fade"
            transparent={true}
            onRequestClose={() => console.log('Modal Closed')}
        >
          <View
              style={{
                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
          >
            <ActivityIndicator color="#fff" />
          </View>
        </Modal>
        <View style={styles.topTabWrapperStyle}>
          <TouchableOpacity
            style={this.state.activeTabIndex === 0 ? styles.activeTabControlStyle : styles.tabControlStyle}
            onPress={() => {
              this.setState({
                activeTabIndex: 0,
                officialComments: this.state.officialCommentCount === 0 ? true : false,
              });
            }}>
            <Text style={styles.tabControlText}>{translate('official_comment')}
              ({this.state.officialCommentCount})</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={this.state.activeTabIndex === 1 ? styles.activeTabControlStyle : styles.tabControlStyle}
            onPress={() => {
              this.setState({
                activeTabIndex: 1,
                officialComments: this.state.normalCommentCount === 0 ? true : false,
              });
            }}>
            <Text style={styles.tabControlText}>{translate('comments')}{' '}({this.state.normalCommentCount})</Text>
          </TouchableOpacity>
        </View>
        {this.state.officialComments === true && (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <View style={{ marginTop: height / 4.5, flexDirection: 'column' }}>
                <Text style={{
                  fontFamily: FontStyle.Light,
                  left: this.props.locale === 'cn' || this.props.locale === 'tw' || this.props.locale === 'zh' ? 45 : 0,
                }}>
                  {this.state.text}
                </Text>
                <View style={{ width: 200, height: 150 }}>
                <Image
                    source={require('../../../../images/inboxlogo_transparent.png')}
                    style={{
                      width: 120,
                      height: 120,
                      marginLeft: this.props.locale === 'cn' || this.props.locale === 'tw' || this.props.locale === 'zh' ? width / 8 : width / 4,
                    }}
                    resizeMode="contain"
                />
                </View>
              </View>
            </View>
        )}
        <View style={styles.rootStyle}>
          {this.state.activeTabIndex === 0 &&
          <FlatList
            style={{ flex: 1 }}
            data={this.state.dataSource}
            renderItem={item => this.renderOfficialRow(item)}
            keyExtractor={(item, index) => index.toString()}
            />
          }
          {this.state.activeTabIndex === 1 &&
          <FlatList
            style={{ flex: 1 }}
            data={this.state.commentDataSource}
            renderItem={item => this.renderRow(item)}
            keyExtractor={(item, index) => index.toString()}
            />
          }
        </View>

      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: Config.backGroundColor,
    flex: 1,
    flexDirection: 'column'
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 15,
    fontFamily: FontStyle.Regular
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  }
};

const mapStateToProps = state => ({
  component: state.component
});

export default connect(mapStateToProps)(Comment);
