import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView
} from 'react-native';

import { translate } from '../i18n';
import Config from '../Config';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../constants/FontStyle';

class TermOfService extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('term_service'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.headerStyle}>
          <Text style={styles.headerTextStyle}>{translate('term_service')}</Text>
        </View>
        <View style={{ height: 10 }}/>
        <ScrollView style={styles.scrollviewStyle}>
          <Text style={styles.h1Style}>{translate('legends_lair_website')}.</Text>
          <Text style={styles.pStyle}>
            {translate('welcome_legends_liar_website')}.
          </Text>
          <Text style={styles.h1Style}>{translate('acceptance_of_terms')}.</Text>
          <Text style={styles.pStyle}>
            {translate('consent_to_permits')}.
          </Text>
          <Text style={styles.h1Style}>{translate('tcs_updates')}.</Text>
          <Text style={styles.pStyle}>
            {translate('legends_lair_reserves')}.
          </Text>

        </ScrollView>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#f7f7f7',
  },

  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  headerTextStyle: {
    color: Config.textColor,
    fontSize: 35,
    fontWeight: 'bold',
  },

  scrollviewStyle: {
    flex: 1,
    paddingHorizontal: 40,
  },

  h1Style: {
    fontSize: 14,
    color: Config.textColor,
    fontWeight: 'bold',
  },

  pStyle: {
    fontSize: 12,
    color: Config.textColor,
    marginBottom: 10,
  },

  listStyle: {
    fontSize: 14,
    color: Config.textColor,
    fontWeight: 'bold',
  },

  listContentStyle: {
    marginLeft: 5,
    paddingLeft: 10,
    borderLeftWidth: 1,
    borderLeftColor: Config.textColor,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TermOfService);
