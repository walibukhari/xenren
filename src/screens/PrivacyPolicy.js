import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView
} from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { translate } from '../i18n';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

class PrivacyPolicy extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('privacy_policy'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.headerStyle}>
          <Text style={styles.headerTextStyle}>{translate('privacy_policy')}</Text>
        </View>
        <View style={{ height: 10 }}/>
        <ScrollView style={styles.scrollviewStyle}>
          <Text style={styles.h1Style}>{translate('our_policy')}:</Text>
          <Text style={styles.pStyle}>
            {translate('welcome_to_legends_lair_website')}.
          </Text>
          <Text style={styles.h1Style}>{translate('info_we_collect')}:</Text>
          <Text style={styles.pStyle}>
            {translate('when_you_interact')}:
          </Text>
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <FontAwesome name="circle" size={10} color={Config.textColor}/>
            <Text style={[styles.h1Style, {
              flex: 1,
              marginLeft: 5
            }]}>{translate('personal_data')}:</Text>
          </View>
          <View style={styles.listContentStyle}>
            <Text style={styles.pStyle}>
              {translate('we_collect_personal_data')}.
            </Text>
            <Text style={styles.pStyle}>
              {translate('by_voluntarily')}.
            </Text>
          </View>
          <View style={{
            flexDirection: 'row',
            alignItems: 'center'
          }}>
            <FontAwesome name="circle" size={10} color={Config.textColor}/>
            <Text style={[styles.h1Style, {
              flex: 1,
              marginLeft: 5
            }]}>
              {translate('non_identifiable')}:
            </Text>
          </View>
          <View style={styles.listContentStyle}>
            <Text style={styles.pStyle}>
              {translate('when_you_interact_with')}.
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
  },

  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  headerTextStyle: {
    color: Config.textColor,
    fontSize: 35,
    fontWeight: 'bold',
  },

  scrollviewStyle: {
    flex: 1,
    paddingHorizontal: 40,
  },

  h1Style: {
    fontSize: 14,
    color: Config.textColor,
    fontWeight: 'bold',
  },

  pStyle: {
    fontSize: 12,
    color: Config.textColor,
    marginBottom: 10,
  },

  listStyle: {
    fontSize: 14,
    color: Config.textColor,
    fontWeight: 'bold',
  },

  listContentStyle: {
    marginLeft: 5,
    paddingLeft: 10,
    borderLeftWidth: 1,
    borderLeftColor: Config.textColor,
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PrivacyPolicy);
