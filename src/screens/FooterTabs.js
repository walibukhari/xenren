/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable max-len */
/* eslint-disable prefer-const */
/* eslint-disable no-tabs */
/* eslint-disable indent */
/* eslint-disable no-unreachable */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-else-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-vars */
/* eslint-disable import/first */
/* eslint-disable global-require */
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Modal,
  Dimensions,
  Platform,
  NativeModules,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import Config from '../Config';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';
import HttpRequest from '../components/HttpRequest';


const { width } = Dimensions.get('window');
class FooterTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttons: [
        {
          active: true,
          middle: false,
          icon: require('../../images/select_seat/bottom_icon_home.png'),
          label: translate('sharedOffice'),
          topLabel: translate('sharedOffice'),
          // label: '共享办公',
          // topLabel: '共享办公',
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/select_seat/bottom_icon_chat.png'),
          label: translate('indoorChat'),
          topLabel: translate('indoorChat'),
        },
        {
          active: false,
          middle: true,
          icon: require('../../images/share_office/xenren.png'),
          label: translate('dashboard'),
          topLabel: translate('dashboard'),
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/select_seat/newMessage.png'),
          label: translate('message'),
          topLabel: translate('message'),
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/select_seat/bottom_icon_profile.png'),
          label: translate('myProfile'),
          topLabel: translate('myProfile'),
        },
      ],
      visibility: 0,
      showModal: false,
      selectChat: false,
      alertText: translate('join_sharedOffice_yet'),
    };
  }

  validateOtherBarCodeScan() {
    console.log('going to check other barcodes');
    HttpRequest.getOtherTypeBarCodes(this.props.userData.token)
      .then((response) => {
        if (response.data.status === 'error') {
          this.setState({
            isLoading: false,
            showModal: true,
          });
        } else if (response.data.status === 'success') {
          const data = response;
          if (response.data.data == null) {
            this.setState({
              showModal: true,
              isLoading: false,
            });
            return;
          }
          console.log(data.data, 'pass to IndoorChat');
          this.props.navigation.navigate('IndoorChat', {
            sharedOfficeData: data.data,
          });
          this.setState({ isLoading: false });
        } else if (data.status === 'failure') {
          this.setState({ isLoading: false });
        } else {
          this.setState({ isLoading: false });
        }
      })
      .catch((error) => {
        console.log('error');
        console.log(error);
        alert(error);
        this.setState({ isLoading: false });
      });
  }

  activateMenu(index, flag) {
    this.setState({ isLoading: true });
    let buttons = this.state.buttons;
    buttons = buttons.map((item) => {
      item.active = false;
      return item;
    });
    buttons[index].active = true;
    switch (index) {
      case 0: {
        if (this.props.navigation.state.routeName !== 'SelectChat') {
          this.props.navigation.navigate('SelectChat');
          return true;
        } else {
          return true;
        }
      }
      case 1: {
        if (this.props.navigation.state.routeName !== 'IndoorChat') {
          if (this.props.sharedOffice === null) {
            this.validateOtherBarCodeScan();
          } else {
            console.log('this.props.sharedOffice');
            console.log(JSON.stringify(this.props.sharedOffice));
            this.props.navigation.navigate('IndoorChat', {
              sharedOfficeData: this.props.sharedOffice,
            });
            return true;
          }
        } else {
          return true;
        }
      }
      case 2: {
        if (this.props.navigation.state.routeName !== 'Dashboard') {
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Dashboard',
              }),
            ],
          }));
          return true;
        } else {
          return true;
        }
      }
      case 3: {
        if (this.props.navigation.state.routeName !== 'Notification') {
          this.props.navigation.navigate('Notification');
          return true;
        } else {
          return true;
        }
      }
      case 4: {
        if (this.props.navigation.state.routeName !== 'Profile') {
          this.props.navigation.navigate('Profile');
          return true;
        } else {
          return true;
        }
      }
      default: {
        this.props.navigation.navigate('Dashboard');
        return true;
      }
    }
  }

  render() {
    return (

      <View style={styles.footerMenuStyle}>
      <View
          style={{
            backgroundColor: '#fff',
            height: 60,
            position: 'absolute',
            width: '100%',
            borderTopColor: Config.lineColor,
            borderTopWidth: 1,
            zIndex: 0,
          }}
        />
        {this.state.buttons.map((item, index) => {
          if (item.middle) {
            let color = item.active ? Config.primaryColor : Config.textColor;
            return (
              <TouchableOpacity
                key={index}
                style={styles.footerMiddleButtonStyle}
                onPress={() => {
                  this.activateMenu(index);
                }}
              >
                <Modal
                    onRequestClose={() => {
                      this.setState({
                        showModal: false,
                      });
                    }}
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showModal}
                >
                  <View style={styles.modalRootStyle}>
                    <View style={styles.modalBoxStyle}>
                      <View style={{ bottom: 25 }}>
                      <Image source={require('./../../images/alert.png')}
                             style={{ height: 70, width: 70 }} resizeMode='cover'/>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={{
                          fontFamily: FontStyle.Regular,
                          color: Config.redColor,
                          fontSize: 18,
                          textAlign: 'center',
                        }}>
                          {translate('join_sharedOffice_yet')}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={{
                          fontFamily: FontStyle.Regular,
                          color: Config.redColor,
                          fontSize: 18,
                          textAlign: 'center',
                        }}>
                          {translate('please_join_sharedOffice')}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={{
                          fontFamily: FontStyle.Regular,
                          color: Config.redColor,
                          fontSize: 18,
                          textAlign: 'center',
                        }}>
                          {translate('sharedOffice')}
                        </Text>
                      </View>
                      <TouchableOpacity style={{
                        backgroundColor: Config.redColor,
                        width: 65,
                        height: 35,
                        borderRadius: 5,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 20,
                      }}
                                        onPress={() => {
                                          this.setState({ showModal: false });
                                        }}
                      >
                        <Text style={{ color: Config.white }}>{translate('ok')}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </Modal>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: 42,
                    height: 42,
                    borderRadius: 20,
                    backgroundColor: '#fff',
                  }}
                >
                  <View style={styles.logoCircleStyle}>
                    <Image
                      source={item.icon}
                      style={{
                        height: 30,
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        position: 'absolute',
                      }}
                      resizeMode="contain"
                    />
                  </View>
                </View>

                <Text style={[styles.footerTextStyles, { color }]}>
                  {item.label}
                </Text>
              </TouchableOpacity>
            );
          } else {
            let color = this.props.currentIndex === index
                ? Config.primaryColor
                : Config.textColor;
            return (
              <TouchableOpacity
                key={index}
                style={[styles.footerButtonStyle, index === 0 ? { marginLeft: 7 } : { marginLeft: 0 }]}
                onPress={() => {
                  this.activateMenu(index, true);
                }}
              >
                <Image
                  source={item.icon}
                  style={{
                    height: 20,
                    tintColor: color,
                  }}
                  resizeMode="contain"
                />
                <Text style={[styles.footerTextStyle, { color }]}>
                  {item.label}
                </Text>
              </TouchableOpacity>
            );
          }
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  footerMenuStyle: {
    flexDirection: 'row',
    height: 50,
    width: '100%',
    alignItems: 'flex-end',
    top: Platform.OS === 'android' ? 0 : NativeModules.DeviceInfo.isIPhoneX_deprecated ? 13 : 5,
  },

  footerButtonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    flexDirection: 'column',
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogBoxStyle: {
    width: '80%',
    height: '31%',
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

  footerMiddleButtonStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 77,
    flexDirection: 'column',
    paddingTop: 8,
  },

  logoCircleStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 42,
    height: 42,
    borderRadius: 21,
    backgroundColor: '#fff',
    zIndex: 1,
  },
  modalRootStyle: {
    flex: 1,
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalBoxStyle: {
    width: width - 60,
    height: width / 1.7,
    backgroundColor: Config.white,
    alignItems: 'center',
    borderRadius: 5,
  },

  footerTextStyle: {
    fontSize: 11,
    marginTop: 4,
  },

  footerTextStyles: {
    fontSize: 11,
    marginTop: 2,
  },
});

const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    sharedOffice: state.auth.sharedOffice,
  };
};

export default connect(
  mapStateToProps,
  null,
)(FooterTabs);
