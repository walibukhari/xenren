import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import moment from 'moment';
import _ from 'lodash';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import HttpRequest from '../../components/HttpRequest';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import {translate} from "../../i18n";
import SpaceList from '../../../images/spacelist.jpg';
import {NavigationActions} from "react-navigation";

const { height } = Dimensions.get('window');

class MySpaces extends Component {
    static navigationOptions = {
        header: null,
    };
  constructor(props) {
    super(props);
    this.root = this.props.component.root;
    this.state = {
      totalAmountSpent: 0,
      spaces: [],
      filteredRecord: [],
      autocompleteText: [],
    };
  }

  componentDidMount() {
    this.getSpaces();
  }

  getSpaces() {
    HttpRequest.getMySpaces(this.props.userData.token)
      .then((response) => {
          console.log('response in  my spaces.js');
          console.log(response);
        this.setState({
          spaces: response.data.data,
          filteredRecord: response.data.data,
        });
      })
      .catch((error) => {
          console.log('error in my spaces.js ');
          console.log(error);
      });
  }

  // eslint-disable-next-line class-methods-use-this
  formatDate(date) {
    return moment(date).format('DD-MMMM-YY');
  }

  showDetail = (id, st, et, mi, ai, on, cd) => {
    this.props.navigation.navigate('SpaceDetail', {
      SpaceDetail: id,
      StartTime: st,
      EndTime: et,
      mainImage: mi,
      allImages: ai,
      officeName: on,
      contactDetail: cd,
    });
  }

  renderSpaceCard() {
    const rec = [];
    console.log('this.state.filteredRecord');
    this.state.filteredRecord.forEach((item) => {
        rec.push(
            <View
                key={item.id}
                style={{
                    width: '100%',
                    height: 120,
                    marginTop: 10,
                    borderRadius: 15,
                    // overflow: "hidden",
                    selfAlign: 'center',
                    textAlign: 'center',
                    align: 'center',
                    justifyContent: 'center',
                    alignItem: 'center',
                    borderWidth: 1,
                    borderColor: '#fff',
                    backgroundColor: '#fff',
                    flexDirection: 'row',
                    shadowColor: '#dfdfdf',
                    shadowOffset: {
                        width: 0,
                        height: 6,
                    },
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    elevation: 1,
                }}
            >
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    selfAlign: 'center',
                    textAlign: 'center',
                    align: 'center',
                    justifyContent: 'center',
                    alignItem: 'center',
                }}>
                    <View style={{
                        width: '35%',
                        height: '100%',
                        borderRadius: 15,
                        overflow: 'hidden',
                        selfAlign: 'center',
                        textAlign: 'center',
                        align: 'center',
                    }}>
                        <Image
                            style={{
                                width: 90,
                                height: 90,
                                borderRadius: 90 / 2,
                                overflow: 'hidden',
                                marginLeft: 10,
                                marginTop: 10,
                            }}
                            resizeMode="stretch"
                            source={{uri: item.office.compressed_image, cache: 'force-cache'}}/>
                    </View>
                    <View style={{
                        width: '65%',
                        height: '100%',
                        borderRadius: 15,
                        overflow: 'hidden',
                        selfAlign: 'flex-start',
                        textAlign: 'center',
                        align: 'center',
                        justifyContent: 'center',
                        alignItem: 'flex-start',
                        paddingLeft: 10,
                        // marginTop: -20
                    }}>
                        <Text style={{
                            fontFamily: FontStyle.Bold,
                            fontSize: 18,
                        }}>{item.office.office_name}</Text>
                        <Text style={{
                            fontFamily: FontStyle.Light,
                            fontSize: 12,
                        }}>
                            {this.formatDate(item.start_time)} - {this.formatDate(item.end_time)}
                        </Text>
                        <Text style={{
                            fontFamily: FontStyle.Light,
                            fontSize: 12,
                            marginTop: 20,
                        }}>{item.office.city.name}, {item.office.country.code}</Text>

                        <Text
                            style={{
                                fontFamily: FontStyle.Regular,
                                fontSize: 12,
                                marginTop: 20,
                                position: 'absolute',
                                bottom: 10,
                                right: 10,
                                borderWidth: 1,
                                borderColor: Config.primaryColor,
                                overflow: 'hidden',
                                borderRadius: 5,
                                color: Config.primaryColor,
                                padding: 5,
                            }}
                            onPress={() => {
                                this.showDetail(
                                    item.id,
                                    (item.start_time),
                                    (item.end_time),
                                    item.office.compressed_image,
                                    item.all_images,
                                    item.office.office_name,
                                    item.office.contact_phone,
                                );
                            }}
                        >
                            GET DETAILS
                        </Text>
                    </View>
                </View>
            </View>,
        );
    });
    return rec;
  }
    goBack = () => {
      this.props.navigation.goBack();
    };

  filter = (text) => {
    if (text && text !== '') {
      const filteredRecord = _.filter(this.state.filteredRecord, (item) => {
        if (item.office.office_name.search(text) !== -1) {
          return item;
        }
        return null;
      });
      // filteredSkills = _.slice(filteredSkills, 0, 10);
      this.setState({
        filteredRecord,
      });
    } else {
      this.setState({
        filteredRecord: this.state.spaces,
      });
    }
  }

  render() {
      let renderData;
      if(this.state.filteredRecord.length === 0) {
          renderData = <View>
              <Image style={{width:240,height:200,marginTop:100,display:'flex',justifyContent:'center',marginLeft:50,marginRight:50}} source={require('../../../images/space.png')}/>
              <Text
                  style={{
                      fontSize: 20,
                      fontWeight:'bold',
                      fontFamily: FontStyle.Regular,
                      display:'flex',
                      justifyContent:'center',
                      marginLeft:100,
                      marginTop:10,
                      marginRight:90
                  }}
              >No Office Found</Text>
          </View>
      } else {
          renderData = this.renderSpaceCard()
      }
    return (
        <View>
            <View style={styles.rootStyle}>
                {/* Back Button View */}
                <View style={styles.navigationStyle}>
                    <View style={styles.navigationWrapper}>
                        <TouchableOpacity
                            onPress={this.goBack}
                            style={{
                                marginTop: 8,
                                marginLeft: 10,
                                flexDirection: "row",
                            }}
                        >
                            <Image
                                style={{width:20,height:26,position:'relative',top:-3}}
                                source={require('../../../images/arrowLA.png')}
                            />

                            <Text
                                style={{
                                    marginLeft: 10,
                                    marginTop:-1,
                                    fontSize: 19.5,
                                    fontWeight:'normal',
                                    color: Config.primaryColor,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate("my_Space_list")}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
             <ScrollView>
        <View
          style={orientation.portrait.searchBoxWrapperStyle}
          ref="SearchInput"
          onLayout={({ nativeEvent }) => {
            this.refs.SearchInput.measure(
              (x, y, width, height, pageX, pageY) => {
                this.setState({
                  autocompletePosition: {
                    x: pageX,
                    y: pageY - 35,
                    width,
                  },
                });
              },
            );
          }}
        >
          <View
            showsHorizontalScrollIndicator={true}
            horizontal={true}
            style={{
              flexDirection: 'row',
              width: '80%',
              height: '100%',
              marginLeft: 5,
              marginRight: 5,
            }}
          >
            <TextInput
              style={{
                flex: 1,
                width: 310,
                padding: 8,
              }}
              autoCapitalize="none"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              onChangeText={(autocompleteText) => {
                this.setState({ autocompleteText });
                this.filter(autocompleteText);
              }}
              value={this.state.autocompleteText}
            />
          </View>

          <TouchableOpacity
              style={styles.searchButtonStyle}
              onPress={() => {
                this.showSearchResult();
              }}>
              <Ionicons name="ios-search" color={'black'} size={35} />
          </TouchableOpacity>
        </View>

        <View style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'flex-start',
          top: 45,
          padding: 15,
          paddingBottom: 50,
          selfAlign: 'center',
          textAlign: 'center',
          align: 'center',
          justifyContent: 'center',
          alignItem: 'center',
          minHeight: 100,
        }}>
            {renderData}
        </View>
      </ScrollView>
        </View>
    );
  }
}

const styles = {
  searchButtonStyle: {
    paddingRight: 5,
    backgroundColor: 'transparent',
    marginRight: 6,
  },
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
};

const orientation = {
  portrait: {
    logo: {
      color: '#fff',
      fontSize: 42,
      fontFamily: FontStyle.Bold,
      position: 'absolute',
      top: height * 0.6 - 330,
    },
    subtitle: {
      color: '#fff',
      fontSize: 11,
      fontFamily: FontStyle.Medium,
      position: 'absolute',
      top: height * 0.6 - 285,
    },
    searchBoxWrapperStyle: {
      borderRadius: 25,
      backgroundColor: '#d7d7d7',
      height: 45,
      width: '90%',
      flexDirection: 'row',
      alignItems: 'center',
      selfAlign: 'center',
      position: 'absolute',
      marginLeft: 15,
      top: 10,
    },
  },
  landscape: {
    logo: {
      color: '#fff',
      fontSize: 42,
      fontFamily: FontStyle.Bold,
      position: 'absolute',
      top: 10,
    },
    subtitle: {
      color: '#fff',
      fontSize: 11,
      fontFamily: FontStyle.Medium,
      position: 'absolute',
      top: 57,
    },
    searchBoxWrapperStyle: {
      borderRadius: 2,
      backgroundColor: '#fff',
      height: 45,
      width: '90%',
      flexDirection: 'row',
      alignItems: 'center',
      position: 'absolute',
      top: 72,
    },
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
  };
}

export default connect(mapStateToProps)(MySpaces);
