/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import HttpRequest from '../../components/HttpRequest';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";
import {translate} from "../../i18n";

class SpaceDetail extends Component {

  static navigationOptions = ({ navigation }) => ({
        title: null,
        headerStyle: {
            backgroundColor: '#FFF',
            borderBottomColor: '#FFF',
        },
        headerTintColor: Config.topNavigation.headerIconColor,
        headerTitleStyle: {
            color: Config.topNavigation.headerTextColor,
            alignSelf: 'center',
            fontFamily: FontStyle.Regular,
            width: '100%',
        },
        headerLeft:
            <View style={style.rootStyle}>
                {/* Back Button View */}
                <View style={style.navigationStyle}>
                    <View style={style.navigationWrapper}>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{
                                marginTop: 8,
                                marginLeft: 10,
                                flexDirection: "row",
                            }}
                        >
                            <Image
                                style={{width:20,height:26,position:'relative',top:-1}}
                                source={require('../../../images/arrowLA.png')}
                            />

                            <Text
                                onPress={() => navigation.goBack()}
                                style={{
                                    marginLeft: 10,
                                    marginTop:0,
                                    fontSize: 19.5,
                                    fontWeight:'normal',
                                    color: Config.primaryColor,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate('space_detail')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>,
    });

  constructor(props) {
    super(props);
    this.root = this.props.component.root;
    this.state = {
      totalAmountSpent: 0,
      office: [],
      start_time: this.props.navigation.state.params.StartTime,
      end_time: this.props.navigation.state.params.EndTime,
      main_image: this.props.navigation.state.params.mainImage,
      all_image: this.props.navigation.state.params.allImages,
      office_name: this.props.navigation.state.params.officeName,
      contact_phone: this.props.navigation.state.params.contactDetail,
    };
  }

  componentDidMount() {
      console.log('SpaceDetail.js');
    this.getOfficeDetail();
  }

  getOfficeDetail = () => {
    HttpRequest.getSharedOfficeDetailById(
      this.props.userData.token,
      this.props.userData.id,
      this.props.navigation.state.params.SpaceDetail,
    )
      .then((response) => {
        // let detail = Object.assign({}, response.data[0]['office_details']);
        this.setState({
          office: response.data,
          // detail: detail
        });
      })
      .catch((error) => {
        console.log('error in HttpRequest', error);
      });
  }

  // _renderOfficeName(){
  //     let detail = this.state.office;
  //     return detail.office_name;
  // }

  // _renderOfficePhone(){
  //     let detail = this.state.office;
  //     return detail.contact_phone;
  // }

  // _renderOfficeEmail(){
  //     let detail = this.state.office;
  //     return detail.contact_email;
  // }

  renderOpeningTime() {
    const detail = this.state.office;
    return `Monday - Saturday ${moment(detail.monday_opening_time).format('HH-MM')} - ${moment(detail.monday_closing_time).format('HH-MM')}`;
  }

  renderSundayTime() {
    const detail = this.state.office;

    try {
      return `Sunday ${moment(detail.sunday_opening_time).format('HH-MM')}`;
    } catch (e) {
      return `Sunday ${detail.sunday_opening_time}`;
    }
  }

  // eslint-disable-next-line class-methods-use-this
  formatDate(date) {
    return moment(date).format('DD-MMMM-YY');
  }

  renderImages() {
    console.log(this.state.office.moreimages);
    console.log(typeof this.state.office.moreimages);
    const img = [];
    this.state.all_image.forEach((item) => {
      console.log(item);
      img.push(
        <View style={{ width: '33.33%', height: 50, paddingRight: 10 }} key={item.id}>
          <Image
            source={{ uri: item.compressed_image, cache: 'force-cache' }}
            style={{
              width: '100%',
              height: 80,
              borderRadius: 12,
              overflow: 'hidden',
            }}
            resizeMode="cover"/>
        </View>,
      );
    });
    console.log(img);
    return img;
  }

  renderMainImage() {
    console.log('main image');
    console.log(this.state.office.image);
    return (
      <Image
        source={{ uri: this.state.main_image, cache: 'force-cache' }}
        style={{
          width: '100%',
          height: '100%',
          borderRadius: 12,
          overflow: 'hidden',
        }}
        resizeMode="cover"/>
    );
  }

  render() {
    return (
      <ScrollView>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'stretch',
            padding: 20,
            backgroundColor: 'white',
          }}
        >
          <View style={{ width: '100%', height: 250 }}>
            {this.renderMainImage()}
          </View>
          <View style={{ height: 80, marginTop: 10 }}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'stretch' }}>
              {this.renderImages()}
            </View>
          </View>
          <View style={{ height: 100, marginTop: 15 }}>
            <Text style={{
              fontFamily: FontStyle.Bold,
              alignSelf: 'center',
              fontSize: 20,
            }}>
              {this.state.office_name}
            </Text>
            <Text style={{
              fontFamily: FontStyle.Light,
              alignSelf: 'center',
              fontSize: 20,
            }}>
              {this.state.office.contact_phone}
            </Text>
            <Text style={{
              fontFamily: FontStyle.Light,
              alignSelf: 'center',
              fontSize: 20,
            }}>
              {this.state.contact_phone}
            </Text>
          </View>
          <View style={{
            height: 250,
            backgroundColor: Config.primaryColor,
            padding: 10,
          }}>
            <Text style={{
              fontFamily: FontStyle.Bold,
              alignSelf: 'center',
              fontSize: 20,
              color: 'white',
            }}>
              Unlimited Pass
            </Text>

            <View style={{
              height: 80,
              marginTop: 30,
              backgroundColor: 'white',
              borderRadius: 12,
            }}>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'stretch',
                textAlign: 'center',
              }}>
                  <View style={{
                    width: '40%',
                    height: 50,
                    paddingRight: 10,
                    marginTop: 20,
                    textAlign: 'center',
                  }}>
                      <Text style={{
                        fontFamily: FontStyle.Bold,
                        fontSize: 18,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          Start Time
                      </Text>
                      <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 10,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          {this.formatDate(this.state.start_time)}
                      </Text>
                      <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 10,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          09-30 AM - 10:11PM
                      </Text>
                  </View>
                  <View style={{
                    width: '20%',
                    height: 50,
                    paddingRight: 10,
                    alignItems: 'stretch',
                    textAlign: 'center',
                    // borderRadius: 100,
                    borderWidth: 1,
                    marginTop: -25,
                    backgroundColor: 'transparent',
                    borderColor: 'transparent',
                  }}>
                      <View style={[{ flexDirection: 'column', alignItems: 'center' }, {
                        flex: 1,
                        backgroundColor: '#ecf5fd',
                        marginLeft: 24,
                        marginRight: 24,
                        marginBottom: 24,
                      }]}>
                          <View style={{
                            backgroundColor: '#fff',
                            height: 50,
                            width: 50,
                            borderRadius: 50,
                          }}>
                              <Ionicons
                                  name="md-alarm"
                                  size={38}
                                  color={'black'}
                                  style={{
                                    align: 'center',
                                    textAlign: 'center',
                                  }}
                              />
                          </View>
                          <View style={{
                            height: 100,
                            width: 60,
                          }}>
                              <Text style={{
                                backgroundColor: Config.primaryColor,
                                color: 'white',
                                fontFamily: FontStyle.Bold,
                                fontSize: 8,
                                textAlign: 'center',
                                borderRadius: 5,
                                overflow: 'hidden',
                                height: 14,
                              }}>Extend Hours</Text>
                          </View>
                      </View>

                  </View>
                  <View style={{
                    width: '40%',
                    height: 50,
                    paddingRight: 10,
                    marginTop: 20,
                    textAlign: 'center',
                  }}>
                      <Text style={{
                        fontFamily: FontStyle.Bold,
                        fontSize: 18,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          End Time
                      </Text>
                      <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 10,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          {this.formatDate(this.state.start_time)}
                      </Text>
                      <Text style={{
                        fontFamily: FontStyle.Light,
                        fontSize: 10,
                        color: 'black',
                        textAlign: 'center',
                      }}>
                          09-30 AM - 10:11PM
                      </Text>
                  </View>
              </View>
            </View>
            <Text style={{
              fontFamily: FontStyle.Bold,
              alignSelf: 'center',
              fontSize: 20,
              color: '#fff',
              marginTop: 20,
            }}>
              Office Hours
            </Text>
            <Text style={{
              fontFamily: FontStyle.Light,
              alignSelf: 'center',
              fontSize: 15,
              color: '#fff',
            }}>
              {this.renderOpeningTime()}
            </Text>
            <Text style={{
              fontFamily: FontStyle.Light,
              alignSelf: 'center',
              fontSize: 15,
              color: '#fff',
            }}>
              {this.renderSundayTime()}
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 48,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
  };
}

export default connect(mapStateToProps)(SpaceDetail);
