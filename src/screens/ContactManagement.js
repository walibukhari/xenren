/* eslint-disable prefer-const */
/* eslint-disable no-mixed-operators */
/* eslint-disable import/first */
/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-key */
/* eslint-disable arrow-parens */
/* eslint-disable indent */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
/* eslint-disable func-names */
/* eslint-disable max-len */
/* eslint-disable global-require */
/* eslint-disable react/no-unescaped-entities */
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Switch,
  Text,
  View,
  FlatList,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Image,
  Modal,
  ActivityIndicator,
  Alert,
  Dimensions
} from "react-native";
import Config from "../Config";
import ManageContactDialog from "./subs/ManageContactDialog";
import { translate } from "../i18n";
import HttpRequest from "../components/HttpRequest";
import { NavigationActions } from "react-navigation";
import LocalData from "../components/LocalData";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import FontStyle from "../constants/FontStyle";

const { height, width } = Dimensions.get("window");

class ContactManagement extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: translate('contact_management'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle: {
        color: Config.topNavigation.headerTextColor,
        alignSelf: "center",
        fontFamily: FontStyle.Regular,
        width: "100%"
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: "row",
            alignSelf: "center",
            padding: 5
          }}
        >
          <SimpleLineIcons
            size={16}
            name="arrow-left"
            color={Config.topNavigation.headerIconColor}
          />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      userInfo: this.props.userData,
      disable: true,
      reportUser: false,
      reasonToReport: "",
      isLoading: false,
      trueSwitchIsOn: true,
      falseSwitchIsOn: false,
      showDialog: false,
      dataFromChild: null,
      isShowSuccess: false,
      blockedUsersImages: [],
      blockedUsersIDs: [],
      currentUserBlocked: false,
      blockContactForSelectedUsers: [
        {
          avatar: require('../../images/contact_management/Tencent_qq.png'),
          name: translate('qq'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/skype-icon.png'),
          name: translate('skype'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/wechat-logo.png'),
          name: translate('wechat'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/MetroUI_phone.png'),
          name: translate('phone'),
          isSelect: false,
        },
      ],
      blockContactForAll: [
        {
          avatar: require('../../images/contact_management/Tencent_qq.png'),
          name: translate('qq'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/skype-icon.png'),
          name: translate('skype'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/wechat-logo.png'),
          name: translate('wechat'),
          isSelect: false,
        },
        {
          avatar: require('../../images/contact_management/MetroUI_phone.png'),
          name: translate('phone'),
          isSelect: false,
        },
      ],
    };
  }

  componentDidMount() {
    // console.log(this.props.navigation.state.params.userInfo.img_avatar);
    this.getBlockedUsersList();
  }

  getBlockedUsersList = () => {
    HttpRequest.blockedUsers(this.props.userData.token)
      .then(response => {
        if (response.data.status === 'success') {
          response.data.data.map(item => {
            if (item.to_user === null) {
              // eslint-disable-next-line no-useless-return
              return;
            }
            if (
              item.to_user.id === this.props.navigation.state.params.userInfo.id
            ) {
              this.setState(
                {
                  currentUserBlocked: true,
                  blockedUsersImages: [
                    ...this.state.blockedUsersImages,
                    this.props.navigation.state.params.userInfo.img_avatar
                  ],
                  blockedUsersIDs: [
                    ...this.state.blockedUsersIDs,
                    this.props.navigation.state.params.userInfo.id
                  ],
                },
                () => {
                  // console.log('console list ----- ', this.state.blockedUsersImages);
                },
              );
            }
          });
          // let selUsers_Arr = this.state.blockContactForSelectedUsers;
          // selUsers_Arr.map((item) => {
          //   console.log(`selUsers_Arr =-=-=-=-=-=-=-=-=> `, item);
          //   if (item.name === 'qq') {
          //     item.isSelect = response.data.data.platform_info.qq === true;
          //   } else if (item.name === 'phone') {
          //     item.isSelect = response.data.data.platform_info.phone === true;
          //   } else if (item.name === 'wechat') {
          //     item.isSelect = response.data.data.platform_info.wechat === true;
          //   } else if (item.name === 'skype') {
          //     item.isSelect = response.data.data.platform_info.skype === true;
          //   }
          // });
          // let allUsers_Arr = this.state.blockContactForAll;
          // allUsers_Arr.map((item) => {
          //   if (item.name === 'qq') {
          //     item.isSelect = response.data.data.all_contact.qq === true;
          //   } else if (item.name === 'phone') {
          //     item.isSelect = response.data.data.all_contact.phone === true;
          //   } else if (item.name === 'wechat') {
          //     item.isSelect = response.data.data.all_contact.wechat === true;
          //   } else if (item.name === 'skype') {
          //     item.isSelect = response.data.data.all_contact.skype === true;
          //   }
          // });
          // console.log('array ------- ', selUsers_Arr, allUsers_Arr);
          // this.setState({
          //   blockContactForAll: allUsers_Arr,
          //   blockContactForSelectedUsers: selUsers_Arr,
          // }, () => {
          //   console.log('state -------- ', this.state.blockContactForAll, this.state.blockContactForSelectedUsers);
          //   console.log('console state image ----- ', this.state.blockedUsersImages);
          // });
        } else if (response.data.data.status === 'failure') {
          alert(translate('network_error'));
        } else {
          alert(translate('network_error'));
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  reportUser = () => {
    let reason = this.state.reasonToReport;
    let userID = this.props.navigation.state.params.userInfo.id;
    this.setState({
      reportUser: false,
      isLoading: true
    });
    HttpRequest.reportUser(this.state.userInfo.token, userID, reason)
      .then(response => {
        const result = response.data;
        if (result.status === 'success') {
          this.showAlert('Success', result.message);
        } else if (result.status === 'failure') {
          alert(translate('network_error'));
        } else {
          alert(result.message);
        }
        this.setState({ isLoading: false });
      })
      .catch(error => {
        alert(error);
        this.setState({ isLoading: false });
      });
  };

  blockSingleUser = () => {
    const user_id = this.props.navigation.state.params.userInfo.id;
    HttpRequest.blockSingleUser(this.props.userData.token, user_id)
      .then(response => {
        const result = response.data;
        if (result.status === 'success') {
          this.setState(
            {
              currentUserBlocked: true,
              blockedUsersImages: [
                ...this.state.blockedUsersImages,
                this.props.navigation.state.params.userInfo.img_avatar
              ],
            },
            () => {
              // console.log('state iamge 2 ----- ', this.state.blockedUsersImages);
            },
          );
          alert(translate('current_user_blocked'));
        } else if (result.status === 'failure') {
          alert(translate('network_error'));
          this.setState({ currentUserBlocked: false });
        } else {
          alert(translate('network_error'));
        }
      })
      .catch(error => {
        alert(error);
        this.setState({ currentUserBlocked: false });
      });
  };

  // To Unblock Single User
  unblockSingleUser = () => {
    const user_id = this.props.navigation.state.params.userInfo.id;
    const data = {
      users: user_id,
      all: 0,
      platform: ""
    };
    HttpRequest.unblockSingleUser(this.state.userInfo.token, data)
      .then(response => {
        if (response.data.status === 'success') {
          alert(translate('current_user_unblocked'));
          const arr = this.state.blockedUsersIDs;
          const index = arr.indexOf(user_id);
          arr.splice(index, 1);

          this.setState(
            {
              currentUserBlocked: false,
              blockedUsersImages: this.state.blockedUsersImages.splice(
                index,
                1
              ),
              blockedUsersIDs: arr
            },
            () => {
              // console.log('state ------- ', arr, this.state.blockedUsersImages, this.state.blockedUsersIDs);
            }
          );
        } else if (response.data.status === 'failure') {
          alert(translate('network_error'));
        } else {
          alert(translate('network_error'));
        }
      })
      .catch(error => {
        alert(error);
      });
  };

  blockUserTapped = () => {
    if (this.state.currentUserBlocked) {
      // console.log('Unblock User');
      this.setState({ currentUserBlocked: false });
      const userIndex = this.state.blockedUsersImages.indexOf(
        this.props.navigation.state.params.userInfo.id
      );
      let tempArr = this.state.blockedUsersIDs;
      tempArr = tempArr.splice(userIndex, 1);
      this.setState({});
      this.unblockSingleUser();
    } else {
      // this.setState({
      //     currentUserBlocked: true,
      //     blockedUsersImages: [...this.state.blockedUsersImages, this.props.navigation.state.params.userInfo.img_avatar],
      //     blockedUsersIDs: [...this.state.blockedUsersIDs, this.props.navigation.state.params.userInfo.id]
      // });
      this.blockSingleUser();
    }
  };

  hideContacts = (selUsers) => {
    if (this.state.blockedUsersIDs.length === 0) {
      alert(translate('select_user_from_specific_platform'));
    } else {
      const users = `${this.state.blockedUsersIDs}`;
      const formData = new FormData();
      formData.append('user_id', users);
      formData.append('plateForm', JSON.stringify(selUsers));
      HttpRequest.hideInfo(this.props.userData.token, formData)
        .then((response) => {
          const result = response.data;
          if (result.status === 'success') {
            this.setState({ isShowSuccess: true });
            setTimeout(() => {
              this.setState({
                isShowSuccess: false,
              });
            }, 2000);
          } else if (result.status === 'failure') {
            alert(translate('network_error'));
          } else {
            alert(result.message);
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  };

  showAlert(title, message) {
    // Show Dialog
    global.setTimeout(() => {
      Alert.alert(title, message);
    }, 200);
  }

  onButton = (item) => {
    if (item === true) {
      this.setState({ disable: false });
    }
  };

  goToIndoorChatRoom = () => {
    // console.log(this.props.userData);
    const resetAction = NavigationActions.reset({
      index: 1,
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Dashboard' }),
        NavigationActions.navigate({
          routeName: 'IndoorChat',
          params: { sharedOfficeData: this.props.sharedOffice },
        }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  };

  myCallback = (userImages, userIDs) => {
    this.setState({
      blockedUsersImages: userImages,
      blockedUsersIDs: userIDs,
      showDialog: false,
    });
  };

  renderBlockedUsers = item => {
    return item.map((image, index) => {
      return (
        <View key={index}>
          <Image
            source={{ uri: `${Config.webUrl}${image}` }}
            style={styles.selectedUser}
          />
        </View>
      );
    });
  };

  closeModal = value => {
    this.setState({ showDialog: value });
  };

  renderContactForSelectedUsers = (item, index) => (
    <View style={styles.rowStyle}>
      <Image source={item.avatar} style={{ height: 20 }} resizeMode="contain" />
      <Text style={styles.wordstyle}>{translate(item.name)}</Text>

      <View style={styles.switchViewStyle}>
        <Switch
          onTintColor="#228b22"
          thumbTintColor="#fff"
          value={item.isSelect}
          onValueChange={value =>
            this.updateSwitchValue(
              'selected',
              index,
              value,
              this.state.blockContactForSelectedUsers
            )
          }
        />
      </View>
    </View>
  );

  renderContactForAllUsers = (item, index) => (
    <View style={styles.rowStyle}>
      <Image source={item.avatar} style={{ height: 20 }} resizeMode="contain" />
      <Text style={styles.wordstyle}>{translate(item.name)}</Text>

      <View style={styles.switchViewStyle}>
        <Switch
          onTintColor="#228b22"
          thumbTintColor="#fff"
          value={item.isSelect}
          onValueChange={value =>
            this.updateSwitchValue(
              'all',
              index,
              value,
              this.state.blockContactForAll,
            )
          }
        />
      </View>
    </View>
  );

  updateSwitchValue = (key, index, value, array) => {
    const newArray = [...array];
    newArray[index] = {
      ...newArray[index],
      isSelect: value,
    };
    if (key === 'selected') {
      this.setState({ blockContactForSelectedUsers: newArray }, function() {
        const allUsers = {};
        const selUsers = {};

        for (const contact in this.state.blockContactForSelectedUsers) {
          if (
            this.state.blockContactForSelectedUsers[contact].isSelect === true
          ) {
            this.state.blockContactForAll[contact].isSelect === true
              ? (allUsers[this.state.blockContactForAll[contact].name] = 1)
              : (selUsers[this.state.blockContactForAll[contact].name] = 1);
          } else {
            this.state.blockContactForAll[contact].isSelect === true
              ? (allUsers[this.state.blockContactForAll[contact].name] = 1)
              : '';
          }
        }
        this.hideContacts(selUsers);
      });
    } else {
      this.setState({ blockContactForAll: newArray }, function() {
        const allUsers = {};
        const selUsers = {};

        for (const contact in this.state.blockContactForAll) {
          if (this.state.blockContactForAll[contact].isSelect === true) {
            this.state.blockContactForAll[contact].isSelect === true
              ? (allUsers[this.state.blockContactForAll[contact].name] = 1)
              : (selUsers[
                  this.state.blockContactForSelectedUsers[contact].name
                ] = 1);
          } else {
            this.state.blockContactForSelectedUsers[contact].isSelect === true
              ? (selUsers[
                  this.state.blockContactForSelectedUsers[contact].name
                ] = 1)
              : '';
          }
        }
        this.hideContactsFromAll(allUsers);
      });
    }
  };

  hideContactsFromAll = (allUsers) => {
    // console.log('allUser-->', Object.keys(allUsers)); // convert into array
    const formData = new FormData();
    formData.append('plateForm', JSON.stringify(allUsers));
    HttpRequest.hideFromAll(this.props.userData.token, formData)
        .then((response) => {
          if (response.data.status === 'success') {
            this.setState({ isShowSuccess: true });
            setTimeout(() => {
              this.setState({
                isShowSuccess: false,
              });
            }, 2000);
          } else {
            alert(response.data.message);
          }
        })
        .catch((error) => {
          alert(error);
        });
  };

  render() {
    return (
      <ScrollView style={styles.rootStyle}>
        {this.state.isLoading && (
          <View
            style={{
              position: 'absolute',
              top: height / 2 - 25,
              justifyContent: 'center',
              alignItems: 'center',
              height: 50,
            }}
          >
            <ActivityIndicator size='large' color={Config.primaryColor} />
          </View>
        )}

        <View style={{ paddingHorizontal: 26 }}>
          <View style={{ flexDirection: 'row' }}>
            <Text
              style={{
                color: '#000000',
                marginTop: 12,
                fontWeight: 'bold',
                fontSize: 15,
              }}
            >
              {translate('dont_let_people_see_my_contact')}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: 16,
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginBottom: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <View style={{ marginRight: 10 }}>
                <TouchableOpacity
                  disabled={this.state.disable}
                  onPress={() => this.setState({ showDialog: true })}
                >
                  <Image
                    source={require('../../images/contact_management/add_icon.png')}
                    style={{
                      height: 30,
                      width: 30,
                      borderRadius: 20,
                    }}
                    resizeMode='contain'
                  />
                </TouchableOpacity>
              </View>

              <ScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginBottom: 5,
                  }}
                >
                  {this.renderBlockedUsers(this.state.blockedUsersImages)}
                </View>
              </ScrollView>
            </View>
          </View>
        </View>

        {/* Block Selected Users  */}
        <FlatList
          data={this.state.blockContactForSelectedUsers}
          extraData={this.state}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) =>
            this.renderContactForSelectedUsers(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
        />

        <View
          style={{
            borderBottomColor: '#ccc',
            paddingHorizontal: 26,
            height: 50,
          }}
        >
          <Text
            style={{
              color: '#000000',
              marginTop: 12,
              fontWeight: 'bold',
              fontSize: 15
            }}
          >
            {translate('dont_let_all_people_see_myContact')}
          </Text>
        </View>

        {/* Block All Users  */}
        <FlatList
          data={this.state.blockContactForAll}
          extraData={this.state}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) =>
            this.renderContactForAllUsers(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
        />

        <View style={{ height: 45 }} />
        <View style={{ flexDirection: 'column' }}>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              onPress={() => this.blockUserTapped()}
              style={{
                backgroundColor: '#5ec329',
                borderRadius: 4,
                flex: 1,
                height: 45,
                alignItems: 'center',
                justifyContent: 'center',
                margin: 10,
                marginBottom: 0,
                padding: 0,
              }}
            >
              <Text
                style={{
                  color: 'white',
                  fontSize: 18,
                  fontWeight: '600',
                  textAlign: 'center',
                }}
              >
                {this.state.currentUserBlocked === true
                  ? translate('unblock_user')
                  : translate('block_user')}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              onPress={() => this.setState({ reportUser: true })}
              style={{
                backgroundColor: '#ffff',
                borderRadius: 4,
                flex: 1,
                height: 45,
                borderColor: '#f26c4e',
                borderWidth: 2,
                alignItems: 'center',
                justifyContent: 'center',
                margin: 10,
                padding: 0,
              }}
            >
              <Text
                style={{
                  color: '#f26c4f',
                  fontSize: 18,
                  fontWeight: '600',
                  textAlign: 'center',
                }}
              >
                {translate('report_user')}
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* Show Users List Modal */}
        <ManageContactDialog
          showDialog={this.state.showDialog}
          callbackFromParent={this.myCallback}
          disableButton={this.onButton}
          user_ids={this.state.blockedUsersIDs}
        />

        {/* Report User Modal */}
        <Modal
          animationType='fade'
          transparent={true}
          visible={this.state.reportUser}
          onRequestClose={() => console.log('Modal Closed')}
        >
          <View style={styles.verifyDialogStyle}>
            <View style={styles.verifyInfoBoxStyle}>
              <Image
                style={{
                  resizeMode: 'contain',
                  margin: 10,
                }}
                source={require('../../images/contact_management/block_icon.png')}
              />
              <Text
                style={{
                  color: 'black',
                  fontSize: 18,
                  textAlign: 'center',
                  padding: 5,
                }}
              >
                {translate('let_us_know_what_happen')}.
              </Text>

              <View
                style={{
                  width: '93%',
                  borderRadius: 2,
                  borderColor: '#E7E8E9',
                  borderWidth: 1,
                  marginTop: 10,
                  marginBottom: 5,
                }}
              >
                <View
                  style={{
                    width: '100%',
                    height: 25,
                    backgroundColor: '#F3F4F6',
                    justifyContent: 'center',
                  }}
                >
                  <Text
                    style={{
                      color: '#87888A',
                      paddingLeft: 10,
                      textAlign: 'left',
                      fontSize: 11,
                      fontWeight: '400',
                    }}
                  >
                    {translate('fill_reason')}
                  </Text>
                </View>

                <TextInput
                  style={{
                    width: '100%',
                    height: 80,
                    textAlignVertical: 'top',
                    padding: 8,
                    backgroundColor: 'white',
                  }}
                  // placeholder='Write your reason here...'
                  multiline={true}
                  numberOfLines={7}
                  underlineColorAndroid="transparent"
                  onChangeText={text => this.setState({ reasonToReport: text })}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '93%',
                  marginTop: 8,
                  marginBottom: 10,
                }}
              >
                <TouchableOpacity
                  style={{
                    width: '48%',
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: Config.primaryColor,
                    borderWidth: 2,
                    borderRadius: 3,
                  }}
                  onPress={() => this.setState({ reportUser: false })}
                >
                  <Text
                    style={{
                      color: Config.primaryColor,
                      textAlign: 'center',
                      fontSize: 14,
                    }}
                  >
                    {translate('CANCEL')}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    width: '48%',
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Config.primaryColor,
                    borderRadius: 3,
                  }}
                  onPress={() => this.reportUser()}
                >
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'center',
                      fontSize: 14,
                    }}
                  >
                    {translate('submit')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        {/* Successfully block User Contacts Modal */}
        <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.isShowSuccess}
            onRequestClose={() => console.log('Modal Close')}
        >
          <View style={styles.dialogStyle}>
            <View style={styles.dialogBoxStyle}>
              <Image
                  source={require('./../../images/login/ticklogo.png')}
                  style={{ height: 85 }}
                  resizeMode="contain"
              />
              <View style={{ height: 12 }} />
              <Text
                  style={{
                    fontFamily: FontStyle.Regular,
                    color: Config.primaryColor,
                    fontSize: 25,
                    textAlign: 'center',
                  }}
              >
                {translate('info_success')}
              </Text>
              <Text style={{
                fontFamily: FontStyle.Regular,
                color: Config.primaryColor,
                fontSize: 25,
              }}>
                {translate('updated')}
              </Text>
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: "#F3F3F3",
    height: 1334
  },
  rowStyle: {
    // height: 35,
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 26,
    backgroundColor: "#fff",
    paddingVertical: 10
  },
  loading: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center"
  },
  wordstyle: {
    marginLeft: 12,
    color: "#000000"
  },
  avatarStyle: {
    height: 30,
    width: 30,
    borderRadius: 25,
    margin: 10,
    flex: 1
  },
  blockUserButtonStyle: {
    backgroundColor: Config.primaryColor,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  reportUserButtonStyle: {
    backgroundColor: "#FF0000",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  switchViewStyle: {
    flex: 1,
    alignItems: "flex-end"
  },

  selectedUser: {
    height: 40,
    width: 40,
    marginRight: 10,
    borderRadius: 20
  },
  verifyDialogStyle: {
    flex: 1,
    backgroundColor: "rgba(44, 62, 80, 0.6)",
    alignItems: "center",
    justifyContent: "center"
  },
  verifyInfoBoxStyle: {
    width: "90%",
    height: 340,
    borderRadius: 10,
    backgroundColor: "#fff",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogBoxStyle: {
    width: 240,
    height: 220,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
    sharedOffice: state.auth.sharedOffice
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: "set_root",
        root
      })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactManagement);
