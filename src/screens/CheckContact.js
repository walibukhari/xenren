/* eslint-disable object-property-newline */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-else-return */
/* eslint-disable no-lonely-if */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable comma-dangle */
/* eslint-disable camelcase */
/* eslint-disable quotes */
/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable object-curly-newline */
/* eslint-disable no-unused-vars */
/* eslint-disable indent */
/* eslint-disable import/first */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Alert
} from "react-native";
import _ from "lodash";
import Ionicons from "react-native-vector-icons/Ionicons";
import AvatarWithStatus from "../components/AvatarWithStatus";
import Config from "../Config";
import { translate } from "../i18n";
import LocalData from "../components/LocalData";
import HttpRequest from "../components/HttpRequest";
import { Rating } from "react-native-elements";
import { NavigationActions } from "react-navigation";
import FontStyle from "../constants/FontStyle";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

class CheckContact extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title:
        navigation.state.params.sharedOfficeData.office_name ||
        navigation.state.params.sharedOfficeData.office.office_name,
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle: {
        color: Config.topNavigation.headerTextColor,
        alignSelf: "center",
        fontFamily: FontStyle.Regular,
        width: "100%"
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: "row",
            alignSelf: "center",
            padding: 5
          }}
        >
          <SimpleLineIcons
            size={16}
            name="arrow-left"
            color={Config.topNavigation.headerIconColor}
          />
        </TouchableOpacity>
      )
    };
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      usersList: [],
      access_token: "",
      colors: ["#f1c40f", Config.primaryColor, "#ecf0f1"],
      textColor: ["#fff", "#fff", "#000"],
      skillIcons: [
        require("../../images/share_office/icon_crown.png"),
        require("../../images/share_office/icon_check.png"),
        require("../../images/share_office/icon_check.png")
      ],
      officeData: this.props.navigation.state.params.sharedOfficeData,
      images: [],
      path: null,
      office_id: ""
    };
  }

  componentDidMount() {
    LocalData.getUserData().then(response => {
      response = JSON.parse(response);
      this.setState({
        access_token: response.token,
        office_id: this.state.officeData.office.id
      });
      this.getUsersList(response.token, this.state.officeData.office.id);
    });
  }

  getUsersList = (access_token, officeId) => {
    HttpRequest.getSharedOfficeUsersList(access_token, officeId)
      .then(response => {
        const result = response.data;
        if (response.status === 200) {
          this.setState({
            usersList: result.data,
            isLoading: false
          });
        } else {
          // Show Dialog
          alert(response.status);
          // this.showAlert();
        }
      })
      .catch(() => {
        this.setState({ isLoading: false });
        alert("Network Error");
      });
  };

  showAlert() {
    // Show Dialog
    global.setTimeout(() => {
      Alert.alert("Warning", "Token is Expired!", [
        {
          text: "OK",
          onPress: () => {
            this.logout();
          }
        }
      ]);
    }, 200);
  }

  logout() {
    LocalData.setUserData(null);
    this.props.navigation.dispatch(
      NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "Login"
          })
        ]
      })
    );
  }

  renderSkill = skills => {
    return skills.map((item, index) => {
      if (item.skill == null) {
        return null;
      } else {
        return (
          <View
            key={index}
            style={{
              backgroundColor:
                  item.experience > 0
                      ? this.state.colors[0]
                      : item.is_client_verified > 0 && item.is_client_verified !== null
                      ? this.state.colors[1]
                      : this.state.colors[2],
              borderRadius: 15,
              borderColor: Config.white,
              borderWidth: 1,
              flexDirection: "row",
              alignItems: "center",
              marginRight: 5,
              paddingHorizontal: 3,
              paddingVertical: 5
            }}
          >
            {item.experience > 0 ?
            <View
              style={{
                width: 24,
                height: 24,
                borderRadius: 12,
                borderColor: "#fff",
                borderWidth: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Image
                style={{
                  height: 13,
                  width: 13,
                  resizeMode: "contain",
                  tintColor: this.state.colors[2]
                }}
                source={this.state.skillIcons[0]}
              />
            </View>
            : item.is_client_verified !== 0 && item.is_client_verified !== null ?
                <View
                    style={{
                      width: 24,
                      height: 24,
                      borderRadius: 12,
                      borderColor: "#fff",
                      borderWidth: 1,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                >
                  <Image
                      style={{
                        height: 13,
                        width: 13,
                        resizeMode: "contain",
                        tintColor: this.state.colors[2]
                      }}
                      source={this.state.skillIcons[1]}
                  />
                </View>
            : <View /> }
            <Text
              style={{
                paddingHorizontal: 10,
                color:
                    item.experience > 0
                        ? this.state.textColor[2]
                        : item.is_client_verified > 0 && item.is_client_verified !== null
                        ? this.state.textColor[0]
                        : this.state.textColor[2],
                backgroundColor: "transparent",
                fontFamily: FontStyle.Light,
              }}
            >
              {item.skill.name_en}
            </Text>
          </View>
        );
      }
    });
  };

  renderRow = item => {
    return (
      <View
        style={{
          marginHorizontal: 10,
          marginVertical: 10
        }}
      >
        <View style={styles.boxInsideStyle}>
          <View style={styles.topSideStyle}>
            <AvatarWithStatus
              diameter={80}
              indicatorColor={`#D3D1D1`}
              source={{ uri: item.user.img_avatar }}
            />
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                marginLeft: 10
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={1}
                  style={styles.text1Style}
                >
                  {item.user.real_name}
                </Text>
                {/* {this.renderStar(item.star)} */}
                <Rating
                  type="star"
                  startingValue={0}
                  readonly
                  imageSize={15}
                  style={{ alignContent: "flex-start" }}
                />
              </View>
              <Text style={styles.text2Style}>{item.user.title}</Text>
              <Text style={styles.text3Style}>
                {item.user_status == null ? "" : item.user_status.user_status}
              </Text>
            </View>
          </View>
          <View style={styles.middleSideStyle}>
            {this.renderSkill(item.user.skills)}
          </View>
          <View
            style={{
              marginTop: 20,
              flexDirection: "row",
              paddingVertical: 10,
              backgroundColor: "#f7f7f7"
            }}
          >
            <TouchableOpacity
              style={styles.cardButtonStyle}
              onPress={() => {
                this.goToPortfolio(item);
              }}
            >
              <Ionicons name="ios-folder-open" color="#adadad" size={15} />
              <Text style={styles.cardButtonTextStyle}>
                {translate("portfolio").toUpperCase()}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                width: 1,
                backgroundColor: "#cdcdcd"
              }}
            />
            <TouchableOpacity
              style={styles.cardButtonStyle}
              onPress={() => {
                this.onCheckTapped(item);
              }}
            >
              <Ionicons name="ios-mail" color="#adadad" size={15} />
              <Text style={styles.cardButtonTextStyle}>
                {translate("check")}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  onCheckTapped = item => {
    this.props.navigation.navigate("ChatSingle", { userInfo: item.user });
  };

  goToPortfolio = item => {
    this.props.navigation.navigate("OtherUserProfile", {
      user_id: item.user.id,
      type: "Portfolio",
      jobPositions: []
    });
  };

  onRefresh() {
    this.setState({ isLoading: true });
    this.getUsersList(this.state.access_token, this.state.office_id);
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <FlatList
          data={this.state.usersList}
          onRefresh={() => this.onRefresh()}
          refreshing={this.state.isLoading}
          renderItem={({ item, index }) => this.renderRow(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    component: state.component
  };
}

const styles = {
  rootStyle: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: "#ecf0f1"
  },

  boxInsideStyle: {
    flexDirection: "column",
    backgroundColor: "#fff"
  },

  topSideStyle: {
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingTop: 10
  },

  middleSideStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 10
  },

  avatarStyle: {
    width: 50,
    height: 50,
    borderRadius: 25
  },

  cardButtonStyle: {
    flex: 1,
    height: 30,
    paddingVertical: 10,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },

  cardButtonTextStyle: {
    color: Config.textSecondaryColor,
    fontSize: 12,
    paddingLeft: 5
  },

  text1Style: {
    flex: 1,
    fontSize: 16,
    color: "#404040"
  },

  text2Style: {
    fontSize: 13,
    color: "#b7b7b7"
  },

  text3Style: {
    marginVertical: 5,
    fontSize: 15,
    fontWeight: "300",
    color: Config.primaryColor
  },

  starStyle: {
    flexDirection: "row"
  }
};

export default connect(mapStateToProps)(CheckContact);
