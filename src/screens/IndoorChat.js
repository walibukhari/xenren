/* eslint-disable no-console */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-mixed-operators */
/* eslint-disable no-return-assign */
/* eslint-disable no-else-return */
/* eslint-disable max-len */
/* eslint-disable consistent-return */
/* eslint-disable prefer-const */
/* eslint-disable react/no-string-refs */
/* eslint-disable global-require */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    Text,
    View,
    ActivityIndicator,
    TouchableOpacity,
    Alert,
    Image,
    TextInput,
    Modal,
    FlatList,
    Dimensions,
    Platform,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import axios from 'axios';
import Config from '../Config';
import {translate} from '../i18n';
import GlowEffectComponent from '../screens/IndoorChat/GlowEffectComponent';
import ProfileGlowModal from '../screens/IndoorChat/ProfileGlowModal';

const {width, height} = Dimensions.get('window');

class IndoorChat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showTermsModal: false,
            showInfoVerifyModal: false,
            checkProfileModal: true,
            userId: null,
            chatData: [],
            isLoading: true,
            access_token: '',
            msg: '',
            userInfo: this.props.userData,
            officeID: this.props.navigation.state.params.sharedOfficeData.office.office_id,
        };
        ini = this;
    }

    componentDidMount() {
        if (this.state.userInfo.is_validated === null ||
            this.state.userInfo.is_validated === 0 ||
            this.state.userInfo.is_validated === 2) {
            this.setState({showInfoVerifyModal: true});
            setTimeout(() => {
                this.setState({
                    showInfoVerifyModal: false,
                }, () => {
                    this.props.navigation.goBack();
                });
            }, 3000);
        }
        this.showCheckProfileModal();
        this.setState({
            access_token: this.state.userInfo.token,
            userId: this.state.userInfo.id.toString(),
        });
        this.getChat(this.state.userInfo.token, this.state.officeID);
        // this.onSubscribeToTopic()
    }

    getChat = (access_token, officeID) => fetch(`${Config.baseUrlV1}/sharedOffice/room/chat/${officeID}`, {
        method: 'GET',
        headers: {
            Authorization: 'Bearer '.concat(access_token),
            'Content-Type': 'application/json',
        },
    })
        .then(response => response.json())
        .then((responseJson) => {
            if (responseJson.status === 'success') {
                this.setState({
                    chatData: responseJson.data,
                    isLoading: false,
                });
            } else {
                this.setState({isLoading: false});
                Alert.alert("Error", translate('network_error'), [
                    {
                        text: "OK",
                    }
                ]);
            }
        })
        .catch((error) => {
            this.setState({isLoading: false});
            alert(error);
        });

    sendMsgAPI = (access_token) => {
        if (this.state.msg !== '') {
            const myMsg = this.state.msg;
            axios({
                method: 'post',
                url: `${Config.baseUrlV1}/sharedOffice/room/chat`,
                data: {
                    office_id: this.state.officeID,
                    message: myMsg,
                },
                headers: {
                    Authorization: 'Bearer '.concat(access_token),
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            }).then((res) => {
                console.log(res.data);
            }).catch((err) => {
                console.log(err.response);
            });
        }
    };

    static navigationOptions = ({navigation}) => {
        const data = navigation.state.params.sharedOfficeData;

        const shared_office = data.office;
        const officeName = shared_office.office_name;
        const total_members = shared_office && shared_office.total_members_in_office && shared_office.total_members_in_office.length ? shared_office.total_members_in_office.length : 0;
        const container_width = 160;
        return {
            headerStyle: {
                backgroundColor: '#FFF'
            },
            headerTitle: (
                (Platform.OS == 'android') ?
                    <GlowEffectComponent
                        wrapperStyle={{
                            width: container_width,
                            bottom: Platform.OS === 'ios' ? 4 : 0,
                            marginLeft: Platform.OS === 'ios' ? 0 : container_width / 5
                        }}
                        onPress={() => {
                            ini.changeChatRoom();
                        }}>
                        <Image source={require('../../images/other/icon_location.png')} style={{
                            tintColor: Config.primaryColor,
                            width: 22,
                            height: 22,
                        }}/>
                        <Text
                            style={{
                                alignSelf: 'center',
                                paddingHorizontal: 2,
                                paddingLeft: 10,
                            }}
                            numberOfLines={1}
                            ellipsizeMode='tail'>{officeName}</Text>
                    </GlowEffectComponent>
                    : <TouchableOpacity style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'row',
                        paddingHorizontal: 10,
                        paddingVertical: 6,
                        borderRadius: 50,
                        borderWidth: 2,
                        borderColor: Config.primaryColor,
                        elevation: 1,
                        shadowColor: Config.textColor,
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        shadowOffset: {
                            height: 1,
                            width: 1,
                        },
                        bottom: Platform.OS === 'ios' ? 4 : 0,
                        marginLeft: Platform.OS === 'ios' ? 0 : width / 3
                    }} onPress={() => {
                        ini.changeChatRoom();
                    }}>
                        <Image source={require('../../images/other/icon_location.png')} style={{
                            tintColor: Config.primaryColor,
                            width: 20,
                            height: 20,
                        }}/>
                        <Text style={{
                            fontSize: 12,
                            color: '#000',
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingLeft: 10,
                        }} numberOfLines={1} ellipsizeMode='tail'>
                            {officeName}
                        </Text>
                    </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity
                    onPress={() => {
                        navigation.goBack();
                    }}
                    style={{
                        marginLeft: 10,
                        flexDirection: 'row',
                        alignSelf: 'center',
                        padding: 5,
                    }}
                >
                    <FontAwesomeIcons
                        size={16}
                        name="chevron-left"
                        color={'#000'}
                    />
                </TouchableOpacity>
            ),
            headerTitleStyle: {textAlign: 'center', alignSelf: 'center'},
            headerRight: (<TouchableOpacity onPress={() => {
                ini.handleManageContact();
            }}>
                <Image source={require('../../images/share_office/TotalUsers_icon.png')}
                       style={{marginTop: 6, marginRight: 20}}/>
                <View style={{
                    width: 14,
                    height: 14,
                    borderRadius: 10,
                    backgroundColor: Config.primaryColor,
                    position: 'absolute',
                    right: 13,
                    top: 0,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}>
                    <Text style={{
                        color: 'white',
                        fontSize: 7,
                        textAlign: 'center'
                    }}>{total_members}</Text>
                </View>
            </TouchableOpacity>),
        };
    };

    //
    changeChatRoom() {
        ini.props.navigation.navigate('JoinedChatRooms', {sharedOfficeData: this.props.navigation.state.params.sharedOfficeData});
    }

    handleManageContact() {
        this.props.navigation.navigate('CheckContact', {sharedOfficeData: this.props.navigation.state.params.sharedOfficeData});
    }

    sendMessage = () => {
        if (this.state.msg) {
            let newData = {
                sender_id: this.state.userId,
                message: this.state.msg,
            };
            this.setState(prevState => ({
                chatData: [...prevState.chatData, newData],
                msg: '',
            }));
            this.sendMsgAPI(this.state.access_token);
        }
    };

    copyToClipboard() {
        this.setState({showTermsModal: false});
        Alert.alert("Information", translate('copy_to_clipboard'), [
            {
                text: "OK",
            }
        ]);
    }

    showCheckProfileModal = () => {
        setTimeout(() => {
            this.setState({
                checkProfileModal: false,
            });
        }, 1300);
    };
    onCheckUserProfileTapped = () => {
        this.props.navigation.navigate('UserProfile');
    };

    renderRow(item) {
        if (this.state.chatData) {
            if (item.item.sender_id === this.state.userId) {
                return (
                    <View style={styles.rowRightStyle}>
                        <View style={[styles.infoStyle, {alignItems: 'flex-end'}]}>
                            <View style={styles.bubbleRightStyle}>
                                <Text style={{color: '#fff'}}>{item.item.message}</Text>
                            </View>
                            <Text style={styles.chatSentTime}>12:34 pm</Text>
                        </View>
                        {/*<Image source={{uri: this.state.userInfo.img_avatar}} style={styles.avatarLeftStyle}/>*/}
                        {/*<Image source={require('./../../images/private_chat/corner_1.png')}*/}
                        {/*       style={{*/}
                        {/*           height: 10, width: 10, tintColor: '#fff', marginRight: -2, marginTop: 5,*/}
                        {/*       }}/>*/}
                    </View>
                );
            } else {
                return (
                    <View style={styles.rowLeftStyle}>
                        {/*<TouchableOpacity>*/}
                        {/*    <Image source={{uri: this.state.userInfo.img_avatar}} style={styles.avatarRightStyle}/>*/}
                        {/*</TouchableOpacity>*/}
                        <View style={styles.infoStyle}>
                            <View style={styles.bubbleLeftStyle}>
                                <Text>{item.item.message}</Text>
                            </View>
                            <Text style={styles.chatSentTime}>12:34 pm</Text>
                        </View>
                    </View>
                );
            }
        }
    }

    render() {
        return (
            <View style={styles.rootStyle}>

                {this.state.isLoading &&
                <View style={{position: 'absolute', top: height * 0.5 - 80, left: width * 0.5 - 10}}>
                    <ActivityIndicator/>
                </View>
                }
                <FlatList
                    ref={ref => this.flatList = ref}
                    onContentSizeChange={() => this.flatList.scrollToEnd({animated: true})}
                    onLayout={() => this.flatList.scrollToEnd({animated: true})}
                    showsVerticalScrollIndicator={false}
                    data={this.state.chatData}
                    renderItem={item => this.renderRow(item)}
                    keyExtractor={(item, index) => index.toString()}
                />

                <View style={styles.inputTextWrapperStyle}>
                    <View style={styles.inputBoxStyle}>

                        <TextInput style={styles.inputTextStyle}
                                   placeholder={translate('type_your_message')}
                                   autoCapitalize='none'
                                   autoCorrect={false}
                                   multiline={true}
                                   underlineColorAndroid='transparent'
                                   onChangeText={msg => {
                                       this.setState({msg})
                                   }}
                                   value={this.state.msg}
                        />

                    </View>

                    <TouchableOpacity style={{
                        paddingLeft: 20,
                        position: 'absolute',
                        right: 8,
                        bottom: 8
                    }} onPress={() => {
                        this.sendMessage();
                    }}>
                        <View style={styles.sendChatButton}>
                            <FontAwesomeIcons name={'paper-plane'} size={16} color={'#FFF'} solid/>
                        </View>
                    </TouchableOpacity>

                </View>

                {/* For accept terms or leave */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showTermsModal}
                    onRequestClose={() => console.log('Modal Closed')}>
                    <View style={styles.dialogStyle}>
                        <View style={styles.dialogBoxStyle}>
                            <View style={{height: 20}}/>

                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                height: 50,
                                paddingHorizontal: 25,
                                marginTop: 10
                            }}>
                                <Image source={require('../../images/other/icon_phone_laptop.png')}
                                       style={{
                                           height: 40, width: 40, tintColor: Config.primaryColor, marginRight: 10,
                                       }} resizeMode='contain'/>
                                <Text style={{flex: 1, fontSize: 10}}>{translate('platform_discussion_issue')}</Text>
                            </View>

                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                height: 50,
                                paddingHorizontal: 25,
                                marginTop: 10
                            }}>
                                <Image source={require('../../images/other/icon_announce.png')}
                                       style={{
                                           height: 40, width: 40, tintColor: Config.primaryColor, marginRight: 10,
                                       }} resizeMode='contain'/>
                                <Text style={{flex: 1, fontSize: 10}}>{translate('advertise_job')}</Text>
                            </View>
                            <View style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingVertical: 20,
                                paddingHorizontal: 25,
                            }}>
                                <Text style={{
                                    fontSize: 10, fontWeight: 'bold', color: Config.primaryColor, textAlign: 'center',
                                }}>
                                    {`"${translate('rules_voilation')}"`}
                                </Text>
                            </View>

                            <View style={{flexDirection: 'row', height: 40}}>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomLeftRadius: 10,
                                    backgroundColor: '#f3f3f3',
                                }} onPress={() => {
                                    this.setState({showTermsModal: false});
                                }}>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>{translate('CANCEL')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomRightRadius: 10,
                                    backgroundColor: Config.primaryColor,
                                }} onPress={() => {
                                    this.copyToClipboard();
                                }}>
                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>{translate('AGREE')}</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                </Modal>

                {/* For Verify Info */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.showInfoVerifyModal}
                    onRequestClose={() => console.log('Modal Closed')}>
                    <View style={styles.verifyDialogStyle}>
                        <View style={styles.verifyInfoBoxStyle}>
                            <Image style={{
                                width: 60, height: 110, marginTop: 15, resizeMode: 'contain',
                            }} source={require('../../images/new_clipboard.png')}/>
                            <Text style={{
                                fontSize: 14,
                                fontWeight: '100',
                                fontFamily: 'Montserrat-Light',
                                marginLeft: 15,
                                marginRight: 15,
                                marginBottom: 15,
                                color: 'black',
                                textAlign: 'center',
                            }}>{translate('before_access_coWorker')},</Text>
                            <TouchableOpacity style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderBottomRightRadius: 10,
                                marginBottom: 15,
                                marginLeft: 5,
                                marginRight: 5,
                            }}>
                                <Text style={{
                                    color: Config.primaryColor, fontSize: 13, fontWeight: '300', textAlign: 'center',
                                }}>{translate('make_sure')}</Text>
                            </TouchableOpacity>

                            <View style={{flexDirection: 'row', height: 40}}>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomLeftRadius: 10,
                                    backgroundColor: '#f3f3f3',
                                }} onPress={() => {
                                    this.setState({showTermsModal: false});
                                }}>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>{'NO'}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomRightRadius: 10,
                                    backgroundColor: Config.primaryColor,
                                }}
                                    // onPress={this.verifyInfo}
                                >
                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>{'YES'}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                {/* New Message Notification */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={false}
                    onRequestClose={() => console.log('Modal Closed')}>
                    <View style={styles.verifyDialogStyle}>
                        <View style={styles.verifyInfoBoxStyle}>
                            <Image style={{
                                width: 60, height: 80, marginTop: 15, resizeMode: 'contain',
                            }} source={require('../../images/share_space.png')}/>
                            <Text style={{
                                fontSize: 14,
                                fontWeight: '100',
                                fontFamily: 'Montserrat-Light',
                                marginLeft: 25,
                                marginRight: 25,
                                marginBottom: 15,
                                color: 'black',
                                textAlign: 'center',
                            }}>
                                You havent use share office once.
                                Use you will chat with this share office comunity!
                            </Text>

                            <View style={{flexDirection: 'row', height: 40}}>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomLeftRadius: 10,
                                    backgroundColor: '#f3f3f3',
                                }} onPress={() => {
                                    this.setState({showTermsModal: false});
                                }}>
                                    <Text style={{
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>{translate('CANCEL')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderBottomRightRadius: 10,
                                    backgroundColor: Config.primaryColor,
                                }}>
                                    <Text style={{
                                        color: '#fff',
                                        fontSize: 11,
                                        fontWeight: 'bold'
                                    }}>OKAY</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                {/* Check Profile Modal */}

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.checkProfileModal}
                    onRequestClose={() => console.log('Modal Closed')}>
                    <ProfileGlowModal
                        userInfo={this.state.userInfo}
                        onTap={this.onCheckUserProfileTapped}/>
                </Modal>

            </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: '#f3f3f3',
        flex: 1,
        justifyContent: 'center',
    },
    headerStyle: {
        flexDirection: 'row',
        height: 70,
        backgroundColor: '#fff',
    },
    socialButtonStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    socialIconStyle: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        marginBottom: 5,
    },
    rowLeftStyle: {
        flexDirection: 'row',
        marginVertical: 5,
        marginRight: 5,
    },
    rowRightStyle: {
        flexDirection: 'row',
        marginVertical: 5,
        marginLeft: 5,
    },
    avatarLeftStyle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        marginHorizontal: 0,
    },
    avatarRightStyle: {
        height: 50,
        width: 50,
        borderRadius: 25,
        marginHorizontal: 5,
    },
    infoStyle: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginTop: 5,
    },
    bubbleLeftStyle: {
        backgroundColor: '#fff',
        borderRadius: 20,
        padding: 15,
        borderTopLeftRadius: 0,
        marginLeft: 15,
        marginRight: '15%'
    },
    bubbleRightStyle: {
        backgroundColor: Config.primaryColor,
        borderRadius: 20,
        paddingVertical: 15,
        paddingHorizontal: 15,
        borderBottomRightRadius: 0,
        marginRight: 15,
        marginLeft: '15%'
    },
    chatSentTime: {
        color: '#a2a2a2',
        fontSize: 10,
        marginHorizontal: 15,
        marginVertical: 8
    },
    sendChatButton: {
        backgroundColor: Config.primaryColor,
        height: 35,
        width: 35,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputTextWrapperStyle: {
        backgroundColor: '#fff',
        paddingVertical: 12,
        paddingHorizontal: 5,
        paddingLeft: 15,
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        marginBottom: 20,
        borderRadius: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.2,
        shadowRadius: 3,
        elevation: 9,
    },
    inputBoxStyle: {
        flex: 1,
        backgroundColor: 'transparent',
        padding: 5,
        paddingRight: 40
    },
    inputTextStyle: {
        fontSize: 14,
        paddingTop: 0,
    },
    dialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    verifyDialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialogBoxStyle: {
        width: '70%',
        height: 'auto',
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    verifyInfoBoxStyle: {
        width: '70%',
        height: 'auto',
        borderRadius: 10,
        backgroundColor: '#fff',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
};

function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(IndoorChat);
