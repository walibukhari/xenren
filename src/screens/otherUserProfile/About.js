/* eslint-disable spaced-comment */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-dupe-keys */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable semi */
/* eslint-disable object-curly-newline */
/* eslint-disable comma-dangle */
/* eslint-disable global-require */
/* eslint-disable no-mixed-operators */
/* eslint-disable no-param-reassign */
/* eslint-disable quote-props */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
/* eslint-disable class-methods-use-this */
/* eslint-disable quotes */
/* eslint-disable prefer-template */
/* eslint-disable vars-on-top */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */
/* eslint-disable no-var */
/* eslint-disable no-else-return */
/* eslint-disable react/jsx-key */
/* eslint-disable indent */
/* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable no-tabs */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
/* eslint-disable no-unused-vars */
import React, {Component} from "react";
import {connect} from "react-redux";
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    Image,
    ScrollView,
    Modal,
    ActivityIndicator,
    Picker, Platform, ActionSheetIOS, Alert
} from "react-native";
import Config from "../../Config";
import FontAwesome, {Icons} from "react-native-fontawesome";
import Ionicons from "react-native-vector-icons/Ionicons";
import HttpRequest from "../../components/HttpRequest";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Input from "../profile/Input";
import {Rating} from "react-native-elements";
import FontStyle from "../../constants/FontStyle";
import {translate} from '../../i18n';

const {width, height} = Dimensions.get("window");

class AboutOtherUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            activeTabIndex: 0,
            isContact: false,
            access_token: "",
            userDetails: {},
            isUpdateContact: false,
            qq_id: "",
            wechat_id: "",
            skype_id: "",
            handphone_no: "",
            isError: false,
            errorMsg: "",
            user_status: "",
            skills: [],
            profilePic: "",
            userStatus: "",
            userRatingsBar: [],
            userOpenProjects: [],
            isSendInvitation: false,
            isSendOffer: false,
            selectedSendOfferProject: false,
            currency: "",
            selectedProjectForInviteId: 0,
            selectedProjectForInvite: "Select Project",
            jobType: 1,
            dailyUpdate: 1,
            selectCurrencyLabel: 'Select Currency',
            selectProjectLabel: translate('select_a_project'),
            jobPositions: this.props.navigation.state.params.jobPositions
        };
    }

    componentDidMount() {
        this.getUsersInfo();
        this.getUserOpenProjects();
    }

    renderSkill = () => {
        return this.props.skills.map((skill, index) => {
            return (
                <View
                    key={index}
                    style={{
                        borderRadius: 15,
                        borderColor: "#f1f1f1",
                        borderWidth: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        marginRight: 5,
                        padding: 3
                    }}
                >
                    <Text
                        style={{
                            paddingHorizontal: 10,
                            backgroundColor: "transparent",
                            color: "#bababa"
                        }}
                    >
                        {skill.name_en}
                    </Text>
                </View>
            );
        });
    };

    renderLanguage = languages => {
        if (languages.length > 0) {
            return languages.map((item, index) => {
                return (
                    <View
                        key={index}
                        style={{
                            flexDirection: "row",
                            marginRight: 5,
                            alignItems: "center"
                        }}
                    >
                        {item.country_flag !== "" && (
                            <Image
                                source={{uri: item.country_flag}}
                                style={{
                                    width: 35,
                                    height: 30,
                                    resizeMode: "contain",
                                    marginRight: 5
                                }}
                            />
                        )}

                        <Text
                            style={{
                                backgroundColor: "transparent",
                                color: "#bababa",
                                fontSize: 17,
                                marginRight: 5,
                                fontFamily: FontStyle.Regular
                            }}
                        >
                            {item.language_detail !== null ? item.language_detail.name : ""}
                        </Text>
                    </View>
                );
            });
        } else {
        }
    };

    updateContact() {
        var qq_id = this.state.qq_id;
        var wechat_id = this.state.wechat_id;
        var skype_id = this.state.skype_id;
        var handphone_no = this.state.handphone_no;
        if (qq_id === "") {
            this.setState({
                isError: true,
                errorMsg: translate('please_enter_qq')
            });
        } else if (wechat_id === "") {
            this.setState({
                isError: true,
                errorMsg: translate('please_enter_wechat')
            });
        } else if (skype_id === "") {
            this.setState({
                isError: true,
                errorMsg: translate('please_enter_skype')
            });
        } else if (handphone_no === "") {
            this.setState({
                isError: true,
                errorMsg: translate('please_enter_phone')
            });
        } else {
            this.setState({
                isError: false,
                errorMsg: ""
            });
            var access_token = this.state.access_token;
            access_token = "bearer " + access_token;
            this.updateContactInfo(
                qq_id,
                wechat_id,
                skype_id,
                handphone_no,
                access_token
            );
        }
    }

    updateContactInfo(qq_id, wechat_id, skype_id, handphone_no, access_token) {
        HttpRequest.updateContactInfo(
            qq_id,
            wechat_id,
            skype_id,
            handphone_no,
            access_token
        ).then((response) => {
            const result = response.data;
            if (result.status === "success") {
                // this.getUsersInfo(access_token)
            } else {
                
            }
        })
            .catch((error) => {
                alert(error);
            });
    }

    getUsersInfo = () => {
        const userID = this.props.userId;
        HttpRequest.getUserProfileByID(this.props.userData.token, userID)
            .then((response) => {
                const result = response.data;
                console.log(result);
                console.log("Abhi");
                
                this.setState({
                    userDetails: result.data,
                    isLoading: false,
                    profilePic: result.data.img_avatar
                });
                if (result.status === "success") {
                    this.ratingData(result.data.user_ratings.totalCount5Star, 5);
                    this.ratingData(result.data.user_ratings.totalCount4Star, 4);
                    this.ratingData(result.data.user_ratings.totalCount3Star, 3);
                    this.ratingData(result.data.user_ratings.totalCount2Star, 2);
                    this.ratingData(result.data.user_ratings.totalCount1Star, 1);

                    this.setState({
                        qq_id: result.data.qq_id,
                        wechat_id: result.data.wechat_id,
                        skype_id: result.data.skype_id,
                        handphone_no: result.data.handphone_no,
                        profilePic: result.data.img_avatar
                    });
                } else if (result.status === "failure") {
                    alert(result.error);
                }
            })
            .catch((error) => {
                alert(error);
            });
    };

    getUserOpenProjects = () => {
        HttpRequest.getUserOpenProjects(this.props.userData.token)
            .then((response) => {
                const result = response.data;
                console.log(result);
                console.log('user open projects');
                if (result.status === "success") {
                    this.setState({
                        userOpenProjects: result.data
                    });
                    console.log(this.state.userOpenProjects);
                } else if (result.status === "failure") {
                    //alert(result.error);
                    console.log(result.error);
                } 
            })
            .catch((error) => {
                alert(error);
            });
    };

    sendOfferAPI = () => {
        // alert(this.state.selectedSendOfferProject);
        const body = {
            projectId: this.state.selectedSendOfferProject,
            receiverId: this.state.userDetails.id,
            pay_type: this.state.jobType,
            daily_update: this.state.dailyUpdate,
            amount: this.state.amount,
            senderMessage: this.state.msg
        };
        HttpRequest.sendOffer(this.props.userData.token, body)
            .then((response) => {
                if (response.data.status === "success") {
                    this.setState({
                        isSendOffer: false,
                        job: "",
                        amount: "",
                        msg: ""
                    });
                    Alert.alert("Success", response.data.message, [
                        {
                            text: "OK",
                        }
                    ]);
                } else if (response.data.status === "failure") {
                    alert(translate('network_error'));
                } else {
                    alert(translate('network_error'));
                }
            })
            .catch((error) => {
                alert(error);
            });
    };

    inviteFreelancerAPI = () => {
        const body = {
            projectId: this.state.selectedProjectForInviteId,
            receiverId: this.state.userDetails.id,
            senderMessage: this.state.msg
        };

        HttpRequest.inviteFreelancer(this.props.userData.token, body)
            .then((response) => {
                console.log(response);
                if (response.data.status === "success" || "Error") {
                    this.setState({
                        isSendInvitation: false,
                        msg: "",
                        job: ""
                    });
                    alert(response.data.message);
                } else if (response.data.status === "failure") {
                    alert(translate('network_error'));
                } else {
                    alert(translate('network_error'));
                }
            })
            .catch((error) => {
                console.log(error);
                alert(error);
            });
    };

    // Calculates Age of the User
    _calculateAge(birthday) {
        birthday = new Date(birthday);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    calcuateBarHeight = (total, value) => {
        return (value / 100) * total;
    };

    // Creates a Bar Object for the Ratings
    ratingData = (count, stars) => {
        const obj = {
            reviewCount: count,
            star: stars
        };
        this.setState({userRatingsBar: [...this.state.userRatingsBar, obj]});
    };

    onContactTapped = info => {
        this.props.navigation.navigate("ChatSingle", {userInfo: info});
    };

    onQRTapped = info => {
        this.props.navigation.navigate("QRDetail", {userInfo: info});
    };

    renderBars = data => {
        if (this.state.userDetails.user_ratings.totalCountReview !== 0) {
            if (data.length > 0) {
                return data.map((item, index) => {
                    const barHeight = this.calcuateBarHeight(90, item.reviewCount);
                    const heightPercent = `${barHeight}%`;
                    return (
                        <View
                            key={index}
                            style={{
                                justifyContent: "flex-end",
                                alignItems: "center"
                            }}
                        >
                            <View
                                style={{
                                    width: 22,
                                    height: heightPercent,
                                    backgroundColor: Config.primaryColor,
                                    marginRight: 5
                                }}
                            />
                            <Image
                                style={{
                                    marginTop: 5,
                                    resizeMode: "contain",
                                    width: 15,
                                    height: 15
                                }}
                                source={require("../../../images/profile/favorite.png")}
                            />
                            <Text
                                style={{
                                    fontSize: 14,
                                    fontFamily: FontStyle.Regular,
                                    color: "#575757",
                                    paddingTop: 5,
                                    textAlign: "center"
                                }}
                            >
                                {item.star}
                            </Text>
                        </View>
                    );
                });
            }
        } else {
        }
    };

    renderModal() {
        return (
            <View
                style={{
                    backgroundColor: "rgba(0,0,0,0.5)",
                    flex: 1,
                    flexDirection: "column",
                    paddingTop: "65%",
                    paddingHorizontal: 10
                }}
            >
                <View
                    style={{
                        borderRadius: 4,
                        borderColor: "#fff",
                        borderWidth: 1,
                        backgroundColor: "#fff",
                        height: 220,
                        paddingHorizontal: 10,
                        paddingVertical: 10
                    }}
                >
                    <View style={{flexDirection: "row"}}>
                        <View style={{flex: 0.2}}>
                            <TouchableOpacity
                                style={{
                                    marginRight: 3,
                                    flexDirection: "row"
                                }}
                                onPress={() =>
                                    this.setState({
                                        isContact: false,
                                        isUpdateContact: true
                                    })
                                }
                            >
                                <SimpleLineIcons size={16} name="note" color="#57af2a"/>
                            </TouchableOpacity>
                        </View>
                        <Text
                            style={{
                                flex: 0.6,
                                textAlign: "center",
                                color: "gray",
                                fontSize: 18,
                                fontFamily: FontStyle.Regular
                            }}
                        >
                            {translate('contact_info')}
                        </Text>
                        <TouchableOpacity
                            onPress={() => this.setState({isContact: false})}
                        >
                            <Text
                                style={{
                                    color: Config.primaryColor,
                                    flex: 0.2,
                                    textAlign: "right",
                                    paddingRight: 3,
                                    fontSize: 22,
                                    fontFamily: FontStyle.Regular
                                }}
                            >
                                X
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            flex: 1,
                            paddingVertical: 20
                        }}
                    >
                        <View
                            style={{
                                flexDirection: "column",
                                flex: 0.5
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center"
                                }}
                            >
                                <Image
                                    source={require("../../../images/account_profile/Tencent_QQ.png")}
                                    style={{
                                        height: 20,
                                        width: 20
                                    }}
                                />
                                <Text style={{textAlign: "center"}}> {translate('qq')} </Text>
                            </View>
                            <Text
                                style={{
                                    color: "gray",
                                    paddingVertical: 10
                                }}
                            >
                                {this.state.userDetails.qq_id}
                            </Text>
                        </View>
                        <View
                            style={{
                                flexDirection: "column",
                                flex: 0.5
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center"
                                }}
                            >
                                <Image
                                    source={require("../../../images/account_profile/wechat-logo.png")}
                                    style={{
                                        height: 20,
                                        width: 20
                                    }}
                                />
                                <Text style={{textAlign: "center"}}> {translate('wechat')} </Text>
                            </View>
                            <Text
                                style={{
                                    color: "gray",
                                    paddingVertical: 10
                                }}
                            >
                                {this.state.userDetails.wechat_id}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            flex: 1,
                            paddingVertical: 20
                        }}
                    >
                        <View
                            style={{
                                flexDirection: "column",
                                flex: 0.5
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center"
                                }}
                            >
                                <Image
                                    source={require("../../../images/account_profile/skype-icon-5.png")}
                                    style={{
                                        height: 20,
                                        width: 20
                                    }}
                                />
                                <Text style={{textAlign: "center"}}> {translate('skype')}</Text>
                            </View>
                            <Text
                                style={{
                                    color: "gray",
                                    paddingVertical: 10
                                }}
                            >
                                {this.state.userDetails.skype_id}
                            </Text>
                        </View>
                        <View
                            style={{
                                flexDirection: "column",
                                flex: 0.5
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center"
                                }}
                            >
                                <Image
                                    source={require("../../../images/account_profile/MetroUI_Phone.png")}
                                    style={{
                                        height: 20,
                                        width: 20
                                    }}
                                />
                                <Text style={{alignContent: "center"}}> {translate('phone')} </Text>
                            </View>
                            <Text
                                style={{
                                    color: "gray",
                                    paddingVertical: 10
                                }}
                            >
                                {this.state.userDetails.handphone_no}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    onSendInvitation = value => {
        this.setState({
            isSendInvitation: true,
            sendType: value
        });
    };

    onSendOffer = value => {
        this.setState({
            isSendOffer: true,
            sendType: value
        });
    };

    onSelectCurrency() {
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: [
                    translate('usd'),
                    translate('yuan')
                ],
            },
            (buttonIndex) => {
                // if (buttonIndex === 1) {
                console.log(buttonIndex);
                if (buttonIndex == 0) {
                    this.setState({
                        selectCurrencyLabel: "USD"
                    });
                } else {
                    this.setState({
                        selectCurrencyLabel: "yuan"
                    });
                }
                /* destructive action */
                // }
            });
    }


    onSelectProject() {
        console.log(JSON.stringify(this.state.jobPositions));
        let projects = this.state.jobPositions.map((projects, i) => {
            return projects.name_en;
        });

        if (projects.length < 1) {
            alert('No Project Found...!');

            return null;
        }
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: projects,
            },
            (buttonIndex) => {

                let selectedproject = '';
                this.state.jobPositions.map((projects, i) => {
                    if (i == buttonIndex) {
                        selectedproject = projects;
                    }
                });

                console.log('selectedproject');
                console.log(selectedproject);
                this.setState({
                    selectedProjectForInvite: selectedproject.name_en,
                    selectedProjectForInviteId: selectedproject.id
                });
                /* destructive action */
                // }
            });
    }

    onSelectSendOfferProject() {
        let projects = this.state.userOpenProjects.map((projects, i) => {
            return projects.name;
        });
        console.log('projects');
        console.log(projects);
        if (projects.length < 1) {
            alert('No Project Found...!');

            return null;
        }
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: projects,
            },
            (buttonIndex) => {

                let selectedproject = ''
                this.state.userOpenProjects.map((projects, i) => {
                    if (i == buttonIndex) {
                        selectedproject = projects;
                    }
                });

                console.log('selectedproject');
                console.log(selectedproject);
                this.setState({
                    selectProjectLabel: selectedproject.name,
                    selectedSendOfferProject: selectedproject.id
                });
                /* destructive action */
                // }
            });
    }

    renderInvitationView = () => {
        return (
            <View
                style={{
                    width: "100%",
                    margin: 5,
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 15
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        justifyContent: "space-between",
                        alignItems: "center"
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.onContactTapped(this.state.userDetails);
                        }}
                        style={{
                            width: " 49%",
                            height: 45,
                            borderRadius: 3,
                            backgroundColor: Config.primaryColor,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Text
                            style={{
                                color: "white",
                                fontSize: 15,
                                fontFamily: FontStyle.Regular
                            }}
                        >
                            {translate('contact_info')}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.onSendOffer(2)}
                        style={{
                            width: " 49%",
                            height: 45,
                            borderRadius: 3,
                            backgroundColor: Config.primaryColor,
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <Text
                            style={{
                                color: "white",
                                fontSize: 15,
                                fontFamily: FontStyle.Regular
                            }}
                        >
                            {translate('send_offers')}
                        </Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity
                    onPress={() => this.onSendInvitation(3)}
                    style={{
                        width: " 100%",
                        height: 45,
                        borderRadius: 3,
                        borderColor: Config.primaryColor,
                        borderWidth: 1,
                        marginTop: 5,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <Text
                        style={{
                            color: Config.primaryColor,
                            fontSize: 16,
                            fontFamily: FontStyle.Regular
                        }}
                    >
                        {translate('invite_to_work')}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    renderInviteToWork() {
        return (
            <View alignItems="center">
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        height: 40,
                        justifyContent: "flex-start",
                        alignItems: "center",
                        borderBottomColor: "lightgrey",
                        borderBottomWidth: 1,
                        paddingLeft: 10,
                        paddingRight: 10
                    }}
                >
                    <Text
                        style={{
                            width: "92%",
                            fontSize: 16,
                            fontFamily: FontStyle.Regular,
                            color: "black",
                            paddingLeft: 10,
                            textAlign: "center"
                        }}
                    >
                        {translate('invite_to_work')}
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({isSendInvitation: false});
                        }}
                    >
                        <Image
                            style={{
                                width: 20,
                                height: 20
                            }}
                            resizeMode="contain"
                            source={require("../../../images/facebook/cross.png")}
                        />
                    </TouchableOpacity>
                </View>

                <ScrollView
                    style={{width: "100%"}}
                    contentContainerStyle={{
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            width: "90%",
                            justifyContent: "flex-start",
                            marginVertical: 20
                        }}
                    >
                        <Image
                            source={{uri: this.state.profilePic}}
                            style={{
                                width: 80,
                                height: 80,
                                borderRadius: 40,
                                borderWidth: 1,
                                borderColor: "lightgrey"
                            }}
                        />
                        <View
                            style={{
                                marginLeft: 10,
                                justifyContent: "center"
                            }}
                        >
                            <Text
                                style={{
                                    color: "black",
                                    fontSize: 15,
                                    fontFamily: FontStyle.Regular
                                }}
                            >
                                {this.state.userDetails.real_name}
                            </Text>
                            <Text
                                style={{
                                    fontSize: 12,
                                    fontFamily: FontStyle.Regular,
                                    paddingTop: 3
                                }}
                            >
                                {this.state.userDetails.title}
                            </Text>
                            <Text
                                style={{
                                    color: Config.primaryColor,
                                    fontSize: 13,
                                    fontFamily: FontStyle.Regular,
                                    paddingTop: 2
                                }}
                            >
                                ${this.state.userDetails.hourly_pay}/hr
                            </Text>
                        </View>
                    </View>

                    <View
                        style={{
                            width: "90%",
                            height: 150,
                            justifyContent: "center",
                            marginBottom: 10
                            
                        }}
                    >
                        <TextInput
                            style={{
                                fontFamily: FontStyle.Light,
                                width: "100%",
                                height: 100,
                                color: "black",
                                textAlignVertical: "top",
                                padding: 5,
                                justifyContent: "flex-start",
                                borderColor: "lightgrey",
                                borderWidth: 1
                            }}
                            placeholder={translate('write_message_to_freelancer')}
                            underlineColorAndroid="transparent"
                            multiline={true}
                            onChangeText={text => {
                                this.setState({msg: text});
                            }}
                        />
                        {Platform.OS === 'android' ? <View
                            style={{
                                width: "100%",
                                height: 40,
                                borderRadius: 3,
                                borderColor: "lightgrey",
                                borderWidth: 1,
                                marginTop: 5,
                                justifyContent: "center"
                            }}
                        >
                            <Picker
                                selectedValue={this.state.job}
                                itemStyle={{
                                    fontSize: 8,
                                    fontFamily: FontStyle.Regular,
                                    color: "black"
                                }}
                                style={{
                                    height: 30,
                                    width: "95%",
                                    color: "black",
                                    margin: 5
                                }}
                                onValueChange={(itemValue, itemIndex) =>
                                    this.setState({job: itemValue})
                                }
                            >
                                <Picker.Item label="Choose a Job" value=""/>
                                {/*{this.state.jobPositions.map((item, key) => (*/}
                                {/*<Picker.Item label={item.name_en} value={item.id} key={key}/>))}*/}
                                <Picker.Item
                                    label="Graphic Designer"
                                    value="India"
                                    key="count"
                                />
                                <Picker.Item label="Freelancer" value="USA" key="count"/>
                                <Picker.Item
                                    label="React Native Developer"
                                    value="Nepal"
                                    key="count"
                                />
                            </Picker>
                        </View> : <TouchableOpacity style={{
                            borderWidth:1,
                            borderColor: "lightgrey",
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                            paddingLeft:7,
                            width: '100%',
                            color: "9A9C9E"
                        }} onPress={this.onSelectProject.bind(this)}>
                            <Text style={{
                               backgroundColor: '#ffffff',
                                color: "9A9C9E"
                            }}>
                                {this.state.selectedProjectForInvite}
                            </Text>
                        </TouchableOpacity>}
                    </View>
                </ScrollView>

                <TouchableOpacity
                    onPress={() => {
                        this.inviteFreelancerAPI();
                    }}
                    style={{
                        width: "90%",
                        height: 50,
                        marginTop: 10,
                        borderRadius: 5,
                        backgroundColor: Config.primaryColor,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <Text
                        style={{
                            color: "white",
                            fontSize: 15,
                            fontFamily: FontStyle.Bold,
                            textAlign: "center"
                        }}
                    >
                        {translate('send_invitation')}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }

    dailyUpdates = (type, value) => {
        type === "jobType"
            ? this.setState({jobType: value})
            : this.setState({dailyUpdate: value});
    };

    renderSendOffer() {
        return (
            <View  style={{flex:1, justifyContent: 'center',
            alignItems: 'center',}}>
                <View
                    style={{
                        flexDirection: "row",
                        width: "100%",
                        height: 40,
                        justifyContent: "space-between",
                        alignItems: "center",
                        borderBottomColor: "lightgrey",
                        borderBottomWidth: 1,
                        paddingLeft: 15,
                        paddingRight: 15
                        
                    }}
                >
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: FontStyle.Regular,
                            color: "#5E6062",
                            textAlign: "center"
                        }}
                    >
                        {translate('send_offers')}
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({isSendOffer: false});
                        }}
                    >
                        <Image
                            style={{
                                width: 20,
                                height: 20
                            }}
                            resizeMode="contain"
                            source={require("../../../images/facebook/cross.png")}
                        />
                    </TouchableOpacity>
                </View>

                <ScrollView
                    style={{width: "100%"}}
                    contentContainerStyle={{
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <View
                        style={{
                            width: "100%",
                            marginTop: 5,
                            marginBottom: 5,
                            justifyContent: "flex-start",
                            alignItems: "flex-start"
                        }}
                    >
                        <Text
                            style={{
                                color: "#5E6062",
                                fontSize: 14,
                                fontFamily: FontStyle.Regular,
                                paddingLeft: 15
                            }}
                        >
                            {translate('job_type')}
                        </Text>
                        <View style={{flexDirection:'row'}}>
                        <View
                            style={{
                                flex:1,
                                flexDirection: "row",
                                marginLeft: 5,
                                marginTop: 5,
                                alignItems: "flex-start"
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    marginTop: 4,
                                    marginLeft: 10,
                                    height: 15,
                                    width: 15,
                                    borderRadius: 14,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor:
                                        this.state.jobType === 1 ? Config.primaryColor : "#ebebeb"
                                }}
                                onPress={() => {
                                    this.dailyUpdates("jobType", 1);
                                }}
                            >
                                <Ionicons name="ios-checkmark" size={15} color="#fff"/>
                            </TouchableOpacity>
                            <View style={{width: "80%"}}>
                                <Text
                                    style={{
                                        color: "#5E6062",
                                        fontSize: 14,
                                        fontFamily: FontStyle.Regular,
                                        paddingLeft: 15
                                    }}
                                    onPress={() => {
                                        this.dailyUpdates("jobType", 1);
                                    }}
                                >
                                    {translate('Hourly')}
                                </Text>
                                <Text
                                    style={{
                                        color: "#9A9C9E",
                                        fontSize: 13,
                                        fontFamily: FontStyle.Regular,
                                        paddingLeft: 15
                                    }}
                                    onPress={() => {
                                        this.dailyUpdates("jobType", 1);
                                    }}
                                >
                                    {translate('work_diary')}
                                </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flex:1,
                                flexDirection: "row",
                                alignItems: "flex-start"
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    marginTop: 4,
                                    height: 15,
                                    width: 15,
                                    borderRadius: 7,
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor:
                                        this.state.jobType === 2 ? Config.primaryColor : "#ebebeb"
                                }}
                                onPress={() => {
                                    this.dailyUpdates("jobType", 2);
                                }}
                            >
                                <Ionicons name="ios-checkmark" size={15} color="#fff"/>
                            </TouchableOpacity>
                            <View style={{width: "80%"}}>
                                <Text
                                    style={{
                                        color: "#5E6062",
                                        fontSize: 14,
                                        fontFamily: FontStyle.Regular,
                                        paddingLeft: 15
                                    }}
                                    onPress={() => {
                                        this.dailyUpdates("jobType", 2);
                                    }}
                                >
                                    {translate('FixedPrice')}
                                </Text>
                                <Text
                                    style={{
                                        color: "#9A9C9E",
                                        fontSize: 13,
                                        fontFamily: FontStyle.Regular,
                                        paddingLeft: 15
                                    }}
                                    onPress={() => {
                                        this.dailyUpdates("jobType", 2);
                                    }}
                                >
                                    {translate('escrow_protection')}
                                </Text>
                            </View>
                        </View>
                        </View>
                        <View style={{marginTop: 15,
                               paddingLeft: 15,
                               paddingRight:15,
                                    flexDirection:'row',}}>
                            <Text
                                style={{
                                    color: "#5E6062",
                                    fontSize: 14,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate('Daily_Updates')}
                            </Text>
                            <View
                                style={{
                                    flexDirection: "row",
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: "row",
                                        alignItems: "center"
                                    }}
                                >
                                    <TouchableOpacity
                                        style={{
                                            marginLeft: 10,
                                            height: 15,
                                            width: 15,
                                            borderRadius: 7,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            backgroundColor:
                                                this.state.dailyUpdate === 1
                                                    ? Config.primaryColor
                                                    : "#ebebeb"
                                        }}
                                        onPress={() => {
                                            this.dailyUpdates("dailyUpdate", 1);
                                        }}
                                    >
                                        <Ionicons name="ios-checkmark" size={15} color="#fff"/>
                                    </TouchableOpacity>
                                    <Text
                                        style={{
                                            paddingLeft: 6,
                                            color: "#5E6062"
                                        }}
                                    >
                                        {translate('yes')}
                                    </Text>
                                </View>

                                <View
                                    style={{
                                        flexDirection: "row",
                                        alignItems: "center"
                                    }}
                                >
                                    <TouchableOpacity
                                        style={{
                                            marginLeft: 10,
                                            height: 15,
                                            width: 15,
                                            borderRadius: 7,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            backgroundColor:
                                                this.state.dailyUpdate === 2
                                                    ? Config.primaryColor
                                                    : "#ebebeb"
                                        }}
                                        onPress={() => {
                                            this.dailyUpdates("dailyUpdate", 2);
                                        }}
                                    >
                                        <Ionicons name="ios-checkmark" size={15} color="#fff"/>
                                    </TouchableOpacity>
                                    <Text
                                        style={{
                                            paddingLeft: 6,
                                            color: "#5E6062"
                                        }}
                                    >
                                        {translate('no')}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View
                        style={{
                            width: "100%",
                            alignItems: "flex-start",
                            marginTop: 10
                        }}
                    >
                        <Text
                            style={{
                                color: "#5E6062",
                                fontSize: 14,
                                fontFamily: FontStyle.Regular,
                                paddingLeft: 15,
                            }}
                        >
                            {translate('Amount')}
                        </Text>
                    </View>

                    <View
                        style={{
                            flexDirection: "row",
                            width: "90%",
                            justifyContent: "space-between",
                            marginTop: 5,
                            marginBottom:5,
                        }}
                    >
                        {Platform.OS === 'android' ?
                            <View
                                style={{
                                    width: "40%",
                                    height: 40,
                                    flexDirection: "row",
                                    borderRadius: 3,
                                    borderColor: "lightgrey",
                                    borderWidth: 1,
                                    justifyContent: "center"
                                }}
                            >
                                <Picker
                                    selectedValue={this.state.currency}
                                    itemStyle={{
                                        fontSize: 8,
                                        fontFamily: FontStyle.Regular
                                    }}
                                    style={{
                                        height: 30,
                                        width: "95%",
                                        color: "#979797",
                                        margin: 5
                                    }}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({currency: itemValue})
                                    }
                                >
                                    <Picker.Item label={translate('currency')} value=""/>
                                    <Picker.Item label={translate('usd')} value="USD" key="count"/>
                                    <Picker.Item label={translate('yuan')} value="Yuan" key="count"/>
                                </Picker>
                            </View> : <TouchableOpacity style={{
                                borderLeftWidth: 1,
                                borderRightWidth: 1,
                                borderTopWidth: 1,
                                borderBottomWidth: 1,
                                borderColor: "lightgrey",
                                flex:1,
                                color: "9A9C9E"
                            }} onPress={this.onSelectCurrency.bind(this)}>
                                <Text style={{
                                    height: 20,
                                    backgroundColor: '#ffffff',
                                    paddingLeft: 15,
                                    paddingRight: 15,
                                    paddingTop: 10,
                                    paddingBottom: 15,
                                    color: "9A9C9E"
                                }}>
                                    {this.state.selectCurrencyLabel}
                                </Text>
                            </TouchableOpacity>
                        }
                        <View
                            style={{
                                width: "58%",
                                height: 40,
                                borderRadius: 3,
                                borderColor: "lightgrey",
                                borderWidth: 1,
                                justifyContent: "center"
                            }}
                        >
                            <TextInput
                                style={{
                                    paddingLeft: 15,
                                    color: "#050505",
                                    fontFamily: FontStyle.Light
                                }}
                                keyboardType='numeric'
                                placeholder={translate('enter_amount')}
                                underlineColorAndroid="transparent"
                                onChangeText={text => {
                                    this.setState({amount: text});
                                }}
                            />
                        </View>
                    </View>

                    <View
                        style={{
                            width: "100%",
                            justifyContent: "center",
                            alignItems: "center"
                        }}
                    >
                        <View
                            style={{
                                width: "100%",
                                alignItems: "flex-start",
                                marginTop: 5,
                                paddingLeft: 15,
                                paddingRight:15,
                            }}
                        >
                            <Text
                                style={{
                                    color: "#5E6062",
                                    fontSize: 14,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate('like_the_offer')}
                            </Text>
                        </View>

                        <View
                            style={{
                                width: "90%",
                                alignItems: "center",
                                borderRadius: 3,
                                borderColor: "white",
                                borderWidth: 1,
                                paddingTop:15,
                                justifyContent: "center"
                            }}
                        >

                            {Platform.OS === 'android' ?
                                <View
                                    style={{
                                        width: "100%",
                                        height: 40,
                                        flexDirection: "row",
                                        borderRadius: 3,
                                        borderColor: "lightgrey",
                                        borderWidth: 1,
                                        justifyContent: "center"
                                    }}
                                >
                                    <Picker
                                        selectedValue={this.state.currency}
                                        itemStyle={{
                                            fontSize: 8,
                                            fontFamily: FontStyle.Regular
                                        }}
                                        style={{
                                            height: 30,
                                            width: "95%",
                                            color: "#979797",
                                            margin: 5
                                        }}
                                        onValueChange={(itemValue, itemIndex) =>
                                            this.setState({selectedSendOfferProject: itemValue})
                                        }
                                    >
                                        <Picker.Item label={translate('select_a_project')} value=""/>
                                        {this.state.userOpenProjects.map((projects, i) => {
                                            return <Picker.Item key={i} value={projects.id} label={projects.name}/>
                                        })}
                                    </Picker>
                                </View> : <TouchableOpacity style={{
                                    borderLeftWidth: 1,
                                    borderRightWidth: 1,
                                    borderTopWidth: 1,
                                    borderBottomWidth: 1,
                                    borderColor: "lightgrey",
                                    height: 40,
                                    width: "100%",
                                    justifyContent:'center',
                                    alignItems:'flex-start',
                                    color: "9A9C9E"
                                }} onPress={this.onSelectSendOfferProject.bind(this)}>
                                    <Text style={{
                                        height: 20,    
                                        marginLeft:15,
                                        backgroundColor: '#ffffff',
                                        borderColor: "#fff",
                                        paddingTop: 1,
                                        color: "9A9C9E"
                                    }}>
                                        {this.state.selectProjectLabel}
                                    </Text>
                                </TouchableOpacity>
                            }

                        </View>
                       
                        <View
                            style={{
                                width: "100%",
                                alignItems: "flex-start",
                                marginTop: 15,
                                paddingRight:15,
                                paddingLeft:15
                            }}
                        >
                            <Text
                                style={{
                                    color: "#5E6062",
                                    fontSize: 14,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate('write_msg_to_freelancer')}
                            </Text>
                        </View>
                        <TextInput
                            style={{
                                fontFamily: FontStyle.Light,
                                width: "90%",
                                height: 90,
                                color: "#050505",
                                margin: 5,
                                marginBottom: 10,
                                textAlignVertical: "top",
                                padding: 7,
                                justifyContent: "flex-start",
                                borderColor: "lightgrey",
                                borderWidth: 1
                            }}
                            placeholder={translate('write_message_to_freelancer')}
                            underlineColorAndroid="transparent"
                            multiline={true}
                            onChangeText={text => {
                                this.setState({msg: text});
                            }}
                        />
                    </View>
                    <TouchableOpacity
                    onPress={() => {
                        this.sendOfferAPI();
                    }}
                    style={{
                        width: "90%",
                        height: 50,
                        marginBottom: 10,
                        borderRadius: 5,
                        backgroundColor: Config.primaryColor,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    <Text
                        style={{
                            color: "white",
                            fontSize: 15,
                            fontFamily: FontStyle.Bold,
                            textAlign: "center"
                        }}
                    >
                        {translate('send_invitation')}
                    </Text>
                </TouchableOpacity>
         
                </ScrollView>

            </View>
        );
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View
                    style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <ActivityIndicator/>
                </View>
            );
        } else {
            return (
                <View style={styles.rootStyle}>
                    <Modal
                        visible={this.state.isContact}
                        animationType="fade"
                        transparent={true}
                        onRequestClose={() => console.log("Modal Closed")}
                    >
                        {this.renderModal()}
                    </Modal>

                    <Modal
                        visible={this.state.isUpdateContact}
                        animationType="fade"
                        transparent={true}
                        onRequestClose={() => console.log("Modal Closed")}
                    >
                        <View
                            style={{
                                backgroundColor: "rgba(0,0,0,0.5)",
                                flex: 1,
                                flexDirection: "column",
                                paddingTop: "10%",
                                paddingHorizontal: 10
                            }}
                        >
                            <View
                                style={{
                                    borderRadius: 4,
                                    height: "90%",
                                    borderColor: "#fff",
                                    borderWidth: 1,
                                    backgroundColor: "#fff",
                                    paddingHorizontal: 10,
                                    paddingVertical: 10
                                }}
                            >
                                <View style={{flexDirection: "row"}}>
                                    <Text
                                        style={{
                                            flex: 0.6,
                                            textAlign: "center",
                                            color: "gray",
                                            fontSize: 18,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {translate('contact_info')}
                                    </Text>
                                    <Text
                                        onPress={() => this.setState({isUpdateContact: false})}
                                        style={{
                                            color: Config.primaryColor,
                                            flex: 0.2,
                                            textAlign: "right",
                                            paddingRight: 3,
                                            fontSize: 22,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        X
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "column",
                                        flex: 1,
                                        paddingVertical: 20
                                    }}
                                >
                                    {this.state.isError && (
                                        <Text
                                            style={{
                                                color: "red",
                                                paddingHorizontal: 10,
                                                paddingVertical: 3
                                            }}
                                        >
                                            {this.state.errorMsg}
                                        </Text>
                                    )}
                                    <Input
                                        title={translate('qq')}
                                        placeholder=""
                                        value={this.state.qq_id}
                                        keyboardType='numeric'
                                        onChangeText={qq_id => {
                                            this.setState({qq_id});
                                        }}
                                    />
                                    <Input
                                        title={translate('wechat')}
                                        placeholder=""
                                        value={this.state.wechat_id}
                                        onChangeText={wechat_id => {
                                            this.setState({wechat_id});
                                        }}
                                    />
                                    <Input
                                        title={translate('skype')}
                                        placeholder=""
                                        value={this.state.skype_id}
                                        onChangeText={skype_id => {
                                            this.setState({skype_id});
                                        }}
                                    />
                                    <Input
                                        title={translate('phone')}
                                        placeholder=""
                                        value={this.state.handphone_no}
                                        onChangeText={handphone_no => {
                                            this.setState({handphone_no});
                                        }}
                                    />
                                    <TouchableOpacity
                                        style={{
                                            height: 35,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            backgroundColor: Config.primaryColor,
                                            borderRadius: 5,
                                            marginHorizontal: 10,
                                            marginTop: 10
                                        }}
                                        onPress={() => {
                                            this.updateContact();
                                        }}
                                    >
                                        <Text style={{color: "#fff"}}>{translate('submit')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                    {/* For Invite Freelancer  */}
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.isSendInvitation}
                        onRequestClose={() => console.log("Modal Closed")}
                    >
                        <View style={styles.modalContainer}>
                            <View style={styles.modalView}>{this.renderInviteToWork()}</View>
                        </View>
                    </Modal>

                    {/* For Send Offer */}
                    <Modal
                        animationType="fade"
                        transparent={true}
                        onRequestClose={() => {
                            this.setState({isSendOffer: false});
                        }}
                        visible={this.state.isSendOffer}
                    >
                        <View style={styles.modalContainer}>
                            <View style={styles.modalViewSendOffer}>
                                {this.renderSendOffer()}
                            </View>
                        </View>
                    </Modal>

                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.containerStyle}>
                            <View style={styles.profileImage}>
                                <Image
                                    source={{uri: this.state.profilePic}}
                                    style={{
                                        width: 100,
                                        height: 100,
                                        borderRadius: 50,
                                        borderWidth: 1,
                                        borderColor: "lightgrey"
                                    }}
                                />
                                <Text
                                    style={{
                                        fontSize: 16,
                                        color: "#1D1D1D",
                                        fontFamily: FontStyle.Regular
                                    }}
                                >
                                    {this.state.userDetails.real_name}
                                </Text>

                                {this.props.navigation.state.params.type ===
                                "FindFreelancer" ? (
                                    this.renderInvitationView()
                                ) : (
                                    <TouchableOpacity
                                        style={styles.contactBtnStyle}
                                        onPress={() => {
                                            this.onContactTapped(this.state.userDetails);
                                        }}
                                    >
                                        <Text style={styles.contactTextStyle}>{translate('contact')}</Text>
                                    </TouchableOpacity>
                                )}
                            </View>
                            <View
                                style={{
                                    flexDirection: "row",
                                    backgroundColor: "#fcf7f7"
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: "column",
                                        paddingHorizontal: 10,
                                        flex: 0.5,
                                        paddingVertical: 8
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.nick_name}
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: 13,
                                            color: Config.primaryColor,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.title}
                                    </Text>
                                </View>
                                <TouchableOpacity
                                    style={{
                                        paddingHorizontal: 10,
                                        flex: 0.5,
                                        alignItems: "flex-end"
                                    }}
                                    onPress={() => {
                                        this.onQRTapped(this.state.userDetails);
                                    }}
                                >
                                    {this.state.userDetails.profile_qr_code !== "" && (
                                        <Image
                                            style={{
                                                height: 60,
                                                width: 60,
                                                alignContent: "flex-end"
                                            }}
                                            source={{uri: this.state.userDetails.profile_qr_code}}
                                        />
                                    )}
                                </TouchableOpacity>
                            </View>
                            <View
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingHorizontal: 10
                                }}
                            >
                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingVertical: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            flex: 0.3,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {translate('status')}{' '}:
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 0.7,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userStatus ? this.state.userStatus : ''}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingVertical: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            flex: 0.3,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {translate('rate')}{' '}:
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 0.7,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.hourly_pay ? this.state.userDetails.hourly_pay + ' / hr' : ''}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingVertical: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            flex: 0.3,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {translate('age')}{' '}:
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 0.7,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.date_of_birth ? this._calculateAge(this.state.userDetails.date_of_birth) : ''}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingVertical: 5
                                    }}
                                >
                                    <Text
                                        style={{
                                            flex: 0.3,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {translate('experience')}{' '}:{" "}
                                    </Text>
                                    <Text
                                        style={{
                                            flex: 0.7,
                                            color: "#bababa",
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.experience ? this.state.userDetails.experience + ' ' + translate('years') : ''}
                                    </Text>
                                </View>
                            </View>
                            <View
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    paddingVertical: 10
                                }}
                            >
                                <Text style={styles.textHeading}>
                                    <FontAwesome
                                        style={{
                                            color: Config.primaryColor,
                                            fontSize: 16,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {Icons.infoCircle}
                                    </FontAwesome>{" "}
                                    {translate('info')}
                                </Text>
                                <Text
                                    style={{
                                        paddingVertical: 10,
                                        color: "#bababa",
                                        fontSize: 16,
                                        fontFamily: FontStyle.Regular
                                    }}
                                >
                                    {this.state.userDetails.about_me}
                                </Text>
                            </View>

                            {/* Render Skills  */}
                            <View
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    paddingVertical: 10
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: 14,
                                        fontFamily: FontStyle.Regular
                                    }}
                                >
                                    <Text style={styles.textHeading}>
                                        <Ionicons
                                            name="md-hand"
                                            color={Config.primaryColor}
                                            size={18}
                                        />{" "}
                                        {translate('skills')}
                                    </Text>
                                </Text>
                                <View style={styles.middleSideStyle}>
                                    {this.renderSkill()}
                                </View>
                            </View>

                            {/* Render Languages */}
                            <View
                                style={{
                                    flexDirection: "column",
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    paddingVertical: 10
                                }}
                            >
                                <Text style={styles.textHeading}>
                                    <Ionicons
                                        name="md-globe"
                                        color={Config.primaryColor}
                                        size={18}
                                    />{" "}
                                    {translate('LANGUAGES')}
                                </Text>
                                <View style={styles.middleSideStyle}>
                                    {this.renderLanguage(this.state.userDetails.languages)}
                                </View>
                            </View>

                            {/* Render Review View */}
                            <View
                                style={{
                                    flexDirection: "row",
                                    flex: 1,
                                    paddingHorizontal: 10,
                                    paddingVertical: 10,
                                    justifyContent: "space-between"
                                }}
                            >
                                <View
                                    style={{
                                        flex: 0.5,
                                        flexDirection: "column",
                                        alignItems: "center",
                                        marginRight: 10
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: Config.primaryColor,
                                            fontSize: 54,
                                            paddingTop: 10,
                                            fontFamily: FontStyle.Regular
                                        }}
                                    >
                                        {this.state.userDetails.user_ratings.averageRating}
                                    </Text>
                                    <Rating
                                        type="star"
                                        startingValue={Number(
                                            this.state.userDetails.user_ratings.averageRating
                                        )}
                                        readonly
                                        imageSize={20}
                                        style={{alignContent: "flex-start"}}
                                    />
                                    <Text
                                        style={{
                                            paddingVertical: 10,
                                            color: "#bababa"
                                        }}
                                    >
                                        ( {this.state.userDetails.user_ratings.totalCountReview}{" "}
                                        {translate('reviews')} )
                                    </Text>
                                </View>

                                <View
                                    style={{
                                        flex: 0.6,
                                        alignItems: "center",
                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "flex-end",
                                        margin: 5,
                                        marginLeft: 10
                                    }}
                                >
                                    {/* Show Graph here */}
                                    {this.renderBars(this.state.userRatingsBar)}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = {
    rootStyle: {
        backgroundColor: "#fff",
        flex: 1,
        flexDirection: "column",
        padding: 10,
        justifyContent: "center"
    },

    containerStyle: {
        padding: 10,
        backgroundColor: "#fff",
        flexDirection: "column",
        flex: 1
    },

    dialogStyle: {
        flex: 1,
        backgroundColor: "rgba(44, 62, 80, 0.6)",
        alignItems: "center",
        justifyContent: "center"
    },

    modalViewSendOffer: {
        width: width * 0.85,
        height: 600,
        backgroundColor: "#fff",
        borderRadius: 3
    },

    modalContainer: {
        flex: 1,
        // width: width,
        // height: height,
        flexDirection: "column",
        backgroundColor: "rgba(44, 62, 80, 0.6)",
        alignItems: "center",
        justifyContent: "center"
    },

    modalView: {
        width: width * 0.85,
        height: 400,
        backgroundColor: "#fff",
        borderRadius: 3
    },

    profileImage: {
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 10
    },

    contactBtnStyle: {
        width: 180,
        height: 45,
        justifyContent: "center",
        alignItems: "center"
    },

    middleSideStyle: {
        flexDirection: "row",
        paddingVertical: 20
    },

    contactTextStyle: {
        color: "#fff",
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 30,
        paddingVertical: 10,
        fontSize: 15,
        fontFamily: FontStyle.Bold,
        marginTop: 10
    },

    topTabWrapperStyle: {
        flexDirection: "row",
        flexWrap: "wrap",
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 5
    },

    tabControlStyle: {
        backgroundColor: "#808080",
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5
    },

    tabControlText: {
        color: "#fff",
        fontSize: 15,
        fontFamily: FontStyle.Regular
    },

    activeTabControlStyle: {
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5
    },

    starStyle: {
        flexDirection: "row"
    },

    textHeading: {
        color: Config.textSecondaryColor,
        fontFamily: FontStyle.Regular
    },

    topButtonWrapperStyle: {
        flexDirection: "row",
        shadowColor: "#ccc",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1
    },

    topButtonStyle: {
        flex: 1,
        height: 50,
        justifyContent: "center",
        alignItems: "center",
        borderBottomColor: "#ecf0f1",
        borderBottomWidth: 2
    },

    topButtonActiveStyle: {
        borderBottomColor: Config.primaryColor,
        borderBottomWidth: 2
    },

    topTextActiveStyle: {
        color: Config.primaryColor
    },

    topSideStyle: {
        flexDirection: "row",
        paddingHorizontal: 10,
        paddingTop: 10,
        marginBottom: 10
    },

    middleSideStyle: {
        flexDirection: "row",
        flexWrap: "wrap",
        marginTop: 10
    },

    avatarStyle: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    cardButtonStyle: {
        flex: 1,
        height: 30,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row"
    },

    cardButtonTextStyle: {
        color: Config.textSecondaryColor,
        fontSize: 12,
        fontFamily: FontStyle.Regular
    },

    text1Style: {
        flex: 1,
        fontSize: 16,
        fontFamily: FontStyle.Regular
    },

    boxInsideStyle: {
        flexDirection: "column",
        backgroundColor: "#fff"
    },
    //
    // text1Style: {
    //   flex: 1,
    //   fontSize: 16,
    //   fontFamily: FontStyle.Regular,
    // },

    text2Style: {
        fontSize: 13,
        color: "#bdc3c7",
        fontFamily: FontStyle.Regular
    },

    text3Style: {
        fontSize: 13,
        fontWeight: "200",
        fontFamily: FontStyle.Light
    },

    textDescription: {
        fontSize: 15,
        color: "grey",
        fontFamily: FontStyle.Regular
    }
};
const mapStateToProps = state => {
    return {
        component: state.component,
        userData: state.auth.userData,
        skills: state.profile.skills
    }
};
const mapDispatchToProps = dispatch => {
    return {
        setRoot: root =>
            dispatch({
                type: "set_root",
                root
            })
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutOtherUser);
