import React, { Component } from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  Image,
  StyleSheet,
  TextInput
} from 'react-native';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Config from '../../Config';
import { translate } from '../../i18n';
import FontStyle from '../../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

export default class QRDetail extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'QR Detail',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);

  }

  render() {
    const userInfo = this.props.navigation.state.params.userInfo;
    return (
      <View style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <ScrollView style={styles.rootStyle} contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center'
        }}
                    keyboardShouldPersistTaps='always' horizontal={false}
                    showsVerticalScrollIndicator={false}>
          <View style={styles.container}>

            {/* Profile Image View */}

            <View style={styles.photoWrapperStyle}>
              <Image source={{ uri: userInfo.img_avatar }}
                     style={{
                       width: 100,
                       height: 100,
                       borderRadius: 50
                     }}/>
              <Text style={styles.userNameStyle}>{userInfo.real_name}</Text>
            </View>

            <View style={{
              width: '96%',
              alignItems: 'center'
            }}>
              <View style={{
                width: '100%',
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: 120,
                backgroundColor: '#E0E0E0',
                marginTop: 60
              }}>
                <Text style={{
                  textAlign: 'center',
                  fontSize: 15,
                  color: '#3F3F3F'
                }}>{userInfo.nick_name}</Text>
                <Text style={{
                  textAlign: 'center',
                  fontSize: 13,
                  color: Config.primaryColor,
                  paddingBottom: 15
                }}>{userInfo.title}</Text>
                {/* <Text style={{textAlign:'center', fontSize: 12, paddingBottom:5 }} >{userInfo.title}</Text> */}
              </View>

              <TouchableOpacity style={{
                position: 'absolute',
                bottom: 80
              }}>
                <Image style={{
                  height: 80,
                  width: 80
                }} source={{ uri: userInfo.profile_qr_code }}/>
              </TouchableOpacity>
            </View>

            <View style={{
              margin: 5,
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Text style={{
                fontWeight: '700',
                fontSize: 16,
                textAlign: 'center',
                color: '#2E2E2E'
              }}>
                Scan to Check Profile Details
              </Text>
            </View>

          </View>


        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    flexDirection: 'column',
    width: '95%',
    backgroundColor: '#f7f7f7',
    marginTop: 10,
    marginBottom: 10
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    width: '100%',
  },
  photoWrapperStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 5
  },
  userNameStyle: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 8
  },
});
