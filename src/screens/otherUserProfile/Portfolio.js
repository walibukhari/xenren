import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ActivityIndicator,
  Alert,
  FlatList,
  Image,
  Dimensions,
} from 'react-native';
import Config from '../../Config';
import Review from '../about/Review';
import { translate, USER_PERFERRED_LOCALE } from '../../i18n';
import Portfolio from './OtherUserProtfolio';
import HttpRequest from '../../components/HttpRequest';
import { NavigationActions } from 'react-navigation';
import LocalData from '../../components/LocalData';
import FontStyle from '../../constants/FontStyle';

const { width, height } = Dimensions.get('window');
class UserPortfolioData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTabIndex: 0,
      data: this.props.userData,
      isLoading: true,
      platfromData: [],
      customerReviewData: [],
      portfolioData: [],
      comment: true,
      customReview: false,
      portfolio: false,
    };
  }

  componentDidMount() {
    this.getPlatformReviews(this.state.data.token);
  }

    getPlatformReviews = (access_token) => {
      const userId = this.props.userId;
      HttpRequest.getUserPlatformReview(access_token, userId).then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          if (result.data.reviews.length > 0) {
            this.setState({
              platfromData: result.data.reviews,
              isLoading: false,
            });
            if (this.state.platfromData !== '') {
              this.setState({ comment: false });
            }
          } else {
            this.setState({ isLoading: false });
            // alert('No data found');
          }
        } else {
          // Show Dialog
          this.showAlert();
          this.setState({
            isLoading: false,
          });
        }
      }).catch((error) => {
        this.setState({ isLoading: false });
        alert('Error Occurred. Please try again.');
      });
    }

    getCustomerReviews = (access_token) => {
      this.setState({ customReview: true });
      const userId = this.props.userId;
      HttpRequest.getUserCustomerReview(access_token, userId).then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          if (result.data.reviews.length > 0) {
            this.setState({
              customerReviewData: result.data.reviews,
              isLoading: false,
              customReview: false,
            });
          } else {
            this.setState({ isLoading: false });
            // alert('No data found');
          }
        } else {
          // Show Dialog
          this.showAlert();
          this.setState({
            isLoading: false,
          });
        }
      }).catch((error) => {
        this.setState({ isLoading: false });
        alert('Error Occurred. Please try again.');
      });
    }

    getProtfolio = (access_token) => {
      this.setState({ portfolio: true });
      const userId = this.props.userId;
      HttpRequest.getUserPortfoilo(access_token, userId).then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          if (result.data.portfolios.length > 0) {
            this.setState({
              portfolioData: result.data.portfolios,
              isLoading: false,
              portfolio: false,
            });
          } else {
            this.setState({ isLoading: false });
            // alert('No data found');
          }
        } else {
          // Show Dialog
          this.showAlert();
          this.setState({
            isLoading: false,
          });
        }
      }).catch((error) => {
        this.setState({ isLoading: false });
        alert('Error Occurred. Please try again.');
      });
    }

    showAlert() {
      // Show Dialog
      global.setTimeout(() => {
        Alert.alert(
          'Warning',
          'Token expire!',
          [
            { text: 'OK', onPress: () => { this.logout(); } },
          ],
        );
      }, 200);
    }

    logout() {
      LocalData.setUserData(null);
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Login',
          }),
        ],
      }));
    }


    onPlatformReviewPressed = () => {
      if (this.state.platfromData.length > 0) {
        this.setState({ activeTabIndex: 0, isLoading: false });
      } else {
        this.setState({ activeTabIndex: 0, comment: true, isLoading: false });
      }
    }

    onCustomerReviewPressed = () => {
      if (this.state.customerReviewData.length > 0) {
        this.setState({ activeTabIndex: 1, isLoading: false });
      } else {
        this.setState({ activeTabIndex: 1, isLoading: true });
        this.getCustomerReviews(this.state.data.token);
      }
    }

    onPortfolioPressed = () => {
      if (this.state.portfolioData.length > 0) {
        this.setState({ activeTabIndex: 2, isLoading: false });
      } else {
        this.setState({ activeTabIndex: 2, isLoading: true });
        this.getProtfolio(this.state.data.token);
      }
    }

    renderLoader = () => (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                <ActivityIndicator />
            </View>
    )

    renderPlatformReview = () => {
      if (this.state.platfromData) {
        return (
                <FlatList
                    style={{ flex: 1 }}
                    data={this.state.platfromData}
                    renderItem={item => this.renderRow(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
        );
      }
      if (this.state.isLoading) {
        this.renderLoader();
      } else {
        return (
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.platfromData}
                        renderItem={item => this.renderRow(item)}
                        keyExtractor={(item, index) => index.toString()}
                    />
        );
      }
    }

    renderCustomerReview = () => {
      if (this.state.customerReviewData) {
        return (
                <FlatList
                        style={{ flex: 1 }}
                        data={this.state.customerReviewData}
                        renderItem={item => this.renderRow(item)}
                        keyExtractor={(item, index) => index.toString()}
                    />
        );
      }
      if (this.state.isLoading) {
        this.renderLoader();
      } else {
        return (
                    <FlatList
                        style={{ flex: 1 }}
                        data={this.state.customerReviewData}
                        renderItem={item => this.renderRow(item)}
                        keyExtractor={(item, index) => index.toString()}
                    />
        );
      }
    }

    renderPortfolio = () => {
      if (this.state.portfolioData.length > 0) {
        return (
                <Portfolio navigation={this.props.navigation} />
        );
      }
      if (this.state.isLoading) {
        this.renderLoader();
      } else {
        return (
                    <Portfolio data={this.state.portfolioData} navigation={this.props.navigation} />
        );
      }
    }

    renderRow = ({ item }) => (<Review review={item} />)

    render() {
      if (this.state.isLoading) {
        return (
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                >
                    <ActivityIndicator />
                </View>
        );
      }
      return (
                <View style={styles.rootStyle}>

                    <View style={styles.topTabWrapperStyle}>

                        <TouchableOpacity
                            style={this.state.activeTabIndex === 0 ? styles.activeTabControlStyle : styles.tabControlStyle}
                            onPress={() => {
                                this.setState({ customReview: false, portfolio: false });
                                this.onPlatformReviewPressed();
                            }}>
                            <Text style={styles.tabControlText}>{translate('platform_review')}
                                ({this.state.platfromData.length})</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.activeTabIndex === 1 ? styles.activeTabControlStyle : styles.tabControlStyle}
                            onPress={() => {
                                this.setState({ comment: false, portfolio: false });
                                this.onCustomerReviewPressed();
                            }}>
                            <Text style={styles.tabControlText}>{translate('custom_review')}
                                ({this.state.customerReviewData.length})</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.activeTabIndex === 2 ? styles.activeTabControlStyle : styles.tabControlStyle}
                            onPress={() => {
                                this.setState({ comment: false, customReview: false });
                                this.onPortfolioPressed();
                            }}>
                            <Text style={styles.tabControlText}>{translate('portfolio')} ({this.state.portfolioData.length})</Text>
                        </TouchableOpacity>

                    </View>

                    {/* Render Selected Tab Data */}
                    {this.state.comment === true && (
                        <View style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                          marginTop: height / 5,
                        }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{
                                  fontFamily: FontStyle.Light,
                                  left: USER_PERFERRED_LOCALE.includes('zh') ? 45 : 0,
                                }}
                                >
                                  {translate('comment_yet')}
                                </Text>
                                <View style={{ width: 200, height: 150 }}>
                                    <Image
                                        source={require('../../../images/inboxlogo.png')}
                                        style={{
                                            width: 120,
                                            height: 120,
                                            marginLeft: USER_PERFERRED_LOCALE.includes('zh') ? width / 8 : width / 4,
                                        }}
                                        resizeMode="contain"
                                    />
                                </View>
                            </View>
                        </View>
                    )}
                    {this.state.customReview === true && (
                        <View style={{
                          flex: 1,
                          alignItems: 'center',
                          justifyContent: 'center',
                          top: 60,
                          marginTop: height / 28,
                        }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{
                                  fontFamily: FontStyle.Light,
                                  left: USER_PERFERRED_LOCALE.includes('zh') ? 40 : 0,
                                }}>
                                  {translate('review_yet')}
                                </Text>
                                <View style={{ width: 200, height: 150 }}>
                                    <Image
                                        source={require('../../../images/inboxlogo.png')}
                                        style={{
                                            width: 120,
                                            height: 120,
                                            marginLeft: USER_PERFERRED_LOCALE.includes('zh') ? width / 8 : width / 4,
                                        }}
                                        resizeMode="contain"
                                    />
                                </View>
                            </View>
                        </View>
                    )}
                    {this.state.portfolio === true && (
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            top: 60,
                            // backgroundColor: '#f9f9f9',
                        }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{
                                  fontFamily: FontStyle.Light,
                                  left: USER_PERFERRED_LOCALE.includes('zh') ? 40 : 0,
                                }}>
                                  {translate('got_portfolio_yet')}
                                </Text>
                                <View style={{ width: 200, height: 150 }}>
                                    <Image
                                        source={require('../../../images/inboxlogo_transparent.png')}
                                        style={{
                                            width: 120,
                                            height: 120,
                                            marginLeft: USER_PERFERRED_LOCALE.includes('zh') ? width / 8 : width / 4,
                                        }}
                                        resizeMode="contain"
                                    />
                                </View>
                            </View>
                        </View>
                    )}
                    <View style={styles.rootStyle}>
                        {this.state.activeTabIndex === 0 &&
                        this.renderPlatformReview()
                        }

                        {this.state.activeTabIndex === 1 &&
                        this.renderCustomerReview()
                        }

                        {this.state.activeTabIndex === 2 &&
                        this.renderPortfolio()
                        }
                    </View>

                </View>
      );
    }
}

const styles = {
  rootStyle: {
    backgroundColor: '#f9f9f9',
    flex: 1,
    flexDirection: 'column',
  },

  topTabWrapperStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5,
  },

  tabControlStyle: {
    backgroundColor: '#808080',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
    margin: 3,
  },

  tabControlText: {
    color: '#fff',
    fontSize: 15,
    fontFamily: FontStyle.Regular,
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5,
  },
};

const mapStateToProps = state => ({
  component: state.component,
  userData: state.auth.userData,
});

export default connect(mapStateToProps)(UserPortfolioData);
