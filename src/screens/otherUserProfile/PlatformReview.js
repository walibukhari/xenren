import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, ListView } from 'react-native';

import Config from '../../Config';
import Review from '../subs/Review';
import { translate } from '../../i18n';

class Comment extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            activeTabIndex: 0,
        }
    }

    
    renderRow(review) {
        return (
          <Review
          review={review}
        />
        )
    }

    render() {
        const { officialComment, comments } = this.state;
        return (
            <View style={styles.rootStyle}>

                <View style={styles.topTabWrapperStyle}>

                    <TouchableOpacity style={this.state.activeTabIndex === 0 ? styles.activeTabControlStyle : styles.tabControlStyle}
                        onPress={() => { this.setState({ activeTabIndex: 0 }) }}>
                        <Text style={styles.tabControlText}>{translate('official_comment')} (21)</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={this.state.activeTabIndex === 1 ? styles.activeTabControlStyle : styles.tabControlStyle}
                        onPress={() => { this.setState({ activeTabIndex: 1 }) }}>
                        <Text style={styles.tabControlText}>{translate('comments')} (315)</Text>
                    </TouchableOpacity>
                    
                </View>
               
                    <View style={styles.rootStyle}>
                    {this.state.activeTabIndex === 0 &&
                        <ListView
                            style={{ flex: 1 }}
                            dataSource={officialComment}
                            renderRow={ (rowData) => { return this.renderRow(rowData) } }
                        />
                    }
                    {this.state.activeTabIndex === 1 && 
                      <ListView
                         style={{ flex: 1 }}
                         dataSource={comments}
                         renderRow={ (rowData) => { return this.renderRow(rowData) } }
                     />
                    }
                    </View>
               
            </View>
        );
    }
}

const styles = {
    rootStyle: {
        backgroundColor: '#f9f9f9',
        flex: 1,
        flexDirection: 'column'
    },

    topTabWrapperStyle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginBottom: 5
    },

    tabControlStyle: {
        backgroundColor: '#808080',
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    },

    tabControlText: {
        color: '#fff',
        fontSize: 15,
        fontWeight: '300'
    },

    activeTabControlStyle: {
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 12,
        paddingVertical: 5,
        borderRadius: 15,
        marginRight: 5,
    }
}

const mapStateToProps = state => ({
    component: state.component
})

export default connect(mapStateToProps)(Comment);
