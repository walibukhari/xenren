import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  ListView,
  Linking
} from "react-native";
import Config from "../../Config";
import Project from "../jobs/subs/Project";
import { translate } from "../../i18n";
import FontStyle from "../../constants/FontStyle";

class UserPortfolio extends Component {
  constructor(props) {
    super(props);

    const portifolioData = [
      {
        name: "Graphic Design for Legend Lair Website",
        link: "www.behance.com/legendlair",
        images: [
          require("../../../images/account_profile/pot1.png"),
          require("../../../images/account_profile/pot2.png"),
          require("../../../images/account_profile/pot3.png")
        ]
      }
    ];

    this.state = {
      activeTabIndex: 0,
      portifolio: this.convertToDataSource(portifolioData),
      officialComment: this.convertToDataSource(this.getOfficialComments())
    };
  }

  convertToDataSource(array) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    return ds.cloneWithRows(array);
  }

  getOfficialComments() {
    const officialComment = [
      {
        id: 1301,
        project: "GRAPHIC DESIGNER REQUIRED FOR VARIOUS PROJECTS",
        priceText: "PAY HOURLY",
        project_type: "OFFICIAL PROJECT",
        end_date: "2017-07-29",
        time_frame: "4 DAYS",
        location: "Legend",
        funding: "SEED",
        projectId: "$15",
        type: "DIFICULTY",
        position: "POSITION NEEDED",
        position_type: "Designer",
        number: "Number Shared Issue",
        price: "$14,540",
        shared: "14,540",
        numberpercent: "10%",
        description:
          "Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over",
        star: 5,
        avatar: require("../../../images/account_profile/pot1.png"),
        avatarM: require("../../../images/jobs/Shape.png"),
        skills: [
          { id: "HTML5" },
          { id: "Photoshop" },
          { id: "Adobe Ilustrator" }
        ],
        attachments: [
          {
            type: "picture",
            source: require("../../../images/other/icon_picture.png")
          },
          { type: "doc", source: "some source" },
          { type: "xls", source: "some source" }
        ]
      },
      {
        id: 1302,
        project: "User Submit Project 2",
        name: "Peter Jude",
        project: "GRAPHIC DESIGNER REQUIRED FOR VARIOUS PROJECTS",
        priceText: "PAY HOURLY",
        project_type: "OFFICIAL PROJECT",
        end_date: "2017-07-29",
        time_frame: "4 DAYS",
        location: "Legend",
        funding: "SEED",
        projectId: "$15",
        date: "Posted,9 months ago",
        type: "DIFICULTY",
        position: "POSITION NEEDED",
        position_type: "Developer",
        number: "Number Shared Issue",
        price: "$14,540",
        numberpercent: "10%",
        description:
          "Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over",
        star: 5,
        avatar: require("../../../images/account_profile/pot2.png"),
        avatarM: require("../../../images/jobs/Shape.png"),
        skills: [
          { id: "HTML5" },
          { id: "Photoshop" },
          { id: "Adobe Ilustrator" }
        ],
        attachments: [
          {
            type: "picture",
            source: require("../../../images/other/icon_picture.png")
          },
          { type: "doc", source: "some source" },
          { type: "xls", source: "some source" }
        ]
      },
      {
        id: 1303,
        project: "User Submit Project 3",
        name: "Peter Jude",
        project: "GRAPHIC DESIGNER REQUIRED FOR VARIOUS PROJECTS",
        priceText: "PAY HOURLY",
        project_type: "OFFICIAL PROJECT",
        end_date: "2017-07-29",
        time_frame: "4 DAYS",
        location: "Legend",
        funding: "SEED",
        projectId: "$15",
        date: "Posted,9 months ago",
        type: "DIFICULTY",
        position: "POSITION NEEDED",
        position_type: "Designer",
        number: "Number Shared Issue",
        price: "$14,540",
        numberpercent: "10%",
        description:
          "Contrary to popular belief, Lorem Ipsum is not simply a random text it has root in clasical Latin literature from 45BC makin it over",
        star: 5,
        avatar: require("../../../images/account_profile/pot3.png"),
        avatarM: require("../../../images/jobs/Shape.png"),
        skills: [
          { id: "HTML5" },
          { id: "Photoshop" },
          { id: "Adobe Ilustrator" }
        ],
        attachments: [
          {
            type: "picture",
            source: require("../../../images/other/icon_picture.png")
          },
          { type: "doc", source: "some source" },
          { type: "xls", source: "some source" }
        ]
      }
    ];
    return officialComment;
  }

  renderImages(images) {
    return images.map((image, index) => (
      <View
        key={index}
        style={{
          flexDirection: "column",
          alignItems: "center",
          marginBottom: 10
        }}
      >
        <Image
          source={image}
          style={styles.itemImageStyle}
          resizeMode="contain"
        >
          <Text style={styles.paragraph}>TITLE</Text>
        </Image>
      </View>
    ));
  }

  renderRow2 = item => {
    return (
      <View style={styles.portifolioWrapper}>
        <View style={{ marginHorizontal: 10, marginVertical: 10 }}>
          <View style={styles.boxInsideStyle}>
            <View style={{ marginVertical: 10 }}>
              <Text style={styles.itemNameStyle}>{item.name}</Text>
            </View>
            <View style={{ marginVertical: 10 }}>
              {this.renderImages(item)}
            </View>
          </View>
        </View>
      </View>
    );
  };
  renderRow(review) {
    return <Project review={review} navigation={this.props.navigation} />;
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.rootStyle2}>
          <FlatList
            data={this.props.data}
            renderItem={({ item }) => this.renderRow(item)}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },
  rootStyle2: {
    //  paddingHorizontal: 10,
    paddingVertical: 0,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#ecf0f1"
  },
  paragraph: {
    textAlign: "center"
  },
  topTabWrapperStyle: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 5
  },

  tabControlStyle: {
    backgroundColor: "#808080",
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  },

  tabControlText: {
    color: "#fff",
    fontSize: 18,
    fontFamily: FontStyle.Regular
  },

  activeTabControlStyle: {
    backgroundColor: Config.primaryColor,
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRadius: 15,
    marginRight: 5
  },

  createProjectWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: 50
  },

  createProjectText: {
    paddingHorizontal: 5,
    color: Config.primaryColor,
    fontSize: 18,
    fontFamily: FontStyle.Regular
  },

  portifolioWrapper: {
    backgroundColor: "#fff",
    paddingHorizontal: 5,
    paddingVertical: 10
  },

  boxInsideStyle: {
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center"
  },

  itemNameStyle: {
    fontSize: 18
  },

  itemImageStyle: {
    width: 350,
    height: 250,
    opacity: 0.2
  },

  itemLinkStyle: {
    flexDirection: "row",
    backgroundColor: "#59af24",
    paddingHorizontal: 15,
    paddingVertical: 6,
    borderRadius: 15
  },

  itemLinkTextStyle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: FontStyle.Regular,
    marginRight: 10
  }
};

const mapStateToProps = state => ({
  component: state.component
});

export default connect(mapStateToProps)(UserPortfolio);
