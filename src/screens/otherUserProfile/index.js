/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Config from '../../Config';
import { translate } from '../../i18n';
import About from './About';
import PortFolio from './Portfolio';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../../constants/FontStyle';

class OtherUserProfile extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    console.log('otherUserProfile index.js')
    super(props);

    this.state = {
      activeTabIndex: 0,
    };

    this.root = this.props.component.root;
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={style.rootStyle}>
          {/* Back Button View */}
          <View style={style.navigationStyle}>
            <View style={style.navigationWrapper}>
              <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  style={{
                    marginTop: 8,
                    marginLeft: 10,
                    flexDirection: "row",
                  }}
              >
                <Image
                    style={{width:20,height:26,position:'relative',top:-3}}
                    source={require('../../../images/arrowLA.png')}
                />

                <Text
                    onPress={() => this.props.navigation.goBack()}
                    style={{
                      marginLeft: 10,
                      marginTop:-1,
                      fontSize: 19.5,
                      fontWeight:'normal',
                      color: Config.primaryColor,
                      fontFamily: FontStyle.Regular,
                    }}
                >
                  {translate("personal_info")}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.topButtonWrapperStyle}>
          <TouchableOpacity onPress={() => {
            this.setState({ activeTabIndex: 0 });
          }}
                            style={[styles.topButtonStyle, this.state.activeTabIndex === 0 ? styles.topButtonActiveStyle : {}]}>
            <Text
              style={this.state.activeTabIndex === 0 ? styles.topTextActiveStyle : {}}>{translate('about')}</Text>
          </TouchableOpacity>
          <View style={{
            width: 1,
            height: 50,
            backgroundColor: '#ecf0f1',
          }}/>
          <TouchableOpacity onPress={() => {
            this.setState({ activeTabIndex: 1 });
          }}
                            style={[styles.topButtonStyle, this.state.activeTabIndex === 1 ? styles.topButtonActiveStyle : {}]}>
            <Text
              style={this.state.activeTabIndex === 1 ? styles.topTextActiveStyle : {}}>{translate('comments')}</Text>
          </TouchableOpacity>
        </View>

        {this.state.activeTabIndex === 0 && <About navigation={this.props.navigation}
                                                   userId={this.props.navigation.state.params.user_id}/>}
        {this.state.activeTabIndex === 1 &&
        < PortFolio userId={this.props.navigation.state.params.user_id}/>}
      </View>
    );
  }
}


const style = {
  rootStyle: {
    backgroundColor: "white",
    // paddingHorizontal: 10
  },

  navigationWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
  },
  navigationStyle: {
    height: 55,
    flexDirection: "row",
    marginTop: 10,
    paddingHorizontal: 5,
    // alignItems:'center'
  },
}
const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },

  topButtonWrapperStyle: {
    flexDirection: 'row',
    shadowColor: '#ccc',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  topButtonStyle: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#ecf0f1',
    borderBottomWidth: 2,
  },

  topButtonActiveStyle: {
    borderBottomColor: Config.primaryColor,
    borderBottomWidth: 2,
  },

  topTextActiveStyle: {
    color: Config.primaryColor,
  },
};

const mapStateToProps = (state) => {
  return {
    component: state.component,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setRoot: root => dispatch({
      type: 'set_root',
      root,
    }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OtherUserProfile);
