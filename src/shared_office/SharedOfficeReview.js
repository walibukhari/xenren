import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  BackHandler,
  ActivityIndicator,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import HttpRequest from '../components/HttpRequest';
import { translate } from '../i18n';

const { width } = Dimensions.get('window');
class SharedOfficeReview extends Component {
    static propTypes = {
      navigation: PropTypes.object.isRequired,
      sharedOffice: PropTypes.func,
      sharedOfficeData: PropTypes.object,
      userData: PropTypes.object,
    };

    static navigationOptions = {
      header: null,
    };

    constructor(props) {
      super(props);
      this.state = {
        sharedOffice: this.props.sharedOfficeData,
        userData: this.props.userData,
        timeCost: this.props.navigation.state.params.timeCost,
        date: '',
        month: '',
        comment: '',
        monthNames: [translate('Jan'), translate('Feb'), translate('March'), translate('April'), translate('May'), translate('June'), translate('July'), translate('August'), translate('Sep'), translate('Oct'), translate('Nov'), translate('Dec'),
        ],
        starColor: false,
        loading: false,
        oneStar: false,
        twoStar: false,
        threeStar: false,
        fourStar: false,
        fiveStar: false,
        ratingValue: 0,
        hours: 0,
        m: 0,
      };
    }

    componentDidMount() {
        console.log('get time price');
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
      if (this.state.timeCost.minutes >= 60) {
        const hours = (this.state.timeCost.minutes / 60);
        const rhours = Math.floor(hours);
        const minutes = (hours - rhours) * 60;
        const rminutes = Math.round(minutes);
        this.setState({ hours: rhours, m: rminutes });
      }
      const today = new Date();
      const currentDate = today.getDate();
      const currentMonth = today.getMonth();
      this.setState({ date: currentDate, month: this.state.monthNames[currentMonth] });
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
      if (this.props.sharedOffice !== null) {
        this.props.sharedOffice(null);
      }
      this.props.navigation.navigate('Root');
    };

    oneStar() {
      if (this.state.oneStar === true) {
        if (this.state.twoStar === true) {
          this.setState({
            twoStar: false,
            threeStar: false,
            fourStar: false,
            fiveStar: false,
            ratingValue: 1,
          });
        } else {
          this.setState({ oneStar: false, ratingValue: 0 });
        }
      } else {
        this.setState({ oneStar: true, ratingValue: 1 });
      }
    }

    twoStar() {
      if (this.state.twoStar === true) {
        if (this.state.threeStar === true) {
          this.setState({ threeStar: false, fourStar: false, fiveStar: false });
          this.setState({ ratingValue: 2 });
        } else {
          this.setState({ oneStar: false, twoStar: false, ratingValue: 1 });
        }
      } else {
        this.setState({ oneStar: true, twoStar: true, ratingValue: 2 });
      }
    }

    threeStar() {
      if (this.state.threeStar === true) {
        if (this.state.fourStar === true) {
          this.setState({ fourStar: false, fiveStar: false, ratingValue: 3 });
        } else {
          this.setState({ oneStar: false, twoStar: false, threeStar: false });
          this.setState({ ratingValue: 2 });
        }
      } else {
        this.setState({ oneStar: true, twoStar: true, threeStar: true });
        this.setState({ ratingValue: 3 });
      }
    }

    fourStar() {
      if (this.state.fourStar === true) {
        if (this.state.fiveStar === true) {
          this.setState({ fiveStar: false, ratingValue: 4 });
        } else {
          this.setState({
            oneStar: false,
            twoStar: false,
            threeStar: false,
            fourStar: false,
            ratingValue: 3,
          });
        }
      } else {
        this.setState({
          oneStar: true,
          twoStar: true,
          threeStar: true,
          fourStar: true,
          ratingValue: 4,
        });
      }
    }

    fiveStar() {
      if (this.state.fiveStar === true) {
        this.setState({
          oneStar: false,
          twoStar: false,
          threeStar: false,
          fourStar: false,
          fiveStar: false,
          ratingValue: 4,
        });
      } else {
        this.setState({
          oneStar: true,
          twoStar: true,
          threeStar: true,
          fourStar: true,
          fiveStar: true,
          ratingValue: 5,
        });
      }
    }

    submitRating() {
      if (this.state.ratingValue !== 0) {
        this.setState({ loading: true });
        const { token } = this.state.userData;
          let officeId;
          if(this.state.sharedOffice && this.state.sharedOffice.office.id !== '' ) {
              officeId = this.state.sharedOffice.office.id;
          } else {
              officeId = this.props.userData.last_scanned_office.office.id;
          }
        const rate = this.state.ratingValue;
        const { comment } = this.state;
        HttpRequest.sharedOfficeRating(token, officeId, rate, comment)
          .then((response) => {
            const result = response.data;
            if (result.status === 'success') {
              this.props.sharedOffice(null);
              this.setState({ loading: false });
              this.props.navigation.navigate('OnSuccessReview');
            } else {
              Alert.alert(result.message);
            }
          })
          .catch((error) => {
            Alert.alert(error);
          });
      } else {
        Alert.alert(translate('office_rating_required'));
      }
    }

    render() {
        let officeName;
        if(this.state.sharedOffice && this.state.sharedOffice.office.office_name !== '' ) {
            officeName = this.state.sharedOffice.office.office_name;
        } else {
            officeName = this.props.userData.last_scanned_office.office.office_name;
        }
      return (
        <View style={styles.rootStyle}>
          {/* Header */}
          <View style={styles.header}>
            {/* Date Month */}
            <View
              style={{
                width: '15%',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: Config.primaryColor,
                  fontSize: 18,
                  fontFamily: FontStyle.Bold,
                }}>
                {this.state.date}.
              </Text>
              <Text style={{ fontFamily: FontStyle.Light }}>
                {this.state.month}
              </Text>
            </View>

            {/* Office and Minutes Spent */}
            <View
              style={{
                width: '55%',
                marginLeft: 25,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'flex-start',
              }}>
              <Text style={{ fontFamily: FontStyle.Regular, fontSize: 14 }}>
                {officeName}
              </Text>
              <View style={{ flexDirection: 'row', top: 6 }}>
                <Entypo name="clock" size={18} color={Config.textSecondaryColor}/>
                <Text style={{ fontFamily: FontStyle.Light }}>
                  {' '}{this.state.hours === 0 ? this.state.timeCost.minutes : `${this.state.hours}:${this.state.m}`}
                  {' '}{this.state.hours === 0 ? translate('minutes_spent') : translate('hours_spent')}
                </Text>
              </View>
            </View>

            {/* Cost and Currency Symbol */}
            <View
              style={{
                width: '30%',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingRight: 30,
              }}>
              <Text
                style={{
                  marginRight: 5,
                  color: Config.primaryColor,
                  fontFamily: FontStyle.Bold,
                  fontSize: 18,
                }}>
                {this.state.timeCost.totalCost}
              </Text>
              <View style={{
                height: 18,
                width: 18,
                borderWidth: 2,
                borderColor: Config.primaryColor,
                borderRadius: 50,
                alignItems: 'center',
              }}>
                {this.state.userData.currency === 1
                  ? <Text style={{ color: Config.primaryColor, bottom: 2.5 }}>$</Text>
                  : <Text style={{ color: Config.primaryColor, bottom: 2.5 }}>¥</Text>
                }
              </View>
            </View>
          </View>

          {/* Body */}
          <View style={{ marginTop: 50, alignItems: 'center' }}>
            <Text style={{ fontFamily: FontStyle.Bold, fontSize: 14, marginTop: 30 }}>
                {translate('give_us_your_thoughts')}
            </Text>
          </View>
          <View
            style={{
              width: width / 1.25,
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: 25,
            }}>
              <TouchableOpacity
                  style={styles.starsStyle}
                  onPress={() => this.oneStar()}
              >
                  <FontAwesome
                      name="star"
                      size={25}
                      color={
                          this.state.oneStar === false ? Config.lightColor : Config.primaryColor
                      }
                  />
              </TouchableOpacity>
              <TouchableOpacity
                  style={styles.starsStyle}
                  onPress={() => this.twoStar()}
              >
                  <FontAwesome
                      name="star"
                      size={25}
                      color={
                          this.state.twoStar === false ? Config.lightColor : Config.primaryColor
                      }
                  />
              </TouchableOpacity>
              <TouchableOpacity
                  style={styles.starsStyle}
                  onPress={() => this.threeStar()}
              >
                  <FontAwesome
                      name="star"
                      size={25}
                      color={
                          this.state.threeStar === false ? Config.lightColor : Config.primaryColor
                      }
                  />
              </TouchableOpacity>
              <TouchableOpacity
                  style={styles.starsStyle}
                  onPress={() => this.fourStar()}
              >
                  <FontAwesome
                      name="star"
                      size={25}
                      color={
                          this.state.fourStar === false ? Config.lightColor : Config.primaryColor
                      }
                  />
              </TouchableOpacity>
              <TouchableOpacity
                  style={styles.starsStyle}
                  onPress={() => this.fiveStar()}
              >
                  <FontAwesome
                      name="star"
                      size={25}
                      color={
                          this.state.fiveStar === false ? Config.lightColor : Config.primaryColor
                      }/>
              </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 25,
              borderWidth: 1,
              width: width / 1.25,
              alignSelf: 'center',
              borderColor: Config.lineColor,
            }}>
            <TextInput
              style={styles.inputStyle}
              placeholder={translate('comment')}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="default"
              underlineColorAndroid="transparent"
              onChangeText={(comment) => this.setState({ comment })}
              value={this.state.comment}
            />
          </View>

          {/* Submit Button */}
          <TouchableOpacity
            style={{
              marginTop: 45,
              width: width / 1.25,
              backgroundColor: Config.primaryColor,
              height: 45,
              alignSelf: 'center',
              borderRadius: 2,
            }}
            onPress={() => this.submitRating()}
          >
            {this.state.loading === true && (
              <ActivityIndicator style={{ top: 13 }} color="#fff" />
            )}
            {this.state.loading === false && (
            <Text
              style={{
                textAlign: 'center',
                fontFamily: FontStyle.Regular,
                top: 13,
                color: Config.white,
              }}>
                {translate('submit')}
              </Text>
            )}
          </TouchableOpacity>
        </View>
      );
    }
}

const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: Config.white,
  },
  header: {
    width: '100%',
    height: width / 6,
    top: width / 16,
    paddingLeft: 8,
    paddingRight: 8,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  starsStyle: {
    marginLeft: 35,
  },
  inputStyle: {
    width: width / 1.28,
    right: 8,
    height: 40,
    marginLeft: 10,
    color: '#000000',
  },
};

function mapStateToProps(state) {
  return {
    component: state.component,
    userData: state.auth.userData,
    sharedOfficeData: state.auth.sharedOffice,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
    sharedOffice: (data) => dispatch({
      type: 'SHARED_OFFICE',
      data,
    }),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SharedOfficeReview);
