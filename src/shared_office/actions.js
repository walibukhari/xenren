const actions = {

  FETCH_OFFICE_LAYOUT_REQUESTED: 'FetchOfficeLayout',
  FETCH_OFFICE_LAYOUT_SUCCEED: 'FecthOfficeLayoutSucceed',
  SELECT_OFFICE: 'SharedOffice/SelectOffice',
  SELECT_SEAT: 'SharedOffice/SelectSeat',
  SELECT_SEAT_POST: 'SharedOffice/PostSelectedSeat',
  SELECT_SEAT_SUCCEED: 'SharedOffice/SelectSeatSucceed',
  SELECT_SEAT_FAILED: 'SharedOffice/SelectSeatFailed',
  START_TIME: 'SharedOffice/StartTime',
  STOP_TIME: 'SharedOffice/StopTime',
  SHAREDOFFICE_STOP_TIME: 'SharedOffice/sharedofficeStopTimer',
  SELECT_QR_SEAT: 'SharedOffice/SelectQRSeatAction',

  saveOfficeLayout: (id, layout) => ({
    type: actions.FETCH_OFFICE_LAYOUT_SUCCEED,
    payload: {
      id,
      layout,
    },
  }),
  selectOffice: (id) => ({
    type: actions.SELECT_OFFICE,
    payload: {
      id,
    },
  }),
  selectSeat: (id) => ({
    type: actions.SELECT_SEAT,
    payload: {
      id,
    },
  }),

  selectQRSeat: (seatNumber) => ({
    type: actions.SELECT_QR_SEAT,
    payload: {
      seatNumber,
    },
  }),

  postSelectedSeat: (id) => ({
    type: actions.SELECT_SEAT_POST,
    payload: {
      id,
    },
  }),

  selectSeatSucceed: (selectedSeatData) => ({
    type: actions.SELECT_SEAT_SUCCEED,
    payload: {
      selectedSeatData,
    },
  }),

  selectSeatFailed: (msg) => ({
    type: actions.SELECT_SEAT_FAILED,
    payload: {
      msg,
    },
  }),

  sharedOfficeStopTimer: (transactionID) => ({
    type: actions.SHAREDOFFICE_STOP_TIME,
    payload: {
      transactionID,
    },
  }),

  startTime: (timeStarted) => ({
    type: actions.START_TIME,
    payload: {
      timeStarted,
    },
  }),

  stopTime: (timeStopped) => ({
    type: actions.STOP_TIME,
    payload: {
      timeStopped,
    },
  }),
};

export default actions;
