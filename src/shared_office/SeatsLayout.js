/* eslint-disable no-unused-vars */
/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Text,
  TouchableWithoutFeedback,
  View,
  Dimensions,
  StyleSheet, FlatList, TouchableOpacity
} from 'react-native';
import Config from '../Config';

export class Seat extends React.PureComponent {
  static propTypes = {
    selected: PropTypes.bool,
    busy: PropTypes.bool.isRequired,
    status: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
  };

  static defaultProps = {
    selected: false,
  };


  render() {
    const chairNumberStyle = {
      position: 'absolute',
      top: 0,
      width: this.props.selected ? 48 : 30,
      height: this.props.selected ? 56 : 35,
      alignItems: 'center',
      justifyContent: 'center',
    };
    if (this.props.busy) {
      return (
        <View>
          <Image
            source={require('../../images/select_seat/icon_chair_red.png')}
            style={{ width: 30, height: 50 }}
            resizeMode='contain' />
          <View style={chairNumberStyle}>
            <Text style={{
              color: 'red',
              backgroundColor: 'transparent',
              fontSize: 12,
            }} numberOfLines={1} ellipsizeMode='tail'>
              {this.props.status}
            </Text>
          </View>
        </View>
      );
    } else {
      let path = '';
      const name = this.props.name.replace(/[^A-Za-z]/g, '').replace(/([A-Z])/g, ' $1').trim();
      if (name === 'Hot Desk') {
        path = require('../../images/select_seat/icon_chair.png');
      } else if (name === 'Dedicated Desk') {
        path = require('../../images/select_seat/dedicated_desk.png');
      } else if (name === 'Meeting Room') {
        path = require('../../images/select_seat/meeting_room.png');
      } else if (name === 'Vending Machine') {
        path = require('../../images/select_seat/vendor_machine.png');
      } else if (name === 'Coffee Machine') {
        path = require('../../images/select_seat/coffee_machine.png');
      } else if (name === 'Dining Room') {
        path = require('../../images/select_seat/dining_room.png');
      } else if (name === 'Phone Room') {
        path = require('../../images/select_seat/phone_room.png');
      } else if (name === 'Door') {
        path = require('../../images/select_seat/door.png');
      } else if (name === 'Table') {
        path = require('../../images/select_seat/table.png');
      } else if (name === 'V R Room') {
        path = require('../../images/select_seat/vr_room.png');
      } else {
        path = require('../../images/select_seat/wall.png');
      }
      return (
          <View>
                <Image  style={{
                  flex:1,
                  height:40,
                  width:100,
                  marginTop:10
                }}
                    source={path}
                    resizeMode='contain'/>
          </View>
      );
    }
  }
}

const SeatStyle = new StyleSheet.create({
  container: {
    width:'100%',
    borderColor: '#fff',
    display: 'flex',
    flexGrow:10,
    flexDirection:'row',
    alignItems: 'stretch'
  },
})
class SeatsLayout extends React.Component {
  static propTypes = {
    layout: PropTypes.array.isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    onSeatSelected: PropTypes.func,
  };
  static defaultItemSize = 65;

  constructor(props) {
    super(props);
    this.state = {
      selected: null,
    };
  }

  _onPressSeat = (number,id) => {
    this.setState({ selected: number }, () => {
      this.props.onSeatSelected(number);
    });
  };


  render() {
    const containerStyle = {
      width: "100%",
      height: "100%",
    };
    const columns = 3;
    return (
      <View>
        <FlatList
            numColumns={columns}
            data={this.props.layout}
            renderItem={({item, id}) => {
              return(
                  <TouchableOpacity onPress={() => this._onPressSeat(item.extra.number,item.id)}
                  >
                    <Seat
                      selected={this.state.selected === item.extra.number}
                      busy={item.extra.busy} id={item.id} status={item.extra.number} name={item.extra.name}
                    />
                  </TouchableOpacity>
              )
            }}
        />
      </View>
    );
  }
}

export default SeatsLayout;
