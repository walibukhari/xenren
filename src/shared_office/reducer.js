import { Map } from 'immutable';
import actions from './actions';

const initialState = new Map({
  offices: Map({}),
  selected_office: null,
  selected_seat: null,
  tracker: Map({
    started: false,
    startTime: null,
    stopTime: null,
  }),
  selectedSeatData: null,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCH_OFFICE_LAYOUT_SUCCEED:
      return state.setIn(
        ['offices', 'layout'],
        action.payload.layout,
      );
    case actions.SELECT_OFFICE:
      return state.set('selected_office', action.payload.id);
    case actions.SELECT_SEAT:
      // TODO : check the time tracker and stop if started, reset if not
      return state.set('selected_seat', action.payload.id);
    case actions.START_TIME:
      return state
        .setIn(['tracker', 'started'], true)
        .setIn(['tracker', 'startTime'], new Date())
        .setIn(['tracker', 'startTime'], new Date());

    case actions.STOP_TIME:
      return state
        .setIn(['tracker', 'started'], false)
        .setIn(['tracker', 'startTime'], null)
        .setIn(['tracker', 'startTime'], null);

    case actions.SELECT_SEAT_SUCCEED:
      return state.set('selectedSeatData', action.payload.selectedSeatData);
    default:
      return state;
  }
};
