/* eslint-disable no-mixed-operators */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable camelcase */
/* eslint-disable no-undef */
/* eslint-disable prefer-const */
/* eslint-disable no-unneeded-ternary */
/* eslint-disable react/prop-types */
/* eslint-disable import/first */
/* eslint-disable no-unused-vars */
/* eslint-disable arrow-parens */
/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator, Dimensions,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { RNCamera } from 'react-native-camera';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';
import TimerComponent from '../screens/TimerComponent';
import FooterTabs from '../screens/FooterTabs';
import VerificationTopup from '../screens/verifyId/VerificationTopup';
import FontStyle from '../constants/FontStyle';
import { Height } from '../utilities/dimensions';

const { width } = Dimensions.get('window');

class Legends extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Xenren CO-Work Space',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
          fontWeight: 'bold',
        },
      headerRight: (
        <TouchableOpacity
          onPress={() => {
            console.log('notification pressed!');
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <MaterialCommunityIcons size={16} name="bell-ring"
            color={Config.topNavigation.headerIconColor} />
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
            color={Config.topNavigation.headerIconColor}/>
      </TouchableOpacity>),
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    // isTrackerStarted: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isSeatChanged: false,
      isScanned: true,
      cameraType: RNCamera.Constants.Type.back,
      qrType: '',
      qrcode: '',
      isLoading: false,
      currency: '',
      totalAmountSpent: '',
      totalTimeSpent: '',
      isTimer: true,
      buttons: [
        {
          active: true,
          middle: false,
          icon: require('../../images/notification/icon_list_message.png'),

          topLabel: 'Message',
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/notification/icon_bell.png'),

          topLabel: 'Notification',
        },
        {
          active: false,
          middle: true,
          icon: require('../../images/share_office/icon_xenren.png'),

          topLabel: 'Menu',
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/notification/icon_bell.png'),

          topLabel: '消息',
        },
        {
          active: false,
          middle: false,
          icon: require('../../images/notification/icon_list_message.png'),

          topLabel: '我的',
        },
      ],
      activeIndex: 0,
      modalVisible: false,
      changeSeatModel: true,
      barCodeScanned: true,
    };

    this.root = this.props.component.root;
  }

  componentDidMount() {
    this.setState({
      isLoading: true,
    });
    this.meAPI();
  }

  meAPI() {
    HttpRequest.me(this.props.userData.token)
      .then((response) => {
        const result = response.data;
        if (result.status === 'success') {
          if (result.shared_office !== null) {
            this.props.sharedOffice(result.shared_office);
          }
          this.props.userLogin(result.data, this.props.userData.token);
          this.setState({
            isLoading: false,
            totalAmountSpent: result.time_price,
            totalTimeSpent: result.minutes,
            currency: result.data.currency ? result.data.currency : 0,
            seat_No: result.seat_number,
            seat_cost: result.shared_office.productfinance.time_price,
          });
        } else {
          alert(result.status);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  changeSeat = () => {
    if (this.props.qrCode === '') {
      alert(translate('pick_directly'));
      this.props.navigation.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Dashboard',
          }),
        ],
      }));
    } else {
      this.props.navigation.navigate('SelectSeat', {
        qr: this.props.qrCode,
        changeSeat: true,
      });
    }
  };

  onSelectSeat = (text) => {
    if (this.state.barCodeScanned) {
      this.setState({ barCodeScanned: false });
      if (text.includes('|')) {
        const data = text.split('|');
        const seatQrcode = data[0];
        const seat = seatQrcode.split(':');
        const officeQrcode = data[1];
        const office = officeQrcode.split(':');
        if (this.state.isScanned === true) {
          this.setState({ isScanned: false }, () => {
            this.seatByQR(seat[1], office[1]);
          });
        }
      } else {
        alert(translate('scan_valid_qr_code'));
      }
    }
  };

  seatByQR = (seatId, office_id) => {
    const { state } = this.props.navigation;
    let data = {
      office_id,
      qr_seat: seatId,
    };
    HttpRequest.changeSeatByQR(this.props.userData.token, data)
      .then((response) => {
        if (response.data.status === 'error') {
          alert(response.data.message);
        } else if (response.data.status === 'success') {
          this.setState({ isSeatChanged: true });
          if (state.params !== undefined) {
            alert(`${translate('time_spent_on_previous_seat')} ${state.params.time}\n${translate('amount_spent_on_previous_seat')}: ${state.params.amount} ${state.params.currency}`);
          }
          this.meAPI();
        } else if (response.data.status === 'failure') {
          alert(response.data.message);
        } else {
          alert(translate('network_error'));
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Dashboard',
              }),
            ],
          }));
        }
        this.setState({ isScanned: true });
      })
      .catch(error => {
        this.setState({ isScanned: true });
        alert(error);
      });
  };

  closeTimer = (data) => {
    if (data === 'yes') {
      this.stopTimerAPI();
    } else {
      this.setState({ showModal: false });
    }
  };

  stopTimerAPI() {
    HttpRequest.stopTimer(this.props.userData.token)
      .then((response) => {
        if (response.data.status === 'success') {
          alert(translate('successfully_payment'));
          let data = this.props.userData;
          data.seat_number = null;
          this.props.userLogin(data, this.props.userData.token);
          this.props.sharedOffice(null);

          this.setState({
            showModal: false,
            isTimer: false,
          });
          this.props.navigation.navigate('Root');
        } else if (response.data.status === 'error') {
          alert(response.data.message);
        } else if (response.data.status === 'failure') {
          alert(response.data.message);
        } else {
          alert(response.data.message);
        }
      })
      .catch((error) => {
        alert(error);
      });
  }

  stopTimer = () => {
    this.setState({ showModal: true });
  };

  renderSeatNumberSize = () => {
    const { seat_no } = this.state;

    if (seat_no <= 99) { return 12; }
    return 8;
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        {
          this.state.activeIndex !== 0
          && <View style={styles.navigationStyle}>
            <View style={styles.navigationTextStyle}>
              <Text style={{ fontSize: 18 }}>
                {this.state.buttons[this.state.activeIndex].topLabel}
              </Text>
            </View>
          </View>
        }
        <View style={styles.bodyStyle}>
          <ScrollView>
          {
            this.state.isSeatChanged === true && <VerificationTopup
              showModal={this.state.changeSeatModel}
              type={'success'}
              title={'Success'}
              message={translate('seat_changed_successfully')}
              closeModal={() => {
                this.setState({ changeSeatModel: false });
              }}
              onOK={() => {}}
            />
          }

          <View style={[styles.qrWrapperStyle, {
            overflow: 'hidden',
            backgroundColor: '#FFF',
            borderRadius: 12.5,
            marginTop: width / 4 - 70,
            borderWidth: 1,
            borderColor: 'transparent',
          }]}>
            <RNCamera style={styles.camera} type={this.state.cameraType}
                      onBarCodeRead={(e) => this.onSelectSeat(e.data)} captureAudio={false}>

              <View style={styles.rectangle}>
                <View
                  style={[
                    styles.cameraEdges,
                    {
                      top: 0,
                      left: 28,
                      borderLeftWidth: 4,
                      borderTopWidth: 4,
                      borderTopLeftRadius: 12.5,
                    },
                  ]}
                />
                <View
                  style={[
                    styles.cameraEdges,
                    {
                      top: 0,
                      right: 28,
                      borderRightWidth: 4,
                      borderTopWidth: 4,
                      borderTopRightRadius: 12.5,
                    },
                  ]}
                />
                <View
                  style={[
                    styles.cameraEdges,
                    {
                      bottom: 0,
                      left: 28,
                      borderLeftWidth: 4,
                      borderBottomWidth: 4,
                      borderBottomLeftRadius: 12.5,
                    },
                  ]}
                />
                <View
                  style={[
                    styles.cameraEdges,
                    {
                      bottom: 0,
                      right: 28,
                      borderRightWidth: 4,
                      borderBottomWidth: 4,
                      borderBottomRightRadius: 12.5,
                    },
                  ]}
                />
              </View>
            </RNCamera>
          </View>

          <TouchableOpacity style={[styles.buttonFullBackgroundStyle, { top: 5, zIndex: 1 }]}
                            onPress={this.changeSeat}>
            <Text style={{
              color: '#fff',
              fontSize: 15,
              fontFamily: FontStyle.Bold,
            }}>{translate('CHANGE_SEAT')}</Text>
          </TouchableOpacity>

          {this.state.isLoading
            ? <ActivityIndicator/>
            : <View style={{
              width: width - 55,
              marginVertical: 25,
              marginHorizontal: 20,
              flexDirection: 'row',
            }}>

            <View style={styles.moneyTabStyle}>
              <Text style={{
                color: '#A7AAAA',
                fontSize: 14,
                fontFamily: 'Montserrat-Medium',
              }}>{translate('MONEY_SPENT')}</Text>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 25,
              }}>
                <Text style={{
                  color: Config.primaryColor,
                  fontSize: 25,
                  fontFamily: 'Montserrat-Bold',
                }}>{this.state.totalAmountSpent} </Text>
                <View style={{
                  width: 22,
                  height: 22,
                  borderRadius: 11,
                  // marginTop: 25,
                  borderWidth: 3,
                  borderColor: Config.primaryColor,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                  <Text style={{
                    color: Config.primaryColor,
                    fontSize: 17,
                    fontFamily: 'Montserrat-Bold',
                  }}>{this.state.currency === 1 ? '$' : '¥'}</Text>
                </View>
              </View>
            </View>

            <View style={{
              width: width / 6,
              height: width / 2 - 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
              <View
                style={{
                  height: 145,
                  marginTop: -10,
                  borderLeftColor: 'grey',
                  borderLeftWidth: 0.6,
                }}
              />
              <View style={{
                width: 50,
                height: 50,
                position: 'absolute',
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 60,
                borderColor: 'grey',
                borderWidth: 0.6,
              }}>
                <Image
                  style={{
                    width: 30,
                    height: 30,
                  }}
                  resizeMode='contain'
                  source={require('../../images/select_seat/icon_chair.png')}
                />
                <View style={{
                  position: 'absolute',
                  top: 7,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: Config.primaryColor,
                      fontSize: this.renderSeatNumberSize(),
                      fontFamily: 'Montserrat-Bold',
                      width: 35,
                      paddingVertical: 5,
                    }}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                  >
                    {this.state.seat_No}
                  </Text>
                </View>
              </View>
              {/* <View style={{ */}
                {/* width: 2, */}
                {/* height: 25, */}
                {/* backgroundColor: Config.lineColor, */}
              {/* }}/> */}
            </View>

            <View style={styles.moneyTabStyle}>
              <Text style={{
                color: '#A7AAAA',
                fontSize: 14,
                fontFamily: 'Montserrat-Medium',
              }}>{translate('TIME_SPENT')}</Text>
              <Text style={{
                color: Config.primaryColor,
                fontSize: 25,
                fontFamily: 'Montserrat-Bold',
                marginTop: 25,
              }}>
                {this.state.totalTimeSpent} <Text style={{
                  color: Config.primaryColor,
                  fontSize: 23,
                  fontWeight: '100',
                }}>{translate('min')}</Text>
              </Text>
            </View>
          </View>}

          <TouchableOpacity style={styles.buttonOutlineBackgroundStyle} onPress={() => {
            this.stopTimer();
          }}>
            <Text style={{
              color: Config.primaryColor,
              fontSize: 15,
              fontFamily: 'Montserrat-Bold',
            }}>{this.state.isTimer === true ? translate('STOP') : translate('timer_stop')}</Text>
          </TouchableOpacity>
          </ScrollView>
        </View>

        {this.state.showModal === true
        && <TimerComponent timerCallback={this.closeTimer} value={true}
                        totalTime={this.state.totalTimeSpent} timeType={this.state.timeType}
                        totalAmount={this.state.totalAmountSpent}
                        currency={this.state.currency === 1 ? '$' : '¥'}
                        seat={this.state.seat_No}/>}
        <FooterTabs navigation={this.props.navigation} currentIndex={2}/>

      </View>
    );
  }
}

const styles = {
  rootStyle: {
    backgroundColor: '#f7f7f7',
    flex: 1,
    flexDirection: 'column',
    paddingHorizontal: 10,
  },

  containerStyle: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },

  bodyStyle: {
    flex: 1,
    backgroundColor: '#fff',
    marginBottom: width / 4 - 80,
    justifyContent: 'center',
    alignItems: 'center',
  },

  qrWrapperStyle: {
    width: width - 55,
    height: width - 60,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
  },

  buttonFullBackgroundStyle: {
    width: width - 55,
    height: 38,
    marginHorizontal: 20,
    backgroundColor: Config.primaryColor,
    borderRadius: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonOutlineBackgroundStyle: {
    width: width - 55,
    height: width / 8,
    bottom: width / 16,
    marginHorizontal: 20,
    borderWidth: 2,
    borderRadius: 1,
    borderColor: Config.primaryColor,
    justifyContent: 'center',
    alignItems: 'center',
  },

  moneyTabStyle: {
    width: width / 2 - 57,
    height: width / 2 - 30,
    bottom: width / 26,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    flexDirection: 'column',
  },

  navigationStyle: {
    height: 50,
    flexDirection: 'row',
    marginTop: 20,
    borderBottomColor: Config.lineColor,
    borderBottomWidth: 1,
  },

  navigationButtonStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },

  navigationTextStyle: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },

  footerMenuStyle: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    height: 65,
    alignItems: 'flex-end',
  },

  footerButtonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    flexDirection: 'column',
  },

  footerMiddleButtonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    flexDirection: 'column',
  },

  footerTextStyle: {
    fontSize: 11,
    marginTop: 5,
    fontFamily: 'Montserrat-Light',
  },

  camera: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: 'red',
    // borderRadius: 15,
  },

  rectangle: {
    width,
    height: '100%',
    borderColor: '#999',
    borderWidth: 0.1,
    backgroundColor: 'transparent',
    borderRadius: 10,
  },

  cameraEdges: {
    position: 'absolute',
    borderColor: Config.primaryColor,
    width: Height(7),
    height: Height(7),
  },

  modalBoxStyle: {
    width: '85%',
    height: 340,
    borderRadius: 5,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  modalHeaderWrapper: {
    paddingHorizontal: 20,
    width: '90%',
    height: 290,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    borderRadius: 8,
  },

  modalContainerStyle: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

const mapStateToProps = (state) => ({
  component: state.component,
  // office: state.sharedOffice.office, // Probably not useful
  // seat: state.sharedOffice.seat,
  // isTrackerStarted: state.sharedOffice.getIn(['tracker', 'started']),
  // selectedSeatData: state.sharedOffice.get('selectedSeatData'),
  // qrCode: state.auth.qrCode,
  userData: state.auth.userData,
  sharedOfficeData: state.auth.sharedOffice,
  // mySelectedSeatData: state.auth.selectedSeatData,
});

const mapDispatchToProps = (dispatch) => ({
  // setRoot: root => dispatch({
  //   type: 'set_root',
  //   root,
  // }),
  userLogin: (userData, token) => dispatch({
    type: 'LOGIN_SUCCESS',
    data: {
      ...userData,
      token,
    },
  }),
  sharedOffice: (data) => dispatch({
    type: 'SHARED_OFFICE',
    data,
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Legends);
