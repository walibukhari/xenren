import React from 'react';
import { 
    View,
    Text,
    Image,
    StyleSheet,
    PixelRatio
} from 'react-native'
import Config from '../Config';



const PickSeatJumbotron = ({ data, duration }) => { 
    let { product, fee, icon } = data;
    let pricing = 0;
    switch (duration.indicator) { 
        case 'monthly':
            pricing = fee.price_monthly;
            break;
        case 'weekly':
            pricing = fee.price_weekly;
            break;
        case 'daily':
            pricing = fee.price_daily;
        default:
            pricing = fee.price_hourly;
    }
   
    return (
        <View style={[PickSeatJumbotronStyle.container]}>
            <View style={{
                width: 60,
                height: 60,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}>
                <Image source={icon}
                    style={{ width: 60, height: 60 }}
                    resizeMode='contain'
                    tintColor={'#fff'} />
            </View>
            <View style={PickSeatJumbotronStyle.primaryDescriptionContainer}>
                <View style={PickSeatJumbotronStyle.primaryDescription}>
                    <Text style={[PickSeatJumbotronStyle.fontWhite, PickSeatJumbotronStyle.fontNormal]}>$</Text>
                    <Text style={[PickSeatJumbotronStyle.fontWhite, PickSeatJumbotronStyle.fontsPirmary, { alignSelf: 'stretch' }]}>{(pricing || 0).toFixed(2)}</Text>
                    <Text style={[PickSeatJumbotronStyle.fontWhite, PickSeatJumbotronStyle.fontNormal, { alignSelf: 'flex-end' }]}>/{duration.short_name}</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={[PickSeatJumbotronStyle.fontWhite, PickSeatJumbotronStyle.fontNormal, { textAlign: 'center' }]}>{product.product_name}</Text>
                </View>
            </View>
        </View>
    );
}

const PickSeatJumbotronStyle = new StyleSheet.create({
    container: {
        flexBasis: "100%",
        flex: 1,
        paddingHorizontal: 50,
        flexDirection: "row",
        backgroundColor: Config.primaryColor,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
    },
    fontWhite: {
        color: '#fff'
    },
    fontsPirmary: {
        fontSize: 50,
        fontWeight: "bold"
    },
    primaryDescriptionContainer: {
        flexDirection: 'column'
    },
    primaryDescription: {
        flexDirection: 'row'
    },
    fontNormal: {
        fontSize: 18
    }
})


export default PickSeatJumbotron;
