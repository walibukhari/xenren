/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions, Image, Alert } from 'react-native';
import Config from '../Config';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

const { width } = Dimensions.get('window');

class SeatSelectSuccess extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: '',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <FontAwesomeIcons
              size={26}
              name="chevron-left"
              color={Config.primaryColor}
              fa5Style={FA5Style.regular}
          />
        </TouchableOpacity>,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      icon: '',
      data: this.props.navigation.state.params.data,
      scan_type: 0
    };
  }

  componentDidMount() {
    this.setState({
      icon: this.props.navigation.state.params.data.icon,
    });
    console.log("scan_type in  seatSelectScreen.js");
    console.log(this.props.navigation.state.params);
    console.log(this.props.navigation.state.params.data);
    console.log(this.props.navigation.state.params.data.scan_type);
    this.setState({
      icon: this.props.navigation.state.params.data.scan_type,
    });
    console.log(this.state.scan_type);
  }

  static propTypes = {
    layout: PropTypes.array.isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    onSeatSelected: PropTypes.func,
    navigation: PropTypes.object.isRequired,
  };

  render() {
    return (
      <View onLayout={this.onLayout} style={style.containerStyle}>
        <View style={style.uppperContainer}>
          <View style={style.seat}>
            <Image
                source={{ uri: this.state.icon }}
                style={{ height: 80, width: 80 }}
                resizeMode='contain'
            />
          </View>
          <Text style={style.title}>{this.state.data.data.type.replace(/[^A-Za-z]/g, '').replace(/([A-Z])/g, ' $1').trim()} Picked</Text>
          <Text style={style.subtitle}>Your {this.state.data.data.type.replace(/[^A-Za-z]/g, '').replace(/([A-Z])/g, ' $1').trim()} picked successfully</Text>
        </View>
        <TouchableOpacity style={style.okButton} onPress={() => {
          console.log(this.props.navigation.state.params.data.scan_type);
          if(this.state.scan_type == 1) {
            this.props.navigation.navigate('SeatStatus');
          } else {
            this.props.navigation.navigate("MySpaces");
          }
        }}>
          <Text style={style.okText}>
            {translate('ok')}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  okButton: {
    height: 45,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: width - 40,
    marginBottom: 20,
  },

  okText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
  },

  seat: {
    backgroundColor: Config.primaryColor,
    height: 150,
    width: 150,
    borderRadius: 75,
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },

  uppperContainer: {
    flex: 1,
    alignItems: 'center',
  },

  title: {
    fontSize: 25,
    fontWeight: '800',
    textAlign: 'center',
    marginTop: 20,
  },

  subtitle: {
    fontSize: 18,
    color: 'grey',
    textAlign: 'center',
    marginTop: 10,
  },
});

export default SeatSelectSuccess;
