/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
import { NavigationActions } from 'react-navigation';
import { call, takeLatest, put } from 'redux-saga/effects';
import Api from '../services/api';
import actions from './actions';


function* fetchOfficeLayout(action) {
  const layout = yield call(Api.getOfficeLayout, action.payload.id);
  // if (layout.data[0].success === false) {
  yield put(actions.saveOfficeLayout(action.payload.id, layout));
  // } else {
  //   yield put(actions.saveOfficeLayout(action.payload.id, layout));
  // }
}

function* selectSeat(action) {
  const seatData = yield call(Api.selectSeatAndStartTimer, action.payload.id);
  if (seatData.data[0].success === true) {
    yield put(actions.selectSeatSucceed(seatData.data[0]));
    yield put(actions.startTime(true));
    yield put(NavigationActions.navigate({ routeName: 'SeatSelectSuccess' }));
  } else {
    yield put(NavigationActions.navigate({ routeName: 'SeatSelectSuccess' }));
    // alert(seatData.data[0].msg);
  }
}

function* selectQRSeat(action) {
  const seatData = yield call(Api.selectSeatFromQRToStartTimer, action.payload.seatNumber);
  if (seatData.data[0].success === true) {
    yield put(actions.selectSeatSucceed(seatData.data[0]));
    yield put(actions.startTime(true));
    yield put(NavigationActions.navigate({ routeName: 'SeatSelectSuccess' }));
  } else {
    alert(seatData.data[0].msg);
  }
}

function* stopTimer(action) {
  // const seatData = yield call(Api.stopTimer, action.payload.transactionID);
  const seatData = yield call(Api.stopTimer);
  if (seatData.data[0].status === 'success') {
    // yield put(actions.stopTime(true));
  } else {
    alert(seatData.data[0].msg);
  }
}

export default function* sharedOfficeSaga() {
  yield takeLatest(actions.FETCH_OFFICE_LAYOUT_REQUESTED, fetchOfficeLayout);
  yield takeLatest(actions.SELECT_SEAT_POST, selectSeat);
  yield takeLatest(actions.SHAREDOFFICE_STOP_TIME, stopTimer);
  yield takeLatest(actions.SELECT_QR_SEAT, selectQRSeat);
}
