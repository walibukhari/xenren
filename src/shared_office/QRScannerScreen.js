import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Image,
  Platform,
  Modal,
} from 'react-native';
import { Button } from 'react-native-elements';
import axios from 'axios';

import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { translate } from '../i18n';
import actions from './actions';
import HttpRequest from '../components/HttpRequest';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

const icLogo = require('../../images/login/ticklogoc.png');
const icMyspace = require('../../images/myspace.png');

const defWeather = {
  city: '-',
  weatherDesc: '-',
  weatherTemp: 0,
};

class QRScanner extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: translate('scan_qr'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerStyle: {
        backgroundColor: '#000000',
      },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <FontAwesomeIcons
              size={26}
              name="chevron-left"
              color={Config.primaryColor}
              fa5Style={FA5Style.regular}
          />
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      cameraType: RNCamera.Constants.Type.back,
      qrcode: '',
      isScanned: true,
      showType1Success: false,
      isLoading: false,
      scannerOfficeName: '',
      barCodeScanned: true,
      ip4:'',
      weather: {
        city: 'searching...',
        weatherDesc: 'searching...',
        weatherTemp: 'searching...',
      },
    };

    this.onSelectSeat = this.onSelectSeat.bind(this);
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
    selectOffice: PropTypes.func.isRequired,
    selectedSeatData: PropTypes.func.isRequired,
    selectSeat: PropTypes.func.isRequired,
    onSelectQRSeat: PropTypes.func.isRequired,
  };

  // eslint-disable-next-line class-methods-use-this
  componentWillUnmount() {
    this.setState({ barCodeScanned: true });
  }

  componentDidMount() {
    this.getLocation();
  }

  getGeoInfo = () => {
    const access_token = this.props.userData.token;
    axios.get(Config.getIpAddress, {
      headers: {
        Accept: 'application/json',
      },
    }).then((response) => {
      console.log('response');
      console.log(response);
      this.getLocationIp(response.data.ip);
      this.setState({ ip4: response.data.ip });
    })
    .catch((error) => {
          alert(translate('network_error'));
          this.setState({ isLoading: false });
        });
  };

  getLocationIp(ip){
    HttpRequest.getLocation(this.props.userData.token,ip)
    .then((response) => {
      this.getWeather(response.data.lat, response.data.lng);
    })
    .catch((error) => {
      console.log(error);
      this.getLocation();
    });
  }

  getLocation = () => {
    // Get the current position of the user
    // eslint-disable-next-line no-undef
    navigator.geolocation.getCurrentPosition(
      (position) => {
        const { latitude, longitude } = position.coords;
        this.getWeather(latitude, longitude);
      },
      // eslint-disable-next-line no-unused-vars
      (error) => {
        this.getGeoInfo();
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  };

  getWeather(latitude, longitude) {
    const weather = defWeather;

    // Construct the API url to call
    const appid = '42b82644be147ce7aa2cd7f919139010';
    const url = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${appid}`;

    // Call the API, and set the state of the weather forecast
    axios.get(url)
      .then((response) => {
        const { data } = response;
        weather.city = data.name;
        weather.weatherDesc = data.weather[0].description;
        weather.weatherTemp = data.main.temp;
        this.setState({ weather });
      })
      .catch(() => {
        this.setState({ weather });
      });
  }

  showAlert = (msg) => {
    alert(msg); // ToDo: change the alert to custom alert
  };

  selectSeat = (officeId, seatNumber) => {
    HttpRequest.pickSeatByQRCode(this.props.userData.token, seatNumber, officeId)
      .then((response) => {
        if (response.data.status === 'success') {
          const { data } = response.data;
          data.changeSeat = false;
          this.props.selectedSeatData(data);
          this.props.navigation.navigate('SeatSelectSuccess', { data: response.data });
          this.setState({ isLoading: false });
          // this.setState({ barCodeScanned: true });
        } else if (response.data.status === 'error') {
          this.showAlert(response.data.message);
          this.setState({ isLoading: false });
          // this.setState({ barCodeScanned: true });
        } else {
          this.showAlert(translate('network_error'));
          this.setState({ isLoading: false });
        }
      })
      .catch((error) => {
        this.setState({ isLoading: false });
        this.showAlert(error);
      });
  };

  otherTypeBarCodeScan = (officeid, type, category) => {
    const self = this;
    const data = {
      shared_office_id: officeid,
      type,
      category,
    };

    HttpRequest.scanOtherTypeBarCodes(this.props.userData.token, data)
      .then((response) => {
        if (response.data.status === 'error') {
          this.showAlert(response.data.message);
          this.setState({ isLoading: false });
        } else if (response.data.status === 'success') {
          if (type === 1) {
            self.setState({
              showType1Success: true,
              scannedOfficeName: response.data.data.office.office_name,
            });
            this.props.navigation.navigate('Dashboard');
            // this.showAlert('Congratulations, your barcode scanner successfully...!');
          } else if (type === 2) {
            this.props.navigation.navigate('SharedOfficeBarCodeScan', {
              sharedOfficeData: response.data.data.office,
            });
          }
          // this.props.selectedSeatData(data);
          // this.props.navigation.navigate('SeatSelectSuccess', { data: response.data.data });
          this.setState({ isLoading: false });
        } else if (response.data.status === 'failure') {
          this.showAlert(translate('network_error'));
          this.setState({ isLoading: false });
        } else {
          this.showAlert(translate('network_error'));
          this.showAlert(JSON.stringify(response));
          this.setState({
            isScanned: true,
            isLoading: false,
          });
        }
      })
      .catch((error) => {
        this.showAlert(error);
        this.setState({ isLoading: false });
      });
  }

  // eslint-disable-next-line camelcase
  seatByQR = (seatId, office_id) => {
    const { state } = this.props.navigation;
    const data = {
      office_id,
      qr_seat: seatId,
    };
    HttpRequest.changeSeatByQR(this.props.userData.token, data)
      .then((response) => {
        if (response.data.status === 'error') {
          this.showAlert(response.data.message);
          this.setState({ isLoading: false });
        } else if (response.data.status === 'success') {
          const { data } = response.data;
          this.props.selectedSeatData(data);
          this.props.navigation.navigate('SeatSelectSuccess', { data: response.data.data });
          this.setState({ isLoading: false });
          this.showAlert(`${translate('time_spent_on_previous_seat')} ${state.params.time}\n${translate('amount_spent_on_previous_seat')}: ${state.params.amount} ${state.params.currency}`);
        } else if (response.data.status === 'failure') {
          this.showAlert(translate('network_error'));
          this.setState({ isLoading: false });
        } else {
          this.showAlert(translate('network_error'));
          this.showAlert(JSON.stringify(response));
          this.setState({ isScanned: true });
          this.setState({ isLoading: false });
        }
      })
      .catch((error) => {
        this.showAlert(error);
        this.setState({ isLoading: false });
      });
  };

  /** @todo needs refractoring.. */
  onSelectSeat(text) {
    if (this.state.barCodeScanned) {
      this.setState({ isLoading: true, barCodeScanned: false });
      if (text.includes('|')) { // seat scan
        const data = text.split('|');
        const seatQrcode = data[0];
        const seat = seatQrcode.split(':');

        if (data.length === 3) { // 3 TYPE BARCODE SCAN
          if (data[1].split(':')[1] === 3) {
            const data = text.split(':');
            const qrType = data[0] ? data[0] : null; // the type of qr
            const qr = data[1] ? data[1] : null; // the id of the seat or the door
            if (qrType == null || qr == null) {
              this.showAlert(translate('network_error'));
            }
            // Setting qrCode
            if (this.props.navigation.state.params.changeSeat
              && this.props.navigation.state.params.changeSeat === true
            ) {
              this.showAlert(translate('scan_seat_qr_code'));
            } else {
              // eslint-disable-next-line react/prop-types
              this.props.setQRCode(qr);
              if (qrType === 'office') {
                if (this.state.isScanned === true) {
                  if (this.state.isScanned === true) {
                    this.setState({ isScanned: false }, () => {
                      this.props.navigation.navigate('SelectSeat', { qr });
                    });
                  }
                }
              }
            }
          } else {
            this.otherTypeBarCodeScan(data[0].split(':')[1], data[1].split(':')[1], data[2].split(':')[1]);
          }
        } else { // SEAT SCAN
          const officeQrcode = data[1];
          const office = officeQrcode.split(':');

          if (this.props.navigation.state.params.changeSeat === true) {
            this.seatByQR(seat[1], office[1]);
          } else {
            this.selectSeat(office[1], seat[1]);
          }
        }
      } else { // office scan
        const data = text.split(':');
        const qrType = data[0] ? data[0] : null; // the type of qr
        const qr = data[1] ? data[1] : null; // the id of the seat or the door
        if (qrType == null || qr == null) {
          this.showAlert(translate('network_error'));
        }
        // Setting qrCode
        if (this.props.navigation.state.params.changeSeat
          && this.props.navigation.state.params.changeSeat === true
        ) {
          this.showAlert(translate('scan_seat_qr_code'));
        } else {
          // eslint-disable-next-line react/prop-types
          this.props.setQRCode(qr);
          if (qrType === 'office') {
            if (this.state.isScanned === true) {
              if (this.state.isScanned === true) {
                this.setState({ isScanned: false }, () => {
                  this.props.navigation.navigate('SelectSeat', { qr });
                });
              }
            }
          }
        }
      }
    }
  }

  renderActionsComponent = (showSeatSelection = false) => {
    const { city, weatherDesc, weatherTemp } = this.state.weather;
    return (
      <View style={actionsComponentStyle.componentContainer}>
        <View style={actionsComponentStyle.container}>
          <View style={{ flexDirection: 'column' }}>
            <Text style={actionsComponentStyle.city}>
              {city}
            </Text>
            <Text style={actionsComponentStyle.weather}>
              {weatherDesc}
            </Text>
            <Text style={actionsComponentStyle.temp}>
              {weatherTemp} °
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              console.log('goto myspaces');
              this.props.navigation.navigate('MySpaces');
            }}
          >
            <Image
              style={{ width: '100%', height: 70 }}
              source={icMyspace}
            />
            <Text style={actionsComponentStyle.myspace}> My Space </Text>
          </TouchableOpacity>
        </View>
        {
          showSeatSelection
            ? <View style={{ alignSelf: 'stretch', marginHorizontal: 20 }}>
              <Button onPress={() => alert('wakekeke')}
                containerViewStyle={{ justifyContent: 'center', alignItems: 'center' }}
                buttonStyle={actionsComponentStyle.pickSeatButton}
                titleStyle={{ fontWeight: 'bold' }}
                title="PICK SEAT" />
            </View>
            : null
        }
      </View>
    );
  };

  render() {
    return (
      <View style={Platform.OS === 'android' ? styles.container : styles.containerIos}>

        <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.showType1Success}
            onRequestClose={() => this.setState({ isShowSuccess: false })}
        >
          <View style={actionsComponentStyle.dialogStyle}>
            <View style={actionsComponentStyle.dialogBoxStyle}>
              <Image
                  source={icLogo}
                  style={{
                    height: 45,
                    top: -30,
                  }}
                  resizeMode="contain"
              />

              <Text style={{
                fontFamily: FontStyle.Light,
                fontSize: 16,
                top: -40,
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 20,
              }}>
                you have joined {this.state.scannerOfficeName} Space Chat Successfully.
                Please follow group rules in order to keep your decent stay.
              </Text>
              <Text style={{
                fontFamily: FontStyle.Bold,
                fontSize: 16,
                top: -20,
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: Config.primaryColor,
                color: 'white',
                borderRadius: 20,
                borderWidth: 1,
                overflow: 'hidden',
                borderColor: Config.primaryColor,
              }} onPress={() => this.setState({ showType1Success: false })}>
                CONTINUE
              </Text>
            </View>
          </View>
        </Modal>

        <RNCamera
          style={styles.camera}
          type={this.state.cameraType}
          onBarCodeRead={(e) => this.onSelectSeat(e.data)}
          captureAudio={false}>
          <View style={styles.rectangleContainer}>
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: Dimensions.get('window').height,
              width: Dimensions.get('window').width,
              borderColor: '#0000004F',
              borderLeftWidth: 65,
              borderRightWidth: 65,
              borderTopWidth: Dimensions.get('window').height / 2 - 100,
              borderBottomWidth: Dimensions.get('window').height / 2 - 100,
              backgroundColor: 'transparent',
            }}>
              <View style={styles.rectangle}>
                <View
                  style={[
                    { borderColor: '#00ff00' },
                    styles.topLeftEdge,
                    {
                      borderLeftWidth: 3,
                      borderTopWidth: 3,
                    },
                  ]}
                />
                <View
                  style={[
                    { borderColor: '#00ff00' },
                    styles.topRightEdge,
                    {
                      borderRightWidth: 3,
                      borderTopWidth: 3,
                    },
                  ]}
                />
                <View
                  style={[
                    { borderColor: '#00ff00' },
                    styles.bottomLeftEdge,
                    {
                      borderLeftWidth: 3,
                      borderBottomWidth: 3,
                    },
                  ]}
                />
                <View
                  style={[
                    { borderColor: '#00ff00' },
                    styles.bottomRightEdge,
                    {
                      borderRightWidth: 3,
                      borderBottomWidth: 3,
                    },
                  ]}
                />
                {this.state.isLoading === true && <ActivityIndicator color='#fff' />}
              </View>
            </View>
          </View>
          { this.renderActionsComponent(true) }
        </RNCamera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'green',
  },

  containerIos: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },

  camera: {
    height: '100%',
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#008000',
    backgroundColor: 'transparent',
  },

  confirmButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: '110%',
    width: '110%',
    borderColor: '#999',
    borderWidth: 0.1,
    backgroundColor: 'transparent',
  },

  topLeftEdge: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 30,
    height: 20,
  },

  topRightEdge: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 30,
    height: 20,
  },

  bottomLeftEdge: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: 30,
    height: 20,
  },

  bottomRightEdge: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 30,
    height: 20,
  },
});

const actionsComponentStyle = StyleSheet.create({
  componentContainer: {
    width: '100%',
    alignItems: 'center',
    flexShrink: 2,
    paddingBottom: 20,
  },
  container: {
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    flexDirection: 'row',
    marginBottom: 10,
  },
  city: {
    fontSize: 15,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'justify',
    color: '#fff',
  },
  weather: {
    fontSize: 10,
    textTransform: 'capitalize',
    color: Config.primaryColor,
  },
  temp: {
    textAlign: 'justify',
    fontSize: 30,
    color: '#fff',
  },
  myspace: {
    width: '100%',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
    justifyContent: 'center',
  },
  pickSeatButton: {
    backgroundColor: Config.primaryColor,
    padding: 15,
    borderRadius: 5,
  },
  dialogStyle: {
    flex: 1,
    backgroundColor: 'rgba(44, 62, 80, 0.6)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  dialogBoxStyle: {
    width: 240,
    height: 220,
    backgroundColor: '#fff',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },

});


const mapStateToProps = (state) => {
  return {
    userData: state.auth.userData,
    sharedOfficeData: state.auth.sharedOffice,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectOffice: (id) => dispatch(actions.selectOffice(id)),
    selectSeat: (id) => dispatch(actions.selectSeat(id)),
    onSelectQRSeat: (seatNumber) => dispatch(actions.selectQRSeat(seatNumber)),
    setQRCode: (code) => dispatch({
      type: 'CURRENT_QRCODE',
      qrCode: code,
    }),
    selectedSeatData: (data) => dispatch({
      type: 'SELECTED_SEAT_DATA',
      selectedSeatData: { ...data },
    }),
    mySelectedSeatData: (data) => dispatch({
      type: 'SELECTED_SEAT_DATA',
      data: { ...data },
    }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(QRScanner);
