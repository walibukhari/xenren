import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
    Text,
    View,
    StyleSheet,
    ActivityIndicator, TouchableOpacity, TextInput, Modal,
} from 'react-native';
import { Button } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import PickSeatJumbotron from './PickSeatJumbotron';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import HttpRequest from "../components/HttpRequest";
import Ionicons from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/dist/FontAwesome";
import moment from "moment";
import {Calendar} from "react-native-calendars";
import _ from "lodash";

const duration = [
    {
        label: 'Hourly',
        indicator: 'hourly',
        short_name: 'hr',
        id: 1
    },
    {
        label: 'Weekly',
        indicator: 'weekly',
        short_name: 'wk',
        id: 2
    }, {
        label: 'Daily',
        indicator: 'daily',
        short_name: 'day',
        id: 3
    }, {
        label: 'Monthly',
        indicator: 'monthly',
        short_name: 'mo',
        id: 4
    }];

class SharedOfficeBarCodeScan extends Component {

    _office_fees = [];
    /**
     * 
     * @todo What if there are no available product on a coworking space? 
     */
    constructor(props) {
        super(props);
        this._c = null;
                
        /**
         *  duration is global
         *  { product, icon, fee, fees }
         *  product object
         *  icon string
         *  fee object
         *  duration object
         *  fees array of fees
         */

        this.state = {
            topupAmount: 0,
            remark: '',
            carouselWidth: 200,
            carouselItemWidth: 200,
            carouselItemHeight: 200,
            loading: true,
            locale: '',
            duration: duration[0],
            carousel_is_loading: true,
            carousel_items: [],
            carousel_active_item_idx: -1, // identify the active index of the carousel
            office_products: [],
            office_categories: [],
            noEnoughBalance: false,
            confirmReservation: false,
            reservation:{
                start_date: '21-sep-2019',
                end_date: '22-sep-2019',
                cost: '150'
            },
            currentBalance: 0.00,
            rate: 0.00,
            currentIndex: 0,
            endIndex: 2,
            startDateModal: false,
            endDateModal: false,
            dateChanged: false,
            setStartDay: moment(new Date()).format('YYYY-MM-DD'),
            setEndDay: '',
            markedDates: {
                '2019-10-20': {selected: true, endingDay: true, color: 'green', textColor: 'gray'},
                '2019-10-22': {selected: true, endingDay: true, color: 'green', textColor: 'gray'},
                '2019-10-23': {selected: true, endingDay: true, color: 'green', textColor: 'gray'},
                '2019-10-04': {selected: true, endingDay: true, color: 'green', textColor: 'gray'}
            },
            selected: [],
            firstDate: true,
            secondDate: false
        };
        
        this._onSelectProduct = this._onSelectProduct.bind(this);
        this.onDayPress = this.onDayPress.bind(this);
    }

    componentDidMount() {
        // sort data being thrown by Danish to state.carousel_items
        this._sort_office_products(this.props.navigation.state.params.sharedOfficeData[0]);
        const self = this;
        setTimeout(function () {
            // var items = {};
            // for(var i = 0; i <5; i++) {
            //     items["2019-09-"+i] = {title: 'first title'};
            // }
            // var result = Object.keys(items).map(function(key) {
            //     return [Number(key), items[key]];
            // });
            self.setState({
                a: 7,
                b: 7
            });
            // // self._renderActiveProductPriceList();
            // setTimeout(function () {
            //     self.setState({
            //         currentIndex: 2
            //     });
            //     // self._renderActiveProductPriceList();
            // }, 5000)
        }, 5000)
    }
    componentDidUpdate(prevProps,prevState){
    }
    _sort_office_products(data) {
        /** 
         * @todo need to update what is inside data[1], might be more office_fees..
        */
        if (data.length < 1) { 
            // no data available.
            return;
        }
        let categories = [];
        for (k in data) {
            let products = data[k];
            if (!Array.isArray(products)) { 
                break;
            }
            let fees = [];

            products.forEach((product) => {
                // sometimes the server side passes a null value make sure to return
                // a value or if null just return 0

                let category_id = product.category_id || 0,
                    no_of_peoples = product.no_of_peoples || 0,
                    hourly_price = product.hourly_price || 0,
                    daily_price = product.daily_price || 0,
                    weekly_price = product.weekly_price || 0,
                    month_price = product.month_price || 0;
                
                fees.push({
                    id: product.id,
                    qr_seat: products[0].number,
                    office_id: products[0].office_id,
                    product_id: parseInt(category_id),
                    capacity: `${no_of_peoples} People`,
                    price_hourly: parseFloat(hourly_price),
                    price_daily: parseFloat(daily_price),
                    price_weekly: parseFloat(weekly_price),
                    price_monthly: parseFloat(month_price)
                });
            })

            categories.push({
                product: {
                    product_id: parseInt(products[0].category_id),
                    product_name: k,
                },
                fees,
                icon: this._getIcon(parseInt(products[0].category_id)),
                fee: fees[0] || {},
            });
        }

            
        this.setState({
            carousel_active_item_idx: categories[0].fees.length < 0? -1: 0, // default selection to -1 and 0
            carousel_items: categories
        })
    }
    _getIcon(product_id) {
        let icon = {};
        switch (product_id) { 
            case 3:
                icon = require('../../images/select_seat/Meeting.png');
                break;
            case 2:
                icon = require('../../images/select_seat/DedicatedDesk.png');
                break;
            default: 
                icon = require('../../images/select_seat/HotDesk.png');
        }
        return icon;
    }
    static navigationOptions = ({ navigation: { navigate } }) => ({
        title: 'Xenren CO-Workspace'
    });
    static propTypes = {
        navigation: PropTypes.object.isRequired,
    };
    // set the selected Product
    _onSelectProduct(index) { 
        this.setState({
            active_product_index: index
        });
    }
    _getActiveProductData = () => {
        let arr =this.state.carousel_items[this.state.carousel_active_item_idx].fees;
        let newarr = [];
        for (var i=0; i<arr.length; i=i+4) {
            if(i==0)
                newarr.push(arr.slice(i,i+5));
            else
                newarr.push(arr.slice(i,i+4));
        }

        return newarr;
        // return this.state.carousel_items[this.state.carousel_active_item_idx];
    }
    _getActiveProductDataNew = () => {
        return this.state.carousel_items[this.state.carousel_active_item_idx];
    }
    _onSetPrice = (fee) => {
        let list = this.state.carousel_items;
        list[this.state.carousel_active_item_idx].fee = fee;
        this.setState({
            carousel_items: list,
        })
    }
    handleNextClick = () => {
        let newIndex = this.state.currentIndex+1;
        this.setState({
            currentIndex: newIndex
        })
    }
    handleBackClick = () => {
        let newIndex = this.state.currentIndex-1;
        this.setState({
            currentIndex: newIndex
        })
    }
    _renderActiveProductPriceList = () => {
        if (this.state.carousel_active_item_idx < 0) { 
            return <Text>Prices are currently unavailable.</Text>;
        }
        let active_product = this._getActiveProductData();
        let active_productnew = this._getActiveProductDataNew();
        let start = this.state.currentIndex;
        let i = 0;
        let obj = [];
        if(start !== 0) {
            obj.push(
                <Button
                    onPress={start !== 0 && this.handleBackClick}
                    containerStyle={{width: "30%", marginHorizontal: (10 / 6) + "%"}}
                    raised={true}
                    buttonStyle={[PickSeatStyle.buttonStyle, {
                        height: 43
                    }]}
                    titleStyle={[PickSeatStyle.titleStyle]}
                    icon={
                        start !== 0 ? <MaterialCommunityIcons
                            name="chevron-left"
                            size={15}
                            color={duration.indicator == this.state.duration.indicator ? '#fff' : Config.primaryColor}
                        /> : '-'
                    }
                />
            );
        }
        active_product[start].map((fee, idx) => {
            let activeButtonStyle = {},
                activeTextStyle = {};
            if (active_productnew.fee.id == fee.id) {
                activeButtonStyle = PickSeatStyle.activeButton;
                activeTextStyle = PickSeatStyle.activeButtonText;
            }

            obj.push(
                <Button
                    onPress={this._onSetPrice.bind(this, fee)}
                    key={idx}
                    containerStyle={{ width: "30%", marginHorizontal: (10/6)+"%" }}
                    raised={true}
                    buttonStyle={[PickSeatStyle.buttonStyle, activeButtonStyle]}
                    titleStyle={[PickSeatStyle.titleStyle, activeTextStyle]}
                    title={"$" + fee['price_' + this.state.duration.indicator].toFixed(2) + "\n" + fee.capacity }
                />
            )
        });
        if(start != active_product.length-1) {
            obj.push(
                <Button
                    onPress={this.handleNextClick}
                    containerStyle={{ width: "30%", marginHorizontal: (10/6)+"%" }}
                    raised={true}
                    buttonStyle={[PickSeatStyle.buttonStyle, {
                        height: 43,
                        paddingTop: -5
                    }]}
                    titleStyle={[PickSeatStyle.titleStyle]}
                    title={"...\n More"}
                />
            )
        }
        return obj;
    }
    _renderJumbotron = ({ item, index }) => { 
        return (<PickSeatJumbotron
            key={index}
            data={item}
            duration={this.state.duration}
        />);
    }
    goBack = () => {
        this.props.navigation.navigate('Root');
    }
    getMe(cb){
        HttpRequest.me(this.props.userData.token)
            .then((response) => {
                cb(false, response);
            })
            .catch((error) => {
                cb(error);
            });
    }
    topup(){
        this.setState({
            noEnoughBalance: false
        });
        HttpRequest.topUpBalance(this.props.userData.token, {"remark": this.state.remark}, this.state.topupAmount)
            .then((response) => {
                this._confirmSeat();
            })
            .catch((error) => {
                this.setState({
                    noEnoughBalance: true
                });
                alert(error)
            });
    }
    getDate = (type) => {
        if(type === 'weekly') {
            let start_date = moment().format("DD-MMM-YYYY");
            let end_date = moment().add(7, 'days').format("DD-MMM-YYYY");
            let end_date_formatted = moment().add(7, 'days').format("YYYY-MM-DD");
            this.setState({
                pickedStartDate: start_date,
                pickedEndDate: end_date_formatted,
            });
            return {
              'start_date': start_date,
              'end_date': end_date,
            };
        } else if (type === 'daily') {
            let start_date = moment().format("DD-MMM-YYYY");
            let end_date = moment().format("DD-MMM-YYYY");
            let end_date_formatted = moment().format("YYYY-MM-DD");
            this.setState({
                pickedStartDate: start_date,
                pickedEndDate: end_date_formatted,
            });
            return {
                'start_date': start_date,
                'end_date': end_date
            };
        } else { //monthly
            let start_date = moment().format("DD-MMM-YYYY");
            let end_date = moment().add(30, 'days').format("DD-MMM-YYYY");
            let end_date_formatted = moment().add(30, 'days').format("YYYY-MM-DD");
            this.setState({
                pickedStartDate: start_date,
                pickedEndDate: end_date_formatted,
            });
            return {
                'start_date': start_date,
                'end_date': end_date
            };
        }
    }
    _pickSeat = () => {
        const self = this;
        this.setState({
            confirmReservation: false
        });
        let selectedProduct = self.state.carousel_items[self.state.carousel_active_item_idx];
        HttpRequest.pickSeatByQRCode(this.props.userData.token, selectedProduct.fee.qr_seat, selectedProduct.fee.office_id, this.state.duration.id, this.state.pickedEndDate, this.state.setStartDay, this.state.setEndDay, this.state.dateChanged)
        .then((response) => {
            if (response.data.status === 'error') {
                if(response.data.type && response.data.type === 'insufficient_balance') {

                } else {
                    alert(response.data.message);
                }
            } else if (response.data.status === 'success') {
                this.props.mySelectedSeatData(response.data.data);
                this.props.navigation.navigate('SeatSelectSuccess', {data: response.data});
            } else if (response.data.status === 'failure') {
                alert(response.data.status);
            }
        })
        .catch((error) => {
            alert(error);
        });
    }
    _confirmSeat = () => {
        const self = this;
        let selectedProduct = self.state.carousel_items[self.state.carousel_active_item_idx];
        let selectedType = self.state.duration.indicator;
        let dates = self.getDate(selectedType);
        if(selectedType === "hourly") {
            this._pickSeat();
        } else {
            this.getMe(function (err, resp) {
                if (resp) {
                    let selectedTypeRate = selectedProduct.fee["price_" + selectedType];
                    if (resp.data.balance < selectedTypeRate) {
                        self.setState({
                            noEnoughBalance: true,
                            currentBalance: resp.data.balance,
                            rate: selectedTypeRate
                        })
                    } else {
                        self.setState({
                            confirmReservation: true,
                            reservation: {
                                cost: selectedTypeRate,
                                start_date: dates.start_date,
                                end_date: dates.end_date
                            }
                        })
                    }
                } else {
                }
            })
        }
    }
    _renderDurations = (duration) => { 
        return (<Button
            onPress={() => this.setState({ duration, })}
            key={duration.indicator}
            raised={true}
            containerStyle={{ width: ((100/4) - ((100/4) * 0.2)) + "%" }}
            buttonStyle={[PickSeatStyle.buttonStyle, duration.indicator == this.state.duration.indicator ? PickSeatStyle.activeButton : {}]}
            titleStyle={[PickSeatStyle.titleStyle, duration.indicator == this.state.duration.indicator ? PickSeatStyle.activeButtonText : {}]}
            icon={
                <MaterialCommunityIcons
                    name="calendar-clock"
                    size={15}
                    color={duration.indicator == this.state.duration.indicator ? '#fff' : Config.primaryColor}
                />}
            title={duration.label}
        />)
    }
    _renderPriceList = () => { 
        return this.state.carousel_is_loading ? <ActivityIndicator color="#F7F7F6" /> :
            this._renderActiveProductPriceList();
    }
    _renderCarousel = () => { 
        return this.state.carousel_is_loading ? <ActivityIndicator color="#F7F7F6" /> : <Carousel
            onSnapToItem={this._onSelectProduct}
            decelerationRate={"fast"}
            data={this.state.carousel_items}
            renderItem={this._renderJumbotron}
            containerCustomStyle={{ flex: 1 }}
            slideStyle={{ flex: 1 }}
            sliderWidth={this.state.carouselWidth}
            itemWidth={this.state.carouselItemWidth}
        />
    }
    _renderReservationConfirmationModal = () => {
        return <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.confirmReservation}
            onRequestClose={() => this.setState({confirmReservation: false})}
        >
            <View style={styles.dialogStyle}>
                <View style={styles.dialogBoxStyle}>

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({confirmReservation: false});
                        }}
                        style={{
                            flexDirection: 'row',
                            alignSelf: 'center',
                            padding: 5,
                            paddingRight: 10,
                            top: 0,
                            right: 0,
                            position: "absolute",
                        }}>
                        <Ionicons
                            name="md-close"
                            size={14}
                            color={"grey"}
                        />
                    </TouchableOpacity>

                    <Text style={{
                        fontFamily: FontStyle.Bold,
                        top: 20,
                        fontSize: 17
                    }}>
                        Reservation Confirmation
                    </Text>

                    <Text style={{
                        fontFamily: FontStyle.Regular,
                        top: 20,
                        fontSize: 12,
                        color: "grey"
                    }}>
                        You've enough balance to reserve
                    </Text>

                    <View style={{flex: 1, flexDirection: 'row', padding: 20, paddingTop: 40}}>
                        <View style={{width: "50%", height: 50, backgroundColor: Config.primaryColor, borderRightWidth: 1, borderColor: "white"}}>

                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <View style={{width: "30%",height: 50}}>
                                    <Ionicons
                                        name="md-calendar"
                                        size={30}
                                        color={"grey"}
                                        style={{
                                            top: 10,
                                            left: 10,
                                            color: "white"
                                        }}
                                    />
                                </View>
                                <View style={{width: "70%", height: 50}}>

                                    <Text style={{
                                        fontFamily: FontStyle.Bold,
                                        color: "white",
                                        textAlign: "center",
                                        paddingTop: 10,
                                        fontSize: 15
                                    }}>
                                        Start Date
                                    </Text>

                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        color: "white",
                                        textAlign: "center",
                                        fontSize: 10
                                    }}>
                                        {this.state.reservation.start_date}
                                    </Text>

                                </View>
                            </View>

                        </View>


                        <View style={{width: "50%", height: 50, backgroundColor: Config.primaryColor}}>

                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <View style={{width: "30%",height: 50}}>
                                    <Ionicons
                                        name="md-calendar"
                                        size={30}
                                        color={"grey"}
                                        style={{
                                            top: 10,
                                            left: 10,
                                            color: "white"
                                        }}
                                    />
                                </View>
                                <View style={{width: "70%", height: 50}}>

                                    <Text style={{
                                        fontFamily: FontStyle.Bold,
                                        color: "white",
                                        textAlign: "center",
                                        paddingTop: 10,
                                        fontSize: 15
                                    }}>
                                        End Date
                                    </Text>

                                    <Text style={{
                                        fontFamily: FontStyle.Regular,
                                        color: "white",
                                        textAlign: "center",
                                        fontSize: 10
                                    }}>
                                        {this.state.reservation.end_date}
                                    </Text>

                                </View>
                            </View>

                        </View>

                    </View>

                    <Text style={{
                        fontFamily: FontStyle.Regular,
                        fontSize: 16,
                        position: "absolute",
                        textAlign: "center",
                        width: "50%",
                        color: "grey",
                        bottom: 70
                    }}>
                        You've to Pay
                    </Text>
                    <Text style={{
                        fontFamily: FontStyle.Bold,
                        fontSize: 16,
                        position: "absolute",
                        textAlign: "center",
                        width: "50%",
                        color: "grey",
                        bottom: 55
                    }}>
                        {"$"+this.state.reservation.cost}
                    </Text>

                    <TouchableOpacity style={{
                        bottom: 0,
                        left: 0,
                        position: "absolute",
                        backgroundColor: Config.primaryColor,
                        paddingTop: 10,
                        height: 40,
                        width: "50%",
                    }} onPress={() => this._pickSeat()}>
                        <Text style={{
                            fontFamily: FontStyle.Bold,
                            fontSize: 16,
                            color: "white",
                            textAlign: "center",
                        }}>
                            CONFIRM
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity  style={{
                        fontFamily: FontStyle.Bold,
                        fontSize: 16,
                        bottom: 0,
                        right: 0,
                        position: "absolute",
                        backgroundColor: "grey",
                        paddingTop: 10,
                        height: 40,
                        textAlign: "center",
                        width: "50%",
                        color: "white"
                    }} >
                        <Text style={{
                            fontFamily: FontStyle.Bold,
                            fontSize: 16,
                            textAlign: "center",
                            color: "white"
                        }} onPress={() => this.setState({confirmReservation: false})}>
                            CANCEL
                        </Text>
                    </TouchableOpacity>

                </View>

            </View>
        </Modal>
    }
    _renderNotEnoughBalanceModal = () => {
        return <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.noEnoughBalance}
            onRequestClose={() => this.setState({noEnoughBalance: false})}
        >
            <View style={styles.dialogStyle2}>
                <View style={styles.dialogBoxStyle2}>

                    <TouchableOpacity
                        onPress={() => {
                            this.setState({noEnoughBalance: false})
                        }}
                        style={{
                            flexDirection: 'row',
                            alignSelf: 'center',
                            padding: 5,
                            paddingRight: 10,
                            top: 0,
                            right: 0,
                            position: "absolute",
                        }}>
                        <Ionicons
                            name="md-close"
                            size={14}
                            color={"grey"}
                        />
                    </TouchableOpacity>

                    <FontAwesome name="exclamation-triangle"
                                 color={Config.primaryColor}
                                 style={{
                                     textAlign: "center",
                                     top: 30
                                 }}
                                 size={45}
                    />

                    <Text style={{
                        fontFamily: FontStyle.Bold,
                        top: 30,
                        fontSize: 17,
                        textAlign: "center"
                    }}>
                        No Enough Balance
                    </Text>

                    <Text style={{
                        fontFamily: FontStyle.Regular,
                        top: 33,
                        fontSize: 12,
                        textAlign: "center",
                        color: "grey"
                    }}>
                        You have to topup balance first to reserve
                    </Text>

                    <Text style={{
                        fontFamily: FontStyle.Regular,
                        top: 33,
                        fontSize: 15,
                        textAlign: "center",
                        color: "black",
                        marginTop: 15
                    }}>
                        Your Current Balance is ${this.state.currentBalance}
                    </Text>

                    <Text style={{
                        fontFamily: FontStyle.Regular,
                        top: 33,
                        fontSize: 15,
                        textAlign: "center",
                        color: Config.primaryColor
                    }}>
                        The amount required for seat ${this.state.rate}
                    </Text>

                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        top: 58,
                        justifyContent: 'center',
                        alignItems: 'stretch'
                    }}>
                        <View style={{
                            width: 120,
                            height: 50,
                            align: "center",
                            paddingTop: 13,
                            color: "grey"
                        }}>
                            <Text style={{
                                alignSelf: "center",
                                color: "grey",
                                fontFamily: FontStyle.Regular,
                                fontSize: 12
                            }}>
                                Topup Amount
                            </Text>
                        </View>
                        <View style={{
                            width: 130,
                            height: 50
                        }}
                        >
                            <TextInput
                                style={{
                                    borderColor: 'lightgray',
                                    borderWidth: 2,
                                    height: 30,
                                    borderRadius: 5,
                                    marginTop: 5,
                                    fontFamily: FontStyle.Regular
                                }}
                                keyboardType='numeric'
                                returnKeyType='done'
                                onChangeText={text => this.setState({ topupAmount: text })}
                                value={this.state.topupAmount}
                            />
                        </View>
                    </View>

                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'stretch',
                        marginTop: 30
                    }}>
                        <View style={{
                            width: 120,
                            align: "center",
                            color: "grey"
                        }}>
                            <Text style={{
                                alignSelf: "center",
                                color: "grey",
                                fontFamily: FontStyle.Regular,
                                fontSize: 12,
                                marginTop: 10
                            }}>
                                Remarks
                            </Text>
                        </View>

                        <View style={{
                            width: 130,
                        }}
                        >
                            <TextInput
                                style={{
                                    borderColor: 'lightgray',
                                    borderWidth: 2,
                                    height: 30,
                                    borderRadius: 5,
                                    fontFamily: FontStyle.Regular
                                }}
                                returnKeyType='done'
                                onChangeText={text => this.setState({ remark: text })}
                                value={this.state.remark}
                            />
                        </View>
                    </View>

                    <View style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'stretch'
                    }}>
                        <Text style={{
                            fontFamily: FontStyle.Regular,
                            fontSize: 12,
                            textAlign: "center",
                            color: Config.primaryColor
                        }}>
                            You have to topup more than ${this.state.rate-this.state.currentBalance}
                        </Text>
                    </View>

                    <TouchableOpacity style={{
                        bottom: 0,
                        left: 0,
                        position: "absolute",
                        backgroundColor: Config.primaryColor,
                        paddingTop: 10,
                        height: 40,
                        width: "50%",
                    }} onPress={() => {
                        this.topup();
                    }}>
                        <Text style={{
                            fontFamily: FontStyle.Bold,
                            fontSize: 16,
                            color: "white",
                            textAlign: "center",
                        }}>
                            TOP UP
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity  style={{
                        fontFamily: FontStyle.Bold,
                        fontSize: 16,
                        bottom: 0,
                        right: 0,
                        position: "absolute",
                        backgroundColor: "grey",
                        paddingTop: 10,
                        height: 40,
                        textAlign: "center",
                        width: "50%",
                        color: "white"
                    }} >
                        <Text style={{
                            fontFamily: FontStyle.Bold,
                            fontSize: 16,
                            textAlign: "center",
                            color: "white"
                        }} onPress={() => this.setState({noEnoughBalance: false})}>
                            CANCEL
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        </Modal>
    };
    setStartDay = (val) => {
        this.setState({
            setStartDay: val.dateString,
            startDateModal: false,
            dateChanged: true
        });
    };
    setEndDay = (val) => {
        this.setState({
            setEndDay: val.dateString,
            endDateModal: false,
            dateChanged: true
        });
    };
    onDayPress(day) {

        if(this.state.firstDate === false) {
            this.setState({
                setStartDay: day.dateString,
                setEndDay: '',
                firstDate: true
            });
        } else {
            this.setState({
                setEndDay: day.dateString,
                firstDate: false
            });
        }
    }
    enumerateDaysBetweenDates(startDate, endDate)  {
        var dates = [];

        var currDate = moment(startDate).startOf('day');
        var lastDate = moment(endDate).startOf('day');

        while(currDate.add(1, 'days').diff(lastDate) < 0) {
            dates.push(moment(currDate.clone().toDate()).format('YYYY-MM-DD'));
        }

        return dates;
    };
    getMarkedDates = () => {
        const marked = {};
        let dates = [this.state.setStartDay];
        if(this.state.setStartDay && this.state.setEndDay) {
            dates = this.enumerateDaysBetweenDates(this.state.setStartDay, this.state.setEndDay);
            marked[this.state.setStartDay] = {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'};
        }
        dates.forEach(item => {
            marked[item] = {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'};
        });
        if(this.state.setStartDay && this.state.setEndDay) {
            marked[this.state.setEndDay] = {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'};
        }
        return marked;
    };
    _renderStartDateCalender = () => {
        return <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.startDateModal}
            onRequestClose={() => this.setState({startDateModal: false})}
        >
            <View style={styles.dialogStyle2}>
                <View style={styles.dialogBoxStyle3}>

                    <TouchableOpacity
                        onPress={() => this.setState({startDateModal: false})}
                        style={{
                            flexDirection: 'row',
                            alignSelf: 'center',
                            padding: 5,
                            paddingRight: 10,
                            top: 0,
                            right: 0,
                            position: "absolute",
                        }}>
                        <Ionicons
                            name="md-close"
                            size={14}
                            color={"grey"}
                        />
                    </TouchableOpacity>
                    <Text style={{
                        textAlign: "center",
                        paddingTop: 20,
                        height: 50,
                        fontFamily: FontStyle.Regular,
                        fontSize: 20
                    }}> Select Start Date </Text>
                    <Calendar
                        // Initially visible month. Default = Date()
                        current={this.state.setStartDay}
                        onDayPress={this.onDayPress}
                        monthFormat={'MMMM yyyy'}
                        hideExtraDays={true}
                        firstDay={1}
                        showWeekNumbers={false}
                        onPressArrowLeft={substractMonth => substractMonth()}
                        onPressArrowRight={addMonth => addMonth()}
                        // markedDates={this.state.selected}
                        markedDates={this.getMarkedDates()}
                        style={{
                            width: '100%',
                            padding: 0,
                            margin: 0

                        }}
                        theme={{
                            calendarBackground: '#fff',
                            textSectionTitleColor: 'grey',
                            dayTextColor: 'grey',
                            todayTextColor: 'black',
                            selectedDayTextColor: '#fff',
                            monthTextColor: '#fff',
                            indicatorColor: 'grey',
                            selectedDayBackgroundColor: Config.primaryColor,
                            arrowColor: "#fff",
                            // textDisabledColor: 'red',
                            'stylesheet.calendar.header': {
                                week: {
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                },
                                month: {
                                    backgroundColor: Config.primaryColor,
                                    color: "red"
                                }
                            },
                        }}

                    />

                </View>

            </View>
        </Modal>;
    };
    // _renderEndDateCalender = () => {
    //     return <Modal
    //         animationType="fade"
    //         transparent={true}
    //         visible={this.state.endDateModal}
    //         onRequestClose={() => this.setState({endDateModal: false})}
    //     >
    //         <View style={styles.dialogStyle2}>
    //             <View style={styles.dialogBoxStyle2}>
    //
    //                 <TouchableOpacity
    //                     onPress={() => this.setState({endDateModal: false})}
    //                     style={{
    //                         flexDirection: 'row',
    //                         alignSelf: 'center',
    //                         padding: 5,
    //                         paddingRight: 10,
    //                         top: 0,
    //                         right: 0,
    //                         position: "absolute",
    //                     }}>
    //                     <Ionicons
    //                         name="md-close"
    //                         size={14}
    //                         color={"grey"}
    //                     />
    //                 </TouchableOpacity>
    //                 <Text style={{
    //                     textAlign: "center",
    //                     paddingTop: 20,
    //                     height: 50,
    //                     fontFamily: FontStyle.Regular,
    //                     fontSize: 20
    //                 }}> Select End Date </Text>
    //                 <Calendar
    //                     // Initially visible month. Default = Date()
    //                     current={this.state.setEndDay}
    //                     // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
    //                     // minDate={'2012-05-10'}
    //                     // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
    //                     // maxDate={'2012-05-30'}
    //                     // Handler which gets executed on day press. Default = undefined
    //                     // onDayPress={(day) => {
    //                     //     this.setState({
    //                     //         currentDate: day.dateString
    //                     //     });
    //                     // }}
    //                     onDayPress={this.setEndDay.bind(this)}
    //                     // Handler which gets executed on day long press. Default = undefined
    //                     // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
    //                     monthFormat={'yyyy MM'}
    //                     // Handler which gets executed when visible month changes in calendar. Default = undefined
    //                     // Hide month navigation arrows. Default = false
    //                     // hideArrows={true}
    //                     // Replace default arrows with custom ones (direction can be 'left' or 'right')
    //                     // renderArrow={(direction) => (<Arrow />)}
    //                     // Do not show days of other months in month page. Default = false
    //                     // hideExtraDays={true}
    //                     // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
    //                     // day from another month that is visible in calendar page. Default = false
    //                     // disableMonthChange={true}
    //                     // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
    //                     firstDay={1}
    //                     // Hide day names. Default = false
    //                     // hideDayNames={true}
    //                     // Show week numbers to the left. Default = false
    //                     showWeekNumbers={true}
    //                     // Handler which gets executed when press arrow icon left. It receive a callback can go back month
    //                     onPressArrowLeft={substractMonth => substractMonth()}
    //                     // Handler which gets executed when press arrow icon left. It receive a callback can go next month
    //                     onPressArrowRight={addMonth => addMonth()}
    //                     theme={{
    //                         calendarBackground: '#fff',
    //                         textSectionTitleColor: 'grey',
    //                         dayTextColor: 'grey',
    //                         todayTextColor: 'black',
    //                         selectedDayTextColor: Config.primaryColor,
    //                         monthTextColor: 'grey',
    //                         indicatorColor: 'grey',
    //                         selectedDayBackgroundColor: '#333248',
    //                         arrowColor: Config.primaryColor,
    //                         // textDisabledColor: 'red',
    //                         'stylesheet.calendar.header': {
    //                             week: {
    //                                 marginTop: 5,
    //                                 flexDirection: 'row',
    //                                 justifyContent: 'space-between',
    //                             },
    //                             month: {
    //                                 backgroundColor: Config.primaryColor
    //                             }
    //                         }
    //                     }}
    //
    //                 />
    //
    //             </View>
    //
    //         </View>
    //     </Modal>;
    // };
    render() {
            return (
                <View style={styles.rootStyle}>
                    {this._renderNotEnoughBalanceModal()}
                    {this._renderReservationConfirmationModal()}
                    {this._renderStartDateCalender()}
                    {/*{this._renderEndDateCalender()}*/}

                    <View style={{
                        flex: 1,
                        flexGrow: 2
                    }}>

                        <View
                            onLayout={(event) => {
                                this.setState({
                                    carousel_is_loading: false,
                                    carouselWidth: event.nativeEvent.layout.width,
                                    carouselItemWidth: event.nativeEvent.layout.width - (event.nativeEvent.layout.width * 0.15),
                                    carouselItemHeight: event.nativeEvent.layout.height
                                })
                            }} style={[PickSeatStyle.flex1, { paddingVertical: 20 } ]}>
                            { this._renderCarousel() }
                        </View>
                        <View style={[PickSeatStyle.flex1, { paddingVertical: 20 }]}>
                            <Text style={{ textAlign: 'center', fontWeight: "bold" }}>Choose number of People</Text>
                            <View style={[PickSeatStyle.flex1, { margin: 10, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'flex-start', alignContent: 'space-around' }]}>
                                { this._renderPriceList() }
                            </View>
                        </View>
                    </View>
                    <View style={[PickSeatStyle.flex1]}>
                        <View style={[PickSeatStyle.greyBackground, { alignItems: 'center', justifyContent: "space-between", padding: 5 }]}>
                            {duration.map(this._renderDurations)}
                        </View>
                        <View style={{ flexGrow: 2, justifyContent: "center", alignItems: "center" }}>
                            <Text style={{
                                fontFamily: FontStyle.Bold,
                                paddingTop: 10
                            }}>Select Date</Text>
                        </View>
                        <View style={{ flexGrow: 2, justifyContent: "center", alignItems: "center" }}>
                            <View
                                style={{
                                    flex: 1,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    padding: 20,
                                    marginTop: -20
                                }}>
                                <View
                                    style={{
                                        width: '50%',
                                        height: 50,
                                        marginRight: 5,
                                        borderWidth: 1,
                                        borderRadius: 6,
                                        borderColor: 'lightgrey',
                                        shadowColor: 'lightgrey',
                                        shadowOpacity: 0.8,
                                        shadowRadius: 10,
                                        shadowOffset: {
                                            height: 5,
                                            width: 0,
                                        },
                                    }}>
                                    <FontAwesome name="calendar"
                                                 color="green"
                                                 style={{
                                                     position: 'absolute',
                                                     top: 6,
                                                     left: 15,
                                                 }}
                                                 size={25}
                                    />
                                    <Text
                                        style={{
                                            textAlign: 'center',
                                            paddingTop: 6,
                                            paddingLeft: 10,
                                            color: 'green',
                                            fontSize: 12,
                                            fontFamily: FontStyle.Bold
                                        }}
                                        onPress={()=>{
                                            this.setState({
                                                startDateModal: true
                                            })
                                        }}
                                    >
                                        Start Date
                                    </Text>
                                    <Text
                                        style={{
                                            textAlign: 'center',
                                            paddingTop: 2,
                                            paddingLeft: 10,
                                            color: 'green',
                                            fontSize: 12,
                                            fontFamily: FontStyle.regular
                                        }}>
                                        {this.state.setStartDay}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        width: '50%',
                                        height: 50,
                                        marginRight: 15,
                                        borderWidth: 1,
                                        borderRadius: 6,
                                        borderColor: 'lightgrey',
                                        shadowColor: 'lightgrey',
                                        shadowOpacity: 0.8,
                                        shadowRadius: 10,
                                        shadowOffset: {
                                            height: 5,
                                            width: 0,
                                        },
                                    }}>
                                    <FontAwesome name="calendar"
                                                 color="green"
                                                 style={{
                                                     position: 'absolute',
                                                     top: 6,
                                                     left: 15,
                                                 }}
                                                 size={25}
                                    />
                                    <Text
                                        style={{
                                            textAlign: 'center',
                                            paddingTop: 6,
                                            paddingLeft: 10,
                                            color: 'green',
                                            fontSize: 12,
                                            fontFamily: FontStyle.Bold
                                        }}
                                        onPress={()=>{
                                            this.setState({
                                                startDateModal: true
                                            })
                                        }}
                                    >
                                        End Date
                                    </Text>
                                    <Text
                                        style={{
                                            textAlign: 'center',
                                            paddingTop: 2,
                                            paddingLeft: 10,
                                            color: 'green',
                                            fontSize: 12,
                                            fontFamily: FontStyle.regular
                                        }}>
                                        {this.state.setEndDay}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexGrow: 2, justifyContent: "center", alignItems: "center" }}>
                            <Button
                                onPress={this._confirmSeat}
                                containerStyle={PickSeatStyle.confirmBtn}
                                buttonStyle={[{ backgroundColor: Config.primaryColor, paddingVertical: 7 }]}
                                title="Confirm" />
                        </View>
                    </View>
                </View>
            );
    }
}

const PickSeatStyle = StyleSheet.create({
    activeButton: {
        backgroundColor: Config.primaryColor
    },
    activeButtonText: {
        color: "#fff",
    },
    confirmBtn: {
        marginHorizontal: 15,
        width: "80%",

    },
    buttonStyle: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 10,
        color: Config.primaryColor
    },
    carousel: {
        paddingVertical: 10
    },
    flex1: {
        flex: 1,
    },
    greyBackground: {
        backgroundColor: '#EDEDED',
        marginHorizontal: 10,
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

});

const styles = {

    boxStyleFirst: {
        width: '100%',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        marginTop: 5,
        height: 100,
        backgroundColor: 'white',
        // shadowColor: "lightgrey",
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // shadowOffset: {
        //     height: 0.5,
        //     width: 0
        // }
    },

    boxStyle: {
        width: '100%',
        borderTopWidth: 0,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        height: 100,
        // shadowColor: "lightgrey",
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // shadowOffset: {
        //     height: 0.5,
        //     width: 0
        // }
    },


    boxStyleSelected: {
        width: '100%',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        height: 100,
        backgroundColor: '#EDF8E7',
    },

    rootStyle: {
        backgroundColor: "#fff",
        flex: 1
    },

    contactTextStyle: {
        color: '#fff',
        backgroundColor: Config.primaryColor,
        paddingHorizontal: 30,
        paddingVertical: 10,
        fontSize: 15,
        marginTop: 10,
        fontFamily: FontStyle.Bold,
    },

    backArrowStyle: {
        marginRight: 10
    },

    backArrowIconStyle: {
        fontFamily: "Montserrat-Light",
        marginLeft: 10
    },

    navigationStyle: {
        height: 40,
        flexDirection: "row",
        marginTop: 20,
        paddingHorizontal: 5
        // alignItems:'center'
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%"
    },

    dialogStyle: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },

    dialogStyle2: {
        flex: 1,
        backgroundColor: 'rgba(44, 62, 80, 0.6)',
        alignItems: 'center',
        justifyContent: 'center',
    },

    dialogBoxStyle2: {
        width: 300,
        height: 360,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

    dialogBoxStyle3: {
        width: 300,
        height: 380,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

    dialogBoxStyle: {
        width: 300,
        height: 230,
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialogBoxStyle1: {
        width: 300,
        height: 200,
        backgroundColor: '#fff',
        borderRadius: 10,
    },
};
function mapStateToProps(state) {
    return {
        component: state.component,
        userData: state.auth.userData,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRoot: root => dispatch({
            type: 'set_root',
            root,
        }),
        mySelectedSeatData: data => dispatch({
            type: 'SELECTED_SEAT_DATA',
            data: { ...data },
        }),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SharedOfficeBarCodeScan);

// export default SharedOfficeBarCodeScan