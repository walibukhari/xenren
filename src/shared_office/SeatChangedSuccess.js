/* eslint-disable global-require */
/* eslint-disable no-unused-vars */
/* eslint-disable import/first */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable react/prop-types */

import React from 'react';
import { View, Modal, Text, Image, TouchableOpacity, Alert } from 'react-native';
import Color from '../Config';
import FontStyle from '../constants/FontStyle';
import _ from 'lodash';
import { NavigationActions } from 'react-navigation';

this.setState = true;

const SeatChanged = ({ callback }) => {
  return (
      <Modal
        animationType='fade'
        onRequestClose={() => console.log("Modal Closed")}
        transparent={true}
        visible={this.setState}
      >
        <View style={styles.modalContainerStyle}>

          <View style={styles.modalBoxStyle}>

            <View style={styles.modalHeaderWrapper}>

              <View style={{
                width: '100%',
                marginTop: 30,
                height: 200,
                justifyContent: 'center',
                alignItems: 'center',
                }}>
            <Text style={{
              fontSize: 18,
              fontFamily: FontStyle.Bold,
              }}>Seat Changed Successfuly</Text>
              </View>
              <TouchableOpacity
                  onPress={() => this.setState = false }
                  style={{
                justifyContent: 'center', alignItems: 'center', width: 150, height: 50, borderRadius: 5, backgroundColor: Color.primaryColor,
                }}>
                <Text style={{ fontSize: 18, fontFamily: FontStyle.Medium, color: 'white' }}>OK</Text>
              </TouchableOpacity>

            </View>
            <View style={{
              position: 'absolute', top: 0, width: 104, height: 104, borderRadius: 52, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center',
              }}>
                <Image style={{ width: 100, height: 100 }} resizeMode='contain' source={require('../../images/select_seat/tick.png')}/>
          </View>
          </View>
        </View>

      </Modal>
  );
};

const styles = {

  modalBoxStyle: {
    width: '85%',
    height: 340,
    borderRadius: 5,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },

  modalHeaderWrapper: {
    paddingHorizontal: 20,
    width: '90%',
    height: 290,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    borderRadius: 8,
  },

  modalContainerStyle: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

export default SeatChanged;
