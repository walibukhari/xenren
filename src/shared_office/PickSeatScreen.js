/* eslint-disable global-require */
/* eslint-disable no-lonely-if */
/* eslint-disable no-dupe-class-members */
/* eslint-disable indent */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable react/prop-types */
/* eslint-disable object-shorthand */
/* eslint-disable prefer-const */
/* eslint-disable camelcase */
/* eslint-disable react/no-deprecated */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import FooterTabs from '../screens/FooterTabs';
import Config from '../Config';
import SeatsLayout from './SeatsLayout';
import actions from './actions';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';
import LocalData from '../components/LocalData';
// eslint-disable-next-line import/first
import { NavigationActions } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontStyle from '../constants/FontStyle';
import Feather from "react-native-vector-icons/Feather";
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

const { width } = Dimensions.get('window');
let scrollHeight = null;

const styles = {
  rootStyle: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    paddingTop: 20,
  },
  errorView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    width: width,
  },
  navigationStyle: {
    height: 50,
    flexDirection: 'row',
  },
  screenTitle: {
    alignSelf: 'stretch',
    marginTop: 20,
    marginBottom: 10,
    marginLeft: 20,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#666',
  },
  layoutWrapper: {
    flex: 1,
  },
  seatStyle: {
    flex: 2,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  seatTopStyle: {
    flexDirection: 'column',
  },
  buttonStyle: {
    backgroundColor: '#fff',
    padding: 10,
    justifyContent: 'center',
  },
  navigationButtonStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  navigationTextStyle: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  confirmButton: {
    height: 45,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  scanButton: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Config.primaryColor,
    borderRadius: 5,
  },
  seatButtonStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowViewStyle: {
    flexDirection: 'row',
  },
};

class SelectSeat extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: null,
    headerStyle: {
      backgroundColor: '#FFF',
      borderBottomColor: '#FFF',
    },
    headerTintColor: Config.topNavigation.headerIconColor,
    headerTitleStyle: {
      color: Config.topNavigation.headerTextColor,
      alignSelf: 'center',
      fontFamily: FontStyle.Regular,
      width: '100%',
    },
    headerLeft:
        <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={{
              marginLeft: 10,
              flexDirection: 'row',
              alignSelf: 'center',
              alignItems: 'center',
              padding: 5,
            }}>
          <FontAwesomeIcons
              size={26}
              name="chevron-left"
              color={Config.primaryColor}
              fa5Style={FA5Style.regular}
          />
          <Text onPress={() => {
            navigation.goBack();
          }} style={{
            marginLeft: 5,
            marginTop:0,
            fontSize: 20,
            fontWeight:'bold',
            color: Config.primaryColor,
            fontFamily: FontStyle.Regular,
          }}>Select Seat</Text>
        </TouchableOpacity>,
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    loadLayout: PropTypes.func.isRequired,
    layout: PropTypes.object.isRequired,
    selectSeatById: PropTypes.func.isRequired,
  };

  constructor(props) {
    console.log('PickSeatScreen.js');
    super(props);
    this.state = {
      loading: false,
      office: null,
      qrCode: this.props.navigation.state.params.qr,
      office_id: null,
    };
  }

  seatByQR = (seatId, office_id) => {
    let data = {
      office_id: office_id,
      new_seat_number: seatId,
    };
    HttpRequest.changeSeatByQR(this.props.userData.token, data)
      .then((response) => {
        if (response.data.status === 'error') {
          alert(response.data.message);
        } else if (response.data.status === 'success') {
          this.props.mySelectedSeatData(response.data.data);
          this.props.navigation.navigate('SeatSelectSuccess');
        } else if (response.data.status === 'failure') {
          alert(response.data.status);
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  pickNewSeat = () => {
    if (this.props.seatId) {
    HttpRequest.pickSeatByQRCode(this.props.userData.token, this.props.seatId, this.state.office_id)
        .then((response) => {
          if (response.data.status === 'error') {
            alert(response.data.message);
          } else if (response.data.status === 'success') {
            this.props.mySelectedSeatData(response.data.data);
            this.props.navigation.navigate('SeatSelectSuccess', { data: response.data });
          } else if (response.data.status === 'failure') {
            alert(response.data.status);
          }
        })
        .catch((error) => {
          alert(error);
        });
    }
  };

  goToLegends = () => {
    if (this.props.navigation.state.params.changeSeat) {
      this.seatByQR(this.props.seatId, this.state.office_id);
    } else {
      this.pickNewSeat();
    }
  };

  componentDidMount() {
    const { qr } = this.props.navigation.state.params;
    this.props.loadLayout(qr);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.layout.data.status) {
      if (nextProps.layout.data.status === 'failure') {
        this.showAlert();
      } else {
        this.setState({ office_id: nextProps.layout.data.data.office_id });
      }
    } else {
      //
    }
  }

  showAlert() {
    setTimeout(() => {
      Alert.alert(
        'Warning',
        'Token expire!',
        [
          {
            text: 'OK',
            onPress: () => {
              this.logout();
            },
          },
        ],
      );
    }, 200);
  }

  logout() {
    LocalData.setUserData(null);
    LocalData.setAppData(null);
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Login',
        }),
      ],
    }));
  }

  render() {
    let layout = null;
    if (!this.props.layout) {
      scrollHeight = 200;
      layout = (
        <View style={styles.errorView}>
          <ActivityIndicator/>
        </View>
      );
    } else {
      if (this.props.layout.data.status === 'success') {
        scrollHeight = 65 * this.props.layout.data.data.size.y;
        layout = (
          <SeatsLayout
            layout={this.props.layout.data.data.items}
            width={this.props.layout.data.data.size.x}
            height={this.props.layout.data.data.size.y}
            onSeatSelected={(seatId) => {
              this.props.selectSeat(seatId);
            }}
          />
        );
      } else {
        scrollHeight = 200;
        layout = (
          <View style={styles.errorView}>
            <Text style={{
              padding: 15,
              textAlign: 'center',
              textAlignVertical: 'center',
            }}>{this.props.layout.data[0].msg}</Text>
          </View>
        );
      }
    }

    return (
      <View style={styles.rootStyle}>
        {/* TODO move all this to Seats Layout */}
          <ScrollView
            style={styles.layoutWrapper}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              // flex: 1,
              height: width + scrollHeight,
            }}
          >
            <Text style={styles.screenTitle}>{translate('SEAT_NUMBER')}</Text>
            {layout}
          </ScrollView>
        <View style={styles.buttonStyle}>
          <TouchableOpacity
            style={styles.confirmButton}
            onPress={() => {
              this.goToLegends();
            }}
          >
            <Text style={{
              color: '#fff',
              fontSize: 16,
            }}>{translate('CONFIRM')}</Text>
          </TouchableOpacity>
          <View>
            <Text
              style={{
                color: Config.textColor,
                fontSize: 14,
                marginVertical: 10,
              }}
            >
              {translate('scan_QRcode_or_select_seat')}
            </Text>
          </View>
          <TouchableOpacity style={styles.scanButton}
                            onPress={() => {
                              this.props.navigation.navigate('QRScanner', { changeSeat: false });
                            }}
          >
            <Text style={{
              color: Config.primaryColor,
              fontSize: 16,
            }}>
              {translate('SCAN')}
            </Text>
            <Image
              source={require('../../images/select_seat/icon_qrcode.png')}
              style={{
                height: 25,
                tintColor: Config.primaryColor,
              }}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <FooterTabs navigation={this.props.navigation} currentIndex={0}/>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const officeId = state.sharedOffice.get('selected_office');
  return {
    layout: state.sharedOffice.getIn(['offices', 'layout']),
    officeId,
    seatId: state.sharedOffice.get('selected_seat'),
    selectedSeatData: state.sharedOffice.get('selectedSeatData'),
    userData: state.auth.userData,
    getMyselectedSeatData: state.auth.selectedSeatData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadLayout: id =>
      dispatch({
        type: actions.FETCH_OFFICE_LAYOUT_REQUESTED,
        payload: {
          id,
        },
      }),
    selectSeatById: (id, code) =>
      dispatch({
        type: actions.SELECT_SEAT_POST,
        payload: {
          id,
          code,
        },
      }),
    selectSeat: id =>
      dispatch({
        type: actions.SELECT_SEAT,
        payload: {
          id,
        },
      }),
    startTime: id =>
      dispatch({
        type: actions.START_TIME,
        payload: {
          id,
        },
      }),
    mySelectedSeatData: data => dispatch({
      type: 'SELECTED_SEAT_DATA',
      data: { ...data },
    }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectSeat);
