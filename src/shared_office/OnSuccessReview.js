import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, BackHandler, TouchableOpacity, Dimensions } from 'react-native';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { translate } from '../i18n';
import FontAwesomeIcons, {FA5Style} from "react-native-vector-icons/FontAwesome5";

const { width } = Dimensions.get('window');
class OnSuccessReview extends Component {
    static navigationOptions = {
      header: null,
    };
    constructor(props) {
      super(props);
      this.state = {
      };
    }

    componentDidMount() {
      BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }

    onBackPress = () => {
      this.props.navigation.navigate('Root');
    };

    goBack() {
      this.props.navigation.navigate('Root');
    }

    render() {
      return (
          <View style={styles.rootStyle}>
              <View style={styles.backButton}>
                  <TouchableOpacity
                      onPress={() => {
                          this.goBack();
                      }}
                      style={{
                          marginTop: 8,
                          marginLeft: 2,
                          flexDirection: 'row',
                      }}
                  >
                      <FontAwesomeIcons
                          size={26}
                          name="chevron-left"
                          color={Config.primaryColor}
                          fa5Style={FA5Style.regular}
                      />
                      <Text
                          style={{
                              marginLeft: 5,
                              marginTop:0,
                              fontWeight:'bold',
                              fontSize: 20,
                              color: Config.primaryColor,
                              fontFamily: FontStyle.Regular,
                          }}
                      >
                          {translate('back')}
                      </Text>
                  </TouchableOpacity>
              </View>
              <View style={styles.bottom}>
                  <AntDesign name="checkcircleo" size={60} color={Config.primaryColor}/>
                  <Text style={styles.successMessage}>{translate('success')}!</Text>
                  <Text style={styles.feedback}>{translate('thank_for_your_feedback')}.</Text>
              </View>
          </View>
      );
    }
}

const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: Config.white,
  },
  backButton: {
    top: 25,
    height: width / 4,
    marginLeft: 10,
    zIndex: 1,
  },
  bottom: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: width / 6,
    backgroundColor: Config.white,
  },
  successMessage: {
    fontFamily: FontStyle.Bold,
    fontSize: 22,
  },
  feedback: {
    fontFamily: FontStyle.Regular,
    fontSize: 18,
  },
};

function mapStateToProps(state) {
  return {
    sharedOfficeData: state.auth.sharedOffice,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRoot: root =>
      dispatch({
        type: 'set_root',
        root,
      }),
    sharedOffice: data => dispatch({
      type: 'SHARED_OFFICE',
      data,
    }),
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OnSuccessReview);
