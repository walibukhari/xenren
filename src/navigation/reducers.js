import { NavigationActions } from 'react-navigation';
import { AppNavigator } from './navigator';

const initModule = 'Root'; // TODO : set to Root before merging
const initialState = AppNavigator.router
  .getStateForAction(AppNavigator.router.getActionForPathAndParams(initModule));

export default (state = initialState, action) => {
  let nextState = AppNavigator.router.getStateForAction(action, state);
  switch (action.type) {
    case 'resetProfile':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 1,
          actions: [
            NavigationActions.navigate({ routeName: 'Root' }),
            NavigationActions.navigate({ routeName: 'Profile' }),
          ],
        }),
        state,
      );
      break;
    case 'goHome':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Root' }),
          ],
        }),
        state,
      );
      break;
    default:
  }
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};
