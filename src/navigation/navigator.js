/* eslint-disable class-methods-use-this */
/* eslint-disable no-alert */
/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';
import { StackNavigator, NavigationActions } from 'react-navigation';
import { BackHandler, Alert, Animated, Easing ,Platform, SafeAreaView} from 'react-native';
import Pushy from 'pushy-react-native';
import { connect } from 'react-redux';
import Root from '../Root';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Confirmation from '../screens/Confirmation';
import Menu from '../screens/Menu';
import Detail from '../screens/Detail';
import Complain from '../screens/Complain';
import ContactManagement from '../screens/ContactManagement';
import SearchResult from '../screens/SearchResult';
import ChatSingle from '../screens/ChatSingle';
import MemberList from '../screens/MemberList';
import TimeTracking from '../screens/TimeTracking';
import TermOfService from '../screens/TermOfService';
import PrivacyPolicy from '../screens/PrivacyPolicy';
import ForgotPassword from '../screens/ForgotPassword';
import EditProfile from '../screens/EditProfile';
import VerifyId from '../screens/verifyId';
import ConfirmUpload from '../screens/verifyId/ConfirmUpload';
import QrcodeResult from '../screens/QrcodeResult';
import Profile from '../screens/subs/Profile';
import SpaceDetail from '../screens/space/SpaceDetail';
import MySpaces from '../screens/space/MySpaces.js';
import CheckContact from '../screens/CheckContact';
import IndoorChat from '../screens/IndoorChat';
import UserProfile from '../screens/account/UserProfile';
import MyProfile from '../screens/MyProfile';
import VerifyPhoto from '../screens/VerifyPhoto';
import VerifyInfo from '../screens/VerifyInfo';
import VerifyInfoSuccess from '../screens/Verify_Info_Success';
import VerifyPhotoSuccess from '../screens/Verify_Photo_Success';
import PaymentRecord from '../screens/PaymentRecord';
import NotificationDetail from '../screens/NotificationDetail';
import SelectChat from '../screens/SelectChat';
import ChatDetail from '../screens/ChatDetail';
import Dashboard from '../screens/Dashboard';
import NotificationScreen from '../screens/subs/Notification';
import NotificationSettings from '../screens/profile/NotificationSettings';
import NotificationSettingsNew from '../screens/profile/NotificationSettingsNew';
import SharedOfficeFilters from '../screens/sharedOffices/Filters';
import FiltersMap from '../screens/sharedOffices/FiltersMap';
import SignUp from '../screens/SignUp';
import QrUser from '../screens/account/profileQrCode';
import UpdateProfile from '../screens/account/editProfile';
import FilterResult from '../screens/sharedOffices/FilterResults/';
// finance
import Balance from '../finance/Balance';
import Deposit from '../finance/Deposit';
import TopUp from '../finance/TopUp';
import Withdraw from '../finance/Withdraw';
import CreditCard from '../finance/CreditCard';
import CreditCardTop from '../finance/CreditCardTop';
import TopUpSuccess from '../finance/TopUpSuccess';
import PaypalView from '../finance/Paypal';
import RevenutMonster from '../finance/RevenueMonster';
// Shared offices
import SeatStatus from '../shared_office/SeatStatusScreen';
import QRScanner from '../shared_office/QRScannerScreen';
import SelectSeat from '../shared_office/PickSeatScreen';
import JobsList from '../screens/jobs/JobsList';
import ProjectDetail from '../screens/jobs/ProjectDetail';
import OfficialProjectDetail from '../screens/jobs/OfficialProjectDetail';
import SharedOfficeReview from '../shared_office/SharedOfficeReview';
import OnSuccessReview from '../shared_office/OnSuccessReview';
import SharedOfficeBarCodeScan from '../shared_office/SharedOfficeBarCodeScan';

// Received Order
import ReceivedOrder from '../screens/order';
// Jobs
import ProjectDetailInfo from '../screens/jobs/subs/ProjectDetailInfo';
// Indoor Chat
import JoinedChatRooms from '../screens/IndoorChat/JoinedChatRooms';
// About
import About from '../screens/about/index';
import QRDetail from '../screens/otherUserProfile/QRDetail';
// Other User Profile
import OtherUserProfile from '../screens/otherUserProfile';
import AboutOtherUser from '../screens/otherUserProfile/About';
import SeatSelectSuccess from '../shared_office/SeatSelectSuccess';
// Facebook Login
import bindFacebook from '../screens/facbookLogin';
import Tracker from '../screens/tracker';
import { translate } from '../i18n';
// Tabs
import Tab from '../../src/screens/profile/Tab2';
// Guest Register
import GuestRegister from '../../src/screens/guestRegister/GuestRegister';
import RegisterUser from '../../src/screens/facbookLogin/registerUser';
import LocalData from "../components/LocalData";
import {parse} from "react-native-svg";
import Upgrade from '../screens/upgradeScreen';


export const AppNavigator = StackNavigator({
  Upgrade : { screen: Upgrade },

  Root: { screen: Root },
  Intro: { screen: Intro },
  Login: { screen: Login },
  SignUp: { screen: SignUp },
  Confirmation: { screen: Confirmation },
  GuestRegister: { screen: GuestRegister },
  RegisterUser: { screen: RegisterUser },
  Menu: { screen: Menu },
  Detail: { screen: Detail },
  Complain: { screen: Complain },
  ContactManagement: { screen: ContactManagement },
  SearchResult: { screen: SearchResult },
  ChatSingle: { screen: ChatSingle },
  MemberList: { screen: MemberList },
  NotificationDetail: { screen: NotificationDetail },
  TimeTracking: { screen: TimeTracking },
  TermOfService: { screen: TermOfService },
  PrivacyPolicy: { screen: PrivacyPolicy },
  ForgotPassword: { screen: ForgotPassword },
  EditProfile: { screen: EditProfile },
  VerifyId: { screen: VerifyId },
  Balance: { screen: Balance },
  ConfirmUpload: { screen: ConfirmUpload },
  // Top Up
  TopUp: { screen: TopUp },
  PaypalView: { screen: PaypalView },
  RevenutMonster: { screen: RevenutMonster },
  TopUpSuccess: { screen: TopUpSuccess },
  Withdraw: { screen: Withdraw },
  CreditCard: { screen: CreditCard },
  CreditCardTop: { screen: CreditCardTop },
  QrcodeResult: { screen: QrcodeResult },
  Deposit: { screen: Deposit },
  // Profile
  Profile: {
    screen: Profile,
    navigationOptions: {
      header: null,
    },
  },
  SpaceDetail: {
    screen: SpaceDetail,
  },
  MySpaces: {
    screen: MySpaces,
  },
  Notification: { screen: NotificationScreen },
  Dashboard: { screen: Dashboard },
  // Office
  QRScanner: { screen: QRScanner },
  SelectSeat: { screen: SelectSeat },
  SeatStatus: { screen: SeatStatus },
  SharedOfficeReview: { screen: SharedOfficeReview },
  OnSuccessReview: { screen: OnSuccessReview },
  SharedOfficeBarCodeScan: { screen: SharedOfficeBarCodeScan },
  NotificationSettings: {
    screen: NotificationSettingsNew,
    navigationOptions: {
      header: null,
    }
   },
  SharedOfficeFilters: {
    screen: SharedOfficeFilters,
   },
  SharedOfficeFiltersMaps: {
    screen: FiltersMap
   },

  CheckContact: { screen: CheckContact },
  IndoorChat: { screen: IndoorChat },
  UserProfile: { screen: UserProfile },
  // jobs list Pages
  JobsList: { screen: JobsList },
  ProjectDetail: { screen: ProjectDetail },
  OfficialProjectDetail: { screen: OfficialProjectDetail },
  ProjectDetailInfo: { screen: ProjectDetailInfo },
  MyProfile: { screen: MyProfile },
  Verify: { screen: VerifyPhoto },
  VerifyPhoto: { screen: VerifyPhoto },
  VerifyInfo: { screen: VerifyInfo },
  Verify_Info_Success: { screen: VerifyInfoSuccess },
  Verify_Photo_Success: { screen: VerifyPhotoSuccess },
  Tracker: { screen: Tracker },
  PaymentRecord: { screen: PaymentRecord },
  SelectChat: { screen: SelectChat },
  FilterResults: { screen: FilterResult },
  ChatDetail: { screen: ChatDetail },
  QrUser: { screen: QrUser },
  UpdateProfile: { screen: UpdateProfile },
  ReceivedOrder: { screen: ReceivedOrder },
  Tab: { screen: Tab },

  // Indoor Chat
  JoinedChatRooms: { screen: JoinedChatRooms },

  // About My Profile
  About: { screen: About },
  QRDetail: { screen: QRDetail },

  // Other User Profile
  OtherUserProfile: { screen: OtherUserProfile },
  AboutOtherUser: { screen: AboutOtherUser },

  // Select Seat
  SeatSelectSuccess: { screen: SeatSelectSuccess },

  // Bind Facbook
  BindFacebook: { screen: bindFacebook },
}, {
  initialRouteName: 'Root',
  transitionConfig: () => ({
    transitionSpec: {
      duration: 0,
      timing: Animated.timing,
      easing: Easing.step0,
    },
  }),
}, {
  mode: 'card',
});
// completed 70%
// Wrapping Navigator inside React Component

class AppWrapper extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      fcm_token: '',
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    Pushy.listen();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {

    const { dispatch, nav } = this.props;
    if (nav.index === 0) {
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    return (
        <SafeAreaView style={{ flex: 1 }}>
          <AppNavigator/>
        </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
});

Platform.OS==='android' ? Pushy.setNotificationIcon('icon') : null;

Pushy.setNotificationListener(async (data) => {
  // Print notification payload data
  if(Platform.OS==='android') {
    let parseData = '';
    if (data.sharedOffice === true) {
      if (data.data !== null || data.data !== 'null') {
        parseData = JSON.parse(data.data);
        // Notification title
        const notificationTitle = translate('xenren_notification');
        // Pushy.setNotificationIcon('ic_launcher');
        const notificationText = parseData.toString() || translate('received_new_message');
        // Display basic system notification
        Pushy.notify(notificationTitle, notificationText);
      }
    } else {
      try {
        parseData = JSON.parse(data.data);
      } catch (e) {
        console.log('error' + e);
      }
      console.log('LocalData');
      let userData = LocalData.getUserData().then((response) => {
        const resp = JSON.parse(response);
        return resp;
      });
      let length = [];
      console.log(parseData);
      console.log(parseData !== 'undefined');
      console.log(parseData !== undefined);
      console.log(parseData.to_user_id !== undefined);
      if(parseData !== 'undefined' || parseData !== undefined || parseData.to_user_id !== undefined || parseData) {
        if(parseData.to_user_id === userData.id){
            length.push(userData.id);
        }
        console.log(length && length.length === 1);
        if(length && length.length === 1) {
            // Notification title
            const notificationTitle = translate('received_new_message');
            // Pushy.setNotificationIcon('ic_launcher'); // set notification icon here Path drawable folder
            const notificationText = parseData.custom_message || translate('xenren_notification');
            // Display basic system notification
            Pushy.notify(notificationTitle, notificationText);
        }
      }
    }
  } else {
    // alert(JSON.stringify(data));
    // Pushy.notify('okie', 'dokie');
  }
});

// Connect React Component to Redux
export default connect(mapStateToProps)(AppWrapper);
