import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert, View, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import LocalData from './components/LocalData';
import HttpRequest from './components/HttpRequest';


class Root extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    setRoot: PropTypes.func,
    userLogin: PropTypes.func,
    sharedOffice: PropTypes.func,
  };

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.props.setRoot(this);

    this.state = {
      page: 'loading',
    };
  }

  componentDidMount() {
    console.log('Root componentDidMount');
    LocalData.getUserData().then((response) => {
      console.log('response', response);
      if (response === null || response === 'null') {
        setTimeout(() => {
          this.props.navigation.dispatch(NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({
                routeName: 'Intro',
              }),
            ],
          }));
        }, 750);
      } else {
        const json = JSON.parse(response);
        if (json.token == null) {
          setTimeout(() => {
            this.props.navigation.dispatch(NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'Intro',
                }),
              ],
            }));
          }, 750);
        } else {
          HttpRequest.me(json.token).then((response) => {
            const result = response.data;
            this.props.userLogin(result.data, json.token, result.seat_number);
            if (result.shared_office !== null || result.shared_office !== 'null') {
              this.props.sharedOffice(result.shared_office);
            }
            LocalData.setUserData({
              ...result.data,
              token: json.token,
              seat_number: result.seat_number,
            });
            if (result.status === 'success') {
              this.props.navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'Dashboard',
                  }),
                ],
              }));
            } else if (result.error === 'Token is Expired' || result.error === 'Token is Invalid') {
              LocalData.setUserData(null);
              LocalData.setAppData(null);
              LocalData.setfileData(null);
              LocalData.setfile1Data(null);
              LocalData.setVerifyData(null);
              this.props.navigation.dispatch(NavigationActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'Login',
                  }),
                ],
              }));
            } else {
              Alert.alert(result.error);
            }
          }).catch(() => {
            this.props.navigation.dispatch(NavigationActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({
                  routeName: 'Login',
                }),
              ],
            }));
          });
        }
      }
    });
  }

  // eslint-disable-next-line class-methods-use-this
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    component: state.component,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
    userLogin: (userData, token) => dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
    sharedOffice: (sharedOffice) => dispatch({
      type: 'SHARED_OFFICE',
      data: sharedOffice,
    }),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Root);
