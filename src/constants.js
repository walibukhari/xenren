import keymirror from 'keymirror';

export default keymirror({
  set_root: null,
  FCM_TOKEN: null,
  LOGIN_SUCCESS: null,
  SHARED_OFFICE: null,
  SET_USER_DATA: null,
  LOGIN_FAILED: null,
  UPLOAD_START: null,
  UPLOAD_STOP: null,
  UPLOAD_DONE: null,
  UPLOAD_ERROR: null,
  USER_VERIFIED_SUCCESS: null,
  CURRENT_QRCODE: null,
  SELECTED_SEAT_DATA: null,
  UPDATE_PROFILE_DATA: null
});
