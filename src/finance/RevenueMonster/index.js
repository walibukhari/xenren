/* eslint-disable react/prop-types */
/* eslint-disable camelcase */
import React, { Component } from 'react';
import {
  WebView,
  View,
  Text,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import webViewStyles from './style';
import Config from '../../Config';
import FontStyle from '../../constants/FontStyle';
import { translate } from '../../i18n';

const { width, height } = Dimensions.get('window');

class RevinueMonsterView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerTitle: '',
      isLoading: false,
    };
  }

  // componentDidMount() {
  //   console.log('mounted');
  // }

  onNavigationStateChange = (webViewState) => {
    const self = this;
    this.setState({ headerTitle: webViewState.title });
    if (webViewState.url.includes('login')) {
      this.setState({ isLoading: true });
      setTimeout(() => {
        self.props.navigation.navigate('Root');
      }, 4000);
    // alert('payment success')
    // this.getPaymentDetails();
    }
  };

  // getPaymentDetails = () => {
  //   const { state } = this.props.navigation;
  //   HttpRequest.showPayment(state.params.RevenueMonsterToken, state.params.payment_id)
  //     .then((response) => {
  //       this.executePayment(
  //         state.params.RevenueMonsterToken,
  //         state.params.payment_id,
  //         response.data.payer.payer_info.payer_id,
  //       );
  //     })
  //     .catch((error) => {
  //       alert(error);
  //     });
  // };

  // executePayment = (RevenueMonsterToken, payment_id, payer_id) => {
  //   HttpRequest.executePayment(RevenueMonsterToken, payment_id, payer_id)
  //     .then((response) => {
  //       this.topUpBalance(response.data);
  //     })
  //     .catch((error) => {
  //       alert('Error occurred. Please try again');
  //     });
  // };

  // topUpBalance = (data) => {
  //   console.log('execute respones ---------- ', data);
  //   const amount = data.transactions[0].amount.details.subtotal;
  //   const { tax } = data.transactions[0].amount.details;
  //   const body = {
  //     transactions: { amount, tax },
  //     id: data.id,
  //     intent: 'sale',
  //     state: 'approved',
  //     cart: '6A5040639D691635B',
  //     payer: data.payer,
  //     payee: data.transactions[0].payee,
  //     result: null,
  //   };
  //   HttpRequest.topUpBalance(this.props.userInfo.token, body, amount)
  //     .then((response) => {
  //       if (response.data.status === 'success') {
  //         this.getUserData(amount);
  //       }
  //     })
  //     .catch((error) => {
  //       alert(error);
  //     });
  // };


  // getUserData = (amount) => {
  //   HttpRequest.me(this.props.userInfo.token)
  //     .then((response) => {
  //       if (
  //          isNaN(this.props.userInfo.account_balance) === true
  //          || this.props.userInfo.account_balance === null
  //          || this.props.userInfo.account_balance === 'null'
  //       ) {
  //          this.props.userInfo.account_balance = parseFloat(amount) + parseFloat(0);
  //       } else {
  //         this.props.userInfo.account_balance =
  //            parseFloat(amount) + parseFloat(this.props.userInfo.account_balance);
  //       }
  //       this.setState({ isLoading: false });
  //       this.props.navigation.navigate(
  //          'TopUpSuccess',
  //          { amount, totalBalance: response.data.balance }
  //        );
  //     })
  //     .catch((error) => {
  //       this.setState({ isLoading: false });
  //       // console.log('error ----------- ', error);
  //     });
  // };

  render() {
    const { state } = this.props.navigation;
    const { isLoading } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <View style={webViewStyles.container}>
          {isLoading ? (
            <View
              style={{
                position: 'absolute',
                top: height / 2 - 160,
                left: 50,
                width: width - 100,
                height: 160,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <ActivityIndicator size="large" color={Config.primaryColor} />
              <Text
                style={{
                  fontSize: 20,
                  margin: 15,
                  textAlign: 'center',
                  fontFamily: FontStyle.Medium,
                }}
              >
                {translate('do_not_press_back_button')}
              </Text>
            </View>
          ) : (
            <WebView
              javaScriptEnabled={true}
              startInLoadingState={true}
              source={{ uri: state.params.RevenutMonsterLink }}
              onNavigationStateChange={this.onNavigationStateChange.bind(this)}
            />
          )}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    userInfo: state.auth.userData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userLogin: (userData, token) => dispatch({
      type: 'LOGIN_SUCCESS',
      data: { ...userData, token },
    }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RevinueMonsterView);
