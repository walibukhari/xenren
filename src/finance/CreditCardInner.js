/* eslint-disable no-dupe-keys */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  TextInput,
} from 'react-native';
import Config from '../Config';
import Input from '../screens/profile/Input';

class CreditCardInner extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    action: PropTypes.string.isRequired,
    onExpiryDate: PropTypes.func.isRequired,
    expiryDateVal: PropTypes.string.isRequired,
    navigation: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    const withdrawalSources = [
      {
        id: 1,
        name: 'WeChat Pay',
        type: 'Recharge',
        logo: require('../../images/other/icon_wechat_pay.png'),
      },
      {
        id: 2,
        name: 'Ali Pay',
        type: 'Recharge',
        logo: require('../../images/other/icon_ali_pay.png'),
      },
      {
        id: 3,
        name: 'Paypal',
        type: 'Recharge',
        logo: require('../../images/other/paypal-logo.png'),
      },
      {
        id: 4,
        name: 'Credit Card',
        type: 'Recharge',
        logo: require('../../images/other/card.png'),
      },
    ];

    this.state = {
      paymentGateways: [],
      withdrawalSources,
      activePay: 'WeChat Pay',
      paypalAccessToken: null,
    };
  }

  renderRow = () => (
    <View style={styles.boxInsideStyle}>
      <View style={styles.amountInputWrapper}>
        <Text style={styles.amountInputTitle}>Amount</Text>
        <TextInput
          underlineColorAndroid="transparent"
          placeholder="Enter Amount to withdraw"
        />
      </View>
    </View>
  );

  render() {
    return (
      <View>
        <View
          style={{
            marginTop: 5,
            paddingHorizontal: 8,
          }}
        >
          <Input
            title="CREDIT CARD NUMBER"
            value={this.state.nickName}
            keyboardType="numeric"
            underlineColorAndroid="transparent"
            placeholder="XXXXXXXXXXXXXXX"
            maxLength={16}
          />
          <Input
            title="EXPIRY DATE"
            maxLength={5}
            value={this.props.expiryDateVal}
            keyboardType="numeric"
            underlineColorAndroid="transparent"
            placeholder="MM/YY"
            onChangeText={(onExpiryDate) => {
              this.props.onExpiryDate = onExpiryDate;
            }}
          />
          <Input
            title="CVV CODE"
            maxLength={4}
            keyboardType="numeric"
            underlineColorAndroid="transparent"
            placeholder="Please Enter 4 digit code"
          />
        </View>
        <View style={styles.footerWrapper}>
          <Text style={styles.footerTextDescription}>
            Click to pay to indicate that you have read and agreed to recharging
            agreement
          </Text>
          <TouchableOpacity
            onPress={null}
            style={styles.buttonWrapper}
          >
            <Text
              style={{
                color: '#fff',
                fontFamily: 'Montserrat-Medium',
                fontSize: 20,
              }}
            >
              {this.props.action}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  footerWrapper: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerTextDescription: {
    justifyContent: 'center',
    textAlign: 'center',
    color: '#888888',
    fontFamily: 'Montserrat-Light',
    fontSize: 15,
  },
  buttonWrapper: {
    backgroundColor: Config.primaryColor,
    height: 40,
    width: '100%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rootStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  topWrapperStyle: {
    backgroundColor: '#f4f4f4',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  normalTopWrapper: {
    backgroundColor: '#5fc229',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: 'Montserrat-Medium',
    fontSize: 32,
    color: '#000',
  },
  priceDescription: {
    fontFamily: 'Montserrat-Light',
    fontSize: 20,
    color: '#5c5c5c',
    marginTop: 5,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },
  balanceText: {
    color: '#fff',
    fontSize: 24,
  },
  amountInputWrapper: {
    margin: 5,
    borderWidth: 1,
    borderColor: '#e6e6e6',
  },
  amountInputTitle: {
    color: '#979797',
    backgroundColor: '#f6f6f6',
    padding: 2,
  },
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});

export default CreditCardInner;
