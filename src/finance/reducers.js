import { RECEIVE_DETAILS } from './actions';

const initialState = {
  balance: 0,
  points: 0,
};


const finance = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_DETAILS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};

export default finance;
