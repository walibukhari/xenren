
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View, Text, Image, ScrollView, TouchableOpacity, TextInput, StyleSheet,
} from 'react-native';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import Payment from './Payment';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';

const icWithdraw = require('../../images/other/withdraw.png');

class Withdraw extends Component {
  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;
    return {
      title: translate('withdraw'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle: {
        color: Config.topNavigation.headerTextColor,
        alignSelf: 'center',
        fontFamily: FontStyle.Regular,
        width: '100%',
      },
      headerLeft: (
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}
        >
          <SimpleLineIcons
            size={16}
            name="arrow-left"
            color={Config.topNavigation.headerIconColor}
          />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      activeButtonIndex: 4,
      customAmount: '',
      topUpAmount: '200',
    };
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    balance: PropTypes.number,
    userData: PropTypes.object,
  };

  renderTopView = () => {
    // console.log('Getting state data ------ >  ', this.state, this.props);
    return (
      <View style={styles.topWrapperStyle}>
        <Image
          source={icWithdraw}
          resizeMode="contain"
          style={{
            width: 120,
            height: 100,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            marginTop: 15,
          }}
        >
          <Text style={styles.priceTextStyle}> {translate('withdraw')}</Text>
          <Text style={styles.priceTextStyle}>199</Text>
          <Text style={styles.priceTextStyle}>¥</Text>
        </View>
        <Text style={styles.priceDescription}>
          {translate('withdraw_deposit_anytime')}
        </Text>
      </View>
    );
  };

  render() {
    const { activeButtonIndex } = this.state;
    return (
        <View style={styles.rootStyle}>
          <ScrollView>
            <View style={styles.balanceWrapperStyle}>
              <Text style={styles.currentBalanceText}>{translate('current_balance')}</Text>
              <View style={styles.balanceBox}>
                <Text style={styles.currentBalanceText}>{this.props.userData.account_balance ? this.props.userData.account_balance : 0}{' '}{translate('usd')}</Text>
              </View>
            </View>
            <View style={styles.selectPriceWrapper}>
              <View style={styles.priceBox}>
                <TouchableOpacity
                    style={[
                      styles.button,
                      styles.smallButton,
                      activeButtonIndex === 1 && styles.activeButton,
                    ]}
                    onPress={() => this.setState({ activeButtonIndex: 1, topUpAmount: '30', customAmount: '' })}>
                  <Text
                      style={[
                        styles.buttonTextStyle,
                        activeButtonIndex === 1 && styles.activebuttonTextStyle,
                      ]}>
                    30{' '}{translate('usd')}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[
                      styles.button,
                      styles.smallButton,
                      activeButtonIndex === 2 && styles.activeButton,
                    ]}
                    onPress={() => this.setState({ activeButtonIndex: 2, topUpAmount: '50', customAmount: '' })}>
                  <Text
                      style={[
                        styles.buttonTextStyle,
                        activeButtonIndex === 2 && styles.activebuttonTextStyle,
                      ]}>
                    {' '}
                    50{' '}{translate('usd')}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.priceBox, styles.priceBoxRaised]}>
                <TouchableOpacity
                    style={[
                      styles.button,
                      styles.smallButton,
                      activeButtonIndex === 3 && styles.activeButton,
                    ]}
                    onPress={() => this.setState({ activeButtonIndex: 3, topUpAmount: '100', customAmount: '' })}>
                  <Text
                      style={[
                        styles.buttonTextStyle,
                        activeButtonIndex === 3 && styles.activebuttonTextStyle,
                      ]}>
                    100{' '}{translate('usd')}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[
                      styles.button,
                      styles.smallButton,
                      activeButtonIndex === 4 && styles.activeButton,
                    ]}
                    onPress={() => this.setState({ activeButtonIndex: 4, topUpAmount: '200', customAmount: '' })}>
                  <Text
                      style={[
                        styles.buttonTextStyle,
                        activeButtonIndex === 4 && styles.activebuttonTextStyle,
                      ]}>
                    {' '}
                    200{' '}{translate('usd')}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.priceBox, styles.priceBoxRaised]}>
                <TouchableOpacity
                    style={[
                      styles.button,
                      styles.largeButton,
                      activeButtonIndex === 5 && styles.activeButton,
                    ]}
                    onPress={() => {
                      this.setState({ activeButtonIndex: 5 });
                      this.custom.focus();
                    }}>
                  <Text
                      style={[
                        styles.buttonTextStyle,
                        activeButtonIndex === 5 && styles.activebuttonTextStyle,
                      ]}>
                    {translate('custom_amount')} :
                  </Text>
                  <TextInput
                      ref={(input) => {
                        this.custom = input;
                      }}
                      maxLength={5}
                      keyboardType='numeric'
                      underlineColorAndroid='transparent'
                      value={this.state.customAmount}
                      onFocus={() => this.setState({ activeButtonIndex: 5 })}
                      onChangeText={(text) => {
                        this.setState({ customAmount: text, activeButtonIndex: 5 });
                      }}
                      style={styles.customAmount}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <Payment
                action={translate('WITHDRAW')}
                navigation={this.props.navigation}
                amount={this.state.customAmount !== '' ? this.state.customAmount : this.state.topUpAmount}
            />
          </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },
  balanceWrapperStyle: {
    width: '100%',
    height: 55,
    marginTop: 8,
    backgroundColor: '#2e404e',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
  },
  currentBalanceText: {
    color: '#ffffff',
    fontFamily: 'Montserrat-Light',
    fontSize: 15,
    fontWeight: '300',
  },
  balanceBox: {
    backgroundColor: '#25333e',
    width: 110,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  selectPriceWrapper: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  priceBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  priceBoxRaised: {
    marginTop: 10,
  },
  customAmount: {
    marginLeft: 10,
    height: 40,
    width: 50,
    color: Config.white,
    fontSize: 16,
    fontFamily: FontStyle.Regular,
    // fontWeight: '00',
  },
  button: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f4f4f4',
  },
  activeButton: {
    backgroundColor: Config.primaryColor,
    color: '#fff',
  },

  smallButton: {
    width: '47%',
  },

  largeButton: {
    width: '100%',
    flexDirection: 'row',
  },

  buttonTextStyle: {
    fontFamily: 'Montserrat-Light',
    color: '#000000',
    fontSize: 15,
  },

  activebuttonTextStyle: {
    color: '#ffffff',
  },
});

const mapStateToProps = (state) => ({
  component: state.component,
  userData: state.auth.userData,
  // balance: state.finance,
});

export default connect(mapStateToProps)(Withdraw);
