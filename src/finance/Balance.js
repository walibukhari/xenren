/* eslint-disable class-methods-use-this */
/* eslint-disable prefer-const */
/* eslint-disable no-console */
/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
/* eslint-disable no-tabs */
/* eslint-disable global-require */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator, Dimensions, Alert,
} from 'react-native';
import FontAwesomeIcons, {FA5Style} from 'react-native-vector-icons/FontAwesome5';
import { NavigationActions } from 'react-navigation';
import Config from '../Config';
import { loadFinanceDetails } from './actions';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';
import LocalData from '../components/LocalData';
import FontStyle from '../constants/FontStyle';

const { height } = Dimensions.get('window');

class Balance extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    points: PropTypes.number.isRequired,
    loadInitialData: PropTypes.func.isRequired,
  };
    static navigationOptions = {
        header: null,
    };

  constructor(props) {
    super(props);
    this.state = {
      balance: null,
      balanceLoading: true,
      userBalance: '...',
      isLoading: true,
    };
  }

  handlePaymentRecord = () => {
    this.props.navigation.navigate('PaymentRecord');
  };

  handleDeposit = () => {
    this.props.navigation.navigate('Deposit', { securityDeposit: false });
  };
    goBack = () => {
        this.props.navigation.goBack();
    };
  handleSecurityDeposit = () => {
    const { navigation } = this.props;
    navigation.navigate('Deposit', { securityDeposit: false });
  };

  handleTopUp = () => {
    this.props.navigation.navigate('TopUp');
  };

  componentDidMount() {
      console.log('Balance.js src/finance/balance.js');
    if (this.props.userInfo.token) {
      HttpRequest.me(this.props.userInfo.token)
        .then((res) => {
          const balance = res.data.data.account_balance
            ? res.data.data.account_balance.toFixed(2)
            : 0;
          if (res.data.status === 'success') {
            this.setState({
              userBalance: balance,
              balanceLoading: false,
              isLoading: false,
            });
          } else {
            Alert.alert('Error', res.data.status, [
              {
                text: 'OK',
              },
            ]);
            this.setState({ balanceLoading: false, isLoading: false });
          }
        })
        .catch(() => {
          Alert.alert('Error', translate('network_error'), [
            {
              text: 'OK',
            },
          ]);
          this.setState({ balanceLoading: false, isLoading: false });
        });
      // Set header right button action
      this.props.navigation.setParams({
        handlePayment: this.handlePaymentRecord,
      });
    } else {
      setTimeout(() => {
        Alert.alert(
          'Warning',
          'Token expire!',
          [
            {
              text: 'OK',
              onPress: () => {
                this.logout();
              },
            },
          ],
        );
      }, 200);
    }
  }

  logout() {
    LocalData.setUserData(null);
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Login',
        }),
      ],
    }));
  }

  renderBallancePointSize(value) {
    const val = value.toString();

    if (val.length >= 9) return 12;
    if (val.length >= 7) return 22;
    if (val.length >= 5) return 32;
    return 42;
  }

  render() {
    const { isLoading, userBalance } = this.state;

    if (isLoading === true) {
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: height / 2 }}>
          <ActivityIndicator/>
        </View>
      );
    }

    return (
        <View style={styles.rootStyle}>
            <View style={style.rootStyle}>
                {/* Back Button View */}
                <View style={style.navigationStyle}>
                    <View style={style.navigationWrapper}>
                        <TouchableOpacity
                            onPress={this.goBack}
                            style={{
                                marginTop: 8,
                                marginLeft: 10,
                                flexDirection: "row",
                            }}
                        >
                            <Image
                                style={{width:20,height:26,position:'relative',top:-3}}
                                source={require('../../images/arrowLA.png')}
                            />

                            <Text
                                style={{
                                    marginLeft: 10,
                                    marginTop:-1,
                                    fontSize: 19.5,
                                    fontWeight:'normal',
                                    color: Config.primaryColor,
                                    fontFamily: FontStyle.Regular,
                                }}
                            >
                                {translate("back")}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={styles.mainViewportStyle}>
                <View style={styles.itemDividerStyle}>
                    <View style={[styles.itemStyle, styles.itemStyleGreen]}>
                        <View style={styles.iconWrapper}>
                            <Image
                                source={require('../../images/notification/new-icons/wallet.png')}
                                style={styles.iconStyles}
                            />
                        </View>
                        <Text
                            style={styles.titleText}
                        >
                            {'WALLET BALANCE'}
                        </Text>
                        <View style={{
                          paddingHorizontal: 20,
                          marginBottom: 15,
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'flex-end',
                        }}>
                            <Text
                                style={{
                                  color: '#FFF',
                                  fontSize: 22,
                                  fontWeight: '600',
                                  textAlign: 'left',
                                  alignSelf: 'flex-start',
                                }}
                            >
                              RM
                            </Text>
                            <Text
                                style={{
                                  color: '#FFF',
                                  fontSize: this.renderBallancePointSize(userBalance),
                                  fontWeight: '600',
                                  paddingHorizontal: 5,
                                  textAlign: 'left',
                                }}
                            >
                              {userBalance}
                            </Text>
                        </View>
                    </View>
                    <View style={[styles.itemStyle, styles.itemStyleDark]}>
                        <View style={styles.iconWrapper}>
                            <Image
                                source={require('../../images/notification/new-icons/xen.png')}
                                style={styles.iconStyles}
                            />
                        </View>
                        <Text
                            style={styles.titleText}
                        >
                            {translate('XEN_POINTS')}
                        </Text>
                        <View style={{
                          paddingHorizontal: 20,
                          marginBottom: 15,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'flex-end',
                        }}>
                            <Text
                                style={{
                                  color: '#FFF',
                                  fontSize: this.renderBallancePointSize(this.props.points),
                                  fontWeight: '600',
                                  paddingHorizontal: 0,
                                  textAlign: 'left',
                                }}
                            >
                                {this.props.points}
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={{
                  backgroundColor: '#f1ffe9',
                  padding: 15,
                  borderRadius: 20,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                    <View style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                        <Image
                            source={require('../../images/notification/new-icons/bank.png')}
                            style={{
                              width: 20,
                              height: 20,
                              tintColor: Config.primaryColor,
                              marginRight: 7,
                            }}
                        />
                        <Text
                            style={{
                              color: Config.primaryColor,
                              fontSize: 12,
                              marginRight: 10,
                              fontWeight: 'bold',
                            }}
                        >
                            {'WITHDRAW BALANCE'}
                        </Text>
                    </View>
                    <View style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'flex-end',
                    }}>
                        <Text
                            style={{
                              color: Config.primaryColor,
                              fontSize: 20,
                              fontWeight: 'bold',
                            }}
                        >
                          RM {' '}
                        </Text>
                        <Text
                            style={{
                              color: Config.primaryColor,
                              fontSize: 22,
                              fontWeight: 'bold',
                            }}
                        >
                          3000
                        </Text>
                    </View>
                </View>
                <TouchableOpacity style={{ marginTop: 15 }} onPress={this.handleTopUp}>
                    <View style={{
                      backgroundColor: '#f1ffe9',
                      padding: 15,
                      borderRadius: 20,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                            <Image
                                source={require('../../images/notification/new-icons/money-yen.png')}
                                style={{
                                  width: 20,
                                  height: 20,
                                  resizeMode: 'contain',
                                  tintColor: Config.primaryColor,
                                  marginRight: 7,
                                }}
                            />
                            <Text
                                style={{
                                  color: Config.primaryColor,
                                  fontSize: 12,
                                  marginRight: 10,
                                  fontWeight: 'bold',
                                }}
                            >
                                {'TOP UP'}
                            </Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'flex-end',
                        }}>
                            <FontAwesomeIcons
                                size={16}
                                name="chevron-right"
                                color={Config.primaryColor}
                            />
                        </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginTop: 15 }}
                  onPress={this.handleSecurityDeposit}>
                    <View style={{
                      backgroundColor: '#f1ffe9',
                      padding: 15,
                      borderRadius: 20,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                            <Image
                                source={require('../../images/notification/new-icons/withdraw.png')}
                                style={{
                                  width: 20,
                                  height: 20,
                                  resizeMode: 'contain',
                                  tintColor: Config.primaryColor,
                                  marginRight: 7,
                                }}
                            />
                            <Text
                                style={{
                                  color: Config.primaryColor,
                                  fontSize: 12,
                                  marginRight: 10,
                                  fontWeight: 'bold',
                                }}
                            >
                                {'WITHDRAW'}
                            </Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'flex-end',
                        }}>
                            <FontAwesomeIcons
                                size={16}
                                name="chevron-right"
                                color={Config.primaryColor}
                            />
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
    // return
  } // render
} // Withdraw

const style = {
    rootStyle: {
        backgroundColor: "white",
        // paddingHorizontal: 10
    },

    navigationWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        width: "100%",
    },
    navigationStyle: {
        height: 55,
        flexDirection: "row",
        marginTop: 10,
        paddingHorizontal: 5,
        // alignItems:'center'
    },
}

const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  mainViewportStyle: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '90%',
    paddingVertical: 30,
  },

  itemDividerStyle: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginBottom: 20,
  },

  itemStyle: {
    width: '47%',
    height: '100%',
    flexDirection: 'column',
    textAlign: 'center',
    borderRadius: 20,
    overflow: 'hidden',
    marginHorizontal: '1%',
  },

  itemStyleGreen: {
    backgroundColor: Config.primaryColor,
  },

  itemStyleDark: {
    backgroundColor: '#36404f',
  },

  iconWrapper: {
    width: 100,
    height: 100,
    backgroundColor: 'rgba(255, 255, 255, .3)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    left: -20,
    top: -15,
  },

  iconStyles: {
    width: 40,
    height: 40,
    tintColor: '#FFF',
    resizeMode: 'contain',
    marginLeft: 10,
    marginTop: 10,
  },

  titleText: {
    color: '#FFF',
    fontSize: 12,
    fontWeight: '700',
    paddingHorizontal: 20,
    marginBottom: 10,
    textAlign: 'left',
  },

  balanceStyle: {
    color: Config.primaryColor,
    fontSize: 25,
    marginRight: 10,
    fontWeight: 'bold',
  },
};

const mapDispatchToProps = (dispatch) => ({
  setRoot: (root) => dispatch({
    type: 'set_root',
    root,
  }),
  loadInitialData: () => dispatch(loadFinanceDetails()),
});
const mapStateToProps = (state) => ({
  userInfo: state.auth.userData,
  component: state.component,
  points: 20,
  // balance: 20,
});

export default connect(mapStateToProps, mapDispatchToProps)(Balance);
