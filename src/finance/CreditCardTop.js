/* eslint-disable no-undef */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import CreditCardInner from './CreditCardInner';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';

class CreditCardTop extends Component {
  static propTypes = {
    balance: PropTypes.number.isRequired,
    navigation: PropTypes.object.isRequired,
  }

  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;
    return {
      title: translate('creditCard'),
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      activeButtonIndex: 5,
    };
  }

  render() {
    const { activeButtonIndex } = this.state;
    // console.log('credit card top ------------- ', this.props);
    return (
      <View style={styles.rootStyle}>
        <ScrollView>
          <View style={styles.balanceWrapperStyle}>
            <Text style={styles.currentBalanceText}>{translate('current_balance')}</Text>
            <View style={styles.balanceBox}>
              <Text style={styles.currentBalanceText}>{`${this.props.balance} USD`}</Text>
            </View>
          </View>
          <View style={styles.selectPriceWrapper}>
            <View style={[styles.priceBox, styles.priceBoxRaised]}>
              <TouchableOpacity
                style={[
                  styles.button,
                  styles.largeButton,
                  activeButtonIndex === 5 && styles.activeButton,
                ]}
                onPress={() => this.setState({ activeButtonIndex: 5 })}>
                <Text
                  style={[
                    styles.buttonTextStyle,
                    activeButtonIndex === 5 && styles.activebuttonTextStyle,
                  ]}>
                  Custom Amount
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <CreditCardInner action="To Pay" navigation={this.props.navigation}/>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  balanceWrapperStyle: {
    width: '100%',
    height: 55,
    backgroundColor: '#2e404e',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 20,
  },
  currentBalanceText: {
    color: '#fff',
    fontFamily: 'Montserrat-Light',
    fontSize: 15,
    fontWeight: '300',
  },
  balanceBox: {
    backgroundColor: '#25333e',
    width: 110,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  selectPriceWrapper: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  priceBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  priceBoxRaised: {
    marginTop: 10,
  },
  button: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f4f4f4',
  },
  activeButton: {
    backgroundColor: Config.primaryColor,
    color: '#fff',
  },

  smallButton: {
    width: '47%',
  },

  largeButton: {
    width: '100%',
  },

  buttonTextStyle: {
    fontFamily: 'Montserrat-Light',
    color: '#000',
    fontSize: 15,
  },

  activebuttonTextStyle: {
    color: '#fff',
  },
});

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
    loadInitialData: () => dispatch(loadFinanceDetails()),
  };
}

const mapStateToProps = (state) => ({
  component: state.component,
  userData: state.auth.userData,
  balance: state.finance ? state.finance.balance : 0,
  // balance: 20,
});

export default connect(mapStateToProps, mapDispatchToProps)(CreditCardTop);
