/* eslint-disable space-infix-ops */
/* eslint-disable prefer-template */
/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import CreditCardInner from './CreditCardInner';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';

class CreditCard extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Credit Card',
      headerTintColor: Config.topNavigation.headerIconColor,
      headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
      headerLeft:
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            marginLeft: 10,
            flexDirection: 'row',
            alignSelf: 'center',
            padding: 5,
          }}>
          <SimpleLineIcons size={16} name="arrow-left"
                           color={Config.topNavigation.headerIconColor}/>
        </TouchableOpacity>,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      expiryDate: '',
    };
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    balance: PropTypes.number.isRequired,
  };

  renderTopView = () => {
    // console.log('--------**********----------- ', this.props);
    return (
        <View style={styles.topWrapperStyle}>
          <Image
            source={require('../../images/other/withdraw.png')}
            resizeMode="contain"
            style={{ width: 100, height: 80 }}
          />
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <Text style={styles.priceTextStyle}>Withdraw</Text>
            {/* <Text style={styles.priceTextStyle}>{this.props.balance}</Text> */}
            <Text style={styles.priceTextStyle}>
              {this.props.navigation.state.params.topUpAmount}
            </Text>
            <Text style={styles.priceTextStyle}>USD</Text>
          </View>
          <Text style={styles.priceDescription}>
            {translate('withdraw_deposit_anytime')}
          </Text>
        </View>
    );
  }

  changeExpiryDate = (text) => {
    let slashedText = text;
    if (slashedText.length === 2) {
      // slashedText = text +'/';
      if (this.state.expiryDate.indexOf('/') === -1) {
        slashedText = text +'/';
      }
    }
    this.setState({ expiryDate: slashedText });
  }

  render() {
    // console.log("props in credit card ---------- ", this.props);
    return (
      <View style={styles.rootStyle}>
        <ScrollView>
          { this.renderTopView() }
          <CreditCardInner action="WITHDRAW"
            onExpiryDate={(text) => this.changeExpiryDate(text)}
            expiryDateVal={this.state.expiryDate}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  topWrapperStyle: {
    backgroundColor: '#f4f4f4',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  normalTopWrapper: {
    backgroundColor: '#5fc229',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: 'Montserrat-Medium',
    fontSize: 28,
    color: '#000',
  },
  priceDescription: {
    fontFamily: 'Montserrat-Light',
    fontSize: 16,
    color: '#5c5c5c',
    marginTop: 5,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },
  balanceText: {
    color: '#fff',
    fontSize: 24,
  },
  amountInputWrapper: {
    margin: 10,
    borderWidth: 1,
    borderColor: '#e6e6e6',
  },
  amountInputTitle: {
    color: '#979797',
    backgroundColor: '#f6f6f6',
    padding: 2,
  },
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
};

const mapStateToProps = (state) => ({
  component: state.component,
  // balance: 199,
});

export default connect(mapStateToProps)(CreditCard);
