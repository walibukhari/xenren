/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import Config from '../Config';
import { loadFinanceDetails } from './actions';
import FontStyle from "../constants/FontStyle";

class Balance extends Component {
  static navigationOptions = () => {
    return {
      headerTintColor: 'green',
      headerRight: (
        <TouchableOpacity
          onPress={this.handlePaymentRecord}
        >
          <Image
            source={require('../../images/other/icon_balance.png')}
            style={{
              width: 30,
              height: 30,
              resizeMode: 'contain',
              marginRight: 10,
              tintColor: Config.primaryColor,
            }}
          />
        </TouchableOpacity>
      ),
      headerTitleStyle: { marginLeft: 5,
          fontSize: 16,
          fontWeight:'bold',
          color: Config.primaryColor,
          fontFamily: FontStyle.Regular, },
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    balance: PropTypes.number.isRequired,
    points: PropTypes.number.isRequired,
    loadInitialData: PropTypes.func.isRequired,
  };

  handlePaymentRecord = () => {
    this.props.navigation.navigate('PaymentRecord');
  };

   handlePaymentRecord1 = () => {
     this.props.navigation.navigate('ChatDetail');
   };


  handleDeposit = () => {
    this.props.navigation.navigate('Deposit', { securityDeposit: false });
  };

  handleSecurityDeposit = () => {
    const { navigation } = this.props;
    navigation.navigate('Deposit', { securityDeposit: true });
  };

  handleTopUp = () => {
    this.props.navigation.navigate('TopUp');
  };

  componentDidMount() {
    // Set header right button action
    this.props.navigation.setParams({
      handlePayment: this.handlePaymentRecord,
    });
    // Load initial data
    this.props.loadInitialData();
  }

  render() {
    return (
      <View style={styles.rootStyle}>
        <View style={styles.mainViewportStyle}>
          <View style={styles.itemDividerStyle}>
            <TouchableOpacity onPress={this.handleTopUp}>
              <View style={styles.itemStyle}>
                <Image
                  source={require('../../images/other/icon_topup.png')}
                  style={{
                    tintColor: Config.primaryColor,
                    width: 90,
                    height: 90,
                  }}
                />
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 25,
                    marginTop: 15,
                    fontWeight: 'bold',
                  }}
                >
                  TOP UP
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleDeposit}>
              <View style={styles.itemStyle}>
                <Image
                  source={require('../../images/other/icon_withdraw.png')}
                  style={{
                    tintColor: Config.primaryColor,
                    width: 90,
                    height: 90,
                  }}
                />
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 25,
                    marginTop: 15,
                    fontWeight: 'bold',
                  }}
                >
                  WITHDRAW
                </Text>
              </View>
            </TouchableOpacity>
			  <TouchableOpacity onPress={this.handlePaymentRecord1}>
              <View style={styles.itemStyle}>
                <Image
                  source={require('../../images/other/icon_withdraw.png')}
                  style={{
                    tintColor: Config.primaryColor,
                    width: 90,
                    height: 90,
                  }}
                />
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 25,
                    marginTop: 15,
                    fontWeight: 'bold',
                  }}
                >
                  WITHDRAW1
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.itemDividerStyle}>
            <View style={styles.itemStyle}>
              <Text
                style={{ color: '#a1a1a1', fontSize: 28, fontWeight: 'bold' }}
              >
                Balance
              </Text>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 38,
                    marginRight: 10,
                    fontWeight: 'bold',
                  }}
                >
                  {this.props.balance}
                </Text>
                <Image
                  source={require('../../images/other/icon_yen.png')}
                  style={{
                    width: 40,
                    height: 40,
                    tintColor: Config.primaryColor,
                    marginTop: 7,
                  }}
                />
              </View>
            </View>
            <View style={styles.itemStyle}>
              <Text
                style={{ color: '#a1a1a1', fontSize: 28, fontWeight: 'bold' }}
              >
                XEN POINTS
              </Text>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 38,
                    marginRight: 10,
                    fontWeight: 'bold',
                  }}
                >
                  {`${this.props.points} XEN`}
                </Text>
              </View>
            </View>
          </View>
          <TouchableOpacity onPress={ this.handleSecurityDeposit } >
            <View style={{ alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Text
                  style={{
                    color: Config.primaryColor,
                    fontSize: 38,
                    marginRight: 10,
                    fontWeight: 'bold',
                  }}
                >
                  WITHDRAW 474
                </Text>
                <Image
                  source={require('../../images/other/icon_yen.png')}
                  style={{
                    width: 40,
                    height: 40,
                    tintColor: Config.primaryColor,
                    marginTop: 7,
                  }}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    ); // return
  } // render
} // Withdraw


const styles = {
  rootStyle: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  mainViewportStyle: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '75%',
    paddingVertical: 30,
  },

  itemDividerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 40,
  },

  itemStyle: {
    flexDirection: 'column',
  },
};

const mapDispatchToProps = (dispatch) => {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
    loadInitialData: () => dispatch(loadFinanceDetails()),
  };
};

const mapStateToProps = (state) => ({
	 component: state.component,
  balance: state.finance.balance,
  points: state.finance.points,
  // balance: 20,
});

export default connect(mapStateToProps, mapDispatchToProps)(Balance);
