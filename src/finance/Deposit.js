/* eslint-disable global-require */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';

import FontAwesomeIcons, {FA5Style} from 'react-native-vector-icons/FontAwesome5';
import Payment from './Payment';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';
import CurrencySelector from './CurrencySelector';
import Config from "../Config";
import FontStyle from "../constants/FontStyle";

// eslint-disable-next-line import/no-unresolved
const icTopupSuccess = require('../../images/balance&withdraw/topUp_success.png');

class Deposit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currSymbol: 'RM',
      userBalance: '...',
    };
  }

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    userData: PropTypes.object.isRequired,
    balance: PropTypes.number.isRequired,
  };

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    if (this.props.userData.token) {
      this.checkBalance();
    } else {
      setTimeout(() => {
        Alert.alert(
          'Warning',
          'Token expire!',
          [
            {
              text: 'OK',
              onPress: () => {
                this.logout();
              },
            },
          ],
        );
      }, 200);
    }
  }

  checkBalance = () => {
    HttpRequest.me(this.props.userData.token)
      .then((res) => {
        const userBalance = res.data.data.account_balance
          ? res.data.data.account_balance.toFixed(2)
          : 0;
        this.setState({ userBalance });
        if (res.data.status !== 'success') {
          Alert.alert('Error', res.data.status, [{ text: 'OK' }]);
        }
      })
      .catch(() => {
        Alert.alert('Error', translate('network_error'), [{ text: 'OK' }]);
      });
  }

  renderTopView = () => {
    const { securityDeposit } = this.props.navigation.state.params;
    if (securityDeposit) {
      return (
        <View style={styles.topWrapperStyle}>
          <Image
            source={icTopupSuccess}
            resizeMode="contain"
            style={{ width: 120, height: 100 }}
          />
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <Text style={styles.priceTextStyle}>Deposit</Text>
            <Text style={styles.priceTextStyle}>199</Text>
            <Text style={styles.priceTextStyle}>¥</Text>
          </View>
          <Text style={styles.priceDescription}>
            {translate('withdraw_deposit_anytime')}
          </Text>
        </View>
      );
    }

    return (
      <View>
        <View style={styles.backgroundWrapper}>
          <Image
            source={require('../../images/profile/new-ui/green-background.png')}
            resizeMode="cover"
            style={{
              height: 210,
              width: '110%',
              left: -10,
              position: 'absolute',
            }}
          />
          <View style={styles.inlineWrapper}>
            {/* <Image
              source={require('../../images/profile/new-ui/yen-icon.png')}
              resizeMode="cover"
              style={{ height: 35, width: 35 }}
            /> */}
            <Text style={[
              styles.headerText,
              {
                marginLeft: 8,
                width: '100%',
                fontSize: 18,
                fontWeight: 'bold',
              },
            ]}>
              {translate('my_current_balance')}
            </Text>
          </View>
          <View style={[
            styles.inlineWrapper,
            {
              position: 'absolute',
              right: 0,
              bottom: 0,
            },
          ]}>
            <Text style={[
              styles.headerText,
              {
                marginLeft: 8,
                fontSize: 46,
                fontWeight: 'bold',
              },
            ]}>
              {this.state.currSymbol}{' '}
              { this.state.userBalance }
            </Text>
          </View>
        </View>
        <View style={styles.amountInputWrapper}>
          <Text style={styles.amountInputTitle}>Withdraw Amount</Text>
          <View style={styles.amountInputBox}>
            <Text style={styles.amountInputAppend}>{this.state.currSymbol}</Text>
            <TextInput
              underlineColorAndroid="transparent"
              placeholder="Enter Amount"
              keyboardType='numeric'
            />
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.rootStyle}>
        {/* Back Button View */}
        <View style={styles.navigationStyle}>
          <View style={styles.navigationWrapper}>
            <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                style={{
                  marginTop: 8,
                  marginLeft: 4,
                  flexDirection: 'row',
                }}
            >
              <Image
                  style={{width:20,height:26,position:'relative',top:-3}}
                  source={require('../../images/arrowLA.png')}
              />
            </TouchableOpacity>

            <Text
                onPress={() => {
                  this.props.navigation.goBack();
                }}
              style={{
                marginLeft: 10,
                marginTop:3,
                fontSize: 19.5,
                fontWeight:'normal',
                color: Config.primaryColor,
                fontFamily: FontStyle.Regular,
              }}
            >
              {translate('withdraw')}
            </Text>

            <CurrencySelector
              onSelected={(data) => {
                this.setState({
                  currSymbol: data.curr_symbol,
                });
              }} />

          </View>
        </View>

        <ScrollView>
          {this.renderTopView()}
          <Text style={[styles.amountInputTitle, { marginHorizontal: 15 }]}>
            Withdraw Amount With
          </Text>
          <Payment
            action="Withdraw"
            height={Dimensions.get('window').height - 290}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },

  navigationStyle: {
    height: 55,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 10,
    // alignItems:'center'
  },
  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },

  topWrapperStyle: {
    backgroundColor: '#f4f4f4',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  normalTopWrapper: {
    backgroundColor: '#5fc229',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
  },
  backgroundWrapper: {
    height: 210,
    marginTop: 8,
    marginHorizontal: 15,
    borderRadius: 25,
    overflow: 'hidden',
    alignItems: 'stretch',
  },
  inlineWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
  },
  priceTextStyle: {
    marginRight: 5,
    fontFamily: 'Montserrat-Medium',
    fontSize: 32,
    color: '#000',
  },
  priceDescription: {
    fontFamily: 'Montserrat-Light',
    fontSize: 20,
    color: '#5c5c5c',
    marginTop: 5,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },
  balanceText: {
    color: '#fff',
    fontSize: 24,
  },

  amountInputWrapper: {
    margin: 15,
  },
  amountInputTitle: {
    color: '#000',
    padding: 2,
    paddingBottom: 8,
  },
  amountInputBox: {
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#adadad',
    flexDirection: 'row',
    alignItems: 'center',
  },
  amountInputAppend: {
    fontWeight: 'bold',
    color: '#000',
    fontSize: 20,
    marginRight: 10,
  },

};

const mapStateToProps = (state) => ({
  component: state.component,
  userData: state.auth.userData,
  // balance: state.auth.userData.account_balance,
});

export default connect(mapStateToProps)(Deposit);
