import { Dimensions, StyleSheet } from 'react-native';

const { height } = Dimensions.get('window');

const webViewStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    height: height * 0.09,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#EFF0F1',
    paddingTop: 10,
    paddingLeft: 10,
  },
});

export default webViewStyles;
