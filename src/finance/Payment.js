/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-const */
/* eslint-disable array-callback-return */
/* eslint-disable global-require */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  ActivityIndicator,
} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import Config from '../Config';
import { translate } from '../i18n';
import HttpRequest from '../components/HttpRequest';

let activePay = 'WeChat Pay';

class Payment extends Component {
  static propTypes = {
    children: PropTypes.node,
    action: PropTypes.string.isRequired,
    style: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const withdrawalSources = [
      // {
      //   id: 1,
      //   name: translate('wechatpay'),
      //   type: 'Recharge',
      //   logo: require('../../images/other/icon_wechat_pay.png'),
      //   selected: false,
      // },
      // {
      //   id: 2,
      //   name: translate('alipay'),
      //   type: 'Recharge',
      //   logo: require('../../images/other/icon_ali_pay.png'),
      //   selected: false,
      // },
      {
        id: 3,
        name: translate('paypal'),
        type: 'Recharge',
        logo: require('../../images/other/paypal-logo.png'),
        selected: false,
      },
      // {
      //   id: 4,
      //   name: translate('creditCard'),
      //   type: 'Recharge',
      //   logo: require('../../images/other/card.png'),
      //   selected: false,
      // },
      {
        id: 5,
        name: 'Boost | Grab Pay | Alipay',
        type: 'Recharge',
        logo: require('../../images/cc.png'),
        selected: false,
      },
    ];

    this.state = {
      paymentGateways: [],
      selectedPaymentMethods: '',
      withdrawalSources,
      isLoading: false,
      redirectUrl: 'https://www.sandbox.paypal.com/cgi-bin/webscr',
      elHeight: 0,
    };
  }

  handlePayment = () => {
    if (activePay === 'Credit Card') {
      if (this.props.action === 'TO PAY') {
        activePay = 'WeChat Pay';
        this.props.navigation.navigate('CreditCardTop');
      } else {
        activePay = 'WeChat Pay';
        this.props.navigation.navigate('CreditCard');
      }
    } else {
      alert('Please Select Credit Card');
    }
  };

  changePaymentMethod = (item) => {
    let self = this;
    let temp = this.state.withdrawalSources;

    console.log('changePaymentMethod', item);
    temp.map((subItem) => {
      if (subItem.id === item.id) {
        subItem.selected = true;
        activePay = subItem.name;
        self.selectedPaymentMethods = item.id;
      } else {
        subItem.selected = false;
      }
    });
    console.log('changePaymentMethod 1', temp);
    this.setState({
      selectedPaymentMethods: self.selectedPaymentMethods,
      withdrawalSources: temp,
    });
  };

  generateToken = () => {
    const { action, userInfo, amount } = this.props;
    const { selectedPaymentMethods } = this.state;
    this.setState({ isLoading: true });
    console.log(this.props);

    if (action === 'WITHDRAW' || action === '提款') {
      console.log('masuk withdraw');
      HttpRequest.withdrawApi(userInfo.token, amount)
        .then((response) => {
          if (response.data.status === 'success') {
            this.setState({ isLoading: false });
            alert(response.data.message);
          } else {
            alert(response.data.message);
          }
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          alert(error);
        });
      return;
    }

    console.log(selectedPaymentMethods);
    if (!selectedPaymentMethods) {
      alert('Please Select Payment Method');
      this.setState({ isLoading: false });
      return;
    }

    if (selectedPaymentMethods === 5) {
      console.log('rm selected');
      this.revenueMonsterLink();
    } else {
      HttpRequest.generateToken()
        .then((response) => {
          this.paypalCheckout(response.data.access_token);
        })
        // eslint-disable-next-line no-unused-vars
        .catch((error) => {
          this.setState({ isLoading: false });
          alert('Error occurred. Please try again');
        });
    }
  };

  revenueMonsterLink() {
    const { amount, userInfo, navigation } = this.props;
    HttpRequest.initiateRevenueMonsterPayment(userInfo.token, amount)
      .then((response) => {
        console.log('revenueMonsterLink', response);
        navigation.navigate('RevenutMonster', {
          RevenutMonsterLink: response.data.data,
          RevenutMonsterToken: 'token',
          payment_id: 2,
        });
      })
      .catch((error) => {
        alert(error);
        this.setState({ isLoading: false });
      });
  }

  paypalTax = (amount) => {
    let taxCal = (amount * 2.75) / 100;
    taxCal = taxCal.toFixed(2);
    return taxCal.toString();
  };

  paypalCheckout = (token) => {
    const invoiceNumber = Math.floor(Math.random() * 1000000 + 1).toString();
    const { amount } = this.props;
    const paypalTax = this.paypalTax(amount);
    const totalAmount = (Number(amount) + Number(paypalTax)).toString();

    const data = JSON.stringify({
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      transactions: [
        {
          amount: {
            total: totalAmount,
            currency: 'USD',
            details: {
              subtotal: amount,
              tax: paypalTax,
              shipping: '0.00',
              handling_fee: '0.00',
              shipping_discount: '0.00',
              insurance: '0.00',
            },
          },
          description: 'The payment transaction description.',
          invoice_number: invoiceNumber,
          payment_options: {
            allowed_payment_method: 'INSTANT_FUNDING_SOURCE',
          },
          item_list: {
            items: [
              {
                name: 'Top-Up',
                description: 'Add balance into my Legends Lair account',
                quantity: '1',
                price: amount,
                tax: '0.00',
                sku: '0',
                currency: 'USD',
              },
            ],
            shipping_address: {
              recipient_name: 'Brian Robinson',
              line1: '4th Floor',
              line2: 'Unit #34',
              city: 'San Jose',
              country_code: 'US',
              postal_code: '95131',
              phone: '011862212345678',
              state: 'CA',
            },
          },
        },
      ],
      note_to_payer: 'Contact us for any questions on your order.',
      redirect_urls: {
        return_url: this.state.redirectUrl,
        cancel_url: this.state.redirectUrl,
      },
    });

    HttpRequest.initiatePayment(token, data)
      .then((response) => {
        this.setState({
          isLoading: false,
          redirectUrl: response.data.links[1].href,
        });
        this.props.navigation.navigate('PaypalView', {
          paypalLink: response.data.links[1].href,
          paypalToken: token,
          payment_id: response.data.id,
        });
      })
      // eslint-disable-next-line no-unused-vars
      .catch((error) => {
        alert('Error occurred. Please try again!');
        this.setState({ isLoading: false });
      });
  };

  selectPaymentMethod = (item) => {
    console.log('selectPaymentMethod', item);
    this.changePaymentMethod(item);
  }

  renderRow = ({ item, index }) => (
    <TouchableOpacity
      activeOpacity={1}
      style={{ marginHorizontal: 15, marginVertical: 0 }}
      onPress={() => this.selectPaymentMethod(item)}
    >
      <View style={[
        styles.boxInsideStyle,
        {
          borderTopWidth: 1,
          borderTopColor: '#d4d4d4',
          borderBottomWidth: this.state.withdrawalSources.length === index + 1 ? 1 : 0,
          borderBottomColor: '#d4d4d4',
        },
      ]}>
        <View style={{ flexDirection: 'row' }}>
          <Image
            source={item.logo}
            resizeMode="contain"
            style={{ width: 30, height: 30 }}
          />
          <View style={{ flexDirection: 'column', justifyContent: 'center', marginLeft: 10 }}>
            <Text
              style={{
                fontSize: 12,
                fontFamily: 'Montserrat-Light',
                color: '#000',
              }}
            >
              {item.name}
            </Text>
            {/* <Text
              style={{
                fontSize: 12,
                fontFamily: 'Montserrat-Light',
                color: '#888888',
              }}
            >
              {item.type}
            </Text> */}
          </View>
        </View>
        <View>
          <View
            style={{
              width: 30,
              height: 30,
              borderRadius: 20,
              backgroundColor: item.selected ? Config.primaryColor : '#ebebeb',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Ionicon name="ios-checkmark" size={30} color="#fff" />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );

  render() {
    const { isLoading, elHeight, withdrawalSources } = this.state;
    return (
      <View
        style={[
          this.props.style,
          { justifyContent: 'space-between' },
          this.props.height && { height: this.props.height - elHeight - 65 },
        ]}
      >
        <View
          style={{ marginTop: 0 }}
          onLayout={(event) => {
            let {
              // eslint-disable-next-line no-unused-vars
              x, y, width, height,
            } = event.nativeEvent.layout;

            this.setState({ elHeight: height });
          }}
        >
          <FlatList
            data={withdrawalSources}
            extraData={this.state}
            renderItem={(rowData) => this.renderRow(rowData)}
            keyExtractor={(item, index) => index.toString()}
          />
          { this.props.showAgreement
            && <Text style={styles.footerTextDescription}>
              {translate('click')} {this.props.action}{' '}
              {translate('read_agree_agreement')}
              {/* Click {this.props.action} to indicate that
                you have read and agreed to recharging agreement */}
            </Text>
          }
        </View>
        <View style={styles.footerWrapper}>
          <TouchableOpacity
            onPress={() => this.generateToken()}
            style={[styles.buttonWrapper, styles.floatBottom]}
            disabled={isLoading}
          >
              {
                isLoading
                  ? (
                  <ActivityIndicator
                    style={{ marginHorizontal: 10 }}
                    color={Config.white}
                    size="small"
                  />)
                  : (
                  <Text
                    style={{
                      color: '#fff',
                      fontFamily: 'Montserrat-Medium',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}
                  >
                    {this.props.action}
                  </Text>)
              }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  boxInsideStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  footerWrapper: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginBottom: 8,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerTextDescription: {
    justifyContent: 'center',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#888888',
    fontFamily: 'Montserrat-Light',
    width: '70%',
    marginTop: 30,
    fontSize: 12,
  },
  buttonWrapper: {
    flexDirection: 'row',
    backgroundColor: Config.primaryColor,
    borderRadius: 10,
    height: 45,
    width: '85%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state) => ({
  userInfo: state.auth.userData,
});

export default connect(mapStateToProps)(Payment);
