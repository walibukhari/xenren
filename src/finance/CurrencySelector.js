import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import Select2 from '../screens/profile/Select2';
// eslint-disable-next-line import/no-unresolved
import { translate } from '../i18n';
import countryJsonData from '../screens/profile/country.json';
import Config from "../Config";

class CurrencySelector extends Component {
  static propTypes = {
    onSelected: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      country: 'MY',
      countryList: countryJsonData,
    };
  }

  render() {
    const select2 = (
      <Select2
        parentStyle={{ width: '50%', justifyContent: 'flex-end' }}
        dataSource={this.state.countryList}
        renderId={(item) => item.code}
        renderRow={(item) => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              paddingHorizontal: 10,
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1}}>{item.name}</Text>
          </View>
        )}
        renderDisplay={(item) => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1,color:Config.primaryColor }}>({item.curr_code}) {item.name}</Text>
            <Ionicons name="md-arrow-dropdown" color="#57b029" />
          </View>
        )}
        renderEmpty={() => (
          <View
            style={{
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}
          >
            <Text style={{ flex: 1 }}>-</Text>
            <Ionicons name="md-arrow-dropdown" color="#000" />
          </View>
        )}
        renderLabel={(item) => item.name}
        cancelText={translate('cancel')}
        selectedValue={this.state.country}
        onSelect={(item) => {
          this.setState(
            { country: item.code },
            () => {
              this.props.onSelected(item);
            },
          );
        }}
      />
    );

    return (
      <View style={styles.rootStyle}>
        <View style={styles.inputWrapperStyle}>
          {select2}
        </View>
      </View>
    );
  }
}

const styles = {
  rootStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },

  navigationStyle: {
    height: 45,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 10,
    // alignItems:'center'
  },
  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  inputWrapperStyle: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
};

export default CurrencySelector;
