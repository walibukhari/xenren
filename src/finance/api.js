/* eslint-disable prefer-promise-reject-errors */
import Config from '../Config';

export const { baseUrl } = Config;

// const loadFinanceDetails = () => axios.get(`${baseUrl}/finance/details`).then((res) => {
const loadFinanceDetails = () => Promise.resolve({
  status: 'success',
  data: {
    balance: 3105,
    points: 30,
  },
}).then((res) => { // TODO Delete when api ready
  if (res.status !== 'success') return Promise.reject('Can\'t load details');
  return Promise.resolve(res.data);
});

export default {
  loadFinanceDetails,
};
