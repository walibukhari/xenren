/* eslint-disable import/no-unresolved */
/* eslint-disable no-unused-vars */
/* eslint-disable camelcase */
/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Text, TouchableOpacity, View, Dimensions, Image, BackHandler,
} from 'react-native';
import { NavigationActions, withNavigation } from 'react-navigation';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Config from '../Config';
import FontStyle from '../constants/FontStyle';
import icon_xenren from '../../images/balance&withdraw/topUp_success.png';

const { width } = Dimensions.get('window');

class TopUpSuccess extends React.Component {
  static propTypes = {
    layout: PropTypes.array.isRequired,
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    onSeatSelected: PropTypes.func,
    navigation: PropTypes.object.isRequired,
  };

  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerTintColor: Config.topNavigation.headerIconColor,
    headerTitleStyle:
        {
          color: Config.topNavigation.headerTextColor,
          alignSelf: 'center',
          fontFamily: FontStyle.Regular,
          width: '100%',
        },
    headerLeft: null,
  });

  goToMyProfile = () => {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({
          routeName: 'Profile',
        }),
      ],
    }));
  }

  render() {
    const { amount, totalBalance } = this.props.navigation.state.params;
    return (
        <View onLayout={this.onLayout} style={style.containerStyle}>
        <View style={style.uppperContainer}>
          <View style={style.seat}>
            <Image source={icon_xenren} />
          </View>
          <Text style={style.title}>Top up Success</Text>
          <Text style={style.subtitle}>Amount: {amount} USD</Text>
          <Text style={style.subtitle}>Current Balance: {totalBalance} USD</Text>
        </View>
        <TouchableOpacity style={style.okButton} onPress={() => this.goToMyProfile()}>
          <Text style={style.okText}>
            OK
            </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const style = StyleSheet.create({
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Config.white,
  },
  okButton: {
    height: 45,
    backgroundColor: Config.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: width - 40,
    marginBottom: 20,
  },
  okText: {
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
  },
  seat: {
    marginVertical: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  uppperContainer: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontSize: 40,
    textAlign: 'center',
    marginTop: 20,
    fontFamily: FontStyle.Medium,
  },
  subtitle: {
    fontSize: 18,
    color: 'grey',
    textAlign: 'center',
    marginTop: 10,
    fontFamily: FontStyle.Regular,
  },
});

export default TopUpSuccess;
