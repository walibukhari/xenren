import Api from './api';

export const RECEIVE_DETAILS = 'Finance/ReceiveDetails';
export const receiveDetails = (balance, points) => ({
  type: RECEIVE_DETAILS,
  payload: {
    balance,
    points,
  },
});


export const loadFinanceDetails = () => (dispatch) => {
  Api.loadFinanceDetails().then((res) => {
    const { balance, points } = res;
    // console.log('Console Balance and Points value ----------->>>> ', balance, points);
    dispatch(receiveDetails(balance, points));
  });
};
