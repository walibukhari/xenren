/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  TextInput,
  Image,
  Alert,
} from 'react-native';
import FontAwesomeIcons, {FA5Style} from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Config from '../Config';
import Select2 from '../screens/profile/Select2';
import { translate } from '../i18n';
import FontStyle from '../constants/FontStyle';
import HttpRequest from '../components/HttpRequest';
import Payment from './Payment';
import CurrencySelector from './CurrencySelector';

// const icRM = require('../../images/profile/new-ui/ringgit-icon.png');
const greenBg = require('../../images/profile/new-ui/green-background.png');

class TopUp extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      activeButtonIndex: 4,
      customAmount: '',
      topUpAmount: '200',
      userBalance: '...',
      currSymbol: 'RM',
    };
  }

  componentDidMount() {
    console.log('TopUp.js');
    if (this.props.userData.token) {
      this.checkBalance();
    } else {
      setTimeout(() => {
        Alert.alert(
          'Warning',
          'Token expire!',
          [
            {
              text: 'OK',
              onPress: () => {
                this.logout();
              },
            },
          ],
        );
      }, 200);
    }
  }

  checkBalance = () => {
    HttpRequest.me(this.props.userData.token)
      .then((res) => {
        const userBalance = res.data.data.account_balance
          ? res.data.data.account_balance.toFixed(2)
          : 0;
        this.setState({ userBalance });
        if (res.data.status !== 'success') {
          Alert.alert('Error', res.data.status, [{ text: 'OK' }]);
        }
      })
      .catch(() => {
        Alert.alert('Error', translate('network_error'), [{ text: 'OK' }]);
      });
  }

  setAmount = (activeButtonIndex, customAmount = '') => {
    let topUpAmount = '';

    if (activeButtonIndex === 1) topUpAmount = '30';
    if (activeButtonIndex === 2) topUpAmount = '50';
    if (activeButtonIndex === 3) topUpAmount = '100';
    if (activeButtonIndex === 4) topUpAmount = '200';

    this.setState({
      activeButtonIndex,
      topUpAmount,
      customAmount,
    });
  }

  render() {
    const { activeButtonIndex } = this.state;

    return (
      <View style={styles.rootStyle}>
        {/* Back Button View */}
        <View style={styles.navigationStyle}>
          <View style={styles.navigationWrapper}>
            <TouchableOpacity
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                style={{
                  marginTop: 8,
                  marginLeft: 5,
                  flexDirection: 'row',
                }}
            >
              <Image
                  style={{width:20,height:26,position:'relative',top:-3}}
                  source={require('../../images/arrowLA.png')}
              />
            </TouchableOpacity>

            <Text
              style={{
                marginLeft: 10,
                marginTop:2,
                fontSize: 19.5,
                fontWeight:'normal',
                color: Config.primaryColor,
                fontFamily: FontStyle.Regular,
              }}
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              {translate('recharge')}
            </Text>

            <CurrencySelector
              onSelected={(data) => {
                this.setState({
                  currSymbol: data.curr_symbol,
                });
              }} />

          </View>
        </View>

        <ScrollView>
          <View style={styles.backgroundWrapper}>
            <Image
              source={greenBg}
              resizeMode="cover"
              style={{
                height: 210, width: '110%', left: -10, position: 'absolute',
              }}
            />
            <View style={styles.inlineWrapper}>
              {/* <Image
                source={icRM}
                resizeMode="cover"
                style={{ height: 35, width: 35 }}
              /> */}
              <Text style={[
                styles.headerText,
                {
                  marginLeft: 8,
                  width: '100%',
                  fontSize: 18,
                  fontWeight: 'bold',
                },
              ]}>
                {translate('my_current_balance')}
              </Text>
            </View>
            <View style={[
              styles.inlineWrapper,
              {
                position: 'absolute',
                right: 0,
                bottom: 0,
              },
            ]}>
              <Text style={[
                styles.headerText,
                {
                  marginLeft: 8,
                  fontSize: 46,
                  fontWeight: 'bold',
                },
              ]}>
                {this.state.currSymbol}{' '}
                {this.state.userBalance}
              </Text>
            </View>
          </View>
          <View style={styles.selectPriceWrapper}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={[
                  styles.button,
                  styles.smallButton,
                  activeButtonIndex === 1 && styles.activeButton,
                ]}
                onPress={() => this.setAmount(1)}>
                <Text
                  style={[
                    styles.buttonTextStyle,
                    activeButtonIndex === 1 && styles.activebuttonTextStyle,
                  ]}>
                  {this.state.currSymbol}{' '}30
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.button,
                  styles.smallButton,
                  activeButtonIndex === 2 && styles.activeButton,
                ]}
                onPress={() => this.setAmount(2)}>
                <Text
                  style={[
                    styles.buttonTextStyle,
                    activeButtonIndex === 2 && styles.activebuttonTextStyle,
                  ]}>
                  {' '}
                  {this.state.currSymbol}{' '}50
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.button,
                  styles.smallButton,
                  activeButtonIndex === 3 && styles.activeButton,
                ]}
                onPress={() => this.setAmount(3)}>
                <Text
                  style={[
                    styles.buttonTextStyle,
                    activeButtonIndex === 3 && styles.activebuttonTextStyle,
                  ]}>
                  {this.state.currSymbol}{' '}100
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.button,
                  styles.smallButton,
                  activeButtonIndex === 4 && styles.activeButton,
                ]}
                onPress={() => this.setAmount(4)}>
                <Text
                  style={[
                    styles.buttonTextStyle,
                    activeButtonIndex === 4 && styles.activebuttonTextStyle,
                  ]}>
                  {' '}
                  {this.state.currSymbol}{' '}200
                </Text>
              </TouchableOpacity>
            </ScrollView>
            <View style={styles.amountBox}>
              <TouchableOpacity
                style={{ width: '100%' }}
                onPress={() => {
                  this.setState({ activeButtonIndex: 5 });
                  this.custom.focus();
                }}>
                <View style={[
                  styles.amountInputWrapper,
                  activeButtonIndex === 5 && styles.activeInput,
                ]}>
                  <View style={styles.amountInputBox}>
                    {/* ¥ yen */}
                    <Text style={styles.amountInputAppend}>RM</Text>
                    <TextInput
                      ref={(input) => {
                        this.custom = input;
                      }}
                      maxLength={5}
                      placeholder="Custom Amount"
                      keyboardType='numeric'
                      underlineColorAndroid='transparent'
                      value={this.state.customAmount}
                      onFocus={() => this.setState({ activeButtonIndex: 5 })}
                      onChangeText={(text) => this.setAmount(5, text)}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          {/**/}
          <Text style={[styles.amountInputTitle, { marginHorizontal: 15 }]}>
            Select Payment
          </Text>
          <Payment
            showAgreement={true}
            action={translate('recharge')}
            navigation={this.props.navigation}
            amount={this.state.customAmount !== '' ? this.state.customAmount : this.state.topUpAmount}
          />
        </ScrollView>
       </View>
    );
  }
}

const styles = StyleSheet.create({
  rootStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
  },

  navigationStyle: {
    height: 55,
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 10,
    // alignItems:'center'
  },
  navigationWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },

  selectPriceWrapper: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  customAmount: {
    marginLeft: 10,
    height: 40,
    width: 50,
    color: Config.white,
    fontSize: 16,
    fontFamily: FontStyle.Regular,
    // fontWeight: '00',
  },
  button: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3e5259',
    paddingHorizontal: 20,
    borderRadius: 10,
    marginRight: 10,
  },
  activeButton: {
    backgroundColor: Config.primaryColor,
    color: '#fff',
  },
  smallButton: {
    width: 'auto',
  },
  buttonTextStyle: {
    // fontFamily: 'Montserrat-Light',
    color: '#ffff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  activebuttonTextStyle: {
    fontWeight: 'bold',
    color: '#ffffff',
  },

  backgroundWrapper: {
    marginTop: 8,
    marginHorizontal: 15,
    borderRadius: 25,
    height: 210,
    overflow: 'hidden',
    alignItems: 'stretch',
  },
  inlineWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },

  // Custom Amount
  amountBox: {
    flexDirection: 'row',
    marginTop: 10,
  },
  amountInputWrapper: {
    marginVertical: 10,
  },
  amountInputTitle: {
    color: '#000',
    padding: 2,
    paddingBottom: 8,
  },
  amountInputBox: {
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#adadad',
    flexDirection: 'row',
    alignItems: 'center',
  },
  amountInputAppend: {
    fontWeight: 'bold',
    color: '#000',
    fontSize: 20,
    marginRight: 10,
  },
});

function mapDispatchToProps(dispatch) {
  return {
    setRoot: (root) => dispatch({
      type: 'set_root',
      root,
    }),
    // eslint-disable-next-line no-undef
    loadInitialData: () => dispatch(loadFinanceDetails()),
  };
}

const mapStateToProps = (state) => ({
  component: state.component,
  userData: state.auth.userData,
  // balance: state.finance ? state.finance.balance : 0,
  // balance: 20,
});

export default connect(mapStateToProps, mapDispatchToProps)(TopUp);
