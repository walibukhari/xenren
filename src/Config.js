/**
 * **** config.js ***
 * All the configurations and static strings we're going to used all
 * over the app will stay in config.js .
* */
export default {
  // Style
  white: '#ffffff',
  primaryColor: '#57b029',
  textColor: '#727272',
  redColor: '#f26c4f',
  textSecondaryColor: '#888888',
  lineColor: '#ebebeb',
  lightColor: '#EAEAEA',
  backGroundColor: '#fcf7f7',
  btnDarkColor: '#363f4f',
  topNavigation: {
    headerIconColor: '#57af29',
    headerTextColor: '#6e6e6e',
  },
  // Api
  baseUrl: 'http://192.168.43.185:1234/api',
  baseUrlV1: 'http://192.168.43.185:1234/api/v1',
  webUrl: 'http://192.168.43.185:1234',
  basePathPayPal: 'https://api.sandbox.paypal.com/v1',
  GoogleApiKey: 'AIzaSyCIasatwSA8nyOeN8Cobzil6yO1jsT13rE',
  getIpAddress: 'https://api.myip.com',
};
