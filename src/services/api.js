/* eslint-disable max-len */
/* eslint-disable camelcase */
import axios from 'axios';
import Config from '../Config';
import localData from '../components/LocalData';

export const { baseUrl } = Config;

const user_id = 419;

const getAccessToken = async () => {
  const userData = await localData.getUserData();
  const token = await JSON.parse(userData).token;
  return token;
};

const getConfigHeader = async () => {
  const token = await getAccessToken();
  const config = {
    headers: { Authorization: `bearer ${token}` },
  };
  return config;
};

const getOfficeDetails = async officeId => axios.post(
  `${baseUrl}/officeDetailsBtn`, {
    user_id,
    office_id: officeId,
  },
  await getConfigHeader(),
);

const getOfficeLayout = async officeId => axios.post(
  `${baseUrl}/sharedofficeQrOfficeDoor`,
  {
    user_id,
    qrOfficeDoor: officeId,
  },
  await getConfigHeader(),
);

const getTrackerInfo = async transaction => axios.post(
  `${baseUrl}/sharedofficeClickBtSeatInfo `, {
    user_id,
    transaction_id: transaction,
  },
  await getConfigHeader(),
);

// const selectSeatAndStartTimer = async (seatId) => axios.post(`${baseUrl}/sharedofficeClickBtSeat`, {
//   user_id,
//   seat_id: seatId,
// },
//   await getConfigHeader(),
// );

const selectSeatAndStartTimer = async seatId => axios.post(
  `${baseUrl}/sharedofficeClickBtSeat`, {
    user_id,
    seat_id: seatId,
  },
  await getConfigHeader(),
);

const selectSeatFromQRToStartTimer = async qr_seat => axios.post(
  `${baseUrl}/sharedofficeQrSeat`,
  {
    user_id,
    qr_seat,
  },
  await getConfigHeader(),
);

const stopTimer = async () => axios.post(
  `${baseUrl}/sharedofficeStopTimer`,
  // {
  //   user_id,
  //   transaction_id: transactionId,
  // },
  await getConfigHeader(),
);

export default {
  getOfficeDetails,
  getOfficeLayout,
  getTrackerInfo,
  selectSeatAndStartTimer,
  stopTimer,
  selectSeatFromQRToStartTimer,
};
