package com.xenren;

import com.AlexanderZaytsev.RNI18n.RNI18nPackage;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import io.xogus.reactnative.versioncheck.RNVersionCheckPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.horcrux.svg.SvgPackage;
import com.reactnativecommunity.slider.ReactSliderPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import io.sentry.RNSentryPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.filepicker.FilePickerPackage; // import package

import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import com.imagepicker.ImagePickerPackage;
import com.github.kevinejohn.keyevent.KeyEventPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import me.pushy.sdk.react.PushyPackage;
import org.reactnative.camera.RNCameraPackage;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.RNFetchBlob.RNFetchBlobPackage;


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
protected static CallbackManager getCallbackManager() {
    return mCallbackManager;
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNGestureHandlerPackage(),
            new RNVersionCheckPackage(),
            new FastImageViewPackage(),
            new SvgPackage(),
            new ReactSliderPackage(),
            new MapsPackage(),
            new RNDeviceInfo(),
            new RNSentryPackage(),
            new FilePickerPackage(), // Add package
            new RNCameraPackage(),
            new RNFirebasePackage(),
            new RNI18nPackage(),
            new ImagePickerPackage(),
            new KeyEventPackage(),
            new VectorIconsPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new FBSDKPackage(mCallbackManager),
            new PushyPackage(),
            new RNNetworkInfoPackage(),
            new AsyncStoragePackage(),
            new RNCWebViewPackage(),
            new RNFetchBlobPackage()

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    AppEventsLogger.activateApp(this);
    SoLoader.init(this, /* native exopackage */ false);
  //   try {
  //       FirebaseApp.initializeApp(this);
  //   }
  //   catch (Exception e) {
  // }
}
}
